package com.gds.appraisalmaybank.database;

public class Motor_Vehicle_API {

    // private variables
    int id;
    String record_id;
    String report_interior_dashboard;
    String report_interior_sidings;
    String report_interior_seats;
    String report_interior_windows;
    String report_interior_ceiling;
    String report_interior_instrument;
    String report_bodytype_grille;
    String report_bodytype_headlight;
    String report_bodytype_fbumper;
    String report_bodytype_hood;
    String report_bodytype_rf_fender;
    String report_bodytype_rf_door;
    String report_bodytype_rr_door;
    String report_bodytype_rr_fender;
    String report_bodytype_backdoor;
    String report_bodytype_taillight;
    String report_bodytype_r_bumper;
    String report_bodytype_lr_fender;
    String report_bodytype_lr_door;
    String report_bodytype_lf_door;
    String report_bodytype_lf_fender;
    String report_bodytype_top;
    String report_bodytype_paint;
    String report_bodytype_flooring;
    String valrep_mv_bodytype_trunk;
    String report_misc_stereo;
    String report_misc_tools;
    String report_misc_speakers;
    String report_misc_airbag;
    String report_misc_tires;
    String report_misc_mag_wheels;
    String report_misc_carpet;
    String report_misc_others;
    String report_enginearea_fuel;
    String report_enginearea_transmission1;
    String report_enginearea_transmission2;
    String report_enginearea_chassis;
    String report_enginearea_battery;
    String report_enginearea_electrical;
    String report_enginearea_engine;
    String report_verification;
    String report_verification_district_office;
    String report_verification_encumbrance;
    String report_verification_registered_owner;
    String report_place_inspected;
    String report_market_valuation_fair_value;
    String report_market_valuation_min;
    String report_market_valuation_max;
    String report_market_valuation_remarks;
    String valrep_mv_market_valuation_previous_remarks;

    String report_date_inspected_month;
    String report_date_inspected_day;
    String report_date_inspected_year;
    String report_time_inspected;

    //ADDED From IAN
    String report_comp1_source;
    String report_comp1_tel_no;
    String report_comp1_unit_model;
    String report_comp1_mileage;
    String report_comp1_price_min;
    String report_comp1_price_max;
    String report_comp1_price;
    String report_comp1_less_amt;
    String report_comp1_less_net;
    String report_comp1_add_amt;
    String report_comp1_add_net;
    String report_comp1_time_adj_value;
    String report_comp1_time_adj_amt;
    String report_comp1_adj_value;
    String report_comp1_adj_amt;
    String report_comp1_index_price;
    String report_comp2_source;
    String report_comp2_tel_no;
    String report_comp2_unit_model;
    String report_comp2_mileage;
    String report_comp2_price_min;
    String report_comp2_price_max;
    String report_comp2_price;
    String report_comp2_less_amt;
    String report_comp2_less_net;
    String report_comp2_add_amt;
    String report_comp2_add_net;
    String report_comp2_time_adj_value;
    String report_comp2_time_adj_amt;
    String report_comp2_adj_value;
    String report_comp2_adj_amt;
    String report_comp2_index_price;
    String report_comp3_source;
    String report_comp3_tel_no;
    String report_comp3_unit_model;
    String report_comp3_mileage;
    String report_comp3_price_min;
    String report_comp3_price_max;
    String report_comp3_price;
    String report_comp3_less_amt;
    String report_comp3_less_net;
    String report_comp3_add_amt;
    String report_comp3_add_net;
    String report_comp3_time_adj_value;
    String report_comp3_time_adj_amt;
    String report_comp3_adj_value;
    String report_comp3_adj_amt;
    String report_comp3_index_price;
    String report_comp_ave_index_price;
    String report_rec_lux_car;
    String report_rec_mile_factor;
    String report_rec_taxi_use;
    String report_rec_for_hire;
    String report_rec_condition;
    String report_rec_repo;
    String report_rec_other_val;
    String report_rec_other_desc;
    String report_rec_other2_val;
    String report_rec_other2_desc;
    String report_rec_other3_val;
    String report_rec_other3_desc;
    String report_rec_market_resistance_rec_total;
    String report_rec_market_resistance_total;
    String report_rec_market_resistance_net_total;
    String report_rep_stereo;
    String report_rep_speakers;
    String report_rep_tires_pcs;
    String report_rep_tires;
    String report_rep_side_mirror_pcs;
    String report_rep_side_mirror;
    String report_rep_light;
    String report_rep_tools;
    String report_rep_battery;
    String report_rep_plates;
    String report_rep_bumpers;
    String report_rep_windows;
    String report_rep_body;
    String report_rep_engine_wash;
    String report_rep_other_desc;
    String report_rep_other;
    String report_rep_total;
    String report_appraised_value;
    //ADDED BY IAN JAN 13 2016
    String report_verification_file_no;
    String report_verification_first_reg_date_month;
    String report_verification_first_reg_date_day;
    String report_verification_first_reg_date_year;
    String report_comp1_mileage_value;
    String report_comp2_mileage_value;
    String report_comp3_mileage_value;
    String report_comp1_mileage_amt;
    String report_comp2_mileage_amt;
    String report_comp3_mileage_amt;
    String report_comp1_adj_desc;
    String report_comp2_adj_desc;
    String report_comp3_adj_desc;

    String valrep_mv_comp1_new_unit_value;
    String valrep_mv_comp2_new_unit_value;
    String valrep_mv_comp3_new_unit_value;
    String valrep_mv_comp1_new_unit_amt;
    String valrep_mv_comp2_new_unit_amt;
    String valrep_mv_comp3_new_unit_amt;

    String valrep_mv_comp_add_index_price_desc;
    String valrep_mv_comp_add_index_price;
    String valrep_mv_comp_temp_ave_index_price;
    String valrep_mv_rep_other2_desc;
    String valrep_mv_rep_other2;
    String valrep_mv_rep_other3_desc;
    String valrep_mv_rep_other3;
    // Empty constructor
    public Motor_Vehicle_API() {

    }

    // constructor
    public Motor_Vehicle_API(int id, String record_id,
                             String report_interior_dashboard, String report_interior_sidings,
                             String report_interior_seats, String report_interior_windows,
                             String report_interior_ceiling, String report_interior_instrument,
                             String report_bodytype_grille, String report_bodytype_headlight,
                             String report_bodytype_fbumper, String report_bodytype_hood,
                             String report_bodytype_rf_fender, String report_bodytype_rf_door,
                             String report_bodytype_rr_door, String report_bodytype_rr_fender,
                             String report_bodytype_backdoor, String report_bodytype_taillight,
                             String report_bodytype_r_bumper, String report_bodytype_lr_fender,
                             String report_bodytype_lr_door, String report_bodytype_lf_door,
                             String report_bodytype_lf_fender, String report_bodytype_top,
                             String report_bodytype_paint, String report_bodytype_flooring, String valrep_mv_bodytype_trunk,
                             String report_misc_stereo, String report_misc_tools,
                             String report_misc_speakers, String report_misc_airbag,
                             String report_misc_tires, String report_misc_mag_wheels,
                             String report_misc_carpet, String report_misc_others,
                             String report_enginearea_fuel,
                             String report_enginearea_transmission1,
                             String report_enginearea_transmission2,
                             String report_enginearea_chassis, String report_enginearea_battery,
                             String report_enginearea_electrical,
                             String report_enginearea_engine, String report_verification,
                             String report_verification_district_office,
                             String report_verification_encumbrance,
                             String report_verification_registered_owner,
                             String report_place_inspected,
                             String report_market_valuation_fair_value,
                             String report_market_valuation_min,
                             String report_market_valuation_max,
                             String report_market_valuation_remarks,
                             String valrep_mv_market_valuation_previous_remarks,
                             String report_date_inspected_month,
                             String report_date_inspected_day,
                             String report_date_inspected_year,
                             String report_time_inspected,
                             //ADDED By IAN
                             String report_comp1_source,
                             String report_comp1_tel_no,
                             String report_comp1_unit_model,
                             String report_comp1_mileage,
                             String report_comp1_price_min,
                             String report_comp1_price_max,
                             String report_comp1_price,
                             String report_comp1_less_amt,
                             String report_comp1_less_net,
                             String report_comp1_add_amt,
                             String report_comp1_add_net,
                             String report_comp1_time_adj_value,
                             String report_comp1_time_adj_amt,
                             String report_comp1_adj_value,
                             String report_comp1_adj_amt,
                             String report_comp1_index_price,
                             String report_comp2_source,
                             String report_comp2_tel_no,
                             String report_comp2_unit_model,
                             String report_comp2_mileage,
                             String report_comp2_price_min,
                             String report_comp2_price_max,
                             String report_comp2_price,
                             String report_comp2_less_amt,
                             String report_comp2_less_net,
                             String report_comp2_add_amt,
                             String report_comp2_add_net,
                             String report_comp2_time_adj_value,
                             String report_comp2_time_adj_amt,
                             String report_comp2_adj_value,
                             String report_comp2_adj_amt,
                             String report_comp2_index_price,
                             String report_comp3_source,
                             String report_comp3_tel_no,
                             String report_comp3_unit_model,
                             String report_comp3_mileage,
                             String report_comp3_price_min,
                             String report_comp3_price_max,
                             String report_comp3_price,
                             String report_comp3_less_amt,
                             String report_comp3_less_net,
                             String report_comp3_add_amt,
                             String report_comp3_add_net,
                             String report_comp3_time_adj_value,
                             String report_comp3_time_adj_amt,
                             String report_comp3_adj_value,
                             String report_comp3_adj_amt,
                             String report_comp3_index_price,
                             String report_comp_ave_index_price,
                             String report_rec_lux_car,
                             String report_rec_mile_factor,
                             String report_rec_taxi_use,
                             String report_rec_for_hire,
                             String report_rec_condition,
                             String report_rec_repo,
                             String report_rec_other_val,
                             String report_rec_other_desc,
                             String report_rec_other2_val,
                             String report_rec_other2_desc,
                             String report_rec_other3_val,
                             String report_rec_other3_desc,
                             String report_rec_market_resistance_rec_total,
                             String report_rec_market_resistance_total,
                             String report_rec_market_resistance_net_total,
                             String report_rep_stereo,
                             String report_rep_speakers,
                             String report_rep_tires_pcs,
                             String report_rep_tires,
                             String report_rep_side_mirror_pcs,
                             String report_rep_side_mirror,
                             String report_rep_light,
                             String report_rep_tools,
                             String report_rep_battery,
                             String report_rep_plates,
                             String report_rep_bumpers,
                             String report_rep_windows,
                             String report_rep_body,
                             String report_rep_engine_wash,
                             String report_rep_other_desc,
                             String report_rep_other,
                             String report_rep_total,
                             String report_appraised_value,
                             //ADDED BY IAN JAN 13 2016
                             String report_verification_file_no,
                             String report_verification_first_reg_date_month,
                             String report_verification_first_reg_date_day,
                             String report_verification_first_reg_date_year,
                             String report_comp1_mileage_value,
                             String report_comp2_mileage_value,
                             String report_comp3_mileage_value,
                             String report_comp1_mileage_amt,
                             String report_comp2_mileage_amt,
                             String report_comp3_mileage_amt,
                             String report_comp1_adj_desc,
                             String report_comp2_adj_desc,
                             String report_comp3_adj_desc,
                             String valrep_mv_comp1_new_unit_value,
                             String valrep_mv_comp2_new_unit_value,
                             String valrep_mv_comp3_new_unit_value,
                             String valrep_mv_comp1_new_unit_amt,
                             String valrep_mv_comp2_new_unit_amt,
                             String valrep_mv_comp3_new_unit_amt,

                             String valrep_mv_comp_add_index_price_desc,
                             String valrep_mv_comp_add_index_price,
                             String valrep_mv_comp_temp_ave_index_price,
                             String valrep_mv_rep_other2_desc,
                             String valrep_mv_rep_other2,
                             String valrep_mv_rep_other3_desc,
                             String valrep_mv_rep_other3
    ) {
        this.id = id;
        this.record_id = record_id;
        this.report_interior_dashboard = report_interior_dashboard;
        this.report_interior_sidings = report_interior_sidings;
        this.report_interior_seats = report_interior_seats;
        this.report_interior_windows = report_interior_windows;
        this.report_interior_ceiling = report_interior_ceiling;
        this.report_interior_instrument = report_interior_instrument;
        this.report_bodytype_grille = report_bodytype_grille;
        this.report_bodytype_headlight = report_bodytype_headlight;
        this.report_bodytype_fbumper = report_bodytype_fbumper;
        this.report_bodytype_hood = report_bodytype_hood;
        this.report_bodytype_rf_fender = report_bodytype_rf_fender;
        this.report_bodytype_rf_door = report_bodytype_rf_door;
        this.report_bodytype_rr_door = report_bodytype_rr_door;
        this.report_bodytype_rr_fender = report_bodytype_rr_fender;
        this.report_bodytype_backdoor = report_bodytype_backdoor;
        this.report_bodytype_taillight = report_bodytype_taillight;
        this.report_bodytype_r_bumper = report_bodytype_r_bumper;
        this.report_bodytype_lr_fender = report_bodytype_lr_fender;
        this.report_bodytype_lr_door = report_bodytype_lr_door;
        this.report_bodytype_lf_door = report_bodytype_lf_door;
        this.report_bodytype_lf_fender = report_bodytype_lf_fender;
        this.report_bodytype_top = report_bodytype_top;
        this.report_bodytype_paint = report_bodytype_paint;
        this.report_bodytype_flooring = report_bodytype_flooring;
        this.valrep_mv_bodytype_trunk = valrep_mv_bodytype_trunk;
        this.report_misc_stereo = report_misc_stereo;
        this.report_misc_tools = report_misc_tools;
        this.report_misc_speakers = report_misc_speakers;
        this.report_misc_airbag = report_misc_airbag;
        this.report_misc_tires = report_misc_tires;
        this.report_misc_mag_wheels = report_misc_mag_wheels;
        this.report_misc_carpet = report_misc_carpet;
        this.report_misc_others = report_misc_others;
        this.report_enginearea_fuel = report_enginearea_fuel;
        this.report_enginearea_transmission1 = report_enginearea_transmission1;
        this.report_enginearea_transmission2 = report_enginearea_transmission2;
        this.report_enginearea_chassis = report_enginearea_chassis;
        this.report_enginearea_battery = report_enginearea_battery;
        this.report_enginearea_electrical = report_enginearea_electrical;
        this.report_enginearea_engine = report_enginearea_engine;
        this.report_verification = report_verification;
        this.report_verification_district_office = report_verification_district_office;
        this.report_verification_encumbrance = report_verification_encumbrance;
        this.report_verification_registered_owner = report_verification_registered_owner;
        this.report_place_inspected = report_place_inspected;
        this.report_market_valuation_fair_value = report_market_valuation_fair_value;
        this.report_market_valuation_min = report_market_valuation_min;
        this.report_market_valuation_max = report_market_valuation_max;
        this.report_market_valuation_remarks = report_market_valuation_remarks;
        this.valrep_mv_market_valuation_previous_remarks = valrep_mv_market_valuation_previous_remarks;
        this.report_date_inspected_month = report_date_inspected_month;
        this.report_date_inspected_day = report_date_inspected_day;
        this.report_date_inspected_year = report_date_inspected_year;
        this.report_time_inspected = report_time_inspected;
        //ADDED By IAN
        this.report_comp1_source = report_comp1_source;
        this.report_comp1_tel_no = report_comp1_tel_no;
        this.report_comp1_unit_model = report_comp1_unit_model;
        this.report_comp1_mileage = report_comp1_mileage;
        this.report_comp1_price_min = report_comp1_price_min;
        this.report_comp1_price_max = report_comp1_price_max;
        this.report_comp1_price = report_comp1_price;
        this.report_comp1_less_amt = report_comp1_less_amt;
        this.report_comp1_less_net = report_comp1_less_net;
        this.report_comp1_add_amt = report_comp1_add_amt;
        this.report_comp1_add_net = report_comp1_add_net;
        this.report_comp1_time_adj_value = report_comp1_time_adj_value;
        this.report_comp1_time_adj_amt = report_comp1_time_adj_amt;
        this.report_comp1_adj_value = report_comp1_adj_value;
        this.report_comp1_adj_amt = report_comp1_adj_amt;
        this.report_comp1_index_price = report_comp1_index_price;

        this.report_comp2_source = report_comp2_source;
        this.report_comp2_tel_no = report_comp2_tel_no;
        this.report_comp2_unit_model = report_comp2_unit_model;
        this.report_comp2_mileage = report_comp2_mileage;
        this.report_comp2_price_min = report_comp2_price_min;
        this.report_comp2_price_max = report_comp2_price_max;
        this.report_comp2_price = report_comp2_price;
        this.report_comp2_less_amt = report_comp2_less_amt;
        this.report_comp2_less_net = report_comp2_less_net;
        this.report_comp2_add_amt = report_comp2_add_amt;
        this.report_comp2_add_net = report_comp2_add_net;
        this.report_comp2_time_adj_value = report_comp2_time_adj_value;
        this.report_comp2_time_adj_amt = report_comp2_time_adj_amt;
        this.report_comp2_adj_value = report_comp2_adj_value;
        this.report_comp2_adj_amt = report_comp2_adj_amt;
        this.report_comp2_index_price = report_comp2_index_price;

        this.report_comp3_source = report_comp3_source;
        this.report_comp3_tel_no = report_comp3_tel_no;
        this.report_comp3_unit_model = report_comp3_unit_model;
        this.report_comp3_mileage = report_comp3_mileage;
        this.report_comp3_price_min = report_comp3_price_min;
        this.report_comp3_price_max = report_comp3_price_max;
        this.report_comp3_price = report_comp3_price;
        this.report_comp3_less_amt = report_comp3_less_amt;
        this.report_comp3_less_net = report_comp3_less_net;
        this.report_comp3_add_amt = report_comp3_add_amt;
        this.report_comp3_add_net = report_comp3_add_net;
        this.report_comp3_time_adj_value = report_comp3_time_adj_value;
        this.report_comp3_time_adj_amt = report_comp3_time_adj_amt;
        this.report_comp3_adj_value = report_comp3_adj_value;
        this.report_comp3_adj_amt = report_comp3_adj_amt;
        this.report_comp3_index_price = report_comp3_index_price;
        this.report_comp_ave_index_price = report_comp_ave_index_price;
        this.report_rec_lux_car = report_rec_lux_car;
        this.report_rec_mile_factor = report_rec_mile_factor;
        this.report_rec_taxi_use = report_rec_taxi_use;
        this.report_rec_for_hire = report_rec_for_hire;
        this.report_rec_condition = report_rec_condition;
        this.report_rec_repo = report_rec_repo;
        this.report_rec_other_val = report_rec_other_val;
        this.report_rec_other_desc = report_rec_other_desc;
        this.report_rec_other2_val = report_rec_other2_val;
        this.report_rec_other2_desc = report_rec_other2_desc;
        this.report_rec_other3_val = report_rec_other3_val;
        this.report_rec_other3_desc = report_rec_other3_desc;
        this.report_rec_market_resistance_rec_total = report_rec_market_resistance_rec_total;
        this.report_rec_market_resistance_total = report_rec_market_resistance_total;
        this.report_rec_market_resistance_net_total = report_rec_market_resistance_net_total;
        this.report_rep_stereo = report_rep_stereo;
        this.report_rep_speakers = report_rep_speakers;
        this.report_rep_tires_pcs = report_rep_tires_pcs;
        this.report_rep_tires = report_rep_tires;
        this.report_rep_side_mirror_pcs = report_rep_side_mirror_pcs;
        this.report_rep_side_mirror = report_rep_side_mirror;
        this.report_rep_light = report_rep_light;
        this.report_rep_tools = report_rep_tools;
        this.report_rep_battery = report_rep_battery;
        this.report_rep_plates = report_rep_plates;
        this.report_rep_bumpers = report_rep_bumpers;
        this.report_rep_windows = report_rep_windows;
        this.report_rep_body = report_rep_body;
        this.report_rep_engine_wash = report_rep_engine_wash;
        this.report_rep_other_desc = report_rep_other_desc;
        this.report_rep_other = report_rep_other;
        this.report_rep_total = report_rep_total;
        this.report_appraised_value = report_appraised_value;
//ADDED BY IAN JAN 13 2016
        this.report_verification_file_no = report_verification_file_no;
        this.report_verification_first_reg_date_month = report_verification_first_reg_date_month;
        this.report_verification_first_reg_date_day = report_verification_first_reg_date_day;
        this.report_verification_first_reg_date_year = report_verification_first_reg_date_year;
        this.report_comp1_mileage_value = report_comp1_mileage_value;
        this.report_comp2_mileage_value = report_comp2_mileage_value;
        this.report_comp3_mileage_value = report_comp3_mileage_value;
        this.report_comp1_mileage_amt = report_comp1_mileage_amt;
        this.report_comp2_mileage_amt = report_comp2_mileage_amt;
        this.report_comp3_mileage_amt = report_comp3_mileage_amt;
        this.report_comp1_adj_desc = report_comp1_adj_desc;
        this.report_comp2_adj_desc = report_comp2_adj_desc;
        this.report_comp3_adj_desc = report_comp3_adj_desc;

        this.valrep_mv_comp1_new_unit_value = valrep_mv_comp1_new_unit_value;
        this.valrep_mv_comp2_new_unit_value = valrep_mv_comp2_new_unit_value;
        this.valrep_mv_comp3_new_unit_value = valrep_mv_comp3_new_unit_value;
        this.valrep_mv_comp1_new_unit_amt = valrep_mv_comp1_new_unit_amt;
        this.valrep_mv_comp2_new_unit_amt = valrep_mv_comp2_new_unit_amt;
        this.valrep_mv_comp3_new_unit_amt = valrep_mv_comp3_new_unit_amt;
        this.valrep_mv_comp_add_index_price_desc = valrep_mv_comp_add_index_price_desc;
        this.valrep_mv_comp_add_index_price = valrep_mv_comp_add_index_price;
        this.valrep_mv_comp_temp_ave_index_price = valrep_mv_comp_temp_ave_index_price;
        this.valrep_mv_rep_other2_desc = valrep_mv_rep_other2_desc;
        this.valrep_mv_rep_other2 = valrep_mv_rep_other2;
        this.valrep_mv_rep_other3_desc = valrep_mv_rep_other3_desc;
        this.valrep_mv_rep_other3 = valrep_mv_rep_other3;

    }

    // constructor
    public Motor_Vehicle_API(String record_id,
                             String report_interior_dashboard, String report_interior_sidings,
                             String report_interior_seats, String report_interior_windows,
                             String report_interior_ceiling, String report_interior_instrument,
                             String report_bodytype_grille, String report_bodytype_headlight,
                             String report_bodytype_fbumper, String report_bodytype_hood,
                             String report_bodytype_rf_fender, String report_bodytype_rf_door,
                             String report_bodytype_rr_door, String report_bodytype_rr_fender,
                             String report_bodytype_backdoor, String report_bodytype_taillight,
                             String report_bodytype_r_bumper, String report_bodytype_lr_fender,
                             String report_bodytype_lr_door, String report_bodytype_lf_door,
                             String report_bodytype_lf_fender, String report_bodytype_top,
                             String report_bodytype_paint, String report_bodytype_flooring, String valrep_mv_bodytype_trunk,
                             String report_misc_stereo, String report_misc_tools,
                             String report_misc_speakers, String report_misc_airbag,
                             String report_misc_tires, String report_misc_mag_wheels,
                             String report_misc_carpet, String report_misc_others,
                             String report_enginearea_fuel,
                             String report_enginearea_transmission1,
                             String report_enginearea_transmission2,
                             String report_enginearea_chassis, String report_enginearea_battery,
                             String report_enginearea_electrical,
                             String report_enginearea_engine, String report_verification,
                             String report_verification_district_office,
                             String report_verification_encumbrance,
                             String report_verification_registered_owner,
                             String report_place_inspected,
                             String report_market_valuation_fair_value,
                             String report_market_valuation_min,
                             String report_market_valuation_max,
                             String report_market_valuation_remarks,
                             String valrep_mv_market_valuation_previous_remarks,
                             String report_date_inspected_month,
                             String report_date_inspected_day,
                             String report_date_inspected_year,
                             String report_time_inspected,
//ADDED By IAN
                             String report_comp1_source,
                             String report_comp1_tel_no,
                             String report_comp1_unit_model,
                             String report_comp1_mileage,
                             String report_comp1_price_min,
                             String report_comp1_price_max,
                             String report_comp1_price,
                             String report_comp1_less_amt,
                             String report_comp1_less_net,
                             String report_comp1_add_amt,
                             String report_comp1_add_net,
                             String report_comp1_time_adj_value,
                             String report_comp1_time_adj_amt,
                             String report_comp1_adj_value,
                             String report_comp1_adj_amt,
                             String report_comp1_index_price,
                             String report_comp2_source,
                             String report_comp2_tel_no,
                             String report_comp2_unit_model,
                             String report_comp2_mileage,
                             String report_comp2_price_min,
                             String report_comp2_price_max,
                             String report_comp2_price,
                             String report_comp2_less_amt,
                             String report_comp2_less_net,
                             String report_comp2_add_amt,
                             String report_comp2_add_net,
                             String report_comp2_time_adj_value,
                             String report_comp2_time_adj_amt,
                             String report_comp2_adj_value,
                             String report_comp2_adj_amt,
                             String report_comp2_index_price,
                             String report_comp3_source,
                             String report_comp3_tel_no,
                             String report_comp3_unit_model,
                             String report_comp3_mileage,
                             String report_comp3_price_min,
                             String report_comp3_price_max,
                             String report_comp3_price,
                             String report_comp3_less_amt,
                             String report_comp3_less_net,
                             String report_comp3_add_amt,
                             String report_comp3_add_net,
                             String report_comp3_time_adj_value,
                             String report_comp3_time_adj_amt,
                             String report_comp3_adj_value,
                             String report_comp3_adj_amt,
                             String report_comp3_index_price,
                             String report_comp_ave_index_price,
                             String report_rec_lux_car,
                             String report_rec_mile_factor,
                             String report_rec_taxi_use,
                             String report_rec_for_hire,
                             String report_rec_condition,
                             String report_rec_repo,
                             String report_rec_other_val,
                             String report_rec_other_desc,
                             String report_rec_other2_val,
                             String report_rec_other2_desc,
                             String report_rec_other3_val,
                             String report_rec_other3_desc,
                             String report_rec_market_resistance_rec_total,
                             String report_rec_market_resistance_total,
                             String report_rec_market_resistance_net_total,
                             String report_rep_stereo,
                             String report_rep_speakers,
                             String report_rep_tires_pcs,
                             String report_rep_tires,
                             String report_rep_side_mirror_pcs,
                             String report_rep_side_mirror,
                             String report_rep_light,
                             String report_rep_tools,
                             String report_rep_battery,
                             String report_rep_plates,
                             String report_rep_bumpers,
                             String report_rep_windows,
                             String report_rep_body,
                             String report_rep_engine_wash,
                             String report_rep_other_desc,
                             String report_rep_other,
                             String report_rep_total,
                             String report_appraised_value,
                             //ADDED BY IAN JAN 13 2016
                             String report_verification_file_no,
                             String report_verification_first_reg_date_month,
                             String report_verification_first_reg_date_day,
                             String report_verification_first_reg_date_year,
                             String report_comp1_mileage_value,
                             String report_comp2_mileage_value,
                             String report_comp3_mileage_value,
                             String report_comp1_mileage_amt,
                             String report_comp2_mileage_amt,
                             String report_comp3_mileage_amt,
                             String report_comp1_adj_desc,
                             String report_comp2_adj_desc,
                             String report_comp3_adj_desc,

                             String valrep_mv_comp1_new_unit_value,
                             String valrep_mv_comp2_new_unit_value,
                             String valrep_mv_comp3_new_unit_value,
                             String valrep_mv_comp1_new_unit_amt,
                             String valrep_mv_comp2_new_unit_amt,
                             String valrep_mv_comp3_new_unit_amt,

                             String valrep_mv_comp_add_index_price_desc,
                             String valrep_mv_comp_add_index_price,
                             String valrep_mv_comp_temp_ave_index_price,
                             String valrep_mv_rep_other2_desc,
                             String valrep_mv_rep_other2,
                             String valrep_mv_rep_other3_desc,
                             String valrep_mv_rep_other3
    ) {
        this.record_id = record_id;
        this.report_interior_dashboard = report_interior_dashboard;
        this.report_interior_sidings = report_interior_sidings;
        this.report_interior_seats = report_interior_seats;
        this.report_interior_windows = report_interior_windows;
        this.report_interior_ceiling = report_interior_ceiling;
        this.report_interior_instrument = report_interior_instrument;
        this.report_bodytype_grille = report_bodytype_grille;
        this.report_bodytype_headlight = report_bodytype_headlight;
        this.report_bodytype_fbumper = report_bodytype_fbumper;
        this.report_bodytype_hood = report_bodytype_hood;
        this.report_bodytype_rf_fender = report_bodytype_rf_fender;
        this.report_bodytype_rf_door = report_bodytype_rf_door;
        this.report_bodytype_rr_door = report_bodytype_rr_door;
        this.report_bodytype_rr_fender = report_bodytype_rr_fender;
        this.report_bodytype_backdoor = report_bodytype_backdoor;
        this.report_bodytype_taillight = report_bodytype_taillight;
        this.report_bodytype_r_bumper = report_bodytype_r_bumper;
        this.report_bodytype_lr_fender = report_bodytype_lr_fender;
        this.report_bodytype_lr_door = report_bodytype_lr_door;
        this.report_bodytype_lf_door = report_bodytype_lf_door;
        this.report_bodytype_lf_fender = report_bodytype_lf_fender;
        this.report_bodytype_top = report_bodytype_top;
        this.report_bodytype_paint = report_bodytype_paint;
        this.report_bodytype_flooring = report_bodytype_flooring;
        this.valrep_mv_bodytype_trunk = valrep_mv_bodytype_trunk;
        this.report_misc_stereo = report_misc_stereo;
        this.report_misc_tools = report_misc_tools;
        this.report_misc_speakers = report_misc_speakers;
        this.report_misc_airbag = report_misc_airbag;
        this.report_misc_tires = report_misc_tires;
        this.report_misc_mag_wheels = report_misc_mag_wheels;
        this.report_misc_carpet = report_misc_carpet;
        this.report_misc_others = report_misc_others;
        this.report_enginearea_fuel = report_enginearea_fuel;
        this.report_enginearea_transmission1 = report_enginearea_transmission1;
        this.report_enginearea_transmission2 = report_enginearea_transmission2;
        this.report_enginearea_chassis = report_enginearea_chassis;
        this.report_enginearea_battery = report_enginearea_battery;
        this.report_enginearea_electrical = report_enginearea_electrical;
        this.report_enginearea_engine = report_enginearea_engine;
        this.report_verification = report_verification;
        this.report_verification_district_office = report_verification_district_office;
        this.report_verification_encumbrance = report_verification_encumbrance;
        this.report_verification_registered_owner = report_verification_registered_owner;
        this.report_place_inspected = report_place_inspected;
        this.report_market_valuation_fair_value = report_market_valuation_fair_value;
        this.report_market_valuation_min = report_market_valuation_min;
        this.report_market_valuation_max = report_market_valuation_max;
        this.report_market_valuation_remarks = report_market_valuation_remarks;
        this.valrep_mv_market_valuation_previous_remarks = valrep_mv_market_valuation_previous_remarks;
        this.report_date_inspected_month = report_date_inspected_month;
        this.report_date_inspected_day = report_date_inspected_day;
        this.report_date_inspected_year = report_date_inspected_year;
        this.report_time_inspected = report_time_inspected;
        //ADDED By IAN
        this.report_comp1_source = report_comp1_source;
        this.report_comp1_tel_no = report_comp1_tel_no;
        this.report_comp1_unit_model = report_comp1_unit_model;
        this.report_comp1_mileage = report_comp1_mileage;
        this.report_comp1_price_min = report_comp1_price_min;
        this.report_comp1_price_max = report_comp1_price_max;
        this.report_comp1_price = report_comp1_price;
        this.report_comp1_less_amt = report_comp1_less_amt;
        this.report_comp1_less_net = report_comp1_less_net;
        this.report_comp1_add_amt = report_comp1_add_amt;
        this.report_comp1_add_net = report_comp1_add_net;
        this.report_comp1_time_adj_value = report_comp1_time_adj_value;
        this.report_comp1_time_adj_amt = report_comp1_time_adj_amt;
        this.report_comp1_adj_value = report_comp1_adj_value;
        this.report_comp1_adj_amt = report_comp1_adj_amt;
        this.report_comp1_index_price = report_comp1_index_price;

        this.report_comp2_source = report_comp2_source;
        this.report_comp2_tel_no = report_comp2_tel_no;
        this.report_comp2_unit_model = report_comp2_unit_model;
        this.report_comp2_mileage = report_comp2_mileage;
        this.report_comp2_price_min = report_comp2_price_min;
        this.report_comp2_price_max = report_comp2_price_max;
        this.report_comp2_price = report_comp2_price;
        this.report_comp2_less_amt = report_comp2_less_amt;
        this.report_comp2_less_net = report_comp2_less_net;
        this.report_comp2_add_amt = report_comp2_add_amt;
        this.report_comp2_add_net = report_comp2_add_net;
        this.report_comp2_time_adj_value = report_comp2_time_adj_value;
        this.report_comp2_time_adj_amt = report_comp2_time_adj_amt;
        this.report_comp2_adj_value = report_comp2_adj_value;
        this.report_comp2_adj_amt = report_comp2_adj_amt;
        this.report_comp2_index_price = report_comp2_index_price;

        this.report_comp3_source = report_comp3_source;
        this.report_comp3_tel_no = report_comp3_tel_no;
        this.report_comp3_unit_model = report_comp3_unit_model;
        this.report_comp3_mileage = report_comp3_mileage;
        this.report_comp3_price_min = report_comp3_price_min;
        this.report_comp3_price_max = report_comp3_price_max;
        this.report_comp3_price = report_comp3_price;
        this.report_comp3_less_amt = report_comp3_less_amt;
        this.report_comp3_less_net = report_comp3_less_net;
        this.report_comp3_add_amt = report_comp3_add_amt;
        this.report_comp3_add_net = report_comp3_add_net;
        this.report_comp3_time_adj_value = report_comp3_time_adj_value;
        this.report_comp3_time_adj_amt = report_comp3_time_adj_amt;
        this.report_comp3_adj_value = report_comp3_adj_value;
        this.report_comp3_adj_amt = report_comp3_adj_amt;
        this.report_comp3_index_price = report_comp3_index_price;
        this.report_comp_ave_index_price = report_comp_ave_index_price;
        this.report_rec_lux_car = report_rec_lux_car;
        this.report_rec_mile_factor = report_rec_mile_factor;
        this.report_rec_taxi_use = report_rec_taxi_use;
        this.report_rec_for_hire = report_rec_for_hire;
        this.report_rec_condition = report_rec_condition;
        this.report_rec_repo = report_rec_repo;
        this.report_rec_other_val = report_rec_other_val;
        this.report_rec_other_desc = report_rec_other_desc;
        this.report_rec_other2_val = report_rec_other2_val;
        this.report_rec_other2_desc = report_rec_other2_desc;
        this.report_rec_other3_val = report_rec_other3_val;
        this.report_rec_other3_desc = report_rec_other3_desc;
        this.report_rec_market_resistance_rec_total = report_rec_market_resistance_rec_total;
        this.report_rec_market_resistance_total = report_rec_market_resistance_total;
        this.report_rec_market_resistance_net_total = report_rec_market_resistance_net_total;
        this.report_rep_stereo = report_rep_stereo;
        this.report_rep_speakers = report_rep_speakers;
        this.report_rep_tires_pcs = report_rep_tires_pcs;
        this.report_rep_tires = report_rep_tires;
        this.report_rep_side_mirror_pcs = report_rep_side_mirror_pcs;
        this.report_rep_side_mirror = report_rep_side_mirror;
        this.report_rep_light = report_rep_light;
        this.report_rep_tools = report_rep_tools;
        this.report_rep_battery = report_rep_battery;
        this.report_rep_plates = report_rep_plates;
        this.report_rep_bumpers = report_rep_bumpers;
        this.report_rep_windows = report_rep_windows;
        this.report_rep_body = report_rep_body;
        this.report_rep_engine_wash = report_rep_engine_wash;
        this.report_rep_other_desc = report_rep_other_desc;
        this.report_rep_other = report_rep_other;
        this.report_rep_total = report_rep_total;
        this.report_appraised_value = report_appraised_value;
        //ADDED BY IAN JAN 13 2016
        this.report_verification_file_no = report_verification_file_no;
        this.report_verification_first_reg_date_month = report_verification_first_reg_date_month;
        this.report_verification_first_reg_date_day = report_verification_first_reg_date_day;
        this.report_verification_first_reg_date_year = report_verification_first_reg_date_year;
        this.report_comp1_mileage_value = report_comp1_mileage_value;
        this.report_comp2_mileage_value = report_comp2_mileage_value;
        this.report_comp3_mileage_value = report_comp3_mileage_value;
        this.report_comp1_mileage_amt = report_comp1_mileage_amt;
        this.report_comp2_mileage_amt = report_comp2_mileage_amt;
        this.report_comp3_mileage_amt = report_comp3_mileage_amt;
        this.report_comp1_adj_desc = report_comp1_adj_desc;
        this.report_comp2_adj_desc = report_comp2_adj_desc;
        this.report_comp3_adj_desc = report_comp3_adj_desc;

        this.valrep_mv_comp1_new_unit_value = valrep_mv_comp1_new_unit_value;
        this.valrep_mv_comp2_new_unit_value = valrep_mv_comp2_new_unit_value;
        this.valrep_mv_comp3_new_unit_value = valrep_mv_comp3_new_unit_value;
        this.valrep_mv_comp1_new_unit_amt = valrep_mv_comp1_new_unit_amt;
        this.valrep_mv_comp2_new_unit_amt = valrep_mv_comp2_new_unit_amt;
        this.valrep_mv_comp3_new_unit_amt = valrep_mv_comp3_new_unit_amt;
        this.valrep_mv_comp_add_index_price_desc = valrep_mv_comp_add_index_price_desc;
        this.valrep_mv_comp_add_index_price = valrep_mv_comp_add_index_price;
        this.valrep_mv_comp_temp_ave_index_price = valrep_mv_comp_temp_ave_index_price;
        this.valrep_mv_rep_other2_desc = valrep_mv_rep_other2_desc;
        this.valrep_mv_rep_other2 = valrep_mv_rep_other2;
        this.valrep_mv_rep_other3_desc = valrep_mv_rep_other3_desc;
        this.valrep_mv_rep_other3 = valrep_mv_rep_other3;
    }

    // constructor
    public Motor_Vehicle_API(String report_interior_dashboard, String report_interior_sidings,
                             String report_interior_seats, String report_interior_windows,
                             String report_interior_ceiling, String report_interior_instrument,
                             String report_bodytype_grille, String report_bodytype_headlight,
                             String report_bodytype_fbumper, String report_bodytype_hood,
                             String report_bodytype_rf_fender, String report_bodytype_rf_door,
                             String report_bodytype_rr_door, String report_bodytype_rr_fender,
                             String report_bodytype_backdoor, String report_bodytype_taillight,
                             String report_bodytype_r_bumper, String report_bodytype_lr_fender,
                             String report_bodytype_lr_door, String report_bodytype_lf_door,
                             String report_bodytype_lf_fender, String report_bodytype_top,
                             String report_bodytype_paint, String report_bodytype_flooring, String valrep_mv_bodytype_trunk,
                             String report_misc_stereo, String report_misc_tools,
                             String report_misc_speakers, String report_misc_airbag,
                             String report_misc_tires, String report_misc_mag_wheels,
                             String report_misc_carpet, String report_misc_others,
                             String report_enginearea_fuel,
                             String report_enginearea_transmission1,
                             String report_enginearea_transmission2,
                             String report_enginearea_chassis, String report_enginearea_battery,
                             String report_enginearea_electrical,
                             String report_enginearea_engine, String report_verification,
                             String report_verification_district_office,
                             String report_verification_encumbrance,
                             String report_verification_registered_owner,
                             String report_place_inspected,
                             String report_market_valuation_fair_value,
                             String report_market_valuation_min,
                             String report_market_valuation_max,
                             String report_market_valuation_remarks,
                             String valrep_mv_market_valuation_previous_remarks,
                             String report_date_inspected_month,
                             String report_date_inspected_day,
                             String report_date_inspected_year,
                             String report_time_inspected,
//ADDED By IAN
                             String report_comp1_source,
                             String report_comp1_tel_no,
                             String report_comp1_unit_model,
                             String report_comp1_mileage,
                             String report_comp1_price_min,
                             String report_comp1_price_max,
                             String report_comp1_price,
                             String report_comp1_less_amt,
                             String report_comp1_less_net,
                             String report_comp1_add_amt,
                             String report_comp1_add_net,
                             String report_comp1_time_adj_value,
                             String report_comp1_time_adj_amt,
                             String report_comp1_adj_value,
                             String report_comp1_adj_amt,
                             String report_comp1_index_price,
                             String report_comp2_source,
                             String report_comp2_tel_no,
                             String report_comp2_unit_model,
                             String report_comp2_mileage,
                             String report_comp2_price_min,
                             String report_comp2_price_max,
                             String report_comp2_price,
                             String report_comp2_less_amt,
                             String report_comp2_less_net,
                             String report_comp2_add_amt,
                             String report_comp2_add_net,
                             String report_comp2_time_adj_value,
                             String report_comp2_time_adj_amt,
                             String report_comp2_adj_value,
                             String report_comp2_adj_amt,
                             String report_comp2_index_price,
                             String report_comp3_source,
                             String report_comp3_tel_no,
                             String report_comp3_unit_model,
                             String report_comp3_mileage,
                             String report_comp3_price_min,
                             String report_comp3_price_max,
                             String report_comp3_price,
                             String report_comp3_less_amt,
                             String report_comp3_less_net,
                             String report_comp3_add_amt,
                             String report_comp3_add_net,
                             String report_comp3_time_adj_value,
                             String report_comp3_time_adj_amt,
                             String report_comp3_adj_value,
                             String report_comp3_adj_amt,
                             String report_comp3_index_price,
                             String report_comp_ave_index_price,
                             String report_rec_lux_car,
                             String report_rec_mile_factor,
                             String report_rec_taxi_use,
                             String report_rec_for_hire,
                             String report_rec_condition,
                             String report_rec_repo,
                             String report_rec_other_val,
                             String report_rec_other_desc,
                             String report_rec_other2_val,
                             String report_rec_other2_desc,
                             String report_rec_other3_val,
                             String report_rec_other3_desc,
                             String report_rec_market_resistance_rec_total,
                             String report_rec_market_resistance_total,
                             String report_rec_market_resistance_net_total,
                             String report_rep_stereo,
                             String report_rep_speakers,
                             String report_rep_tires_pcs,
                             String report_rep_tires,
                             String report_rep_side_mirror_pcs,
                             String report_rep_side_mirror,
                             String report_rep_light,
                             String report_rep_tools,
                             String report_rep_battery,
                             String report_rep_plates,
                             String report_rep_bumpers,
                             String report_rep_windows,
                             String report_rep_body,
                             String report_rep_engine_wash,
                             String report_rep_other_desc,
                             String report_rep_other,
                             String report_rep_total,
                             String report_appraised_value,
                             //ADDED BY IAN JAN 13 2016
                             String report_verification_file_no,
                             String report_verification_first_reg_date_month,
                             String report_verification_first_reg_date_day,
                             String report_verification_first_reg_date_year,
                             String report_comp1_mileage_value,
                             String report_comp2_mileage_value,
                             String report_comp3_mileage_value,
                             String report_comp1_mileage_amt,
                             String report_comp2_mileage_amt,
                             String report_comp3_mileage_amt,
                             String report_comp1_adj_desc,
                             String report_comp2_adj_desc,
                             String report_comp3_adj_desc,

                             String valrep_mv_comp1_new_unit_value,
                             String valrep_mv_comp2_new_unit_value,
                             String valrep_mv_comp3_new_unit_value,
                             String valrep_mv_comp1_new_unit_amt,
                             String valrep_mv_comp2_new_unit_amt,
                             String valrep_mv_comp3_new_unit_amt,

                             String valrep_mv_comp_add_index_price_desc,
                             String valrep_mv_comp_add_index_price,
                             String valrep_mv_comp_temp_ave_index_price,
                             String valrep_mv_rep_other2_desc,
                             String valrep_mv_rep_other2,
                             String valrep_mv_rep_other3_desc,
                             String valrep_mv_rep_other3
    ) {
        this.report_interior_dashboard = report_interior_dashboard;
        this.report_interior_sidings = report_interior_sidings;
        this.report_interior_seats = report_interior_seats;
        this.report_interior_windows = report_interior_windows;
        this.report_interior_ceiling = report_interior_ceiling;
        this.report_interior_instrument = report_interior_instrument;
        this.report_bodytype_grille = report_bodytype_grille;
        this.report_bodytype_headlight = report_bodytype_headlight;
        this.report_bodytype_fbumper = report_bodytype_fbumper;
        this.report_bodytype_hood = report_bodytype_hood;
        this.report_bodytype_rf_fender = report_bodytype_rf_fender;
        this.report_bodytype_rf_door = report_bodytype_rf_door;
        this.report_bodytype_rr_door = report_bodytype_rr_door;
        this.report_bodytype_rr_fender = report_bodytype_rr_fender;
        this.report_bodytype_backdoor = report_bodytype_backdoor;
        this.report_bodytype_taillight = report_bodytype_taillight;
        this.report_bodytype_r_bumper = report_bodytype_r_bumper;
        this.report_bodytype_lr_fender = report_bodytype_lr_fender;
        this.report_bodytype_lr_door = report_bodytype_lr_door;
        this.report_bodytype_lf_door = report_bodytype_lf_door;
        this.report_bodytype_lf_fender = report_bodytype_lf_fender;
        this.report_bodytype_top = report_bodytype_top;
        this.report_bodytype_paint = report_bodytype_paint;
        this.report_bodytype_flooring = report_bodytype_flooring;
        this.valrep_mv_bodytype_trunk = valrep_mv_bodytype_trunk;
        this.report_misc_stereo = report_misc_stereo;
        this.report_misc_tools = report_misc_tools;
        this.report_misc_speakers = report_misc_speakers;
        this.report_misc_airbag = report_misc_airbag;
        this.report_misc_tires = report_misc_tires;
        this.report_misc_mag_wheels = report_misc_mag_wheels;
        this.report_misc_carpet = report_misc_carpet;
        this.report_misc_others = report_misc_others;
        this.report_enginearea_fuel = report_enginearea_fuel;
        this.report_enginearea_transmission1 = report_enginearea_transmission1;
        this.report_enginearea_transmission2 = report_enginearea_transmission2;
        this.report_enginearea_chassis = report_enginearea_chassis;
        this.report_enginearea_battery = report_enginearea_battery;
        this.report_enginearea_electrical = report_enginearea_electrical;
        this.report_enginearea_engine = report_enginearea_engine;
        this.report_verification = report_verification;
        this.report_verification_district_office = report_verification_district_office;
        this.report_verification_encumbrance = report_verification_encumbrance;
        this.report_verification_registered_owner = report_verification_registered_owner;
        this.report_place_inspected = report_place_inspected;
        this.report_market_valuation_fair_value = report_market_valuation_fair_value;
        this.report_market_valuation_min = report_market_valuation_min;
        this.report_market_valuation_max = report_market_valuation_max;
        this.report_market_valuation_remarks = report_market_valuation_remarks;
        this.valrep_mv_market_valuation_previous_remarks = valrep_mv_market_valuation_previous_remarks;
        this.report_date_inspected_month = report_date_inspected_month;
        this.report_date_inspected_day = report_date_inspected_day;
        this.report_date_inspected_year = report_date_inspected_year;
        this.report_time_inspected = report_time_inspected;
        //ADDED By IAN
        this.report_comp1_source = report_comp1_source;
        this.report_comp1_tel_no = report_comp1_tel_no;
        this.report_comp1_unit_model = report_comp1_unit_model;
        this.report_comp1_mileage = report_comp1_mileage;
        this.report_comp1_price_min = report_comp1_price_min;
        this.report_comp1_price_max = report_comp1_price_max;
        this.report_comp1_price = report_comp1_price;
        this.report_comp1_less_amt = report_comp1_less_amt;
        this.report_comp1_less_net = report_comp1_less_net;
        this.report_comp1_add_amt = report_comp1_add_amt;
        this.report_comp1_add_net = report_comp1_add_net;
        this.report_comp1_time_adj_value = report_comp1_time_adj_value;
        this.report_comp1_time_adj_amt = report_comp1_time_adj_amt;
        this.report_comp1_adj_value = report_comp1_adj_value;
        this.report_comp1_adj_amt = report_comp1_adj_amt;
        this.report_comp1_index_price = report_comp1_index_price;

        this.report_comp2_source = report_comp2_source;
        this.report_comp2_tel_no = report_comp2_tel_no;
        this.report_comp2_unit_model = report_comp2_unit_model;
        this.report_comp2_mileage = report_comp2_mileage;
        this.report_comp2_price_min = report_comp2_price_min;
        this.report_comp2_price_max = report_comp2_price_max;
        this.report_comp2_price = report_comp2_price;
        this.report_comp2_less_amt = report_comp2_less_amt;
        this.report_comp2_less_net = report_comp2_less_net;
        this.report_comp2_add_amt = report_comp2_add_amt;
        this.report_comp2_add_net = report_comp2_add_net;
        this.report_comp2_time_adj_value = report_comp2_time_adj_value;
        this.report_comp2_time_adj_amt = report_comp2_time_adj_amt;
        this.report_comp2_adj_value = report_comp2_adj_value;
        this.report_comp2_adj_amt = report_comp2_adj_amt;
        this.report_comp2_index_price = report_comp2_index_price;

        this.report_comp3_source = report_comp3_source;
        this.report_comp3_tel_no = report_comp3_tel_no;
        this.report_comp3_unit_model = report_comp3_unit_model;
        this.report_comp3_mileage = report_comp3_mileage;
        this.report_comp3_price_min = report_comp3_price_min;
        this.report_comp3_price_max = report_comp3_price_max;
        this.report_comp3_price = report_comp3_price;
        this.report_comp3_less_amt = report_comp3_less_amt;
        this.report_comp3_less_net = report_comp3_less_net;
        this.report_comp3_add_amt = report_comp3_add_amt;
        this.report_comp3_add_net = report_comp3_add_net;
        this.report_comp3_time_adj_value = report_comp3_time_adj_value;
        this.report_comp3_time_adj_amt = report_comp3_time_adj_amt;
        this.report_comp3_adj_value = report_comp3_adj_value;
        this.report_comp3_adj_amt = report_comp3_adj_amt;
        this.report_comp3_index_price = report_comp3_index_price;
        this.report_comp_ave_index_price = report_comp_ave_index_price;
        this.report_rec_lux_car = report_rec_lux_car;
        this.report_rec_mile_factor = report_rec_mile_factor;
        this.report_rec_taxi_use = report_rec_taxi_use;
        this.report_rec_for_hire = report_rec_for_hire;
        this.report_rec_condition = report_rec_condition;
        this.report_rec_repo = report_rec_repo;
        this.report_rec_other_val = report_rec_other_val;
        this.report_rec_other_desc = report_rec_other_desc;
        this.report_rec_other2_val = report_rec_other2_val;
        this.report_rec_other2_desc = report_rec_other2_desc;
        this.report_rec_other3_val = report_rec_other3_val;
        this.report_rec_other3_desc = report_rec_other3_desc;
        this.report_rec_market_resistance_rec_total = report_rec_market_resistance_rec_total;
        this.report_rec_market_resistance_total = report_rec_market_resistance_total;
        this.report_rec_market_resistance_net_total = report_rec_market_resistance_net_total;
        this.report_rep_stereo = report_rep_stereo;
        this.report_rep_speakers = report_rep_speakers;
        this.report_rep_tires_pcs = report_rep_tires_pcs;
        this.report_rep_tires = report_rep_tires;
        this.report_rep_side_mirror_pcs = report_rep_side_mirror_pcs;
        this.report_rep_side_mirror = report_rep_side_mirror;
        this.report_rep_light = report_rep_light;
        this.report_rep_tools = report_rep_tools;
        this.report_rep_battery = report_rep_battery;
        this.report_rep_plates = report_rep_plates;
        this.report_rep_bumpers = report_rep_bumpers;
        this.report_rep_windows = report_rep_windows;
        this.report_rep_body = report_rep_body;
        this.report_rep_engine_wash = report_rep_engine_wash;
        this.report_rep_other_desc = report_rep_other_desc;
        this.report_rep_other = report_rep_other;
        this.report_rep_total = report_rep_total;
        this.report_appraised_value = report_appraised_value;
        //ADDED BY IAN JAN 13 2016
        this.report_verification_file_no = report_verification_file_no;
        this.report_verification_first_reg_date_month = report_verification_first_reg_date_month;
        this.report_verification_first_reg_date_day = report_verification_first_reg_date_day;
        this.report_verification_first_reg_date_year = report_verification_first_reg_date_year;
        this.report_comp1_mileage_value = report_comp1_mileage_value;
        this.report_comp2_mileage_value = report_comp2_mileage_value;
        this.report_comp3_mileage_value = report_comp3_mileage_value;
        this.report_comp1_mileage_amt = report_comp1_mileage_amt;
        this.report_comp2_mileage_amt = report_comp2_mileage_amt;
        this.report_comp3_mileage_amt = report_comp3_mileage_amt;
        this.report_comp1_adj_desc = report_comp1_adj_desc;
        this.report_comp2_adj_desc = report_comp2_adj_desc;
        this.report_comp3_adj_desc = report_comp3_adj_desc;

        this.valrep_mv_comp1_new_unit_value = valrep_mv_comp1_new_unit_value;
        this.valrep_mv_comp2_new_unit_value = valrep_mv_comp2_new_unit_value;
        this.valrep_mv_comp3_new_unit_value = valrep_mv_comp3_new_unit_value;
        this.valrep_mv_comp1_new_unit_amt = valrep_mv_comp1_new_unit_amt;
        this.valrep_mv_comp2_new_unit_amt = valrep_mv_comp2_new_unit_amt;
        this.valrep_mv_comp3_new_unit_amt = valrep_mv_comp3_new_unit_amt;
        this.valrep_mv_comp_add_index_price_desc = valrep_mv_comp_add_index_price_desc;
        this.valrep_mv_comp_add_index_price = valrep_mv_comp_add_index_price;
        this.valrep_mv_comp_temp_ave_index_price = valrep_mv_comp_temp_ave_index_price;
        this.valrep_mv_rep_other2_desc = valrep_mv_rep_other2_desc;
        this.valrep_mv_rep_other2 = valrep_mv_rep_other2;
        this.valrep_mv_rep_other3_desc = valrep_mv_rep_other3_desc;
        this.valrep_mv_rep_other3 = valrep_mv_rep_other3;

    }

    // getting ID
    public int getID() {
        return this.id;
    }

    // setting id
    public void setID(int id) {
        this.id = id;
    }

    // getting record_id
    public String getrecord_id() {
        return this.record_id;
    }

    public void setrecord_id(String record_id) {
        this.record_id = record_id;
    }

    //getters
    public String getreport_interior_dashboard() {
        return this.report_interior_dashboard;
    }

    public String getreport_interior_sidings() {
        return this.report_interior_sidings;
    }

    public String getreport_interior_seats() {
        return this.report_interior_seats;
    }

    public String getreport_interior_windows() {
        return this.report_interior_windows;
    }

    public String getreport_interior_ceiling() {
        return this.report_interior_ceiling;
    }

    public String getreport_interior_instrument() {
        return this.report_interior_instrument;
    }

    public String getreport_bodytype_grille() {
        return this.report_bodytype_grille;
    }

    public String getreport_bodytype_headlight() {
        return this.report_bodytype_headlight;
    }

    public String getreport_bodytype_fbumper() {
        return this.report_bodytype_fbumper;
    }

    public String getreport_bodytype_hood() {
        return this.report_bodytype_hood;
    }

    public String getreport_bodytype_rf_fender() {
        return this.report_bodytype_rf_fender;
    }

    public String getreport_bodytype_rf_door() {
        return this.report_bodytype_rf_door;
    }

    public String getreport_bodytype_rr_door() {
        return this.report_bodytype_rr_door;
    }

    public String getreport_bodytype_rr_fender() {
        return this.report_bodytype_rr_fender;
    }

    public String getreport_bodytype_backdoor() {
        return this.report_bodytype_backdoor;
    }

    public String getreport_bodytype_taillight() {
        return this.report_bodytype_taillight;
    }

    public String getreport_bodytype_r_bumper() {
        return this.report_bodytype_r_bumper;
    }

    public String getreport_bodytype_lr_fender() {
        return this.report_bodytype_lr_fender;
    }

    public String getreport_bodytype_lr_door() {
        return this.report_bodytype_lr_door;
    }

    public String getreport_bodytype_lf_door() {
        return this.report_bodytype_lf_door;
    }

    public String getreport_bodytype_lf_fender() {
        return this.report_bodytype_lf_fender;
    }

    public String getreport_bodytype_top() {
        return this.report_bodytype_top;
    }

    public String getreport_bodytype_paint() {
        return this.report_bodytype_paint;
    }

    public String getvalrep_mv_bodytype_trunk() {
        return this.valrep_mv_bodytype_trunk;
    }

    public String getreport_bodytype_flooring() {
        return this.report_bodytype_flooring;
    }

    public String getreport_misc_stereo() {
        return this.report_misc_stereo;
    }

    public String getreport_misc_tools() {
        return this.report_misc_tools;
    }

    public String getreport_misc_speakers() {
        return this.report_misc_speakers;
    }

    public String getreport_misc_airbag() {
        return this.report_misc_airbag;
    }

    public String getreport_misc_tires() {
        return this.report_misc_tires;
    }

    public String getreport_misc_mag_wheels() {
        return this.report_misc_mag_wheels;
    }

    public String getreport_misc_carpet() {
        return this.report_misc_carpet;
    }

    public String getreport_misc_others() {
        return this.report_misc_others;
    }

    public String getreport_enginearea_fuel() {
        return this.report_enginearea_fuel;
    }

    public String getreport_enginearea_transmission1() {
        return this.report_enginearea_transmission1;
    }

    public String getreport_enginearea_transmission2() {
        return this.report_enginearea_transmission2;
    }

    public String getreport_enginearea_chassis() {
        return this.report_enginearea_chassis;
    }

    public String getreport_enginearea_battery() {
        return this.report_enginearea_battery;
    }

    public String getreport_enginearea_electrical() {
        return this.report_enginearea_electrical;
    }

    public String getreport_enginearea_engine() {
        return this.report_enginearea_engine;
    }

    public String getreport_verification() {
        return this.report_verification;
    }

    public String getreport_verification_district_office() {
        return this.report_verification_district_office;
    }

    public String getreport_verification_encumbrance() {
        return this.report_verification_encumbrance;
    }

    public String getreport_verification_registered_owner() {
        return this.report_verification_registered_owner;
    }

    public String getreport_place_inspected() {
        return this.report_place_inspected;
    }

    public String getreport_market_valuation_fair_value() {
        return this.report_market_valuation_fair_value;
    }

    public String getreport_market_valuation_min() {
        return this.report_market_valuation_min;
    }

    public String getreport_market_valuation_max() {
        return this.report_market_valuation_max;
    }

    public String getreport_market_valuation_remarks() {
        return this.report_market_valuation_remarks;
    }

    public String getvalrep_mv_market_valuation_previous_remarks() {
        return this.valrep_mv_market_valuation_previous_remarks;
    }

    public String getreport_date_inspected_month() {
        return this.report_date_inspected_month;
    }

    public String getreport_date_inspected_day() {
        return this.report_date_inspected_day;
    }

    public String getreport_date_inspected_year() {
        return this.report_date_inspected_year;
    }

    public String getreport_time_inspected() {
        return this.report_time_inspected;
    }


    //ADDED By IAN

    public String getreport_comp1_source() {
        return this.report_comp1_source;
    }

    public String getreport_comp1_tel_no() {
        return this.report_comp1_tel_no;
    }

    public String getreport_comp1_unit_model() {
        return this.report_comp1_unit_model;
    }

    public String getreport_comp1_mileage() {
        return this.report_comp1_mileage;
    }

    public String getreport_comp1_price_min() {
        return this.report_comp1_price_min;
    }

    public String getreport_comp1_price_max() {
        return this.report_comp1_price_max;
    }

    public String getreport_comp1_price() {
        return this.report_comp1_price;
    }

    public String getreport_comp1_less_amt() {
        return this.report_comp1_less_amt;
    }

    public String getreport_comp1_less_net() {
        return this.report_comp1_less_net;
    }

    public String getreport_comp1_add_amt() {
        return this.report_comp1_add_amt;
    }

    public String getreport_comp1_add_net() {
        return this.report_comp1_add_net;
    }

    public String getreport_comp1_time_adj_value() {
        return this.report_comp1_time_adj_value;
    }

    public String getreport_comp1_time_adj_amt() {
        return this.report_comp1_time_adj_amt;
    }

    public String getreport_comp1_adj_value() {
        return this.report_comp1_adj_value;
    }

    public String getreport_comp1_adj_amt() {
        return this.report_comp1_adj_amt;
    }

    public String getreport_comp1_index_price() {
        return this.report_comp1_index_price;
    }


    public String getreport_comp2_source() {
        return this.report_comp2_source;
    }

    public String getreport_comp2_tel_no() {
        return this.report_comp2_tel_no;
    }

    public String getreport_comp2_unit_model() {
        return this.report_comp2_unit_model;
    }

    public String getreport_comp2_mileage() {
        return this.report_comp2_mileage;
    }

    public String getreport_comp2_price_min() {
        return this.report_comp2_price_min;
    }

    public String getreport_comp2_price_max() {
        return this.report_comp2_price_max;
    }

    public String getreport_comp2_price() {
        return this.report_comp2_price;
    }

    public String getreport_comp2_less_amt() {
        return this.report_comp2_less_amt;
    }

    public String getreport_comp2_less_net() {
        return this.report_comp2_less_net;
    }

    public String getreport_comp2_add_amt() {
        return this.report_comp2_add_amt;
    }

    public String getreport_comp2_add_net() {
        return this.report_comp2_add_net;
    }

    public String getreport_comp2_time_adj_value() {
        return this.report_comp2_time_adj_value;
    }

    public String getreport_comp2_time_adj_amt() {
        return this.report_comp2_time_adj_amt;
    }

    public String getreport_comp2_adj_value() {
        return this.report_comp2_adj_value;
    }

    public String getreport_comp2_adj_amt() {
        return this.report_comp2_adj_amt;
    }

    public String getreport_comp2_index_price() {
        return this.report_comp2_index_price;
    }


    public String getreport_comp3_source() {
        return this.report_comp3_source;
    }

    public String getreport_comp3_tel_no() {
        return this.report_comp3_tel_no;
    }

    public String getreport_comp3_unit_model() {
        return this.report_comp3_unit_model;
    }

    public String getreport_comp3_mileage() {
        return this.report_comp3_mileage;
    }

    public String getreport_comp3_price_min() {
        return this.report_comp3_price_min;
    }

    public String getreport_comp3_price_max() {
        return this.report_comp3_price_max;
    }

    public String getreport_comp3_price() {
        return this.report_comp3_price;
    }

    public String getreport_comp3_less_amt() {
        return this.report_comp3_less_amt;
    }

    public String getreport_comp3_less_net() {
        return this.report_comp3_less_net;
    }

    public String getreport_comp3_add_amt() {
        return this.report_comp3_add_amt;
    }

    public String getreport_comp3_add_net() {
        return this.report_comp3_add_net;
    }

    public String getreport_comp3_time_adj_value() {
        return this.report_comp3_time_adj_value;
    }

    public String getreport_comp3_time_adj_amt() {
        return this.report_comp3_time_adj_amt;
    }

    public String getreport_comp3_adj_value() {
        return this.report_comp3_adj_value;
    }

    public String getreport_comp3_adj_amt() {
        return this.report_comp3_adj_amt;
    }

    public String getreport_comp3_index_price() {
        return this.report_comp3_index_price;
    }


    public String getreport_comp_ave_index_price() {
        return this.report_comp_ave_index_price;
    }

    public String getreport_rec_lux_car() {
        return this.report_rec_lux_car;
    }

    public String getreport_rec_mile_factor() {
        return this.report_rec_mile_factor;
    }

    public String getreport_rec_taxi_use() {
        return this.report_rec_taxi_use;
    }

    public String getreport_rec_for_hire() {
        return this.report_rec_for_hire;
    }

    public String getreport_rec_condition() {
        return this.report_rec_condition;
    }

    public String getreport_rec_repo() {
        return this.report_rec_repo;
    }

    public String getreport_rec_other_val() {
        return this.report_rec_other_val;
    }

    public String getreport_rec_other_desc() {
        return this.report_rec_other_desc;
    }

    public String getreport_rec_other2_val() {
        return this.report_rec_other2_val;
    }

    public String getreport_rec_other2_desc() {
        return this.report_rec_other2_desc;
    }

    public String getreport_rec_other3_val() {
        return this.report_rec_other3_val;
    }

    public String getreport_rec_other3_desc() {
        return this.report_rec_other3_desc;
    }

    public String getreport_rec_market_resistance_rec_total() {
        return this.report_rec_market_resistance_rec_total;
    }

    public String getreport_rec_market_resistance_total() {
        return this.report_rec_market_resistance_total;
    }

    public String getreport_rec_market_resistance_net_total() {
        return this.report_rec_market_resistance_net_total;
    }

    public String getreport_rep_stereo() {
        return this.report_rep_stereo;
    }

    public String getreport_rep_speakers() {
        return this.report_rep_speakers;
    }

    public String getreport_rep_tires_pcs() {
        return this.report_rep_tires_pcs;
    }

    public String getreport_rep_tires() {
        return this.report_rep_tires;
    }

    public String getreport_rep_side_mirror_pcs() {
        return this.report_rep_side_mirror_pcs;
    }

    public String getreport_rep_side_mirror() {
        return this.report_rep_side_mirror;
    }

    public String getreport_rep_light() {
        return this.report_rep_light;
    }

    public String getreport_rep_tools() {
        return this.report_rep_tools;
    }

    public String getreport_rep_battery() {
        return this.report_rep_battery;
    }

    public String getreport_rep_plates() {
        return this.report_rep_plates;
    }

    public String getreport_rep_bumpers() {
        return this.report_rep_bumpers;
    }

    public String getreport_rep_windows() {
        return this.report_rep_windows;
    }

    public String getreport_rep_body() {
        return this.report_rep_body;
    }

    public String getreport_rep_engine_wash() {
        return this.report_rep_engine_wash;
    }

    public String getreport_rep_other_desc() {
        return this.report_rep_other_desc;
    }

    public String getreport_rep_other() {
        return this.report_rep_other;
    }

    public String getreport_rep_total() {
        return this.report_rep_total;
    }

    public String getreport_appraised_value() {
        return this.report_appraised_value;
    }

    //ADDED BY IAN JAN 13 2016
    public String getreport_verification_file_no() {
        return this.report_verification_file_no;
    }

    public String getreport_verification_first_reg_date_month() {
        return this.report_verification_first_reg_date_month;
    }

    public String getreport_verification_first_reg_date_day() {
        return this.report_verification_first_reg_date_day;
    }

    public String getreport_verification_first_reg_date_year() {
        return this.report_verification_first_reg_date_year;
    }

    public String getreport_comp1_mileage_value() {
        return this.report_comp1_mileage_value;
    }

    public String getreport_comp2_mileage_value() {
        return this.report_comp2_mileage_value;
    }

    public String getreport_comp3_mileage_value() {
        return this.report_comp3_mileage_value;
    }

    public String getreport_comp1_mileage_amt() {
        return this.report_comp1_mileage_amt;
    }

    public String getreport_comp2_mileage_amt() {
        return this.report_comp2_mileage_amt;
    }

    public String getreport_comp3_mileage_amt() {
        return this.report_comp3_mileage_amt;
    }

    public String getreport_comp1_adj_desc() {
        return this.report_comp1_adj_desc;
    }

    public String getreport_comp2_adj_desc() {
        return this.report_comp2_adj_desc;
    }

    public String getreport_comp3_adj_desc() {
        return this.report_comp3_adj_desc;
    }


    public String getvalrep_mv_comp1_new_unit_value() {
        return this.valrep_mv_comp1_new_unit_value;
    }
    public String getvalrep_mv_comp2_new_unit_value() {
        return this.valrep_mv_comp2_new_unit_value;
    }
    public String getvalrep_mv_comp3_new_unit_value() {
        return this.valrep_mv_comp3_new_unit_value;
    }
    public String getvalrep_mv_comp1_new_unit_amt() {
        return this.valrep_mv_comp1_new_unit_amt;
    }
    public String getvalrep_mv_comp2_new_unit_amt() {
        return this.valrep_mv_comp2_new_unit_amt;
    }
    public String getvalrep_mv_comp3_new_unit_amt() {
        return this.valrep_mv_comp3_new_unit_amt;
    }
    public String getvalrep_mv_comp_add_index_price_desc() {
        return this.valrep_mv_comp_add_index_price_desc;
    }
    public String getvalrep_mv_comp_add_index_price() {
        return this.valrep_mv_comp_add_index_price;
    }
    public String getvalrep_mv_comp_temp_ave_index_price() {
        return this.valrep_mv_comp_temp_ave_index_price;
    }
    public String getvalrep_mv_rep_other2_desc() {
        return this.valrep_mv_rep_other2_desc;
    }
    public String getvalrep_mv_rep_other2() {
        return this.valrep_mv_rep_other2;
    }
    public String getvalrep_mv_rep_other3_desc() {
        return this.valrep_mv_rep_other3_desc;
    }
    public String getvalrep_mv_rep_other3() {
        return this.valrep_mv_rep_other3;
    }


    // setters
    public void setreport_interior_dashboard(String report_interior_dashboard) {
        this.report_interior_dashboard = report_interior_dashboard;
    }

    public void setreport_interior_sidings(String report_interior_sidings) {
        this.report_interior_sidings = report_interior_sidings;
    }

    public void setreport_interior_seats(String report_interior_seats) {
        this.report_interior_seats = report_interior_seats;
    }

    public void setreport_interior_windows(String report_interior_windows) {
        this.report_interior_windows = report_interior_windows;
    }

    public void setreport_interior_ceiling(String report_interior_ceiling) {
        this.report_interior_ceiling = report_interior_ceiling;
    }

    public void setreport_interior_instrument(String report_interior_instrument) {
        this.report_interior_instrument = report_interior_instrument;
    }

    public void setreport_bodytype_grille(String report_bodytype_grille) {
        this.report_bodytype_grille = report_bodytype_grille;
    }

    public void setreport_bodytype_headlight(String report_bodytype_headlight) {
        this.report_bodytype_headlight = report_bodytype_headlight;
    }

    public void setreport_bodytype_fbumper(String report_bodytype_fbumper) {
        this.report_bodytype_fbumper = report_bodytype_fbumper;
    }

    public void setreport_bodytype_hood(String report_bodytype_hood) {
        this.report_bodytype_hood = report_bodytype_hood;
    }

    public void setreport_bodytype_rf_fender(String report_bodytype_rf_fender) {
        this.report_bodytype_rf_fender = report_bodytype_rf_fender;
    }

    public void setreport_bodytype_rf_door(String report_bodytype_rf_door) {
        this.report_bodytype_rf_door = report_bodytype_rf_door;
    }

    public void setreport_bodytype_rr_door(String report_bodytype_rr_door) {
        this.report_bodytype_rr_door = report_bodytype_rr_door;
    }

    public void setreport_bodytype_rr_fender(String report_bodytype_rr_fender) {
        this.report_bodytype_rr_fender = report_bodytype_rr_fender;
    }

    public void setreport_bodytype_backdoor(String report_bodytype_backdoor) {
        this.report_bodytype_backdoor = report_bodytype_backdoor;
    }

    public void setreport_bodytype_taillight(String report_bodytype_taillight) {
        this.report_bodytype_taillight = report_bodytype_taillight;
    }

    public void setreport_bodytype_r_bumper(String report_bodytype_r_bumper) {
        this.report_bodytype_r_bumper = report_bodytype_r_bumper;
    }

    public void setreport_bodytype_lr_fender(String report_bodytype_lr_fender) {
        this.report_bodytype_lr_fender = report_bodytype_lr_fender;
    }

    public void setreport_bodytype_lr_door(String report_bodytype_lr_door) {
        this.report_bodytype_lr_door = report_bodytype_lr_door;
    }

    public void setreport_bodytype_lf_door(String report_bodytype_lf_door) {
        this.report_bodytype_lf_door = report_bodytype_lf_door;
    }

    public void setreport_bodytype_lf_fender(String report_bodytype_lf_fender) {
        this.report_bodytype_lf_fender = report_bodytype_lf_fender;
    }

    public void setreport_bodytype_top(String report_bodytype_top) {
        this.report_bodytype_top = report_bodytype_top;
    }

    public void setreport_bodytype_paint(String report_bodytype_paint) {
        this.report_bodytype_paint = report_bodytype_paint;
    }

    public void setreport_bodytype_flooring(String report_bodytype_flooring) {
        this.report_bodytype_flooring = report_bodytype_flooring;
    }

    public void setvalrep_mv_bodytype_trunk(String valrep_mv_bodytype_trunk) {
        this.valrep_mv_bodytype_trunk = valrep_mv_bodytype_trunk;
    }

    public void setreport_misc_stereo(String report_misc_stereo) {
        this.report_misc_stereo = report_misc_stereo;
    }

    public void setreport_misc_tools(String report_misc_tools) {
        this.report_misc_tools = report_misc_tools;
    }

    public void setreport_misc_speakers(String report_misc_speakers) {
        this.report_misc_speakers = report_misc_speakers;
    }

    public void setreport_misc_airbag(String report_misc_airbag) {
        this.report_misc_airbag = report_misc_airbag;
    }

    public void setreport_misc_tires(String report_misc_tires) {
        this.report_misc_tires = report_misc_tires;
    }

    public void setreport_misc_mag_wheels(String report_misc_mag_wheels) {
        this.report_misc_mag_wheels = report_misc_mag_wheels;
    }

    public void setreport_misc_carpet(String report_misc_carpet) {
        this.report_misc_carpet = report_misc_carpet;
    }

    public void setreport_misc_others(String report_misc_others) {
        this.report_misc_others = report_misc_others;
    }

    public void setreport_enginearea_fuel(String report_enginearea_fuel) {
        this.report_enginearea_fuel = report_enginearea_fuel;
    }

    public void setreport_enginearea_transmission1(
            String report_enginearea_transmission1) {
        this.report_enginearea_transmission1 = report_enginearea_transmission1;
    }

    public void setreport_enginearea_transmission2(
            String report_enginearea_transmission2) {
        this.report_enginearea_transmission2 = report_enginearea_transmission2;
    }

    public void setreport_enginearea_chassis(String report_enginearea_chassis) {
        this.report_enginearea_chassis = report_enginearea_chassis;
    }

    public void setreport_enginearea_battery(String report_enginearea_battery) {
        this.report_enginearea_battery = report_enginearea_battery;
    }

    public void setreport_enginearea_electrical(
            String report_enginearea_electrical) {
        this.report_enginearea_electrical = report_enginearea_electrical;
    }

    public void setreport_enginearea_engine(String report_enginearea_engine) {
        this.report_enginearea_engine = report_enginearea_engine;
    }

    public void setreport_verification(String report_verification) {
        this.report_verification = report_verification;
    }

    public void setreport_verification_district_office(
            String report_verification_district_office) {
        this.report_verification_district_office = report_verification_district_office;
    }

    public void setreport_verification_encumbrance(
            String report_verification_encumbrance) {
        this.report_verification_encumbrance = report_verification_encumbrance;
    }

    public void setreport_verification_registered_owner(
            String report_verification_registered_owner) {
        this.report_verification_registered_owner = report_verification_registered_owner;
    }

    public void setreport_place_inspected(String report_place_inspected) {
        this.report_place_inspected = report_place_inspected;
    }

    public void setreport_market_valuation_fair_value(
            String report_market_valuation_fair_value) {
        this.report_market_valuation_fair_value = report_market_valuation_fair_value;
    }

    public void setreport_market_valuation_min(
            String report_market_valuation_min) {
        this.report_market_valuation_min = report_market_valuation_min;
    }

    public void setreport_market_valuation_max(
            String report_market_valuation_max) {
        this.report_market_valuation_max = report_market_valuation_max;
    }

    public void setreport_market_valuation_remarks(
            String report_market_valuation_remarks) {
        this.report_market_valuation_remarks = report_market_valuation_remarks;
    }

    public void setvalrep_mv_market_valuation_previous_remarks(
            String valrep_mv_market_valuation_previous_remarks) {
        this.valrep_mv_market_valuation_previous_remarks = valrep_mv_market_valuation_previous_remarks;
    }

    public void setreport_date_inspected_month(
            String report_date_inspected_month) {
        this.report_date_inspected_month = report_date_inspected_month;
    }

    public void setreport_date_inspected_day(
            String report_date_inspected_day) {
        this.report_date_inspected_day = report_date_inspected_day;
    }

    public void setreport_date_inspected_year(
            String report_date_inspected_year) {
        this.report_date_inspected_year = report_date_inspected_year;
    }

    public void setreport_time_inspected(
            String report_time_inspected) {
        this.report_time_inspected = report_time_inspected;
    }


    //ADDED By IAN

    public void setreport_comp1_source(
            String report_comp1_source) {
        this.report_comp1_source = report_comp1_source;
    }

    public void setreport_comp1_tel_no(
            String report_comp1_tel_no) {
        this.report_comp1_tel_no = report_comp1_tel_no;
    }

    public void setreport_comp1_unit_model(
            String report_comp1_unit_model) {
        this.report_comp1_unit_model = report_comp1_unit_model;
    }

    public void setreport_comp1_mileage(
            String report_comp1_mileage) {
        this.report_comp1_mileage = report_comp1_mileage;
    }

    public void setreport_comp1_price_min(
            String report_comp1_price_min) {
        this.report_comp1_price_min = report_comp1_price_min;
    }

    public void setreport_comp1_price_max(
            String report_comp1_price_max) {
        this.report_comp1_price_max = report_comp1_price_max;
    }

    public void setreport_comp1_price(
            String report_comp1_price) {
        this.report_comp1_price = report_comp1_price;
    }

    public void setreport_comp1_less_amt(
            String report_comp1_less_amt) {
        this.report_comp1_less_amt = report_comp1_less_amt;
    }

    public void setreport_comp1_less_net(
            String report_comp1_less_net) {
        this.report_comp1_less_net = report_comp1_less_net;
    }

    public void setreport_comp1_add_amt(
            String report_comp1_add_amt) {
        this.report_comp1_add_amt = report_comp1_add_amt;
    }

    public void setreport_comp1_add_net(
            String report_comp1_add_net) {
        this.report_comp1_add_net = report_comp1_add_net;
    }

    public void setreport_comp1_time_adj_value(
            String report_comp1_time_adj_value) {
        this.report_comp1_time_adj_value = report_comp1_time_adj_value;
    }

    public void setreport_comp1_time_adj_amt(
            String report_comp1_time_adj_amt) {
        this.report_comp1_time_adj_amt = report_comp1_time_adj_amt;
    }

    public void setreport_comp1_adj_value(
            String report_comp1_adj_value) {
        this.report_comp1_adj_value = report_comp1_adj_value;
    }

    public void setreport_comp1_adj_amt(
            String report_comp1_adj_amt) {
        this.report_comp1_adj_amt = report_comp1_adj_amt;
    }

    public void setreport_comp1_index_price(
            String report_comp1_index_price) {
        this.report_comp1_index_price = report_comp1_index_price;
    }


    public void setreport_comp2_source(
            String report_comp2_source) {
        this.report_comp2_source = report_comp2_source;
    }

    public void setreport_comp2_tel_no(
            String report_comp2_tel_no) {
        this.report_comp2_tel_no = report_comp2_tel_no;
    }

    public void setreport_comp2_unit_model(
            String report_comp2_unit_model) {
        this.report_comp2_unit_model = report_comp2_unit_model;
    }

    public void setreport_comp2_mileage(
            String report_comp2_mileage) {
        this.report_comp2_mileage = report_comp2_mileage;
    }

    public void setreport_comp2_price_min(
            String report_comp2_price_min) {
        this.report_comp2_price_min = report_comp2_price_min;
    }

    public void setreport_comp2_price_max(
            String report_comp2_price_max) {
        this.report_comp2_price_max = report_comp2_price_max;
    }

    public void setreport_comp2_price(
            String report_comp2_price) {
        this.report_comp2_price = report_comp2_price;
    }

    public void setreport_comp2_less_amt(
            String report_comp2_less_amt) {
        this.report_comp2_less_amt = report_comp2_less_amt;
    }

    public void setreport_comp2_less_net(
            String report_comp2_less_net) {
        this.report_comp2_less_net = report_comp2_less_net;
    }

    public void setreport_comp2_add_amt(
            String report_comp2_add_amt) {
        this.report_comp2_add_amt = report_comp2_add_amt;
    }

    public void setreport_comp2_add_net(
            String report_comp2_add_net) {
        this.report_comp2_add_net = report_comp2_add_net;
    }

    public void setreport_comp2_time_adj_value(
            String report_comp2_time_adj_value) {
        this.report_comp2_time_adj_value = report_comp2_time_adj_value;
    }

    public void setreport_comp2_time_adj_amt(
            String report_comp2_time_adj_amt) {
        this.report_comp2_time_adj_amt = report_comp2_time_adj_amt;
    }

    public void setreport_comp2_adj_value(
            String report_comp2_adj_value) {
        this.report_comp2_adj_value = report_comp2_adj_value;
    }

    public void setreport_comp2_adj_amt(
            String report_comp2_adj_amt) {
        this.report_comp2_adj_amt = report_comp2_adj_amt;
    }

    public void setreport_comp2_index_price(
            String report_comp2_index_price) {
        this.report_comp2_index_price = report_comp2_index_price;
    }


    public void setreport_comp3_source(
            String report_comp3_source) {
        this.report_comp3_source = report_comp3_source;
    }

    public void setreport_comp3_tel_no(
            String report_comp3_tel_no) {
        this.report_comp3_tel_no = report_comp3_tel_no;
    }

    public void setreport_comp3_unit_model(
            String report_comp3_unit_model) {
        this.report_comp3_unit_model = report_comp3_unit_model;
    }

    public void setreport_comp3_mileage(
            String report_comp3_mileage) {
        this.report_comp3_mileage = report_comp3_mileage;
    }

    public void setreport_comp3_price_min(
            String report_comp3_price_min) {
        this.report_comp3_price_min = report_comp3_price_min;
    }

    public void setreport_comp3_price_max(
            String report_comp3_price_max) {
        this.report_comp3_price_max = report_comp3_price_max;
    }

    public void setreport_comp3_price(
            String report_comp3_price) {
        this.report_comp3_price = report_comp3_price;
    }

    public void setreport_comp3_less_amt(
            String report_comp3_less_amt) {
        this.report_comp3_less_amt = report_comp3_less_amt;
    }

    public void setreport_comp3_less_net(
            String report_comp3_less_net) {
        this.report_comp3_less_net = report_comp3_less_net;
    }

    public void setreport_comp3_add_amt(
            String report_comp3_add_amt) {
        this.report_comp3_add_amt = report_comp3_add_amt;
    }

    public void setreport_comp3_add_net(
            String report_comp3_add_net) {
        this.report_comp3_add_net = report_comp3_add_net;
    }

    public void setreport_comp3_time_adj_value(
            String report_comp3_time_adj_value) {
        this.report_comp3_time_adj_value = report_comp3_time_adj_value;
    }

    public void setreport_comp3_time_adj_amt(
            String report_comp3_time_adj_amt) {
        this.report_comp3_time_adj_amt = report_comp3_time_adj_amt;
    }

    public void setreport_comp3_adj_value(
            String report_comp3_adj_value) {
        this.report_comp3_adj_value = report_comp3_adj_value;
    }

    public void setreport_comp3_adj_amt(
            String report_comp3_adj_amt) {
        this.report_comp3_adj_amt = report_comp3_adj_amt;
    }

    public void setreport_comp3_index_price(
            String report_comp3_index_price) {
        this.report_comp3_index_price = report_comp3_index_price;
    }


    public void setreport_comp_ave_index_price(
            String report_comp_ave_index_price) {
        this.report_comp_ave_index_price = report_comp_ave_index_price;
    }

    public void setreport_rec_lux_car(
            String report_rec_lux_car) {
        this.report_rec_lux_car = report_rec_lux_car;
    }

    public void setreport_rec_mile_factor(
            String report_rec_mile_factor) {
        this.report_rec_mile_factor = report_rec_mile_factor;
    }

    public void setreport_rec_taxi_use(
            String report_rec_taxi_use) {
        this.report_rec_taxi_use = report_rec_taxi_use;
    }

    public void setreport_rec_for_hire(
            String report_rec_for_hire) {
        this.report_rec_for_hire = report_rec_for_hire;
    }

    public void setreport_rec_condition(
            String report_rec_condition) {
        this.report_rec_condition = report_rec_condition;
    }

    public void setreport_rec_repo(
            String report_rec_repo) {
        this.report_rec_repo = report_rec_repo;
    }

    public void setreport_rec_other_val(
            String report_rec_other_val) {
        this.report_rec_other_val = report_rec_other_val;
    }

    public void setreport_rec_other_desc(
            String report_rec_other_desc) {
        this.report_rec_other_desc = report_rec_other_desc;
    }

    public void setreport_rec_other2_val(
            String report_rec_other2_val) {
        this.report_rec_other2_val = report_rec_other2_val;
    }

    public void setreport_rec_other2_desc(
            String report_rec_other2_desc) {
        this.report_rec_other2_desc = report_rec_other2_desc;
    }

    public void setreport_rec_other3_val(
            String report_rec_other3_val) {
        this.report_rec_other3_val = report_rec_other3_val;
    }

    public void setreport_rec_other3_desc(
            String report_rec_other3_desc) {
        this.report_rec_other3_desc = report_rec_other3_desc;
    }

    public void setreport_rec_market_resistance_rec_total(
            String report_rec_market_resistance_rec_total) {
        this.report_rec_market_resistance_rec_total = report_rec_market_resistance_rec_total;
    }

    public void setreport_rec_market_resistance_total(
            String report_rec_market_resistance_total) {
        this.report_rec_market_resistance_total = report_rec_market_resistance_total;
    }

    public void setreport_rec_market_resistance_net_total(
            String report_rec_market_resistance_net_total) {
        this.report_rec_market_resistance_net_total = report_rec_market_resistance_net_total;
    }

    public void setreport_rep_stereo(
            String report_rep_stereo) {
        this.report_rep_stereo = report_rep_stereo;
    }

    public void setreport_rep_speakers(
            String report_rep_speakers) {
        this.report_rep_speakers = report_rep_speakers;
    }

    public void setreport_rep_tires_pcs(
            String report_rep_tires_pcs) {
        this.report_rep_tires_pcs = report_rep_tires_pcs;
    }

    public void setreport_rep_tires(
            String report_rep_tires) {
        this.report_rep_tires = report_rep_tires;
    }

    public void setreport_rep_side_mirror_pcs(
            String report_rep_side_mirror_pcs) {
        this.report_rep_side_mirror_pcs = report_rep_side_mirror_pcs;
    }

    public void setreport_rep_side_mirror(
            String report_rep_side_mirror) {
        this.report_rep_side_mirror = report_rep_side_mirror;
    }

    public void setreport_rep_light(
            String report_rep_light) {
        this.report_rep_light = report_rep_light;
    }

    public void setreport_rep_tools(
            String report_rep_tools) {
        this.report_rep_tools = report_rep_tools;
    }

    public void setreport_rep_battery(
            String report_rep_battery) {
        this.report_rep_battery = report_rep_battery;
    }

    public void setreport_rep_plates(
            String report_rep_plates) {
        this.report_rep_plates = report_rep_plates;
    }

    public void setreport_rep_bumpers(
            String report_rep_bumpers) {
        this.report_rep_bumpers = report_rep_bumpers;
    }

    public void setreport_rep_windows(
            String report_rep_windows) {
        this.report_rep_windows = report_rep_windows;
    }

    public void setreport_rep_body(
            String report_rep_body) {
        this.report_rep_body = report_rep_body;
    }

    public void setreport_rep_engine_wash(
            String report_rep_engine_wash) {
        this.report_rep_engine_wash = report_rep_engine_wash;
    }

    public void setreport_rep_other_desc(
            String report_rep_other_desc) {
        this.report_rep_other_desc = report_rep_other_desc;
    }

    public void setreport_rep_other(
            String report_rep_other) {
        this.report_rep_other = report_rep_other;
    }

    public void setreport_rep_total(
            String report_rep_total) {
        this.report_rep_total = report_rep_total;
    }

    public void setreport_appraised_value(
            String report_appraised_value) {
        this.report_appraised_value = report_appraised_value;
    }

    //ADDED BY IAN JAN 13 2016
    public void setreport_verification_file_no(
            String report_verification_file_no) {
        this.report_verification_file_no = report_verification_file_no;
    }

    public void setreport_verification_first_reg_date_month(
            String report_verification_first_reg_date_month) {
        this.report_verification_first_reg_date_month = report_verification_first_reg_date_month;
    }

    public void setreport_verification_first_reg_date_day(
            String report_verification_first_reg_date_day) {
        this.report_verification_first_reg_date_day = report_verification_first_reg_date_day;
    }

    public void setreport_verification_first_reg_date_year(
            String report_verification_first_reg_date_year) {
        this.report_verification_first_reg_date_year = report_verification_first_reg_date_year;
    }

    public void setreport_comp1_mileage_value(
            String report_comp1_mileage_value) {
        this.report_comp1_mileage_value = report_comp1_mileage_value;
    }

    public void setreport_comp2_mileage_value(
            String report_comp2_mileage_value) {
        this.report_comp2_mileage_value = report_comp2_mileage_value;
    }

    public void setreport_comp3_mileage_value(
            String report_comp3_mileage_value) {
        this.report_comp3_mileage_value = report_comp3_mileage_value;
    }

    public void setreport_comp1_mileage_amt(
            String report_comp1_mileage_amt) {
        this.report_comp1_mileage_amt = report_comp1_mileage_amt;
    }

    public void setreport_comp2_mileage_amt(
            String report_comp2_mileage_amt) {
        this.report_comp2_mileage_amt = report_comp2_mileage_amt;
    }

    public void setreport_comp3_mileage_amt(
            String report_comp3_mileage_amt) {
        this.report_comp3_mileage_amt = report_comp3_mileage_amt;
    }

    public void setreport_comp1_adj_desc(
            String report_comp1_adj_desc) {
        this.report_comp1_adj_desc = report_comp1_adj_desc;
    }

    public void setreport_comp2_adj_desc(
            String report_comp2_adj_desc) {
        this.report_comp2_adj_desc = report_comp2_adj_desc;
    }

    public void setreport_comp3_adj_desc(
            String report_comp3_adj_desc) {
        this.report_comp3_adj_desc = report_comp3_adj_desc;
    }


    public void setvalrep_mv_comp1_new_unit_value(
            String valrep_mv_comp1_new_unit_value) {
        this.valrep_mv_comp1_new_unit_value = valrep_mv_comp1_new_unit_value;
    }
    public void setvalrep_mv_comp2_new_unit_value(
            String valrep_mv_comp2_new_unit_value) {
        this.valrep_mv_comp2_new_unit_value = valrep_mv_comp2_new_unit_value;
    }
    public void setvalrep_mv_comp3_new_unit_value(
            String valrep_mv_comp3_new_unit_value) {
        this.valrep_mv_comp3_new_unit_value = valrep_mv_comp3_new_unit_value;
    }
    public void setvalrep_mv_comp1_new_unit_amt(
            String valrep_mv_comp1_new_unit_amt) {
        this.valrep_mv_comp1_new_unit_amt = valrep_mv_comp1_new_unit_amt;
    }
    public void setvalrep_mv_comp2_new_unit_amt(
            String valrep_mv_comp2_new_unit_amt) {
        this.valrep_mv_comp2_new_unit_amt = valrep_mv_comp2_new_unit_amt;
    }
    public void setvalrep_mv_comp3_new_unit_amt(
            String valrep_mv_comp3_new_unit_amt) {
        this.valrep_mv_comp3_new_unit_amt = valrep_mv_comp3_new_unit_amt;
    }
    public void setvalrep_mv_comp_add_index_price_desc(
            String valrep_mv_comp_add_index_price_desc) {
        this.valrep_mv_comp_add_index_price_desc = valrep_mv_comp_add_index_price_desc;
    }
    public void setvalrep_mv_comp_add_index_price(
            String valrep_mv_comp_add_index_price) {
        this.valrep_mv_comp_add_index_price = valrep_mv_comp_add_index_price;
    }
    public void setvalrep_mv_comp_temp_ave_index_price(
            String valrep_mv_comp_temp_ave_index_price) {
        this.valrep_mv_comp_temp_ave_index_price = valrep_mv_comp_temp_ave_index_price;
    }
    public void setvalrep_mv_rep_other2_desc(
            String valrep_mv_rep_other2_desc) {
        this.valrep_mv_rep_other2_desc = valrep_mv_rep_other2_desc;
    }
    public void setvalrep_mv_rep_other2(
            String valrep_mv_rep_other2) {
        this.valrep_mv_rep_other2 = valrep_mv_rep_other2;
    }
    public void setvalrep_mv_rep_other3_desc(
            String valrep_mv_rep_other3_desc) {
        this.valrep_mv_rep_other3_desc = valrep_mv_rep_other3_desc;
    }
    public void setvalrep_mv_rep_other3(
            String valrep_mv_rep_other3) {
        this.valrep_mv_rep_other3 = valrep_mv_rep_other3;
    }


}
