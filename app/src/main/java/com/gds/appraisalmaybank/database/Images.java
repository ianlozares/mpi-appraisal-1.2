package com.gds.appraisalmaybank.database;

public class Images {

	// private variables
	int id;
	String path;
	String filename;

	// Empty constructor
	public Images() {

	}

	// constructor
	public Images(int id, String path, String filename) {
		this.id = id;
		this.path = path;
		this.filename = filename;
	}

	// constructor
	public Images(String path, String filename) {

		this.path = path;
		this.filename = filename;
	}

	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	// getting path
	public String getpath() {
		return this.path;
	}

	// setting path
	public void setpath(String path) {
		this.path = path;
	}

	// getting filename
	public String getfilename() {
		return this.filename;
	}

	// setting rq_tct_no
	public void setfilename(String filename) {
		this.filename = filename;
	}
}
