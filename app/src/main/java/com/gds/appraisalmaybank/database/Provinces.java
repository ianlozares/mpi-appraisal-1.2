package com.gds.appraisalmaybank.database;

public class Provinces {

	// private variables
	int id;
	String name;
	String region_id;

	// Empty constructor
	public Provinces() {

	}

	// constructor
	public Provinces(int id, String name, String region_id) {
		this.id = id;
		this.name = name;
		this.region_id = region_id;
	}

	// constructor
	public Provinces(String name, String region_id) {
		this.name = name;
		this.region_id = region_id;
	}

	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	// getting name
	public String getname() {
		return this.name;
	}

	// setting name
	public void setname(String name) {
		this.name = name;
	}

	// getting region_id
	public String getregion_id() {
		return this.region_id;
	}

	// setting region_id
	public void setregion_id(String region_id) {
		this.region_id = region_id;
	}
}
