package com.gds.appraisalmaybank.database;

public class Report_Accepted_Jobs {

	// private variables
	int id;
	String record_id;
	String dr_month;
	String dr_day;
	String dr_year;
	String classification;
	String fname;
	String mname;
	String lname;
	String requesting_party;
	String requestor;
	String control_no;
	String appraisal_type;
	String nature_appraisal;
	String ins_date1_month;
	String ins_date1_day;
	String ins_date1_year;
	String ins_date2_month;
	String ins_date2_day;
	String ins_date2_year;
	String com_month;
	String com_day;
	String com_year;
	String tct_no;
	String unit_no;
	String building_name;
	String street_no;
	String street_name;
	String village;
	String district;
	String zip_code;
	String city;
	String province;
	String region;
	String country;
	String counter;
	String vehicle_type;
	String car_model;
	String lot_no;
	String block_no;
	String car_loan_purpose;
	String car_mileage;
	String car_series;
	String car_color;
	String car_plate_no;
	String car_body_type;
	String car_displacement;
	String car_motor_no;
	String car_chassis_no;
	String car_no_cylinders;
	String car_cr_no;
	String car_cr_date_month;
	String car_cr_date_day;
	String car_cr_date_year;
	String rework_reason;
	String kind_of_appraisal;
	String app_request_remarks;
	String app_branch_code;
	String app_main_id;

	// Empty constructor
	public Report_Accepted_Jobs() {

	}

	// constructor
	public Report_Accepted_Jobs(int id, String record_id, String dr_month,
			String dr_day, String dr_year, String classification, String fname,
			String mname, String lname, String requesting_party,String requestor,
			String control_no, String appraisal_type, String nature_appraisal,
			String ins_date1_month, String ins_date1_day,
			String ins_date1_year, String ins_date2_month,
			String ins_date2_day, String ins_date2_year, String com_month,
			String com_day, String com_year, String tct_no, String unit_no,
			String building_name, String street_no, String street_name,
			String village, String district, String zip_code, String city,
			String province, String region, String country, String counter,
			String vehicle_type, String car_model,
			String lot_no, String block_no,
			String car_loan_purpose,
			String car_mileage,
			String car_series,
			String car_color,
			String car_plate_no,
			String car_body_type,
			String car_displacement,
			String car_motor_no,
			String car_chassis_no,
								String car_no_cylinders,
			String car_cr_no,
			String car_cr_date_month,
			String car_cr_date_day,
			String car_cr_date_year,
			String rework_reason,
								String kind_of_appraisal,
								String app_request_remarks,
								String app_branch_code,String app_main_id					) {
		this.id = id;
		this.record_id = record_id;
		this.dr_month = dr_month;
		this.dr_day = dr_day;
		this.dr_year = dr_year;
		this.classification = classification;
		this.fname = fname;
		this.mname = mname;
		this.lname = lname;
		this.requesting_party = requesting_party;
		this.requestor = requestor;
		this.control_no = control_no;
		this.appraisal_type = appraisal_type;
		this.nature_appraisal = nature_appraisal;
		this.ins_date1_month = ins_date1_month;
		this.ins_date1_day = ins_date1_day;
		this.ins_date1_year = ins_date1_year;
		this.ins_date2_month = ins_date2_month;
		this.ins_date2_day = ins_date2_day;
		this.ins_date2_year = ins_date2_year;
		this.com_month = com_month;
		this.com_day = com_day;
		this.com_year = com_year;
		this.tct_no = tct_no;
		this.unit_no = unit_no;
		this.building_name = building_name;
		this.street_no = street_no;
		this.street_name = street_name;
		this.village = village;
		this.district = district;
		this.zip_code = zip_code;
		this.city = city;
		this.province = province;
		this.region = region;
		this.country = country;
		this.counter = counter;
		this.vehicle_type = vehicle_type;
		this.car_model = car_model;
		this.lot_no = lot_no;
		this.block_no = block_no;
		this.car_loan_purpose = car_loan_purpose;
		this.car_mileage = car_mileage;
		this.car_series = car_series;
		this.car_color = car_color;
		this.car_plate_no = car_plate_no;
		this.car_body_type = car_body_type;
		this.car_displacement = car_displacement;
		this.car_motor_no = car_motor_no;
		this.car_chassis_no = car_chassis_no;
		this.car_no_cylinders = car_no_cylinders;
		this.car_cr_no = car_cr_no;
		this.car_cr_date_month = car_cr_date_month;
		this.car_cr_date_day = car_cr_date_day;
		this.car_cr_date_year = car_cr_date_year;
		this.rework_reason = rework_reason;
		this.kind_of_appraisal = kind_of_appraisal;
		this.app_request_remarks = app_request_remarks;
		this.app_branch_code = app_branch_code;
		this.app_main_id = app_main_id;
	}

	// constructor
	public Report_Accepted_Jobs(String record_id, String dr_month,
			String dr_day, String dr_year, String classification, String fname,
			String mname, String lname, String requesting_party,String requestor,
			String control_no, String appraisal_type, String nature_appraisal,
			String ins_date1_month, String ins_date1_day,
			String ins_date1_year, String ins_date2_month,
			String ins_date2_day, String ins_date2_year, String com_month,
			String com_day, String com_year, String tct_no, String unit_no,
			String building_name, String street_no, String street_name,
			String village, String district, String zip_code, String city,
			String province, String region, String country, String counter,
			String vehicle_type, String car_model,
			String lot_no, String block_no,
			String car_loan_purpose,
			String car_mileage,
			String car_series,
			String car_color,
			String car_plate_no,
			String car_body_type,
			String car_displacement,
			String car_motor_no,
			String car_chassis_no,
								String car_no_cylinders,
			String car_cr_no,
			String car_cr_date_month,
			String car_cr_date_day,
			String car_cr_date_year,
			String rework_reason,
								String kind_of_appraisal,
								String app_request_remarks,
								String app_branch_code,String app_main_id) {

		this.record_id = record_id;
		this.dr_month = dr_month;
		this.dr_day = dr_day;
		this.dr_year = dr_year;
		this.classification = classification;
		this.fname = fname;
		this.mname = mname;
		this.lname = lname;
		this.requesting_party = requesting_party;
		this.requestor = requestor;
		this.control_no = control_no;
		this.appraisal_type = appraisal_type;
		this.nature_appraisal = nature_appraisal;
		this.ins_date1_month = ins_date1_month;
		this.ins_date1_day = ins_date1_day;
		this.ins_date1_year = ins_date1_year;
		this.ins_date2_month = ins_date2_month;
		this.ins_date2_day = ins_date2_day;
		this.ins_date2_year = ins_date2_year;
		this.com_month = com_month;
		this.com_day = com_day;
		this.com_year = com_year;
		this.tct_no = tct_no;
		this.unit_no = unit_no;
		this.building_name = building_name;
		this.street_no = street_no;
		this.street_name = street_name;
		this.village = village;
		this.district = district;
		this.zip_code = zip_code;
		this.city = city;
		this.province = province;
		this.region = region;
		this.country = country;
		this.counter = counter;
		this.vehicle_type = vehicle_type;
		this.car_model = car_model;
		this.lot_no = lot_no;
		this.block_no = block_no;
		this.car_loan_purpose = car_loan_purpose;
		this.car_mileage = car_mileage;
		this.car_series = car_series;
		this.car_color = car_color;
		this.car_plate_no = car_plate_no;
		this.car_body_type = car_body_type;
		this.car_displacement = car_displacement;
		this.car_motor_no = car_motor_no;
		this.car_chassis_no = car_chassis_no;
		this.car_no_cylinders = car_no_cylinders;
		this.car_cr_no = car_cr_no;
		this.car_cr_date_month = car_cr_date_month;
		this.car_cr_date_day = car_cr_date_day;
		this.car_cr_date_year = car_cr_date_year;
		this.rework_reason = rework_reason;
		this.kind_of_appraisal = kind_of_appraisal;
		this.app_request_remarks = app_request_remarks;
		this.app_branch_code = app_branch_code;
		this.app_main_id = app_main_id;
	}
	
	// constructor address only
	public Report_Accepted_Jobs(String unit_no,
			String building_name, String street_name,
			String village, String district, String zip_code, String city,
			String province, String region, String country,
			String lot_no, String block_no) {
		this.unit_no = unit_no;
		this.building_name = building_name;
		this.street_name = street_name;
		this.village = village;
		this.district = district;
		this.zip_code = zip_code;
		this.city = city;
		this.province = province;
		this.region = region;
		this.country = country;
		this.lot_no = lot_no;
		this.block_no = block_no;
	}

	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	// getting record_id
	public String getrecord_id() {
		return this.record_id;
	}

	// setting record_id
	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}

	// getting dr_month
	public String getdr_month() {
		return this.dr_month;
	}

	// setting dr_month
	public void setdr_month(String dr_month) {
		this.dr_month = dr_month;
	}

	// getting dr_day
	public String getdr_day() {
		return this.dr_day;
	}

	// setting dr_day
	public void setdr_day(String dr_day) {
		this.dr_day = dr_day;
	}

	// getting dr_year
	public String getdr_year() {
		return this.dr_year;
	}

	// setting dr_year
	public void setdr_year(String dr_year) {
		this.dr_year = dr_year;
	}

	// getting classification
	public String getclassification() {
		return this.classification;
	}

	// setting classification
	public void setclassification(String classification) {
		this.classification = classification;
	}

	// getting fname
	public String getfname() {
		return this.fname;
	}

	// setting fname
	public void setfname(String fname) {
		this.fname = fname;
	}

	// getting mname
	public String getmname() {
		return this.mname;
	}

	// setting mname
	public void setmname(String mname) {
		this.mname = mname;
	}

	// getting lname
	public String getlname() {
		return this.lname;
	}

	// setting lname
	public void setlname(String lname) {
		this.lname = lname;
	}

	// getting requesting_party
	public String getrequesting_party() {
		return this.requesting_party;
	}

	// setting requesting_party
	public void setrequesting_party(String requesting_party) {
		this.requesting_party = requesting_party;
	}

	// getting requestor
	public String getrequestor() {
		return this.requestor;
	}

	// setting requestor
	public void setrequestor(String requestor) {
		this.requestor = requestor;
	}

	// getting control_no
	public String getcontrol_no() {
		return this.control_no;
	}

	// setting control_no
	public void setcontrol_no(String control_no) {
		this.control_no = control_no;
	}

	// getting appraisal_type
	public String getappraisal_type() {
		return this.appraisal_type;
	}

	// setting appraisal_type
	public void setappraisal_type(String appraisal_type) {
		this.appraisal_type = appraisal_type;
	}

	// getting nature_appraisal
	public String getnature_appraisal() {
		return this.nature_appraisal;
	}

	// setting nature_appraisal
	public void setnature_appraisal(String nature_appraisal) {
		this.nature_appraisal = nature_appraisal;
	}

	// getting ins_date1_month
	public String getins_date1_month() {
		return this.ins_date1_month;
	}

	// setting ins_date1_month
	public void setins_date1_month(String ins_date1_month) {
		this.ins_date1_month = ins_date1_month;
	}

	// getting ins_date1_day
	public String getins_date1_day() {
		return this.ins_date1_day;
	}

	// setting ins_date1_day
	public void setins_date1_day(String ins_date1_day) {
		this.ins_date1_day = ins_date1_day;
	}

	// getting ins_date1_year
	public String getins_date1_year() {
		return this.ins_date1_year;
	}

	// setting ins_date1_year
	public void setins_date1_year(String ins_date1_year) {
		this.ins_date1_year = ins_date1_year;
	}

	// getting ins_date2_month
	public String getins_date2_month() {
		return this.ins_date2_month;
	}

	// setting ins_date2_month
	public void setins_date2_month(String ins_date2_month) {
		this.ins_date2_month = ins_date2_month;
	}

	// getting ins_date2_day
	public String getins_date2_day() {
		return this.ins_date2_day;
	}

	// setting ins_date2_day
	public void setins_date2_day(String ins_date2_day) {
		this.ins_date2_day = ins_date2_day;
	}

	// getting ins_date2_year
	public String getins_date2_year() {
		return this.ins_date2_year;
	}

	// setting ins_date2_year
	public void setins_date2_year(String ins_date2_year) {
		this.ins_date2_year = ins_date2_year;
	}

	// getting com_month
	public String getcom_month() {
		return this.com_month;
	}

	// setting com_month
	public void setcom_month(String com_month) {
		this.com_month = com_month;
	}

	// getting com_day
	public String getcom_day() {
		return this.com_day;
	}

	// setting com_day
	public void setcom_day(String com_day) {
		this.com_day = com_day;
	}

	// getting com_year
	public String getcom_year() {
		return this.com_year;
	}

	// setting com_year
	public void setcom_year(String com_year) {
		this.com_year = com_year;
	}

	// getting tct_no
	public String gettct_no() {
		return this.tct_no;
	}

	// setting tct_no
	public void settct_no(String tct_no) {
		this.tct_no = tct_no;
	}

	// getting unit_no
	public String getunit_no() {
		return this.unit_no;
	}

	// setting unit_no
	public void setunit_no(String unit_no) {
		this.unit_no = unit_no;
	}

	// getting building_name
	public String getbuilding_name() {
		return this.building_name;
	}

	// setting building_name
	public void setbuilding_name(String building_name) {
		this.building_name = building_name;
	}

	// getting street_no
	public String getstreet_no() {
		return this.street_no;
	}

	// setting street_no
	public void setstreet_no(String street_no) {
		this.street_no = street_no;
	}

	// getting street_name
	public String getstreet_name() {
		return this.street_name;
	}

	// setting street_name
	public void setstreet_name(String street_name) {
		this.street_name = street_name;
	}

	// getting village
	public String getvillage() {
		return this.village;
	}

	// setting village
	public void setvillage(String village) {
		this.village = village;
	}

	// getting district
	public String getdistrict() {
		return this.district;
	}

	// setting district
	public void setdistrict(String district) {
		this.district = district;
	}

	// getting zip_code
	public String getzip_code() {
		return this.zip_code;
	}

	// setting zip_code
	public void setzip_code(String zip_code) {
		this.zip_code = zip_code;
	}

	// getting city
	public String getcity() {
		return this.city;
	}

	// setting city
	public void setcity(String city) {
		this.city = city;
	}

	// getting province
	public String getprovince() {
		return this.province;
	}

	// setting province
	public void setprovince(String province) {
		this.province = province;
	}

	// getting region
	public String getregion() {
		return this.region;
	}

	// setting region
	public void setregion(String region) {
		this.region = region;
	}

	// getting country
	public String getcountry() {
		return this.country;
	}

	// setting country
	public void setcountry(String country) {
		this.country = country;
	}

	// getting counter
	public String getcounter() {
		return this.counter;
	}

	// setting counter
	public void setcounter(String counter) {
		this.counter = counter;
	}

	// getting vehicle_type
	public String getvehicle_type() {
		return this.vehicle_type;
	}

	// setting vehicle_type
	public void setvehicle_type(String vehicle_type) {
		this.vehicle_type = vehicle_type;
	}

	// getting car_model
	public String getcar_model() {
		return this.car_model;
	}

	// setting car_model
	public void setcar_model(String car_model) {
		this.car_model = car_model;
	}
	
	// getting lot_no
	public String getlot_no() {
		return this.lot_no;
	}
	
	// setting lot_no
	public void setlot_no(String lot_no) {
		this.lot_no = lot_no;
	}
	
	// getting block_no
	public String getblock_no() {
		return this.block_no;
	}
	
	// setting block_no
	public void setblock_no(String block_no) {
		this.block_no = block_no;
	}
	
	public String getcar_loan_purpose() {
		return this.car_loan_purpose;
	}
	
	public void setcar_loan_purpose(String car_loan_purpose) {
		this.car_loan_purpose = car_loan_purpose;
	}
	
	public String getcar_mileage() {
		return this.car_mileage;
	}
	
	public void setcar_mileage(String car_mileage) {
		this.car_mileage = car_mileage;
	}
	
	public String getcar_series() {
		return this.car_series;
	}
	
	public void setcar_series(String car_series) {
		this.car_series = car_series;
	}
	
	public String getcar_color() {
		return this.car_color;
	}
	
	public void setcar_color(String car_color) {
		this.car_color = car_color;
	}
	
	public String getcar_plate_no() {
		return this.car_plate_no;
	}
	
	public void setcar_plate_no(String car_plate_no) {
		this.car_plate_no = car_plate_no;
	}
	
	public String getcar_body_type() {
		return this.car_body_type;
	}
	
	public void setcar_body_type(String car_body_type) {
		this.car_body_type = car_body_type;
	}
	
	public String getcar_displacement() {
		return this.car_displacement;
	}
	
	public void setcar_displacement(String car_displacement) {
		this.car_displacement = car_displacement;
	}
	
	public String getcar_motor_no() {
		return this.car_motor_no;
	}
	
	public void setcar_motor_no(String car_motor_no) {
		this.car_motor_no = car_motor_no;
	}
	
	public String getcar_chassis_no() {
		return this.car_chassis_no;
	}

	public String getcar_no_cylinders() {
		return this.car_no_cylinders;
	}
	
	public void setcar_chassis_no(String car_chassis_no) {
		this.car_chassis_no = car_chassis_no;
	}

	public void setcar_no_cylinders(String car_no_cylinders) {
		this.car_no_cylinders = car_no_cylinders;
	}
	
	public String getcar_cr_no() {
		return this.car_cr_no;
	}
	
	public void setcar_cr_no(String car_cr_no) {
		this.car_cr_no = car_cr_no;
	}
	
	public String getcar_cr_date_month() {
		return this.car_cr_date_month;
	}
	
	public void setcar_cr_date_month(String car_cr_date_month) {
		this.car_cr_date_month = car_cr_date_month;
	}
	
	public String getcar_cr_date_day() {
		return this.car_cr_date_day;
	}
	
	public void setcar_cr_date_day(String car_cr_date_day) {
		this.car_cr_date_day = car_cr_date_day;
	}
	
	public String getcar_cr_date_year() {
		return this.car_cr_date_year;
	}
	
	public void setcar_cr_date_year(String car_cr_date_year) {
		this.car_cr_date_year = car_cr_date_year;
	}
	
	public String getrework_reason() {
		return this.rework_reason;
	}
	
	public void setrework_reason(String rework_reason) {
		this.rework_reason = rework_reason;
	}


	public String getkind_of_appraisal() {
		return this.kind_of_appraisal;
	}

	public void setkind_of_appraisal(String kind_of_appraisal) {
		this.kind_of_appraisal = kind_of_appraisal;
	}
	public String getapp_request_remarks() {
		return this.app_request_remarks;
	}

	public void setapp_request_remarks(String app_request_remarks) {
		this.app_request_remarks = app_request_remarks;
	}

	public String getapp_branch_code() {
		return this.app_branch_code;
	}

	public void setapp_branch_code(String app_branch_code) {
		this.app_branch_code = app_branch_code;
	}

	public String getapp_main_id() {
		return this.app_main_id;
	}

	public void setapp_main_id(String app_main_id) {
		this.app_main_id = app_main_id;
	}
}
