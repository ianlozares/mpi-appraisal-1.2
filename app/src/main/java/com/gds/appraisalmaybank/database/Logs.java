package com.gds.appraisalmaybank.database;

public class Logs {
	// private variables
	int id;
	String record_id;
	String date_submitted;
	String status;
	String status_reason;
	// Empty constructor
	public Logs() {
	}

	// constructor
	public Logs(int id, String record_id, String date_submitted, String status,
			String status_reason) {
		this.id = id;
		this.record_id = record_id;
		this.date_submitted = date_submitted;
		this.status = status;
		this.status_reason = status_reason;
	}

	public Logs(String record_id, String date_submitted, String status,
			String status_reason) {
		this.record_id = record_id;
		this.date_submitted = date_submitted;
		this.status = status;
		this.status_reason = status_reason;
	}
	
	public Logs(String date_submitted, String status,
			String status_reason) {
		this.date_submitted = date_submitted;
		this.status = status;
		this.status_reason = status_reason;
	}

	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	// getting record_id
	public String getrecord_id() {
		return this.record_id;
	}

	// setting record_id
	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}

	// getting date_submitted
	public String getdate_submitted() {
		return this.date_submitted;
	}

	// setting date_submitted
	public void setdate_submitted(String date_submitted) {
		this.date_submitted = date_submitted;
	}
	
	// getting status
	public String getstatus() {
		return this.status;
	}
	
	// setting status
	public void setstatus(String status) {
		this.status = status;
	}
	
	// getting status
	public String getstatus_reason() {
		return this.status_reason;
	}
	
	// setting status
	public void setstatus_reason(String status_reason) {
		this.status_reason = status_reason;
	}
}