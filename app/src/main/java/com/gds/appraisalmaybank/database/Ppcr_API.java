package com.gds.appraisalmaybank.database;

public class Ppcr_API {

	// private variables
	int id;
	String record_id;
	String report_date_inspected_month;
	String report_date_inspected_day;
	String report_date_inspected_year;
	String report_time_inspected;
	String report_registered_owner;
	String report_project_type;
	String report_is_condo;
	String report_remarks;

	// Empty constructor
	public Ppcr_API() {

	}

	// constructor
	public Ppcr_API(int id, String record_id,
			String report_date_inspected_month,
			String report_date_inspected_day,
			String report_date_inspected_year, String report_time_inspected,
			String report_registered_owner, String report_project_type,
			String report_is_condo, String report_remarks) {
		this.id = id;
		this.record_id = record_id;
		this.report_date_inspected_month = report_date_inspected_month;
		this.report_date_inspected_day = report_date_inspected_day;
		this.report_date_inspected_year = report_date_inspected_year;
		this.report_time_inspected = report_time_inspected;
		this.report_registered_owner = report_registered_owner;
		this.report_project_type = report_project_type;
		this.report_is_condo = report_is_condo;
		this.report_remarks = report_remarks;
	}
	
	public Ppcr_API(String record_id, String report_date_inspected_month,
			String report_date_inspected_day,
			String report_date_inspected_year, String report_time_inspected,
			String report_registered_owner, String report_project_type,
			String report_is_condo, String report_remarks) {
		this.record_id = record_id;
		this.report_date_inspected_month = report_date_inspected_month;
		this.report_date_inspected_day = report_date_inspected_day;
		this.report_date_inspected_year = report_date_inspected_year;
		this.report_time_inspected = report_time_inspected;
		this.report_registered_owner = report_registered_owner;
		this.report_project_type = report_project_type;
		this.report_is_condo = report_is_condo;
		this.report_remarks = report_remarks;
	}
	
	public Ppcr_API(String report_date_inspected_month,
			String report_date_inspected_day,
			String report_date_inspected_year, String report_time_inspected,
			String report_registered_owner, String report_project_type,
			String report_is_condo, String report_remarks) {
		this.report_date_inspected_month = report_date_inspected_month;
		this.report_date_inspected_day = report_date_inspected_day;
		this.report_date_inspected_year = report_date_inspected_year;
		this.report_time_inspected = report_time_inspected;
		this.report_registered_owner = report_registered_owner;
		this.report_project_type = report_project_type;
		this.report_is_condo = report_is_condo;
		this.report_remarks = report_remarks;
	}


	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	// getting record_id
	public String getrecord_id() {
		return this.record_id;
	}

	// setting record_id
	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}
	
	//getters
	public String getreport_date_inspected_month() {
		return this.report_date_inspected_month;
	}

	public String getreport_date_inspected_day() {
		return this.report_date_inspected_day;
	}

	public String getreport_date_inspected_year() {
		return this.report_date_inspected_year;
	}

	public String getreport_time_inspected() {
		return this.report_time_inspected;
	}

	public String getreport_registered_owner() {
		return this.report_registered_owner;
	}

	public String getreport_project_type() {
		return this.report_project_type;
	}

	public String getreport_is_condo() {
		return this.report_is_condo;
	}

	public String getreport_remarks() {
		return this.report_remarks;
	}
	
	//setters
	public void setreport_date_inspected_month(
			String report_date_inspected_month) {
		this.report_date_inspected_month = report_date_inspected_month;
	}

	public void setreport_date_inspected_day(String report_date_inspected_day) {
		this.report_date_inspected_day = report_date_inspected_day;
	}

	public void setreport_date_inspected_year(String report_date_inspected_year) {
		this.report_date_inspected_year = report_date_inspected_year;
	}

	public void setreport_time_inspected(String report_time_inspected) {
		this.report_time_inspected = report_time_inspected;
	}

	public void setreport_registered_owner(String report_registered_owner) {
		this.report_registered_owner = report_registered_owner;
	}

	public void setreport_project_type(String report_project_type) {
		this.report_project_type = report_project_type;
	}

	public void setreport_is_condo(String report_is_condo) {
		this.report_is_condo = report_is_condo;
	}

	public void setreport_remarks(String report_remarks) {
		this.report_remarks = report_remarks;
	}

}
