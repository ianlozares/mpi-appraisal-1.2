package com.gds.appraisalmaybank.database;

public class Request_Collateral_Address {

	// private variables
	int id;
	String rt_id;
	String rq_tct_no;
	String rq_unit_no;
	String rq_building_name;
	String rq_street_no;
	String rq_street_name;
	String rq_village;
	String rq_barangay;
	String rq_zip_code;
	String rq_city;
	String rq_province;
	String rq_region;
	String rq_country;

	// Empty constructor
	public Request_Collateral_Address() {

	}

	// constructor
	public Request_Collateral_Address(int id, String rt_id, String rq_tct_no,
			String rq_unit_no, String rq_building_name, String rq_street_no,
			String rq_street_name, String rq_village, String rq_barangay,
			String rq_zip_code, String rq_city, String rq_province,
			String rq_region, String rq_country) {
		this.id = id;
		this.rt_id = rt_id;
		this.rq_tct_no = rq_tct_no;
		this.rq_unit_no = rq_unit_no;
		this.rq_building_name = rq_building_name;
		this.rq_street_no = rq_street_no;
		this.rq_street_name = rq_street_name;
		this.rq_village = rq_village;
		this.rq_barangay = rq_barangay;
		this.rq_zip_code = rq_zip_code;
		this.rq_city = rq_city;
		this.rq_province = rq_province;
		this.rq_region = rq_region;
		this.rq_country = rq_country;

	}

	// constructor
	public Request_Collateral_Address(String rt_id, String rq_tct_no,
			String rq_unit_no, String rq_building_name, String rq_street_no,
			String rq_street_name, String rq_village, String rq_barangay,
			String rq_zip_code, String rq_city, String rq_province,
			String rq_region, String rq_country) {

		this.rt_id = rt_id;
		this.rq_tct_no = rq_tct_no;
		this.rq_unit_no = rq_unit_no;
		this.rq_building_name = rq_building_name;
		this.rq_street_no = rq_street_no;
		this.rq_street_name = rq_street_name;
		this.rq_village = rq_village;
		this.rq_barangay = rq_barangay;
		this.rq_zip_code = rq_zip_code;
		this.rq_city = rq_city;
		this.rq_province = rq_province;
		this.rq_region = rq_region;
		this.rq_country = rq_country;
	}

	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	// getting rt_id
	public String getrt_id() {
		return this.rt_id;
	}

	// setting rt_id
	public void setrt_id(String rt_id) {
		this.rt_id = rt_id;
	}

	// getting rq_tct_no
	public String getrq_tct_no() {
		return this.rq_tct_no;
	}

	// setting rq_tct_no
	public void setrq_tct_no(String rq_tct_no) {
		this.rq_tct_no = rq_tct_no;
	}

	// getting rq__unit_no
	public String getrq_unit_no() {
		return this.rq_unit_no;
	}

	// setting rq__unit_no
	public void setrq_unit_no(String rq_unit_no) {
		this.rq_unit_no = rq_unit_no;
	}

	// getting rq_building_name
	public String getrq_building_name() {
		return this.rq_building_name;
	}

	// setting rq_building_name
	public void setrq_building_name(String rq_building_name) {
		this.rq_building_name = rq_building_name;
	}

	// getting rq_street_no
	public String getrq_street_no() {
		return this.rq_street_no;
	}

	// setting rq_street_no
	public void setrq_street_no(String rq_street_no) {
		this.rq_street_no = rq_street_no;
	}

	// getting rq_street_name
	public String getrq_street_name() {
		return this.rq_street_name;
	}

	// setting rq_street_name
	public void setrq_street_name(String rq_street_name) {
		this.rq_street_name = rq_street_name;
	}

	// getting rq_village
	public String getrq_village() {
		return this.rq_village;
	}

	// setting rq_village
	public void setrq_village(String rq_village) {
		this.rq_village = rq_village;
	}

	// getting rq_barangay
	public String getrq_barangay() {
		return this.rq_barangay;
	}

	// setting rq_barangay
	public void setrq_barangay(String rq_barangay) {
		this.rq_barangay = rq_barangay;
	}

	// getting rq_zip_code
	public String getrq_zip_code() {
		return this.rq_zip_code;
	}

	// setting rq_zip_code
	public void setrq_zip_code(String rq_zip_code) {
		this.rq_zip_code = rq_zip_code;
	}

	// getting rq_city
	public String getrq_city() {
		return this.rq_city;
	}

	// setting rq_city
	public void setrq_city(String rq_city) {
		this.rq_city = rq_city;
	}

	// getting rq_province
	public String getrq_province() {
		return this.rq_province;
	}

	// setting rq_province
	public void setrq_province(String rq_province) {
		this.rq_province = rq_province;
	}

	// getting rq_region
	public String getrq_region() {
		return this.rq_region;
	}

	// setting rq_region
	public void setrq_region(String rq_region) {
		this.rq_region = rq_region;
	}

	// getting rq_country
	public String getrq_country() {
		return this.rq_country;
	}

	// setting rq_country
	public void setrq_country(String rq_country) {
		this.rq_country = rq_country;
	}
}
