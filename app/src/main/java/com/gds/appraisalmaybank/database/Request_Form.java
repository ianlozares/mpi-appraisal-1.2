package com.gds.appraisalmaybank.database;

public class Request_Form {

	// private variables
	int id;
	String rf_date_requested_month;
	String rf_date_requested_day;
	String rf_date_requested_year;
	String rf_classification;
	String rf_account_fname;
	String rf_account_mname;
	String rf_account_lname;
	String rf_requesting_party;

	// Empty constructor
	public Request_Form() {

	}

	// constructor
	public Request_Form(int id, String rf_date_requested_month,
			String rf_date_requested_day, String rf_date_requested_year,
			String rf_classification, String rf_account_fname,
			String rf_account_mname, String rf_account_lname,
			String rf_requesting_party) {
		this.id = id;
		this.rf_date_requested_month = rf_date_requested_month;
		this.rf_date_requested_day = rf_date_requested_day;
		this.rf_date_requested_year = rf_date_requested_year;
		this.rf_classification = rf_classification;
		this.rf_account_fname = rf_account_fname;
		this.rf_account_mname = rf_account_mname;
		this.rf_account_lname = rf_account_lname;
		this.rf_requesting_party = rf_requesting_party;
	}

	// constructor
	public Request_Form(String rf_date_requested_month,
			String rf_date_requested_day, String rf_date_requested_year,
			String rf_classification, String rf_account_fname,
			String rf_account_mname, String rf_account_lname,
			String rf_requesting_party) {

		this.rf_date_requested_month = rf_date_requested_month;
		this.rf_date_requested_day = rf_date_requested_day;
		this.rf_date_requested_year = rf_date_requested_year;
		this.rf_classification = rf_classification;
		this.rf_account_fname = rf_account_fname;
		this.rf_account_mname = rf_account_mname;
		this.rf_account_lname = rf_account_lname;
		this.rf_requesting_party = rf_requesting_party;
	}

	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	// getting rf_date_requested_month
	public String getrf_date_requested_month() {
		return this.rf_date_requested_month;
	}

	// setting rf_date_requested_month
	public void setrf_date_requested_month(String rf_date_requested_month) {
		this.rf_date_requested_month = rf_date_requested_month;
	}

	// getting rf_date_requested_day
	public String getrf_date_requested_day() {
		return this.rf_date_requested_day;
	}

	// setting rf_date_requested_day
	public void setrf_date_requested_day(String rf_date_requested_day) {
		this.rf_date_requested_day = rf_date_requested_day;
	}

	// getting rf_date_requested_year
	public String getrf_date_requested_year() {
		return this.rf_date_requested_year;
	}

	// setting rf_date_requested_year
	public void setrf_date_requested_year(String rf_date_requested_year) {
		this.rf_date_requested_year = rf_date_requested_year;
	}

	// getting rf_classification
	public String getrf_classification() {
		return this.rf_classification;
	}

	// setting rf_classification
	public void setrf_classification(String rf_classification) {
		this.rf_classification = rf_classification;
	}

	// getting rf_account_fname
	public String getrf_account_fname() {
		return this.rf_account_fname;
	}

	// setting rf_account_fname
	public void setrf_account_fname(String rf_account_fname) {
		this.rf_account_fname = rf_account_fname;
	}

	// getting rf_account_mname
	public String getrf_account_mname() {
		return this.rf_account_mname;
	}

	// setting rf_account_mname
	public void setrf_account_mname(String rf_account_mname) {
		this.rf_account_mname = rf_account_mname;
	}

	// getting rf_account_lname
	public String getrf_account_lname() {
		return this.rf_account_lname;
	}

	// setting rf_account_lname
	public void setrf_account_lname(String rf_account_lname) {
		this.rf_account_lname = rf_account_lname;
	}

	// getting rf_requesting_party
	public String getrf_requesting_party() {
		return this.rf_requesting_party;
	}

	// setting rf_requesting_party
	public void setrf_requesting_party(String rf_requesting_party) {
		this.rf_requesting_party = rf_requesting_party;
	}
}
