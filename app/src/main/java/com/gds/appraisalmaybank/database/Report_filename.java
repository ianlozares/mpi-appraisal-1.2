package com.gds.appraisalmaybank.database;

public class Report_filename {

	// private variables
	int id;
	String record_id;
	String uid;
	String file;
	String filename;
	String appraisal_type;
	String candidate_done;

	// Empty constructor
	public Report_filename() {

	}

	// constructor
	public Report_filename(int id, String record_id, String uid, String file,
			String filename, String appraisal_type, String candidate_done) {
		this.id = id;
		this.record_id = record_id;
		this.uid = uid;
		this.file = file;
		this.filename = filename;
		this.appraisal_type = appraisal_type;
		this.candidate_done = candidate_done;
	}

	// constructor
	public Report_filename(String record_id, String uid, String file,
			String filename, String appraisal_type, String candidate_done) {

		this.record_id = record_id;
		this.uid = uid;
		this.file = file;
		this.filename = filename;
		this.appraisal_type = appraisal_type;
		this.candidate_done = candidate_done;
	}

	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	// getting record_id
	public String getrecord_id() {
		return this.record_id;
	}

	// setting record_id
	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}

	// getting uid
	public String getuid() {
		return this.uid;
	}

	// setting uid
	public void setuid(String uid) {
		this.uid = uid;
	}

	// getting file
	public String getfile() {
		return this.file;
	}

	// setting file
	public void setfile(String file) {
		this.file = file;
	}

	// getting filename
	public String getfilename() {
		return this.filename;
	}

	// setting filename
	public void setfilename(String filename) {
		this.filename = filename;
	}

	// getting appraisal_type
	public String getappraisal_type() {
		return this.appraisal_type;
	}

	// setting appraisal_type
	public void setappraisal_type(String appraisal_type) {
		this.appraisal_type = appraisal_type;
	}

	// getting candidate_done
	public String getcandidate_done() {
		return this.candidate_done;
	}

	// setting candidate_done
	public void setcandidate_done(String candidate_done) {
		this.candidate_done = candidate_done;
	}
}
