package com.gds.appraisalmaybank.database;

public class Townhouse_API {

	// private variables
	int id;
	String record_id;
	String report_date_inspected_month;
	String report_date_inspected_day;
	String report_date_inspected_year;
	String report_id_association;
	String report_id_tax;
	String report_id_lra;
	String report_id_lot_config;
	String report_id_subd_map;
	String report_id_nbrhood_checking;
	String report_imp_street_name;
	String report_imp_street;
	String report_imp_sidewalk;
	String report_imp_curb;
	String report_imp_drainage;
	String report_imp_street_lights;
	String report_physical_corner_lot;
	String report_physical_non_corner_lot;
	String report_physical_perimeter_lot;
	String report_physical_intersected_lot;
	String report_physical_interior_with_row;
	String report_physical_landlocked;
	String report_lotclass_per_tax_dec;
	String report_lotclass_actual_usage;
	String report_lotclass_neighborhood;
	String report_lotclass_highest_best_use;
	String report_physical_shape;
	String report_physical_frontage;
	String report_physical_depth;
	String report_physical_road;
	String report_physical_elevation;
	String report_physical_terrain;
	String report_landmark_1;
	String report_landmark_2;
	String report_landmark_3;
	String report_landmark_4;
	String report_landmark_5;
	String report_distance_1;
	String report_distance_2;
	String report_distance_3;
	String report_distance_4;
	String report_distance_5;
	String report_util_electricity;
	String report_util_water;
	String report_util_telephone;
	String report_util_garbage;
	String report_trans_jeep;
	String report_trans_bus;
	String report_trans_taxi;
	String report_trans_tricycle;
	String report_faci_recreation;
	String report_faci_security;
	String report_faci_clubhouse;
	String report_faci_pool;
	String report_bound_right;
	String report_bound_left;
	String report_bound_rear;
	String report_bound_front;
	String report_comp1_date_month;
	String report_comp1_date_day;
	String report_comp1_date_year;
	String report_comp1_source;
	String report_comp1_contact_no;
	String report_comp1_location;
	String report_comp1_area;
	String report_comp1_base_price;
	String report_comp1_price_sqm;
	String report_comp1_discount_rate;
	String report_comp1_discounts;
	String report_comp1_selling_price;
	String report_comp1_rec_location;
	String report_comp1_rec_lot_size;
	String report_comp1_rec_floor_area;
	String report_comp1_rec_unit_condition;
	String report_comp1_rec_unit_features;
	String report_comp1_rec_degree_of_devt;
	String report_comp1_rec_corner_influence;
	String report_comp1_concluded_adjustment;
	String report_comp1_adjusted_value;
	String report_comp2_date_month;
	String report_comp2_date_day;
	String report_comp2_date_year;
	String report_comp2_source;
	String report_comp2_contact_no;
	String report_comp2_location;
	String report_comp2_area;
	String report_comp2_base_price;
	String report_comp2_price_sqm;
	String report_comp2_discount_rate;
	String report_comp2_discounts;
	String report_comp2_selling_price;
	String report_comp2_rec_location;
	String report_comp2_rec_lot_size;
	String report_comp2_rec_floor_area;
	String report_comp2_rec_unit_condition;
	String report_comp2_rec_unit_features;
	String report_comp2_rec_degree_of_devt;
	String report_comp2_rec_corner_influence;
	String report_comp2_concluded_adjustment;
	String report_comp2_adjusted_value;
	String report_comp3_date_month;
	String report_comp3_date_day;
	String report_comp3_date_year;
	String report_comp3_source;
	String report_comp3_contact_no;
	String report_comp3_location;
	String report_comp3_area;
	String report_comp3_base_price;
	String report_comp3_price_sqm;
	String report_comp3_discount_rate;
	String report_comp3_discounts;
	String report_comp3_selling_price;
	String report_comp3_rec_location;
	String report_comp3_rec_lot_size;
	String report_comp3_rec_floor_area;
	String report_comp3_rec_unit_condition;
	String report_comp3_rec_unit_features;
	String report_comp3_rec_degree_of_devt;
	String report_comp3_rec_corner_influence;
	String report_comp3_concluded_adjustment;
	String report_comp3_adjusted_value;
	String report_comp4_date_month;
	String report_comp4_date_day;
	String report_comp4_date_year;
	String report_comp4_source;
	String report_comp4_contact_no;
	String report_comp4_location;
	String report_comp4_area;
	String report_comp4_base_price;
	String report_comp4_price_sqm;
	String report_comp4_discount_rate;
	String report_comp4_discounts;
	String report_comp4_selling_price;
	String report_comp4_rec_location;
	String report_comp4_rec_lot_size;
	String report_comp4_rec_floor_area;
	String report_comp4_rec_unit_condition;
	String report_comp4_rec_unit_features;
	String report_comp4_rec_degree_of_devt;
	String report_comp4_rec_corner_influence;
	String report_comp4_concluded_adjustment;
	String report_comp4_adjusted_value;
	String report_comp5_date_month;
	String report_comp5_date_day;
	String report_comp5_date_year;
	String report_comp5_source;
	String report_comp5_contact_no;
	String report_comp5_location;
	String report_comp5_area;
	String report_comp5_base_price;
	String report_comp5_price_sqm;
	String report_comp5_discount_rate;
	String report_comp5_discounts;
	String report_comp5_selling_price;
	String report_comp5_rec_location;
	String report_comp5_rec_lot_size;
	String report_comp5_rec_floor_area;
	String report_comp5_rec_unit_condition;
	String report_comp5_rec_unit_features;
	String report_comp5_rec_degree_of_devt;
	String report_comp5_rec_corner_influence;
	String report_comp5_concluded_adjustment;
	String report_comp5_adjusted_value;
	String report_comp_average;
	String report_comp_rounded_to;
	String report_zonal_location;
	String report_zonal_lot_classification;
	String report_zonal_value;
	String report_value_total_area;
	String report_value_total_deduction;
	String report_value_total_net_area;
	String report_value_total_landimp_value;
	String report_imp_value_total_imp_value;
	//report summary
	String report_final_value_total_appraised_value_land_imp;
	String report_final_value_recommended_deductions_desc;
	String report_final_value_recommended_deductions_rate;
	String report_final_value_recommended_deductions_other;
	String report_final_value_recommended_deductions_amt;
	String report_final_value_net_appraised_value;
	String report_final_value_quick_sale_value_rate;
	String report_final_value_quick_sale_value_amt;
	String report_factors_of_concern;
	String report_suggested_corrective_actions;
	String report_remarks;
	String report_requirements;
	String report_time_inspected;
	
	String report_comp1_remarks;
	String report_comp2_remarks;
	String report_comp3_remarks;
	String report_comp4_remarks;
	String report_comp5_remarks;
	
	String report_comp1_lot_area;
	String report_comp2_lot_area;
	String report_comp3_lot_area;
	String report_comp4_lot_area;
	String report_comp5_lot_area;

	String report_concerns_minor_1;
	String report_concerns_minor_2;
	String report_concerns_minor_3;
	String report_concerns_minor_others;
	String report_concerns_minor_others_desc;
	String report_concerns_major_1;
	String report_concerns_major_2;
	String report_concerns_major_3;
	String report_concerns_major_4;
	String report_concerns_major_5;
	String report_concerns_major_6;
	String report_concerns_major_others;
	String report_concerns_major_others_desc;


	//Added Field Added from IAN
	String report_prev_date;
	String report_prev_appraiser;
	String report_prev_total_appraised_value;
	String report_townhouse_td_no;
	// Empty constructor
	public Townhouse_API() {

	}
	// constructor with int recordid only for parameter
	public Townhouse_API(String record_id) {
		this.record_id = record_id;
	}
	//update total only
	public Townhouse_API(String record_id, String report_imp_value_total_imp_value) {
		this.record_id = record_id;
		this.report_imp_value_total_imp_value = report_imp_value_total_imp_value;
	}

	// constructor with int id
	public Townhouse_API(int id,
			String record_id,
			String report_date_inspected_month,
			String report_date_inspected_day,
			String report_date_inspected_year,
			String report_id_association,
			String report_id_tax,
			String report_id_lra,
			String report_id_lot_config,
			String report_id_subd_map,
			String report_id_nbrhood_checking,
			String report_imp_street_name,
			String report_imp_street,
			String report_imp_sidewalk,
			String report_imp_curb,
			String report_imp_drainage,
			String report_imp_street_lights,
			String report_physical_corner_lot,
			String report_physical_non_corner_lot,
			String report_physical_perimeter_lot,
			String report_physical_intersected_lot,
			String report_physical_interior_with_row,
			String report_physical_landlocked,
			String report_lotclass_per_tax_dec,
			String report_lotclass_actual_usage,
			String report_lotclass_neighborhood,
			String report_lotclass_highest_best_use,
			String report_physical_shape,
			String report_physical_frontage,
			String report_physical_depth,
			String report_physical_road,
			String report_physical_elevation,
			String report_physical_terrain,
			String report_landmark_1,
			String report_landmark_2,
			String report_landmark_3,
			String report_landmark_4,
			String report_landmark_5,
			String report_distance_1,
			String report_distance_2,
			String report_distance_3,
			String report_distance_4,
			String report_distance_5,
			String report_util_electricity,
			String report_util_water,
			String report_util_telephone,
			String report_util_garbage,
			String report_trans_jeep,
			String report_trans_bus,
			String report_trans_taxi,
			String report_trans_tricycle,
			String report_faci_recreation,
			String report_faci_security,
			String report_faci_clubhouse,
			String report_faci_pool,
			String report_bound_right,
			String report_bound_left,
			String report_bound_rear,
			String report_bound_front,
			String report_comp1_date_month,
			String report_comp1_date_day,
			String report_comp1_date_year,
			String report_comp1_source,
			String report_comp1_contact_no,
			String report_comp1_location,
			String report_comp1_area,
			String report_comp1_base_price,
			String report_comp1_price_sqm,
			String report_comp1_discount_rate,
			String report_comp1_discounts,
			String report_comp1_selling_price,
			String report_comp1_rec_location,
			String report_comp1_rec_lot_size,
			String report_comp1_rec_floor_area,
			String report_comp1_rec_unit_condition,
			String report_comp1_rec_unit_features,
						 String report_comp1_rec_degree_of_devt,
						 String report_comp1_rec_corner_influence,
			String report_comp1_concluded_adjustment,
			String report_comp1_adjusted_value,
			String report_comp2_date_month,
			String report_comp2_date_day,
			String report_comp2_date_year,
			String report_comp2_source,
			String report_comp2_contact_no,
			String report_comp2_location,
			String report_comp2_area,
			String report_comp2_base_price,
			String report_comp2_price_sqm,
			String report_comp2_discount_rate,
			String report_comp2_discounts,
			String report_comp2_selling_price,
			String report_comp2_rec_location,
			String report_comp2_rec_lot_size,
			String report_comp2_rec_floor_area,
			String report_comp2_rec_unit_condition,
			String report_comp2_rec_unit_features,
						 String report_comp2_rec_degree_of_devt,
						 String report_comp2_rec_corner_influence,
			String report_comp2_concluded_adjustment,
			String report_comp2_adjusted_value,
			String report_comp3_date_month,
			String report_comp3_date_day,
			String report_comp3_date_year,
			String report_comp3_source,
			String report_comp3_contact_no,
			String report_comp3_location,
			String report_comp3_area,
			String report_comp3_base_price,
			String report_comp3_price_sqm,
			String report_comp3_discount_rate,
			String report_comp3_discounts,
			String report_comp3_selling_price,
			String report_comp3_rec_location,
			String report_comp3_rec_lot_size,
			String report_comp3_rec_floor_area,
			String report_comp3_rec_unit_condition,
			String report_comp3_rec_unit_features,
						 String report_comp3_rec_degree_of_devt,
						 String report_comp3_rec_corner_influence,
			String report_comp3_concluded_adjustment,
			String report_comp3_adjusted_value,
			String report_comp4_date_month,
			String report_comp4_date_day,
			String report_comp4_date_year,
			String report_comp4_source,
			String report_comp4_contact_no,
			String report_comp4_location,
			String report_comp4_area,
			String report_comp4_base_price,
			String report_comp4_price_sqm,
			String report_comp4_discount_rate,
			String report_comp4_discounts,
			String report_comp4_selling_price,
			String report_comp4_rec_location,
			String report_comp4_rec_lot_size,
			String report_comp4_rec_floor_area,
			String report_comp4_rec_unit_condition,
			String report_comp4_rec_unit_features,
						 String report_comp4_rec_degree_of_devt,
						 String report_comp4_rec_corner_influence,
			String report_comp4_concluded_adjustment,
			String report_comp4_adjusted_value,
			String report_comp5_date_month,
			String report_comp5_date_day,
			String report_comp5_date_year,
			String report_comp5_source,
			String report_comp5_contact_no,
			String report_comp5_location,
			String report_comp5_area,
			String report_comp5_base_price,
			String report_comp5_price_sqm,
			String report_comp5_discount_rate,
			String report_comp5_discounts,
			String report_comp5_selling_price,
			String report_comp5_rec_location,
			String report_comp5_rec_lot_size,
			String report_comp5_rec_floor_area,
			String report_comp5_rec_unit_condition,
			String report_comp5_rec_unit_features,
						 String report_comp5_rec_degree_of_devt,
						 String report_comp5_rec_corner_influence,
			String report_comp5_concluded_adjustment,
			String report_comp5_adjusted_value,
			String report_comp_average,
			String report_comp_rounded_to,
			String report_zonal_location,
			String report_zonal_lot_classification,
			String report_zonal_value,
			String report_value_total_area,
			String report_value_total_deduction,
			String report_value_total_net_area,
			String report_value_total_landimp_value,
			String report_imp_value_total_imp_value,
			String report_final_value_total_appraised_value_land_imp,
			String report_final_value_recommended_deductions_desc,
			String report_final_value_recommended_deductions_rate,
						 String report_final_value_recommended_deductions_other,
			String report_final_value_recommended_deductions_amt,
			String report_final_value_net_appraised_value,
			String report_final_value_quick_sale_value_rate,
			String report_final_value_quick_sale_value_amt,
			String report_factors_of_concern,
			String report_suggested_corrective_actions,
			String report_remarks,
			String report_requirements,
			String report_time_inspected,
			String report_comp1_remarks,
			String report_comp2_remarks,
			String report_comp3_remarks,
			String report_comp4_remarks,
			String report_comp5_remarks,
			String report_comp1_lot_area,
			String report_comp2_lot_area,
			String report_comp3_lot_area,
			String report_comp4_lot_area,
			String report_comp5_lot_area,
			String report_concerns_minor_1,
			String report_concerns_minor_2,
			String report_concerns_minor_3,
			String report_concerns_minor_others,
			String report_concerns_minor_others_desc,
			String report_concerns_major_1,
			String report_concerns_major_2,
			String report_concerns_major_3,
			String report_concerns_major_4,
			String report_concerns_major_5,
			String report_concerns_major_6,
			String report_concerns_major_others,
			String report_concerns_major_others_desc,
						 //Added from IAN
						 String report_prev_date,
						 String report_prev_appraiser,
						 String report_prev_total_appraised_value,
						 String report_townhouse_td_no) {
		this.id = id; 
		this.record_id = record_id;
		this.report_date_inspected_month = report_date_inspected_month;
		this.report_date_inspected_day = report_date_inspected_day;
		this.report_date_inspected_year = report_date_inspected_year;
		this.report_id_association = report_id_association;
		this.report_id_tax = report_id_tax;
		this.report_id_lra = report_id_lra;
		this.report_id_lot_config = report_id_lot_config;
		this.report_id_subd_map = report_id_subd_map;
		this.report_id_nbrhood_checking = report_id_nbrhood_checking;
		this.report_imp_street_name = report_imp_street_name;
		this.report_imp_street = report_imp_street;
		this.report_imp_sidewalk = report_imp_sidewalk;
		this.report_imp_curb = report_imp_curb;
		this.report_imp_drainage = report_imp_drainage;
		this.report_imp_street_lights = report_imp_street_lights;
		this.report_physical_corner_lot = report_physical_corner_lot;
		this.report_physical_non_corner_lot = report_physical_non_corner_lot;
		this.report_physical_perimeter_lot = report_physical_perimeter_lot;
		this.report_physical_intersected_lot = report_physical_intersected_lot;
		this.report_physical_interior_with_row = report_physical_interior_with_row;
		this.report_physical_landlocked = report_physical_landlocked;
		this.report_lotclass_per_tax_dec = report_lotclass_per_tax_dec;
		this.report_lotclass_actual_usage = report_lotclass_actual_usage;
		this.report_lotclass_neighborhood = report_lotclass_neighborhood;
		this.report_lotclass_highest_best_use = report_lotclass_highest_best_use;
		this.report_physical_shape = report_physical_shape;
		this.report_physical_frontage = report_physical_frontage;
		this.report_physical_depth = report_physical_depth;
		this.report_physical_road = report_physical_road;
		this.report_physical_elevation = report_physical_elevation;
		this.report_physical_terrain = report_physical_terrain;
		this.report_landmark_1 = report_landmark_1;
		this.report_landmark_2 = report_landmark_2;
		this.report_landmark_3 = report_landmark_3;
		this.report_landmark_4 = report_landmark_4;
		this.report_landmark_5 = report_landmark_5;
		this.report_distance_1 = report_distance_1;
		this.report_distance_2 = report_distance_2;
		this.report_distance_3 = report_distance_3;
		this.report_distance_4 = report_distance_4;
		this.report_distance_5 = report_distance_5;
		this.report_util_electricity = report_util_electricity;
		this.report_util_water = report_util_water;
		this.report_util_telephone = report_util_telephone;
		this.report_util_garbage = report_util_garbage;
		this.report_trans_jeep = report_trans_jeep;
		this.report_trans_bus = report_trans_bus;
		this.report_trans_taxi = report_trans_taxi;
		this.report_trans_tricycle = report_trans_tricycle;
		this.report_faci_recreation = report_faci_recreation;
		this.report_faci_security = report_faci_security;
		this.report_faci_clubhouse = report_faci_clubhouse;
		this.report_faci_pool = report_faci_pool;
		this.report_bound_right = report_bound_right;
		this.report_bound_left = report_bound_left;
		this.report_bound_rear = report_bound_rear;
		this.report_bound_front = report_bound_front;		
		this.report_comp1_date_month = report_comp1_date_month;
		this.report_comp1_date_day = report_comp1_date_day;
		this.report_comp1_date_year = report_comp1_date_year;
		this.report_comp1_source = report_comp1_source;
		this.report_comp1_contact_no = report_comp1_contact_no;
		this.report_comp1_location = report_comp1_location;
		this.report_comp1_area = report_comp1_area;
		this.report_comp1_base_price = report_comp1_base_price;
		this.report_comp1_price_sqm = report_comp1_price_sqm;
		this.report_comp1_discount_rate = report_comp1_discount_rate;
		this.report_comp1_discounts = report_comp1_discounts;
		this.report_comp1_selling_price = report_comp1_selling_price;
		this.report_comp1_rec_location = report_comp1_rec_location;
		this.report_comp1_rec_lot_size = report_comp1_rec_lot_size;
		this.report_comp1_rec_floor_area = report_comp1_rec_floor_area;
		this.report_comp1_rec_unit_condition = report_comp1_rec_unit_condition;
		this.report_comp1_rec_unit_features = report_comp1_rec_unit_features;
		this.report_comp1_rec_degree_of_devt = report_comp1_rec_degree_of_devt;
		this.report_comp1_rec_corner_influence = report_comp1_rec_corner_influence;
		this.report_comp1_concluded_adjustment = report_comp1_concluded_adjustment;
		this.report_comp1_adjusted_value = report_comp1_adjusted_value;
		this.report_comp2_date_month = report_comp2_date_month;
		this.report_comp2_date_day = report_comp2_date_day;
		this.report_comp2_date_year = report_comp2_date_year;
		this.report_comp2_source = report_comp2_source;
		this.report_comp2_contact_no = report_comp2_contact_no;
		this.report_comp2_location = report_comp2_location;
		this.report_comp2_area = report_comp2_area;
		this.report_comp2_base_price = report_comp2_base_price;
		this.report_comp2_price_sqm = report_comp2_price_sqm;
		this.report_comp2_discount_rate = report_comp2_discount_rate;
		this.report_comp2_discounts = report_comp2_discounts;
		this.report_comp2_selling_price = report_comp2_selling_price;
		this.report_comp2_rec_location = report_comp2_rec_location;
		this.report_comp2_rec_lot_size = report_comp2_rec_lot_size;
		this.report_comp2_rec_floor_area = report_comp2_rec_floor_area;
		this.report_comp2_rec_unit_condition = report_comp2_rec_unit_condition;
		this.report_comp2_rec_unit_features = report_comp2_rec_unit_features;
		this.report_comp2_rec_degree_of_devt = report_comp2_rec_degree_of_devt;
		this.report_comp2_rec_corner_influence = report_comp2_rec_corner_influence;
		this.report_comp2_concluded_adjustment = report_comp2_concluded_adjustment;
		this.report_comp2_adjusted_value = report_comp2_adjusted_value;
		this.report_comp3_date_month = report_comp3_date_month;
		this.report_comp3_date_day = report_comp3_date_day;
		this.report_comp3_date_year = report_comp3_date_year;
		this.report_comp3_source = report_comp3_source;
		this.report_comp3_contact_no = report_comp3_contact_no;
		this.report_comp3_location = report_comp3_location;
		this.report_comp3_area = report_comp3_area;
		this.report_comp3_base_price = report_comp3_base_price;
		this.report_comp3_price_sqm = report_comp3_price_sqm;
		this.report_comp3_discount_rate = report_comp3_discount_rate;
		this.report_comp3_discounts = report_comp3_discounts;
		this.report_comp3_selling_price = report_comp3_selling_price;
		this.report_comp3_rec_location = report_comp3_rec_location;
		this.report_comp3_rec_lot_size = report_comp3_rec_lot_size;
		this.report_comp3_rec_floor_area = report_comp3_rec_floor_area;
		this.report_comp3_rec_unit_condition = report_comp3_rec_unit_condition;
		this.report_comp3_rec_unit_features = report_comp3_rec_unit_features;
		this.report_comp3_rec_degree_of_devt = report_comp3_rec_degree_of_devt;
		this.report_comp3_rec_corner_influence = report_comp3_rec_corner_influence;
		this.report_comp3_concluded_adjustment = report_comp3_concluded_adjustment;
		this.report_comp3_adjusted_value = report_comp3_adjusted_value;
		this.report_comp4_date_month = report_comp4_date_month;
		this.report_comp4_date_day = report_comp4_date_day;
		this.report_comp4_date_year = report_comp4_date_year;
		this.report_comp4_source = report_comp4_source;
		this.report_comp4_contact_no = report_comp4_contact_no;
		this.report_comp4_location = report_comp4_location;
		this.report_comp4_area = report_comp4_area;
		this.report_comp4_base_price = report_comp4_base_price;
		this.report_comp4_price_sqm = report_comp4_price_sqm;
		this.report_comp4_discount_rate = report_comp4_discount_rate;
		this.report_comp4_discounts = report_comp4_discounts;
		this.report_comp4_selling_price = report_comp4_selling_price;
		this.report_comp4_rec_location = report_comp4_rec_location;
		this.report_comp4_rec_lot_size = report_comp4_rec_lot_size;
		this.report_comp4_rec_floor_area = report_comp4_rec_floor_area;
		this.report_comp4_rec_unit_condition = report_comp4_rec_unit_condition;
		this.report_comp4_rec_unit_features = report_comp4_rec_unit_features;
		this.report_comp4_rec_degree_of_devt = report_comp4_rec_degree_of_devt;
		this.report_comp4_rec_corner_influence = report_comp4_rec_corner_influence;
		this.report_comp4_concluded_adjustment = report_comp4_concluded_adjustment;
		this.report_comp4_adjusted_value = report_comp4_adjusted_value;
		this.report_comp5_date_month = report_comp5_date_month;
		this.report_comp5_date_day = report_comp5_date_day;
		this.report_comp5_date_year = report_comp5_date_year;
		this.report_comp5_source = report_comp5_source;
		this.report_comp5_contact_no = report_comp5_contact_no;
		this.report_comp5_location = report_comp5_location;
		this.report_comp5_area = report_comp5_area;
		this.report_comp5_base_price = report_comp5_base_price;
		this.report_comp5_price_sqm = report_comp5_price_sqm;
		this.report_comp5_discount_rate = report_comp5_discount_rate;
		this.report_comp5_discounts = report_comp5_discounts;
		this.report_comp5_selling_price = report_comp5_selling_price;
		this.report_comp5_rec_location = report_comp5_rec_location;
		this.report_comp5_rec_lot_size = report_comp5_rec_lot_size;
		this.report_comp5_rec_floor_area = report_comp5_rec_floor_area;
		this.report_comp5_rec_unit_condition = report_comp5_rec_unit_condition;
		this.report_comp5_rec_unit_features = report_comp5_rec_unit_features;
		this.report_comp5_rec_degree_of_devt = report_comp5_rec_degree_of_devt;
		this.report_comp5_rec_corner_influence = report_comp5_rec_corner_influence;
		this.report_comp5_concluded_adjustment = report_comp5_concluded_adjustment;
		this.report_comp5_adjusted_value = report_comp5_adjusted_value;
		this.report_comp_average = report_comp_average;
		this.report_comp_rounded_to = report_comp_rounded_to;
		this.report_zonal_location = report_zonal_location;
		this.report_zonal_lot_classification = report_zonal_lot_classification;
		this.report_zonal_value = report_zonal_value;
		this.report_value_total_area = report_value_total_area;
		this.report_value_total_deduction = report_value_total_deduction;
		this.report_value_total_net_area = report_value_total_net_area;
		this.report_value_total_landimp_value = report_value_total_landimp_value;
		this.report_imp_value_total_imp_value = report_imp_value_total_imp_value;
		this.report_final_value_total_appraised_value_land_imp = report_final_value_total_appraised_value_land_imp;
		this.report_final_value_recommended_deductions_desc = report_final_value_recommended_deductions_desc;
		this.report_final_value_recommended_deductions_rate = report_final_value_recommended_deductions_rate;
		this.report_final_value_recommended_deductions_other = report_final_value_recommended_deductions_other;
		this.report_final_value_recommended_deductions_amt = report_final_value_recommended_deductions_amt;
		this.report_final_value_net_appraised_value = report_final_value_net_appraised_value;
		this.report_final_value_quick_sale_value_rate = report_final_value_quick_sale_value_rate;
		this.report_final_value_quick_sale_value_amt = report_final_value_quick_sale_value_amt;
		this.report_factors_of_concern = report_factors_of_concern;
		this.report_suggested_corrective_actions = report_suggested_corrective_actions;
		this.report_remarks = report_remarks;
		this.report_requirements = report_requirements;
		this.report_time_inspected = report_time_inspected;
		this.report_comp1_remarks = report_comp1_remarks;
		this.report_comp2_remarks = report_comp2_remarks;
		this.report_comp3_remarks = report_comp3_remarks;
		this.report_comp4_remarks = report_comp4_remarks;
		this.report_comp5_remarks = report_comp5_remarks;
		this.report_comp1_lot_area = report_comp1_lot_area;
		this.report_comp2_lot_area = report_comp2_lot_area;
		this.report_comp3_lot_area = report_comp3_lot_area;
		this.report_comp4_lot_area = report_comp4_lot_area;
		this.report_comp5_lot_area = report_comp5_lot_area;
		this.report_concerns_minor_1 = report_concerns_minor_1;
		this.report_concerns_minor_2 = report_concerns_minor_2;
		this.report_concerns_minor_3 = report_concerns_minor_3;
		this.report_concerns_minor_others = report_concerns_minor_others;
		this.report_concerns_minor_others_desc = report_concerns_minor_others_desc;
		this.report_concerns_major_1 = report_concerns_major_1;
		this.report_concerns_major_2 = report_concerns_major_2;
		this.report_concerns_major_3 = report_concerns_major_3;
		this.report_concerns_major_4 = report_concerns_major_4;
		this.report_concerns_major_5 = report_concerns_major_5;
		this.report_concerns_major_6 = report_concerns_major_6;
		this.report_concerns_major_others = report_concerns_major_others;
		this.report_concerns_major_others_desc = report_concerns_major_others_desc;
		//Added from IAN
		this.report_prev_date = report_prev_date;
		this.report_prev_appraiser = report_prev_appraiser;
		this.report_prev_total_appraised_value = report_prev_total_appraised_value;
		this.report_townhouse_td_no=report_townhouse_td_no;
	}

	// constructor without int id
	public Townhouse_API(
			String record_id,
			String report_date_inspected_month,
			String report_date_inspected_day,
			String report_date_inspected_year,
			String report_id_association,
			String report_id_tax,
			String report_id_lra,
			String report_id_lot_config,
			String report_id_subd_map,
			String report_id_nbrhood_checking,
			String report_imp_street_name,
			String report_imp_street,
			String report_imp_sidewalk,
			String report_imp_curb,
			String report_imp_drainage,
			String report_imp_street_lights,
			String report_physical_corner_lot,
			String report_physical_non_corner_lot,
			String report_physical_perimeter_lot,
			String report_physical_intersected_lot,
			String report_physical_interior_with_row,
			String report_physical_landlocked,
			String report_lotclass_per_tax_dec,
			String report_lotclass_actual_usage,
			String report_lotclass_neighborhood,
			String report_lotclass_highest_best_use,
			String report_physical_shape,
			String report_physical_frontage,
			String report_physical_depth,
			String report_physical_road,
			String report_physical_elevation,
			String report_physical_terrain,
			String report_landmark_1,
			String report_landmark_2,
			String report_landmark_3,
			String report_landmark_4,
			String report_landmark_5,
			String report_distance_1,
			String report_distance_2,
			String report_distance_3,
			String report_distance_4,
			String report_distance_5,
			String report_util_electricity,
			String report_util_water,
			String report_util_telephone,
			String report_util_garbage,
			String report_trans_jeep,
			String report_trans_bus,
			String report_trans_taxi,
			String report_trans_tricycle,
			String report_faci_recreation,
			String report_faci_security,
			String report_faci_clubhouse,
			String report_faci_pool,
			String report_bound_right,
			String report_bound_left,
			String report_bound_rear,
			String report_bound_front,
			String report_comp1_date_month,
			String report_comp1_date_day,
			String report_comp1_date_year,
			String report_comp1_source,
			String report_comp1_contact_no,
			String report_comp1_location,
			String report_comp1_area,
			String report_comp1_base_price,
			String report_comp1_price_sqm,
			String report_comp1_discount_rate,
			String report_comp1_discounts,
			String report_comp1_selling_price,
			String report_comp1_rec_location,
			String report_comp1_rec_lot_size,
			String report_comp1_rec_floor_area,
			String report_comp1_rec_unit_condition,
			String report_comp1_rec_unit_features,
			String report_comp1_rec_degree_of_devt,
			String report_comp1_rec_corner_influence,
			String report_comp1_concluded_adjustment,
			String report_comp1_adjusted_value,
			String report_comp2_date_month,
			String report_comp2_date_day,
			String report_comp2_date_year,
			String report_comp2_source,
			String report_comp2_contact_no,
			String report_comp2_location,
			String report_comp2_area,
			String report_comp2_base_price,
			String report_comp2_price_sqm,
			String report_comp2_discount_rate,
			String report_comp2_discounts,
			String report_comp2_selling_price,
			String report_comp2_rec_location,
			String report_comp2_rec_lot_size,
			String report_comp2_rec_floor_area,
			String report_comp2_rec_unit_condition,
			String report_comp2_rec_unit_features,
			String report_comp2_rec_degree_of_devt,
			String report_comp2_rec_corner_influence,
			String report_comp2_concluded_adjustment,
			String report_comp2_adjusted_value,
			String report_comp3_date_month,
			String report_comp3_date_day,
			String report_comp3_date_year,
			String report_comp3_source,
			String report_comp3_contact_no,
			String report_comp3_location,
			String report_comp3_area,
			String report_comp3_base_price,
			String report_comp3_price_sqm,
			String report_comp3_discount_rate,
			String report_comp3_discounts,
			String report_comp3_selling_price,
			String report_comp3_rec_location,
			String report_comp3_rec_lot_size,
			String report_comp3_rec_floor_area,
			String report_comp3_rec_unit_condition,
			String report_comp3_rec_unit_features,
			String report_comp3_rec_degree_of_devt,
			String report_comp3_rec_corner_influence,
			String report_comp3_concluded_adjustment,
			String report_comp3_adjusted_value,
			String report_comp4_date_month,
			String report_comp4_date_day,
			String report_comp4_date_year,
			String report_comp4_source,
			String report_comp4_contact_no,
			String report_comp4_location,
			String report_comp4_area,
			String report_comp4_base_price,
			String report_comp4_price_sqm,
			String report_comp4_discount_rate,
			String report_comp4_discounts,
			String report_comp4_selling_price,
			String report_comp4_rec_location,
			String report_comp4_rec_lot_size,
			String report_comp4_rec_floor_area,
			String report_comp4_rec_unit_condition,
			String report_comp4_rec_unit_features,
			String report_comp4_rec_degree_of_devt,
			String report_comp4_rec_corner_influence,
			String report_comp4_concluded_adjustment,
			String report_comp4_adjusted_value,
			String report_comp5_date_month,
			String report_comp5_date_day,
			String report_comp5_date_year,
			String report_comp5_source,
			String report_comp5_contact_no,
			String report_comp5_location,
			String report_comp5_area,
			String report_comp5_base_price,
			String report_comp5_price_sqm,
			String report_comp5_discount_rate,
			String report_comp5_discounts,
			String report_comp5_selling_price,
			String report_comp5_rec_location,
			String report_comp5_rec_lot_size,
			String report_comp5_rec_floor_area,
			String report_comp5_rec_unit_condition,
			String report_comp5_rec_unit_features,
			String report_comp5_rec_degree_of_devt,
			String report_comp5_rec_corner_influence,
			String report_comp5_concluded_adjustment,
			String report_comp5_adjusted_value,
			String report_comp_average,
			String report_comp_rounded_to,
			String report_zonal_location,
			String report_zonal_lot_classification,
			String report_zonal_value,
			String report_value_total_area,
			String report_value_total_deduction,
			String report_value_total_net_area,
			String report_value_total_landimp_value,
			String report_imp_value_total_imp_value,
			String report_final_value_total_appraised_value_land_imp,
			String report_final_value_recommended_deductions_desc,
			String report_final_value_recommended_deductions_rate,
			String report_final_value_recommended_deductions_other,
			String report_final_value_recommended_deductions_amt,
			String report_final_value_net_appraised_value,
			String report_final_value_quick_sale_value_rate,
			String report_final_value_quick_sale_value_amt,
			String report_factors_of_concern,
			String report_suggested_corrective_actions,
			String report_remarks,
			String report_requirements,
			String report_time_inspected,
			String report_comp1_remarks,
			String report_comp2_remarks,
			String report_comp3_remarks,
			String report_comp4_remarks,
			String report_comp5_remarks,
			String report_comp1_lot_area,
			String report_comp2_lot_area,
			String report_comp3_lot_area,
			String report_comp4_lot_area,
			String report_comp5_lot_area,
			String report_concerns_minor_1,
			String report_concerns_minor_2,
			String report_concerns_minor_3,
			String report_concerns_minor_others,
			String report_concerns_minor_others_desc,
			String report_concerns_major_1,
			String report_concerns_major_2,
			String report_concerns_major_3,
			String report_concerns_major_4,
			String report_concerns_major_5,
			String report_concerns_major_6,
			String report_concerns_major_others,
			String report_concerns_major_others_desc,
			//Added from IAN
			String report_prev_date,
			String report_prev_appraiser,
			String report_prev_total_appraised_value,
			String report_townhouse_td_no) {
		this.record_id = record_id;
		this.report_date_inspected_month = report_date_inspected_month;
		this.report_date_inspected_day = report_date_inspected_day;
		this.report_date_inspected_year = report_date_inspected_year;
		this.report_id_association = report_id_association;
		this.report_id_tax = report_id_tax;
		this.report_id_lra = report_id_lra;
		this.report_id_lot_config = report_id_lot_config;
		this.report_id_subd_map = report_id_subd_map;
		this.report_id_nbrhood_checking = report_id_nbrhood_checking;
		this.report_imp_street_name = report_imp_street_name;
		this.report_imp_street = report_imp_street;
		this.report_imp_sidewalk = report_imp_sidewalk;
		this.report_imp_curb = report_imp_curb;
		this.report_imp_drainage = report_imp_drainage;
		this.report_imp_street_lights = report_imp_street_lights;
		this.report_physical_corner_lot = report_physical_corner_lot;
		this.report_physical_non_corner_lot = report_physical_non_corner_lot;
		this.report_physical_perimeter_lot = report_physical_perimeter_lot;
		this.report_physical_intersected_lot = report_physical_intersected_lot;
		this.report_physical_interior_with_row = report_physical_interior_with_row;
		this.report_physical_landlocked = report_physical_landlocked;
		this.report_lotclass_per_tax_dec = report_lotclass_per_tax_dec;
		this.report_lotclass_actual_usage = report_lotclass_actual_usage;
		this.report_lotclass_neighborhood = report_lotclass_neighborhood;
		this.report_lotclass_highest_best_use = report_lotclass_highest_best_use;
		this.report_physical_shape = report_physical_shape;
		this.report_physical_frontage = report_physical_frontage;
		this.report_physical_depth = report_physical_depth;
		this.report_physical_road = report_physical_road;
		this.report_physical_elevation = report_physical_elevation;
		this.report_physical_terrain = report_physical_terrain;
		this.report_landmark_1 = report_landmark_1;
		this.report_landmark_2 = report_landmark_2;
		this.report_landmark_3 = report_landmark_3;
		this.report_landmark_4 = report_landmark_4;
		this.report_landmark_5 = report_landmark_5;
		this.report_distance_1 = report_distance_1;
		this.report_distance_2 = report_distance_2;
		this.report_distance_3 = report_distance_3;
		this.report_distance_4 = report_distance_4;
		this.report_distance_5 = report_distance_5;
		this.report_util_electricity = report_util_electricity;
		this.report_util_water = report_util_water;
		this.report_util_telephone = report_util_telephone;
		this.report_util_garbage = report_util_garbage;
		this.report_trans_jeep = report_trans_jeep;
		this.report_trans_bus = report_trans_bus;
		this.report_trans_taxi = report_trans_taxi;
		this.report_trans_tricycle = report_trans_tricycle;
		this.report_faci_recreation = report_faci_recreation;
		this.report_faci_security = report_faci_security;
		this.report_faci_clubhouse = report_faci_clubhouse;
		this.report_faci_pool = report_faci_pool;
		this.report_bound_right = report_bound_right;
		this.report_bound_left = report_bound_left;
		this.report_bound_rear = report_bound_rear;
		this.report_bound_front = report_bound_front;		
		this.report_comp1_date_month = report_comp1_date_month;
		this.report_comp1_date_day = report_comp1_date_day;
		this.report_comp1_date_year = report_comp1_date_year;
		this.report_comp1_source = report_comp1_source;
		this.report_comp1_contact_no = report_comp1_contact_no;
		this.report_comp1_location = report_comp1_location;
		this.report_comp1_area = report_comp1_area;
		this.report_comp1_base_price = report_comp1_base_price;
		this.report_comp1_price_sqm = report_comp1_price_sqm;
		this.report_comp1_discount_rate = report_comp1_discount_rate;
		this.report_comp1_discounts = report_comp1_discounts;
		this.report_comp1_selling_price = report_comp1_selling_price;
		this.report_comp1_rec_location = report_comp1_rec_location;
		this.report_comp1_rec_lot_size = report_comp1_rec_lot_size;
		this.report_comp1_rec_floor_area = report_comp1_rec_floor_area;
		this.report_comp1_rec_unit_condition = report_comp1_rec_unit_condition;
		this.report_comp1_rec_unit_features = report_comp1_rec_unit_features;
		this.report_comp1_rec_degree_of_devt = report_comp1_rec_degree_of_devt;
		this.report_comp1_rec_corner_influence = report_comp1_rec_corner_influence;
		this.report_comp1_concluded_adjustment = report_comp1_concluded_adjustment;
		this.report_comp1_adjusted_value = report_comp1_adjusted_value;
		this.report_comp2_date_month = report_comp2_date_month;
		this.report_comp2_date_day = report_comp2_date_day;
		this.report_comp2_date_year = report_comp2_date_year;
		this.report_comp2_source = report_comp2_source;
		this.report_comp2_contact_no = report_comp2_contact_no;
		this.report_comp2_location = report_comp2_location;
		this.report_comp2_area = report_comp2_area;
		this.report_comp2_base_price = report_comp2_base_price;
		this.report_comp2_price_sqm = report_comp2_price_sqm;
		this.report_comp2_discount_rate = report_comp2_discount_rate;
		this.report_comp2_discounts = report_comp2_discounts;
		this.report_comp2_selling_price = report_comp2_selling_price;
		this.report_comp2_rec_location = report_comp2_rec_location;
		this.report_comp2_rec_lot_size = report_comp2_rec_lot_size;
		this.report_comp2_rec_floor_area = report_comp2_rec_floor_area;
		this.report_comp2_rec_unit_condition = report_comp2_rec_unit_condition;
		this.report_comp2_rec_unit_features = report_comp2_rec_unit_features;
		this.report_comp2_rec_degree_of_devt = report_comp2_rec_degree_of_devt;
		this.report_comp2_rec_corner_influence = report_comp2_rec_corner_influence;
		this.report_comp2_concluded_adjustment = report_comp2_concluded_adjustment;
		this.report_comp2_adjusted_value = report_comp2_adjusted_value;
		this.report_comp3_date_month = report_comp3_date_month;
		this.report_comp3_date_day = report_comp3_date_day;
		this.report_comp3_date_year = report_comp3_date_year;
		this.report_comp3_source = report_comp3_source;
		this.report_comp3_contact_no = report_comp3_contact_no;
		this.report_comp3_location = report_comp3_location;
		this.report_comp3_area = report_comp3_area;
		this.report_comp3_base_price = report_comp3_base_price;
		this.report_comp3_price_sqm = report_comp3_price_sqm;
		this.report_comp3_discount_rate = report_comp3_discount_rate;
		this.report_comp3_discounts = report_comp3_discounts;
		this.report_comp3_selling_price = report_comp3_selling_price;
		this.report_comp3_rec_location = report_comp3_rec_location;
		this.report_comp3_rec_lot_size = report_comp3_rec_lot_size;
		this.report_comp3_rec_floor_area = report_comp3_rec_floor_area;
		this.report_comp3_rec_unit_condition = report_comp3_rec_unit_condition;
		this.report_comp3_rec_unit_features = report_comp3_rec_unit_features;
		this.report_comp3_rec_degree_of_devt = report_comp3_rec_degree_of_devt;
		this.report_comp3_rec_corner_influence = report_comp3_rec_corner_influence;
		this.report_comp3_concluded_adjustment = report_comp3_concluded_adjustment;
		this.report_comp3_adjusted_value = report_comp3_adjusted_value;
		this.report_comp4_date_month = report_comp4_date_month;
		this.report_comp4_date_day = report_comp4_date_day;
		this.report_comp4_date_year = report_comp4_date_year;
		this.report_comp4_source = report_comp4_source;
		this.report_comp4_contact_no = report_comp4_contact_no;
		this.report_comp4_location = report_comp4_location;
		this.report_comp4_area = report_comp4_area;
		this.report_comp4_base_price = report_comp4_base_price;
		this.report_comp4_price_sqm = report_comp4_price_sqm;
		this.report_comp4_discount_rate = report_comp4_discount_rate;
		this.report_comp4_discounts = report_comp4_discounts;
		this.report_comp4_selling_price = report_comp4_selling_price;
		this.report_comp4_rec_location = report_comp4_rec_location;
		this.report_comp4_rec_lot_size = report_comp4_rec_lot_size;
		this.report_comp4_rec_floor_area = report_comp4_rec_floor_area;
		this.report_comp4_rec_unit_condition = report_comp4_rec_unit_condition;
		this.report_comp4_rec_unit_features = report_comp4_rec_unit_features;
		this.report_comp4_rec_degree_of_devt = report_comp4_rec_degree_of_devt;
		this.report_comp4_rec_corner_influence = report_comp4_rec_corner_influence;
		this.report_comp4_concluded_adjustment = report_comp4_concluded_adjustment;
		this.report_comp4_adjusted_value = report_comp4_adjusted_value;
		this.report_comp5_date_month = report_comp5_date_month;
		this.report_comp5_date_day = report_comp5_date_day;
		this.report_comp5_date_year = report_comp5_date_year;
		this.report_comp5_source = report_comp5_source;
		this.report_comp5_contact_no = report_comp5_contact_no;
		this.report_comp5_location = report_comp5_location;
		this.report_comp5_area = report_comp5_area;
		this.report_comp5_base_price = report_comp5_base_price;
		this.report_comp5_price_sqm = report_comp5_price_sqm;
		this.report_comp5_discount_rate = report_comp5_discount_rate;
		this.report_comp5_discounts = report_comp5_discounts;
		this.report_comp5_selling_price = report_comp5_selling_price;
		this.report_comp5_rec_location = report_comp5_rec_location;
		this.report_comp5_rec_lot_size = report_comp5_rec_lot_size;
		this.report_comp5_rec_floor_area = report_comp5_rec_floor_area;
		this.report_comp5_rec_unit_condition = report_comp5_rec_unit_condition;
		this.report_comp5_rec_unit_features = report_comp5_rec_unit_features;
		this.report_comp5_rec_degree_of_devt = report_comp5_rec_degree_of_devt;
		this.report_comp5_rec_corner_influence = report_comp5_rec_corner_influence;
		this.report_comp5_concluded_adjustment = report_comp5_concluded_adjustment;
		this.report_comp5_adjusted_value = report_comp5_adjusted_value;
		this.report_comp_average = report_comp_average;
		this.report_comp_rounded_to = report_comp_rounded_to;
		this.report_zonal_location = report_zonal_location;
		this.report_zonal_lot_classification = report_zonal_lot_classification;
		this.report_zonal_value = report_zonal_value;
		this.report_value_total_area = report_value_total_area;
		this.report_value_total_deduction = report_value_total_deduction;
		this.report_value_total_net_area = report_value_total_net_area;
		this.report_value_total_landimp_value = report_value_total_landimp_value;
		this.report_imp_value_total_imp_value = report_imp_value_total_imp_value;
		this.report_final_value_total_appraised_value_land_imp = report_final_value_total_appraised_value_land_imp;
		this.report_final_value_recommended_deductions_desc = report_final_value_recommended_deductions_desc;
		this.report_final_value_recommended_deductions_rate = report_final_value_recommended_deductions_rate;
		this.report_final_value_recommended_deductions_other = report_final_value_recommended_deductions_other;
		this.report_final_value_recommended_deductions_amt = report_final_value_recommended_deductions_amt;
		this.report_final_value_net_appraised_value = report_final_value_net_appraised_value;
		this.report_final_value_quick_sale_value_rate = report_final_value_quick_sale_value_rate;
		this.report_final_value_quick_sale_value_amt = report_final_value_quick_sale_value_amt;
		this.report_factors_of_concern = report_factors_of_concern;
		this.report_suggested_corrective_actions = report_suggested_corrective_actions;
		this.report_remarks = report_remarks;
		this.report_requirements = report_requirements;
		this.report_time_inspected = report_time_inspected;
		this.report_comp1_remarks = report_comp1_remarks;
		this.report_comp2_remarks = report_comp2_remarks;
		this.report_comp3_remarks = report_comp3_remarks;
		this.report_comp4_remarks = report_comp4_remarks;
		this.report_comp5_remarks = report_comp5_remarks;
		this.report_comp1_lot_area = report_comp1_lot_area;
		this.report_comp2_lot_area = report_comp2_lot_area;
		this.report_comp3_lot_area = report_comp3_lot_area;
		this.report_comp4_lot_area = report_comp4_lot_area;
		this.report_comp5_lot_area = report_comp5_lot_area;
		this.report_concerns_minor_1 = report_concerns_minor_1;
		this.report_concerns_minor_2 = report_concerns_minor_2;
		this.report_concerns_minor_3 = report_concerns_minor_3;
		this.report_concerns_minor_others = report_concerns_minor_others;
		this.report_concerns_minor_others_desc = report_concerns_minor_others_desc;
		this.report_concerns_major_1 = report_concerns_major_1;
		this.report_concerns_major_2 = report_concerns_major_2;
		this.report_concerns_major_3 = report_concerns_major_3;
		this.report_concerns_major_4 = report_concerns_major_4;
		this.report_concerns_major_5 = report_concerns_major_5;
		this.report_concerns_major_6 = report_concerns_major_6;
		this.report_concerns_major_others = report_concerns_major_others;
		this.report_concerns_major_others_desc = report_concerns_major_others_desc;
		//Added from IAN
		this.report_prev_date = report_prev_date;
		this.report_prev_appraiser = report_prev_appraiser;
		this.report_prev_total_appraised_value = report_prev_total_appraised_value;
		this.report_townhouse_td_no=report_townhouse_td_no;
	}

	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	public String getrecord_id() {
		return this.record_id;
	}

	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}
	
//getters
	public String getreport_date_inspected_month() {
	return this.report_date_inspected_month;
	}

	public String getreport_date_inspected_day() {
	return this.report_date_inspected_day;
	}

	public String getreport_date_inspected_year() {
	return this.report_date_inspected_year;
	}

	public String getreport_id_association() {
	return this.report_id_association;
	}

	public String getreport_id_tax() {
	return this.report_id_tax;
	}

	public String getreport_id_lra() {
	return this.report_id_lra;
	}

	public String getreport_id_lot_config() {
	return this.report_id_lot_config;
	}

	public String getreport_id_subd_map() {
	return this.report_id_subd_map;
	}

	public String getreport_id_nbrhood_checking() {
	return this.report_id_nbrhood_checking;
	}

	public String getreport_imp_street_name() {
	return this.report_imp_street_name;
	}

	public String getreport_imp_street() {
	return this.report_imp_street;
	}

	public String getreport_imp_sidewalk() {
	return this.report_imp_sidewalk;
	}

	public String getreport_imp_curb() {
	return this.report_imp_curb;
	}

	public String getreport_imp_drainage() {
	return this.report_imp_drainage;
	}

	public String getreport_imp_street_lights() {
	return this.report_imp_street_lights;
	}

	public String getreport_physical_corner_lot() {
	return this.report_physical_corner_lot;
	}

	public String getreport_physical_non_corner_lot() {
	return this.report_physical_non_corner_lot;
	}

	public String getreport_physical_perimeter_lot() {
	return this.report_physical_perimeter_lot;
	}

	public String getreport_physical_intersected_lot() {
	return this.report_physical_intersected_lot;
	}

	public String getreport_physical_interior_with_row() {
	return this.report_physical_interior_with_row;
	}

	public String getreport_physical_landlocked() {
	return this.report_physical_landlocked;
	}

	public String getreport_lotclass_per_tax_dec() {
	return this.report_lotclass_per_tax_dec;
	}

	public String getreport_lotclass_actual_usage() {
	return this.report_lotclass_actual_usage;
	}

	public String getreport_lotclass_neighborhood() {
	return this.report_lotclass_neighborhood;
	}

	public String getreport_lotclass_highest_best_use() {
	return this.report_lotclass_highest_best_use;
	}

	public String getreport_physical_shape() {
	return this.report_physical_shape;
	}

	public String getreport_physical_frontage() {
	return this.report_physical_frontage;
	}

	public String getreport_physical_depth() {
	return this.report_physical_depth;
	}

	public String getreport_physical_road() {
	return this.report_physical_road;
	}

	public String getreport_physical_elevation() {
	return this.report_physical_elevation;
	}

	public String getreport_physical_terrain() {
	return this.report_physical_terrain;
	}

	public String getreport_landmark_1() {
	return this.report_landmark_1;
	}

	public String getreport_landmark_2() {
	return this.report_landmark_2;
	}

	public String getreport_landmark_3() {
	return this.report_landmark_3;
	}

	public String getreport_landmark_4() {
	return this.report_landmark_4;
	}

	public String getreport_landmark_5() {
	return this.report_landmark_5;
	}

	public String getreport_distance_1() {
	return this.report_distance_1;
	}

	public String getreport_distance_2() {
	return this.report_distance_2;
	}

	public String getreport_distance_3() {
	return this.report_distance_3;
	}

	public String getreport_distance_4() {
	return this.report_distance_4;
	}

	public String getreport_distance_5() {
	return this.report_distance_5;
	}

	public String getreport_util_electricity() {
	return this.report_util_electricity;
	}

	public String getreport_util_water() {
	return this.report_util_water;
	}

	public String getreport_util_telephone() {
	return this.report_util_telephone;
	}

	public String getreport_util_garbage() {
	return this.report_util_garbage;
	}

	public String getreport_trans_jeep() {
	return this.report_trans_jeep;
	}

	public String getreport_trans_bus() {
	return this.report_trans_bus;
	}

	public String getreport_trans_taxi() {
	return this.report_trans_taxi;
	}

	public String getreport_trans_tricycle() {
	return this.report_trans_tricycle;
	}

	public String getreport_faci_recreation() {
	return this.report_faci_recreation;
	}

	public String getreport_faci_security() {
	return this.report_faci_security;
	}

	public String getreport_faci_clubhouse() {
	return this.report_faci_clubhouse;
	}

	public String getreport_faci_pool() {
	return this.report_faci_pool;
	}
	
	public String getreport_bound_right() {
	return this.report_bound_right;
	}
	public String getreport_bound_left() {
		return this.report_bound_left;
	}
	public String getreport_bound_rear() {
		return this.report_bound_rear;
	}
	public String getreport_bound_front() {
		return this.report_bound_front;
	}
	

	public String getreport_comp1_date_month() {
	return this.report_comp1_date_month;
	}

	public String getreport_comp1_date_day() {
	return this.report_comp1_date_day;
	}

	public String getreport_comp1_date_year() {
	return this.report_comp1_date_year;
	}

	public String getreport_comp1_source() {
	return this.report_comp1_source;
	}

	public String getreport_comp1_contact_no() {
	return this.report_comp1_contact_no;
	}

	public String getreport_comp1_location() {
	return this.report_comp1_location;
	}

	public String getreport_comp1_area() {
	return this.report_comp1_area;
	}

	public String getreport_comp1_base_price() {
	return this.report_comp1_base_price;
	}

	public String getreport_comp1_price_sqm() {
	return this.report_comp1_price_sqm;
	}

	public String getreport_comp1_discount_rate() {
	return this.report_comp1_discount_rate;
	}

	public String getreport_comp1_discounts() {
	return this.report_comp1_discounts;
	}

	public String getreport_comp1_selling_price() {
	return this.report_comp1_selling_price;
	}

	public String getreport_comp1_rec_location() {
	return this.report_comp1_rec_location;
	}

	public String getreport_comp1_rec_lot_size() {
	return this.report_comp1_rec_lot_size;
	}

	public String getreport_comp1_rec_floor_area() {
	return this.report_comp1_rec_floor_area;
	}

	public String getreport_comp1_rec_unit_condition() {
	return this.report_comp1_rec_unit_condition;
	}

	public String getreport_comp1_rec_unit_features() {
	return this.report_comp1_rec_unit_features;
	}

	public String getreport_comp1_rec_degree_of_devt() {
	return this.report_comp1_rec_degree_of_devt;
	}

	public String getreport_comp1_rec_corner_influence() {
		return this.report_comp1_rec_corner_influence;
	}
	public String getreport_comp1_concluded_adjustment() {
	return this.report_comp1_concluded_adjustment;
	}

	public String getreport_comp1_adjusted_value() {
	return this.report_comp1_adjusted_value;
	}

	public String getreport_comp2_date_month() {
	return this.report_comp2_date_month;
	}

	public String getreport_comp2_date_day() {
	return this.report_comp2_date_day;
	}

	public String getreport_comp2_date_year() {
	return this.report_comp2_date_year;
	}

	public String getreport_comp2_source() {
	return this.report_comp2_source;
	}

	public String getreport_comp2_contact_no() {
	return this.report_comp2_contact_no;
	}

	public String getreport_comp2_location() {
	return this.report_comp2_location;
	}

	public String getreport_comp2_area() {
	return this.report_comp2_area;
	}

	public String getreport_comp2_base_price() {
	return this.report_comp2_base_price;
	}

	public String getreport_comp2_price_sqm() {
	return this.report_comp2_price_sqm;
	}

	public String getreport_comp2_discount_rate() {
	return this.report_comp2_discount_rate;
	}

	public String getreport_comp2_discounts() {
	return this.report_comp2_discounts;
	}

	public String getreport_comp2_selling_price() {
	return this.report_comp2_selling_price;
	}

	public String getreport_comp2_rec_location() {
	return this.report_comp2_rec_location;
	}

	public String getreport_comp2_rec_lot_size() {
	return this.report_comp2_rec_lot_size;
	}

	public String getreport_comp2_rec_floor_area() {
	return this.report_comp2_rec_floor_area;
	}

	public String getreport_comp2_rec_unit_condition() {
	return this.report_comp2_rec_unit_condition;
	}

	public String getreport_comp2_rec_unit_features() {
	return this.report_comp2_rec_unit_features;
	}

	public String getreport_comp2_rec_degree_of_devt() {
	return this.report_comp2_rec_degree_of_devt;
	}

	public String getreport_comp2_rec_corner_influence() {
		return this.report_comp2_rec_corner_influence;
	}
	public String getreport_comp2_concluded_adjustment() {
	return this.report_comp2_concluded_adjustment;
	}

	public String getreport_comp2_adjusted_value() {
	return this.report_comp2_adjusted_value;
	}

	public String getreport_comp3_date_month() {
	return this.report_comp3_date_month;
	}

	public String getreport_comp3_date_day() {
	return this.report_comp3_date_day;
	}

	public String getreport_comp3_date_year() {
	return this.report_comp3_date_year;
	}

	public String getreport_comp3_source() {
	return this.report_comp3_source;
	}

	public String getreport_comp3_contact_no() {
	return this.report_comp3_contact_no;
	}

	public String getreport_comp3_location() {
	return this.report_comp3_location;
	}

	public String getreport_comp3_area() {
	return this.report_comp3_area;
	}

	public String getreport_comp3_base_price() {
	return this.report_comp3_base_price;
	}

	public String getreport_comp3_price_sqm() {
	return this.report_comp3_price_sqm;
	}

	public String getreport_comp3_discount_rate() {
	return this.report_comp3_discount_rate;
	}

	public String getreport_comp3_discounts() {
	return this.report_comp3_discounts;
	}

	public String getreport_comp3_selling_price() {
	return this.report_comp3_selling_price;
	}

	public String getreport_comp3_rec_location() {
	return this.report_comp3_rec_location;
	}

	public String getreport_comp3_rec_lot_size() {
	return this.report_comp3_rec_lot_size;
	}

	public String getreport_comp3_rec_floor_area() {
	return this.report_comp3_rec_floor_area;
	}

	public String getreport_comp3_rec_unit_condition() {
	return this.report_comp3_rec_unit_condition;
	}

	public String getreport_comp3_rec_unit_features() {
	return this.report_comp3_rec_unit_features;
	}

	public String getreport_comp3_rec_degree_of_devt() {
	return this.report_comp3_rec_degree_of_devt;
	}

	public String getreport_comp3_rec_corner_influence() {
		return this.report_comp3_rec_corner_influence;
	}
	public String getreport_comp3_concluded_adjustment() {
	return this.report_comp3_concluded_adjustment;
	}

	public String getreport_comp3_adjusted_value() {
	return this.report_comp3_adjusted_value;
	}

	public String getreport_comp4_date_month() {
	return this.report_comp4_date_month;
	}

	public String getreport_comp4_date_day() {
	return this.report_comp4_date_day;
	}

	public String getreport_comp4_date_year() {
	return this.report_comp4_date_year;
	}

	public String getreport_comp4_source() {
	return this.report_comp4_source;
	}

	public String getreport_comp4_contact_no() {
	return this.report_comp4_contact_no;
	}

	public String getreport_comp4_location() {
	return this.report_comp4_location;
	}

	public String getreport_comp4_area() {
	return this.report_comp4_area;
	}

	public String getreport_comp4_base_price() {
	return this.report_comp4_base_price;
	}

	public String getreport_comp4_price_sqm() {
	return this.report_comp4_price_sqm;
	}

	public String getreport_comp4_discount_rate() {
	return this.report_comp4_discount_rate;
	}

	public String getreport_comp4_discounts() {
	return this.report_comp4_discounts;
	}

	public String getreport_comp4_selling_price() {
	return this.report_comp4_selling_price;
	}

	public String getreport_comp4_rec_location() {
	return this.report_comp4_rec_location;
	}

	public String getreport_comp4_rec_lot_size() {
	return this.report_comp4_rec_lot_size;
	}

	public String getreport_comp4_rec_floor_area() {
	return this.report_comp4_rec_floor_area;
	}

	public String getreport_comp4_rec_unit_condition() {
	return this.report_comp4_rec_unit_condition;
	}

	public String getreport_comp4_rec_unit_features() {
	return this.report_comp4_rec_unit_features;
	}

	public String getreport_comp4_rec_degree_of_devt() {
	return this.report_comp4_rec_degree_of_devt;
	}

	public String getreport_comp4_rec_corner_influence() {
		return this.report_comp4_rec_corner_influence;
	}
	public String getreport_comp4_concluded_adjustment() {
	return this.report_comp4_concluded_adjustment;
	}

	public String getreport_comp4_adjusted_value() {
	return this.report_comp4_adjusted_value;
	}

	public String getreport_comp5_date_month() {
	return this.report_comp5_date_month;
	}

	public String getreport_comp5_date_day() {
	return this.report_comp5_date_day;
	}

	public String getreport_comp5_date_year() {
	return this.report_comp5_date_year;
	}

	public String getreport_comp5_source() {
	return this.report_comp5_source;
	}

	public String getreport_comp5_contact_no() {
	return this.report_comp5_contact_no;
	}

	public String getreport_comp5_location() {
	return this.report_comp5_location;
	}

	public String getreport_comp5_area() {
	return this.report_comp5_area;
	}

	public String getreport_comp5_base_price() {
	return this.report_comp5_base_price;
	}

	public String getreport_comp5_price_sqm() {
	return this.report_comp5_price_sqm;
	}

	public String getreport_comp5_discount_rate() {
	return this.report_comp5_discount_rate;
	}

	public String getreport_comp5_discounts() {
	return this.report_comp5_discounts;
	}

	public String getreport_comp5_selling_price() {
	return this.report_comp5_selling_price;
	}

	public String getreport_comp5_rec_location() {
	return this.report_comp5_rec_location;
	}

	public String getreport_comp5_rec_lot_size() {
	return this.report_comp5_rec_lot_size;
	}

	public String getreport_comp5_rec_floor_area() {
	return this.report_comp5_rec_floor_area;
	}

	public String getreport_comp5_rec_unit_condition() {
	return this.report_comp5_rec_unit_condition;
	}

	public String getreport_comp5_rec_unit_features() {
	return this.report_comp5_rec_unit_features;
	}

	public String getreport_comp5_rec_degree_of_devt() {
	return this.report_comp5_rec_degree_of_devt;
	}

	public String getreport_comp5_rec_corner_influence() {
		return this.report_comp5_rec_corner_influence;
	}
	public String getreport_comp5_concluded_adjustment() {
	return this.report_comp5_concluded_adjustment;
	}

	public String getreport_comp5_adjusted_value() {
	return this.report_comp5_adjusted_value;
	}

	public String getreport_comp_average() {
	return this.report_comp_average;
	}

	public String getreport_comp_rounded_to() {
	return this.report_comp_rounded_to;
	}

	public String getreport_zonal_location() {
	return this.report_zonal_location;
	}

	public String getreport_zonal_lot_classification() {
	return this.report_zonal_lot_classification;
	}

	public String getreport_zonal_value() {
	return this.report_zonal_value;
	}

	public String getreport_value_total_area() {
	return this.report_value_total_area;
	}

	public String getreport_value_total_deduction() {
	return this.report_value_total_deduction;
	}

	public String getreport_value_total_net_area() {
	return this.report_value_total_net_area;
	}

	public String getreport_value_total_landimp_value() {
	return this.report_value_total_landimp_value;
	}

	public String getreport_imp_value_total_imp_value() {
	return this.report_imp_value_total_imp_value;
	}

	public String getreport_final_value_total_appraised_value_land_imp() {
	return this.report_final_value_total_appraised_value_land_imp;
	}

	public String getreport_final_value_recommended_deductions_desc() {
	return this.report_final_value_recommended_deductions_desc;
	}

	public String getreport_final_value_recommended_deductions_rate() {
	return this.report_final_value_recommended_deductions_rate;
	}
	public String getreport_final_value_recommended_deductions_other() {
		return this.report_final_value_recommended_deductions_other;
	}

	public String getreport_final_value_recommended_deductions_amt() {
	return this.report_final_value_recommended_deductions_amt;
	}

	public String getreport_final_value_net_appraised_value() {
	return this.report_final_value_net_appraised_value;
	}

	public String getreport_final_value_quick_sale_value_rate() {
	return this.report_final_value_quick_sale_value_rate;
	}

	public String getreport_final_value_quick_sale_value_amt() {
	return this.report_final_value_quick_sale_value_amt;
	}

	public String getreport_factors_of_concern() {
	return this.report_factors_of_concern;
	}

	public String getreport_suggested_corrective_actions() {
	return this.report_suggested_corrective_actions;
	}

	public String getreport_remarks() {
	return this.report_remarks;
	}

	public String getreport_requirements() {
	return this.report_requirements;
	}
	
	public String getreport_time_inspected() {
		return this.report_time_inspected;
	}
	
	public String getreport_comp1_remarks() {
		return this.report_comp1_remarks;
	}
	
	public String getreport_comp2_remarks() {
		return this.report_comp2_remarks;
	}
	
	public String getreport_comp3_remarks() {
		return this.report_comp3_remarks;
	}
	
	public String getreport_comp4_remarks() {
		return this.report_comp4_remarks;
	}
	
	public String getreport_comp5_remarks() {
		return this.report_comp5_remarks;
	}
	
	public String getreport_comp1_lot_area() {
		return this.report_comp1_lot_area;
	}
	public String getreport_comp2_lot_area() {
		return this.report_comp2_lot_area;
	}
	public String getreport_comp3_lot_area() {
		return this.report_comp3_lot_area;
	}
	public String getreport_comp4_lot_area() {
		return this.report_comp4_lot_area;
	}
	public String getreport_comp5_lot_area() {
		return this.report_comp5_lot_area;
	}
	
	public String getreport_concerns_minor_1() {
		return this.report_concerns_minor_1;
	}
	
	public String getreport_concerns_minor_2() {
		return this.report_concerns_minor_2;
	}
	
	public String getreport_concerns_minor_3() {
		return this.report_concerns_minor_3;
	}
	
	public String getreport_concerns_minor_others() {
		return this.report_concerns_minor_others;
	}
	
	public String getreport_concerns_minor_others_desc() {
		return this.report_concerns_minor_others_desc;
	}
	
	public String getreport_concerns_major_1() {
		return this.report_concerns_major_1;
	}
	
	public String getreport_concerns_major_2() {
		return this.report_concerns_major_2;
	}
	
	public String getreport_concerns_major_3() {
		return this.report_concerns_major_3;
	}
	
	public String getreport_concerns_major_4() {
		return this.report_concerns_major_4;
	}
	
	public String getreport_concerns_major_5() {
		return this.report_concerns_major_5;
	}
	
	public String getreport_concerns_major_6() {
		return this.report_concerns_major_6;
	}
	
	public String getreport_concerns_major_others() {
		return this.report_concerns_major_others;
	}
	
	public String getreport_concerns_major_others_desc() {
		return this.report_concerns_major_others_desc;
	}

	//Added from IAN
	public String getreport_prev_date() {
		return this.report_prev_date;
	}
	public String getreport_prev_appraiser() {
		return this.report_prev_appraiser;
	}
	public String getreport_prev_total_appraised_value() {
		return this.report_prev_total_appraised_value;
	}

	public String getreport_townhouse_td_no() {
		return this.report_townhouse_td_no;
	}
//setters
	public void setreport_date_inspected_month(String report_date_inspected_month) {
	this.report_date_inspected_month = report_date_inspected_month;
	}

	public void setreport_date_inspected_day(String report_date_inspected_day) {
	this.report_date_inspected_day = report_date_inspected_day;
	}

	public void setreport_date_inspected_year(String report_date_inspected_year) {
	this.report_date_inspected_year = report_date_inspected_year;
	}

	public void setreport_id_association(String report_id_association) {
	this.report_id_association = report_id_association;
	}

	public void setreport_id_tax(String report_id_tax) {
	this.report_id_tax = report_id_tax;
	}

	public void setreport_id_lra(String report_id_lra) {
	this.report_id_lra = report_id_lra;
	}

	public void setreport_id_lot_config(String report_id_lot_config) {
	this.report_id_lot_config = report_id_lot_config;
	}

	public void setreport_id_subd_map(String report_id_subd_map) {
	this.report_id_subd_map = report_id_subd_map;
	}

	public void setreport_id_nbrhood_checking(String report_id_nbrhood_checking) {
	this.report_id_nbrhood_checking = report_id_nbrhood_checking;
	}

	public void setreport_imp_street_name(String report_imp_street_name) {
	this.report_imp_street_name = report_imp_street_name;
	}

	public void setreport_imp_street(String report_imp_street) {
	this.report_imp_street = report_imp_street;
	}

	public void setreport_imp_sidewalk(String report_imp_sidewalk) {
	this.report_imp_sidewalk = report_imp_sidewalk;
	}

	public void setreport_imp_curb(String report_imp_curb) {
	this.report_imp_curb = report_imp_curb;
	}

	public void setreport_imp_drainage(String report_imp_drainage) {
	this.report_imp_drainage = report_imp_drainage;
	}

	public void setreport_imp_street_lights(String report_imp_street_lights) {
	this.report_imp_street_lights = report_imp_street_lights;
	}

	public void setreport_physical_corner_lot(String report_physical_corner_lot) {
	this.report_physical_corner_lot = report_physical_corner_lot;
	}

	public void setreport_physical_non_corner_lot(String report_physical_non_corner_lot) {
	this.report_physical_non_corner_lot = report_physical_non_corner_lot;
	}

	public void setreport_physical_perimeter_lot(String report_physical_perimeter_lot) {
	this.report_physical_perimeter_lot = report_physical_perimeter_lot;
	}

	public void setreport_physical_intersected_lot(String report_physical_intersected_lot) {
	this.report_physical_intersected_lot = report_physical_intersected_lot;
	}

	public void setreport_physical_interior_with_row(String report_physical_interior_with_row) {
	this.report_physical_interior_with_row = report_physical_interior_with_row;
	}

	public void setreport_physical_landlocked(String report_physical_landlocked) {
	this.report_physical_landlocked = report_physical_landlocked;
	}

	public void setreport_lotclass_per_tax_dec(String report_lotclass_per_tax_dec) {
	this.report_lotclass_per_tax_dec = report_lotclass_per_tax_dec;
	}

	public void setreport_lotclass_actual_usage(String report_lotclass_actual_usage) {
	this.report_lotclass_actual_usage = report_lotclass_actual_usage;
	}

	public void setreport_lotclass_neighborhood(String report_lotclass_neighborhood) {
	this.report_lotclass_neighborhood = report_lotclass_neighborhood;
	}

	public void setreport_lotclass_highest_best_use(String report_lotclass_highest_best_use) {
	this.report_lotclass_highest_best_use = report_lotclass_highest_best_use;
	}

	public void setreport_physical_shape(String report_physical_shape) {
	this.report_physical_shape = report_physical_shape;
	}

	public void setreport_physical_frontage(String report_physical_frontage) {
	this.report_physical_frontage = report_physical_frontage;
	}

	public void setreport_physical_depth(String report_physical_depth) {
	this.report_physical_depth = report_physical_depth;
	}

	public void setreport_physical_road(String report_physical_road) {
	this.report_physical_road = report_physical_road;
	}

	public void setreport_physical_elevation(String report_physical_elevation) {
	this.report_physical_elevation = report_physical_elevation;
	}

	public void setreport_physical_terrain(String report_physical_terrain) {
	this.report_physical_terrain = report_physical_terrain;
	}

	public void setreport_landmark_1(String report_landmark_1) {
	this.report_landmark_1 = report_landmark_1;
	}

	public void setreport_landmark_2(String report_landmark_2) {
	this.report_landmark_2 = report_landmark_2;
	}

	public void setreport_landmark_3(String report_landmark_3) {
	this.report_landmark_3 = report_landmark_3;
	}

	public void setreport_landmark_4(String report_landmark_4) {
	this.report_landmark_4 = report_landmark_4;
	}

	public void setreport_landmark_5(String report_landmark_5) {
	this.report_landmark_5 = report_landmark_5;
	}

	public void setreport_distance_1(String report_distance_1) {
	this.report_distance_1 = report_distance_1;
	}

	public void setreport_distance_2(String report_distance_2) {
	this.report_distance_2 = report_distance_2;
	}

	public void setreport_distance_3(String report_distance_3) {
	this.report_distance_3 = report_distance_3;
	}

	public void setreport_distance_4(String report_distance_4) {
	this.report_distance_4 = report_distance_4;
	}

	public void setreport_distance_5(String report_distance_5) {
	this.report_distance_5 = report_distance_5;
	}

	public void setreport_util_electricity(String report_util_electricity) {
	this.report_util_electricity = report_util_electricity;
	}

	public void setreport_util_water(String report_util_water) {
	this.report_util_water = report_util_water;
	}

	public void setreport_util_telephone(String report_util_telephone) {
	this.report_util_telephone = report_util_telephone;
	}

	public void setreport_util_garbage(String report_util_garbage) {
	this.report_util_garbage = report_util_garbage;
	}

	public void setreport_trans_jeep(String report_trans_jeep) {
	this.report_trans_jeep = report_trans_jeep;
	}

	public void setreport_trans_bus(String report_trans_bus) {
	this.report_trans_bus = report_trans_bus;
	}

	public void setreport_trans_taxi(String report_trans_taxi) {
	this.report_trans_taxi = report_trans_taxi;
	}

	public void setreport_trans_tricycle(String report_trans_tricycle) {
	this.report_trans_tricycle = report_trans_tricycle;
	}

	public void setreport_faci_recreation(String report_faci_recreation) {
	this.report_faci_recreation = report_faci_recreation;
	}

	public void setreport_faci_security(String report_faci_security) {
	this.report_faci_security = report_faci_security;
	}

	public void setreport_faci_clubhouse(String report_faci_clubhouse) {
	this.report_faci_clubhouse = report_faci_clubhouse;
	}

	public void setreport_faci_pool(String report_faci_pool) {
	this.report_faci_pool = report_faci_pool;
	}
	
	public void setreport_bound_right(String report_bound_right) {
	this.report_bound_right = report_bound_right;
	}
	public void setreport_bound_left(String report_bound_left) {
		this.report_bound_left = report_bound_left;
	}
	public void setreport_bound_rear(String report_bound_rear) {
		this.report_bound_rear = report_bound_rear;
	}
	public void setreport_bound_front(String report_bound_front) {
		this.report_bound_front = report_bound_front;
	}

	public void setreport_comp1_date_month(String report_comp1_date_month) {
	this.report_comp1_date_month = report_comp1_date_month;
	}

	public void setreport_comp1_date_day(String report_comp1_date_day) {
	this.report_comp1_date_day = report_comp1_date_day;
	}

	public void setreport_comp1_date_year(String report_comp1_date_year) {
	this.report_comp1_date_year = report_comp1_date_year;
	}

	public void setreport_comp1_source(String report_comp1_source) {
	this.report_comp1_source = report_comp1_source;
	}

	public void setreport_comp1_contact_no(String report_comp1_contact_no) {
	this.report_comp1_contact_no = report_comp1_contact_no;
	}

	public void setreport_comp1_location(String report_comp1_location) {
	this.report_comp1_location = report_comp1_location;
	}

	public void setreport_comp1_area(String report_comp1_area) {
	this.report_comp1_area = report_comp1_area;
	}

	public void setreport_comp1_base_price(String report_comp1_base_price) {
	this.report_comp1_base_price = report_comp1_base_price;
	}

	public void setreport_comp1_price_sqm(String report_comp1_price_sqm) {
	this.report_comp1_price_sqm = report_comp1_price_sqm;
	}

	public void setreport_comp1_discount_rate(String report_comp1_discount_rate) {
	this.report_comp1_discount_rate = report_comp1_discount_rate;
	}

	public void setreport_comp1_discounts(String report_comp1_discounts) {
	this.report_comp1_discounts = report_comp1_discounts;
	}

	public void setreport_comp1_selling_price(String report_comp1_selling_price) {
	this.report_comp1_selling_price = report_comp1_selling_price;
	}

	public void setreport_comp1_rec_location(String report_comp1_rec_location) {
	this.report_comp1_rec_location = report_comp1_rec_location;
	}

	public void setreport_comp1_rec_lot_size(String report_comp1_rec_lot_size) {
	this.report_comp1_rec_lot_size = report_comp1_rec_lot_size;
	}

	public void setreport_comp1_rec_floor_area(String report_comp1_rec_floor_area) {
	this.report_comp1_rec_floor_area = report_comp1_rec_floor_area;
	}

	public void setreport_comp1_rec_unit_condition(String report_comp1_rec_unit_condition) {
	this.report_comp1_rec_unit_condition = report_comp1_rec_unit_condition;
	}

	public void setreport_comp1_rec_unit_features(String report_comp1_rec_unit_features) {
	this.report_comp1_rec_unit_features = report_comp1_rec_unit_features;
	}

	public void setreport_comp1_rec_degree_of_devt(String report_comp1_rec_degree_of_devt) {
	this.report_comp1_rec_degree_of_devt = report_comp1_rec_degree_of_devt;
	}

	public void setreport_comp1_rec_corner_influence(String report_comp1_rec_corner_influence) {
		this.report_comp1_rec_corner_influence = report_comp1_rec_corner_influence;
	}

	public void setreport_comp1_concluded_adjustment(String report_comp1_concluded_adjustment) {
	this.report_comp1_concluded_adjustment = report_comp1_concluded_adjustment;
	}

	public void setreport_comp1_adjusted_value(String report_comp1_adjusted_value) {
	this.report_comp1_adjusted_value = report_comp1_adjusted_value;
	}

	public void setreport_comp2_date_month(String report_comp2_date_month) {
	this.report_comp2_date_month = report_comp2_date_month;
	}

	public void setreport_comp2_date_day(String report_comp2_date_day) {
	this.report_comp2_date_day = report_comp2_date_day;
	}

	public void setreport_comp2_date_year(String report_comp2_date_year) {
	this.report_comp2_date_year = report_comp2_date_year;
	}

	public void setreport_comp2_source(String report_comp2_source) {
	this.report_comp2_source = report_comp2_source;
	}

	public void setreport_comp2_contact_no(String report_comp2_contact_no) {
	this.report_comp2_contact_no = report_comp2_contact_no;
	}

	public void setreport_comp2_location(String report_comp2_location) {
	this.report_comp2_location = report_comp2_location;
	}

	public void setreport_comp2_area(String report_comp2_area) {
	this.report_comp2_area = report_comp2_area;
	}

	public void setreport_comp2_base_price(String report_comp2_base_price) {
	this.report_comp2_base_price = report_comp2_base_price;
	}

	public void setreport_comp2_price_sqm(String report_comp2_price_sqm) {
	this.report_comp2_price_sqm = report_comp2_price_sqm;
	}

	public void setreport_comp2_discount_rate(String report_comp2_discount_rate) {
	this.report_comp2_discount_rate = report_comp2_discount_rate;
	}

	public void setreport_comp2_discounts(String report_comp2_discounts) {
	this.report_comp2_discounts = report_comp2_discounts;
	}

	public void setreport_comp2_selling_price(String report_comp2_selling_price) {
	this.report_comp2_selling_price = report_comp2_selling_price;
	}

	public void setreport_comp2_rec_location(String report_comp2_rec_location) {
	this.report_comp2_rec_location = report_comp2_rec_location;
	}

	public void setreport_comp2_rec_lot_size(String report_comp2_rec_lot_size) {
	this.report_comp2_rec_lot_size = report_comp2_rec_lot_size;
	}

	public void setreport_comp2_rec_floor_area(String report_comp2_rec_floor_area) {
	this.report_comp2_rec_floor_area = report_comp2_rec_floor_area;
	}

	public void setreport_comp2_rec_unit_condition(String report_comp2_rec_unit_condition) {
	this.report_comp2_rec_unit_condition = report_comp2_rec_unit_condition;
	}

	public void setreport_comp2_rec_unit_features(String report_comp2_rec_unit_features) {
	this.report_comp2_rec_unit_features = report_comp2_rec_unit_features;
	}

	public void setreport_comp2_rec_degree_of_devt(String report_comp2_rec_degree_of_devt) {
	this.report_comp2_rec_degree_of_devt = report_comp2_rec_degree_of_devt;
	}

	public void setreport_comp2_rec_corner_influence(String report_comp2_rec_corner_influence) {
		this.report_comp2_rec_corner_influence = report_comp2_rec_corner_influence;
	}

	public void setreport_comp2_concluded_adjustment(String report_comp2_concluded_adjustment) {
	this.report_comp2_concluded_adjustment = report_comp2_concluded_adjustment;
	}

	public void setreport_comp2_adjusted_value(String report_comp2_adjusted_value) {
	this.report_comp2_adjusted_value = report_comp2_adjusted_value;
	}

	public void setreport_comp3_date_month(String report_comp3_date_month) {
	this.report_comp3_date_month = report_comp3_date_month;
	}

	public void setreport_comp3_date_day(String report_comp3_date_day) {
	this.report_comp3_date_day = report_comp3_date_day;
	}

	public void setreport_comp3_date_year(String report_comp3_date_year) {
	this.report_comp3_date_year = report_comp3_date_year;
	}

	public void setreport_comp3_source(String report_comp3_source) {
	this.report_comp3_source = report_comp3_source;
	}

	public void setreport_comp3_contact_no(String report_comp3_contact_no) {
	this.report_comp3_contact_no = report_comp3_contact_no;
	}

	public void setreport_comp3_location(String report_comp3_location) {
	this.report_comp3_location = report_comp3_location;
	}

	public void setreport_comp3_area(String report_comp3_area) {
	this.report_comp3_area = report_comp3_area;
	}

	public void setreport_comp3_base_price(String report_comp3_base_price) {
	this.report_comp3_base_price = report_comp3_base_price;
	}

	public void setreport_comp3_price_sqm(String report_comp3_price_sqm) {
	this.report_comp3_price_sqm = report_comp3_price_sqm;
	}

	public void setreport_comp3_discount_rate(String report_comp3_discount_rate) {
	this.report_comp3_discount_rate = report_comp3_discount_rate;
	}

	public void setreport_comp3_discounts(String report_comp3_discounts) {
	this.report_comp3_discounts = report_comp3_discounts;
	}

	public void setreport_comp3_selling_price(String report_comp3_selling_price) {
	this.report_comp3_selling_price = report_comp3_selling_price;
	}

	public void setreport_comp3_rec_location(String report_comp3_rec_location) {
	this.report_comp3_rec_location = report_comp3_rec_location;
	}

	public void setreport_comp3_rec_lot_size(String report_comp3_rec_lot_size) {
	this.report_comp3_rec_lot_size = report_comp3_rec_lot_size;
	}

	public void setreport_comp3_rec_floor_area(String report_comp3_rec_floor_area) {
	this.report_comp3_rec_floor_area = report_comp3_rec_floor_area;
	}

	public void setreport_comp3_rec_unit_condition(String report_comp3_rec_unit_condition) {
	this.report_comp3_rec_unit_condition = report_comp3_rec_unit_condition;
	}

	public void setreport_comp3_rec_unit_features(String report_comp3_rec_unit_features) {
	this.report_comp3_rec_unit_features = report_comp3_rec_unit_features;
	}

	public void setreport_comp3_rec_degree_of_devt(String report_comp3_rec_degree_of_devt) {
	this.report_comp3_rec_degree_of_devt = report_comp3_rec_degree_of_devt;
	}

	public void setreport_comp3_rec_corner_influence(String report_comp3_rec_corner_influence) {
		this.report_comp3_rec_corner_influence = report_comp3_rec_corner_influence;
	}

	public void setreport_comp3_concluded_adjustment(String report_comp3_concluded_adjustment) {
	this.report_comp3_concluded_adjustment = report_comp3_concluded_adjustment;
	}

	public void setreport_comp3_adjusted_value(String report_comp3_adjusted_value) {
	this.report_comp3_adjusted_value = report_comp3_adjusted_value;
	}

	public void setreport_comp4_date_month(String report_comp4_date_month) {
	this.report_comp4_date_month = report_comp4_date_month;
	}

	public void setreport_comp4_date_day(String report_comp4_date_day) {
	this.report_comp4_date_day = report_comp4_date_day;
	}

	public void setreport_comp4_date_year(String report_comp4_date_year) {
	this.report_comp4_date_year = report_comp4_date_year;
	}

	public void setreport_comp4_source(String report_comp4_source) {
	this.report_comp4_source = report_comp4_source;
	}

	public void setreport_comp4_contact_no(String report_comp4_contact_no) {
	this.report_comp4_contact_no = report_comp4_contact_no;
	}

	public void setreport_comp4_location(String report_comp4_location) {
	this.report_comp4_location = report_comp4_location;
	}

	public void setreport_comp4_area(String report_comp4_area) {
	this.report_comp4_area = report_comp4_area;
	}

	public void setreport_comp4_base_price(String report_comp4_base_price) {
	this.report_comp4_base_price = report_comp4_base_price;
	}

	public void setreport_comp4_price_sqm(String report_comp4_price_sqm) {
	this.report_comp4_price_sqm = report_comp4_price_sqm;
	}

	public void setreport_comp4_discount_rate(String report_comp4_discount_rate) {
	this.report_comp4_discount_rate = report_comp4_discount_rate;
	}

	public void setreport_comp4_discounts(String report_comp4_discounts) {
	this.report_comp4_discounts = report_comp4_discounts;
	}

	public void setreport_comp4_selling_price(String report_comp4_selling_price) {
	this.report_comp4_selling_price = report_comp4_selling_price;
	}

	public void setreport_comp4_rec_location(String report_comp4_rec_location) {
	this.report_comp4_rec_location = report_comp4_rec_location;
	}

	public void setreport_comp4_rec_lot_size(String report_comp4_rec_lot_size) {
	this.report_comp4_rec_lot_size = report_comp4_rec_lot_size;
	}

	public void setreport_comp4_rec_floor_area(String report_comp4_rec_floor_area) {
	this.report_comp4_rec_floor_area = report_comp4_rec_floor_area;
	}

	public void setreport_comp4_rec_unit_condition(String report_comp4_rec_unit_condition) {
	this.report_comp4_rec_unit_condition = report_comp4_rec_unit_condition;
	}

	public void setreport_comp4_rec_unit_features(String report_comp4_rec_unit_features) {
	this.report_comp4_rec_unit_features = report_comp4_rec_unit_features;
	}

	public void setreport_comp4_rec_degree_of_devt(String report_comp4_rec_degree_of_devt) {
	this.report_comp4_rec_degree_of_devt = report_comp4_rec_degree_of_devt;
	}

	public void setreport_comp4_rec_corner_influence(String report_comp4_rec_corner_influence) {
		this.report_comp4_rec_corner_influence = report_comp4_rec_corner_influence;
	}

	public void setreport_comp4_concluded_adjustment(String report_comp4_concluded_adjustment) {
	this.report_comp4_concluded_adjustment = report_comp4_concluded_adjustment;
	}

	public void setreport_comp4_adjusted_value(String report_comp4_adjusted_value) {
	this.report_comp4_adjusted_value = report_comp4_adjusted_value;
	}

	public void setreport_comp5_date_month(String report_comp5_date_month) {
	this.report_comp5_date_month = report_comp5_date_month;
	}

	public void setreport_comp5_date_day(String report_comp5_date_day) {
	this.report_comp5_date_day = report_comp5_date_day;
	}

	public void setreport_comp5_date_year(String report_comp5_date_year) {
	this.report_comp5_date_year = report_comp5_date_year;
	}

	public void setreport_comp5_source(String report_comp5_source) {
	this.report_comp5_source = report_comp5_source;
	}

	public void setreport_comp5_contact_no(String report_comp5_contact_no) {
	this.report_comp5_contact_no = report_comp5_contact_no;
	}

	public void setreport_comp5_location(String report_comp5_location) {
	this.report_comp5_location = report_comp5_location;
	}

	public void setreport_comp5_area(String report_comp5_area) {
	this.report_comp5_area = report_comp5_area;
	}

	public void setreport_comp5_base_price(String report_comp5_base_price) {
	this.report_comp5_base_price = report_comp5_base_price;
	}

	public void setreport_comp5_price_sqm(String report_comp5_price_sqm) {
	this.report_comp5_price_sqm = report_comp5_price_sqm;
	}

	public void setreport_comp5_discount_rate(String report_comp5_discount_rate) {
	this.report_comp5_discount_rate = report_comp5_discount_rate;
	}

	public void setreport_comp5_discounts(String report_comp5_discounts) {
	this.report_comp5_discounts = report_comp5_discounts;
	}

	public void setreport_comp5_selling_price(String report_comp5_selling_price) {
	this.report_comp5_selling_price = report_comp5_selling_price;
	}

	public void setreport_comp5_rec_location(String report_comp5_rec_location) {
	this.report_comp5_rec_location = report_comp5_rec_location;
	}

	public void setreport_comp5_rec_lot_size(String report_comp5_rec_lot_size) {
	this.report_comp5_rec_lot_size = report_comp5_rec_lot_size;
	}

	public void setreport_comp5_rec_floor_area(String report_comp5_rec_floor_area) {
	this.report_comp5_rec_floor_area = report_comp5_rec_floor_area;
	}

	public void setreport_comp5_rec_unit_condition(String report_comp5_rec_unit_condition) {
	this.report_comp5_rec_unit_condition = report_comp5_rec_unit_condition;
	}

	public void setreport_comp5_rec_unit_features(String report_comp5_rec_unit_features) {
	this.report_comp5_rec_unit_features = report_comp5_rec_unit_features;
	}

	public void setreport_comp5_rec_degree_of_devt(String report_comp5_rec_degree_of_devt) {
	this.report_comp5_rec_degree_of_devt = report_comp5_rec_degree_of_devt;
	}

	public void setreport_comp5_rec_corner_influence(String report_comp5_rec_corner_influence) {
		this.report_comp5_rec_corner_influence = report_comp5_rec_corner_influence;
	}

	public void setreport_comp5_concluded_adjustment(String report_comp5_concluded_adjustment) {
	this.report_comp5_concluded_adjustment = report_comp5_concluded_adjustment;
	}

	public void setreport_comp5_adjusted_value(String report_comp5_adjusted_value) {
	this.report_comp5_adjusted_value = report_comp5_adjusted_value;
	}

	public void setreport_comp_average(String report_comp_average) {
	this.report_comp_average = report_comp_average;
	}

	public void setreport_comp_rounded_to(String report_comp_rounded_to) {
	this.report_comp_rounded_to = report_comp_rounded_to;
	}

	public void setreport_zonal_location(String report_zonal_location) {
	this.report_zonal_location = report_zonal_location;
	}

	public void setreport_zonal_lot_classification(String report_zonal_lot_classification) {
	this.report_zonal_lot_classification = report_zonal_lot_classification;
	}

	public void setreport_zonal_value(String report_zonal_value) {
	this.report_zonal_value = report_zonal_value;
	}

	public void setreport_value_total_area(String report_value_total_area) {
	this.report_value_total_area = report_value_total_area;
	}

	public void setreport_value_total_deduction(String report_value_total_deduction) {
	this.report_value_total_deduction = report_value_total_deduction;
	}

	public void setreport_value_total_net_area(String report_value_total_net_area) {
	this.report_value_total_net_area = report_value_total_net_area;
	}

	public void setreport_value_total_landimp_value(String report_value_total_landimp_value) {
	this.report_value_total_landimp_value = report_value_total_landimp_value;
	}

	public void setreport_imp_value_total_imp_value(String report_imp_value_total_imp_value) {
	this.report_imp_value_total_imp_value = report_imp_value_total_imp_value;
	}

	public void setreport_final_value_total_appraised_value_land_imp(String report_final_value_total_appraised_value_land_imp) {
	this.report_final_value_total_appraised_value_land_imp = report_final_value_total_appraised_value_land_imp;
	}

	public void setreport_final_value_recommended_deductions_desc(String report_final_value_recommended_deductions_desc) {
	this.report_final_value_recommended_deductions_desc = report_final_value_recommended_deductions_desc;
	}

	public void setreport_final_value_recommended_deductions_rate(String report_final_value_recommended_deductions_rate) {
	this.report_final_value_recommended_deductions_rate = report_final_value_recommended_deductions_rate;
	}
	public void setreport_final_value_recommended_deductions_other(String report_final_value_recommended_deductions_other) {
		this.report_final_value_recommended_deductions_other = report_final_value_recommended_deductions_other;
	}

	public void setreport_final_value_recommended_deductions_amt(String report_final_value_recommended_deductions_amt) {
	this.report_final_value_recommended_deductions_amt = report_final_value_recommended_deductions_amt;
	}

	public void setreport_final_value_net_appraised_value(String report_final_value_net_appraised_value) {
	this.report_final_value_net_appraised_value = report_final_value_net_appraised_value;
	}

	public void setreport_final_value_quick_sale_value_rate(String report_final_value_quick_sale_value_rate) {
	this.report_final_value_quick_sale_value_rate = report_final_value_quick_sale_value_rate;
	}

	public void setreport_final_value_quick_sale_value_amt(String report_final_value_quick_sale_value_amt) {
	this.report_final_value_quick_sale_value_amt = report_final_value_quick_sale_value_amt;
	}

	public void setreport_factors_of_concern(String report_factors_of_concern) {
	this.report_factors_of_concern = report_factors_of_concern;
	}

	public void setreport_suggested_corrective_actions(String report_suggested_corrective_actions) {
	this.report_suggested_corrective_actions = report_suggested_corrective_actions;
	}

	public void setreport_remarks(String report_remarks) {
	this.report_remarks = report_remarks;
	}

	public void setreport_requirements(String report_requirements) {
	this.report_requirements = report_requirements;
	}
	
	public void setreport_time_inspected(String report_time_inspected) {
		this.report_time_inspected = report_time_inspected;
	}
	
	public void setreport_comp1_remarks(String report_comp1_remarks) {
		this.report_comp1_remarks = report_comp1_remarks;
	}
	
	public void setreport_comp2_remarks(String report_comp2_remarks) {
		this.report_comp2_remarks = report_comp2_remarks;
	}
	
	public void setreport_comp3_remarks(String report_comp3_remarks) {
		this.report_comp3_remarks = report_comp3_remarks;
	}
	
	public void setreport_comp4_remarks(String report_comp4_remarks) {
		this.report_comp4_remarks = report_comp4_remarks;
	}
	
	public void setreport_comp5_remarks(String report_comp5_remarks) {
		this.report_comp5_remarks = report_comp5_remarks;
	}
	
	public void setreport_comp1_lot_area(String report_comp1_lot_area) {
		this.report_comp1_lot_area = report_comp1_lot_area;
	}
	public void setreport_comp2_lot_area(String report_comp2_lot_area) {
		this.report_comp2_lot_area = report_comp2_lot_area;
	}
	public void setreport_comp3_lot_area(String report_comp3_lot_area) {
		this.report_comp3_lot_area = report_comp3_lot_area;
	}
	public void setreport_comp4_lot_area(String report_comp4_lot_area) {
		this.report_comp4_lot_area = report_comp4_lot_area;
	}
	public void setreport_comp5_lot_area(String report_comp5_lot_area) {
		this.report_comp5_lot_area = report_comp5_lot_area;
	}
	
	public void setreport_concerns_minor_1(String report_concerns_minor_1) {
		this.report_concerns_minor_1 = report_concerns_minor_1;
	}
	
	public void setreport_concerns_minor_2(String report_concerns_minor_2) {
		this.report_concerns_minor_2 = report_concerns_minor_2;
	}
	
	public void setreport_concerns_minor_3(String report_concerns_minor_3) {
		this.report_concerns_minor_3 = report_concerns_minor_3;
	}
	
	public void setreport_concerns_minor_others(String report_concerns_minor_others) {
		this.report_concerns_minor_others = report_concerns_minor_others;
	}
	
	public void setreport_concerns_minor_others_desc(String report_concerns_minor_others_desc) {
		this.report_concerns_minor_others_desc = report_concerns_minor_others_desc;
	}
	
	public void setreport_concerns_major_1(String report_concerns_major_1) {
		this.report_concerns_major_1 = report_concerns_major_1;
	}
	
	public void setreport_concerns_major_2(String report_concerns_major_2) {
		this.report_concerns_major_2 = report_concerns_major_2;
	}
	
	public void setreport_concerns_major_3(String report_concerns_major_3) {
		this.report_concerns_major_3 = report_concerns_major_3;
	}
	
	public void setreport_concerns_major_4(String report_concerns_major_4) {
		this.report_concerns_major_4 = report_concerns_major_4;
	}
	
	public void setreport_concerns_major_5(String report_concerns_major_5) {
		this.report_concerns_major_5 = report_concerns_major_5;
	}
	
	public void setreport_concerns_major_6(String report_concerns_major_6) {
		this.report_concerns_major_6 = report_concerns_major_6;
	}
	
	public void setreport_concerns_major_others(String report_concerns_major_others) {
		this.report_concerns_major_others = report_concerns_major_others;
	}
	
	public void setreport_concerns_major_others_desc(String report_concerns_major_others_desc) {
		this.report_concerns_major_others_desc = report_concerns_major_others_desc;
	}

	//Added from IAN
	public void setreport_prev_date(String report_prev_date) {
		this.report_prev_date = report_prev_date;
	}

	public void setreport_prev_appraiser(String report_prev_appraiser) {
		this.report_prev_appraiser = report_prev_appraiser;
	}

	public void setreport_prev_total_appraised_value(String report_prev_total_appraised_value) {
		this.report_prev_total_appraised_value = report_prev_total_appraised_value;
	}
	public void setreport_townhouse_td_no(String report_townhouse_td_no) {
		this.report_townhouse_td_no = report_townhouse_td_no;
	}
}