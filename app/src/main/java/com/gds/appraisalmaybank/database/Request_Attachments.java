package com.gds.appraisalmaybank.database;

public class Request_Attachments {

	// private variables
	int id;
	String rt_id;
	String rq_uid;
	String rq_file;
	String rq_filename;
	String rq_appraisal_type;
	String rq_candidate_done;

	// Empty constructor
	public Request_Attachments() {

	}

	// constructor
	public Request_Attachments(int id, String rt_id, String rq_uid,
			String rq_file, String rq_filename, String rq_appraisal_type,
			String rq_candidate_done) {
		this.id = id;
		this.rt_id = rt_id;
		this.rq_uid = rq_uid;
		this.rq_file = rq_file;
		this.rq_filename = rq_filename;
		this.rq_appraisal_type = rq_appraisal_type;
		this.rq_candidate_done = rq_candidate_done;
	}

	// constructor
	public Request_Attachments(String rt_id, String rq_uid, String rq_file,
			String rq_filename, String rq_appraisal_type,
			String rq_candidate_done) {

		this.rt_id = rt_id;
		this.rq_uid = rq_uid;
		this.rq_file = rq_file;
		this.rq_filename = rq_filename;
		this.rq_appraisal_type = rq_appraisal_type;
		this.rq_candidate_done = rq_candidate_done;
	}

	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	// getting rt_id
	public String getrt_id() {
		return this.rt_id;
	}

	// setting rt_id
	public void setrt_id(String rt_id) {
		this.rt_id = rt_id;
	}

	// getting rq_uid
	public String getrq_uid() {
		return this.rq_uid;
	}

	// setting rq_uid
	public void setrq_uid(String rq_uid) {
		this.rq_uid = rq_uid;
	}

	// getting rq_file
	public String getrq_file() {
		return this.rq_file;
	}

	// setting rq_file
	public void setrq_file(String rq_file) {
		this.rq_file = rq_file;
	}

	// getting rq_filename
	public String getrq_filename() {
		return this.rq_filename;
	}

	// setting rq_filename
	public void setrq_filename(String rq_filename) {
		this.rq_filename = rq_filename;
	}

	// getting rq_appraisal_type
	public String getrq_appraisal_type() {
		return this.rq_appraisal_type;
	}

	// setting rq_appraisal_type
	public void setrq_appraisal_type(String rq_appraisal_type) {
		this.rq_appraisal_type = rq_appraisal_type;
	}

	// getting rq_candidate_done
	public String getrq_candidate_done() {
		return this.rq_candidate_done;
	}

	// setting rq_candidate_done
	public void setrq_candidate_done(String rq_candidate_done) {
		this.rq_candidate_done = rq_candidate_done;
	}
}
