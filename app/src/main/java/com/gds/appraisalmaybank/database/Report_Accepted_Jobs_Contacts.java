package com.gds.appraisalmaybank.database;

public class Report_Accepted_Jobs_Contacts {

	// private variables
	int id;
	String record_id;
	String contact_person;
	String contact_mobile_no_prefix;
	String contact_mobile_no;
	String contact_landline;

	// Empty constructor
	public Report_Accepted_Jobs_Contacts() {

	}

	// constructor
	public Report_Accepted_Jobs_Contacts(int id, String record_id,
			String contact_person, String contact_mobile_no_prefix,
			String contact_mobile_no, String contact_landline) {
		this.id = id;
		this.record_id = record_id;
		this.contact_person = contact_person;
		this.contact_mobile_no_prefix = contact_mobile_no_prefix;
		this.contact_mobile_no = contact_mobile_no;
		this.contact_landline = contact_landline;
	}

	// constructor
	public Report_Accepted_Jobs_Contacts(String record_id,
			String contact_person, String contact_mobile_no_prefix,
			String contact_mobile_no, String contact_landline) {

		this.record_id = record_id;
		this.contact_person = contact_person;
		this.contact_mobile_no_prefix = contact_mobile_no_prefix;
		this.contact_mobile_no = contact_mobile_no;
		this.contact_landline = contact_landline;
	}

	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	// getting record_id
	public String getrecord_id() {
		return this.record_id;
	}

	// setting record_id
	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}

	// getting contact_person
	public String getcontact_person() {
		return this.contact_person;
	}

	// setting contact_person
	public void setcontact_person(String contact_person) {
		this.contact_person = contact_person;
	}

	// getting contact_mobile_no_prefix
	public String getcontact_mobile_no_prefix() {
		return this.contact_mobile_no_prefix;
	}

	// setting contact_mobile_no_prefix
	public void setcontact_mobile_no_prefix(String contact_mobile_no_prefix) {
		this.contact_mobile_no_prefix = contact_mobile_no_prefix;
	}

	// getting contact_mobile_no
	public String getcontact_mobile_no() {
		return this.contact_mobile_no;
	}

	// setting contact_mobile_no
	public void setcontact_mobile_no(String contact_mobile_no) {
		this.contact_mobile_no = contact_mobile_no;
	}

	// getting contact_landline
	public String getcontact_landline() {
		return this.contact_landline;
	}

	// setting contact_landline
	public void setcontact_landline(String contact_landline) {
		this.contact_landline = contact_landline;
	}
}
