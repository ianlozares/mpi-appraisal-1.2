package com.gds.appraisalmaybank.database;

/**
 * Created by ianlo on 02/08/2016.
 */
public class Report_Submitted_Jobs {

    // private variables
    int id;
    String record_id;
    String first_name;
    String middle_name;
    String last_name;
    String appraisal_type;
    String status;

    // Empty constructor
    public Report_Submitted_Jobs() {

    }

    // constructor
    public Report_Submitted_Jobs(int id, String record_id, String first_name,
                                String middle_name, String last_name, String appraisal_type, String status) {
        this.id = id;
        this.record_id = record_id;
        this.first_name = first_name;
        this.middle_name = middle_name;
        this.last_name = last_name;
        this.appraisal_type = appraisal_type;
        this.status = status;
    }

    // constructor
    public Report_Submitted_Jobs(String record_id, String first_name,
                                String middle_name, String last_name, String appraisal_type, String status) {

        this.record_id = record_id;
        this.first_name = first_name;
        this.middle_name = middle_name;
        this.last_name = last_name;
        this.appraisal_type = appraisal_type;
        this.status = status;
    }



    // getting ID
    public int getID() {
        return this.id;
    }

    // setting id
    public void setID(int id) {
        this.id = id;
    }

    // getting record_id
    public String getrecord_id() {
        return this.record_id;
    }

    // setting record_id
    public void setrecord_id(String record_id) {
        this.record_id = record_id;
    }

    // getting first_name
    public String getfirst_name() {
        return this.first_name;
    }

    // setting first_name
    public void setfirst_name(String first_name) {
        this.first_name = first_name;
    }

    // getting middle_name
    public String getmiddle_name() {
        return this.middle_name;
    }

    // setting middle_name
    public void setmiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    // getting last_name
    public String getlast_name() {
        return this.last_name;
    }

    // setting last_name
    public void setlast_name(String last_name) {
        this.last_name = last_name;
    }

    // getting appraisal_type
    public String getappraisal_type() {
        return this.appraisal_type;
    }

    // setting appraisal_type
    public void setappraisal_type(String appraisal_type) {
        this.appraisal_type = appraisal_type;
    }

    // getting status
    public String getstatus() {
        return this.status;
    }

    // setting status
    public void setstatus(String status) {
        this.status = status;
    }


}