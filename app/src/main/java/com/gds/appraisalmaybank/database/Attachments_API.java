package com.gds.appraisalmaybank.database;

public class Attachments_API {

	// private variables
	int id;
	String record_id;
	String uid;
	String appraisal_type;
	String file;
	String file_name;
	
	// Empty constructor
	public Attachments_API() {

	}
	
	public Attachments_API(String record_id) {
		this.record_id = record_id;
	}
	
	public Attachments_API(int id, String record_id, String uid, String appraisal_type,
			String file, String file_name) {
		this.id = id;
		this.record_id = record_id;
		this.uid = uid;
		this.appraisal_type = appraisal_type;
		this.file = file;
		this.file_name = file_name;
	}
	
	public Attachments_API(String record_id, String uid, String appraisal_type,
			String file, String file_name) {
		this.record_id = record_id;
		this.uid = uid;
		this.appraisal_type = appraisal_type;
		this.file = file;
		this.file_name = file_name;
	}
	
	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}
	
	public String getrecord_id() {
		return this.record_id;
	}
	
	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}
	
	public String getuid() {
		return this.uid;
	}
	
	public void setuid(String uid) {
		this.uid = uid;
	}
	
	// getters
	public String getappraisal_type() {
		return this.appraisal_type;
	}
	
	public String getfile() {
		return this.file;
	}
	public String getfile_name() {
		return this.file_name;
	}
	
	//setters
	public void setappraisal_type(String appraisal_type) {
		this.appraisal_type = appraisal_type;
	}
	
	public void setfile(String file) {
		this.file = file;
	}
	
	public void setfile_name(String file_name) {
		this.file_name = file_name;
	}
}
