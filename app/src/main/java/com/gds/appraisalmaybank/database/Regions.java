package com.gds.appraisalmaybank.database;

public class Regions {

	// private variables
	int id;
	String name;

	// Empty constructor
	public Regions() {

	}

	// constructor
	public Regions(int id, String name) {
		this.id = id;
		this.name = name;
	}

	// constructor
	public Regions(String name) {

		this.name = name;
	}

	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	// getting name
	public String getname() {
		return this.name;
	}

	// setting name
	public void setname(String name) {
		this.name = name;
	}
}
