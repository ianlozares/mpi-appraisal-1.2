package com.gds.appraisalmaybank.database;

public class Construction_Ebm_API {

	// private variables
	int id;
	String record_id;
	String report_date_inspected_month;
	String report_date_inspected_day;
	String report_date_inspected_year;
	String report_time_inspected;
	String report_project_type;
	String report_floor_area;
	String report_storeys;
	String report_expected_economic_life;
	String report_type_of_housing_unit;
	String report_const_type_reinforced_concrete;
	String report_const_type_semi_concrete;
	String report_const_type_mixed_materials;
	String report_foundation_concrete;
	String report_foundation_other;
	String report_foundation_other_value;
	String report_post_concrete;
	String report_post_concrete_timber;
	String report_post_steel;
	String report_post_other;
	String report_post_other_value;
	String report_beams_concrete;
	String report_beams_timber;
	String report_beams_steel;
	String report_beams_other;
	String report_beams_other_value;
	String report_floors_concrete;
	String report_floors_tiles;
	String report_floors_tiles_cement;
	String report_floors_laminated_wood;
	String report_floors_ceramic_tiles;
	String report_floors_wood_planks;
	String report_floors_marble_washout;
	String report_floors_concrete_boards;
	String report_floors_granite_tiles;
	String report_floors_marble_wood;
	String report_floors_carpet;
	String report_floors_other;
	String report_floors_other_value;
	String report_walls_chb;
	String report_walls_chb_cement;
	String report_walls_anay;
	String report_walls_chb_wood;
	String report_walls_precast;
	String report_walls_decorative_stone;
	String report_walls_adobe;
	String report_walls_ceramic_tiles;
	String report_walls_cast_in_place;
	String report_walls_sandblast;
	String report_walls_mactan_stone;
	String report_walls_painted;
	String report_walls_other;
	String report_walls_other_value;
	String report_partitions_chb;
	String report_partitions_painted_cement;
	String report_partitions_anay;
	String report_partitions_wood_boards;
	String report_partitions_precast;
	String report_partitions_decorative_stone;
	String report_partitions_adobe;
	String report_partitions_granite;
	String report_partitions_cast_in_place;
	String report_partitions_sandblast;
	String report_partitions_mactan_stone;
	String report_partitions_ceramic_tiles;
	String report_partitions_chb_plywood;
	String report_partitions_hardiflex;
	String report_partitions_wallpaper;
	String report_partitions_painted;
	String report_partitions_other;
	String report_partitions_other_value;
	String report_windows_steel_casement;
	String report_windows_fixed_view;
	String report_windows_analok_sliding;
	String report_windows_alum_glass;
	String report_windows_aluminum_sliding;
	String report_windows_awning_type;
	String report_windows_powder_coated;
	String report_windows_wooden_frame;
	String report_windows_other;
	String report_windows_other_value;
	String report_doors_wood_panel;
	String report_doors_pvc;
	String report_doors_analok_sliding;
	String report_doors_screen_door;
	String report_doors_flush;
	String report_doors_molded_door;
	String report_doors_aluminum_sliding;
	String report_doors_flush_french;
	String report_doors_other;
	String report_doors_other_value;
	String report_ceiling_plywood;
	String report_ceiling_painted_gypsum;
	String report_ceiling_soffit_slab;
	String report_ceiling_metal_deck;
	String report_ceiling_hardiflex;
	String report_ceiling_plywood_tg;
	String report_ceiling_plywood_pvc;
	String report_ceiling_painted;
	String report_ceiling_with_cornice;
	String report_ceiling_with_moulding;
	String report_ceiling_drop_ceiling;
	String report_ceiling_other;
	String report_ceiling_other_value;
	String report_roof_pre_painted;
	String report_roof_rib_type;
	String report_roof_tilespan;
	String report_roof_tegula_asphalt;
	String report_roof_tegula_longspan;
	String report_roof_tegula_gi;
	String report_roof_steel_concrete;
	String report_roof_polycarbonate;
	String report_roof_on_steel_trusses;
	String report_roof_on_wooden_trusses;
	String report_roof_other;
	String report_roof_other_value;
	String report_valuation_total_area;
	String report_valuation_total_proj_cost;
	String report_valuation_remarks;
	
	// Empty constructor
	public Construction_Ebm_API() {

	}
	
	// constructor with int recordid only for parameter
	public Construction_Ebm_API(String record_id) {
		this.record_id = record_id;
	}
	
	//with id
	public Construction_Ebm_API(
			int id,
			String record_id,
			String report_date_inspected_month,
			String report_date_inspected_day,
			String report_date_inspected_year,
			String report_time_inspected,
			String report_project_type,
			String report_floor_area,
			String report_storeys,
			String report_expected_economic_life,
			String report_type_of_housing_unit,
			String report_const_type_reinforced_concrete,
			String report_const_type_semi_concrete,
			String report_const_type_mixed_materials,
			String report_foundation_concrete,
			String report_foundation_other,
			String report_foundation_other_value,
			String report_post_concrete,
			String report_post_concrete_timber,
			String report_post_steel,
			String report_post_other,
			String report_post_other_value,
			String report_beams_concrete,
			String report_beams_timber,
			String report_beams_steel,
			String report_beams_other,
			String report_beams_other_value,
			String report_floors_concrete,
			String report_floors_tiles,
			String report_floors_tiles_cement,
			String report_floors_laminated_wood,
			String report_floors_ceramic_tiles,
			String report_floors_wood_planks,
			String report_floors_marble_washout,
			String report_floors_concrete_boards,
			String report_floors_granite_tiles,
			String report_floors_marble_wood,
			String report_floors_carpet,
			String report_floors_other,
			String report_floors_other_value,
			String report_walls_chb,
			String report_walls_chb_cement,
			String report_walls_anay,
			String report_walls_chb_wood,
			String report_walls_precast,
			String report_walls_decorative_stone,
			String report_walls_adobe,
			String report_walls_ceramic_tiles,
			String report_walls_cast_in_place,
			String report_walls_sandblast,
			String report_walls_mactan_stone,
			String report_walls_painted,
			String report_walls_other,
			String report_walls_other_value,
			String report_partitions_chb,
			String report_partitions_painted_cement,
			String report_partitions_anay,
			String report_partitions_wood_boards,
			String report_partitions_precast,
			String report_partitions_decorative_stone,
			String report_partitions_adobe,
			String report_partitions_granite,
			String report_partitions_cast_in_place,
			String report_partitions_sandblast,
			String report_partitions_mactan_stone,
			String report_partitions_ceramic_tiles,
			String report_partitions_chb_plywood,
			String report_partitions_hardiflex,
			String report_partitions_wallpaper,
			String report_partitions_painted,
			String report_partitions_other,
			String report_partitions_other_value,
			String report_windows_steel_casement,
			String report_windows_fixed_view,
			String report_windows_analok_sliding,
			String report_windows_alum_glass,
			String report_windows_aluminum_sliding,
			String report_windows_awning_type,
			String report_windows_powder_coated,
			String report_windows_wooden_frame,
			String report_windows_other,
			String report_windows_other_value,
			String report_doors_wood_panel,
			String report_doors_pvc,
			String report_doors_analok_sliding,
			String report_doors_screen_door,
			String report_doors_flush,
			String report_doors_molded_door,
			String report_doors_aluminum_sliding,
			String report_doors_flush_french,
			String report_doors_other,
			String report_doors_other_value,
			String report_ceiling_plywood,
			String report_ceiling_painted_gypsum,
			String report_ceiling_soffit_slab,
			String report_ceiling_metal_deck,
			String report_ceiling_hardiflex,
			String report_ceiling_plywood_tg,
			String report_ceiling_plywood_pvc,
			String report_ceiling_painted,
			String report_ceiling_with_cornice,
			String report_ceiling_with_moulding,
			String report_ceiling_drop_ceiling,
			String report_ceiling_other,
			String report_ceiling_other_value,
			String report_roof_pre_painted,
			String report_roof_rib_type,
			String report_roof_tilespan,
			String report_roof_tegula_asphalt,
			String report_roof_tegula_longspan,
			String report_roof_tegula_gi,
			String report_roof_steel_concrete,
			String report_roof_polycarbonate,
			String report_roof_on_steel_trusses,
			String report_roof_on_wooden_trusses,
			String report_roof_other,
			String report_roof_other_value,
			String report_valuation_total_area,
			String report_valuation_total_proj_cost,
			String report_valuation_remarks) {
		this.id = id;;
		this.record_id = record_id;;
		this.report_date_inspected_month = report_date_inspected_month;
		this.report_date_inspected_day = report_date_inspected_day;
		this.report_date_inspected_year = report_date_inspected_year;
		this.report_time_inspected = report_time_inspected;
		this.report_project_type = report_project_type;
		this.report_floor_area = report_floor_area;
		this.report_storeys = report_storeys;
		this.report_expected_economic_life = report_expected_economic_life;
		this.report_type_of_housing_unit = report_type_of_housing_unit;
		this.report_const_type_reinforced_concrete = report_const_type_reinforced_concrete;
		this.report_const_type_semi_concrete = report_const_type_semi_concrete;
		this.report_const_type_mixed_materials = report_const_type_mixed_materials;
		this.report_foundation_concrete = report_foundation_concrete;
		this.report_foundation_other = report_foundation_other;
		this.report_foundation_other_value = report_foundation_other_value;
		this.report_post_concrete = report_post_concrete;
		this.report_post_concrete_timber = report_post_concrete_timber;
		this.report_post_steel = report_post_steel;
		this.report_post_other = report_post_other;
		this.report_post_other_value = report_post_other_value;
		this.report_beams_concrete = report_beams_concrete;
		this.report_beams_timber = report_beams_timber;
		this.report_beams_steel = report_beams_steel;
		this.report_beams_other = report_beams_other;
		this.report_beams_other_value = report_beams_other_value;
		this.report_floors_concrete = report_floors_concrete;
		this.report_floors_tiles = report_floors_tiles;
		this.report_floors_tiles_cement = report_floors_tiles_cement;
		this.report_floors_laminated_wood = report_floors_laminated_wood;
		this.report_floors_ceramic_tiles = report_floors_ceramic_tiles;
		this.report_floors_wood_planks = report_floors_wood_planks;
		this.report_floors_marble_washout = report_floors_marble_washout;
		this.report_floors_concrete_boards = report_floors_concrete_boards;
		this.report_floors_granite_tiles = report_floors_granite_tiles;
		this.report_floors_marble_wood = report_floors_marble_wood;
		this.report_floors_carpet = report_floors_carpet;
		this.report_floors_other = report_floors_other;
		this.report_floors_other_value = report_floors_other_value;
		this.report_walls_chb = report_walls_chb;
		this.report_walls_chb_cement = report_walls_chb_cement;
		this.report_walls_anay = report_walls_anay;
		this.report_walls_chb_wood = report_walls_chb_wood;
		this.report_walls_precast = report_walls_precast;
		this.report_walls_decorative_stone = report_walls_decorative_stone;
		this.report_walls_adobe = report_walls_adobe;
		this.report_walls_ceramic_tiles = report_walls_ceramic_tiles;
		this.report_walls_cast_in_place = report_walls_cast_in_place;
		this.report_walls_sandblast = report_walls_sandblast;
		this.report_walls_mactan_stone = report_walls_mactan_stone;
		this.report_walls_painted = report_walls_painted;
		this.report_walls_other = report_walls_other;
		this.report_walls_other_value = report_walls_other_value;
		this.report_partitions_chb = report_partitions_chb;
		this.report_partitions_painted_cement = report_partitions_painted_cement;
		this.report_partitions_anay = report_partitions_anay;
		this.report_partitions_wood_boards = report_partitions_wood_boards;
		this.report_partitions_precast = report_partitions_precast;
		this.report_partitions_decorative_stone = report_partitions_decorative_stone;
		this.report_partitions_adobe = report_partitions_adobe;
		this.report_partitions_granite = report_partitions_granite;
		this.report_partitions_cast_in_place = report_partitions_cast_in_place;
		this.report_partitions_sandblast = report_partitions_sandblast;
		this.report_partitions_mactan_stone = report_partitions_mactan_stone;
		this.report_partitions_ceramic_tiles = report_partitions_ceramic_tiles;
		this.report_partitions_chb_plywood = report_partitions_chb_plywood;
		this.report_partitions_hardiflex = report_partitions_hardiflex;
		this.report_partitions_wallpaper = report_partitions_wallpaper;
		this.report_partitions_painted = report_partitions_painted;
		this.report_partitions_other = report_partitions_other;
		this.report_partitions_other_value = report_partitions_other_value;
		this.report_windows_steel_casement = report_windows_steel_casement;
		this.report_windows_fixed_view = report_windows_fixed_view;
		this.report_windows_analok_sliding = report_windows_analok_sliding;
		this.report_windows_alum_glass = report_windows_alum_glass;
		this.report_windows_aluminum_sliding = report_windows_aluminum_sliding;
		this.report_windows_awning_type = report_windows_awning_type;
		this.report_windows_powder_coated = report_windows_powder_coated;
		this.report_windows_wooden_frame = report_windows_wooden_frame;
		this.report_windows_other = report_windows_other;
		this.report_windows_other_value = report_windows_other_value;
		this.report_doors_wood_panel = report_doors_wood_panel;
		this.report_doors_pvc = report_doors_pvc;
		this.report_doors_analok_sliding = report_doors_analok_sliding;
		this.report_doors_screen_door = report_doors_screen_door;
		this.report_doors_flush = report_doors_flush;
		this.report_doors_molded_door = report_doors_molded_door;
		this.report_doors_aluminum_sliding = report_doors_aluminum_sliding;
		this.report_doors_flush_french = report_doors_flush_french;
		this.report_doors_other = report_doors_other;
		this.report_doors_other_value = report_doors_other_value;
		this.report_ceiling_plywood = report_ceiling_plywood;
		this.report_ceiling_painted_gypsum = report_ceiling_painted_gypsum;
		this.report_ceiling_soffit_slab = report_ceiling_soffit_slab;
		this.report_ceiling_metal_deck = report_ceiling_metal_deck;
		this.report_ceiling_hardiflex = report_ceiling_hardiflex;
		this.report_ceiling_plywood_tg = report_ceiling_plywood_tg;
		this.report_ceiling_plywood_pvc = report_ceiling_plywood_pvc;
		this.report_ceiling_painted = report_ceiling_painted;
		this.report_ceiling_with_cornice = report_ceiling_with_cornice;
		this.report_ceiling_with_moulding = report_ceiling_with_moulding;
		this.report_ceiling_drop_ceiling = report_ceiling_drop_ceiling;
		this.report_ceiling_other = report_ceiling_other;
		this.report_ceiling_other_value = report_ceiling_other_value;
		this.report_roof_pre_painted = report_roof_pre_painted;
		this.report_roof_rib_type = report_roof_rib_type;
		this.report_roof_tilespan = report_roof_tilespan;
		this.report_roof_tegula_asphalt = report_roof_tegula_asphalt;
		this.report_roof_tegula_longspan = report_roof_tegula_longspan;
		this.report_roof_tegula_gi = report_roof_tegula_gi;
		this.report_roof_steel_concrete = report_roof_steel_concrete;
		this.report_roof_polycarbonate = report_roof_polycarbonate;
		this.report_roof_on_steel_trusses = report_roof_on_steel_trusses;
		this.report_roof_on_wooden_trusses = report_roof_on_wooden_trusses;
		this.report_roof_other = report_roof_other;
		this.report_roof_other_value = report_roof_other_value;
		this.report_valuation_total_area = report_valuation_total_area;
		this.report_valuation_total_proj_cost = report_valuation_total_proj_cost;
		this.report_valuation_remarks = report_valuation_remarks;
	}
	
	//without id
	public Construction_Ebm_API(
			String record_id,
			String report_date_inspected_month,
			String report_date_inspected_day,
			String report_date_inspected_year,
			String report_time_inspected,
			String report_project_type,
			String report_floor_area,
			String report_storeys,
			String report_expected_economic_life,
			String report_type_of_housing_unit,
			String report_const_type_reinforced_concrete,
			String report_const_type_semi_concrete,
			String report_const_type_mixed_materials,
			String report_foundation_concrete,
			String report_foundation_other,
			String report_foundation_other_value,
			String report_post_concrete,
			String report_post_concrete_timber,
			String report_post_steel,
			String report_post_other,
			String report_post_other_value,
			String report_beams_concrete,
			String report_beams_timber,
			String report_beams_steel,
			String report_beams_other,
			String report_beams_other_value,
			String report_floors_concrete,
			String report_floors_tiles,
			String report_floors_tiles_cement,
			String report_floors_laminated_wood,
			String report_floors_ceramic_tiles,
			String report_floors_wood_planks,
			String report_floors_marble_washout,
			String report_floors_concrete_boards,
			String report_floors_granite_tiles,
			String report_floors_marble_wood,
			String report_floors_carpet,
			String report_floors_other,
			String report_floors_other_value,
			String report_walls_chb,
			String report_walls_chb_cement,
			String report_walls_anay,
			String report_walls_chb_wood,
			String report_walls_precast,
			String report_walls_decorative_stone,
			String report_walls_adobe,
			String report_walls_ceramic_tiles,
			String report_walls_cast_in_place,
			String report_walls_sandblast,
			String report_walls_mactan_stone,
			String report_walls_painted,
			String report_walls_other,
			String report_walls_other_value,
			String report_partitions_chb,
			String report_partitions_painted_cement,
			String report_partitions_anay,
			String report_partitions_wood_boards,
			String report_partitions_precast,
			String report_partitions_decorative_stone,
			String report_partitions_adobe,
			String report_partitions_granite,
			String report_partitions_cast_in_place,
			String report_partitions_sandblast,
			String report_partitions_mactan_stone,
			String report_partitions_ceramic_tiles,
			String report_partitions_chb_plywood,
			String report_partitions_hardiflex,
			String report_partitions_wallpaper,
			String report_partitions_painted,
			String report_partitions_other,
			String report_partitions_other_value,
			String report_windows_steel_casement,
			String report_windows_fixed_view,
			String report_windows_analok_sliding,
			String report_windows_alum_glass,
			String report_windows_aluminum_sliding,
			String report_windows_awning_type,
			String report_windows_powder_coated,
			String report_windows_wooden_frame,
			String report_windows_other,
			String report_windows_other_value,
			String report_doors_wood_panel,
			String report_doors_pvc,
			String report_doors_analok_sliding,
			String report_doors_screen_door,
			String report_doors_flush,
			String report_doors_molded_door,
			String report_doors_aluminum_sliding,
			String report_doors_flush_french,
			String report_doors_other,
			String report_doors_other_value,
			String report_ceiling_plywood,
			String report_ceiling_painted_gypsum,
			String report_ceiling_soffit_slab,
			String report_ceiling_metal_deck,
			String report_ceiling_hardiflex,
			String report_ceiling_plywood_tg,
			String report_ceiling_plywood_pvc,
			String report_ceiling_painted,
			String report_ceiling_with_cornice,
			String report_ceiling_with_moulding,
			String report_ceiling_drop_ceiling,
			String report_ceiling_other,
			String report_ceiling_other_value,
			String report_roof_pre_painted,
			String report_roof_rib_type,
			String report_roof_tilespan,
			String report_roof_tegula_asphalt,
			String report_roof_tegula_longspan,
			String report_roof_tegula_gi,
			String report_roof_steel_concrete,
			String report_roof_polycarbonate,
			String report_roof_on_steel_trusses,
			String report_roof_on_wooden_trusses,
			String report_roof_other,
			String report_roof_other_value,
			String report_valuation_total_area,
			String report_valuation_total_proj_cost,
			String report_valuation_remarks) {
		this.record_id = record_id;
		this.report_date_inspected_month = report_date_inspected_month;
		this.report_date_inspected_day = report_date_inspected_day;
		this.report_date_inspected_year = report_date_inspected_year;
		this.report_time_inspected = report_time_inspected;
		this.report_project_type = report_project_type;
		this.report_floor_area = report_floor_area;
		this.report_storeys = report_storeys;
		this.report_expected_economic_life = report_expected_economic_life;
		this.report_type_of_housing_unit = report_type_of_housing_unit;
		this.report_const_type_reinforced_concrete = report_const_type_reinforced_concrete;
		this.report_const_type_semi_concrete = report_const_type_semi_concrete;
		this.report_const_type_mixed_materials = report_const_type_mixed_materials;
		this.report_foundation_concrete = report_foundation_concrete;
		this.report_foundation_other = report_foundation_other;
		this.report_foundation_other_value = report_foundation_other_value;
		this.report_post_concrete = report_post_concrete;
		this.report_post_concrete_timber = report_post_concrete_timber;
		this.report_post_steel = report_post_steel;
		this.report_post_other = report_post_other;
		this.report_post_other_value = report_post_other_value;
		this.report_beams_concrete = report_beams_concrete;
		this.report_beams_timber = report_beams_timber;
		this.report_beams_steel = report_beams_steel;
		this.report_beams_other = report_beams_other;
		this.report_beams_other_value = report_beams_other_value;
		this.report_floors_concrete = report_floors_concrete;
		this.report_floors_tiles = report_floors_tiles;
		this.report_floors_tiles_cement = report_floors_tiles_cement;
		this.report_floors_laminated_wood = report_floors_laminated_wood;
		this.report_floors_ceramic_tiles = report_floors_ceramic_tiles;
		this.report_floors_wood_planks = report_floors_wood_planks;
		this.report_floors_marble_washout = report_floors_marble_washout;
		this.report_floors_concrete_boards = report_floors_concrete_boards;
		this.report_floors_granite_tiles = report_floors_granite_tiles;
		this.report_floors_marble_wood = report_floors_marble_wood;
		this.report_floors_carpet = report_floors_carpet;
		this.report_floors_other = report_floors_other;
		this.report_floors_other_value = report_floors_other_value;
		this.report_walls_chb = report_walls_chb;
		this.report_walls_chb_cement = report_walls_chb_cement;
		this.report_walls_anay = report_walls_anay;
		this.report_walls_chb_wood = report_walls_chb_wood;
		this.report_walls_precast = report_walls_precast;
		this.report_walls_decorative_stone = report_walls_decorative_stone;
		this.report_walls_adobe = report_walls_adobe;
		this.report_walls_ceramic_tiles = report_walls_ceramic_tiles;
		this.report_walls_cast_in_place = report_walls_cast_in_place;
		this.report_walls_sandblast = report_walls_sandblast;
		this.report_walls_mactan_stone = report_walls_mactan_stone;
		this.report_walls_painted = report_walls_painted;
		this.report_walls_other = report_walls_other;
		this.report_walls_other_value = report_walls_other_value;
		this.report_partitions_chb = report_partitions_chb;
		this.report_partitions_painted_cement = report_partitions_painted_cement;
		this.report_partitions_anay = report_partitions_anay;
		this.report_partitions_wood_boards = report_partitions_wood_boards;
		this.report_partitions_precast = report_partitions_precast;
		this.report_partitions_decorative_stone = report_partitions_decorative_stone;
		this.report_partitions_adobe = report_partitions_adobe;
		this.report_partitions_granite = report_partitions_granite;
		this.report_partitions_cast_in_place = report_partitions_cast_in_place;
		this.report_partitions_sandblast = report_partitions_sandblast;
		this.report_partitions_mactan_stone = report_partitions_mactan_stone;
		this.report_partitions_ceramic_tiles = report_partitions_ceramic_tiles;
		this.report_partitions_chb_plywood = report_partitions_chb_plywood;
		this.report_partitions_hardiflex = report_partitions_hardiflex;
		this.report_partitions_wallpaper = report_partitions_wallpaper;
		this.report_partitions_painted = report_partitions_painted;
		this.report_partitions_other = report_partitions_other;
		this.report_partitions_other_value = report_partitions_other_value;
		this.report_windows_steel_casement = report_windows_steel_casement;
		this.report_windows_fixed_view = report_windows_fixed_view;
		this.report_windows_analok_sliding = report_windows_analok_sliding;
		this.report_windows_alum_glass = report_windows_alum_glass;
		this.report_windows_aluminum_sliding = report_windows_aluminum_sliding;
		this.report_windows_awning_type = report_windows_awning_type;
		this.report_windows_powder_coated = report_windows_powder_coated;
		this.report_windows_wooden_frame = report_windows_wooden_frame;
		this.report_windows_other = report_windows_other;
		this.report_windows_other_value = report_windows_other_value;
		this.report_doors_wood_panel = report_doors_wood_panel;
		this.report_doors_pvc = report_doors_pvc;
		this.report_doors_analok_sliding = report_doors_analok_sliding;
		this.report_doors_screen_door = report_doors_screen_door;
		this.report_doors_flush = report_doors_flush;
		this.report_doors_molded_door = report_doors_molded_door;
		this.report_doors_aluminum_sliding = report_doors_aluminum_sliding;
		this.report_doors_flush_french = report_doors_flush_french;
		this.report_doors_other = report_doors_other;
		this.report_doors_other_value = report_doors_other_value;
		this.report_ceiling_plywood = report_ceiling_plywood;
		this.report_ceiling_painted_gypsum = report_ceiling_painted_gypsum;
		this.report_ceiling_soffit_slab = report_ceiling_soffit_slab;
		this.report_ceiling_metal_deck = report_ceiling_metal_deck;
		this.report_ceiling_hardiflex = report_ceiling_hardiflex;
		this.report_ceiling_plywood_tg = report_ceiling_plywood_tg;
		this.report_ceiling_plywood_pvc = report_ceiling_plywood_pvc;
		this.report_ceiling_painted = report_ceiling_painted;
		this.report_ceiling_with_cornice = report_ceiling_with_cornice;
		this.report_ceiling_with_moulding = report_ceiling_with_moulding;
		this.report_ceiling_drop_ceiling = report_ceiling_drop_ceiling;
		this.report_ceiling_other = report_ceiling_other;
		this.report_ceiling_other_value = report_ceiling_other_value;
		this.report_roof_pre_painted = report_roof_pre_painted;
		this.report_roof_rib_type = report_roof_rib_type;
		this.report_roof_tilespan = report_roof_tilespan;
		this.report_roof_tegula_asphalt = report_roof_tegula_asphalt;
		this.report_roof_tegula_longspan = report_roof_tegula_longspan;
		this.report_roof_tegula_gi = report_roof_tegula_gi;
		this.report_roof_steel_concrete = report_roof_steel_concrete;
		this.report_roof_polycarbonate = report_roof_polycarbonate;
		this.report_roof_on_steel_trusses = report_roof_on_steel_trusses;
		this.report_roof_on_wooden_trusses = report_roof_on_wooden_trusses;
		this.report_roof_other = report_roof_other;
		this.report_roof_other_value = report_roof_other_value;
		this.report_valuation_total_area = report_valuation_total_area;
		this.report_valuation_total_proj_cost = report_valuation_total_proj_cost;
		this.report_valuation_remarks = report_valuation_remarks;
	}
	
	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	public String getrecord_id() {
		return this.record_id;
	}

	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}
	
//getters
	public String getreport_date_inspected_month() {
		return this.report_date_inspected_month;
	}

	public String getreport_date_inspected_day() {
		return this.report_date_inspected_day;
	}

	public String getreport_date_inspected_year() {
		return this.report_date_inspected_year;
	}

	public String getreport_time_inspected() {
		return this.report_time_inspected;
	}

	public String getreport_project_type() {
		return this.report_project_type;
	}

	public String getreport_floor_area() {
		return this.report_floor_area;
	}

	public String getreport_storeys() {
		return this.report_storeys;
	}

	public String getreport_expected_economic_life() {
		return this.report_expected_economic_life;
	}

	public String getreport_type_of_housing_unit() {
		return this.report_type_of_housing_unit;
	}

	public String getreport_const_type_reinforced_concrete() {
		return this.report_const_type_reinforced_concrete;
	}

	public String getreport_const_type_semi_concrete() {
		return this.report_const_type_semi_concrete;
	}

	public String getreport_const_type_mixed_materials() {
		return this.report_const_type_mixed_materials;
	}

	public String getreport_foundation_concrete() {
		return this.report_foundation_concrete;
	}

	public String getreport_foundation_other() {
		return this.report_foundation_other;
	}

	public String getreport_foundation_other_value() {
		return this.report_foundation_other_value;
	}

	public String getreport_post_concrete() {
		return this.report_post_concrete;
	}

	public String getreport_post_concrete_timber() {
		return this.report_post_concrete_timber;
	}

	public String getreport_post_steel() {
		return this.report_post_steel;
	}

	public String getreport_post_other() {
		return this.report_post_other;
	}

	public String getreport_post_other_value() {
		return this.report_post_other_value;
	}

	public String getreport_beams_concrete() {
		return this.report_beams_concrete;
	}

	public String getreport_beams_timber() {
		return this.report_beams_timber;
	}

	public String getreport_beams_steel() {
		return this.report_beams_steel;
	}

	public String getreport_beams_other() {
		return this.report_beams_other;
	}

	public String getreport_beams_other_value() {
		return this.report_beams_other_value;
	}

	public String getreport_floors_concrete() {
		return this.report_floors_concrete;
	}

	public String getreport_floors_tiles() {
		return this.report_floors_tiles;
	}

	public String getreport_floors_tiles_cement() {
		return this.report_floors_tiles_cement;
	}

	public String getreport_floors_laminated_wood() {
		return this.report_floors_laminated_wood;
	}

	public String getreport_floors_ceramic_tiles() {
		return this.report_floors_ceramic_tiles;
	}

	public String getreport_floors_wood_planks() {
		return this.report_floors_wood_planks;
	}

	public String getreport_floors_marble_washout() {
		return this.report_floors_marble_washout;
	}

	public String getreport_floors_concrete_boards() {
		return this.report_floors_concrete_boards;
	}

	public String getreport_floors_granite_tiles() {
		return this.report_floors_granite_tiles;
	}

	public String getreport_floors_marble_wood() {
		return this.report_floors_marble_wood;
	}

	public String getreport_floors_carpet() {
		return this.report_floors_carpet;
	}

	public String getreport_floors_other() {
		return this.report_floors_other;
	}

	public String getreport_floors_other_value() {
		return this.report_floors_other_value;
	}

	public String getreport_walls_chb() {
		return this.report_walls_chb;
	}

	public String getreport_walls_chb_cement() {
		return this.report_walls_chb_cement;
	}

	public String getreport_walls_anay() {
		return this.report_walls_anay;
	}

	public String getreport_walls_chb_wood() {
		return this.report_walls_chb_wood;
	}

	public String getreport_walls_precast() {
		return this.report_walls_precast;
	}

	public String getreport_walls_decorative_stone() {
		return this.report_walls_decorative_stone;
	}

	public String getreport_walls_adobe() {
		return this.report_walls_adobe;
	}

	public String getreport_walls_ceramic_tiles() {
		return this.report_walls_ceramic_tiles;
	}

	public String getreport_walls_cast_in_place() {
		return this.report_walls_cast_in_place;
	}

	public String getreport_walls_sandblast() {
		return this.report_walls_sandblast;
	}

	public String getreport_walls_mactan_stone() {
		return this.report_walls_mactan_stone;
	}

	public String getreport_walls_painted() {
		return this.report_walls_painted;
	}

	public String getreport_walls_other() {
		return this.report_walls_other;
	}

	public String getreport_walls_other_value() {
		return this.report_walls_other_value;
	}

	public String getreport_partitions_chb() {
		return this.report_partitions_chb;
	}

	public String getreport_partitions_painted_cement() {
		return this.report_partitions_painted_cement;
	}

	public String getreport_partitions_anay() {
		return this.report_partitions_anay;
	}

	public String getreport_partitions_wood_boards() {
		return this.report_partitions_wood_boards;
	}

	public String getreport_partitions_precast() {
		return this.report_partitions_precast;
	}

	public String getreport_partitions_decorative_stone() {
		return this.report_partitions_decorative_stone;
	}

	public String getreport_partitions_adobe() {
		return this.report_partitions_adobe;
	}

	public String getreport_partitions_granite() {
		return this.report_partitions_granite;
	}

	public String getreport_partitions_cast_in_place() {
		return this.report_partitions_cast_in_place;
	}

	public String getreport_partitions_sandblast() {
		return this.report_partitions_sandblast;
	}

	public String getreport_partitions_mactan_stone() {
		return this.report_partitions_mactan_stone;
	}

	public String getreport_partitions_ceramic_tiles() {
		return this.report_partitions_ceramic_tiles;
	}

	public String getreport_partitions_chb_plywood() {
		return this.report_partitions_chb_plywood;
	}

	public String getreport_partitions_hardiflex() {
		return this.report_partitions_hardiflex;
	}

	public String getreport_partitions_wallpaper() {
		return this.report_partitions_wallpaper;
	}

	public String getreport_partitions_painted() {
		return this.report_partitions_painted;
	}

	public String getreport_partitions_other() {
		return this.report_partitions_other;
	}

	public String getreport_partitions_other_value() {
		return this.report_partitions_other_value;
	}

	public String getreport_windows_steel_casement() {
		return this.report_windows_steel_casement;
	}

	public String getreport_windows_fixed_view() {
		return this.report_windows_fixed_view;
	}

	public String getreport_windows_analok_sliding() {
		return this.report_windows_analok_sliding;
	}

	public String getreport_windows_alum_glass() {
		return this.report_windows_alum_glass;
	}

	public String getreport_windows_aluminum_sliding() {
		return this.report_windows_aluminum_sliding;
	}

	public String getreport_windows_awning_type() {
		return this.report_windows_awning_type;
	}

	public String getreport_windows_powder_coated() {
		return this.report_windows_powder_coated;
	}

	public String getreport_windows_wooden_frame() {
		return this.report_windows_wooden_frame;
	}

	public String getreport_windows_other() {
		return this.report_windows_other;
	}

	public String getreport_windows_other_value() {
		return this.report_windows_other_value;
	}

	public String getreport_doors_wood_panel() {
		return this.report_doors_wood_panel;
	}

	public String getreport_doors_pvc() {
		return this.report_doors_pvc;
	}

	public String getreport_doors_analok_sliding() {
		return this.report_doors_analok_sliding;
	}

	public String getreport_doors_screen_door() {
		return this.report_doors_screen_door;
	}

	public String getreport_doors_flush() {
		return this.report_doors_flush;
	}

	public String getreport_doors_molded_door() {
		return this.report_doors_molded_door;
	}

	public String getreport_doors_aluminum_sliding() {
		return this.report_doors_aluminum_sliding;
	}

	public String getreport_doors_flush_french() {
		return this.report_doors_flush_french;
	}

	public String getreport_doors_other() {
		return this.report_doors_other;
	}

	public String getreport_doors_other_value() {
		return this.report_doors_other_value;
	}

	public String getreport_ceiling_plywood() {
		return this.report_ceiling_plywood;
	}

	public String getreport_ceiling_painted_gypsum() {
		return this.report_ceiling_painted_gypsum;
	}

	public String getreport_ceiling_soffit_slab() {
		return this.report_ceiling_soffit_slab;
	}

	public String getreport_ceiling_metal_deck() {
		return this.report_ceiling_metal_deck;
	}

	public String getreport_ceiling_hardiflex() {
		return this.report_ceiling_hardiflex;
	}

	public String getreport_ceiling_plywood_tg() {
		return this.report_ceiling_plywood_tg;
	}

	public String getreport_ceiling_plywood_pvc() {
		return this.report_ceiling_plywood_pvc;
	}

	public String getreport_ceiling_painted() {
		return this.report_ceiling_painted;
	}

	public String getreport_ceiling_with_cornice() {
		return this.report_ceiling_with_cornice;
	}

	public String getreport_ceiling_with_moulding() {
		return this.report_ceiling_with_moulding;
	}

	public String getreport_ceiling_drop_ceiling() {
		return this.report_ceiling_drop_ceiling;
	}

	public String getreport_ceiling_other() {
		return this.report_ceiling_other;
	}

	public String getreport_ceiling_other_value() {
		return this.report_ceiling_other_value;
	}

	public String getreport_roof_pre_painted() {
		return this.report_roof_pre_painted;
	}

	public String getreport_roof_rib_type() {
		return this.report_roof_rib_type;
	}

	public String getreport_roof_tilespan() {
		return this.report_roof_tilespan;
	}

	public String getreport_roof_tegula_asphalt() {
		return this.report_roof_tegula_asphalt;
	}

	public String getreport_roof_tegula_longspan() {
		return this.report_roof_tegula_longspan;
	}

	public String getreport_roof_tegula_gi() {
		return this.report_roof_tegula_gi;
	}

	public String getreport_roof_steel_concrete() {
		return this.report_roof_steel_concrete;
	}

	public String getreport_roof_polycarbonate() {
		return this.report_roof_polycarbonate;
	}

	public String getreport_roof_on_steel_trusses() {
		return this.report_roof_on_steel_trusses;
	}

	public String getreport_roof_on_wooden_trusses() {
		return this.report_roof_on_wooden_trusses;
	}

	public String getreport_roof_other() {
		return this.report_roof_other;
	}

	public String getreport_roof_other_value() {
		return this.report_roof_other_value;
	}

	public String getreport_valuation_total_area() {
		return this.report_valuation_total_area;
	}

	public String getreport_valuation_total_proj_cost() {
		return this.report_valuation_total_proj_cost;
	}

	public String getreport_valuation_remarks() {
		return this.report_valuation_remarks;
	}
	
//setters
	public void setreport_date_inspected_month(
			String report_date_inspected_month) {
		this.report_date_inspected_month = report_date_inspected_month;
	}

	public void setreport_date_inspected_day(String report_date_inspected_day) {
		this.report_date_inspected_day = report_date_inspected_day;
	}

	public void setreport_date_inspected_year(String report_date_inspected_year) {
		this.report_date_inspected_year = report_date_inspected_year;
	}

	public void setreport_time_inspected(String report_time_inspected) {
		this.report_time_inspected = report_time_inspected;
	}

	public void setreport_project_type(String report_project_type) {
		this.report_project_type = report_project_type;
	}

	public void setreport_floor_area(String report_floor_area) {
		this.report_floor_area = report_floor_area;
	}

	public void setreport_storeys(String report_storeys) {
		this.report_storeys = report_storeys;
	}

	public void setreport_expected_economic_life(
			String report_expected_economic_life) {
		this.report_expected_economic_life = report_expected_economic_life;
	}

	public void setreport_type_of_housing_unit(
			String report_type_of_housing_unit) {
		this.report_type_of_housing_unit = report_type_of_housing_unit;
	}

	public void setreport_const_type_reinforced_concrete(
			String report_const_type_reinforced_concrete) {
		this.report_const_type_reinforced_concrete = report_const_type_reinforced_concrete;
	}

	public void setreport_const_type_semi_concrete(
			String report_const_type_semi_concrete) {
		this.report_const_type_semi_concrete = report_const_type_semi_concrete;
	}

	public void setreport_const_type_mixed_materials(
			String report_const_type_mixed_materials) {
		this.report_const_type_mixed_materials = report_const_type_mixed_materials;
	}

	public void setreport_foundation_concrete(String report_foundation_concrete) {
		this.report_foundation_concrete = report_foundation_concrete;
	}

	public void setreport_foundation_other(String report_foundation_other) {
		this.report_foundation_other = report_foundation_other;
	}

	public void setreport_foundation_other_value(
			String report_foundation_other_value) {
		this.report_foundation_other_value = report_foundation_other_value;
	}

	public void setreport_post_concrete(String report_post_concrete) {
		this.report_post_concrete = report_post_concrete;
	}

	public void setreport_post_concrete_timber(
			String report_post_concrete_timber) {
		this.report_post_concrete_timber = report_post_concrete_timber;
	}

	public void setreport_post_steel(String report_post_steel) {
		this.report_post_steel = report_post_steel;
	}

	public void setreport_post_other(String report_post_other) {
		this.report_post_other = report_post_other;
	}

	public void setreport_post_other_value(String report_post_other_value) {
		this.report_post_other_value = report_post_other_value;
	}

	public void setreport_beams_concrete(String report_beams_concrete) {
		this.report_beams_concrete = report_beams_concrete;
	}

	public void setreport_beams_timber(String report_beams_timber) {
		this.report_beams_timber = report_beams_timber;
	}

	public void setreport_beams_steel(String report_beams_steel) {
		this.report_beams_steel = report_beams_steel;
	}

	public void setreport_beams_other(String report_beams_other) {
		this.report_beams_other = report_beams_other;
	}

	public void setreport_beams_other_value(String report_beams_other_value) {
		this.report_beams_other_value = report_beams_other_value;
	}

	public void setreport_floors_concrete(String report_floors_concrete) {
		this.report_floors_concrete = report_floors_concrete;
	}

	public void setreport_floors_tiles(String report_floors_tiles) {
		this.report_floors_tiles = report_floors_tiles;
	}

	public void setreport_floors_tiles_cement(String report_floors_tiles_cement) {
		this.report_floors_tiles_cement = report_floors_tiles_cement;
	}

	public void setreport_floors_laminated_wood(
			String report_floors_laminated_wood) {
		this.report_floors_laminated_wood = report_floors_laminated_wood;
	}

	public void setreport_floors_ceramic_tiles(
			String report_floors_ceramic_tiles) {
		this.report_floors_ceramic_tiles = report_floors_ceramic_tiles;
	}

	public void setreport_floors_wood_planks(String report_floors_wood_planks) {
		this.report_floors_wood_planks = report_floors_wood_planks;
	}

	public void setreport_floors_marble_washout(
			String report_floors_marble_washout) {
		this.report_floors_marble_washout = report_floors_marble_washout;
	}

	public void setreport_floors_concrete_boards(
			String report_floors_concrete_boards) {
		this.report_floors_concrete_boards = report_floors_concrete_boards;
	}

	public void setreport_floors_granite_tiles(
			String report_floors_granite_tiles) {
		this.report_floors_granite_tiles = report_floors_granite_tiles;
	}

	public void setreport_floors_marble_wood(String report_floors_marble_wood) {
		this.report_floors_marble_wood = report_floors_marble_wood;
	}

	public void setreport_floors_carpet(String report_floors_carpet) {
		this.report_floors_carpet = report_floors_carpet;
	}

	public void setreport_floors_other(String report_floors_other) {
		this.report_floors_other = report_floors_other;
	}

	public void setreport_floors_other_value(String report_floors_other_value) {
		this.report_floors_other_value = report_floors_other_value;
	}

	public void setreport_walls_chb(String report_walls_chb) {
		this.report_walls_chb = report_walls_chb;
	}

	public void setreport_walls_chb_cement(String report_walls_chb_cement) {
		this.report_walls_chb_cement = report_walls_chb_cement;
	}

	public void setreport_walls_anay(String report_walls_anay) {
		this.report_walls_anay = report_walls_anay;
	}

	public void setreport_walls_chb_wood(String report_walls_chb_wood) {
		this.report_walls_chb_wood = report_walls_chb_wood;
	}

	public void setreport_walls_precast(String report_walls_precast) {
		this.report_walls_precast = report_walls_precast;
	}

	public void setreport_walls_decorative_stone(
			String report_walls_decorative_stone) {
		this.report_walls_decorative_stone = report_walls_decorative_stone;
	}

	public void setreport_walls_adobe(String report_walls_adobe) {
		this.report_walls_adobe = report_walls_adobe;
	}

	public void setreport_walls_ceramic_tiles(String report_walls_ceramic_tiles) {
		this.report_walls_ceramic_tiles = report_walls_ceramic_tiles;
	}

	public void setreport_walls_cast_in_place(String report_walls_cast_in_place) {
		this.report_walls_cast_in_place = report_walls_cast_in_place;
	}

	public void setreport_walls_sandblast(String report_walls_sandblast) {
		this.report_walls_sandblast = report_walls_sandblast;
	}

	public void setreport_walls_mactan_stone(String report_walls_mactan_stone) {
		this.report_walls_mactan_stone = report_walls_mactan_stone;
	}

	public void setreport_walls_painted(String report_walls_painted) {
		this.report_walls_painted = report_walls_painted;
	}

	public void setreport_walls_other(String report_walls_other) {
		this.report_walls_other = report_walls_other;
	}

	public void setreport_walls_other_value(String report_walls_other_value) {
		this.report_walls_other_value = report_walls_other_value;
	}

	public void setreport_partitions_chb(String report_partitions_chb) {
		this.report_partitions_chb = report_partitions_chb;
	}

	public void setreport_partitions_painted_cement(
			String report_partitions_painted_cement) {
		this.report_partitions_painted_cement = report_partitions_painted_cement;
	}

	public void setreport_partitions_anay(String report_partitions_anay) {
		this.report_partitions_anay = report_partitions_anay;
	}

	public void setreport_partitions_wood_boards(
			String report_partitions_wood_boards) {
		this.report_partitions_wood_boards = report_partitions_wood_boards;
	}

	public void setreport_partitions_precast(String report_partitions_precast) {
		this.report_partitions_precast = report_partitions_precast;
	}

	public void setreport_partitions_decorative_stone(
			String report_partitions_decorative_stone) {
		this.report_partitions_decorative_stone = report_partitions_decorative_stone;
	}

	public void setreport_partitions_adobe(String report_partitions_adobe) {
		this.report_partitions_adobe = report_partitions_adobe;
	}

	public void setreport_partitions_granite(String report_partitions_granite) {
		this.report_partitions_granite = report_partitions_granite;
	}

	public void setreport_partitions_cast_in_place(
			String report_partitions_cast_in_place) {
		this.report_partitions_cast_in_place = report_partitions_cast_in_place;
	}

	public void setreport_partitions_sandblast(
			String report_partitions_sandblast) {
		this.report_partitions_sandblast = report_partitions_sandblast;
	}

	public void setreport_partitions_mactan_stone(
			String report_partitions_mactan_stone) {
		this.report_partitions_mactan_stone = report_partitions_mactan_stone;
	}

	public void setreport_partitions_ceramic_tiles(
			String report_partitions_ceramic_tiles) {
		this.report_partitions_ceramic_tiles = report_partitions_ceramic_tiles;
	}

	public void setreport_partitions_chb_plywood(
			String report_partitions_chb_plywood) {
		this.report_partitions_chb_plywood = report_partitions_chb_plywood;
	}

	public void setreport_partitions_hardiflex(
			String report_partitions_hardiflex) {
		this.report_partitions_hardiflex = report_partitions_hardiflex;
	}

	public void setreport_partitions_wallpaper(
			String report_partitions_wallpaper) {
		this.report_partitions_wallpaper = report_partitions_wallpaper;
	}

	public void setreport_partitions_painted(String report_partitions_painted) {
		this.report_partitions_painted = report_partitions_painted;
	}

	public void setreport_partitions_other(String report_partitions_other) {
		this.report_partitions_other = report_partitions_other;
	}

	public void setreport_partitions_other_value(
			String report_partitions_other_value) {
		this.report_partitions_other_value = report_partitions_other_value;
	}

	public void setreport_windows_steel_casement(
			String report_windows_steel_casement) {
		this.report_windows_steel_casement = report_windows_steel_casement;
	}

	public void setreport_windows_fixed_view(String report_windows_fixed_view) {
		this.report_windows_fixed_view = report_windows_fixed_view;
	}

	public void setreport_windows_analok_sliding(
			String report_windows_analok_sliding) {
		this.report_windows_analok_sliding = report_windows_analok_sliding;
	}

	public void setreport_windows_alum_glass(String report_windows_alum_glass) {
		this.report_windows_alum_glass = report_windows_alum_glass;
	}

	public void setreport_windows_aluminum_sliding(
			String report_windows_aluminum_sliding) {
		this.report_windows_aluminum_sliding = report_windows_aluminum_sliding;
	}

	public void setreport_windows_awning_type(String report_windows_awning_type) {
		this.report_windows_awning_type = report_windows_awning_type;
	}

	public void setreport_windows_powder_coated(
			String report_windows_powder_coated) {
		this.report_windows_powder_coated = report_windows_powder_coated;
	}

	public void setreport_windows_wooden_frame(
			String report_windows_wooden_frame) {
		this.report_windows_wooden_frame = report_windows_wooden_frame;
	}

	public void setreport_windows_other(String report_windows_other) {
		this.report_windows_other = report_windows_other;
	}

	public void setreport_windows_other_value(String report_windows_other_value) {
		this.report_windows_other_value = report_windows_other_value;
	}

	public void setreport_doors_wood_panel(String report_doors_wood_panel) {
		this.report_doors_wood_panel = report_doors_wood_panel;
	}

	public void setreport_doors_pvc(String report_doors_pvc) {
		this.report_doors_pvc = report_doors_pvc;
	}

	public void setreport_doors_analok_sliding(
			String report_doors_analok_sliding) {
		this.report_doors_analok_sliding = report_doors_analok_sliding;
	}

	public void setreport_doors_screen_door(String report_doors_screen_door) {
		this.report_doors_screen_door = report_doors_screen_door;
	}

	public void setreport_doors_flush(String report_doors_flush) {
		this.report_doors_flush = report_doors_flush;
	}

	public void setreport_doors_molded_door(String report_doors_molded_door) {
		this.report_doors_molded_door = report_doors_molded_door;
	}

	public void setreport_doors_aluminum_sliding(
			String report_doors_aluminum_sliding) {
		this.report_doors_aluminum_sliding = report_doors_aluminum_sliding;
	}

	public void setreport_doors_flush_french(String report_doors_flush_french) {
		this.report_doors_flush_french = report_doors_flush_french;
	}

	public void setreport_doors_other(String report_doors_other) {
		this.report_doors_other = report_doors_other;
	}

	public void setreport_doors_other_value(String report_doors_other_value) {
		this.report_doors_other_value = report_doors_other_value;
	}

	public void setreport_ceiling_plywood(String report_ceiling_plywood) {
		this.report_ceiling_plywood = report_ceiling_plywood;
	}

	public void setreport_ceiling_painted_gypsum(
			String report_ceiling_painted_gypsum) {
		this.report_ceiling_painted_gypsum = report_ceiling_painted_gypsum;
	}

	public void setreport_ceiling_soffit_slab(String report_ceiling_soffit_slab) {
		this.report_ceiling_soffit_slab = report_ceiling_soffit_slab;
	}

	public void setreport_ceiling_metal_deck(String report_ceiling_metal_deck) {
		this.report_ceiling_metal_deck = report_ceiling_metal_deck;
	}

	public void setreport_ceiling_hardiflex(String report_ceiling_hardiflex) {
		this.report_ceiling_hardiflex = report_ceiling_hardiflex;
	}

	public void setreport_ceiling_plywood_tg(String report_ceiling_plywood_tg) {
		this.report_ceiling_plywood_tg = report_ceiling_plywood_tg;
	}

	public void setreport_ceiling_plywood_pvc(String report_ceiling_plywood_pvc) {
		this.report_ceiling_plywood_pvc = report_ceiling_plywood_pvc;
	}

	public void setreport_ceiling_painted(String report_ceiling_painted) {
		this.report_ceiling_painted = report_ceiling_painted;
	}

	public void setreport_ceiling_with_cornice(
			String report_ceiling_with_cornice) {
		this.report_ceiling_with_cornice = report_ceiling_with_cornice;
	}

	public void setreport_ceiling_with_moulding(
			String report_ceiling_with_moulding) {
		this.report_ceiling_with_moulding = report_ceiling_with_moulding;
	}

	public void setreport_ceiling_drop_ceiling(
			String report_ceiling_drop_ceiling) {
		this.report_ceiling_drop_ceiling = report_ceiling_drop_ceiling;
	}

	public void setreport_ceiling_other(String report_ceiling_other) {
		this.report_ceiling_other = report_ceiling_other;
	}

	public void setreport_ceiling_other_value(String report_ceiling_other_value) {
		this.report_ceiling_other_value = report_ceiling_other_value;
	}

	public void setreport_roof_pre_painted(String report_roof_pre_painted) {
		this.report_roof_pre_painted = report_roof_pre_painted;
	}

	public void setreport_roof_rib_type(String report_roof_rib_type) {
		this.report_roof_rib_type = report_roof_rib_type;
	}

	public void setreport_roof_tilespan(String report_roof_tilespan) {
		this.report_roof_tilespan = report_roof_tilespan;
	}

	public void setreport_roof_tegula_asphalt(String report_roof_tegula_asphalt) {
		this.report_roof_tegula_asphalt = report_roof_tegula_asphalt;
	}

	public void setreport_roof_tegula_longspan(
			String report_roof_tegula_longspan) {
		this.report_roof_tegula_longspan = report_roof_tegula_longspan;
	}

	public void setreport_roof_tegula_gi(String report_roof_tegula_gi) {
		this.report_roof_tegula_gi = report_roof_tegula_gi;
	}

	public void setreport_roof_steel_concrete(String report_roof_steel_concrete) {
		this.report_roof_steel_concrete = report_roof_steel_concrete;
	}

	public void setreport_roof_polycarbonate(String report_roof_polycarbonate) {
		this.report_roof_polycarbonate = report_roof_polycarbonate;
	}

	public void setreport_roof_on_steel_trusses(
			String report_roof_on_steel_trusses) {
		this.report_roof_on_steel_trusses = report_roof_on_steel_trusses;
	}

	public void setreport_roof_on_wooden_trusses(
			String report_roof_on_wooden_trusses) {
		this.report_roof_on_wooden_trusses = report_roof_on_wooden_trusses;
	}

	public void setreport_roof_other(String report_roof_other) {
		this.report_roof_other = report_roof_other;
	}

	public void setreport_roof_other_value(String report_roof_other_value) {
		this.report_roof_other_value = report_roof_other_value;
	}

	public void setreport_valuation_total_area(
			String report_valuation_total_area) {
		this.report_valuation_total_area = report_valuation_total_area;
	}

	public void setreport_valuation_total_proj_cost(
			String report_valuation_total_proj_cost) {
		this.report_valuation_total_proj_cost = report_valuation_total_proj_cost;
	}

	public void setreport_valuation_remarks(String report_valuation_remarks) {
		this.report_valuation_remarks = report_valuation_remarks;
	}
}
