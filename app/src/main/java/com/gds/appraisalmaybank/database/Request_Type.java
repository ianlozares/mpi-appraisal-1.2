package com.gds.appraisalmaybank.database;

public class Request_Type {

	// private variables
	int id;
	String rf_id;
	String rt_control_no;
	String rt_nature_of_appraisal;
	String rt_appraisal_type;
	String rt_pref_ins_date1_month;
	String rt_pref_ins_date1_day;
	String rt_pref_ins_date1_year;
	String rt_pref_ins_date2_month;
	String rt_pref_ins_date2_day;
	String rt_pref_ins_date2_year;
	String rt_mv_year;
	String rt_mv_make;
	String rt_mv_model;
	String rt_mv_odometer;
	String rt_mv_vin;
	String rt_me_quantity;
	String rt_me_type;
	String rt_me_manufacturer;
	String rt_me_model;
	String rt_me_serial;
	String rt_me_age;
	String rt_me_condition;

	// Empty constructor
	public Request_Type() {

	}

	// constructor
	public Request_Type(int id, String rf_id, String rt_control_no,
			String rt_nature_of_appraisal, String rt_appraisal_type,
			String rt_pref_ins_date1_month, String rt_pref_ins_date1_day,
			String rt_pref_ins_date1_year, String rt_pref_ins_date2_month,
			String rt_pref_ins_date2_day, String rt_pref_ins_date2_year,
			String rt_mv_year, String rt_mv_make, String rt_mv_model,
			String rt_mv_odometer, String rt_mv_vin, String rt_me_quantity,
			String rt_me_type, String rt_me_manufacturer, String rt_me_model,
			String rt_me_serial, String rt_me_age, String rt_me_condition) {
		this.id = id;
		this.rf_id = rf_id;
		this.rt_control_no = rt_control_no;
		this.rt_nature_of_appraisal = rt_nature_of_appraisal;
		this.rt_appraisal_type = rt_appraisal_type;
		this.rt_pref_ins_date1_month = rt_pref_ins_date1_month;
		this.rt_pref_ins_date1_day = rt_pref_ins_date1_day;
		this.rt_pref_ins_date1_year = rt_pref_ins_date1_year;
		this.rt_pref_ins_date2_month = rt_pref_ins_date2_month;
		this.rt_pref_ins_date2_day = rt_pref_ins_date2_day;
		this.rt_pref_ins_date2_year = rt_pref_ins_date2_year;
		this.rt_mv_year = rt_mv_year;
		this.rt_mv_make = rt_mv_make;
		this.rt_mv_model = rt_mv_model;
		this.rt_mv_odometer = rt_mv_odometer;
		this.rt_mv_vin = rt_mv_vin;
		this.rt_me_quantity = rt_me_quantity;
		this.rt_me_type = rt_me_type;
		this.rt_me_manufacturer = rt_me_manufacturer;
		this.rt_me_model = rt_me_model;
		this.rt_me_serial = rt_me_serial;
		this.rt_me_age = rt_me_age;
		this.rt_me_condition = rt_me_condition;
	}

	// constructor
	public Request_Type(String rf_id, String rt_control_no,
			String rt_nature_of_appraisal, String rt_cost_center,
			String rt_appraisal_type, String rt_pref_ins_date1_month,
			String rt_pref_ins_date1_day, String rt_pref_ins_date1_year,
			String rt_pref_ins_date2_month, String rt_pref_ins_date2_day,
			String rt_pref_ins_date2_year, String rt_mv_year,
			String rt_mv_make, String rt_mv_model, String rt_mv_odometer,
			String rt_mv_vin, String rt_me_quantity, String rt_me_type,
			String rt_me_manufacturer, String rt_me_model, String rt_me_serial,
			String rt_me_age, String rt_me_condition) {

		this.rf_id = rf_id;
		this.rt_control_no = rt_control_no;
		this.rt_nature_of_appraisal = rt_nature_of_appraisal;
		this.rt_appraisal_type = rt_appraisal_type;
		this.rt_pref_ins_date1_month = rt_pref_ins_date1_month;
		this.rt_pref_ins_date1_day = rt_pref_ins_date1_day;
		this.rt_pref_ins_date1_year = rt_pref_ins_date1_year;
		this.rt_pref_ins_date2_month = rt_pref_ins_date2_month;
		this.rt_pref_ins_date2_day = rt_pref_ins_date2_day;
		this.rt_pref_ins_date2_year = rt_pref_ins_date2_year;
		this.rt_mv_year = rt_mv_year;
		this.rt_mv_make = rt_mv_make;
		this.rt_mv_model = rt_mv_model;
		this.rt_mv_odometer = rt_mv_odometer;
		this.rt_mv_vin = rt_mv_vin;
		this.rt_me_quantity = rt_me_quantity;
		this.rt_me_type = rt_me_type;
		this.rt_me_manufacturer = rt_me_manufacturer;
		this.rt_me_model = rt_me_model;
		this.rt_me_serial = rt_me_serial;
		this.rt_me_age = rt_me_age;
		this.rt_me_condition = rt_me_condition;
	}

	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	// getting rf_id
	public String getrf_id() {
		return this.rf_id;
	}

	// setting rf_id
	public void setrf_id(String rf_id) {
		this.rf_id = rf_id;
	}

	// getting rt_control_no
	public String getrt_control_no() {
		return this.rt_control_no;
	}

	// setting rt_control_no
	public void setrt_control_no(String rt_control_no) {
		this.rt_control_no = rt_control_no;
	}

	// getting rt_nature_of_appraisal
	public String getrt_nature_of_appraisal() {
		return this.rt_nature_of_appraisal;
	}

	// setting rt_nature_of_appraisal
	public void setrt_nature_of_appraisal(String rt_nature_of_appraisal) {
		this.rt_nature_of_appraisal = rt_nature_of_appraisal;
	}

	// getting rt_appraisal_type
	public String getrt_appraisal_type() {
		return this.rt_appraisal_type;
	}

	// setting rt_appraisal_type
	public void setrt_appraisal_type(String rt_appraisal_type) {
		this.rt_appraisal_type = rt_appraisal_type;
	}

	// getting rt_pref_ins_date1_month
	public String getrt_pref_ins_date1_month() {
		return this.rt_pref_ins_date1_month;
	}

	// setting rt_pref_ins_date1_month
	public void setrt_pref_ins_date1_month(String rt_pref_ins_date1_month) {
		this.rt_pref_ins_date1_month = rt_pref_ins_date1_month;
	}

	// getting rt_pref_ins_date1_day
	public String getrt_pref_ins_date1_day() {
		return this.rt_pref_ins_date1_day;
	}

	// setting rt_pref_ins_date1_day
	public void setrt_pref_ins_date1_day(String rt_pref_ins_date1_day) {
		this.rt_pref_ins_date1_day = rt_pref_ins_date1_day;
	}

	// getting rt_pref_ins_date1_year
	public String getrt_pref_ins_date1_year() {
		return this.rt_pref_ins_date1_year;
	}

	// setting rt_pref_ins_date1_year
	public void setrt_pref_ins_date1_year(String rt_pref_ins_date1_year) {
		this.rt_pref_ins_date1_year = rt_pref_ins_date1_year;
	}

	// getting rt_pref_ins_date2_month
	public String getrt_pref_ins_date2_month() {
		return this.rt_pref_ins_date2_month;
	}

	// setting rt_pref_ins_date2_month
	public void setrt_pref_ins_date2_month(String rt_pref_ins_date2_month) {
		this.rt_pref_ins_date2_month = rt_pref_ins_date2_month;
	}

	// getting rt_pref_ins_date2_day
	public String getrt_pref_ins_date2_day() {
		return this.rt_pref_ins_date2_day;
	}

	// setting rt_pref_ins_date2_day
	public void setrt_pref_ins_date2_day(String rt_pref_ins_date2_day) {
		this.rt_pref_ins_date2_day = rt_pref_ins_date2_day;
	}

	// getting rt_pref_ins_date2_year
	public String getrt_pref_ins_date2_year() {
		return this.rt_pref_ins_date2_year;
	}

	// setting rt_pref_ins_date2_year
	public void setrt_pref_ins_date2_year(String rt_pref_ins_date2_year) {
		this.rt_pref_ins_date2_year = rt_pref_ins_date2_year;
	}

	// getting rt_mv_year
	public String getrt_mv_year() {
		return this.rt_mv_year;
	}

	// setting rt_mv_year
	public void setrt_mv_year(String rt_mv_year) {
		this.rt_mv_year = rt_mv_year;
	}

	// getting rt_mv_make
	public String getrt_mv_make() {
		return this.rt_mv_make;
	}

	// setting rt_mv_make
	public void setrt_mv_make(String rt_mv_make) {
		this.rt_mv_make = rt_mv_make;
	}

	// getting rt_mv_model
	public String getrt_mv_model() {
		return this.rt_mv_model;
	}

	// setting rt_mv_model
	public void setrt_mv_model(String rt_mv_model) {
		this.rt_mv_model = rt_mv_model;
	}

	// getting rt_mv_odometer
	public String getrt_mv_odometer() {
		return this.rt_mv_odometer;
	}

	// setting rt_mv_odometer
	public void setrt_mv_odometer(String rt_mv_odometer) {
		this.rt_mv_odometer = rt_mv_odometer;
	}

	// getting rt_mv_vin
	public String getrt_mv_vin() {
		return this.rt_mv_vin;
	}

	// setting rt_mv_vin
	public void setrt_mv_vin(String rt_mv_vin) {
		this.rt_mv_vin = rt_mv_vin;
	}

	// getting rt_me_quantity
	public String getrt_me_quantity() {
		return this.rt_me_quantity;
	}

	// setting rt_me_quantity
	public void setrt_me_quantity(String rt_me_quantity) {
		this.rt_me_quantity = rt_me_quantity;
	}

	// getting rt_me_type
	public String getrt_me_type() {
		return this.rt_me_type;
	}

	// setting rt_me_type
	public void setrt_me_type(String rt_me_type) {
		this.rt_me_type = rt_me_type;
	}

	// getting rt_me_manufacturer
	public String getrt_me_manufacturer() {
		return this.rt_me_manufacturer;
	}

	// setting rt_me_manufacturer
	public void setrt_me_manufacturer(String rt_me_manufacturer) {
		this.rt_me_manufacturer = rt_me_manufacturer;
	}

	// getting rt_me_model
	public String getrt_me_model() {
		return this.rt_me_model;
	}

	// setting rt_me_model
	public void setrt_me_model(String rt_me_model) {
		this.rt_me_model = rt_me_model;
	}

	// getting rt_me_serial
	public String getrt_me_serial() {
		return this.rt_me_serial;
	}

	// setting rt_me_serial
	public void setrt_me_serial(String rt_me_serial) {
		this.rt_me_serial = rt_me_serial;
	}

	// getting rt_me_age
	public String getrt_me_age() {
		return this.rt_me_age;
	}

	// setting rt_me_age
	public void setrt_me_age(String rt_me_age) {
		this.rt_me_age = rt_me_age;
	}

	// getting rt_me_condition
	public String getrt_me_condition() {
		return this.rt_me_condition;
	}

	// setting rt_me_condition
	public void setrt_me_condition(String rt_me_condition) {
		this.rt_me_condition = rt_me_condition;
	}
}
