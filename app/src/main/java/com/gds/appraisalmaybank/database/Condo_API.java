package com.gds.appraisalmaybank.database;

public class Condo_API {

	// private variables
	int id;
	String record_id;
	String report_date_inspected_month;
	String report_date_inspected_day;
	String report_date_inspected_year;
	String report_id_admin;
	String report_id_unit_numbering;
	String report_id_bldg_plan;
	String report_unitclass_per_tax_dec;
	String report_unitclass_actual_usage;
	String report_unitclass_neighborhood;
	String report_unitclass_highest_best_use;
	String report_trans_jeep;
	String report_trans_bus;
	String report_trans_taxi;
	String report_trans_tricycle;
	String report_faci_function_room;
	String report_faci_gym;
	String report_faci_sauna;
	String report_faci_pool;
	String report_faci_fire_alarm;
	String report_faci_cctv;
	String report_faci_elevator;
	String report_util_electricity;
	String report_util_water;
	String report_util_telephone;
	String report_util_garbage;
	String report_landmarks_1;
	String report_landmarks_2;
	String report_landmarks_3;
	String report_landmarks_4;
	String report_landmarks_5;
	String report_distance_1;
	String report_distance_2;
	String report_distance_3;
	String report_distance_4;
	String report_distance_5;
	String report_zonal_location;
	String report_zonal_lot_classification;
	String report_zonal_value;
	String report_final_value_total_appraised_value;
	String report_final_value_recommended_deductions_desc;
	String report_final_value_recommended_deductions_rate;
	String report_final_value_recommended_deductions_amt;
	String report_final_value_net_appraised_value;
	String report_final_value_quick_sale_value_rate;
	String report_final_value_quick_sale_value_amt;
	String report_factors_of_concern;
	String report_suggested_corrective_actions;
	String report_remarks;
	String report_requirements;
	String report_time_inspected;

	//late added fields (comparatives)
	String report_comp1_date_month;
	String report_comp1_date_day;
	String report_comp1_date_year;
	String report_comp1_source;
	String report_comp1_contact_no;
	String report_comp1_location;
	String report_comp1_area;
	String report_comp1_base_price;
	String report_comp1_price_sqm;
	String report_comp1_discount_rate;
	String report_comp1_discounts;
	String report_comp1_selling_price;
	String report_comp1_rec_location;
	String report_comp1_rec_unit_floor_location;
	String report_comp1_rec_orientation;
	String report_comp1_rec_size;
	String report_comp1_rec_unit_condition;
	String report_comp1_rec_amenities;
	String report_comp1_rec_unit_features;
	String report_comp1_concluded_adjustment;
	String report_comp1_adjusted_value;
	String report_comp2_date_month;
	String report_comp2_date_day;
	String report_comp2_date_year;
	String report_comp2_source;
	String report_comp2_contact_no;
	String report_comp2_location;
	String report_comp2_area;
	String report_comp2_base_price;
	String report_comp2_price_sqm;
	String report_comp2_discount_rate;
	String report_comp2_discounts;
	String report_comp2_selling_price;
	String report_comp2_rec_location;
	String report_comp2_rec_unit_floor_location;
	String report_comp2_rec_orientation;
	String report_comp2_rec_size;
	String report_comp2_rec_unit_condition;
	String report_comp2_rec_amenities;
	String report_comp2_rec_unit_features;
	String report_comp2_concluded_adjustment;
	String report_comp2_adjusted_value;
	String report_comp3_date_month;
	String report_comp3_date_day;
	String report_comp3_date_year;
	String report_comp3_source;
	String report_comp3_contact_no;
	String report_comp3_location;
	String report_comp3_area;
	String report_comp3_base_price;
	String report_comp3_price_sqm;
	String report_comp3_discount_rate;
	String report_comp3_discounts;
	String report_comp3_selling_price;
	String report_comp3_rec_location;
	String report_comp3_rec_unit_floor_location;
	String report_comp3_rec_orientation;
	String report_comp3_rec_size;
	String report_comp3_rec_unit_condition;
	String report_comp3_rec_amenities;
	String report_comp3_rec_unit_features;
	String report_comp3_concluded_adjustment;
	String report_comp3_adjusted_value;
	String report_comp4_date_month;
	String report_comp4_date_day;
	String report_comp4_date_year;
	String report_comp4_source;
	String report_comp4_contact_no;
	String report_comp4_location;
	String report_comp4_area;
	String report_comp4_base_price;
	String report_comp4_price_sqm;
	String report_comp4_discount_rate;
	String report_comp4_discounts;
	String report_comp4_selling_price;
	String report_comp4_rec_location;
	String report_comp4_rec_unit_floor_location;
	String report_comp4_rec_orientation;
	String report_comp4_rec_size;
	String report_comp4_rec_unit_condition;
	String report_comp4_rec_amenities;
	String report_comp4_rec_unit_features;
	String report_comp4_concluded_adjustment;
	String report_comp4_adjusted_value;
	String report_comp5_date_month;
	String report_comp5_date_day;
	String report_comp5_date_year;
	String report_comp5_source;
	String report_comp5_contact_no;
	String report_comp5_location;
	String report_comp5_area;
	String report_comp5_base_price;
	String report_comp5_price_sqm;
	String report_comp5_discount_rate;
	String report_comp5_discounts;
	String report_comp5_selling_price;
	String report_comp5_rec_location;
	String report_comp5_rec_unit_floor_location;
	String report_comp5_rec_orientation;
	String report_comp5_rec_size;
	String report_comp5_rec_unit_condition;
	String report_comp5_rec_amenities;
	String report_comp5_rec_unit_features;
	String report_comp5_concluded_adjustment;
	String report_comp5_adjusted_value;
	String report_comp_average;
	String report_comp_rounded_to;
	
	String report_comp1_rec_time_element;
	String report_comp2_rec_time_element;
	String report_comp3_rec_time_element;
	String report_comp4_rec_time_element;
	String report_comp5_rec_time_element;
	String report_comp1_remarks;
	String report_comp2_remarks;
	String report_comp3_remarks;
	String report_comp4_remarks;
	String report_comp5_remarks;
	//Added Field Added from IAN
	String report_prev_date;
	String report_prev_appraiser;
	String report_prev_total_appraised_value;
	// Empty constructor
	public Condo_API() {

	}
	// constructor with int recordid only for parameter
	public Condo_API(String record_id) {
		this.record_id = record_id;
	}
	
	public Condo_API(int id, String record_id,
			String report_date_inspected_month,
			String report_date_inspected_day,
			String report_date_inspected_year, String report_id_admin,
			String report_id_unit_numbering, String report_id_bldg_plan,
			String report_unitclass_per_tax_dec,
			String report_unitclass_actual_usage,
			String report_unitclass_neighborhood,
			String report_unitclass_highest_best_use, String report_trans_jeep,
			String report_trans_bus, String report_trans_taxi,
			String report_trans_tricycle, String report_faci_function_room,
			String report_faci_gym, String report_faci_sauna,
			String report_faci_pool, String report_faci_fire_alarm,
			String report_faci_cctv, String report_faci_elevator,
			String report_util_electricity, String report_util_water,
			String report_util_telephone, String report_util_garbage,
			String report_landmarks_1, String report_landmarks_2,
			String report_landmarks_3, String report_landmarks_4,
			String report_landmarks_5, String report_distance_1,
			String report_distance_2, String report_distance_3,
			String report_distance_4, String report_distance_5,
			String report_zonal_location,
			String report_zonal_lot_classification, String report_zonal_value,
			String report_final_value_total_appraised_value,
			String report_final_value_recommended_deductions_desc,
			String report_final_value_recommended_deductions_rate,
			String report_final_value_recommended_deductions_amt,
			String report_final_value_net_appraised_value,
			String report_final_value_quick_sale_value_rate,
			String report_final_value_quick_sale_value_amt,
			String report_factors_of_concern,
			String report_suggested_corrective_actions, String report_remarks,
			String report_requirements, String report_time_inspected,
			String report_comp1_date_month,
			String report_comp1_date_day,
			String report_comp1_date_year,
			String report_comp1_source,
			String report_comp1_contact_no,
			String report_comp1_location,
			String report_comp1_area,
			String report_comp1_base_price,
			String report_comp1_price_sqm,
			String report_comp1_discount_rate,
			String report_comp1_discounts,
			String report_comp1_selling_price,
			String report_comp1_rec_location,
			String report_comp1_rec_unit_floor_location,
			String report_comp1_rec_orientation,
			String report_comp1_rec_size,
			String report_comp1_rec_unit_condition,
			String report_comp1_rec_amenities,
			String report_comp1_rec_unit_features,
			String report_comp1_concluded_adjustment,
			String report_comp1_adjusted_value,
			String report_comp2_date_month,
			String report_comp2_date_day,
			String report_comp2_date_year,
			String report_comp2_source,
			String report_comp2_contact_no,
			String report_comp2_location,
			String report_comp2_area,
			String report_comp2_base_price,
			String report_comp2_price_sqm,
			String report_comp2_discount_rate,
			String report_comp2_discounts,
			String report_comp2_selling_price,
			String report_comp2_rec_location,
			String report_comp2_rec_unit_floor_location,
			String report_comp2_rec_orientation,
			String report_comp2_rec_size,
			String report_comp2_rec_unit_condition,
			String report_comp2_rec_amenities,
			String report_comp2_rec_unit_features,
			String report_comp2_concluded_adjustment,
			String report_comp2_adjusted_value,
			String report_comp3_date_month,
			String report_comp3_date_day,
			String report_comp3_date_year,
			String report_comp3_source,
			String report_comp3_contact_no,
			String report_comp3_location,
			String report_comp3_area,
			String report_comp3_base_price,
			String report_comp3_price_sqm,
			String report_comp3_discount_rate,
			String report_comp3_discounts,
			String report_comp3_selling_price,
			String report_comp3_rec_location,
			String report_comp3_rec_unit_floor_location,
			String report_comp3_rec_orientation,
			String report_comp3_rec_size,
			String report_comp3_rec_unit_condition,
			String report_comp3_rec_amenities,
			String report_comp3_rec_unit_features,
			String report_comp3_concluded_adjustment,
			String report_comp3_adjusted_value,
			String report_comp4_date_month,
			String report_comp4_date_day,
			String report_comp4_date_year,
			String report_comp4_source,
			String report_comp4_contact_no,
			String report_comp4_location,
			String report_comp4_area,
			String report_comp4_base_price,
			String report_comp4_price_sqm,
			String report_comp4_discount_rate,
			String report_comp4_discounts,
			String report_comp4_selling_price,
			String report_comp4_rec_location,
			String report_comp4_rec_unit_floor_location,
			String report_comp4_rec_orientation,
			String report_comp4_rec_size,
			String report_comp4_rec_unit_condition,
			String report_comp4_rec_amenities,
			String report_comp4_rec_unit_features,
			String report_comp4_concluded_adjustment,
			String report_comp4_adjusted_value,
			String report_comp5_date_month,
			String report_comp5_date_day,
			String report_comp5_date_year,
			String report_comp5_source,
			String report_comp5_contact_no,
			String report_comp5_location,
			String report_comp5_area,
			String report_comp5_base_price,
			String report_comp5_price_sqm,
			String report_comp5_discount_rate,
			String report_comp5_discounts,
			String report_comp5_selling_price,
			String report_comp5_rec_location,
			String report_comp5_rec_unit_floor_location,
			String report_comp5_rec_orientation,
			String report_comp5_rec_size,
			String report_comp5_rec_unit_condition,
			String report_comp5_rec_amenities,
			String report_comp5_rec_unit_features,
			String report_comp5_concluded_adjustment,
			String report_comp5_adjusted_value,
			String report_comp_average,
			String report_comp_rounded_to,
			String report_comp1_rec_time_element,
			String report_comp2_rec_time_element,
			String report_comp3_rec_time_element,
			String report_comp4_rec_time_element,
			String report_comp5_rec_time_element,
			String report_comp1_remarks,
			String report_comp2_remarks,
			String report_comp3_remarks,
			String report_comp4_remarks,
			String report_comp5_remarks,
					 //Added from IAN
					 String report_prev_date,
					 String report_prev_appraiser,
					 String report_prev_total_appraised_value) {
		this.id = id;
		this.record_id = record_id;
		this.report_date_inspected_month = report_date_inspected_month;
		this.report_date_inspected_day = report_date_inspected_day;
		this.report_date_inspected_year = report_date_inspected_year;
		this.report_id_admin = report_id_admin;
		this.report_id_unit_numbering = report_id_unit_numbering;
		this.report_id_bldg_plan = report_id_bldg_plan;
		this.report_unitclass_per_tax_dec = report_unitclass_per_tax_dec;
		this.report_unitclass_actual_usage = report_unitclass_actual_usage;
		this.report_unitclass_neighborhood = report_unitclass_neighborhood;
		this.report_unitclass_highest_best_use = report_unitclass_highest_best_use;
		this.report_trans_jeep = report_trans_jeep;
		this.report_trans_bus = report_trans_bus;
		this.report_trans_taxi = report_trans_taxi;
		this.report_trans_tricycle = report_trans_tricycle;
		this.report_faci_function_room = report_faci_function_room;
		this.report_faci_gym = report_faci_gym;
		this.report_faci_sauna = report_faci_sauna;
		this.report_faci_pool = report_faci_pool;
		this.report_faci_fire_alarm = report_faci_fire_alarm;
		this.report_faci_cctv = report_faci_cctv;
		this.report_faci_elevator = report_faci_elevator;
		this.report_util_electricity = report_util_electricity;
		this.report_util_water = report_util_water;
		this.report_util_telephone = report_util_telephone;
		this.report_util_garbage = report_util_garbage;
		this.report_landmarks_1 = report_landmarks_1;
		this.report_landmarks_2 = report_landmarks_2;
		this.report_landmarks_3 = report_landmarks_3;
		this.report_landmarks_4 = report_landmarks_4;
		this.report_landmarks_5 = report_landmarks_5;
		this.report_distance_1 = report_distance_1;
		this.report_distance_2 = report_distance_2;
		this.report_distance_3 = report_distance_3;
		this.report_distance_4 = report_distance_4;
		this.report_distance_5 = report_distance_5;
		this.report_zonal_location = report_zonal_location;
		this.report_zonal_lot_classification = report_zonal_lot_classification;
		this.report_zonal_value = report_zonal_value;
		this.report_final_value_total_appraised_value = report_final_value_total_appraised_value;
		this.report_final_value_recommended_deductions_desc = report_final_value_recommended_deductions_desc;
		this.report_final_value_recommended_deductions_rate = report_final_value_recommended_deductions_rate;
		this.report_final_value_recommended_deductions_amt = report_final_value_recommended_deductions_amt;
		this.report_final_value_net_appraised_value = report_final_value_net_appraised_value;
		this.report_final_value_quick_sale_value_rate = report_final_value_quick_sale_value_rate;
		this.report_final_value_quick_sale_value_amt = report_final_value_quick_sale_value_amt;
		this.report_factors_of_concern = report_factors_of_concern;
		this.report_suggested_corrective_actions = report_suggested_corrective_actions;
		this.report_remarks = report_remarks;
		this.report_requirements = report_requirements;
		this.report_time_inspected = report_time_inspected;
		this.report_comp1_date_month = report_comp1_date_month;
		this.report_comp1_date_day = report_comp1_date_day;
		this.report_comp1_date_year = report_comp1_date_year;
		this.report_comp1_source = report_comp1_source;
		this.report_comp1_contact_no = report_comp1_contact_no;
		this.report_comp1_location = report_comp1_location;
		this.report_comp1_area = report_comp1_area;
		this.report_comp1_base_price = report_comp1_base_price;
		this.report_comp1_price_sqm = report_comp1_price_sqm;
		this.report_comp1_discount_rate = report_comp1_discount_rate;
		this.report_comp1_discounts = report_comp1_discounts;
		this.report_comp1_selling_price = report_comp1_selling_price;
		this.report_comp1_rec_location = report_comp1_rec_location;
		this.report_comp1_rec_unit_floor_location = report_comp1_rec_unit_floor_location;
		this.report_comp1_rec_orientation = report_comp1_rec_orientation;
		this.report_comp1_rec_size = report_comp1_rec_size;
		this.report_comp1_rec_unit_condition = report_comp1_rec_unit_condition;
		this.report_comp1_rec_amenities = report_comp1_rec_amenities;
		this.report_comp1_rec_unit_features = report_comp1_rec_unit_features;
		this.report_comp1_concluded_adjustment = report_comp1_concluded_adjustment;
		this.report_comp1_adjusted_value = report_comp1_adjusted_value;
		this.report_comp2_date_month = report_comp2_date_month;
		this.report_comp2_date_day = report_comp2_date_day;
		this.report_comp2_date_year = report_comp2_date_year;
		this.report_comp2_source = report_comp2_source;
		this.report_comp2_contact_no = report_comp2_contact_no;
		this.report_comp2_location = report_comp2_location;
		this.report_comp2_area = report_comp2_area;
		this.report_comp2_base_price = report_comp2_base_price;
		this.report_comp2_price_sqm = report_comp2_price_sqm;
		this.report_comp2_discount_rate = report_comp2_discount_rate;
		this.report_comp2_discounts = report_comp2_discounts;
		this.report_comp2_selling_price = report_comp2_selling_price;
		this.report_comp2_rec_location = report_comp2_rec_location;
		this.report_comp2_rec_unit_floor_location = report_comp2_rec_unit_floor_location;
		this.report_comp2_rec_orientation = report_comp2_rec_orientation;
		this.report_comp2_rec_size = report_comp2_rec_size;
		this.report_comp2_rec_unit_condition = report_comp2_rec_unit_condition;
		this.report_comp2_rec_amenities = report_comp2_rec_amenities;
		this.report_comp2_rec_unit_features = report_comp2_rec_unit_features;
		this.report_comp2_concluded_adjustment = report_comp2_concluded_adjustment;
		this.report_comp2_adjusted_value = report_comp2_adjusted_value;
		this.report_comp3_date_month = report_comp3_date_month;
		this.report_comp3_date_day = report_comp3_date_day;
		this.report_comp3_date_year = report_comp3_date_year;
		this.report_comp3_source = report_comp3_source;
		this.report_comp3_contact_no = report_comp3_contact_no;
		this.report_comp3_location = report_comp3_location;
		this.report_comp3_area = report_comp3_area;
		this.report_comp3_base_price = report_comp3_base_price;
		this.report_comp3_price_sqm = report_comp3_price_sqm;
		this.report_comp3_discount_rate = report_comp3_discount_rate;
		this.report_comp3_discounts = report_comp3_discounts;
		this.report_comp3_selling_price = report_comp3_selling_price;
		this.report_comp3_rec_location = report_comp3_rec_location;
		this.report_comp3_rec_unit_floor_location = report_comp3_rec_unit_floor_location;
		this.report_comp3_rec_orientation = report_comp3_rec_orientation;
		this.report_comp3_rec_size = report_comp3_rec_size;
		this.report_comp3_rec_unit_condition = report_comp3_rec_unit_condition;
		this.report_comp3_rec_amenities = report_comp3_rec_amenities;
		this.report_comp3_rec_unit_features = report_comp3_rec_unit_features;
		this.report_comp3_concluded_adjustment = report_comp3_concluded_adjustment;
		this.report_comp3_adjusted_value = report_comp3_adjusted_value;
		this.report_comp4_date_month = report_comp4_date_month;
		this.report_comp4_date_day = report_comp4_date_day;
		this.report_comp4_date_year = report_comp4_date_year;
		this.report_comp4_source = report_comp4_source;
		this.report_comp4_contact_no = report_comp4_contact_no;
		this.report_comp4_location = report_comp4_location;
		this.report_comp4_area = report_comp4_area;
		this.report_comp4_base_price = report_comp4_base_price;
		this.report_comp4_price_sqm = report_comp4_price_sqm;
		this.report_comp4_discount_rate = report_comp4_discount_rate;
		this.report_comp4_discounts = report_comp4_discounts;
		this.report_comp4_selling_price = report_comp4_selling_price;
		this.report_comp4_rec_location = report_comp4_rec_location;
		this.report_comp4_rec_unit_floor_location = report_comp4_rec_unit_floor_location;
		this.report_comp4_rec_orientation = report_comp4_rec_orientation;
		this.report_comp4_rec_size = report_comp4_rec_size;
		this.report_comp4_rec_unit_condition = report_comp4_rec_unit_condition;
		this.report_comp4_rec_amenities = report_comp4_rec_amenities;
		this.report_comp4_rec_unit_features = report_comp4_rec_unit_features;
		this.report_comp4_concluded_adjustment = report_comp4_concluded_adjustment;
		this.report_comp4_adjusted_value = report_comp4_adjusted_value;
		this.report_comp5_date_month = report_comp5_date_month;
		this.report_comp5_date_day = report_comp5_date_day;
		this.report_comp5_date_year = report_comp5_date_year;
		this.report_comp5_source = report_comp5_source;
		this.report_comp5_contact_no = report_comp5_contact_no;
		this.report_comp5_location = report_comp5_location;
		this.report_comp5_area = report_comp5_area;
		this.report_comp5_base_price = report_comp5_base_price;
		this.report_comp5_price_sqm = report_comp5_price_sqm;
		this.report_comp5_discount_rate = report_comp5_discount_rate;
		this.report_comp5_discounts = report_comp5_discounts;
		this.report_comp5_selling_price = report_comp5_selling_price;
		this.report_comp5_rec_location = report_comp5_rec_location;
		this.report_comp5_rec_unit_floor_location = report_comp5_rec_unit_floor_location;
		this.report_comp5_rec_orientation = report_comp5_rec_orientation;
		this.report_comp5_rec_size = report_comp5_rec_size;
		this.report_comp5_rec_unit_condition = report_comp5_rec_unit_condition;
		this.report_comp5_rec_amenities = report_comp5_rec_amenities;
		this.report_comp5_rec_unit_features = report_comp5_rec_unit_features;
		this.report_comp5_concluded_adjustment = report_comp5_concluded_adjustment;
		this.report_comp5_adjusted_value = report_comp5_adjusted_value;
		this.report_comp_average = report_comp_average;
		this.report_comp_rounded_to = report_comp_rounded_to;
		this.report_comp1_rec_time_element = report_comp1_rec_time_element;
		this.report_comp2_rec_time_element = report_comp2_rec_time_element;
		this.report_comp3_rec_time_element = report_comp3_rec_time_element;
		this.report_comp4_rec_time_element = report_comp4_rec_time_element;
		this.report_comp5_rec_time_element = report_comp5_rec_time_element;
		this.report_comp1_remarks = report_comp1_remarks;
		this.report_comp2_remarks = report_comp2_remarks;
		this.report_comp3_remarks = report_comp3_remarks;
		this.report_comp4_remarks = report_comp4_remarks;
		this.report_comp5_remarks = report_comp5_remarks;
		//Added from IAN
		this.report_prev_date = report_prev_date;
		this.report_prev_appraiser = report_prev_appraiser;
		this.report_prev_total_appraised_value = report_prev_total_appraised_value;
	}
	
	//without int id
	public Condo_API(String record_id, String report_date_inspected_month,
			String report_date_inspected_day,
			String report_date_inspected_year, String report_id_admin,
			String report_id_unit_numbering, String report_id_bldg_plan,
			String report_unitclass_per_tax_dec,
			String report_unitclass_actual_usage,
			String report_unitclass_neighborhood,
			String report_unitclass_highest_best_use, String report_trans_jeep,
			String report_trans_bus, String report_trans_taxi,
			String report_trans_tricycle, String report_faci_function_room,
			String report_faci_gym, String report_faci_sauna,
			String report_faci_pool, String report_faci_fire_alarm,
			String report_faci_cctv, String report_faci_elevator,
			String report_util_electricity, String report_util_water,
			String report_util_telephone, String report_util_garbage,
			String report_landmarks_1, String report_landmarks_2,
			String report_landmarks_3, String report_landmarks_4,
			String report_landmarks_5, String report_distance_1,
			String report_distance_2, String report_distance_3,
			String report_distance_4, String report_distance_5,
			String report_zonal_location,
			String report_zonal_lot_classification, String report_zonal_value,
			String report_final_value_total_appraised_value,
			String report_final_value_recommended_deductions_desc,
			String report_final_value_recommended_deductions_rate,
			String report_final_value_recommended_deductions_amt,
			String report_final_value_net_appraised_value,
			String report_final_value_quick_sale_value_rate,
			String report_final_value_quick_sale_value_amt,
			String report_factors_of_concern,
			String report_suggested_corrective_actions, String report_remarks,
			String report_requirements, String report_time_inspected,
			String report_comp1_date_month,
			String report_comp1_date_day,
			String report_comp1_date_year,
			String report_comp1_source,
			String report_comp1_contact_no,
			String report_comp1_location,
			String report_comp1_area,
			String report_comp1_base_price,
			String report_comp1_price_sqm,
			String report_comp1_discount_rate,
			String report_comp1_discounts,
			String report_comp1_selling_price,
			String report_comp1_rec_location,
			String report_comp1_rec_unit_floor_location,
			String report_comp1_rec_orientation,
			String report_comp1_rec_size,
			String report_comp1_rec_unit_condition,
			String report_comp1_rec_amenities,
			String report_comp1_rec_unit_features,
			String report_comp1_concluded_adjustment,
			String report_comp1_adjusted_value,
			String report_comp2_date_month,
			String report_comp2_date_day,
			String report_comp2_date_year,
			String report_comp2_source,
			String report_comp2_contact_no,
			String report_comp2_location,
			String report_comp2_area,
			String report_comp2_base_price,
			String report_comp2_price_sqm,
			String report_comp2_discount_rate,
			String report_comp2_discounts,
			String report_comp2_selling_price,
			String report_comp2_rec_location,
			String report_comp2_rec_unit_floor_location,
			String report_comp2_rec_orientation,
			String report_comp2_rec_size,
			String report_comp2_rec_unit_condition,
			String report_comp2_rec_amenities,
			String report_comp2_rec_unit_features,
			String report_comp2_concluded_adjustment,
			String report_comp2_adjusted_value,
			String report_comp3_date_month,
			String report_comp3_date_day,
			String report_comp3_date_year,
			String report_comp3_source,
			String report_comp3_contact_no,
			String report_comp3_location,
			String report_comp3_area,
			String report_comp3_base_price,
			String report_comp3_price_sqm,
			String report_comp3_discount_rate,
			String report_comp3_discounts,
			String report_comp3_selling_price,
			String report_comp3_rec_location,
			String report_comp3_rec_unit_floor_location,
			String report_comp3_rec_orientation,
			String report_comp3_rec_size,
			String report_comp3_rec_unit_condition,
			String report_comp3_rec_amenities,
			String report_comp3_rec_unit_features,
			String report_comp3_concluded_adjustment,
			String report_comp3_adjusted_value,
			String report_comp4_date_month,
			String report_comp4_date_day,
			String report_comp4_date_year,
			String report_comp4_source,
			String report_comp4_contact_no,
			String report_comp4_location,
			String report_comp4_area,
			String report_comp4_base_price,
			String report_comp4_price_sqm,
			String report_comp4_discount_rate,
			String report_comp4_discounts,
			String report_comp4_selling_price,
			String report_comp4_rec_location,
			String report_comp4_rec_unit_floor_location,
			String report_comp4_rec_orientation,
			String report_comp4_rec_size,
			String report_comp4_rec_unit_condition,
			String report_comp4_rec_amenities,
			String report_comp4_rec_unit_features,
			String report_comp4_concluded_adjustment,
			String report_comp4_adjusted_value,
			String report_comp5_date_month,
			String report_comp5_date_day,
			String report_comp5_date_year,
			String report_comp5_source,
			String report_comp5_contact_no,
			String report_comp5_location,
			String report_comp5_area,
			String report_comp5_base_price,
			String report_comp5_price_sqm,
			String report_comp5_discount_rate,
			String report_comp5_discounts,
			String report_comp5_selling_price,
			String report_comp5_rec_location,
			String report_comp5_rec_unit_floor_location,
			String report_comp5_rec_orientation,
			String report_comp5_rec_size,
			String report_comp5_rec_unit_condition,
			String report_comp5_rec_amenities,
			String report_comp5_rec_unit_features,
			String report_comp5_concluded_adjustment,
			String report_comp5_adjusted_value,
			String report_comp_average,
			String report_comp_rounded_to,
			String report_comp1_rec_time_element,
			String report_comp2_rec_time_element,
			String report_comp3_rec_time_element,
			String report_comp4_rec_time_element,
			String report_comp5_rec_time_element,
			String report_comp1_remarks,
			String report_comp2_remarks,
			String report_comp3_remarks,
			String report_comp4_remarks,
			String report_comp5_remarks,
					 //Added from IAN
					 String report_prev_date,
					 String report_prev_appraiser,
					 String report_prev_total_appraised_value) {
		this.record_id = record_id;
		this.report_date_inspected_month = report_date_inspected_month;
		this.report_date_inspected_day = report_date_inspected_day;
		this.report_date_inspected_year = report_date_inspected_year;
		this.report_id_admin = report_id_admin;
		this.report_id_unit_numbering = report_id_unit_numbering;
		this.report_id_bldg_plan = report_id_bldg_plan;
		this.report_unitclass_per_tax_dec = report_unitclass_per_tax_dec;
		this.report_unitclass_actual_usage = report_unitclass_actual_usage;
		this.report_unitclass_neighborhood = report_unitclass_neighborhood;
		this.report_unitclass_highest_best_use = report_unitclass_highest_best_use;
		this.report_trans_jeep = report_trans_jeep;
		this.report_trans_bus = report_trans_bus;
		this.report_trans_taxi = report_trans_taxi;
		this.report_trans_tricycle = report_trans_tricycle;
		this.report_faci_function_room = report_faci_function_room;
		this.report_faci_gym = report_faci_gym;
		this.report_faci_sauna = report_faci_sauna;
		this.report_faci_pool = report_faci_pool;
		this.report_faci_fire_alarm = report_faci_fire_alarm;
		this.report_faci_cctv = report_faci_cctv;
		this.report_faci_elevator = report_faci_elevator;
		this.report_util_electricity = report_util_electricity;
		this.report_util_water = report_util_water;
		this.report_util_telephone = report_util_telephone;
		this.report_util_garbage = report_util_garbage;
		this.report_landmarks_1 = report_landmarks_1;
		this.report_landmarks_2 = report_landmarks_2;
		this.report_landmarks_3 = report_landmarks_3;
		this.report_landmarks_4 = report_landmarks_4;
		this.report_landmarks_5 = report_landmarks_5;
		this.report_distance_1 = report_distance_1;
		this.report_distance_2 = report_distance_2;
		this.report_distance_3 = report_distance_3;
		this.report_distance_4 = report_distance_4;
		this.report_distance_5 = report_distance_5;
		this.report_zonal_location = report_zonal_location;
		this.report_zonal_lot_classification = report_zonal_lot_classification;
		this.report_zonal_value = report_zonal_value;
		this.report_final_value_total_appraised_value = report_final_value_total_appraised_value;
		this.report_final_value_recommended_deductions_desc = report_final_value_recommended_deductions_desc;
		this.report_final_value_recommended_deductions_rate = report_final_value_recommended_deductions_rate;
		this.report_final_value_recommended_deductions_amt = report_final_value_recommended_deductions_amt;
		this.report_final_value_net_appraised_value = report_final_value_net_appraised_value;
		this.report_final_value_quick_sale_value_rate = report_final_value_quick_sale_value_rate;
		this.report_final_value_quick_sale_value_amt = report_final_value_quick_sale_value_amt;
		this.report_factors_of_concern = report_factors_of_concern;
		this.report_suggested_corrective_actions = report_suggested_corrective_actions;
		this.report_remarks = report_remarks;
		this.report_requirements = report_requirements;
		this.report_time_inspected = report_time_inspected;
		this.report_comp1_date_month = report_comp1_date_month;
		this.report_comp1_date_day = report_comp1_date_day;
		this.report_comp1_date_year = report_comp1_date_year;
		this.report_comp1_source = report_comp1_source;
		this.report_comp1_contact_no = report_comp1_contact_no;
		this.report_comp1_location = report_comp1_location;
		this.report_comp1_area = report_comp1_area;
		this.report_comp1_base_price = report_comp1_base_price;
		this.report_comp1_price_sqm = report_comp1_price_sqm;
		this.report_comp1_discount_rate = report_comp1_discount_rate;
		this.report_comp1_discounts = report_comp1_discounts;
		this.report_comp1_selling_price = report_comp1_selling_price;
		this.report_comp1_rec_location = report_comp1_rec_location;
		this.report_comp1_rec_unit_floor_location = report_comp1_rec_unit_floor_location;
		this.report_comp1_rec_orientation = report_comp1_rec_orientation;
		this.report_comp1_rec_size = report_comp1_rec_size;
		this.report_comp1_rec_unit_condition = report_comp1_rec_unit_condition;
		this.report_comp1_rec_amenities = report_comp1_rec_amenities;
		this.report_comp1_rec_unit_features = report_comp1_rec_unit_features;
		this.report_comp1_concluded_adjustment = report_comp1_concluded_adjustment;
		this.report_comp1_adjusted_value = report_comp1_adjusted_value;
		this.report_comp2_date_month = report_comp2_date_month;
		this.report_comp2_date_day = report_comp2_date_day;
		this.report_comp2_date_year = report_comp2_date_year;
		this.report_comp2_source = report_comp2_source;
		this.report_comp2_contact_no = report_comp2_contact_no;
		this.report_comp2_location = report_comp2_location;
		this.report_comp2_area = report_comp2_area;
		this.report_comp2_base_price = report_comp2_base_price;
		this.report_comp2_price_sqm = report_comp2_price_sqm;
		this.report_comp2_discount_rate = report_comp2_discount_rate;
		this.report_comp2_discounts = report_comp2_discounts;
		this.report_comp2_selling_price = report_comp2_selling_price;
		this.report_comp2_rec_location = report_comp2_rec_location;
		this.report_comp2_rec_unit_floor_location = report_comp2_rec_unit_floor_location;
		this.report_comp2_rec_orientation = report_comp2_rec_orientation;
		this.report_comp2_rec_size = report_comp2_rec_size;
		this.report_comp2_rec_unit_condition = report_comp2_rec_unit_condition;
		this.report_comp2_rec_amenities = report_comp2_rec_amenities;
		this.report_comp2_rec_unit_features = report_comp2_rec_unit_features;
		this.report_comp2_concluded_adjustment = report_comp2_concluded_adjustment;
		this.report_comp2_adjusted_value = report_comp2_adjusted_value;
		this.report_comp3_date_month = report_comp3_date_month;
		this.report_comp3_date_day = report_comp3_date_day;
		this.report_comp3_date_year = report_comp3_date_year;
		this.report_comp3_source = report_comp3_source;
		this.report_comp3_contact_no = report_comp3_contact_no;
		this.report_comp3_location = report_comp3_location;
		this.report_comp3_area = report_comp3_area;
		this.report_comp3_base_price = report_comp3_base_price;
		this.report_comp3_price_sqm = report_comp3_price_sqm;
		this.report_comp3_discount_rate = report_comp3_discount_rate;
		this.report_comp3_discounts = report_comp3_discounts;
		this.report_comp3_selling_price = report_comp3_selling_price;
		this.report_comp3_rec_location = report_comp3_rec_location;
		this.report_comp3_rec_unit_floor_location = report_comp3_rec_unit_floor_location;
		this.report_comp3_rec_orientation = report_comp3_rec_orientation;
		this.report_comp3_rec_size = report_comp3_rec_size;
		this.report_comp3_rec_unit_condition = report_comp3_rec_unit_condition;
		this.report_comp3_rec_amenities = report_comp3_rec_amenities;
		this.report_comp3_rec_unit_features = report_comp3_rec_unit_features;
		this.report_comp3_concluded_adjustment = report_comp3_concluded_adjustment;
		this.report_comp3_adjusted_value = report_comp3_adjusted_value;
		this.report_comp4_date_month = report_comp4_date_month;
		this.report_comp4_date_day = report_comp4_date_day;
		this.report_comp4_date_year = report_comp4_date_year;
		this.report_comp4_source = report_comp4_source;
		this.report_comp4_contact_no = report_comp4_contact_no;
		this.report_comp4_location = report_comp4_location;
		this.report_comp4_area = report_comp4_area;
		this.report_comp4_base_price = report_comp4_base_price;
		this.report_comp4_price_sqm = report_comp4_price_sqm;
		this.report_comp4_discount_rate = report_comp4_discount_rate;
		this.report_comp4_discounts = report_comp4_discounts;
		this.report_comp4_selling_price = report_comp4_selling_price;
		this.report_comp4_rec_location = report_comp4_rec_location;
		this.report_comp4_rec_unit_floor_location = report_comp4_rec_unit_floor_location;
		this.report_comp4_rec_orientation = report_comp4_rec_orientation;
		this.report_comp4_rec_size = report_comp4_rec_size;
		this.report_comp4_rec_unit_condition = report_comp4_rec_unit_condition;
		this.report_comp4_rec_amenities = report_comp4_rec_amenities;
		this.report_comp4_rec_unit_features = report_comp4_rec_unit_features;
		this.report_comp4_concluded_adjustment = report_comp4_concluded_adjustment;
		this.report_comp4_adjusted_value = report_comp4_adjusted_value;
		this.report_comp5_date_month = report_comp5_date_month;
		this.report_comp5_date_day = report_comp5_date_day;
		this.report_comp5_date_year = report_comp5_date_year;
		this.report_comp5_source = report_comp5_source;
		this.report_comp5_contact_no = report_comp5_contact_no;
		this.report_comp5_location = report_comp5_location;
		this.report_comp5_area = report_comp5_area;
		this.report_comp5_base_price = report_comp5_base_price;
		this.report_comp5_price_sqm = report_comp5_price_sqm;
		this.report_comp5_discount_rate = report_comp5_discount_rate;
		this.report_comp5_discounts = report_comp5_discounts;
		this.report_comp5_selling_price = report_comp5_selling_price;
		this.report_comp5_rec_location = report_comp5_rec_location;
		this.report_comp5_rec_unit_floor_location = report_comp5_rec_unit_floor_location;
		this.report_comp5_rec_orientation = report_comp5_rec_orientation;
		this.report_comp5_rec_size = report_comp5_rec_size;
		this.report_comp5_rec_unit_condition = report_comp5_rec_unit_condition;
		this.report_comp5_rec_amenities = report_comp5_rec_amenities;
		this.report_comp5_rec_unit_features = report_comp5_rec_unit_features;
		this.report_comp5_concluded_adjustment = report_comp5_concluded_adjustment;
		this.report_comp5_adjusted_value = report_comp5_adjusted_value;
		this.report_comp_average = report_comp_average;
		this.report_comp_rounded_to = report_comp_rounded_to;
		this.report_comp1_rec_time_element = report_comp1_rec_time_element;
		this.report_comp2_rec_time_element = report_comp2_rec_time_element;
		this.report_comp3_rec_time_element = report_comp3_rec_time_element;
		this.report_comp4_rec_time_element = report_comp4_rec_time_element;
		this.report_comp5_rec_time_element = report_comp5_rec_time_element;
		this.report_comp1_remarks = report_comp1_remarks;
		this.report_comp2_remarks = report_comp2_remarks;
		this.report_comp3_remarks = report_comp3_remarks;
		this.report_comp4_remarks = report_comp4_remarks;
		this.report_comp5_remarks = report_comp5_remarks;
		//Added from IAN
		this.report_prev_date = report_prev_date;
		this.report_prev_appraiser = report_prev_appraiser;
		this.report_prev_total_appraised_value = report_prev_total_appraised_value;
	}
	
	//without record_id
	public Condo_API(String report_date_inspected_month,
			String report_date_inspected_day,
			String report_date_inspected_year, String report_id_admin,
			String report_id_unit_numbering, String report_id_bldg_plan,
			String report_unitclass_per_tax_dec,
			String report_unitclass_actual_usage,
			String report_unitclass_neighborhood,
			String report_unitclass_highest_best_use, String report_trans_jeep,
			String report_trans_bus, String report_trans_taxi,
			String report_trans_tricycle, String report_faci_function_room,
			String report_faci_gym, String report_faci_sauna,
			String report_faci_pool, String report_faci_fire_alarm,
			String report_faci_cctv, String report_faci_elevator,
			String report_util_electricity, String report_util_water,
			String report_util_telephone, String report_util_garbage,
			String report_landmarks_1, String report_landmarks_2,
			String report_landmarks_3, String report_landmarks_4,
			String report_landmarks_5, String report_distance_1,
			String report_distance_2, String report_distance_3,
			String report_distance_4, String report_distance_5,
			String report_zonal_location,
			String report_zonal_lot_classification, String report_zonal_value,
			String report_final_value_total_appraised_value,
			String report_final_value_recommended_deductions_desc,
			String report_final_value_recommended_deductions_rate,
			String report_final_value_recommended_deductions_amt,
			String report_final_value_net_appraised_value,
			String report_final_value_quick_sale_value_rate,
			String report_final_value_quick_sale_value_amt,
			String report_factors_of_concern,
			String report_suggested_corrective_actions, String report_remarks,
			String report_requirements, String report_time_inspected,
			String report_comp1_date_month,
			String report_comp1_date_day,
			String report_comp1_date_year,
			String report_comp1_source,
			String report_comp1_contact_no,
			String report_comp1_location,
			String report_comp1_area,
			String report_comp1_base_price,
			String report_comp1_price_sqm,
			String report_comp1_discount_rate,
			String report_comp1_discounts,
			String report_comp1_selling_price,
			String report_comp1_rec_location,
			String report_comp1_rec_unit_floor_location,
			String report_comp1_rec_orientation,
			String report_comp1_rec_size,
			String report_comp1_rec_unit_condition,
			String report_comp1_rec_amenities,
			String report_comp1_rec_unit_features,
			String report_comp1_concluded_adjustment,
			String report_comp1_adjusted_value,
			String report_comp2_date_month,
			String report_comp2_date_day,
			String report_comp2_date_year,
			String report_comp2_source,
			String report_comp2_contact_no,
			String report_comp2_location,
			String report_comp2_area,
			String report_comp2_base_price,
			String report_comp2_price_sqm,
			String report_comp2_discount_rate,
			String report_comp2_discounts,
			String report_comp2_selling_price,
			String report_comp2_rec_location,
			String report_comp2_rec_unit_floor_location,
			String report_comp2_rec_orientation,
			String report_comp2_rec_size,
			String report_comp2_rec_unit_condition,
			String report_comp2_rec_amenities,
			String report_comp2_rec_unit_features,
			String report_comp2_concluded_adjustment,
			String report_comp2_adjusted_value,
			String report_comp3_date_month,
			String report_comp3_date_day,
			String report_comp3_date_year,
			String report_comp3_source,
			String report_comp3_contact_no,
			String report_comp3_location,
			String report_comp3_area,
			String report_comp3_base_price,
			String report_comp3_price_sqm,
			String report_comp3_discount_rate,
			String report_comp3_discounts,
			String report_comp3_selling_price,
			String report_comp3_rec_location,
			String report_comp3_rec_unit_floor_location,
			String report_comp3_rec_orientation,
			String report_comp3_rec_size,
			String report_comp3_rec_unit_condition,
			String report_comp3_rec_amenities,
			String report_comp3_rec_unit_features,
			String report_comp3_concluded_adjustment,
			String report_comp3_adjusted_value,
			String report_comp4_date_month,
			String report_comp4_date_day,
			String report_comp4_date_year,
			String report_comp4_source,
			String report_comp4_contact_no,
			String report_comp4_location,
			String report_comp4_area,
			String report_comp4_base_price,
			String report_comp4_price_sqm,
			String report_comp4_discount_rate,
			String report_comp4_discounts,
			String report_comp4_selling_price,
			String report_comp4_rec_location,
			String report_comp4_rec_unit_floor_location,
			String report_comp4_rec_orientation,
			String report_comp4_rec_size,
			String report_comp4_rec_unit_condition,
			String report_comp4_rec_amenities,
			String report_comp4_rec_unit_features,
			String report_comp4_concluded_adjustment,
			String report_comp4_adjusted_value,
			String report_comp5_date_month,
			String report_comp5_date_day,
			String report_comp5_date_year,
			String report_comp5_source,
			String report_comp5_contact_no,
			String report_comp5_location,
			String report_comp5_area,
			String report_comp5_base_price,
			String report_comp5_price_sqm,
			String report_comp5_discount_rate,
			String report_comp5_discounts,
			String report_comp5_selling_price,
			String report_comp5_rec_location,
			String report_comp5_rec_unit_floor_location,
			String report_comp5_rec_orientation,
			String report_comp5_rec_size,
			String report_comp5_rec_unit_condition,
			String report_comp5_rec_amenities,
			String report_comp5_rec_unit_features,
			String report_comp5_concluded_adjustment,
			String report_comp5_adjusted_value,
			String report_comp_average,
			String report_comp_rounded_to,
					 //Added from IAN
					 String report_prev_date,
					 String report_prev_appraiser,
					 String report_prev_total_appraised_value) {
		this.report_date_inspected_month = report_date_inspected_month;
		this.report_date_inspected_day = report_date_inspected_day;
		this.report_date_inspected_year = report_date_inspected_year;
		this.report_id_admin = report_id_admin;
		this.report_id_unit_numbering = report_id_unit_numbering;
		this.report_id_bldg_plan = report_id_bldg_plan;
		this.report_unitclass_per_tax_dec = report_unitclass_per_tax_dec;
		this.report_unitclass_actual_usage = report_unitclass_actual_usage;
		this.report_unitclass_neighborhood = report_unitclass_neighborhood;
		this.report_unitclass_highest_best_use = report_unitclass_highest_best_use;
		this.report_trans_jeep = report_trans_jeep;
		this.report_trans_bus = report_trans_bus;
		this.report_trans_taxi = report_trans_taxi;
		this.report_trans_tricycle = report_trans_tricycle;
		this.report_faci_function_room = report_faci_function_room;
		this.report_faci_gym = report_faci_gym;
		this.report_faci_sauna = report_faci_sauna;
		this.report_faci_pool = report_faci_pool;
		this.report_faci_fire_alarm = report_faci_fire_alarm;
		this.report_faci_cctv = report_faci_cctv;
		this.report_faci_elevator = report_faci_elevator;
		this.report_util_electricity = report_util_electricity;
		this.report_util_water = report_util_water;
		this.report_util_telephone = report_util_telephone;
		this.report_util_garbage = report_util_garbage;
		this.report_landmarks_1 = report_landmarks_1;
		this.report_landmarks_2 = report_landmarks_2;
		this.report_landmarks_3 = report_landmarks_3;
		this.report_landmarks_4 = report_landmarks_4;
		this.report_landmarks_5 = report_landmarks_5;
		this.report_distance_1 = report_distance_1;
		this.report_distance_2 = report_distance_2;
		this.report_distance_3 = report_distance_3;
		this.report_distance_4 = report_distance_4;
		this.report_distance_5 = report_distance_5;
		this.report_zonal_location = report_zonal_location;
		this.report_zonal_lot_classification = report_zonal_lot_classification;
		this.report_zonal_value = report_zonal_value;
		this.report_final_value_total_appraised_value = report_final_value_total_appraised_value;
		this.report_final_value_recommended_deductions_desc = report_final_value_recommended_deductions_desc;
		this.report_final_value_recommended_deductions_rate = report_final_value_recommended_deductions_rate;
		this.report_final_value_recommended_deductions_amt = report_final_value_recommended_deductions_amt;
		this.report_final_value_net_appraised_value = report_final_value_net_appraised_value;
		this.report_final_value_quick_sale_value_rate = report_final_value_quick_sale_value_rate;
		this.report_final_value_quick_sale_value_amt = report_final_value_quick_sale_value_amt;
		this.report_factors_of_concern = report_factors_of_concern;
		this.report_suggested_corrective_actions = report_suggested_corrective_actions;
		this.report_remarks = report_remarks;
		this.report_requirements = report_requirements;
		this.report_time_inspected = report_time_inspected;
		this.report_comp1_date_month = report_comp1_date_month;
		this.report_comp1_date_day = report_comp1_date_day;
		this.report_comp1_date_year = report_comp1_date_year;
		this.report_comp1_source = report_comp1_source;
		this.report_comp1_contact_no = report_comp1_contact_no;
		this.report_comp1_location = report_comp1_location;
		this.report_comp1_area = report_comp1_area;
		this.report_comp1_base_price = report_comp1_base_price;
		this.report_comp1_price_sqm = report_comp1_price_sqm;
		this.report_comp1_discount_rate = report_comp1_discount_rate;
		this.report_comp1_discounts = report_comp1_discounts;
		this.report_comp1_selling_price = report_comp1_selling_price;
		this.report_comp1_rec_location = report_comp1_rec_location;
		this.report_comp1_rec_unit_floor_location = report_comp1_rec_unit_floor_location;
		this.report_comp1_rec_orientation = report_comp1_rec_orientation;
		this.report_comp1_rec_size = report_comp1_rec_size;
		this.report_comp1_rec_unit_condition = report_comp1_rec_unit_condition;
		this.report_comp1_rec_amenities = report_comp1_rec_amenities;
		this.report_comp1_rec_unit_features = report_comp1_rec_unit_features;
		this.report_comp1_concluded_adjustment = report_comp1_concluded_adjustment;
		this.report_comp1_adjusted_value = report_comp1_adjusted_value;
		this.report_comp2_date_month = report_comp2_date_month;
		this.report_comp2_date_day = report_comp2_date_day;
		this.report_comp2_date_year = report_comp2_date_year;
		this.report_comp2_source = report_comp2_source;
		this.report_comp2_contact_no = report_comp2_contact_no;
		this.report_comp2_location = report_comp2_location;
		this.report_comp2_area = report_comp2_area;
		this.report_comp2_base_price = report_comp2_base_price;
		this.report_comp2_price_sqm = report_comp2_price_sqm;
		this.report_comp2_discount_rate = report_comp2_discount_rate;
		this.report_comp2_discounts = report_comp2_discounts;
		this.report_comp2_selling_price = report_comp2_selling_price;
		this.report_comp2_rec_location = report_comp2_rec_location;
		this.report_comp2_rec_unit_floor_location = report_comp2_rec_unit_floor_location;
		this.report_comp2_rec_orientation = report_comp2_rec_orientation;
		this.report_comp2_rec_size = report_comp2_rec_size;
		this.report_comp2_rec_unit_condition = report_comp2_rec_unit_condition;
		this.report_comp2_rec_amenities = report_comp2_rec_amenities;
		this.report_comp2_rec_unit_features = report_comp2_rec_unit_features;
		this.report_comp2_concluded_adjustment = report_comp2_concluded_adjustment;
		this.report_comp2_adjusted_value = report_comp2_adjusted_value;
		this.report_comp3_date_month = report_comp3_date_month;
		this.report_comp3_date_day = report_comp3_date_day;
		this.report_comp3_date_year = report_comp3_date_year;
		this.report_comp3_source = report_comp3_source;
		this.report_comp3_contact_no = report_comp3_contact_no;
		this.report_comp3_location = report_comp3_location;
		this.report_comp3_area = report_comp3_area;
		this.report_comp3_base_price = report_comp3_base_price;
		this.report_comp3_price_sqm = report_comp3_price_sqm;
		this.report_comp3_discount_rate = report_comp3_discount_rate;
		this.report_comp3_discounts = report_comp3_discounts;
		this.report_comp3_selling_price = report_comp3_selling_price;
		this.report_comp3_rec_location = report_comp3_rec_location;
		this.report_comp3_rec_unit_floor_location = report_comp3_rec_unit_floor_location;
		this.report_comp3_rec_orientation = report_comp3_rec_orientation;
		this.report_comp3_rec_size = report_comp3_rec_size;
		this.report_comp3_rec_unit_condition = report_comp3_rec_unit_condition;
		this.report_comp3_rec_amenities = report_comp3_rec_amenities;
		this.report_comp3_rec_unit_features = report_comp3_rec_unit_features;
		this.report_comp3_concluded_adjustment = report_comp3_concluded_adjustment;
		this.report_comp3_adjusted_value = report_comp3_adjusted_value;
		this.report_comp4_date_month = report_comp4_date_month;
		this.report_comp4_date_day = report_comp4_date_day;
		this.report_comp4_date_year = report_comp4_date_year;
		this.report_comp4_source = report_comp4_source;
		this.report_comp4_contact_no = report_comp4_contact_no;
		this.report_comp4_location = report_comp4_location;
		this.report_comp4_area = report_comp4_area;
		this.report_comp4_base_price = report_comp4_base_price;
		this.report_comp4_price_sqm = report_comp4_price_sqm;
		this.report_comp4_discount_rate = report_comp4_discount_rate;
		this.report_comp4_discounts = report_comp4_discounts;
		this.report_comp4_selling_price = report_comp4_selling_price;
		this.report_comp4_rec_location = report_comp4_rec_location;
		this.report_comp4_rec_unit_floor_location = report_comp4_rec_unit_floor_location;
		this.report_comp4_rec_orientation = report_comp4_rec_orientation;
		this.report_comp4_rec_size = report_comp4_rec_size;
		this.report_comp4_rec_unit_condition = report_comp4_rec_unit_condition;
		this.report_comp4_rec_amenities = report_comp4_rec_amenities;
		this.report_comp4_rec_unit_features = report_comp4_rec_unit_features;
		this.report_comp4_concluded_adjustment = report_comp4_concluded_adjustment;
		this.report_comp4_adjusted_value = report_comp4_adjusted_value;
		this.report_comp5_date_month = report_comp5_date_month;
		this.report_comp5_date_day = report_comp5_date_day;
		this.report_comp5_date_year = report_comp5_date_year;
		this.report_comp5_source = report_comp5_source;
		this.report_comp5_contact_no = report_comp5_contact_no;
		this.report_comp5_location = report_comp5_location;
		this.report_comp5_area = report_comp5_area;
		this.report_comp5_base_price = report_comp5_base_price;
		this.report_comp5_price_sqm = report_comp5_price_sqm;
		this.report_comp5_discount_rate = report_comp5_discount_rate;
		this.report_comp5_discounts = report_comp5_discounts;
		this.report_comp5_selling_price = report_comp5_selling_price;
		this.report_comp5_rec_location = report_comp5_rec_location;
		this.report_comp5_rec_unit_floor_location = report_comp5_rec_unit_floor_location;
		this.report_comp5_rec_orientation = report_comp5_rec_orientation;
		this.report_comp5_rec_size = report_comp5_rec_size;
		this.report_comp5_rec_unit_condition = report_comp5_rec_unit_condition;
		this.report_comp5_rec_amenities = report_comp5_rec_amenities;
		this.report_comp5_rec_unit_features = report_comp5_rec_unit_features;
		this.report_comp5_concluded_adjustment = report_comp5_concluded_adjustment;
		this.report_comp5_adjusted_value = report_comp5_adjusted_value;
		this.report_comp_average = report_comp_average;
		this.report_comp_rounded_to = report_comp_rounded_to;
		//Added from IAN
		this.report_prev_date = report_prev_date;
		this.report_prev_appraiser = report_prev_appraiser;
		this.report_prev_total_appraised_value = report_prev_total_appraised_value;
	}
	
	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	public String getrecord_id() {
		return this.record_id;
	}

	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}
	
//getters
	public String getreport_date_inspected_month() {
		return this.report_date_inspected_month;
	}

	public String getreport_date_inspected_day() {
		return this.report_date_inspected_day;
	}

	public String getreport_date_inspected_year() {
		return this.report_date_inspected_year;
	}

	public String getreport_id_admin() {
		return this.report_id_admin;
	}

	public String getreport_id_unit_numbering() {
		return this.report_id_unit_numbering;
	}

	public String getreport_id_bldg_plan() {
		return this.report_id_bldg_plan;
	}

	public String getreport_unitclass_per_tax_dec() {
		return this.report_unitclass_per_tax_dec;
	}

	public String getreport_unitclass_actual_usage() {
		return this.report_unitclass_actual_usage;
	}

	public String getreport_unitclass_neighborhood() {
		return this.report_unitclass_neighborhood;
	}

	public String getreport_unitclass_highest_best_use() {
		return this.report_unitclass_highest_best_use;
	}

	public String getreport_trans_jeep() {
		return this.report_trans_jeep;
	}

	public String getreport_trans_bus() {
		return this.report_trans_bus;
	}

	public String getreport_trans_taxi() {
		return this.report_trans_taxi;
	}

	public String getreport_trans_tricycle() {
		return this.report_trans_tricycle;
	}

	public String getreport_faci_function_room() {
		return this.report_faci_function_room;
	}

	public String getreport_faci_gym() {
		return this.report_faci_gym;
	}

	public String getreport_faci_sauna() {
		return this.report_faci_sauna;
	}

	public String getreport_faci_pool() {
		return this.report_faci_pool;
	}

	public String getreport_faci_fire_alarm() {
		return this.report_faci_fire_alarm;
	}

	public String getreport_faci_cctv() {
		return this.report_faci_cctv;
	}

	public String getreport_faci_elevator() {
		return this.report_faci_elevator;
	}

	public String getreport_util_electricity() {
		return this.report_util_electricity;
	}

	public String getreport_util_water() {
		return this.report_util_water;
	}

	public String getreport_util_telephone() {
		return this.report_util_telephone;
	}

	public String getreport_util_garbage() {
		return this.report_util_garbage;
	}

	public String getreport_landmarks_1() {
		return this.report_landmarks_1;
	}

	public String getreport_landmarks_2() {
		return this.report_landmarks_2;
	}

	public String getreport_landmarks_3() {
		return this.report_landmarks_3;
	}

	public String getreport_landmarks_4() {
		return this.report_landmarks_4;
	}

	public String getreport_landmarks_5() {
		return this.report_landmarks_5;
	}

	public String getreport_distance_1() {
		return this.report_distance_1;
	}

	public String getreport_distance_2() {
		return this.report_distance_2;
	}

	public String getreport_distance_3() {
		return this.report_distance_3;
	}

	public String getreport_distance_4() {
		return this.report_distance_4;
	}

	public String getreport_distance_5() {
		return this.report_distance_5;
	}

	public String getreport_zonal_location() {
		return this.report_zonal_location;
	}

	public String getreport_zonal_lot_classification() {
		return this.report_zonal_lot_classification;
	}

	public String getreport_zonal_value() {
		return this.report_zonal_value;
	}

	public String getreport_final_value_total_appraised_value() {
		return this.report_final_value_total_appraised_value;
	}

	public String getreport_final_value_recommended_deductions_desc() {
		return this.report_final_value_recommended_deductions_desc;
	}

	public String getreport_final_value_recommended_deductions_rate() {
		return this.report_final_value_recommended_deductions_rate;
	}

	public String getreport_final_value_recommended_deductions_amt() {
		return this.report_final_value_recommended_deductions_amt;
	}

	public String getreport_final_value_net_appraised_value() {
		return this.report_final_value_net_appraised_value;
	}

	public String getreport_final_value_quick_sale_value_rate() {
		return this.report_final_value_quick_sale_value_rate;
	}

	public String getreport_final_value_quick_sale_value_amt() {
		return this.report_final_value_quick_sale_value_amt;
	}

	public String getreport_factors_of_concern() {
		return this.report_factors_of_concern;
	}

	public String getreport_suggested_corrective_actions() {
		return this.report_suggested_corrective_actions;
	}

	public String getreport_remarks() {
		return this.report_remarks;
	}

	public String getreport_requirements() {
		return this.report_requirements;
	}
	
	public String getreport_time_inspected() {
		return this.report_time_inspected;
	}
	
	public String getreport_comp1_date_month() {
	return this.report_comp1_date_month;
	}

	public String getreport_comp1_date_day() {
	return this.report_comp1_date_day;
	}

	public String getreport_comp1_date_year() {
	return this.report_comp1_date_year;
	}

	public String getreport_comp1_source() {
	return this.report_comp1_source;
	}

	public String getreport_comp1_contact_no() {
	return this.report_comp1_contact_no;
	}

	public String getreport_comp1_location() {
	return this.report_comp1_location;
	}

	public String getreport_comp1_area() {
	return this.report_comp1_area;
	}

	public String getreport_comp1_base_price() {
	return this.report_comp1_base_price;
	}

	public String getreport_comp1_price_sqm() {
	return this.report_comp1_price_sqm;
	}

	public String getreport_comp1_discount_rate() {
	return this.report_comp1_discount_rate;
	}

	public String getreport_comp1_discounts() {
	return this.report_comp1_discounts;
	}

	public String getreport_comp1_selling_price() {
	return this.report_comp1_selling_price;
	}

	public String getreport_comp1_rec_location() {
	return this.report_comp1_rec_location;
	}

	public String getreport_comp1_rec_unit_floor_location() {
	return this.report_comp1_rec_unit_floor_location;
	}

	public String getreport_comp1_rec_orientation() {
	return this.report_comp1_rec_orientation;
	}

	public String getreport_comp1_rec_size() {
	return this.report_comp1_rec_size;
	}

	public String getreport_comp1_rec_unit_condition() {
	return this.report_comp1_rec_unit_condition;
	}

	public String getreport_comp1_rec_amenities() {
	return this.report_comp1_rec_amenities;
	}

	public String getreport_comp1_rec_unit_features() {
	return this.report_comp1_rec_unit_features;
	}

	public String getreport_comp1_concluded_adjustment() {
	return this.report_comp1_concluded_adjustment;
	}

	public String getreport_comp1_adjusted_value() {
	return this.report_comp1_adjusted_value;
	}

	public String getreport_comp2_date_month() {
	return this.report_comp2_date_month;
	}

	public String getreport_comp2_date_day() {
	return this.report_comp2_date_day;
	}

	public String getreport_comp2_date_year() {
	return this.report_comp2_date_year;
	}

	public String getreport_comp2_source() {
	return this.report_comp2_source;
	}

	public String getreport_comp2_contact_no() {
	return this.report_comp2_contact_no;
	}

	public String getreport_comp2_location() {
	return this.report_comp2_location;
	}

	public String getreport_comp2_area() {
	return this.report_comp2_area;
	}

	public String getreport_comp2_base_price() {
	return this.report_comp2_base_price;
	}

	public String getreport_comp2_price_sqm() {
	return this.report_comp2_price_sqm;
	}

	public String getreport_comp2_discount_rate() {
	return this.report_comp2_discount_rate;
	}

	public String getreport_comp2_discounts() {
	return this.report_comp2_discounts;
	}

	public String getreport_comp2_selling_price() {
	return this.report_comp2_selling_price;
	}

	public String getreport_comp2_rec_location() {
	return this.report_comp2_rec_location;
	}

	public String getreport_comp2_rec_unit_floor_location() {
	return this.report_comp2_rec_unit_floor_location;
	}

	public String getreport_comp2_rec_orientation() {
	return this.report_comp2_rec_orientation;
	}

	public String getreport_comp2_rec_size() {
	return this.report_comp2_rec_size;
	}

	public String getreport_comp2_rec_unit_condition() {
	return this.report_comp2_rec_unit_condition;
	}

	public String getreport_comp2_rec_amenities() {
	return this.report_comp2_rec_amenities;
	}

	public String getreport_comp2_rec_unit_features() {
	return this.report_comp2_rec_unit_features;
	}

	public String getreport_comp2_concluded_adjustment() {
	return this.report_comp2_concluded_adjustment;
	}

	public String getreport_comp2_adjusted_value() {
	return this.report_comp2_adjusted_value;
	}

	public String getreport_comp3_date_month() {
	return this.report_comp3_date_month;
	}

	public String getreport_comp3_date_day() {
	return this.report_comp3_date_day;
	}

	public String getreport_comp3_date_year() {
	return this.report_comp3_date_year;
	}

	public String getreport_comp3_source() {
	return this.report_comp3_source;
	}

	public String getreport_comp3_contact_no() {
	return this.report_comp3_contact_no;
	}

	public String getreport_comp3_location() {
	return this.report_comp3_location;
	}

	public String getreport_comp3_area() {
	return this.report_comp3_area;
	}

	public String getreport_comp3_base_price() {
	return this.report_comp3_base_price;
	}

	public String getreport_comp3_price_sqm() {
	return this.report_comp3_price_sqm;
	}

	public String getreport_comp3_discount_rate() {
	return this.report_comp3_discount_rate;
	}

	public String getreport_comp3_discounts() {
	return this.report_comp3_discounts;
	}

	public String getreport_comp3_selling_price() {
	return this.report_comp3_selling_price;
	}

	public String getreport_comp3_rec_location() {
	return this.report_comp3_rec_location;
	}

	public String getreport_comp3_rec_unit_floor_location() {
	return this.report_comp3_rec_unit_floor_location;
	}

	public String getreport_comp3_rec_orientation() {
	return this.report_comp3_rec_orientation;
	}

	public String getreport_comp3_rec_size() {
	return this.report_comp3_rec_size;
	}

	public String getreport_comp3_rec_unit_condition() {
	return this.report_comp3_rec_unit_condition;
	}

	public String getreport_comp3_rec_amenities() {
	return this.report_comp3_rec_amenities;
	}

	public String getreport_comp3_rec_unit_features() {
	return this.report_comp3_rec_unit_features;
	}

	public String getreport_comp3_concluded_adjustment() {
	return this.report_comp3_concluded_adjustment;
	}

	public String getreport_comp3_adjusted_value() {
	return this.report_comp3_adjusted_value;
	}

	public String getreport_comp4_date_month() {
	return this.report_comp4_date_month;
	}

	public String getreport_comp4_date_day() {
	return this.report_comp4_date_day;
	}

	public String getreport_comp4_date_year() {
	return this.report_comp4_date_year;
	}

	public String getreport_comp4_source() {
	return this.report_comp4_source;
	}

	public String getreport_comp4_contact_no() {
	return this.report_comp4_contact_no;
	}

	public String getreport_comp4_location() {
	return this.report_comp4_location;
	}

	public String getreport_comp4_area() {
	return this.report_comp4_area;
	}

	public String getreport_comp4_base_price() {
	return this.report_comp4_base_price;
	}

	public String getreport_comp4_price_sqm() {
	return this.report_comp4_price_sqm;
	}

	public String getreport_comp4_discount_rate() {
	return this.report_comp4_discount_rate;
	}

	public String getreport_comp4_discounts() {
	return this.report_comp4_discounts;
	}

	public String getreport_comp4_selling_price() {
	return this.report_comp4_selling_price;
	}

	public String getreport_comp4_rec_location() {
	return this.report_comp4_rec_location;
	}

	public String getreport_comp4_rec_unit_floor_location() {
	return this.report_comp4_rec_unit_floor_location;
	}

	public String getreport_comp4_rec_orientation() {
	return this.report_comp4_rec_orientation;
	}

	public String getreport_comp4_rec_size() {
	return this.report_comp4_rec_size;
	}

	public String getreport_comp4_rec_unit_condition() {
	return this.report_comp4_rec_unit_condition;
	}

	public String getreport_comp4_rec_amenities() {
	return this.report_comp4_rec_amenities;
	}

	public String getreport_comp4_rec_unit_features() {
	return this.report_comp4_rec_unit_features;
	}

	public String getreport_comp4_concluded_adjustment() {
	return this.report_comp4_concluded_adjustment;
	}

	public String getreport_comp4_adjusted_value() {
	return this.report_comp4_adjusted_value;
	}

	public String getreport_comp5_date_month() {
	return this.report_comp5_date_month;
	}

	public String getreport_comp5_date_day() {
	return this.report_comp5_date_day;
	}

	public String getreport_comp5_date_year() {
	return this.report_comp5_date_year;
	}

	public String getreport_comp5_source() {
	return this.report_comp5_source;
	}

	public String getreport_comp5_contact_no() {
	return this.report_comp5_contact_no;
	}

	public String getreport_comp5_location() {
	return this.report_comp5_location;
	}

	public String getreport_comp5_area() {
	return this.report_comp5_area;
	}

	public String getreport_comp5_base_price() {
	return this.report_comp5_base_price;
	}

	public String getreport_comp5_price_sqm() {
	return this.report_comp5_price_sqm;
	}

	public String getreport_comp5_discount_rate() {
	return this.report_comp5_discount_rate;
	}

	public String getreport_comp5_discounts() {
	return this.report_comp5_discounts;
	}

	public String getreport_comp5_selling_price() {
	return this.report_comp5_selling_price;
	}

	public String getreport_comp5_rec_location() {
	return this.report_comp5_rec_location;
	}

	public String getreport_comp5_rec_unit_floor_location() {
	return this.report_comp5_rec_unit_floor_location;
	}

	public String getreport_comp5_rec_orientation() {
	return this.report_comp5_rec_orientation;
	}

	public String getreport_comp5_rec_size() {
	return this.report_comp5_rec_size;
	}

	public String getreport_comp5_rec_unit_condition() {
	return this.report_comp5_rec_unit_condition;
	}

	public String getreport_comp5_rec_amenities() {
	return this.report_comp5_rec_amenities;
	}

	public String getreport_comp5_rec_unit_features() {
	return this.report_comp5_rec_unit_features;
	}

	public String getreport_comp5_concluded_adjustment() {
	return this.report_comp5_concluded_adjustment;
	}

	public String getreport_comp5_adjusted_value() {
	return this.report_comp5_adjusted_value;
	}

	public String getreport_comp_average() {
	return this.report_comp_average;
	}

	public String getreport_comp_rounded_to() {
	return this.report_comp_rounded_to;
	}
	
	public String getreport_comp1_rec_time_element() {
		return this.report_comp1_rec_time_element;
	}
	
	public String getreport_comp2_rec_time_element() {
		return this.report_comp2_rec_time_element;
	}
	
	public String getreport_comp3_rec_time_element() {
		return this.report_comp3_rec_time_element;
	}
	
	public String getreport_comp4_rec_time_element() {
		return this.report_comp4_rec_time_element;
	}
	
	public String getreport_comp5_rec_time_element() {
		return this.report_comp5_rec_time_element;
	}
	
	public String getreport_comp1_remarks() {
		return this.report_comp1_remarks;
	}
	
	public String getreport_comp2_remarks() {
		return this.report_comp2_remarks;
	}
	
	public String getreport_comp3_remarks() {
		return this.report_comp3_remarks;
	}
	
	public String getreport_comp4_remarks() {
		return this.report_comp4_remarks;
	}
	
	public String getreport_comp5_remarks() {
		return this.report_comp5_remarks;
	}

	//Added from IAN
	public String getreport_prev_date() {
		return this.report_prev_date;
	}
	public String getreport_prev_appraiser() {
		return this.report_prev_appraiser;
	}
	public String getreport_prev_total_appraised_value() {
		return this.report_prev_total_appraised_value;
	}


//setters
	public void setreport_date_inspected_month(
			String report_date_inspected_month) {
		this.report_date_inspected_month = report_date_inspected_month;
	}

	public void setreport_date_inspected_day(String report_date_inspected_day) {
		this.report_date_inspected_day = report_date_inspected_day;
	}

	public void setreport_date_inspected_year(String report_date_inspected_year) {
		this.report_date_inspected_year = report_date_inspected_year;
	}

	public void setreport_id_admin(String report_id_admin) {
		this.report_id_admin = report_id_admin;
	}

	public void setreport_id_unit_numbering(String report_id_unit_numbering) {
		this.report_id_unit_numbering = report_id_unit_numbering;
	}

	public void setreport_id_bldg_plan(String report_id_bldg_plan) {
		this.report_id_bldg_plan = report_id_bldg_plan;
	}

	public void setreport_unitclass_per_tax_dec(
			String report_unitclass_per_tax_dec) {
		this.report_unitclass_per_tax_dec = report_unitclass_per_tax_dec;
	}

	public void setreport_unitclass_actual_usage(
			String report_unitclass_actual_usage) {
		this.report_unitclass_actual_usage = report_unitclass_actual_usage;
	}

	public void setreport_unitclass_neighborhood(
			String report_unitclass_neighborhood) {
		this.report_unitclass_neighborhood = report_unitclass_neighborhood;
	}

	public void setreport_unitclass_highest_best_use(
			String report_unitclass_highest_best_use) {
		this.report_unitclass_highest_best_use = report_unitclass_highest_best_use;
	}

	public void setreport_trans_jeep(String report_trans_jeep) {
		this.report_trans_jeep = report_trans_jeep;
	}

	public void setreport_trans_bus(String report_trans_bus) {
		this.report_trans_bus = report_trans_bus;
	}

	public void setreport_trans_taxi(String report_trans_taxi) {
		this.report_trans_taxi = report_trans_taxi;
	}

	public void setreport_trans_tricycle(String report_trans_tricycle) {
		this.report_trans_tricycle = report_trans_tricycle;
	}

	public void setreport_faci_function_room(String report_faci_function_room) {
		this.report_faci_function_room = report_faci_function_room;
	}

	public void setreport_faci_gym(String report_faci_gym) {
		this.report_faci_gym = report_faci_gym;
	}

	public void setreport_faci_sauna(String report_faci_sauna) {
		this.report_faci_sauna = report_faci_sauna;
	}

	public void setreport_faci_pool(String report_faci_pool) {
		this.report_faci_pool = report_faci_pool;
	}

	public void setreport_faci_fire_alarm(String report_faci_fire_alarm) {
		this.report_faci_fire_alarm = report_faci_fire_alarm;
	}

	public void setreport_faci_cctv(String report_faci_cctv) {
		this.report_faci_cctv = report_faci_cctv;
	}

	public void setreport_faci_elevator(String report_faci_elevator) {
		this.report_faci_elevator = report_faci_elevator;
	}

	public void setreport_util_electricity(String report_util_electricity) {
		this.report_util_electricity = report_util_electricity;
	}

	public void setreport_util_water(String report_util_water) {
		this.report_util_water = report_util_water;
	}

	public void setreport_util_telephone(String report_util_telephone) {
		this.report_util_telephone = report_util_telephone;
	}

	public void setreport_util_garbage(String report_util_garbage) {
		this.report_util_garbage = report_util_garbage;
	}

	public void setreport_landmarks_1(String report_landmarks_1) {
		this.report_landmarks_1 = report_landmarks_1;
	}

	public void setreport_landmarks_2(String report_landmarks_2) {
		this.report_landmarks_2 = report_landmarks_2;
	}

	public void setreport_landmarks_3(String report_landmarks_3) {
		this.report_landmarks_3 = report_landmarks_3;
	}

	public void setreport_landmarks_4(String report_landmarks_4) {
		this.report_landmarks_4 = report_landmarks_4;
	}

	public void setreport_landmarks_5(String report_landmarks_5) {
		this.report_landmarks_5 = report_landmarks_5;
	}

	public void setreport_distance_1(String report_distance_1) {
		this.report_distance_1 = report_distance_1;
	}

	public void setreport_distance_2(String report_distance_2) {
		this.report_distance_2 = report_distance_2;
	}

	public void setreport_distance_3(String report_distance_3) {
		this.report_distance_3 = report_distance_3;
	}

	public void setreport_distance_4(String report_distance_4) {
		this.report_distance_4 = report_distance_4;
	}

	public void setreport_distance_5(String report_distance_5) {
		this.report_distance_5 = report_distance_5;
	}

	public void setreport_zonal_location(String report_zonal_location) {
		this.report_zonal_location = report_zonal_location;
	}

	public void setreport_zonal_lot_classification(
			String report_zonal_lot_classification) {
		this.report_zonal_lot_classification = report_zonal_lot_classification;
	}

	public void setreport_zonal_value(String report_zonal_value) {
		this.report_zonal_value = report_zonal_value;
	}

	public void setreport_final_value_total_appraised_value(
			String report_final_value_total_appraised_value) {
		this.report_final_value_total_appraised_value = report_final_value_total_appraised_value;
	}

	public void setreport_final_value_recommended_deductions_desc(
			String report_final_value_recommended_deductions_desc) {
		this.report_final_value_recommended_deductions_desc = report_final_value_recommended_deductions_desc;
	}

	public void setreport_final_value_recommended_deductions_rate(
			String report_final_value_recommended_deductions_rate) {
		this.report_final_value_recommended_deductions_rate = report_final_value_recommended_deductions_rate;
	}

	public void setreport_final_value_recommended_deductions_amt(
			String report_final_value_recommended_deductions_amt) {
		this.report_final_value_recommended_deductions_amt = report_final_value_recommended_deductions_amt;
	}

	public void setreport_final_value_net_appraised_value(
			String report_final_value_net_appraised_value) {
		this.report_final_value_net_appraised_value = report_final_value_net_appraised_value;
	}

	public void setreport_final_value_quick_sale_value_rate(
			String report_final_value_quick_sale_value_rate) {
		this.report_final_value_quick_sale_value_rate = report_final_value_quick_sale_value_rate;
	}

	public void setreport_final_value_quick_sale_value_amt(
			String report_final_value_quick_sale_value_amt) {
		this.report_final_value_quick_sale_value_amt = report_final_value_quick_sale_value_amt;
	}

	public void setreport_factors_of_concern(String report_factors_of_concern) {
		this.report_factors_of_concern = report_factors_of_concern;
	}

	public void setreport_suggested_corrective_actions(
			String report_suggested_corrective_actions) {
		this.report_suggested_corrective_actions = report_suggested_corrective_actions;
	}

	public void setreport_remarks(String report_remarks) {
		this.report_remarks = report_remarks;
	}

	public void setreport_requirements(String report_requirements) {
		this.report_requirements = report_requirements;
	}
	
	public void setreport_time_inspected(String report_time_inspected) {
		this.report_time_inspected = report_time_inspected;
	}
	
	public void setreport_comp1_date_month(String report_comp1_date_month) {
	this.report_comp1_date_month = report_comp1_date_month;
	}

	public void setreport_comp1_date_day(String report_comp1_date_day) {
	this.report_comp1_date_day = report_comp1_date_day;
	}

	public void setreport_comp1_date_year(String report_comp1_date_year) {
	this.report_comp1_date_year = report_comp1_date_year;
	}

	public void setreport_comp1_source(String report_comp1_source) {
	this.report_comp1_source = report_comp1_source;
	}

	public void setreport_comp1_contact_no(String report_comp1_contact_no) {
	this.report_comp1_contact_no = report_comp1_contact_no;
	}

	public void setreport_comp1_location(String report_comp1_location) {
	this.report_comp1_location = report_comp1_location;
	}

	public void setreport_comp1_area(String report_comp1_area) {
	this.report_comp1_area = report_comp1_area;
	}

	public void setreport_comp1_base_price(String report_comp1_base_price) {
	this.report_comp1_base_price = report_comp1_base_price;
	}

	public void setreport_comp1_price_sqm(String report_comp1_price_sqm) {
	this.report_comp1_price_sqm = report_comp1_price_sqm;
	}

	public void setreport_comp1_discount_rate(String report_comp1_discount_rate) {
	this.report_comp1_discount_rate = report_comp1_discount_rate;
	}

	public void setreport_comp1_discounts(String report_comp1_discounts) {
	this.report_comp1_discounts = report_comp1_discounts;
	}

	public void setreport_comp1_selling_price(String report_comp1_selling_price) {
	this.report_comp1_selling_price = report_comp1_selling_price;
	}

	public void setreport_comp1_rec_location(String report_comp1_rec_location) {
	this.report_comp1_rec_location = report_comp1_rec_location;
	}

	public void setreport_comp1_rec_unit_floor_location(String report_comp1_rec_unit_floor_location) {
	this.report_comp1_rec_unit_floor_location = report_comp1_rec_unit_floor_location;
	}

	public void setreport_comp1_rec_orientation(String report_comp1_rec_orientation) {
	this.report_comp1_rec_orientation = report_comp1_rec_orientation;
	}

	public void setreport_comp1_rec_size(String report_comp1_rec_size) {
	this.report_comp1_rec_size = report_comp1_rec_size;
	}

	public void setreport_comp1_rec_unit_condition(String report_comp1_rec_unit_condition) {
	this.report_comp1_rec_unit_condition = report_comp1_rec_unit_condition;
	}

	public void setreport_comp1_rec_amenities(String report_comp1_rec_amenities) {
	this.report_comp1_rec_amenities = report_comp1_rec_amenities;
	}

	public void setreport_comp1_rec_unit_features(String report_comp1_rec_unit_features) {
	this.report_comp1_rec_unit_features = report_comp1_rec_unit_features;
	}

	public void setreport_comp1_concluded_adjustment(String report_comp1_concluded_adjustment) {
	this.report_comp1_concluded_adjustment = report_comp1_concluded_adjustment;
	}

	public void setreport_comp1_adjusted_value(String report_comp1_adjusted_value) {
	this.report_comp1_adjusted_value = report_comp1_adjusted_value;
	}

	public void setreport_comp2_date_month(String report_comp2_date_month) {
	this.report_comp2_date_month = report_comp2_date_month;
	}

	public void setreport_comp2_date_day(String report_comp2_date_day) {
	this.report_comp2_date_day = report_comp2_date_day;
	}

	public void setreport_comp2_date_year(String report_comp2_date_year) {
	this.report_comp2_date_year = report_comp2_date_year;
	}

	public void setreport_comp2_source(String report_comp2_source) {
	this.report_comp2_source = report_comp2_source;
	}

	public void setreport_comp2_contact_no(String report_comp2_contact_no) {
	this.report_comp2_contact_no = report_comp2_contact_no;
	}

	public void setreport_comp2_location(String report_comp2_location) {
	this.report_comp2_location = report_comp2_location;
	}

	public void setreport_comp2_area(String report_comp2_area) {
	this.report_comp2_area = report_comp2_area;
	}

	public void setreport_comp2_base_price(String report_comp2_base_price) {
	this.report_comp2_base_price = report_comp2_base_price;
	}

	public void setreport_comp2_price_sqm(String report_comp2_price_sqm) {
	this.report_comp2_price_sqm = report_comp2_price_sqm;
	}

	public void setreport_comp2_discount_rate(String report_comp2_discount_rate) {
	this.report_comp2_discount_rate = report_comp2_discount_rate;
	}

	public void setreport_comp2_discounts(String report_comp2_discounts) {
	this.report_comp2_discounts = report_comp2_discounts;
	}

	public void setreport_comp2_selling_price(String report_comp2_selling_price) {
	this.report_comp2_selling_price = report_comp2_selling_price;
	}

	public void setreport_comp2_rec_location(String report_comp2_rec_location) {
	this.report_comp2_rec_location = report_comp2_rec_location;
	}

	public void setreport_comp2_rec_unit_floor_location(String report_comp2_rec_unit_floor_location) {
	this.report_comp2_rec_unit_floor_location = report_comp2_rec_unit_floor_location;
	}

	public void setreport_comp2_rec_orientation(String report_comp2_rec_orientation) {
	this.report_comp2_rec_orientation = report_comp2_rec_orientation;
	}

	public void setreport_comp2_rec_size(String report_comp2_rec_size) {
	this.report_comp2_rec_size = report_comp2_rec_size;
	}

	public void setreport_comp2_rec_unit_condition(String report_comp2_rec_unit_condition) {
	this.report_comp2_rec_unit_condition = report_comp2_rec_unit_condition;
	}

	public void setreport_comp2_rec_amenities(String report_comp2_rec_amenities) {
	this.report_comp2_rec_amenities = report_comp2_rec_amenities;
	}

	public void setreport_comp2_rec_unit_features(String report_comp2_rec_unit_features) {
	this.report_comp2_rec_unit_features = report_comp2_rec_unit_features;
	}

	public void setreport_comp2_concluded_adjustment(String report_comp2_concluded_adjustment) {
	this.report_comp2_concluded_adjustment = report_comp2_concluded_adjustment;
	}

	public void setreport_comp2_adjusted_value(String report_comp2_adjusted_value) {
	this.report_comp2_adjusted_value = report_comp2_adjusted_value;
	}

	public void setreport_comp3_date_month(String report_comp3_date_month) {
	this.report_comp3_date_month = report_comp3_date_month;
	}

	public void setreport_comp3_date_day(String report_comp3_date_day) {
	this.report_comp3_date_day = report_comp3_date_day;
	}

	public void setreport_comp3_date_year(String report_comp3_date_year) {
	this.report_comp3_date_year = report_comp3_date_year;
	}

	public void setreport_comp3_source(String report_comp3_source) {
	this.report_comp3_source = report_comp3_source;
	}

	public void setreport_comp3_contact_no(String report_comp3_contact_no) {
	this.report_comp3_contact_no = report_comp3_contact_no;
	}

	public void setreport_comp3_location(String report_comp3_location) {
	this.report_comp3_location = report_comp3_location;
	}

	public void setreport_comp3_area(String report_comp3_area) {
	this.report_comp3_area = report_comp3_area;
	}

	public void setreport_comp3_base_price(String report_comp3_base_price) {
	this.report_comp3_base_price = report_comp3_base_price;
	}

	public void setreport_comp3_price_sqm(String report_comp3_price_sqm) {
	this.report_comp3_price_sqm = report_comp3_price_sqm;
	}

	public void setreport_comp3_discount_rate(String report_comp3_discount_rate) {
	this.report_comp3_discount_rate = report_comp3_discount_rate;
	}

	public void setreport_comp3_discounts(String report_comp3_discounts) {
	this.report_comp3_discounts = report_comp3_discounts;
	}

	public void setreport_comp3_selling_price(String report_comp3_selling_price) {
	this.report_comp3_selling_price = report_comp3_selling_price;
	}

	public void setreport_comp3_rec_location(String report_comp3_rec_location) {
	this.report_comp3_rec_location = report_comp3_rec_location;
	}

	public void setreport_comp3_rec_unit_floor_location(String report_comp3_rec_unit_floor_location) {
	this.report_comp3_rec_unit_floor_location = report_comp3_rec_unit_floor_location;
	}

	public void setreport_comp3_rec_orientation(String report_comp3_rec_orientation) {
	this.report_comp3_rec_orientation = report_comp3_rec_orientation;
	}

	public void setreport_comp3_rec_size(String report_comp3_rec_size) {
	this.report_comp3_rec_size = report_comp3_rec_size;
	}

	public void setreport_comp3_rec_unit_condition(String report_comp3_rec_unit_condition) {
	this.report_comp3_rec_unit_condition = report_comp3_rec_unit_condition;
	}

	public void setreport_comp3_rec_amenities(String report_comp3_rec_amenities) {
	this.report_comp3_rec_amenities = report_comp3_rec_amenities;
	}

	public void setreport_comp3_rec_unit_features(String report_comp3_rec_unit_features) {
	this.report_comp3_rec_unit_features = report_comp3_rec_unit_features;
	}

	public void setreport_comp3_concluded_adjustment(String report_comp3_concluded_adjustment) {
	this.report_comp3_concluded_adjustment = report_comp3_concluded_adjustment;
	}

	public void setreport_comp3_adjusted_value(String report_comp3_adjusted_value) {
	this.report_comp3_adjusted_value = report_comp3_adjusted_value;
	}

	public void setreport_comp4_date_month(String report_comp4_date_month) {
	this.report_comp4_date_month = report_comp4_date_month;
	}

	public void setreport_comp4_date_day(String report_comp4_date_day) {
	this.report_comp4_date_day = report_comp4_date_day;
	}

	public void setreport_comp4_date_year(String report_comp4_date_year) {
	this.report_comp4_date_year = report_comp4_date_year;
	}

	public void setreport_comp4_source(String report_comp4_source) {
	this.report_comp4_source = report_comp4_source;
	}

	public void setreport_comp4_contact_no(String report_comp4_contact_no) {
	this.report_comp4_contact_no = report_comp4_contact_no;
	}

	public void setreport_comp4_location(String report_comp4_location) {
	this.report_comp4_location = report_comp4_location;
	}

	public void setreport_comp4_area(String report_comp4_area) {
	this.report_comp4_area = report_comp4_area;
	}

	public void setreport_comp4_base_price(String report_comp4_base_price) {
	this.report_comp4_base_price = report_comp4_base_price;
	}

	public void setreport_comp4_price_sqm(String report_comp4_price_sqm) {
	this.report_comp4_price_sqm = report_comp4_price_sqm;
	}

	public void setreport_comp4_discount_rate(String report_comp4_discount_rate) {
	this.report_comp4_discount_rate = report_comp4_discount_rate;
	}

	public void setreport_comp4_discounts(String report_comp4_discounts) {
	this.report_comp4_discounts = report_comp4_discounts;
	}

	public void setreport_comp4_selling_price(String report_comp4_selling_price) {
	this.report_comp4_selling_price = report_comp4_selling_price;
	}

	public void setreport_comp4_rec_location(String report_comp4_rec_location) {
	this.report_comp4_rec_location = report_comp4_rec_location;
	}

	public void setreport_comp4_rec_unit_floor_location(String report_comp4_rec_unit_floor_location) {
	this.report_comp4_rec_unit_floor_location = report_comp4_rec_unit_floor_location;
	}

	public void setreport_comp4_rec_orientation(String report_comp4_rec_orientation) {
	this.report_comp4_rec_orientation = report_comp4_rec_orientation;
	}

	public void setreport_comp4_rec_size(String report_comp4_rec_size) {
	this.report_comp4_rec_size = report_comp4_rec_size;
	}

	public void setreport_comp4_rec_unit_condition(String report_comp4_rec_unit_condition) {
	this.report_comp4_rec_unit_condition = report_comp4_rec_unit_condition;
	}

	public void setreport_comp4_rec_amenities(String report_comp4_rec_amenities) {
	this.report_comp4_rec_amenities = report_comp4_rec_amenities;
	}

	public void setreport_comp4_rec_unit_features(String report_comp4_rec_unit_features) {
	this.report_comp4_rec_unit_features = report_comp4_rec_unit_features;
	}

	public void setreport_comp4_concluded_adjustment(String report_comp4_concluded_adjustment) {
	this.report_comp4_concluded_adjustment = report_comp4_concluded_adjustment;
	}

	public void setreport_comp4_adjusted_value(String report_comp4_adjusted_value) {
	this.report_comp4_adjusted_value = report_comp4_adjusted_value;
	}

	public void setreport_comp5_date_month(String report_comp5_date_month) {
	this.report_comp5_date_month = report_comp5_date_month;
	}

	public void setreport_comp5_date_day(String report_comp5_date_day) {
	this.report_comp5_date_day = report_comp5_date_day;
	}

	public void setreport_comp5_date_year(String report_comp5_date_year) {
	this.report_comp5_date_year = report_comp5_date_year;
	}

	public void setreport_comp5_source(String report_comp5_source) {
	this.report_comp5_source = report_comp5_source;
	}

	public void setreport_comp5_contact_no(String report_comp5_contact_no) {
	this.report_comp5_contact_no = report_comp5_contact_no;
	}

	public void setreport_comp5_location(String report_comp5_location) {
	this.report_comp5_location = report_comp5_location;
	}

	public void setreport_comp5_area(String report_comp5_area) {
	this.report_comp5_area = report_comp5_area;
	}

	public void setreport_comp5_base_price(String report_comp5_base_price) {
	this.report_comp5_base_price = report_comp5_base_price;
	}

	public void setreport_comp5_price_sqm(String report_comp5_price_sqm) {
	this.report_comp5_price_sqm = report_comp5_price_sqm;
	}

	public void setreport_comp5_discount_rate(String report_comp5_discount_rate) {
	this.report_comp5_discount_rate = report_comp5_discount_rate;
	}

	public void setreport_comp5_discounts(String report_comp5_discounts) {
	this.report_comp5_discounts = report_comp5_discounts;
	}

	public void setreport_comp5_selling_price(String report_comp5_selling_price) {
	this.report_comp5_selling_price = report_comp5_selling_price;
	}

	public void setreport_comp5_rec_location(String report_comp5_rec_location) {
	this.report_comp5_rec_location = report_comp5_rec_location;
	}

	public void setreport_comp5_rec_unit_floor_location(String report_comp5_rec_unit_floor_location) {
	this.report_comp5_rec_unit_floor_location = report_comp5_rec_unit_floor_location;
	}

	public void setreport_comp5_rec_orientation(String report_comp5_rec_orientation) {
	this.report_comp5_rec_orientation = report_comp5_rec_orientation;
	}

	public void setreport_comp5_rec_size(String report_comp5_rec_size) {
	this.report_comp5_rec_size = report_comp5_rec_size;
	}

	public void setreport_comp5_rec_unit_condition(String report_comp5_rec_unit_condition) {
	this.report_comp5_rec_unit_condition = report_comp5_rec_unit_condition;
	}

	public void setreport_comp5_rec_amenities(String report_comp5_rec_amenities) {
	this.report_comp5_rec_amenities = report_comp5_rec_amenities;
	}

	public void setreport_comp5_rec_unit_features(String report_comp5_rec_unit_features) {
	this.report_comp5_rec_unit_features = report_comp5_rec_unit_features;
	}

	public void setreport_comp5_concluded_adjustment(String report_comp5_concluded_adjustment) {
	this.report_comp5_concluded_adjustment = report_comp5_concluded_adjustment;
	}

	public void setreport_comp5_adjusted_value(String report_comp5_adjusted_value) {
	this.report_comp5_adjusted_value = report_comp5_adjusted_value;
	}

	public void setreport_comp_average(String report_comp_average) {
	this.report_comp_average = report_comp_average;
	}

	public void setreport_comp_rounded_to(String report_comp_rounded_to) {
	this.report_comp_rounded_to = report_comp_rounded_to;
	}
	
	public void setreport_comp1_rec_time_element(String report_comp1_rec_time_element) {
		this.report_comp1_rec_time_element = report_comp1_rec_time_element;
	}
	
	public void setreport_comp2_rec_time_element(String report_comp2_rec_time_element) {
		this.report_comp2_rec_time_element = report_comp2_rec_time_element;
	}
	
	public void setreport_comp3_rec_time_element(String report_comp3_rec_time_element) {
		this.report_comp3_rec_time_element = report_comp3_rec_time_element;
	}
	
	public void setreport_comp4_rec_time_element(String report_comp4_rec_time_element) {
		this.report_comp4_rec_time_element = report_comp4_rec_time_element;
	}
	
	public void setreport_comp5_rec_time_element(String report_comp5_rec_time_element) {
		this.report_comp5_rec_time_element = report_comp5_rec_time_element;
	}
	
	public void setreport_comp1_remarks(String report_comp1_remarks) {
		this.report_comp1_remarks = report_comp1_remarks;
	}
	
	public void setreport_comp2_remarks(String report_comp2_remarks) {
		this.report_comp2_remarks = report_comp2_remarks;
	}
	
	public void setreport_comp3_remarks(String report_comp3_remarks) {
		this.report_comp3_remarks = report_comp3_remarks;
	}
	
	public void setreport_comp4_remarks(String report_comp4_remarks) {
		this.report_comp4_remarks = report_comp4_remarks;
	}
	
	public void setreport_comp5_remarks(String report_comp5_remarks) {
		this.report_comp5_remarks = report_comp5_remarks;
	}

	//Added from IAN
	public void setreport_prev_date(String report_prev_date) {
		this.report_prev_date = report_prev_date;
	}

	public void setreport_prev_appraiser(String report_prev_appraiser) {
		this.report_prev_appraiser = report_prev_appraiser;
	}

	public void setreport_prev_total_appraised_value(String report_prev_total_appraised_value) {
		this.report_prev_total_appraised_value = report_prev_total_appraised_value;
	}
}
