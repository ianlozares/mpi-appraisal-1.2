package com.gds.appraisalmaybank.database;

public class Request_Contact_Persons {

	// private variables
	int id;
	String rt_id;
	String rq_contact_person;
	String rq_contact_mobile_no_prefix;
	String rq_contact_mobile_no;
	String rq_contact_landline;

	// Empty constructor
	public Request_Contact_Persons() {

	}

	// constructor
	public Request_Contact_Persons(int id, String rt_id,
			String rq_contact_person, String rq_contact_mobile_no_prefix,
			String rq_contact_mobile_no, String rq_contact_landline) {
		this.id = id;
		this.rt_id = rt_id;
		this.rq_contact_person = rq_contact_person;
		this.rq_contact_mobile_no_prefix = rq_contact_mobile_no_prefix;
		this.rq_contact_mobile_no = rq_contact_mobile_no;
		this.rq_contact_landline = rq_contact_landline;
	}

	// constructor
	public Request_Contact_Persons(String rt_id, String rq_contact_person,
			String rq_contact_mobile_no_prefix, String rq_contact_mobile_no,
			String rq_contact_landline) {

		this.rt_id = rt_id;
		this.rq_contact_person = rq_contact_person;
		this.rq_contact_mobile_no_prefix = rq_contact_mobile_no_prefix;
		this.rq_contact_mobile_no = rq_contact_mobile_no;
		this.rq_contact_landline = rq_contact_landline;
	}

	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	// getting rt_id
	public String getrt_id() {
		return this.rt_id;
	}

	// setting rt_id
	public void setrt_id(String rt_id) {
		this.rt_id = rt_id;
	}

	// getting rq_contact_person
	public String getrq_contact_person() {
		return this.rq_contact_person;
	}

	// setting rq_contact_person
	public void setrq_contact_person(String rq_contact_person) {
		this.rq_contact_person = rq_contact_person;
	}

	// getting rq_contact_mobile_no_prefix
	public String getrq_contact_mobile_no_prefix() {
		return this.rq_contact_mobile_no_prefix;
	}

	// setting rq_contact_mobile_no_prefix
	public void setrq_contact_mobile_no_prefix(String rq_contact_mobile_no_prefix) {
		this.rq_contact_mobile_no_prefix = rq_contact_mobile_no_prefix;
	}

	// getting rq_contact_mobile_no
	public String getrq_contact_mobile_no() {
		return this.rq_contact_mobile_no;
	}

	// setting rq_contact_mobile_no
	public void setrq_contact_mobile_no(String rq_contact_mobile_no) {
		this.rq_contact_mobile_no = rq_contact_mobile_no;
	}

	// getting rq_contact_landline
	public String getrq_contact_landline() {
		return this.rq_contact_landline;
	}

	// setting rq_contact_landline
	public void setrq_contact_landline(String rq_contact_landline) {
		this.rq_contact_landline = rq_contact_landline;
	}
}
