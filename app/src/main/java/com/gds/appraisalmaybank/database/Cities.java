package com.gds.appraisalmaybank.database;

public class Cities {

	// private variables
	int id;
	String name;
	String province_id;

	// Empty constructor
	public Cities() {

	}

	// constructor
	public Cities(int id, String name, String province_id) {
		this.id = id;
		this.name = name;
		this.province_id = province_id;
	}

	// constructor
	public Cities(String name, String province_id) {
		this.name = name;
		this.province_id = province_id;
	}

	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	// getting name
	public String getname() {
		return this.name;
	}

	// setting name
	public void setname(String name) {
		this.name = name;
	}

	// getting province_id
	public String getprovince_id() {
		return this.province_id;
	}

	// setting province_id
	public void setprovince_id(String province_id) {
		this.province_id = province_id;
	}
}
