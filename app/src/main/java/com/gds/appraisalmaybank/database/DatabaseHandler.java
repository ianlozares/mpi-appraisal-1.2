package com.gds.appraisalmaybank.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.gds.appraisalmaybank.main.Global;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

public class DatabaseHandler extends SQLiteOpenHelper {
	Global gs = new Global();
	// Database Version
	
	private static final String tbl_submission_logs = "submission_logs";
	private static final String tbl_request_form = "tbl_request_form";
	private static final String tbl_request_type = "tbl_request_type";
	private static final String tbl_request_contact_persons = "tbl_request_contact_persons";
	private static final String tbl_request_collateral_address = "tbl_request_collateral_address";
	private static final String tbl_request_attachments = "tbl_request_attachments";
	private static final String tbl_images = "tbl_images";
	private static final String tbl_regions = "tbl_regions";
	private static final String tbl_required_attachments = "tbl_required_attachments";
	private static final String tbl_attachments = "tbl_attachments";
	private static final String tbl_report_accepted_jobs = "tbl_report_accepted_jobs";
	private static final String tbl_report_accepted_jobs_contacts = "tbl_report_accepted_jobs_contacts";
	private static final String tbl_report_filename = "report_filename";
	private static final String tbl_townhouse_condo = "townhouse_condo";
	public static final String tbl_motor_vehicle = "motor_vehicle";
	public static final String tbl_motor_vehicle_prev = "motor_vehicle_previous_appraisal";
	private static final String tbl_vacant_lot = "vacant_lot";
	private static final String tbl_ppcr = "ppcr";
	private static final String tbl_land_improvements = "land_improvements";
	private static final String tbl_construction_ebm = "construction_ebm";
	private static final String tbl_cities = "tbl_cities";
	private static final String tbl_provinces = "tbl_provinces";
	private static final String tbl_condo = "condo";
	private static final String tbl_townhouse = "townhouse";

	// tbl_request_form Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_rf_date_requested_month = "rf_date_requested_month";
	private static final String KEY_rf_date_requested_day = "rf_date_requested_day";
	private static final String KEY_rf_date_requested_year = "rf_date_requested_year";
	private static final String KEY_rf_classification = "rf_classification";
	private static final String KEY_rf_account_fname = "rf_account_fname";
	private static final String KEY_rf_account_mname = "rf_account_mname";
	private static final String KEY_rf_account_lname = "rf_account_lname";
	private static final String KEY_rf_requesting_party = "rf_requesting_party";

	// tbl_request_type Columns names
	private static final String KEY_rf_id = "rf_id";
	private static final String KEY_rt_control_no = "rt_control_no";
	private static final String KEY_rt_nature_of_appraisal = "rt_nature_of_appraisal";
	private static final String KEY_rt_appraisal_type = "rt_appraisal_type";
	private static final String KEY_rt_pref_ins_date1_month = "rt_pref_ins_date1_month";
	private static final String KEY_rt_pref_ins_date1_day = "rt_pref_ins_date1_day";
	private static final String KEY_rt_pref_ins_date1_year = "rt_pref_ins_date1_year";
	private static final String KEY_rt_pref_ins_date2_month = "rt_pref_ins_date2_month";
	private static final String KEY_rt_pref_ins_date2_day = "rt_pref_ins_date2_day";
	private static final String KEY_rt_pref_ins_date2_year = "rt_pref_ins_date2_year";
	private static final String KEY_rt_mv_year = "rt_mv_year";
	private static final String KEY_rt_mv_make = "rt_mv_make";
	private static final String KEY_rt_mv_model = "rt_mv_model";
	private static final String KEY_rt_mv_odometer = "rt_mv_odometer";
	private static final String KEY_rt_mv_vin = "rt_mv_vin";
	private static final String KEY_rt_me_quantity = "rt_me_quantity";
	private static final String KEY_rt_me_type = "rt_me_type";
	private static final String KEY_rt_me_manufacturer = "rt_me_manufacturer";
	private static final String KEY_rt_me_model = "rt_me_model";
	private static final String KEY_rt_me_serial = "rt_me_serial";
	private static final String KEY_rt_me_age = "rt_me_age";
	private static final String KEY_rt_me_condition = "rt_me_condition";

	// tbl_report_accepted_jobs Columns names
	private static final String unit_no = "unit_no";
	private static final String building_name = "building_name";
	private static final String street_no = "street_no";
	private static final String street_name = "street_name";
	private static final String village = "village";
	private static final String district = "district";
	private static final String zip_code = "zip_code";
	private static final String city = "city";
	private static final String province = "province";
	private static final String region = "region";
	private static final String country = "country";
	
	private static final String report_car_loan_purpose = "car_loan_purpose";
	private static final String report_vehicle_type = "vehicle_type";
	private static final String report_car_model = "car_model";
	private static final String report_car_mileage = "car_mileage";
	private static final String report_car_series = "car_series";
	private static final String report_car_color = "car_color";
	private static final String report_car_plate_no = "car_plate_no";
	private static final String report_car_body_type = "car_body_type";
	private static final String report_car_displacement = "car_displacement";
	private static final String report_car_motor_no = "car_motor_no";
	private static final String report_car_chassis_no = "car_chassis_no";
	private static final String report_car_no_cylinders = "car_no_of_cylinders";
	private static final String report_car_cr_no = "car_cr_no";
	private static final String report_car_cr_date_month = "car_cr_date_month";
	private static final String report_car_cr_date_day = "car_cr_date_day";
	private static final String report_car_cr_date_year = "car_cr_date_year";
	
	
	// tbl_request_contact_no Columns names
	private static final String KEY_rt_id = "rt_id";
	private static final String KEY_rq_contact_person = "rq_contact_person";
	private static final String KEY_rq_contact_mobile_no_prefix = "rq_contact_mobile_no_prefix";
	private static final String KEY_rq_contact_mobile_no = "rq_contact_mobile_no";
	private static final String KEY_rq_contact_landline = "rq_contact_landline";
	// tbl_request_collateral_address Columns names
	private static final String KEY_rq_tct_no = "rq_tct_no";
	private static final String KEY_rq_unit_no = "rq_unit_no";
	private static final String KEY_rq_building_name = "rq_building_name";
	private static final String KEY_rq_street_no = "rq_street_no";
	private static final String KEY_rq_street_name = "rq_street_name";
	private static final String KEY_rq_village = "rq_village";
	private static final String KEY_rq_barangay = "rq_barangay";
	private static final String KEY_rq_zip_code = "rq_zip_code";
	private static final String KEY_rq_city = "rq_city";
	private static final String KEY_rq_province = "rq_province";
	private static final String KEY_rq_region = "rq_region";
	private static final String KEY_rq_country = "rq_country";
	// tbl_request_attachments Columns names
	private static final String KEY_rq_uid = "rq_uid";
	private static final String KEY_rq_file = "rq_file";
	private static final String KEY_rq_filename = "rq_filename";
	private static final String KEY_rq_appraisal_type = "rq_appraisal_type";
	private static final String KEY_rq_candidate_done = "rq_candidate_done";

	// tbl_images Columns names
	private static final String KEY_path = "path";
	private static final String KEY_filename = "filename";

	// tbl regions
	private static final String KEY_name = "name";
	// tbl required_attachments
	private static final String KEY_appraisal_type = "appraisal_type";
	// report jobs Columns names
	private static final String Report_record_id = "record_id";
	private static final String Report_rt_id = "rt_id";
	private static final String Report_dr_month = "dr_month";
	private static final String Report_dr_day = "dr_day";
	private static final String Report_dr_year = "dr_year";
	private static final String Report_classification = "classification";
	private static final String Report_fname = "fname";
	private static final String Report_mname = "mname";
	private static final String Report_lname = "lname";
	private static final String Report_requesting_party = "requesting_party";
	private static final String Report_requestor = "requestor";
	private static final String Report_control_no = "control_no";
	private static final String Report_appraisal_type = "appraisal_type";
	private static final String Report_nature_appraisal = "nature_appraisal";
	private static final String Report_ins_date1_month = "ins_date1_month";
	private static final String Report_ins_date1_day = "ins_date1_day";
	private static final String Report_ins_date1_year = "ins_date1_year";
	private static final String Report_ins_date2_month = "ins_date2_month";
	private static final String Report_ins_date2_day = "ins_date2_day";
	private static final String Report_ins_date2_year = "ins_date2_year";
	private static final String Report_com_month = "com_month";
	private static final String Report_com_day = "com_day";
	private static final String Report_com_year = "com_year";
	private static final String Report_tct_no = "tct_no";
	private static final String Report_unit_no = "unit_no";
	private static final String Report_building_name = "building_name";
	private static final String Report_street_no = "street_no";
	private static final String Report_street_name = "street_name";
	private static final String Report_village = "village";
	private static final String Report_district = "district";
	private static final String Report_zip_code = "zip_code";
	private static final String Report_city = "city";
	private static final String Report_province = "province";
	private static final String Report_region = "region";
	private static final String Report_country = "country";
	private static final String Report_counter = "counter";
	private static final String Report_vehicle_type = "vehicle_type";
	private static final String Report_car_model = "car_model";
	private static final String Report_lot_no = "lot_no";
	private static final String Report_block_no = "block_no";
	private static final String Report_car_loan_purpose = "car_loan_purpose";
	private static final String Report_car_mileage = "car_mileage";
	private static final String Report_car_series = "car_series";
	private static final String Report_car_color = "car_color";
	private static final String Report_car_plate_no = "car_plate_no";
	private static final String Report_car_body_type = "car_body_type";
	private static final String Report_car_displacement = "car_displacement";
	private static final String Report_car_motor_no = "car_motor_no";
	private static final String Report_car_chassis_no = "car_chassis_no";
	private static final String Report_car_no_cylinders = "car_no_of_cylinders";
	private static final String Report_car_cr_no = "car_cr_no";
	private static final String Report_car_cr_date_month = "car_cr_date_month";
	private static final String Report_car_cr_date_day = "car_cr_date_day";
	private static final String Report_car_cr_date_year = "car_cr_date_year";
	private static final String Report_rework_reason = "rework_reason";
	private static final String Report_kind_of_appraisal = "kind_of_appraisal";
	private static final String Report_app_request_remarks = "app_request_remarks";
	private static final String Report_app_branch_code = "app_branch_code";
	private static final String Report_app_main_id = "app_main_id";

	// report jobs contacts Columns names
	private static final String Report_contact_person = "contact_person";
	private static final String Report_contact_mobile_no_prefix = "contact_mobile_no_prefix";
	private static final String Report_contact_mobile_no = "contact_mobile_no";
	private static final String Report_contact_landline = "contact_landline";

	// Townhouse_condo
	private static final String report_cct_no = "report_cct_no";
	private static final String report_area = "report_area";
	private static final String report_date_reg_month = "report_date_reg_month";
	private static final String report_date_reg_day = "report_date_reg_day";
	private static final String report_date_reg_year = "report_date_reg_year";
	private static final String report_reg_owner = "report_reg_owner";
	private static final String report_homeowners_assoc = "report_homeowners_assoc";
	private static final String report_admin_office = "report_admin_office";
	private static final String report_tax_mapping = "report_tax_mapping";
	private static final String report_lra_office = "report_lra_office";
	private static final String report_subd_map = "report_subd_map";
	private static final String report_lot_config = "report_lot_config";
	private static final String report_street = "report_street";
	private static final String report_sidewalk = "report_sidewalk";
	private static final String report_gutter = "report_gutter";
	private static final String report_street_lights = "report_street_lights";
	private static final String report_shape = "report_shape";
	private static final String report_elevation = "report_elevation";
	private static final String report_road = "report_road";
	private static final String report_frontage = "report_frontage";
	private static final String report_depth = "report_depth";
	private static final String report_electricity = "report_electricity";
	private static final String report_water = "report_water";
	private static final String report_telephone = "report_telephone";
	private static final String report_drainage = "report_drainage";
	private static final String report_garbage = "report_garbage";
	private static final String report_jeepneys = "report_jeepneys";
	private static final String report_bus = "report_bus";
	private static final String report_taxi = "report_taxi";
	private static final String report_tricycle = "report_tricycle";
	private static final String report_recreation_facilities = "report_recreation_facilities";
	private static final String report_security = "report_security";
	private static final String report_clubhouse = "report_clubhouse";
	private static final String report_swimming_pool = "report_swimming_pool";
	private static final String report_community = "report_community";
	private static final String report_classification = "report_classification";
	private static final String report_growth_rate = "report_growth_rate";
	private static final String report_adverse = "report_adverse";
	private static final String report_occupancy = "report_occupancy";
	private static final String report_right = "report_right";
	private static final String report_left = "report_left";
	private static final String report_rear = "report_rear";
	private static final String report_front = "report_front";
	private static final String report_desc = "report_desc";
	private static final String report_total_area = "report_total_area";
	private static final String report_deduc = "report_deduc";
	private static final String report_net_area = "report_net_area";
	private static final String report_unit_value = "report_unit_value";
	private static final String report_total_parking_area = "report_total_parking_area";
	private static final String report_parking_area_value = "report_parking_area_value";
	private static final String report_total_condo_value = "report_total_condo_value";
	private static final String report_total_parking_value = "report_total_parking_value";
	private static final String report_total_market_value = "report_total_market_value";
	
	// motor vehicle
	private static final String report_interior_dashboard = "report_interior_dashboard";
	private static final String report_interior_sidings = "report_interior_sidings";
	private static final String report_interior_seats = "report_interior_seats";
	private static final String report_interior_windows = "report_interior_windows";
	private static final String report_interior_ceiling = "report_interior_ceiling";
	private static final String report_interior_instrument = "report_interior_instrument";
	private static final String report_bodytype_grille = "report_bodytype_grille";
	private static final String report_bodytype_headlight = "report_bodytype_headlight";
	private static final String report_bodytype_fbumper = "report_bodytype_fbumper";
	private static final String report_bodytype_hood = "report_bodytype_hood";
	private static final String report_bodytype_rf_fender = "report_bodytype_rf_fender";
	private static final String report_bodytype_rf_door = "report_bodytype_rf_door";
	private static final String report_bodytype_rr_door = "report_bodytype_rr_door";
	private static final String report_bodytype_rr_fender = "report_bodytype_rr_fender";
	private static final String report_bodytype_backdoor = "report_bodytype_backdoor";
	private static final String report_bodytype_taillight = "report_bodytype_taillight";
	private static final String report_bodytype_r_bumper = "report_bodytype_r_bumper";
	private static final String report_bodytype_lr_fender = "report_bodytype_lr_fender";
	private static final String report_bodytype_lr_door = "report_bodytype_lr_door";
	private static final String report_bodytype_lf_door = "report_bodytype_lf_door";
	private static final String report_bodytype_lf_fender = "report_bodytype_lf_fender";
	private static final String report_bodytype_top = "report_bodytype_top";
	private static final String report_bodytype_paint = "report_bodytype_paint";
	private static final String report_bodytype_flooring = "report_bodytype_flooring";
	private static final String report_misc_stereo = "report_misc_stereo";
	private static final String report_misc_tools = "report_misc_tools";
	private static final String report_misc_speakers = "report_misc_speakers";
	private static final String report_misc_airbag = "report_misc_airbag";
	private static final String report_misc_tires = "report_misc_tires";
	private static final String report_misc_mag_wheels = "report_misc_mag_wheels";
	private static final String report_misc_carpet = "report_misc_carpet";
	private static final String report_misc_others = "report_misc_others";
	private static final String report_enginearea_fuel = "report_enginearea_fuel";
	private static final String report_enginearea_transmission1 = "report_enginearea_transmission1";
	private static final String report_enginearea_transmission2 = "report_enginearea_transmission2";
	private static final String report_enginearea_chassis = "report_enginearea_chassis";
	private static final String report_enginearea_battery = "report_enginearea_battery";
	private static final String report_enginearea_electrical = "report_enginearea_electrical";
	private static final String report_enginearea_engine = "report_enginearea_engine";
	private static final String report_verification = "report_verification";
	private static final String report_verification_district_office = "report_verification_district_office";
	private static final String report_verification_encumbrance = "report_verification_encumbrance";
	private static final String report_verification_registered_owner = "report_verification_registered_owner";
	private static final String report_place_inspected = "report_place_inspected";
	private static final String report_market_valuation_fair_value = "report_market_valuation_fair_value";
	private static final String report_market_valuation_min = "report_market_valuation_min";
	private static final String report_market_valuation_max = "report_market_valuation_max";
	private static final String report_market_valuation_remarks = "report_market_valuation_remarks";
	
	// vacantlot 
	private static final String report_tct_no = "report_tct_no";
	private static final String report_lot = "report_lot";
	private static final String report_block = "report_block";
	private static final String report_survey_no = "report_survey_no";
	private static final String report_place = "report_place";
	private static final String report_land_value = "report_land_value";
	private static final String report_date_year = "report_date_year";
	// land_improvements same with vacant lot
	private static final String report_date_inspected_month = "report_date_inspected_month";
	private static final String report_date_inspected_day = "report_date_inspected_day";
	private static final String report_date_inspected_year = "report_date_inspected_year";
	private static final String report_id_association = "report_id_association";
	private static final String report_id_tax = "report_id_tax";
	private static final String report_id_lra = "report_id_lra";
	private static final String report_id_lot_config = "report_id_lot_config";
	private static final String report_id_subd_map = "report_id_subd_map";
	private static final String report_id_nbrhood_checking = "report_id_nbrhood_checking";
	private static final String report_imp_street_name = "report_imp_street_name";
	private static final String report_imp_street = "report_imp_street";
	private static final String report_imp_sidewalk = "report_imp_sidewalk";
	private static final String report_imp_curb = "report_imp_curb";
	private static final String report_imp_drainage = "report_imp_drainage";
	private static final String report_imp_street_lights = "report_imp_street_lights";
	private static final String report_physical_corner_lot = "report_physical_corner_lot";
	private static final String report_physical_non_corner_lot = "report_physical_non_corner_lot";
	private static final String report_physical_perimeter_lot = "report_physical_perimeter_lot";
	private static final String report_physical_intersected_lot = "report_physical_intersected_lot";
	private static final String report_physical_interior_with_row = "report_physical_interior_with_row";
	private static final String report_physical_landlocked = "report_physical_landlocked";
	private static final String report_lotclass_per_tax_dec = "report_lotclass_per_tax_dec";
	private static final String report_lotclass_actual_usage = "report_lotclass_actual_usage";
	private static final String report_lotclass_neighborhood = "report_lotclass_neighborhood";
	private static final String report_lotclass_highest_best_use = "report_lotclass_highest_best_use";
	private static final String report_physical_shape = "report_physical_shape";
	private static final String report_physical_frontage = "report_physical_frontage";
	private static final String report_physical_depth = "report_physical_depth";
	private static final String report_physical_road = "report_physical_road";
	private static final String report_physical_elevation = "report_physical_elevation";
	private static final String report_physical_terrain = "report_physical_terrain";
	private static final String report_landmark_1 = "report_landmark_1";
	private static final String report_landmark_2 = "report_landmark_2";
	private static final String report_landmark_3 = "report_landmark_3";
	private static final String report_landmark_4 = "report_landmark_4";
	private static final String report_landmark_5 = "report_landmark_5";
	private static final String report_distance_1 = "report_distance_1";
	private static final String report_distance_2 = "report_distance_2";
	private static final String report_distance_3 = "report_distance_3";
	private static final String report_distance_4 = "report_distance_4";
	private static final String report_distance_5 = "report_distance_5";
	private static final String report_util_electricity = "report_util_electricity";
	private static final String report_util_water = "report_util_water";
	private static final String report_util_telephone = "report_util_telephone";
	private static final String report_util_garbage = "report_util_garbage";
	private static final String report_trans_jeep = "report_trans_jeep";
	private static final String report_trans_bus = "report_trans_bus";
	private static final String report_trans_taxi = "report_trans_taxi";
	private static final String report_trans_tricycle = "report_trans_tricycle";
	private static final String report_faci_recreation = "report_faci_recreation";
	private static final String report_faci_security = "report_faci_security";
	private static final String report_faci_clubhouse = "report_faci_clubhouse";
	private static final String report_faci_pool = "report_faci_pool";
	private static final String report_bound_right = "report_bound_right";
	private static final String report_bound_left = "report_bound_left";
	private static final String report_bound_rear = "report_bound_rear";
	private static final String report_bound_front = "report_bound_front";
	private static final String report_prev_date = "report_prev_date";
	private static final String report_prev_appraiser = "report_prev_appraiser";
	private static final String report_landimp_td_no = "report_landimp_td_no";
	private static final String report_townhouse_td_no = "report_townhouse_td_no";


	//ADDED By IAN MOTOR VEHICLE Comparative Analysis

	private static final String report_comp1_source = "report_comp1_source";
	private static final String report_comp1_tel_no="report_comp1_tel_no";
	private static final String report_comp1_unit_model="report_comp1_unit_model";
	private static final String report_comp1_mileage="report_comp1_mileage";
	private static final String report_comp1_price_min="report_comp1_price_min";
	private static final String report_comp1_price_max="report_comp1_price_max";
	private static final String report_comp1_price="report_comp1_price";
	private static final String report_comp1_less_amt="report_comp1_less_amt";
	private static final String report_comp1_less_net="report_comp1_less_net";
	private static final String report_comp1_add_amt="report_comp1_add_amt";
	private static final String report_comp1_add_net="report_comp1_add_net";
	private static final String report_comp1_time_adj_value="report_comp1_time_adj_value";
	private static final String report_comp1_time_adj_amt="report_comp1_time_adj_amt";
	private static final String report_comp1_adj_value="report_comp1_adj_value";
	private static final String report_comp1_adj_amt="report_comp1_adj_amt";
	private static final String report_comp1_index_price="report_comp1_index_price";

	private static final String report_comp2_tel_no="report_comp2_tel_no";
	private static final String report_comp2_unit_model="report_comp2_unit_model";
	private static final String report_comp2_mileage="report_comp2_mileage";
	private static final String report_comp2_price_min="report_comp2_price_min";
	private static final String report_comp2_price_max="report_comp2_price_max";
	private static final String report_comp2_price="report_comp2_price";
	private static final String report_comp2_less_amt="report_comp2_less_amt";
	private static final String report_comp2_less_net="report_comp2_less_net";
	private static final String report_comp2_add_amt="report_comp2_add_amt";
	private static final String report_comp2_add_net="report_comp2_add_net";
	private static final String report_comp2_time_adj_value="report_comp2_time_adj_value";
	private static final String report_comp2_time_adj_amt="report_comp2_time_adj_amt";
	private static final String report_comp2_adj_value="report_comp2_adj_value";
	private static final String report_comp2_adj_amt="report_comp2_adj_amt";
	private static final String report_comp2_index_price="report_comp2_index_price";


	private static final String report_comp3_tel_no="report_comp3_tel_no";
	private static final String report_comp3_unit_model="report_comp3_unit_model";
	private static final String report_comp3_mileage="report_comp3_mileage";
	private static final String report_comp3_price_min="report_comp3_price_min";
	private static final String report_comp3_price_max="report_comp3_price_max";
	private static final String report_comp3_price="report_comp3_price";
	private static final String report_comp3_less_amt="report_comp3_less_amt";
	private static final String report_comp3_less_net="report_comp3_less_net";
	private static final String report_comp3_add_amt="report_comp3_add_amt";
	private static final String report_comp3_add_net="report_comp3_add_net";
	private static final String report_comp3_time_adj_value="report_comp3_time_adj_value";
	private static final String report_comp3_time_adj_amt="report_comp3_time_adj_amt";
	private static final String report_comp3_adj_value="report_comp3_adj_value";
	private static final String report_comp3_adj_amt="report_comp3_adj_amt";
	private static final String report_comp3_index_price="report_comp3_index_price";
	private static final String report_comp_ave_index_price="report_comp_ave_index_price";
	private static final String report_rec_lux_car="report_rec_lux_car";
	private static final String report_rec_mile_factor="report_rec_mile_factor";
	private static final String report_rec_taxi_use="report_rec_taxi_use";
	private static final String report_rec_for_hire="report_rec_for_hire";
	private static final String report_rec_condition="report_rec_condition";
	private static final String report_rec_repo="report_rec_repo";
	private static final String report_rec_other_val="report_rec_other_val";
	private static final String report_rec_other_desc="report_rec_other_desc";
	private static final String report_rec_other2_val="report_rec_other2_val";
	private static final String report_rec_other2_desc="report_rec_other2_desc";
	private static final String report_rec_other3_val="report_rec_other3_val";
	private static final String report_rec_other3_desc="report_rec_other3_desc";
	private static final String report_rec_market_resistance_rec_total="report_rec_market_resistance_rec_total";
	private static final String report_rec_market_resistance_total="report_rec_market_resistance_total";
	private static final String report_rec_market_resistance_net_total="report_rec_market_resistance_net_total";
	private static final String report_rep_stereo="report_rep_stereo";
	private static final String report_rep_speakers="report_rep_speakers";
	private static final String report_rep_tires_pcs="report_rep_tires_pcs";
	private static final String report_rep_tires="report_rep_tires";
	private static final String report_rep_side_mirror_pcs="report_rep_side_mirror_pcs";
	private static final String report_rep_side_mirror="report_rep_side_mirror";
	private static final String report_rep_light="report_rep_light";
	private static final String report_rep_tools="report_rep_tools";
	private static final String report_rep_battery="report_rep_battery";
	private static final String report_rep_plates="report_rep_plates";
	private static final String report_rep_bumpers="report_rep_bumpers";
	private static final String report_rep_windows="report_rep_windows";
	private static final String report_rep_body="report_rep_body";
	private static final String report_rep_engine_wash="report_rep_engine_wash";
	private static final String report_rep_other_desc="report_rep_other_desc";
	private static final String report_rep_other="report_rep_other";
	private static final String report_rep_total="report_rep_total";
	private static final String report_appraised_value="report_appraised_value";
	private static final String report_prev_requestor = "report_prev_requestor";
	private static final String report_prev_appraised_value = "report_prev_appraised_value";
	//ADDED BY IAN JAN 13 2016
	private static final String report_verification_file_no="report_verification_file_no";
	private static final String report_verification_first_reg_date_month="report_verification_first_reg_date_month";
	private static final String report_verification_first_reg_date_day="report_verification_first_reg_date_day";
	private static final String report_verification_first_reg_date_year="report_verification_first_reg_date_year";
	private static final String report_comp1_mileage_value="report_comp1_mileage_value";
	private static final String report_comp2_mileage_value="report_comp2_mileage_value";
	private static final String report_comp3_mileage_value="report_comp3_mileage_value";
	private static final String report_comp1_mileage_amt="report_comp1_mileage_amt";
	private static final String report_comp2_mileage_amt="report_comp2_mileage_amt";
	private static final String report_comp3_mileage_amt="report_comp3_mileage_amt";
	private static final String report_comp1_adj_desc="report_comp1_adj_desc";
	private static final String report_comp2_adj_desc = "report_comp2_adj_desc";
	private static final String valrep_mv_bodytype_trunk = "valrep_mv_bodytype_trunk";
	private static final String valrep_mv_market_valuation_previous_remarks = "valrep_mv_market_valuation_previous_remarks";
	private static final String report_comp3_adj_desc = "report_comp3_adj_desc";
	private static final String report_comp1_date_month = "report_comp1_date_month";
	private static final String report_comp1_date_day = "report_comp1_date_day";
	private static final String report_comp1_date_year = "report_comp1_date_year";
	private static final String report_comp1_contact_no = "report_comp1_contact_no";
	private static final String report_comp1_location = "report_comp1_location";
	private static final String report_comp1_area = "report_comp1_area";
	private static final String report_comp1_base_price = "report_comp1_base_price";
	private static final String report_comp1_price_sqm = "report_comp1_price_sqm";
	private static final String report_comp1_discount_rate = "report_comp1_discount_rate";
	private static final String report_comp1_discounts = "report_comp1_discounts";
	private static final String report_comp1_selling_price = "report_comp1_selling_price";
	private static final String report_comp1_rec_location = "report_comp1_rec_location";
	private static final String report_comp1_rec_size = "report_comp1_rec_size";
	private static final String report_comp1_rec_accessibility = "report_comp1_rec_accessibility";
	private static final String report_comp1_rec_amenities = "report_comp1_rec_amenities";
	private static final String report_comp1_rec_terrain = "report_comp1_rec_terrain";
	private static final String report_comp1_rec_shape = "report_comp1_rec_shape";
	private static final String report_comp1_rec_corner_influence = "report_comp1_rec_corner_influence";
	private static final String report_comp1_rec_tru_lot = "report_comp1_rec_tru_lot";
	private static final String report_comp1_rec_elevation = "report_comp1_rec_elevation";
	private static final String report_comp1_rec_degree_of_devt = "report_comp1_rec_degree_of_devt";
	private static final String report_comp1_concluded_adjustment = "report_comp1_concluded_adjustment";
	private static final String report_comp1_adjusted_value = "report_comp1_adjusted_value";
	private static final String report_comp2_date_month = "report_comp2_date_month";
	private static final String report_comp2_date_day = "report_comp2_date_day";
	private static final String report_comp2_date_year = "report_comp2_date_year";
	private static final String report_comp2_source = "report_comp2_source";
	private static final String report_comp2_contact_no = "report_comp2_contact_no";
	private static final String report_comp2_location = "report_comp2_location";
	private static final String report_comp2_area = "report_comp2_area";
	private static final String report_comp2_base_price = "report_comp2_base_price";
	private static final String report_comp2_price_sqm = "report_comp2_price_sqm";
	private static final String report_comp2_discount_rate = "report_comp2_discount_rate";
	private static final String report_comp2_discounts = "report_comp2_discounts";
	private static final String report_comp2_selling_price = "report_comp2_selling_price";
	private static final String report_comp2_rec_location = "report_comp2_rec_location";
	private static final String report_comp2_rec_size = "report_comp2_rec_size";
	private static final String report_comp2_rec_accessibility = "report_comp2_rec_accessibility";
	private static final String report_comp2_rec_amenities = "report_comp2_rec_amenities";
	private static final String report_comp2_rec_terrain = "report_comp2_rec_terrain";
	private static final String report_comp2_rec_shape = "report_comp2_rec_shape";
	private static final String report_comp2_rec_corner_influence = "report_comp2_rec_corner_influence";
	private static final String report_comp2_rec_tru_lot = "report_comp2_rec_tru_lot";
	private static final String report_comp2_rec_elevation = "report_comp2_rec_elevation";
	private static final String report_comp2_rec_degree_of_devt = "report_comp2_rec_degree_of_devt";
	private static final String report_comp2_concluded_adjustment = "report_comp2_concluded_adjustment";
	private static final String report_comp2_adjusted_value = "report_comp2_adjusted_value";
	private static final String report_comp3_date_month = "report_comp3_date_month";
	private static final String report_comp3_date_day = "report_comp3_date_day";
	private static final String report_comp3_date_year = "report_comp3_date_year";
	private static final String report_comp3_source = "report_comp3_source";
	private static final String report_comp3_contact_no = "report_comp3_contact_no";
	private static final String report_comp3_location = "report_comp3_location";
	private static final String report_comp3_area = "report_comp3_area";
	private static final String report_comp3_base_price = "report_comp3_base_price";
	private static final String report_comp3_price_sqm = "report_comp3_price_sqm";
	private static final String report_comp3_discount_rate = "report_comp3_discount_rate";
	private static final String report_comp3_discounts = "report_comp3_discounts";
	private static final String report_comp3_selling_price = "report_comp3_selling_price";
	private static final String report_comp3_rec_location = "report_comp3_rec_location";
	private static final String report_comp3_rec_size = "report_comp3_rec_size";
	private static final String report_comp3_rec_accessibility = "report_comp3_rec_accessibility";
	private static final String report_comp3_rec_amenities = "report_comp3_rec_amenities";
	private static final String report_comp3_rec_terrain = "report_comp3_rec_terrain";
	private static final String report_comp3_rec_shape = "report_comp3_rec_shape";
	private static final String report_comp3_rec_corner_influence = "report_comp3_rec_corner_influence";
	private static final String report_comp3_rec_tru_lot = "report_comp3_rec_tru_lot";
	private static final String report_comp3_rec_elevation = "report_comp3_rec_elevation";
	private static final String report_comp3_rec_degree_of_devt = "report_comp3_rec_degree_of_devt";
	private static final String report_comp3_concluded_adjustment = "report_comp3_concluded_adjustment";
	private static final String report_comp3_adjusted_value = "report_comp3_adjusted_value";
	private static final String report_comp4_date_month = "report_comp4_date_month";
	private static final String report_comp4_date_day = "report_comp4_date_day";
	private static final String report_comp4_date_year = "report_comp4_date_year";
	private static final String report_comp4_source = "report_comp4_source";
	private static final String report_comp4_contact_no = "report_comp4_contact_no";
	private static final String report_comp4_location = "report_comp4_location";
	private static final String report_comp4_area = "report_comp4_area";
	private static final String report_comp4_base_price = "report_comp4_base_price";
	private static final String report_comp4_price_sqm = "report_comp4_price_sqm";
	private static final String report_comp4_discount_rate = "report_comp4_discount_rate";
	private static final String report_comp4_discounts = "report_comp4_discounts";
	private static final String report_comp4_selling_price = "report_comp4_selling_price";
	private static final String report_comp4_rec_location = "report_comp4_rec_location";
	private static final String report_comp4_rec_size = "report_comp4_rec_size";
	private static final String report_comp4_rec_accessibility = "report_comp4_rec_accessibility";
	private static final String report_comp4_rec_amenities = "report_comp4_rec_amenities";
	private static final String report_comp4_rec_terrain = "report_comp4_rec_terrain";
	private static final String report_comp4_rec_shape = "report_comp4_rec_shape";
	private static final String report_comp4_rec_corner_influence = "report_comp4_rec_corner_influence";
	private static final String report_comp4_rec_tru_lot = "report_comp4_rec_tru_lot";
	private static final String report_comp4_rec_elevation = "report_comp4_rec_elevation";
	private static final String report_comp4_rec_degree_of_devt = "report_comp4_rec_degree_of_devt";
	private static final String report_comp4_concluded_adjustment = "report_comp4_concluded_adjustment";
	private static final String report_comp4_adjusted_value = "report_comp4_adjusted_value";
	private static final String report_comp5_date_month = "report_comp5_date_month";
	private static final String report_comp5_date_day = "report_comp5_date_day";
	private static final String report_comp5_date_year = "report_comp5_date_year";
	private static final String report_comp5_source = "report_comp5_source";
	private static final String report_comp5_contact_no = "report_comp5_contact_no";
	private static final String report_comp5_location = "report_comp5_location";
	private static final String report_comp5_area = "report_comp5_area";
	private static final String report_comp5_base_price = "report_comp5_base_price";
	private static final String report_comp5_price_sqm = "report_comp5_price_sqm";
	private static final String report_comp5_discount_rate = "report_comp5_discount_rate";
	private static final String report_comp5_discounts = "report_comp5_discounts";
	private static final String report_comp5_selling_price = "report_comp5_selling_price";
	private static final String report_comp5_rec_location = "report_comp5_rec_location";
	private static final String report_comp5_rec_size = "report_comp5_rec_size";
	private static final String report_comp5_rec_accessibility = "report_comp5_rec_accessibility";
	private static final String report_comp5_rec_amenities = "report_comp5_rec_amenities";
	private static final String report_comp5_rec_terrain = "report_comp5_rec_terrain";
	private static final String report_comp5_rec_shape = "report_comp5_rec_shape";
	private static final String report_comp5_rec_corner_influence = "report_comp5_rec_corner_influence";
	private static final String report_comp5_rec_tru_lot = "report_comp5_rec_tru_lot";
	private static final String report_comp5_rec_elevation = "report_comp5_rec_elevation";
	private static final String report_comp5_rec_degree_of_devt = "report_comp5_rec_degree_of_devt";
	private static final String report_comp5_concluded_adjustment = "report_comp5_concluded_adjustment";
	private static final String report_comp5_adjusted_value = "report_comp5_adjusted_value";
	private static final String report_comp_average = "report_comp_average";
	private static final String report_comp_rounded_to = "report_comp_rounded_to";
	private static final String report_zonal_location = "report_zonal_location";
	private static final String report_zonal_lot_classification = "report_zonal_lot_classification";
	private static final String report_zonal_value = "report_zonal_value";
	private static final String report_value_total_area = "report_value_total_area";
	private static final String report_value_total_deduction = "report_value_total_deduction";
	private static final String report_value_total_net_area = "report_value_total_net_area";
	private static final String report_value_total_landimp_value = "report_value_total_landimp_value";
	private static final String report_imp_value_total_imp_value = "report_imp_value_total_imp_value";
	private static final String report_final_value_total_appraised_value_land_imp = "report_final_value_total_appraised_value_land_imp";
	private static final String report_final_value_recommended_deductions_desc = "report_final_value_recommended_deductions_desc";
	private static final String report_final_value_recommended_deductions_rate = "report_final_value_recommended_deductions_rate";
	private static final String report_final_value_recommended_deductions_other = "report_final_value_recommended_deductions_other";
	private static final String report_final_value_recommended_deductions_amt = "report_final_value_recommended_deductions_amt";
	private static final String report_final_value_net_appraised_value = "report_final_value_net_appraised_value";
	private static final String report_final_value_quick_sale_value_rate = "report_final_value_quick_sale_value_rate";
	private static final String report_final_value_quick_sale_value_amt = "report_final_value_quick_sale_value_amt";
	private static final String report_factors_of_concern = "report_factors_of_concern";
	private static final String report_suggested_corrective_actions = "report_suggested_corrective_actions";
	private static final String report_remarks = "report_remarks";
	private static final String report_requirements = "report_requirements";
	private static final String report_time_inspected = "report_time_inspected";
	private static final String report_comp1_rec_time_element = "report_comp1_rec_time_element";
	private static final String report_comp2_rec_time_element = "report_comp2_rec_time_element";
	private static final String report_comp3_rec_time_element = "report_comp3_rec_time_element";
	private static final String report_comp4_rec_time_element = "report_comp4_rec_time_element";
	private static final String report_comp5_rec_time_element = "report_comp5_rec_time_element";

	private static final String report_comp1_rec_others = "report_comp1_rec_others";
	private static final String report_comp2_rec_others = "report_comp2_rec_others";
	private static final String report_comp3_rec_others = "report_comp3_rec_others";
	private static final String report_comp4_rec_others = "report_comp4_rec_others";
	private static final String report_comp5_rec_others = "report_comp5_rec_others";

	private static final String report_comp1_remarks = "report_comp1_remarks";
	private static final String report_comp2_remarks = "report_comp2_remarks";
	private static final String report_comp3_remarks = "report_comp3_remarks";
	private static final String report_comp4_remarks = "report_comp4_remarks";
	private static final String report_comp5_remarks = "report_comp5_remarks";
	
	//concerns
	private static final String report_concerns_minor_1 = "report_concerns_minor_1";
	private static final String report_concerns_minor_2 = "report_concerns_minor_2";
	private static final String report_concerns_minor_3 = "report_concerns_minor_3";
	private static final String report_concerns_minor_others = "report_concerns_minor_others";
	private static final String report_concerns_minor_others_desc = "report_concerns_minor_others_desc";
	private static final String report_concerns_major_1 = "report_concerns_major_1";
	private static final String report_concerns_major_2 = "report_concerns_major_2";
	private static final String report_concerns_major_3 = "report_concerns_major_3";
	private static final String report_concerns_major_4 = "report_concerns_major_4";
	private static final String report_concerns_major_5 = "report_concerns_major_5";
	private static final String report_concerns_major_6 = "report_concerns_major_6";
	private static final String report_concerns_major_others = "report_concerns_major_others";
	private static final String report_concerns_major_others_desc = "report_concerns_major_others_desc";
	
	//condo fields (undeclared variables means it is already declared in other appraisal type
	private static final String report_id_admin = "report_id_admin";
	private static final String report_id_unit_numbering = "report_id_unit_numbering";
	private static final String report_id_bldg_plan = "report_id_bldg_plan";
	private static final String report_unitclass_per_tax_dec = "report_unitclass_per_tax_dec";
	private static final String report_unitclass_actual_usage = "report_unitclass_actual_usage";
	private static final String report_unitclass_neighborhood = "report_unitclass_neighborhood";
	private static final String report_unitclass_highest_best_use = "report_unitclass_highest_best_use";
	private static final String report_faci_function_room = "report_faci_function_room";
	private static final String report_faci_gym = "report_faci_gym";
	private static final String report_faci_sauna = "report_faci_sauna";
	private static final String report_faci_fire_alarm = "report_faci_fire_alarm";
	private static final String report_faci_cctv = "report_faci_cctv";
	private static final String report_faci_elevator = "report_faci_elevator";
	private static final String report_landmarks_1 = "report_landmarks_1";
	private static final String report_landmarks_2 = "report_landmarks_2";
	private static final String report_landmarks_3 = "report_landmarks_3";
	private static final String report_landmarks_4 = "report_landmarks_4";
	private static final String report_landmarks_5 = "report_landmarks_5";
	private static final String report_final_value_total_appraised_value = "report_final_value_total_appraised_value";
	private static final String report_comp1_rec_unit_floor_location = "report_comp1_rec_unit_floor_location";
	private static final String report_comp2_rec_unit_floor_location = "report_comp2_rec_unit_floor_location";
	private static final String report_comp3_rec_unit_floor_location = "report_comp3_rec_unit_floor_location";
	private static final String report_comp4_rec_unit_floor_location = "report_comp4_rec_unit_floor_location";
	private static final String report_comp5_rec_unit_floor_location = "report_comp5_rec_unit_floor_location";
	private static final String report_comp1_rec_orientation = "report_comp1_rec_orientation";
	private static final String report_comp2_rec_orientation = "report_comp2_rec_orientation";
	private static final String report_comp3_rec_orientation = "report_comp3_rec_orientation";
	private static final String report_comp4_rec_orientation = "report_comp4_rec_orientation";
	private static final String report_comp5_rec_orientation = "report_comp5_rec_orientation";
	private static final String report_comp1_rec_unit_condition = "report_comp1_rec_unit_condition";
	private static final String report_comp2_rec_unit_condition = "report_comp2_rec_unit_condition";
	private static final String report_comp3_rec_unit_condition = "report_comp3_rec_unit_condition";
	private static final String report_comp4_rec_unit_condition = "report_comp4_rec_unit_condition";
	private static final String report_comp5_rec_unit_condition = "report_comp5_rec_unit_condition";

	private static final String report_comp1_rec_unit_features = "report_comp1_rec_unit_features";
	private static final String report_comp2_rec_unit_features = "report_comp2_rec_unit_features";
	private static final String report_comp3_rec_unit_features = "report_comp3_rec_unit_features";
	private static final String report_comp4_rec_unit_features = "report_comp4_rec_unit_features";
	private static final String report_comp5_rec_unit_features = "report_comp5_rec_unit_features";
	
	//townhouse
	private static final String report_comp1_rec_lot_size = "report_comp1_rec_lot_size";
	private static final String report_comp2_rec_lot_size = "report_comp2_rec_lot_size";
	private static final String report_comp3_rec_lot_size = "report_comp3_rec_lot_size";
	private static final String report_comp4_rec_lot_size = "report_comp4_rec_lot_size";
	private static final String report_comp5_rec_lot_size = "report_comp5_rec_lot_size";
	private static final String report_comp1_rec_floor_area = "report_comp1_rec_floor_area";
	private static final String report_comp2_rec_floor_area = "report_comp2_rec_floor_area";
	private static final String report_comp3_rec_floor_area = "report_comp3_rec_floor_area";
	private static final String report_comp4_rec_floor_area = "report_comp4_rec_floor_area";
	private static final String report_comp5_rec_floor_area = "report_comp5_rec_floor_area";
	private static final String report_comp1_lot_area = "report_comp1_lot_area";
	private static final String report_comp2_lot_area = "report_comp2_lot_area";
	private static final String report_comp3_lot_area = "report_comp3_lot_area";
	private static final String report_comp4_lot_area = "report_comp4_lot_area";
	private static final String report_comp5_lot_area = "report_comp5_lot_area";
	
	//construction ebm fields
	private static final String report_project_type = "report_project_type";
	private static final String report_floor_area = "report_floor_area";
	private static final String report_storeys = "report_storeys";
	private static final String report_expected_economic_life = "report_expected_economic_life";
	private static final String report_type_of_housing_unit = "report_type_of_housing_unit";
	private static final String report_const_type_reinforced_concrete = "report_const_type_reinforced_concrete";
	private static final String report_const_type_semi_concrete = "report_const_type_semi_concrete";
	private static final String report_const_type_mixed_materials = "report_const_type_mixed_materials";
	private static final String report_foundation_concrete = "report_foundation_concrete";
	private static final String report_foundation_other = "report_foundation_other";
	private static final String report_foundation_other_value = "report_foundation_other_value";
	private static final String report_post_concrete = "report_post_concrete";
	private static final String report_post_concrete_timber = "report_post_concrete_timber";
	private static final String report_post_steel = "report_post_steel";
	private static final String report_post_other = "report_post_other";
	private static final String report_post_other_value = "report_post_other_value";
	private static final String report_beams_concrete = "report_beams_concrete";
	private static final String report_beams_timber = "report_beams_timber";
	private static final String report_beams_steel = "report_beams_steel";
	private static final String report_beams_other = "report_beams_other";
	private static final String report_beams_other_value = "report_beams_other_value";
	private static final String report_floors_concrete = "report_floors_concrete";
	private static final String report_floors_tiles = "report_floors_tiles";
	private static final String report_floors_tiles_cement = "report_floors_tiles_cement";
	private static final String report_floors_laminated_wood = "report_floors_laminated_wood";
	private static final String report_floors_ceramic_tiles = "report_floors_ceramic_tiles";
	private static final String report_floors_wood_planks = "report_floors_wood_planks";
	private static final String report_floors_marble_washout = "report_floors_marble_washout";
	private static final String report_floors_concrete_boards = "report_floors_concrete_boards";
	private static final String report_floors_granite_tiles = "report_floors_granite_tiles";
	private static final String report_floors_marble_wood = "report_floors_marble_wood";
	private static final String report_floors_carpet = "report_floors_carpet";
	private static final String report_floors_other = "report_floors_other";
	private static final String report_floors_other_value = "report_floors_other_value";
	private static final String report_walls_chb = "report_walls_chb";
	private static final String report_walls_chb_cement = "report_walls_chb_cement";
	private static final String report_walls_anay = "report_walls_anay";
	private static final String report_walls_chb_wood = "report_walls_chb_wood";
	private static final String report_walls_precast = "report_walls_precast";
	private static final String report_walls_decorative_stone = "report_walls_decorative_stone";
	private static final String report_walls_adobe = "report_walls_adobe";
	private static final String report_walls_ceramic_tiles = "report_walls_ceramic_tiles";
	private static final String report_walls_cast_in_place = "report_walls_cast_in_place";
	private static final String report_walls_sandblast = "report_walls_sandblast";
	private static final String report_walls_mactan_stone = "report_walls_mactan_stone";
	private static final String report_walls_painted = "report_walls_painted";
	private static final String report_walls_other = "report_walls_other";
	private static final String report_walls_other_value = "report_walls_other_value";
	private static final String report_partitions_chb = "report_partitions_chb";
	private static final String report_partitions_painted_cement = "report_partitions_painted_cement";
	private static final String report_partitions_anay = "report_partitions_anay";
	private static final String report_partitions_wood_boards = "report_partitions_wood_boards";
	private static final String report_partitions_precast = "report_partitions_precast";
	private static final String report_partitions_decorative_stone = "report_partitions_decorative_stone";
	private static final String report_partitions_adobe = "report_partitions_adobe";
	private static final String report_partitions_granite = "report_partitions_granite";
	private static final String report_partitions_cast_in_place = "report_partitions_cast_in_place";
	private static final String report_partitions_sandblast = "report_partitions_sandblast";
	private static final String report_partitions_mactan_stone = "report_partitions_mactan_stone";
	private static final String report_partitions_ceramic_tiles = "report_partitions_ceramic_tiles";
	private static final String report_partitions_chb_plywood = "report_partitions_chb_plywood";
	private static final String report_partitions_hardiflex = "report_partitions_hardiflex";
	private static final String report_partitions_wallpaper = "report_partitions_wallpaper";
	private static final String report_partitions_painted = "report_partitions_painted";
	private static final String report_partitions_other = "report_partitions_other";
	private static final String report_partitions_other_value = "report_partitions_other_value";
	private static final String report_windows_steel_casement = "report_windows_steel_casement";
	private static final String report_windows_fixed_view = "report_windows_fixed_view";
	private static final String report_windows_analok_sliding = "report_windows_analok_sliding";
	private static final String report_windows_alum_glass = "report_windows_alum_glass";
	private static final String report_windows_aluminum_sliding = "report_windows_aluminum_sliding";
	private static final String report_windows_awning_type = "report_windows_awning_type";
	private static final String report_windows_powder_coated = "report_windows_powder_coated";
	private static final String report_windows_wooden_frame = "report_windows_wooden_frame";
	private static final String report_windows_other = "report_windows_other";
	private static final String report_windows_other_value = "report_windows_other_value";
	private static final String report_doors_wood_panel = "report_doors_wood_panel";
	private static final String report_doors_pvc = "report_doors_pvc";
	private static final String report_doors_analok_sliding = "report_doors_analok_sliding";
	private static final String report_doors_screen_door = "report_doors_screen_door";
	private static final String report_doors_flush = "report_doors_flush";
	private static final String report_doors_molded_door = "report_doors_molded_door";
	private static final String report_doors_aluminum_sliding = "report_doors_aluminum_sliding";
	private static final String report_doors_flush_french = "report_doors_flush_french";
	private static final String report_doors_other = "report_doors_other";
	private static final String report_doors_other_value = "report_doors_other_value";
	private static final String report_ceiling_plywood = "report_ceiling_plywood";
	private static final String report_ceiling_painted_gypsum = "report_ceiling_painted_gypsum";
	private static final String report_ceiling_soffit_slab = "report_ceiling_soffit_slab";
	private static final String report_ceiling_metal_deck = "report_ceiling_metal_deck";
	private static final String report_ceiling_hardiflex = "report_ceiling_hardiflex";
	private static final String report_ceiling_plywood_tg = "report_ceiling_plywood_tg";
	private static final String report_ceiling_plywood_pvc = "report_ceiling_plywood_pvc";
	private static final String report_ceiling_painted = "report_ceiling_painted";
	private static final String report_ceiling_with_cornice = "report_ceiling_with_cornice";
	private static final String report_ceiling_with_moulding = "report_ceiling_with_moulding";
	private static final String report_ceiling_drop_ceiling = "report_ceiling_drop_ceiling";
	private static final String report_ceiling_other = "report_ceiling_other";
	private static final String report_ceiling_other_value = "report_ceiling_other_value";
	private static final String report_roof_pre_painted = "report_roof_pre_painted";
	private static final String report_roof_rib_type = "report_roof_rib_type";
	private static final String report_roof_tilespan = "report_roof_tilespan";
	private static final String report_roof_tegula_asphalt = "report_roof_tegula_asphalt";
	private static final String report_roof_tegula_longspan = "report_roof_tegula_longspan";
	private static final String report_roof_tegula_gi = "report_roof_tegula_gi";
	private static final String report_roof_steel_concrete = "report_roof_steel_concrete";
	private static final String report_roof_polycarbonate = "report_roof_polycarbonate";
	private static final String report_roof_on_steel_trusses = "report_roof_on_steel_trusses";
	private static final String report_roof_on_wooden_trusses = "report_roof_on_wooden_trusses";
	private static final String report_roof_other = "report_roof_other";
	private static final String report_roof_other_value = "report_roof_other_value";
	private static final String report_valuation_total_area = "report_valuation_total_area";
	private static final String report_valuation_total_proj_cost = "report_valuation_total_proj_cost";
	private static final String report_valuation_remarks = "report_valuation_remarks";
	// ppcr
	private static final String report_is_condo = "report_is_condo";
	private static final String report_registered_owner = "report_registered_owner";
	
	// report filename
	private static final String report_filename = "filename";
	private static final String report_uid = "uid";
	private static final String report_file = "file";
	private static final String report_file_name = "file_name";
	private static final String report_appraisal_type = "appraisal_type";
	private static final String report_candidate_done = "candidate_done";

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}//*/

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		/*String CREATE_tbl_regions = "CREATE TABLE IF NOT EXISTS " + tbl_regions + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_name + " TEXT)";
		db.execSQL(CREATE_tbl_regions);*/
		Log.i("DatabaseHandler:", "Creating Tables ...");


	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
//		db.execSQL("DROP TABLE IF EXISTS " + tbl_request_form);
//		// Create tables again
//		onCreate(db);
//		if (oldVersion < newVersion){
//		    Log.v("Test", "Within onUpgrade. Old is: " + oldVersion + " New is: " + newVersion);
//		    mycontext.deleteDatabase(DB_NAME);
//		    try {
//		        copydatabase();
//		    } catch (IOException e) {
//		        // TODO Auto-generated catch block
//		        e.printStackTrace();
//		    }
//		}
		/*Log.v("Test", "Within onUpgrade. Old is: " + oldVersion + " New is: " + newVersion);
		String CREATE_tbl_submission_logs = "CREATE TABLE IF NOT EXISTS submission_logs " +
				"(id INTEGER, record_id TEXT, date_submitted TEXT, status TEXT, status_reason TEXT, PRIMARY KEY(id))";
		
		switch(oldVersion) {
		   case 1:
		       // we want both updates, so no break statement here...
		   case 2:
			   db.execSQL(CREATE_tbl_submission_logs);
		}*/
	}
	
	
	//new block
	private static final int DATABASE_VERSION = 2;
	private static final String DATABASE_NAME = "GDSAppraisalDb";
	private static final String tbl_check = "db_check";
//	private static String DB_PATH = "/data/data/com.gds.appraisalsbs.main/databases/";
//	private static String DB_NAME = "GDSAppraisalDb";
	
//   private Context mycontext;
//   public SQLiteDatabase myDataBase;


	/*public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	    this.mycontext=context;
	    boolean dbexist = checkdatabase();
	    if (dbexist) {
	        try {
				opendatabase();
				Log.e("dbexist","dbexist");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	    } else {
	        System.out.println("Database doesn't exist");
	        try {
				createdatabase();
				Log.e("createdatabase","createdatabase");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	}

	public void createdatabase() throws IOException {
	    boolean dbexist = checkdatabase();
	    if(dbexist) {
	        System.out.println(" Database exists.");
	    } else {
	        this.getReadableDatabase();
	        try {
	            copydatabase();
	        } catch(IOException e) {
	            throw new Error("Error copying database");
	        }
	    }
	}   

	private boolean checkdatabase() {

	    boolean checkdb = false;
	    try {
	        String myPath = DB_PATH + DB_NAME;
	        File dbfile = new File(myPath);

	        checkdb = dbfile.exists();
	        
	    } catch(SQLiteException e) {
	        System.out.println("Database doesn't exist");
	    }
	    return checkdb;
	}

	private void copydatabase() throws IOException {
	    //Open your local db as the input stream
	    InputStream myinput = mycontext.getAssets().open(DB_NAME);

	    // Path to the just created empty db
	    String outfilename = DB_PATH + DB_NAME;

	    //Open the empty db as the output stream
	    OutputStream myoutput = new FileOutputStream(outfilename);

	    // transfer byte to inputfile to outputfile
	    byte[] buffer = new byte[1024];
	    int length;
	    while ((length = myinput.read(buffer))>0) {
	        myoutput.write(buffer,0,length);
	    }

	    //Close the streams
	    myoutput.flush();
	    myoutput.close();
	    myinput.close();
	    // Once the DB has been copied, set the new version
        myDataBase.setVersion(DATABASE_VERSION);
	}

	public void opendatabase() throws SQLException {
	    //Open the database
	    String mypath = DB_PATH + DB_NAME;
	    myDataBase = SQLiteDatabase.openDatabase(mypath, null, SQLiteDatabase.OPEN_READWRITE);
	}

	public synchronized void close() {
	    if(myDataBase != null) {
	        myDataBase.close();
	    }
	    super.close();
	}*/
	//new block
	
	
	
	/**
	 * Update Appraisal Type
	 */
	public int updateAppraisal_Type(String record_id, String app_type, String tbl_name) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_appraisal_type, app_type);
		// updating row
		return db.update(tbl_name, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	/**
	 * All CRUD(Create, Read, Update, Delete) Operations
	 */

	// Adding new tbl_request_form
	public void addRequest_Form(Request_Form request_form) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_rf_date_requested_month,
				request_form.getrf_date_requested_month());
		values.put(KEY_rf_date_requested_day,
				request_form.getrf_date_requested_day());
		values.put(KEY_rf_date_requested_year,
				request_form.getrf_date_requested_year());
		values.put(KEY_rf_classification, request_form.getrf_classification());
		values.put(KEY_rf_account_fname, request_form.getrf_account_fname());
		values.put(KEY_rf_account_mname, request_form.getrf_account_mname());
		values.put(KEY_rf_account_lname, request_form.getrf_account_lname());
		values.put(KEY_rf_requesting_party,
				request_form.getrf_requesting_party());
		// Inserting Row
		db.insert(tbl_request_form, null, values);
		db.close(); // Closing database connection
	}

	// Adding new tbl_request_type
	public void addRequest_Type(Request_Type request_type) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_rf_id, request_type.getrf_id());
		values.put(KEY_rt_control_no, request_type.getrt_control_no());
		values.put(KEY_rt_nature_of_appraisal,
				request_type.getrt_nature_of_appraisal());
		values.put(KEY_rt_appraisal_type, request_type.getrt_appraisal_type());
		values.put(KEY_rt_pref_ins_date1_month,
				request_type.getrt_pref_ins_date1_month());
		values.put(KEY_rt_pref_ins_date1_day,
				request_type.getrt_pref_ins_date1_day());
		values.put(KEY_rt_pref_ins_date1_year,
				request_type.getrt_pref_ins_date1_year());
		values.put(KEY_rt_pref_ins_date2_month,
				request_type.getrt_pref_ins_date2_month());
		values.put(KEY_rt_pref_ins_date2_day,
				request_type.getrt_pref_ins_date2_day());
		values.put(KEY_rt_pref_ins_date2_year,
				request_type.getrt_pref_ins_date2_year());
		values.put(KEY_rt_mv_year, request_type.getrt_mv_year());
		values.put(KEY_rt_mv_make, request_type.getrt_mv_make());
		values.put(KEY_rt_mv_model, request_type.getrt_mv_model());
		values.put(KEY_rt_mv_odometer, request_type.getrt_mv_odometer());
		values.put(KEY_rt_mv_vin, request_type.getrt_mv_vin());
		values.put(KEY_rt_me_quantity, request_type.getrt_me_quantity());
		values.put(KEY_rt_me_type, request_type.getrt_me_type());
		values.put(KEY_rt_me_manufacturer, request_type.getrt_me_manufacturer());
		values.put(KEY_rt_me_model, request_type.getrt_me_model());
		values.put(KEY_rt_me_serial, request_type.getrt_me_serial());
		values.put(KEY_rt_me_age, request_type.getrt_me_age());
		values.put(KEY_rt_me_condition, request_type.getrt_me_condition());
		// Inserting Row
		db.insert(tbl_request_type, null, values);
		db.close(); // Closing database connection
	}

	// Adding new tbl_request_contact_persons
	public void addRequest_Contact_Persons(
			Request_Contact_Persons request_contact_persons) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_rt_id, request_contact_persons.getrt_id());
		values.put(KEY_rq_contact_person,
				request_contact_persons.getrq_contact_person());
		values.put(KEY_rq_contact_mobile_no_prefix,
				request_contact_persons.getrq_contact_mobile_no_prefix());
		values.put(KEY_rq_contact_mobile_no,
				request_contact_persons.getrq_contact_mobile_no());
		values.put(KEY_rq_contact_landline,
				request_contact_persons.getrq_contact_landline());
		// Inserting Row
		db.insert(tbl_request_contact_persons, null, values);
		db.close(); // Closing database connection
	}

	// Adding new tbl_request_collateral_address
	public void addRequest_Collateral_Address(
			Request_Collateral_Address request_collateral_address) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_rt_id, request_collateral_address.getrt_id());
		values.put(KEY_rq_tct_no, request_collateral_address.getrq_tct_no());
		values.put(KEY_rq_unit_no, request_collateral_address.getrq_unit_no());
		values.put(KEY_rq_building_name,
				request_collateral_address.getrq_building_name());
		values.put(KEY_rq_street_no,
				request_collateral_address.getrq_street_no());
		values.put(KEY_rq_street_name,
				request_collateral_address.getrq_street_name());
		values.put(KEY_rq_village, request_collateral_address.getrq_village());
		values.put(KEY_rq_barangay, request_collateral_address.getrq_barangay());
		values.put(KEY_rq_zip_code, request_collateral_address.getrq_zip_code());
		values.put(KEY_rq_city, request_collateral_address.getrq_city());
		values.put(KEY_rq_province, request_collateral_address.getrq_province());
		values.put(KEY_rq_region, request_collateral_address.getrq_region());
		values.put(KEY_rq_country, request_collateral_address.getrq_country());

		// Inserting Row
		db.insert(tbl_request_collateral_address, null, values);
		db.close(); // Closing database connection
	}

	// Adding new tbl_request_attachments
	public void addRequest_Attachments(Request_Attachments request_attachments) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_rt_id, request_attachments.getrt_id());
		values.put(KEY_rq_uid, request_attachments.getrq_uid());
		values.put(KEY_rq_file, request_attachments.getrq_file());
		values.put(KEY_rq_filename, request_attachments.getrq_filename());
		values.put(KEY_rq_appraisal_type,
				request_attachments.getrq_appraisal_type());
		values.put(KEY_rq_candidate_done,
				request_attachments.getrq_candidate_done());
		// Inserting Row
		db.insert(tbl_request_attachments, null, values);
		db.close(); // Closing database connection

	}

	// Adding new tbl_images
	public void addImages(Images images) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_path, images.getpath());
		values.put(KEY_filename, images.getfilename());
		// Inserting Row
		db.insert(tbl_images, null, values);
		db.close(); // Closing database connection
	}

	// Adding new Report_Accepted_Jobs
	public void addReport_Accepted_Jobs(
			Report_Accepted_Jobs report_accepted_jobs) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Report_record_id, report_accepted_jobs.getrecord_id());
		values.put(Report_dr_month, report_accepted_jobs.getdr_month());
		values.put(Report_dr_day, report_accepted_jobs.getdr_day());
		values.put(Report_dr_year, report_accepted_jobs.getdr_year());
		values.put(Report_classification,
				report_accepted_jobs.getclassification());
		values.put(Report_fname, report_accepted_jobs.getfname());
		values.put(Report_mname, report_accepted_jobs.getmname());
		values.put(Report_lname, report_accepted_jobs.getlname());
		values.put(Report_requesting_party,
				report_accepted_jobs.getrequesting_party());
		values.put(Report_requestor,
				report_accepted_jobs.getrequestor());
		values.put(Report_control_no, report_accepted_jobs.getcontrol_no());
		values.put(Report_appraisal_type,
				report_accepted_jobs.getappraisal_type());
		values.put(Report_nature_appraisal,
				report_accepted_jobs.getnature_appraisal());
		values.put(Report_ins_date1_month,
				report_accepted_jobs.getins_date1_month());
		values.put(Report_ins_date1_day,
				report_accepted_jobs.getins_date1_day());
		values.put(Report_ins_date1_year,
				report_accepted_jobs.getins_date1_year());
		values.put(Report_ins_date2_month,
				report_accepted_jobs.getins_date2_month());
		values.put(Report_ins_date2_day,
				report_accepted_jobs.getins_date2_day());
		values.put(Report_ins_date2_year,
				report_accepted_jobs.getins_date2_year());
		values.put(Report_com_month, report_accepted_jobs.getcom_month());
		values.put(Report_com_day, report_accepted_jobs.getcom_day());
		values.put(Report_com_year, report_accepted_jobs.getcom_year());
		values.put(Report_tct_no, report_accepted_jobs.gettct_no());
		values.put(Report_unit_no, report_accepted_jobs.getunit_no());
		values.put(Report_building_name,
				report_accepted_jobs.getbuilding_name());
		values.put(Report_street_no, report_accepted_jobs.getstreet_no());
		values.put(Report_street_name, report_accepted_jobs.getstreet_name());
		values.put(Report_village, report_accepted_jobs.getvillage());
		values.put(Report_district, report_accepted_jobs.getdistrict());
		values.put(Report_zip_code, report_accepted_jobs.getzip_code());
		values.put(Report_city, report_accepted_jobs.getcity());
		values.put(Report_province, report_accepted_jobs.getprovince());
		values.put(Report_region, report_accepted_jobs.getregion());
		values.put(Report_country, report_accepted_jobs.getcountry());
		values.put(Report_counter, report_accepted_jobs.getcounter());
		values.put(Report_vehicle_type, report_accepted_jobs.getvehicle_type());
		values.put(Report_car_model, report_accepted_jobs.getcar_model());
		values.put(Report_lot_no, report_accepted_jobs.getlot_no());
		values.put(Report_block_no, report_accepted_jobs.getblock_no());
		values.put(Report_car_loan_purpose, report_accepted_jobs.getcar_loan_purpose());
		values.put(Report_car_mileage, report_accepted_jobs.getcar_mileage());
		values.put(Report_car_series, report_accepted_jobs.getcar_series());
		values.put(Report_car_color, report_accepted_jobs.getcar_color());
		values.put(Report_car_plate_no, report_accepted_jobs.getcar_plate_no());
		values.put(Report_car_body_type, report_accepted_jobs.getcar_body_type());
		values.put(Report_car_displacement, report_accepted_jobs.getcar_displacement());
		values.put(Report_car_motor_no, report_accepted_jobs.getcar_motor_no());
		values.put(Report_car_chassis_no, report_accepted_jobs.getcar_chassis_no());
		values.put(Report_car_no_cylinders, report_accepted_jobs.getcar_no_cylinders());
		values.put(Report_car_cr_no, report_accepted_jobs.getcar_cr_no());
		values.put(Report_car_cr_date_month, report_accepted_jobs.getcar_cr_date_month());
		values.put(Report_car_cr_date_day, report_accepted_jobs.getcar_cr_date_day());
		values.put(Report_car_cr_date_year, report_accepted_jobs.getcar_cr_date_year());
		values.put(Report_rework_reason, report_accepted_jobs.getrework_reason());
		values.put(Report_kind_of_appraisal, report_accepted_jobs.getkind_of_appraisal());
		values.put(Report_app_request_remarks, report_accepted_jobs.getapp_request_remarks());
		values.put(Report_app_branch_code, report_accepted_jobs.getapp_branch_code());
		values.put(Report_app_main_id, report_accepted_jobs.getapp_main_id());

		values.put("app_account_is_company", "");
		values.put("app_account_company_name", "");

		// Inserting Row
		db.insert(tbl_report_accepted_jobs, null, values);
		db.close(); // Closing database connection
	}

	// Adding new tbl_request_contact_persons
	public void addReport_Accepted_Jobs_Contacts(
			Report_Accepted_Jobs_Contacts report_accepted_jobs_contacts) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Report_record_id,
				report_accepted_jobs_contacts.getrecord_id());
		values.put(Report_contact_person,
				report_accepted_jobs_contacts.getcontact_person());
		values.put(Report_contact_mobile_no_prefix,
				report_accepted_jobs_contacts.getcontact_mobile_no_prefix());
		values.put(Report_contact_mobile_no,
				report_accepted_jobs_contacts.getcontact_mobile_no());
		values.put(Report_contact_landline,
				report_accepted_jobs_contacts.getcontact_landline());
		// Inserting Row
		db.insert(tbl_report_accepted_jobs_contacts, null, values);
		db.close(); // Closing database connection
	}

	// Adding new Townhouse_condo
	public void addTownhouse_Condo(Townhouse_Condo_API townhouse_condo) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Report_record_id, townhouse_condo.getrecord_id());
		values.put(report_cct_no, townhouse_condo.getreport_cct_no());
		values.put(report_area, townhouse_condo.getreport_area());
		values.put(report_date_reg_month,
				townhouse_condo.getreport_date_reg_month());
		values.put(report_date_reg_day,
				townhouse_condo.getreport_date_reg_day());
		values.put(report_date_reg_year,
				townhouse_condo.getreport_date_reg_year());
		values.put(report_reg_owner, townhouse_condo.getreport_reg_owner());
		values.put(report_homeowners_assoc,
				townhouse_condo.getreport_homeowners_assoc());
		values.put(report_admin_office,
				townhouse_condo.getreport_admin_office());
		values.put(report_tax_mapping, townhouse_condo.getreport_tax_mapping());
		values.put(report_lra_office, townhouse_condo.getreport_lra_office());
		values.put(report_subd_map, townhouse_condo.getreport_subd_map());
		values.put(report_lot_config, townhouse_condo.getreport_lot_config());
		values.put(report_street, townhouse_condo.getreport_street());
		values.put(report_sidewalk, townhouse_condo.getreport_sidewalk());
		values.put(report_gutter, townhouse_condo.getreport_gutter());
		values.put(report_street_lights,
				townhouse_condo.getreport_street_lights());
		values.put(report_shape, townhouse_condo.getreport_shape());
		values.put(report_elevation, townhouse_condo.getreport_elevation());
		values.put(report_road, townhouse_condo.getreport_road());
		values.put(report_frontage, townhouse_condo.getreport_frontage());
		values.put(report_depth, townhouse_condo.getreport_depth());
		values.put(report_electricity, townhouse_condo.getreport_electricity());
		values.put(report_water, townhouse_condo.getreport_water());
		values.put(report_telephone, townhouse_condo.getreport_telephone());
		values.put(report_drainage, townhouse_condo.getreport_drainage());
		values.put(report_garbage, townhouse_condo.getreport_garbage());
		values.put(report_jeepneys, townhouse_condo.getreport_jeepneys());
		values.put(report_bus, townhouse_condo.getreport_bus());
		values.put(report_taxi, townhouse_condo.getreport_taxi());
		values.put(report_tricycle, townhouse_condo.getreport_tricycle());
		values.put(report_recreation_facilities,
				townhouse_condo.getreport_recreation_facilities());
		values.put(report_security, townhouse_condo.getreport_security());
		values.put(report_clubhouse, townhouse_condo.getreport_clubhouse());
		values.put(report_swimming_pool,
				townhouse_condo.getreport_swimming_pool());
		values.put(report_community, townhouse_condo.getreport_community());
		values.put(report_classification,
				townhouse_condo.getreport_classification());
		values.put(report_growth_rate, townhouse_condo.getreport_growth_rate());
		values.put(report_adverse, townhouse_condo.getreport_adverse());
		values.put(report_occupancy, townhouse_condo.getreport_occupancy());
		values.put(report_right, townhouse_condo.getreport_right());
		values.put(report_left, townhouse_condo.getreport_left());
		values.put(report_rear, townhouse_condo.getreport_rear());
		values.put(report_front, townhouse_condo.getreport_front());
		values.put(report_desc, townhouse_condo.getreport_desc());
		values.put(report_total_area, townhouse_condo.getreport_total_area());
		values.put(report_deduc, townhouse_condo.getreport_deduc());
		values.put(report_net_area, townhouse_condo.getreport_net_area());
		values.put(report_unit_value, townhouse_condo.getreport_unit_value());
		values.put(report_total_parking_area,
				townhouse_condo.getreport_total_parking_area());
		values.put(report_parking_area_value,
				townhouse_condo.getreport_parking_area_value());
		values.put(report_total_condo_value,
				townhouse_condo.getreport_total_condo_value());
		values.put(report_total_parking_value,
				townhouse_condo.getreport_total_parking_value());
		values.put(report_total_market_value,
				townhouse_condo.getreport_total_market_value());
		values.put(report_date_inspected_month,
				townhouse_condo.getreport_date_inspected_month());
		values.put(report_date_inspected_day,
				townhouse_condo.getreport_date_inspected_day());
		values.put(report_date_inspected_year,
				townhouse_condo.getreport_date_inspected_year());
		// Inserting Row
		db.insert(tbl_townhouse_condo, null, values);
		db.close(); // Closing database connection
	}
	
	//Adding new Condo
	public void addCondo(Condo_API condo) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Report_record_id, condo.getrecord_id());
		values.put(report_date_inspected_month,
				condo.getreport_date_inspected_month());
		values.put(report_date_inspected_day,
				condo.getreport_date_inspected_day());
		values.put(report_date_inspected_year,
				condo.getreport_date_inspected_year());
		values.put(report_id_admin, condo.getreport_id_admin());
		values.put(report_id_unit_numbering,
				condo.getreport_id_unit_numbering());
		values.put(report_id_bldg_plan, condo.getreport_id_bldg_plan());
		values.put(report_unitclass_per_tax_dec,
				condo.getreport_unitclass_per_tax_dec());
		values.put(report_unitclass_actual_usage,
				condo.getreport_unitclass_actual_usage());
		values.put(report_unitclass_neighborhood,
				condo.getreport_unitclass_neighborhood());
		values.put(report_unitclass_highest_best_use,
				condo.getreport_unitclass_highest_best_use());
		values.put(report_trans_jeep, condo.getreport_trans_jeep());
		values.put(report_trans_bus, condo.getreport_trans_bus());
		values.put(report_trans_taxi, condo.getreport_trans_taxi());
		values.put(report_trans_tricycle, condo.getreport_trans_tricycle());
		values.put(report_faci_function_room,
				condo.getreport_faci_function_room());
		values.put(report_faci_gym, condo.getreport_faci_gym());
		values.put(report_faci_sauna, condo.getreport_faci_sauna());
		values.put(report_faci_pool, condo.getreport_faci_pool());
		values.put(report_faci_fire_alarm, condo.getreport_faci_fire_alarm());
		values.put(report_faci_cctv, condo.getreport_faci_cctv());
		values.put(report_faci_elevator, condo.getreport_faci_elevator());
		values.put(report_util_electricity, condo.getreport_util_electricity());
		values.put(report_util_water, condo.getreport_util_water());
		values.put(report_util_telephone, condo.getreport_util_telephone());
		values.put(report_util_garbage, condo.getreport_util_garbage());
		values.put(report_landmarks_1, condo.getreport_landmarks_1());
		values.put(report_landmarks_2, condo.getreport_landmarks_2());
		values.put(report_landmarks_3, condo.getreport_landmarks_3());
		values.put(report_landmarks_4, condo.getreport_landmarks_4());
		values.put(report_landmarks_5, condo.getreport_landmarks_5());
		values.put(report_distance_1, condo.getreport_distance_1());
		values.put(report_distance_2, condo.getreport_distance_2());
		values.put(report_distance_3, condo.getreport_distance_3());
		values.put(report_distance_4, condo.getreport_distance_4());
		values.put(report_distance_5, condo.getreport_distance_5());
		values.put(report_zonal_location, condo.getreport_zonal_location());
		values.put(report_zonal_lot_classification,
				condo.getreport_zonal_lot_classification());
		values.put(report_zonal_value, condo.getreport_zonal_value());
		values.put(report_final_value_total_appraised_value,
				condo.getreport_final_value_total_appraised_value());
		values.put(report_final_value_recommended_deductions_desc,
				condo.getreport_final_value_recommended_deductions_desc());
		values.put(report_final_value_recommended_deductions_rate,
				condo.getreport_final_value_recommended_deductions_rate());
		values.put(report_final_value_recommended_deductions_amt,
				condo.getreport_final_value_recommended_deductions_amt());
		values.put(report_final_value_net_appraised_value,
				condo.getreport_final_value_net_appraised_value());
		values.put(report_final_value_quick_sale_value_rate,
				condo.getreport_final_value_quick_sale_value_rate());
		values.put(report_final_value_quick_sale_value_amt,
				condo.getreport_final_value_quick_sale_value_amt());
		values.put(report_factors_of_concern,
				condo.getreport_factors_of_concern());
		values.put(report_suggested_corrective_actions,
				condo.getreport_suggested_corrective_actions());
		values.put(report_remarks, condo.getreport_remarks());
		values.put(report_requirements, condo.getreport_requirements());
		values.put(report_time_inspected, condo.getreport_time_inspected());

		values.put(report_comp1_date_month, condo.getreport_comp1_date_month());
		values.put(report_comp1_date_day, condo.getreport_comp1_date_day());
		values.put(report_comp1_date_year, condo.getreport_comp1_date_year());
		values.put(report_comp1_source, condo.getreport_comp1_source());
		values.put(report_comp1_contact_no, condo.getreport_comp1_contact_no());
		values.put(report_comp1_location, condo.getreport_comp1_location());
		values.put(report_comp1_area, condo.getreport_comp1_area());
		values.put(report_comp1_base_price, condo.getreport_comp1_base_price());
		values.put(report_comp1_price_sqm, condo.getreport_comp1_price_sqm());
		values.put(report_comp1_discount_rate, condo.getreport_comp1_discount_rate());
		values.put(report_comp1_discounts, condo.getreport_comp1_discounts());
		values.put(report_comp1_selling_price, condo.getreport_comp1_selling_price());
		values.put(report_comp1_rec_location, condo.getreport_comp1_rec_location());
		values.put(report_comp1_rec_unit_floor_location, condo.getreport_comp1_rec_unit_floor_location());
		values.put(report_comp1_rec_orientation, condo.getreport_comp1_rec_orientation());
		values.put(report_comp1_rec_size, condo.getreport_comp1_rec_size());
		values.put(report_comp1_rec_unit_condition, condo.getreport_comp1_rec_unit_condition());
		values.put(report_comp1_rec_amenities, condo.getreport_comp1_rec_amenities());
		values.put(report_comp1_rec_unit_features, condo.getreport_comp1_rec_unit_features());
		values.put(report_comp1_rec_time_element, condo.getreport_comp1_rec_time_element());
		values.put(report_comp1_concluded_adjustment, condo.getreport_comp1_concluded_adjustment());
		values.put(report_comp1_adjusted_value, condo.getreport_comp1_adjusted_value());
		values.put(report_comp1_remarks, condo.getreport_comp1_remarks());
		
		values.put(report_comp2_date_month, condo.getreport_comp2_date_month());
		values.put(report_comp2_date_day, condo.getreport_comp2_date_day());
		values.put(report_comp2_date_year, condo.getreport_comp2_date_year());
		values.put(report_comp2_source, condo.getreport_comp2_source());
		values.put(report_comp2_contact_no, condo.getreport_comp2_contact_no());
		values.put(report_comp2_location, condo.getreport_comp2_location());
		values.put(report_comp2_area, condo.getreport_comp2_area());
		values.put(report_comp2_base_price, condo.getreport_comp2_base_price());
		values.put(report_comp2_price_sqm, condo.getreport_comp2_price_sqm());
		values.put(report_comp2_discount_rate, condo.getreport_comp2_discount_rate());
		values.put(report_comp2_discounts, condo.getreport_comp2_discounts());
		values.put(report_comp2_selling_price, condo.getreport_comp2_selling_price());
		values.put(report_comp2_rec_location, condo.getreport_comp2_rec_location());
		values.put(report_comp2_rec_unit_floor_location, condo.getreport_comp2_rec_unit_floor_location());
		values.put(report_comp2_rec_orientation, condo.getreport_comp2_rec_orientation());
		values.put(report_comp2_rec_size, condo.getreport_comp2_rec_size());
		values.put(report_comp2_rec_unit_condition, condo.getreport_comp2_rec_unit_condition());
		values.put(report_comp2_rec_amenities, condo.getreport_comp2_rec_amenities());
		values.put(report_comp2_rec_unit_features, condo.getreport_comp2_rec_unit_features());
		values.put(report_comp2_rec_time_element, condo.getreport_comp2_rec_time_element());
		values.put(report_comp2_concluded_adjustment, condo.getreport_comp2_concluded_adjustment());
		values.put(report_comp2_adjusted_value, condo.getreport_comp2_adjusted_value());
		values.put(report_comp2_remarks, condo.getreport_comp2_remarks());
		
		values.put(report_comp3_date_month, condo.getreport_comp3_date_month());
		values.put(report_comp3_date_day, condo.getreport_comp3_date_day());
		values.put(report_comp3_date_year, condo.getreport_comp3_date_year());
		values.put(report_comp3_source, condo.getreport_comp3_source());
		values.put(report_comp3_contact_no, condo.getreport_comp3_contact_no());
		values.put(report_comp3_location, condo.getreport_comp3_location());
		values.put(report_comp3_area, condo.getreport_comp3_area());
		values.put(report_comp3_base_price, condo.getreport_comp3_base_price());
		values.put(report_comp3_price_sqm, condo.getreport_comp3_price_sqm());
		values.put(report_comp3_discount_rate, condo.getreport_comp3_discount_rate());
		values.put(report_comp3_discounts, condo.getreport_comp3_discounts());
		values.put(report_comp3_selling_price, condo.getreport_comp3_selling_price());
		values.put(report_comp3_rec_location, condo.getreport_comp3_rec_location());
		values.put(report_comp3_rec_unit_floor_location, condo.getreport_comp3_rec_unit_floor_location());
		values.put(report_comp3_rec_orientation, condo.getreport_comp3_rec_orientation());
		values.put(report_comp3_rec_size, condo.getreport_comp3_rec_size());
		values.put(report_comp3_rec_unit_condition, condo.getreport_comp3_rec_unit_condition());
		values.put(report_comp3_rec_amenities, condo.getreport_comp3_rec_amenities());
		values.put(report_comp3_rec_unit_features, condo.getreport_comp3_rec_unit_features());
		values.put(report_comp3_rec_time_element, condo.getreport_comp3_rec_time_element());
		values.put(report_comp3_concluded_adjustment, condo.getreport_comp3_concluded_adjustment());
		values.put(report_comp3_adjusted_value, condo.getreport_comp3_adjusted_value());
		values.put(report_comp3_remarks, condo.getreport_comp3_remarks());
		
		values.put(report_comp4_date_month, condo.getreport_comp4_date_month());
		values.put(report_comp4_date_day, condo.getreport_comp4_date_day());
		values.put(report_comp4_date_year, condo.getreport_comp4_date_year());
		values.put(report_comp4_source, condo.getreport_comp4_source());
		values.put(report_comp4_contact_no, condo.getreport_comp4_contact_no());
		values.put(report_comp4_location, condo.getreport_comp4_location());
		values.put(report_comp4_area, condo.getreport_comp4_area());
		values.put(report_comp4_base_price, condo.getreport_comp4_base_price());
		values.put(report_comp4_price_sqm, condo.getreport_comp4_price_sqm());
		values.put(report_comp4_discount_rate, condo.getreport_comp4_discount_rate());
		values.put(report_comp4_discounts, condo.getreport_comp4_discounts());
		values.put(report_comp4_selling_price, condo.getreport_comp4_selling_price());
		values.put(report_comp4_rec_location, condo.getreport_comp4_rec_location());
		values.put(report_comp4_rec_unit_floor_location, condo.getreport_comp4_rec_unit_floor_location());
		values.put(report_comp4_rec_orientation, condo.getreport_comp4_rec_orientation());
		values.put(report_comp4_rec_size, condo.getreport_comp4_rec_size());
		values.put(report_comp4_rec_unit_condition, condo.getreport_comp4_rec_unit_condition());
		values.put(report_comp4_rec_amenities, condo.getreport_comp4_rec_amenities());
		values.put(report_comp4_rec_unit_features, condo.getreport_comp4_rec_unit_features());
		values.put(report_comp4_rec_time_element, condo.getreport_comp4_rec_time_element());
		values.put(report_comp4_concluded_adjustment, condo.getreport_comp4_concluded_adjustment());
		values.put(report_comp4_adjusted_value, condo.getreport_comp4_adjusted_value());
		values.put(report_comp4_remarks, condo.getreport_comp4_remarks());
		
		values.put(report_comp5_date_month, condo.getreport_comp5_date_month());
		values.put(report_comp5_date_day, condo.getreport_comp5_date_day());
		values.put(report_comp5_date_year, condo.getreport_comp5_date_year());
		values.put(report_comp5_source, condo.getreport_comp5_source());
		values.put(report_comp5_contact_no, condo.getreport_comp5_contact_no());
		values.put(report_comp5_location, condo.getreport_comp5_location());
		values.put(report_comp5_area, condo.getreport_comp5_area());
		values.put(report_comp5_base_price, condo.getreport_comp5_base_price());
		values.put(report_comp5_price_sqm, condo.getreport_comp5_price_sqm());
		values.put(report_comp5_discount_rate, condo.getreport_comp5_discount_rate());
		values.put(report_comp5_discounts, condo.getreport_comp5_discounts());
		values.put(report_comp5_selling_price, condo.getreport_comp5_selling_price());
		values.put(report_comp5_rec_location, condo.getreport_comp5_rec_location());
		values.put(report_comp5_rec_unit_floor_location, condo.getreport_comp5_rec_unit_floor_location());
		values.put(report_comp5_rec_orientation, condo.getreport_comp5_rec_orientation());
		values.put(report_comp5_rec_size, condo.getreport_comp5_rec_size());
		values.put(report_comp5_rec_unit_condition, condo.getreport_comp5_rec_unit_condition());
		values.put(report_comp5_rec_amenities, condo.getreport_comp5_rec_amenities());
		values.put(report_comp5_rec_unit_features, condo.getreport_comp5_rec_unit_features());
		values.put(report_comp5_rec_time_element, condo.getreport_comp5_rec_time_element());
		values.put(report_comp5_concluded_adjustment, condo.getreport_comp5_concluded_adjustment());
		values.put(report_comp5_adjusted_value, condo.getreport_comp5_adjusted_value());
		values.put(report_comp5_remarks, condo.getreport_comp5_remarks());
		
		values.put(report_comp_average, condo.getreport_comp_average());
		values.put(report_comp_rounded_to, condo.getreport_comp_rounded_to());
		//Added from IAN
		values.put(report_prev_date, condo.getreport_prev_date());
		values.put(report_prev_appraiser, condo.getreport_prev_appraiser());
		// Inserting Row
		db.insert(tbl_condo, null, values);
		db.close(); // Closing database connection
	}
	
	// Adding new townhouse lot
	public void addTownhouse(Townhouse_API li) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Report_record_id, li.getrecord_id());
		values.put(report_date_inspected_month, li.getreport_date_inspected_month());
		values.put(report_date_inspected_day, li.getreport_date_inspected_day());
		values.put(report_date_inspected_year, li.getreport_date_inspected_year());
		values.put(report_id_association, li.getreport_id_association());
		values.put(report_id_tax, li.getreport_id_tax());
		values.put(report_id_lra, li.getreport_id_lra());
		values.put(report_id_lot_config, li.getreport_id_lot_config());
		values.put(report_id_subd_map, li.getreport_id_subd_map());
		values.put(report_id_nbrhood_checking, li.getreport_id_nbrhood_checking());
		values.put(report_imp_street_name, li.getreport_imp_street_name());
		values.put(report_imp_street, li.getreport_imp_street());
		values.put(report_imp_sidewalk, li.getreport_imp_sidewalk());
		values.put(report_imp_curb, li.getreport_imp_curb());
		values.put(report_imp_drainage, li.getreport_imp_drainage());
		values.put(report_imp_street_lights, li.getreport_imp_street_lights());
		values.put(report_physical_corner_lot, li.getreport_physical_corner_lot());
		values.put(report_physical_non_corner_lot, li.getreport_physical_non_corner_lot());
		values.put(report_physical_perimeter_lot, li.getreport_physical_perimeter_lot());
		values.put(report_physical_intersected_lot, li.getreport_physical_intersected_lot());
		values.put(report_physical_interior_with_row, li.getreport_physical_interior_with_row());
		values.put(report_physical_landlocked, li.getreport_physical_landlocked());
		values.put(report_lotclass_per_tax_dec, li.getreport_lotclass_per_tax_dec());
		values.put(report_lotclass_actual_usage, li.getreport_lotclass_actual_usage());
		values.put(report_lotclass_neighborhood, li.getreport_lotclass_neighborhood());
		values.put(report_lotclass_highest_best_use, li.getreport_lotclass_highest_best_use());
		values.put(report_physical_shape, li.getreport_physical_shape());
		values.put(report_physical_frontage, li.getreport_physical_frontage());
		values.put(report_physical_depth, li.getreport_physical_depth());
		values.put(report_physical_road, li.getreport_physical_road());
		values.put(report_physical_elevation, li.getreport_physical_elevation());
		values.put(report_physical_terrain, li.getreport_physical_terrain());
		values.put(report_landmark_1, li.getreport_landmark_1());
		values.put(report_landmark_2, li.getreport_landmark_2());
		values.put(report_landmark_3, li.getreport_landmark_3());
		values.put(report_landmark_4, li.getreport_landmark_4());
		values.put(report_landmark_5, li.getreport_landmark_5());
		values.put(report_distance_1, li.getreport_distance_1());
		values.put(report_distance_2, li.getreport_distance_2());
		values.put(report_distance_3, li.getreport_distance_3());
		values.put(report_distance_4, li.getreport_distance_4());
		values.put(report_distance_5, li.getreport_distance_5());
		values.put(report_util_electricity, li.getreport_util_electricity());
		values.put(report_util_water, li.getreport_util_water());
		values.put(report_util_telephone, li.getreport_util_telephone());
		values.put(report_util_garbage, li.getreport_util_garbage());
		values.put(report_trans_jeep, li.getreport_trans_jeep());
		values.put(report_trans_bus, li.getreport_trans_bus());
		values.put(report_trans_taxi, li.getreport_trans_taxi());
		values.put(report_trans_tricycle, li.getreport_trans_tricycle());
		values.put(report_faci_recreation, li.getreport_faci_recreation());
		values.put(report_faci_security, li.getreport_faci_security());
		values.put(report_faci_clubhouse, li.getreport_faci_clubhouse());
		values.put(report_faci_pool, li.getreport_faci_pool());
		values.put(report_bound_right, li.getreport_bound_right());
		values.put(report_bound_left, li.getreport_bound_left());
		values.put(report_bound_rear, li.getreport_bound_rear());
		values.put(report_bound_front, li.getreport_bound_front());
		values.put(report_comp1_date_month, li.getreport_comp1_date_month());
		values.put(report_comp1_date_day, li.getreport_comp1_date_day());
		values.put(report_comp1_date_year, li.getreport_comp1_date_year());
		values.put(report_comp1_source, li.getreport_comp1_source());
		values.put(report_comp1_contact_no, li.getreport_comp1_contact_no());
		values.put(report_comp1_location, li.getreport_comp1_location());
		values.put(report_comp1_area, li.getreport_comp1_area());
		values.put(report_comp1_base_price, li.getreport_comp1_base_price());
		values.put(report_comp1_price_sqm, li.getreport_comp1_price_sqm());
		values.put(report_comp1_discount_rate, li.getreport_comp1_discount_rate());
		values.put(report_comp1_discounts, li.getreport_comp1_discounts());
		values.put(report_comp1_selling_price, li.getreport_comp1_selling_price());
		values.put(report_comp1_rec_location, li.getreport_comp1_rec_location());
		values.put(report_comp1_rec_lot_size, li.getreport_comp1_rec_lot_size());
		values.put(report_comp1_rec_floor_area, li.getreport_comp1_rec_floor_area());
		values.put(report_comp1_rec_unit_condition, li.getreport_comp1_rec_unit_condition());
		values.put(report_comp1_rec_unit_features, li.getreport_comp1_rec_unit_features());
		values.put(report_comp1_rec_degree_of_devt, li.getreport_comp1_rec_degree_of_devt());
		values.put(report_comp1_concluded_adjustment, li.getreport_comp1_concluded_adjustment());
		values.put(report_comp1_adjusted_value, li.getreport_comp1_adjusted_value());
		values.put(report_comp2_date_month, li.getreport_comp2_date_month());
		values.put(report_comp2_date_day, li.getreport_comp2_date_day());
		values.put(report_comp2_date_year, li.getreport_comp2_date_year());
		values.put(report_comp2_source, li.getreport_comp2_source());
		values.put(report_comp2_contact_no, li.getreport_comp2_contact_no());
		values.put(report_comp2_location, li.getreport_comp2_location());
		values.put(report_comp2_area, li.getreport_comp2_area());
		values.put(report_comp2_base_price, li.getreport_comp2_base_price());
		values.put(report_comp2_price_sqm, li.getreport_comp2_price_sqm());
		values.put(report_comp2_discount_rate, li.getreport_comp2_discount_rate());
		values.put(report_comp2_discounts, li.getreport_comp2_discounts());
		values.put(report_comp2_selling_price, li.getreport_comp2_selling_price());
		values.put(report_comp2_rec_location, li.getreport_comp2_rec_location());
		values.put(report_comp2_rec_lot_size, li.getreport_comp2_rec_lot_size());
		values.put(report_comp2_rec_floor_area, li.getreport_comp2_rec_floor_area());
		values.put(report_comp2_rec_unit_condition, li.getreport_comp2_rec_unit_condition());
		values.put(report_comp2_rec_unit_features, li.getreport_comp2_rec_unit_features());
		values.put(report_comp2_rec_degree_of_devt, li.getreport_comp2_rec_degree_of_devt());
		values.put(report_comp2_concluded_adjustment, li.getreport_comp2_concluded_adjustment());
		values.put(report_comp2_adjusted_value, li.getreport_comp2_adjusted_value());
		values.put(report_comp3_date_month, li.getreport_comp3_date_month());
		values.put(report_comp3_date_day, li.getreport_comp3_date_day());
		values.put(report_comp3_date_year, li.getreport_comp3_date_year());
		values.put(report_comp3_source, li.getreport_comp3_source());
		values.put(report_comp3_contact_no, li.getreport_comp3_contact_no());
		values.put(report_comp3_location, li.getreport_comp3_location());
		values.put(report_comp3_area, li.getreport_comp3_area());
		values.put(report_comp3_base_price, li.getreport_comp3_base_price());
		values.put(report_comp3_price_sqm, li.getreport_comp3_price_sqm());
		values.put(report_comp3_discount_rate, li.getreport_comp3_discount_rate());
		values.put(report_comp3_discounts, li.getreport_comp3_discounts());
		values.put(report_comp3_selling_price, li.getreport_comp3_selling_price());
		values.put(report_comp3_rec_location, li.getreport_comp3_rec_location());
		values.put(report_comp3_rec_lot_size, li.getreport_comp3_rec_lot_size());
		values.put(report_comp3_rec_floor_area, li.getreport_comp3_rec_floor_area());
		values.put(report_comp3_rec_unit_condition, li.getreport_comp3_rec_unit_condition());
		values.put(report_comp3_rec_unit_features, li.getreport_comp3_rec_unit_features());
		values.put(report_comp3_rec_degree_of_devt, li.getreport_comp3_rec_degree_of_devt());
		values.put(report_comp3_concluded_adjustment, li.getreport_comp3_concluded_adjustment());
		values.put(report_comp3_adjusted_value, li.getreport_comp3_adjusted_value());
		values.put(report_comp4_date_month, li.getreport_comp4_date_month());
		values.put(report_comp4_date_day, li.getreport_comp4_date_day());
		values.put(report_comp4_date_year, li.getreport_comp4_date_year());
		values.put(report_comp4_source, li.getreport_comp4_source());
		values.put(report_comp4_contact_no, li.getreport_comp4_contact_no());
		values.put(report_comp4_location, li.getreport_comp4_location());
		values.put(report_comp4_area, li.getreport_comp4_area());
		values.put(report_comp4_base_price, li.getreport_comp4_base_price());
		values.put(report_comp4_price_sqm, li.getreport_comp4_price_sqm());
		values.put(report_comp4_discount_rate, li.getreport_comp4_discount_rate());
		values.put(report_comp4_discounts, li.getreport_comp4_discounts());
		values.put(report_comp4_selling_price, li.getreport_comp4_selling_price());
		values.put(report_comp4_rec_location, li.getreport_comp4_rec_location());
		values.put(report_comp4_rec_lot_size, li.getreport_comp4_rec_lot_size());
		values.put(report_comp4_rec_floor_area, li.getreport_comp4_rec_floor_area());
		values.put(report_comp4_rec_unit_condition, li.getreport_comp4_rec_unit_condition());
		values.put(report_comp4_rec_unit_features, li.getreport_comp4_rec_unit_features());
		values.put(report_comp4_rec_degree_of_devt, li.getreport_comp4_rec_degree_of_devt());
		values.put(report_comp4_concluded_adjustment, li.getreport_comp4_concluded_adjustment());
		values.put(report_comp4_adjusted_value, li.getreport_comp4_adjusted_value());
		values.put(report_comp5_date_month, li.getreport_comp5_date_month());
		values.put(report_comp5_date_day, li.getreport_comp5_date_day());
		values.put(report_comp5_date_year, li.getreport_comp5_date_year());
		values.put(report_comp5_source, li.getreport_comp5_source());
		values.put(report_comp5_contact_no, li.getreport_comp5_contact_no());
		values.put(report_comp5_location, li.getreport_comp5_location());
		values.put(report_comp5_area, li.getreport_comp5_area());
		values.put(report_comp5_base_price, li.getreport_comp5_base_price());
		values.put(report_comp5_price_sqm, li.getreport_comp5_price_sqm());
		values.put(report_comp5_discount_rate, li.getreport_comp5_discount_rate());
		values.put(report_comp5_discounts, li.getreport_comp5_discounts());
		values.put(report_comp5_selling_price, li.getreport_comp5_selling_price());
		values.put(report_comp5_rec_location, li.getreport_comp5_rec_location());
		values.put(report_comp5_rec_lot_size, li.getreport_comp5_rec_lot_size());
		values.put(report_comp5_rec_floor_area, li.getreport_comp5_rec_floor_area());
		values.put(report_comp5_rec_unit_condition, li.getreport_comp5_rec_unit_condition());
		values.put(report_comp5_rec_unit_features, li.getreport_comp5_rec_unit_features());
		values.put(report_comp5_rec_degree_of_devt, li.getreport_comp5_rec_degree_of_devt());
		values.put(report_comp5_concluded_adjustment, li.getreport_comp5_concluded_adjustment());
		values.put(report_comp5_adjusted_value, li.getreport_comp5_adjusted_value());
		values.put(report_comp_average, li.getreport_comp_average());
		values.put(report_comp_rounded_to, li.getreport_comp_rounded_to());
		values.put(report_zonal_location, li.getreport_zonal_location());
		values.put(report_zonal_lot_classification, li.getreport_zonal_lot_classification());
		values.put(report_zonal_value, li.getreport_zonal_value());
		values.put(report_value_total_area, li.getreport_value_total_area());
		values.put(report_value_total_deduction, li.getreport_value_total_deduction());
		values.put(report_value_total_net_area, li.getreport_value_total_net_area());
		values.put(report_value_total_landimp_value, li.getreport_value_total_landimp_value());
		values.put(report_imp_value_total_imp_value, li.getreport_imp_value_total_imp_value());
		values.put(report_final_value_total_appraised_value_land_imp, li.getreport_final_value_total_appraised_value_land_imp());
		values.put(report_final_value_recommended_deductions_desc, li.getreport_final_value_recommended_deductions_desc());
		values.put(report_final_value_recommended_deductions_rate, li.getreport_final_value_recommended_deductions_rate());
		values.put(report_final_value_recommended_deductions_amt, li.getreport_final_value_recommended_deductions_amt());
		values.put(report_final_value_net_appraised_value, li.getreport_final_value_net_appraised_value());
		values.put(report_final_value_quick_sale_value_rate, li.getreport_final_value_quick_sale_value_rate());
		values.put(report_final_value_quick_sale_value_amt, li.getreport_final_value_quick_sale_value_amt());
		values.put(report_factors_of_concern, li.getreport_factors_of_concern());
		values.put(report_suggested_corrective_actions, li.getreport_suggested_corrective_actions());
		values.put(report_remarks, li.getreport_remarks());
		values.put(report_requirements, li.getreport_requirements());
		values.put(report_time_inspected, li.getreport_time_inspected());
		values.put(report_comp1_remarks, li.getreport_comp1_remarks());
		values.put(report_comp2_remarks, li.getreport_comp2_remarks());
		values.put(report_comp3_remarks, li.getreport_comp3_remarks());
		values.put(report_comp4_remarks, li.getreport_comp4_remarks());
		values.put(report_comp5_remarks, li.getreport_comp5_remarks());
		values.put(report_comp1_lot_area, li.getreport_comp1_lot_area());
		values.put(report_comp2_lot_area, li.getreport_comp2_lot_area());
		values.put(report_comp3_lot_area, li.getreport_comp3_lot_area());
		values.put(report_comp4_lot_area, li.getreport_comp4_lot_area());
		values.put(report_comp5_lot_area, li.getreport_comp5_lot_area());
		values.put(report_concerns_minor_1, li.getreport_concerns_minor_1());
		values.put(report_concerns_minor_2, li.getreport_concerns_minor_2());
		values.put(report_concerns_minor_3, li.getreport_concerns_minor_3());
		values.put(report_concerns_minor_others, li.getreport_concerns_minor_others());
		values.put(report_concerns_minor_others_desc, li.getreport_concerns_minor_others_desc());
		values.put(report_concerns_major_1, li.getreport_concerns_major_1());
		values.put(report_concerns_major_2, li.getreport_concerns_major_2());
		values.put(report_concerns_major_3, li.getreport_concerns_major_3());
		values.put(report_concerns_major_4, li.getreport_concerns_major_4());
		values.put(report_concerns_major_5, li.getreport_concerns_major_5());
		values.put(report_concerns_major_6, li.getreport_concerns_major_6());
		values.put(report_concerns_major_others, li.getreport_concerns_major_others());
		values.put(report_concerns_major_others_desc, li.getreport_concerns_major_others_desc());
		values.put(report_prev_date, li.getreport_prev_date());
		values.put(report_prev_appraiser, li.getreport_prev_appraiser());
		values.put("report_prev_total_appraised_value", li.getreport_prev_total_appraised_value());
		values.put("report_final_value_recommended_deductions_other", li.getreport_final_value_recommended_deductions_other());
		values.put("report_comp1_rec_corner_influence", li.getreport_comp1_rec_corner_influence());
		values.put("report_comp2_rec_corner_influence", li.getreport_comp2_rec_corner_influence());
		values.put("report_comp3_rec_corner_influence", li.getreport_comp3_rec_corner_influence());
		values.put("report_comp4_rec_corner_influence", li.getreport_comp4_rec_corner_influence());
		values.put("report_comp5_rec_corner_influence", li.getreport_comp5_rec_corner_influence());
		values.put(report_townhouse_td_no, li.getreport_townhouse_td_no());
		// Inserting Row
		db.insert(tbl_townhouse, null, values);
		db.close(); // Closing database connection
	}

	// Adding new motor_vehicle
	public void addMotor_Vehicle(Motor_Vehicle_API mv) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Report_record_id, mv.getrecord_id());
		values.put(report_interior_dashboard, mv.getreport_interior_dashboard());
		values.put(report_interior_sidings, mv.getreport_interior_sidings());
		values.put(report_interior_seats, mv.getreport_interior_seats());
		values.put(report_interior_windows, mv.getreport_interior_windows());
		values.put(report_interior_ceiling, mv.getreport_interior_ceiling());
		values.put(report_interior_instrument, mv.getreport_interior_instrument());
		values.put(report_bodytype_grille, mv.getreport_bodytype_grille());
		values.put(report_bodytype_headlight, mv.getreport_bodytype_headlight());
		values.put(report_bodytype_fbumper, mv.getreport_bodytype_fbumper());
		values.put(report_bodytype_hood, mv.getreport_bodytype_hood());
		values.put(report_bodytype_rf_fender, mv.getreport_bodytype_rf_fender());
		values.put(report_bodytype_rf_door, mv.getreport_bodytype_rf_door());
		values.put(report_bodytype_rr_door, mv.getreport_bodytype_rr_door());
		values.put(report_bodytype_rr_fender, mv.getreport_bodytype_rr_fender());
		values.put(report_bodytype_backdoor, mv.getreport_bodytype_backdoor());
		values.put(report_bodytype_taillight, mv.getreport_bodytype_taillight());
		values.put(report_bodytype_r_bumper, mv.getreport_bodytype_r_bumper());
		values.put(report_bodytype_lr_fender, mv.getreport_bodytype_lr_fender());
		values.put(report_bodytype_lr_door, mv.getreport_bodytype_lr_door());
		values.put(report_bodytype_lf_door, mv.getreport_bodytype_lf_door());
		values.put(report_bodytype_lf_fender, mv.getreport_bodytype_lf_fender());
		values.put(report_bodytype_top, mv.getreport_bodytype_top());
		values.put(report_bodytype_paint, mv.getreport_bodytype_paint());
		values.put(report_bodytype_flooring, mv.getreport_bodytype_flooring());
		values.put(report_misc_stereo, mv.getreport_misc_stereo());
		values.put(report_misc_tools, mv.getreport_misc_tools());
		values.put(report_misc_speakers, mv.getreport_misc_speakers());
		values.put(report_misc_airbag, mv.getreport_misc_airbag());
		values.put(report_misc_tires, mv.getreport_misc_tires());
		values.put(report_misc_mag_wheels, mv.getreport_misc_mag_wheels());
		values.put(report_misc_carpet, mv.getreport_misc_carpet());
		values.put(report_misc_others, mv.getreport_misc_others());
		values.put(report_enginearea_fuel, mv.getreport_enginearea_fuel());
		values.put(report_enginearea_transmission1, mv.getreport_enginearea_transmission1());
		values.put(report_enginearea_transmission2, mv.getreport_enginearea_transmission2());
		values.put(report_enginearea_chassis, mv.getreport_enginearea_chassis());
		values.put(report_enginearea_battery, mv.getreport_enginearea_battery());
		values.put(report_enginearea_electrical, mv.getreport_enginearea_electrical());
		values.put(report_enginearea_engine, mv.getreport_enginearea_engine());
		values.put(report_verification, mv.getreport_verification());
		values.put(report_verification_district_office, mv.getreport_verification_district_office());
		values.put(report_verification_encumbrance, mv.getreport_verification_encumbrance());
		values.put(report_verification_registered_owner, mv.getreport_verification_registered_owner());
		values.put(report_place_inspected, mv.getreport_place_inspected());
		values.put(report_market_valuation_fair_value, mv.getreport_market_valuation_fair_value());
		values.put(report_market_valuation_min, mv.getreport_market_valuation_min());
		values.put(report_market_valuation_max, mv.getreport_market_valuation_max());
		values.put(report_market_valuation_remarks, mv.getreport_market_valuation_remarks());
		values.put(report_date_inspected_month, mv.getreport_date_inspected_month());
		values.put(report_date_inspected_day, mv.getreport_date_inspected_day());
		values.put(report_date_inspected_year, mv.getreport_date_inspected_year());
		values.put(report_time_inspected, mv.getreport_time_inspected());

		values.put(report_comp1_source, mv.getreport_comp1_source());
		values.put(report_comp1_tel_no, mv.getreport_comp1_tel_no());
		values.put(report_comp1_unit_model, mv.getreport_comp1_unit_model());
		values.put(report_comp1_mileage, mv.getreport_comp1_mileage());
		values.put(report_comp1_price_min, mv.getreport_comp1_price_min());
		values.put(report_comp1_price_max, mv.getreport_comp1_price_max());
		values.put(report_comp1_price, mv.getreport_comp1_price());
		values.put(report_comp1_less_amt, mv.getreport_comp1_less_amt());
		values.put(report_comp1_less_net, mv.getreport_comp1_less_net());
		values.put(report_comp1_add_amt, mv.getreport_comp1_add_amt());
		values.put(report_comp1_add_net, mv.getreport_comp1_add_net());
		values.put(report_comp1_time_adj_value, mv.getreport_comp1_time_adj_value());
		values.put(report_comp1_time_adj_amt, mv.getreport_comp1_time_adj_amt());
		values.put(report_comp1_adj_value, mv.getreport_comp1_adj_value());
		values.put(report_comp1_adj_amt, mv.getreport_comp1_adj_amt());
		values.put(report_comp1_index_price, mv.getreport_comp1_index_price());


		values.put(report_comp2_source, mv.getreport_comp2_source());
		values.put(report_comp2_tel_no, mv.getreport_comp2_tel_no());
		values.put(report_comp2_unit_model, mv.getreport_comp2_unit_model());
		values.put(report_comp2_mileage, mv.getreport_comp2_mileage());
		values.put(report_comp2_price_min, mv.getreport_comp2_price_min());
		values.put(report_comp2_price_max, mv.getreport_comp2_price_max());
		values.put(report_comp2_price, mv.getreport_comp2_price());
		values.put(report_comp2_less_amt, mv.getreport_comp2_less_amt());
		values.put(report_comp2_less_net, mv.getreport_comp2_less_net());
		values.put(report_comp2_add_amt, mv.getreport_comp2_add_amt());
		values.put(report_comp2_add_net, mv.getreport_comp2_add_net());
		values.put(report_comp2_time_adj_value, mv.getreport_comp2_time_adj_value());
		values.put(report_comp2_time_adj_amt, mv.getreport_comp2_time_adj_amt());
		values.put(report_comp2_adj_value, mv.getreport_comp2_adj_value());
		values.put(report_comp2_adj_amt, mv.getreport_comp2_adj_amt());
		values.put(report_comp2_index_price, mv.getreport_comp2_index_price());


		values.put(report_comp3_source, mv.getreport_comp3_source());
		values.put(report_comp3_tel_no, mv.getreport_comp3_tel_no());
		values.put(report_comp3_unit_model, mv.getreport_comp3_unit_model());
		values.put(report_comp3_mileage, mv.getreport_comp3_mileage());
		values.put(report_comp3_price_min, mv.getreport_comp3_price_min());
		values.put(report_comp3_price_max, mv.getreport_comp3_price_max());
		values.put(report_comp3_price, mv.getreport_comp3_price());
		values.put(report_comp3_less_amt, mv.getreport_comp3_less_amt());
		values.put(report_comp3_less_net, mv.getreport_comp3_less_net());
		values.put(report_comp3_add_amt, mv.getreport_comp3_add_amt());
		values.put(report_comp3_add_net, mv.getreport_comp3_add_net());
		values.put(report_comp3_time_adj_value, mv.getreport_comp3_time_adj_value());
		values.put(report_comp3_time_adj_amt, mv.getreport_comp3_time_adj_amt());
		values.put(report_comp3_adj_value, mv.getreport_comp3_adj_value());
		values.put(report_comp3_adj_amt, mv.getreport_comp3_adj_amt());
		values.put(report_comp3_index_price, mv.getreport_comp3_index_price());


		values.put(report_comp_ave_index_price, mv.getreport_comp_ave_index_price());
		values.put(report_rec_lux_car, mv.getreport_rec_lux_car());
		values.put(report_rec_mile_factor, mv.getreport_rec_mile_factor());
		values.put(report_rec_taxi_use, mv.getreport_rec_taxi_use());
		values.put(report_rec_for_hire, mv.getreport_rec_for_hire());
		values.put(report_rec_condition, mv.getreport_rec_condition());
		values.put(report_rec_repo, mv.getreport_rec_repo());
		values.put(report_rec_other_val, mv.getreport_rec_other_val());
		values.put(report_rec_other_desc, mv.getreport_rec_other_desc());
		values.put(report_rec_other2_val, mv.getreport_rec_other2_val());
		values.put(report_rec_other2_desc, mv.getreport_rec_other2_desc());
		values.put(report_rec_other3_val, mv.getreport_rec_other3_val());
		values.put(report_rec_other3_desc, mv.getreport_rec_other3_desc());
		values.put(report_rec_market_resistance_rec_total, mv.getreport_rec_market_resistance_rec_total());
		values.put(report_rec_market_resistance_total, mv.getreport_rec_market_resistance_total());
		values.put(report_rec_market_resistance_net_total, mv.getreport_rec_market_resistance_net_total());
		values.put(report_rep_stereo, mv.getreport_rep_stereo());
		values.put(report_rep_speakers, mv.getreport_rep_speakers());
		values.put(report_rep_tires_pcs, mv.getreport_rep_tires_pcs());
		values.put(report_rep_tires, mv.getreport_rep_tires());
		values.put(report_rep_side_mirror_pcs, mv.getreport_rep_side_mirror_pcs());
		values.put(report_rep_side_mirror, mv.getreport_rep_side_mirror());
		values.put(report_rep_light, mv.getreport_rep_light());
		values.put(report_rep_tools, mv.getreport_rep_tools());
		values.put(report_rep_battery, mv.getreport_rep_battery());
		values.put(report_rep_plates, mv.getreport_rep_plates());
		values.put(report_rep_bumpers, mv.getreport_rep_bumpers());
		values.put(report_rep_windows, mv.getreport_rep_windows());
		values.put(report_rep_body, mv.getreport_rep_body());
		values.put(report_rep_engine_wash, mv.getreport_rep_engine_wash());
		values.put(report_rep_other_desc, mv.getreport_rep_other_desc());
		values.put(report_rep_other, mv.getreport_rep_other());
		values.put(report_rep_total, mv.getreport_rep_total());
		values.put(report_appraised_value, mv.getreport_appraised_value());
		//ADDED BY IAN JAN 13 2016
		values.put(report_verification_file_no, mv.getreport_verification_file_no());
		values.put(report_verification_first_reg_date_month, mv.getreport_verification_first_reg_date_month());
		values.put(report_verification_first_reg_date_day, mv.getreport_verification_first_reg_date_day());
		values.put(report_verification_first_reg_date_year, mv.getreport_verification_first_reg_date_year());
		values.put(report_comp1_mileage_value, mv.getreport_comp1_mileage_value());
		values.put(report_comp2_mileage_value, mv.getreport_comp2_mileage_value());
		values.put(report_comp3_mileage_value, mv.getreport_comp3_mileage_value());
		values.put(report_comp1_mileage_amt, mv.getreport_comp1_mileage_amt());
		values.put(report_comp2_mileage_amt, mv.getreport_comp2_mileage_amt());
		values.put(report_comp3_mileage_amt, mv.getreport_comp3_mileage_amt());
		values.put(report_comp1_adj_desc, mv.getreport_comp1_adj_desc());
		values.put(report_comp2_adj_desc, mv.getreport_comp2_adj_desc());
		values.put(report_comp3_adj_desc, mv.getreport_comp3_adj_desc());
		values.put(valrep_mv_bodytype_trunk, mv.getvalrep_mv_bodytype_trunk());
		values.put(valrep_mv_market_valuation_previous_remarks, mv.getvalrep_mv_market_valuation_previous_remarks());

		values.put("valrep_mv_comp1_new_unit_value", mv.getvalrep_mv_comp1_new_unit_value());
		values.put("valrep_mv_comp2_new_unit_value", mv.getvalrep_mv_comp2_new_unit_value());
		values.put("valrep_mv_comp3_new_unit_value", mv.getvalrep_mv_comp3_new_unit_value());
		values.put("valrep_mv_comp1_new_unit_amt", mv.getvalrep_mv_comp1_new_unit_amt());
		values.put("valrep_mv_comp2_new_unit_amt", mv.getvalrep_mv_comp2_new_unit_amt());
		values.put("valrep_mv_comp3_new_unit_amt", mv.getvalrep_mv_comp3_new_unit_amt());
		values.put("valrep_mv_comp_add_index_price_desc", mv.getvalrep_mv_comp_add_index_price_desc());
		values.put("valrep_mv_comp_add_index_price", mv.getvalrep_mv_comp_add_index_price());
		values.put("valrep_mv_comp_temp_ave_index_price", mv.getvalrep_mv_comp_temp_ave_index_price());
		values.put("valrep_mv_rep_other2_desc", mv.getvalrep_mv_rep_other2_desc());
		values.put("valrep_mv_rep_other2", mv.getvalrep_mv_rep_other2());
		values.put("valrep_mv_rep_other3_desc", mv.getvalrep_mv_rep_other3_desc());
		values.put("valrep_mv_rep_other3", mv.getvalrep_mv_rep_other3());

		// Inserting Row
		db.insert(tbl_motor_vehicle, null, values);
		db.close(); // Closing database connection
	}

	// Adding new ppcr
	public void addPpcr(Ppcr_API ppcr) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Report_record_id, ppcr.getrecord_id());
		values.put(report_date_inspected_month, ppcr.getreport_date_inspected_month());
		values.put(report_date_inspected_day, ppcr.getreport_date_inspected_day());
		values.put(report_date_inspected_year, ppcr.getreport_date_inspected_year());
		values.put(report_time_inspected, ppcr.getreport_time_inspected());
		values.put(report_registered_owner, ppcr.getreport_registered_owner());
		values.put(report_project_type, ppcr.getreport_project_type());
		values.put(report_is_condo, ppcr.getreport_is_condo());
		values.put(report_remarks, ppcr.getreport_remarks());
		// Inserting Row
		db.insert(tbl_ppcr, null, values);
		db.close(); // Closing database connection
	}

	// Adding new vacant lot
	public void addVacant_Lot(Vacant_Lot_API vacant_lot) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Report_record_id, vacant_lot.getrecord_id());
		values.put(report_date_inspected_month, vacant_lot.getreport_date_inspected_month());
		values.put(report_date_inspected_day, vacant_lot.getreport_date_inspected_day());
		values.put(report_date_inspected_year, vacant_lot.getreport_date_inspected_year());
		values.put(report_id_association, vacant_lot.getreport_id_association());
		values.put(report_id_tax, vacant_lot.getreport_id_tax());
		values.put(report_id_lra, vacant_lot.getreport_id_lra());
		values.put(report_id_lot_config, vacant_lot.getreport_id_lot_config());
		values.put(report_id_subd_map, vacant_lot.getreport_id_subd_map());
		values.put(report_id_nbrhood_checking, vacant_lot.getreport_id_nbrhood_checking());
		values.put(report_imp_street_name, vacant_lot.getreport_imp_street_name());
		values.put(report_imp_street, vacant_lot.getreport_imp_street());
		values.put(report_imp_sidewalk, vacant_lot.getreport_imp_sidewalk());
		values.put(report_imp_curb, vacant_lot.getreport_imp_curb());
		values.put(report_imp_drainage, vacant_lot.getreport_imp_drainage());
		values.put(report_imp_street_lights, vacant_lot.getreport_imp_street_lights());
		values.put(report_physical_corner_lot, vacant_lot.getreport_physical_corner_lot());
		values.put(report_physical_non_corner_lot, vacant_lot.getreport_physical_non_corner_lot());
		values.put(report_physical_perimeter_lot, vacant_lot.getreport_physical_perimeter_lot());
		values.put(report_physical_intersected_lot, vacant_lot.getreport_physical_intersected_lot());
		values.put(report_physical_interior_with_row, vacant_lot.getreport_physical_interior_with_row());
		values.put(report_physical_landlocked, vacant_lot.getreport_physical_landlocked());
		values.put(report_lotclass_per_tax_dec, vacant_lot.getreport_lotclass_per_tax_dec());
		values.put(report_lotclass_actual_usage, vacant_lot.getreport_lotclass_actual_usage());
		values.put(report_lotclass_neighborhood, vacant_lot.getreport_lotclass_neighborhood());
		values.put(report_lotclass_highest_best_use, vacant_lot.getreport_lotclass_highest_best_use());
		values.put(report_physical_shape, vacant_lot.getreport_physical_shape());
		values.put(report_physical_frontage, vacant_lot.getreport_physical_frontage());
		values.put(report_physical_depth, vacant_lot.getreport_physical_depth());
		values.put(report_physical_road, vacant_lot.getreport_physical_road());
		values.put(report_physical_elevation, vacant_lot.getreport_physical_elevation());
		values.put(report_physical_terrain, vacant_lot.getreport_physical_terrain());
		values.put(report_landmark_1, vacant_lot.getreport_landmark_1());
		values.put(report_landmark_2, vacant_lot.getreport_landmark_2());
		values.put(report_landmark_3, vacant_lot.getreport_landmark_3());
		values.put(report_landmark_4, vacant_lot.getreport_landmark_4());
		values.put(report_landmark_5, vacant_lot.getreport_landmark_5());
		values.put(report_distance_1, vacant_lot.getreport_distance_1());
		values.put(report_distance_2, vacant_lot.getreport_distance_2());
		values.put(report_distance_3, vacant_lot.getreport_distance_3());
		values.put(report_distance_4, vacant_lot.getreport_distance_4());
		values.put(report_distance_5, vacant_lot.getreport_distance_5());
		values.put(report_util_electricity, vacant_lot.getreport_util_electricity());
		values.put(report_util_water, vacant_lot.getreport_util_water());
		values.put(report_util_telephone, vacant_lot.getreport_util_telephone());
		values.put(report_util_garbage, vacant_lot.getreport_util_garbage());
		values.put(report_trans_jeep, vacant_lot.getreport_trans_jeep());
		values.put(report_trans_bus, vacant_lot.getreport_trans_bus());
		values.put(report_trans_taxi, vacant_lot.getreport_trans_taxi());
		values.put(report_trans_tricycle, vacant_lot.getreport_trans_tricycle());
		values.put(report_faci_recreation, vacant_lot.getreport_faci_recreation());
		values.put(report_faci_security, vacant_lot.getreport_faci_security());
		values.put(report_faci_clubhouse, vacant_lot.getreport_faci_clubhouse());
		values.put(report_faci_pool, vacant_lot.getreport_faci_pool());
		values.put(report_bound_right, vacant_lot.getreport_bound_right());
		values.put(report_bound_left, vacant_lot.getreport_bound_left());
		values.put(report_bound_rear, vacant_lot.getreport_bound_rear());
		values.put(report_bound_front, vacant_lot.getreport_bound_front());
		values.put(report_comp1_date_month, vacant_lot.getreport_comp1_date_month());
		values.put(report_comp1_date_day, vacant_lot.getreport_comp1_date_day());
		values.put(report_comp1_date_year, vacant_lot.getreport_comp1_date_year());
		values.put(report_comp1_source, vacant_lot.getreport_comp1_source());
		values.put(report_comp1_contact_no, vacant_lot.getreport_comp1_contact_no());
		values.put(report_comp1_location, vacant_lot.getreport_comp1_location());
		values.put(report_comp1_area, vacant_lot.getreport_comp1_area());
		values.put(report_comp1_base_price, vacant_lot.getreport_comp1_base_price());
		values.put(report_comp1_price_sqm, vacant_lot.getreport_comp1_price_sqm());
		values.put(report_comp1_discount_rate, vacant_lot.getreport_comp1_discount_rate());
		values.put(report_comp1_discounts, vacant_lot.getreport_comp1_discounts());
		values.put(report_comp1_selling_price, vacant_lot.getreport_comp1_selling_price());
		values.put(report_comp1_rec_location, vacant_lot.getreport_comp1_rec_location());
		values.put(report_comp1_rec_size, vacant_lot.getreport_comp1_rec_size());
		values.put(report_comp1_rec_shape, vacant_lot.getreport_comp1_rec_shape());
		values.put(report_comp1_rec_corner_influence, vacant_lot.getreport_comp1_rec_corner_influence());
		values.put(report_comp1_rec_tru_lot, vacant_lot.getreport_comp1_rec_tru_lot());
		values.put(report_comp1_rec_elevation, vacant_lot.getreport_comp1_rec_elevation());
		values.put(report_comp1_rec_degree_of_devt, vacant_lot.getreport_comp1_rec_degree_of_devt());
		values.put(report_comp1_concluded_adjustment, vacant_lot.getreport_comp1_concluded_adjustment());
		values.put(report_comp1_adjusted_value, vacant_lot.getreport_comp1_adjusted_value());
		values.put(report_comp2_date_month, vacant_lot.getreport_comp2_date_month());
		values.put(report_comp2_date_day, vacant_lot.getreport_comp2_date_day());
		values.put(report_comp2_date_year, vacant_lot.getreport_comp2_date_year());
		values.put(report_comp2_source, vacant_lot.getreport_comp2_source());
		values.put(report_comp2_contact_no, vacant_lot.getreport_comp2_contact_no());
		values.put(report_comp2_location, vacant_lot.getreport_comp2_location());
		values.put(report_comp2_area, vacant_lot.getreport_comp2_area());
		values.put(report_comp2_base_price, vacant_lot.getreport_comp2_base_price());
		values.put(report_comp2_price_sqm, vacant_lot.getreport_comp2_price_sqm());
		values.put(report_comp2_discount_rate, vacant_lot.getreport_comp2_discount_rate());
		values.put(report_comp2_discounts, vacant_lot.getreport_comp2_discounts());
		values.put(report_comp2_selling_price, vacant_lot.getreport_comp2_selling_price());
		values.put(report_comp2_rec_location, vacant_lot.getreport_comp2_rec_location());
		values.put(report_comp2_rec_size, vacant_lot.getreport_comp2_rec_size());
		values.put(report_comp2_rec_shape, vacant_lot.getreport_comp2_rec_shape());
		values.put(report_comp2_rec_corner_influence, vacant_lot.getreport_comp2_rec_corner_influence());
		values.put(report_comp2_rec_tru_lot, vacant_lot.getreport_comp2_rec_tru_lot());
		values.put(report_comp2_rec_elevation, vacant_lot.getreport_comp2_rec_elevation());
		values.put(report_comp2_rec_degree_of_devt, vacant_lot.getreport_comp2_rec_degree_of_devt());
		values.put(report_comp2_concluded_adjustment, vacant_lot.getreport_comp2_concluded_adjustment());
		values.put(report_comp2_adjusted_value, vacant_lot.getreport_comp2_adjusted_value());
		values.put(report_comp3_date_month, vacant_lot.getreport_comp3_date_month());
		values.put(report_comp3_date_day, vacant_lot.getreport_comp3_date_day());
		values.put(report_comp3_date_year, vacant_lot.getreport_comp3_date_year());
		values.put(report_comp3_source, vacant_lot.getreport_comp3_source());
		values.put(report_comp3_contact_no, vacant_lot.getreport_comp3_contact_no());
		values.put(report_comp3_location, vacant_lot.getreport_comp3_location());
		values.put(report_comp3_area, vacant_lot.getreport_comp3_area());
		values.put(report_comp3_base_price, vacant_lot.getreport_comp3_base_price());
		values.put(report_comp3_price_sqm, vacant_lot.getreport_comp3_price_sqm());
		values.put(report_comp3_discount_rate, vacant_lot.getreport_comp3_discount_rate());
		values.put(report_comp3_discounts, vacant_lot.getreport_comp3_discounts());
		values.put(report_comp3_selling_price, vacant_lot.getreport_comp3_selling_price());
		values.put(report_comp3_rec_location, vacant_lot.getreport_comp3_rec_location());
		values.put(report_comp3_rec_size, vacant_lot.getreport_comp3_rec_size());
		values.put(report_comp3_rec_shape, vacant_lot.getreport_comp3_rec_shape());
		values.put(report_comp3_rec_corner_influence, vacant_lot.getreport_comp3_rec_corner_influence());
		values.put(report_comp3_rec_tru_lot, vacant_lot.getreport_comp3_rec_tru_lot());
		values.put(report_comp3_rec_elevation, vacant_lot.getreport_comp3_rec_elevation());
		values.put(report_comp3_rec_degree_of_devt, vacant_lot.getreport_comp3_rec_degree_of_devt());
		values.put(report_comp3_concluded_adjustment, vacant_lot.getreport_comp3_concluded_adjustment());
		values.put(report_comp3_adjusted_value, vacant_lot.getreport_comp3_adjusted_value());
		values.put(report_comp4_date_month, vacant_lot.getreport_comp4_date_month());
		values.put(report_comp4_date_day, vacant_lot.getreport_comp4_date_day());
		values.put(report_comp4_date_year, vacant_lot.getreport_comp4_date_year());
		values.put(report_comp4_source, vacant_lot.getreport_comp4_source());
		values.put(report_comp4_contact_no, vacant_lot.getreport_comp4_contact_no());
		values.put(report_comp4_location, vacant_lot.getreport_comp4_location());
		values.put(report_comp4_area, vacant_lot.getreport_comp4_area());
		values.put(report_comp4_base_price, vacant_lot.getreport_comp4_base_price());
		values.put(report_comp4_price_sqm, vacant_lot.getreport_comp4_price_sqm());
		values.put(report_comp4_discount_rate, vacant_lot.getreport_comp4_discount_rate());
		values.put(report_comp4_discounts, vacant_lot.getreport_comp4_discounts());
		values.put(report_comp4_selling_price, vacant_lot.getreport_comp4_selling_price());
		values.put(report_comp4_rec_location, vacant_lot.getreport_comp4_rec_location());
		values.put(report_comp4_rec_size, vacant_lot.getreport_comp4_rec_size());
		values.put(report_comp4_rec_shape, vacant_lot.getreport_comp4_rec_shape());
		values.put(report_comp4_rec_corner_influence, vacant_lot.getreport_comp4_rec_corner_influence());
		values.put(report_comp4_rec_tru_lot, vacant_lot.getreport_comp4_rec_tru_lot());
		values.put(report_comp4_rec_elevation, vacant_lot.getreport_comp4_rec_elevation());
		values.put(report_comp4_rec_degree_of_devt, vacant_lot.getreport_comp4_rec_degree_of_devt());
		values.put(report_comp4_concluded_adjustment, vacant_lot.getreport_comp4_concluded_adjustment());
		values.put(report_comp4_adjusted_value, vacant_lot.getreport_comp4_adjusted_value());
		values.put(report_comp5_date_month, vacant_lot.getreport_comp5_date_month());
		values.put(report_comp5_date_day, vacant_lot.getreport_comp5_date_day());
		values.put(report_comp5_date_year, vacant_lot.getreport_comp5_date_year());
		values.put(report_comp5_source, vacant_lot.getreport_comp5_source());
		values.put(report_comp5_contact_no, vacant_lot.getreport_comp5_contact_no());
		values.put(report_comp5_location, vacant_lot.getreport_comp5_location());
		values.put(report_comp5_area, vacant_lot.getreport_comp5_area());
		values.put(report_comp5_base_price, vacant_lot.getreport_comp5_base_price());
		values.put(report_comp5_price_sqm, vacant_lot.getreport_comp5_price_sqm());
		values.put(report_comp5_discount_rate, vacant_lot.getreport_comp5_discount_rate());
		values.put(report_comp5_discounts, vacant_lot.getreport_comp5_discounts());
		values.put(report_comp5_selling_price, vacant_lot.getreport_comp5_selling_price());
		values.put(report_comp5_rec_location, vacant_lot.getreport_comp5_rec_location());
		values.put(report_comp5_rec_size, vacant_lot.getreport_comp5_rec_size());
		values.put(report_comp5_rec_shape, vacant_lot.getreport_comp5_rec_shape());
		values.put(report_comp5_rec_corner_influence, vacant_lot.getreport_comp5_rec_corner_influence());
		values.put(report_comp5_rec_tru_lot, vacant_lot.getreport_comp5_rec_tru_lot());
		values.put(report_comp5_rec_elevation, vacant_lot.getreport_comp5_rec_elevation());
		values.put(report_comp5_rec_degree_of_devt, vacant_lot.getreport_comp5_rec_degree_of_devt());
		values.put(report_comp5_concluded_adjustment, vacant_lot.getreport_comp5_concluded_adjustment());
		values.put(report_comp5_adjusted_value, vacant_lot.getreport_comp5_adjusted_value());
		values.put(report_comp_average, vacant_lot.getreport_comp_average());
		values.put(report_comp_rounded_to, vacant_lot.getreport_comp_rounded_to());
		values.put(report_zonal_location, vacant_lot.getreport_zonal_location());
		values.put(report_zonal_lot_classification, vacant_lot.getreport_zonal_lot_classification());
		values.put(report_zonal_value, vacant_lot.getreport_zonal_value());
		values.put(report_value_total_area, vacant_lot.getreport_value_total_area());
		values.put(report_value_total_deduction, vacant_lot.getreport_value_total_deduction());
		values.put(report_value_total_net_area, vacant_lot.getreport_value_total_net_area());
		values.put(report_value_total_landimp_value, vacant_lot.getreport_value_total_landimp_value());
		values.put(report_imp_value_total_imp_value, vacant_lot.getreport_imp_value_total_imp_value());
		values.put(report_final_value_total_appraised_value_land_imp, vacant_lot.getreport_final_value_total_appraised_value_land_imp());
		values.put(report_final_value_recommended_deductions_desc, vacant_lot.getreport_final_value_recommended_deductions_desc());
		values.put(report_final_value_recommended_deductions_rate, vacant_lot.getreport_final_value_recommended_deductions_rate());
		values.put(report_final_value_recommended_deductions_amt, vacant_lot.getreport_final_value_recommended_deductions_amt());
		values.put(report_final_value_net_appraised_value, vacant_lot.getreport_final_value_net_appraised_value());
		values.put(report_final_value_quick_sale_value_rate, vacant_lot.getreport_final_value_quick_sale_value_rate());
		values.put(report_final_value_quick_sale_value_amt, vacant_lot.getreport_final_value_quick_sale_value_amt());
		values.put(report_factors_of_concern, vacant_lot.getreport_factors_of_concern());
		values.put(report_suggested_corrective_actions, vacant_lot.getreport_suggested_corrective_actions());
		values.put(report_remarks, vacant_lot.getreport_remarks());
		values.put(report_requirements, vacant_lot.getreport_requirements());
		values.put(report_time_inspected, vacant_lot.getreport_time_inspected());
		values.put(report_comp1_rec_time_element, vacant_lot.getreport_comp1_rec_time_element());
		values.put(report_comp2_rec_time_element, vacant_lot.getreport_comp2_rec_time_element());
		values.put(report_comp3_rec_time_element, vacant_lot.getreport_comp3_rec_time_element());
		values.put(report_comp4_rec_time_element, vacant_lot.getreport_comp4_rec_time_element());
		values.put(report_comp5_rec_time_element, vacant_lot.getreport_comp5_rec_time_element());
		values.put(report_comp1_remarks, vacant_lot.getreport_comp1_remarks());
		values.put(report_comp2_remarks, vacant_lot.getreport_comp2_remarks());
		values.put(report_comp3_remarks, vacant_lot.getreport_comp3_remarks());
		values.put(report_comp4_remarks, vacant_lot.getreport_comp4_remarks());
		values.put(report_comp5_remarks, vacant_lot.getreport_comp5_remarks());
		
		values.put(report_concerns_minor_1, vacant_lot.getreport_concerns_minor_1());
		values.put(report_concerns_minor_2, vacant_lot.getreport_concerns_minor_2());
		values.put(report_concerns_minor_3, vacant_lot.getreport_concerns_minor_3());
		values.put(report_concerns_minor_others, vacant_lot.getreport_concerns_minor_others());
		values.put(report_concerns_minor_others_desc, vacant_lot.getreport_concerns_minor_others_desc());
		values.put(report_concerns_major_1, vacant_lot.getreport_concerns_major_1());
		values.put(report_concerns_major_2, vacant_lot.getreport_concerns_major_2());
		values.put(report_concerns_major_3, vacant_lot.getreport_concerns_major_3());
		values.put(report_concerns_major_4, vacant_lot.getreport_concerns_major_4());
		values.put(report_concerns_major_5, vacant_lot.getreport_concerns_major_5());
		values.put(report_concerns_major_6, vacant_lot.getreport_concerns_major_6());
		values.put(report_concerns_major_others, vacant_lot.getreport_concerns_major_others());
		values.put(report_concerns_major_others_desc, vacant_lot.getreport_concerns_major_others_desc());
		//added by ian nov 29
		values.put("report_final_value_recommended_deductions_other", "");
		values.put("valrep_land_physical_others", "");
		values.put("valrep_land_physical_others_desc", "");
		values.put("valrep_land_id_others", "");
		values.put("valrep_land_id_others_desc", "");
		values.put("valrep_land_comp1_total_rectification", "");
		values.put("valrep_land_comp2_total_rectification", "");
		values.put("valrep_land_comp3_total_rectification", "");
		values.put("valrep_land_comp4_total_rectification", "");
		values.put("valrep_land_comp5_total_rectification", "");
		values.put("valrep_land_road_right_of_way", "");
		// Inserting Row
		db.insert(tbl_vacant_lot, null, values);
		db.close(); // Closing database connection
	}

	// Adding new construction_ebm
	public void addConstruction_Ebm(Construction_Ebm_API ce) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Report_record_id, ce.getrecord_id());
		values.put(report_date_inspected_month, ce.getreport_date_inspected_month());
		values.put(report_date_inspected_day, ce.getreport_date_inspected_day());
		values.put(report_date_inspected_year, ce.getreport_date_inspected_year());
		values.put(report_time_inspected, ce.getreport_time_inspected());
		values.put(report_project_type, ce.getreport_project_type());
		values.put(report_floor_area, ce.getreport_floor_area());
		values.put(report_storeys, ce.getreport_storeys());
		values.put(report_expected_economic_life, ce.getreport_expected_economic_life());
		values.put(report_type_of_housing_unit, ce.getreport_type_of_housing_unit());
		values.put(report_const_type_reinforced_concrete, ce.getreport_const_type_reinforced_concrete());
		values.put(report_const_type_semi_concrete, ce.getreport_const_type_semi_concrete());
		values.put(report_const_type_mixed_materials, ce.getreport_const_type_mixed_materials());
		values.put(report_foundation_concrete, ce.getreport_foundation_concrete());
		values.put(report_foundation_other, ce.getreport_foundation_other());
		values.put(report_foundation_other_value, ce.getreport_foundation_other_value());
		values.put(report_post_concrete, ce.getreport_post_concrete());
		values.put(report_post_concrete_timber, ce.getreport_post_concrete_timber());
		values.put(report_post_steel, ce.getreport_post_steel());
		values.put(report_post_other, ce.getreport_post_other());
		values.put(report_post_other_value, ce.getreport_post_other_value());
		values.put(report_beams_concrete, ce.getreport_beams_concrete());
		values.put(report_beams_timber, ce.getreport_beams_timber());
		values.put(report_beams_steel, ce.getreport_beams_steel());
		values.put(report_beams_other, ce.getreport_beams_other());
		values.put(report_beams_other_value, ce.getreport_beams_other_value());
		values.put(report_floors_concrete, ce.getreport_floors_concrete());
		values.put(report_floors_tiles, ce.getreport_floors_tiles());
		values.put(report_floors_tiles_cement, ce.getreport_floors_tiles_cement());
		values.put(report_floors_laminated_wood, ce.getreport_floors_laminated_wood());
		values.put(report_floors_ceramic_tiles, ce.getreport_floors_ceramic_tiles());
		values.put(report_floors_wood_planks, ce.getreport_floors_wood_planks());
		values.put(report_floors_marble_washout, ce.getreport_floors_marble_washout());
		values.put(report_floors_concrete_boards, ce.getreport_floors_concrete_boards());
		values.put(report_floors_granite_tiles, ce.getreport_floors_granite_tiles());
		values.put(report_floors_marble_wood, ce.getreport_floors_marble_wood());
		values.put(report_floors_carpet, ce.getreport_floors_carpet());
		values.put(report_floors_other, ce.getreport_floors_other());
		values.put(report_floors_other_value, ce.getreport_floors_other_value());
		values.put(report_walls_chb, ce.getreport_walls_chb());
		values.put(report_walls_chb_cement, ce.getreport_walls_chb_cement());
		values.put(report_walls_anay, ce.getreport_walls_anay());
		values.put(report_walls_chb_wood, ce.getreport_walls_chb_wood());
		values.put(report_walls_precast, ce.getreport_walls_precast());
		values.put(report_walls_decorative_stone, ce.getreport_walls_decorative_stone());
		values.put(report_walls_adobe, ce.getreport_walls_adobe());
		values.put(report_walls_ceramic_tiles, ce.getreport_walls_ceramic_tiles());
		values.put(report_walls_cast_in_place, ce.getreport_walls_cast_in_place());
		values.put(report_walls_sandblast, ce.getreport_walls_sandblast());
		values.put(report_walls_mactan_stone, ce.getreport_walls_mactan_stone());
		values.put(report_walls_painted, ce.getreport_walls_painted());
		values.put(report_walls_other, ce.getreport_walls_other());
		values.put(report_walls_other_value, ce.getreport_walls_other_value());
		values.put(report_partitions_chb, ce.getreport_partitions_chb());
		values.put(report_partitions_painted_cement, ce.getreport_partitions_painted_cement());
		values.put(report_partitions_anay, ce.getreport_partitions_anay());
		values.put(report_partitions_wood_boards, ce.getreport_partitions_wood_boards());
		values.put(report_partitions_precast, ce.getreport_partitions_precast());
		values.put(report_partitions_decorative_stone, ce.getreport_partitions_decorative_stone());
		values.put(report_partitions_adobe, ce.getreport_partitions_adobe());
		values.put(report_partitions_granite, ce.getreport_partitions_granite());
		values.put(report_partitions_cast_in_place, ce.getreport_partitions_cast_in_place());
		values.put(report_partitions_sandblast, ce.getreport_partitions_sandblast());
		values.put(report_partitions_mactan_stone, ce.getreport_partitions_mactan_stone());
		values.put(report_partitions_ceramic_tiles, ce.getreport_partitions_ceramic_tiles());
		values.put(report_partitions_chb_plywood, ce.getreport_partitions_chb_plywood());
		values.put(report_partitions_hardiflex, ce.getreport_partitions_hardiflex());
		values.put(report_partitions_wallpaper, ce.getreport_partitions_wallpaper());
		values.put(report_partitions_painted, ce.getreport_partitions_painted());
		values.put(report_partitions_other, ce.getreport_partitions_other());
		values.put(report_partitions_other_value, ce.getreport_partitions_other_value());
		values.put(report_windows_steel_casement, ce.getreport_windows_steel_casement());
		values.put(report_windows_fixed_view, ce.getreport_windows_fixed_view());
		values.put(report_windows_analok_sliding, ce.getreport_windows_analok_sliding());
		values.put(report_windows_alum_glass, ce.getreport_windows_alum_glass());
		values.put(report_windows_aluminum_sliding, ce.getreport_windows_aluminum_sliding());
		values.put(report_windows_awning_type, ce.getreport_windows_awning_type());
		values.put(report_windows_powder_coated, ce.getreport_windows_powder_coated());
		values.put(report_windows_wooden_frame, ce.getreport_windows_wooden_frame());
		values.put(report_windows_other, ce.getreport_windows_other());
		values.put(report_windows_other_value, ce.getreport_windows_other_value());
		values.put(report_doors_wood_panel, ce.getreport_doors_wood_panel());
		values.put(report_doors_pvc, ce.getreport_doors_pvc());
		values.put(report_doors_analok_sliding, ce.getreport_doors_analok_sliding());
		values.put(report_doors_screen_door, ce.getreport_doors_screen_door());
		values.put(report_doors_flush, ce.getreport_doors_flush());
		values.put(report_doors_molded_door, ce.getreport_doors_molded_door());
		values.put(report_doors_aluminum_sliding, ce.getreport_doors_aluminum_sliding());
		values.put(report_doors_flush_french, ce.getreport_doors_flush_french());
		values.put(report_doors_other, ce.getreport_doors_other());
		values.put(report_doors_other_value, ce.getreport_doors_other_value());
		values.put(report_ceiling_plywood, ce.getreport_ceiling_plywood());
		values.put(report_ceiling_painted_gypsum, ce.getreport_ceiling_painted_gypsum());
		values.put(report_ceiling_soffit_slab, ce.getreport_ceiling_soffit_slab());
		values.put(report_ceiling_metal_deck, ce.getreport_ceiling_metal_deck());
		values.put(report_ceiling_hardiflex, ce.getreport_ceiling_hardiflex());
		values.put(report_ceiling_plywood_tg, ce.getreport_ceiling_plywood_tg());
		values.put(report_ceiling_plywood_pvc, ce.getreport_ceiling_plywood_pvc());
		values.put(report_ceiling_painted, ce.getreport_ceiling_painted());
		values.put(report_ceiling_with_cornice, ce.getreport_ceiling_with_cornice());
		values.put(report_ceiling_with_moulding, ce.getreport_ceiling_with_moulding());
		values.put(report_ceiling_drop_ceiling, ce.getreport_ceiling_drop_ceiling());
		values.put(report_ceiling_other, ce.getreport_ceiling_other());
		values.put(report_ceiling_other_value, ce.getreport_ceiling_other_value());
		values.put(report_roof_pre_painted, ce.getreport_roof_pre_painted());
		values.put(report_roof_rib_type, ce.getreport_roof_rib_type());
		values.put(report_roof_tilespan, ce.getreport_roof_tilespan());
		values.put(report_roof_tegula_asphalt, ce.getreport_roof_tegula_asphalt());
		values.put(report_roof_tegula_longspan, ce.getreport_roof_tegula_longspan());
		values.put(report_roof_tegula_gi, ce.getreport_roof_tegula_gi());
		values.put(report_roof_steel_concrete, ce.getreport_roof_steel_concrete());
		values.put(report_roof_polycarbonate, ce.getreport_roof_polycarbonate());
		values.put(report_roof_on_steel_trusses, ce.getreport_roof_on_steel_trusses());
		values.put(report_roof_on_wooden_trusses, ce.getreport_roof_on_wooden_trusses());
		values.put(report_roof_other, ce.getreport_roof_other());
		values.put(report_roof_other_value, ce.getreport_roof_other_value());
		values.put(report_valuation_total_area, ce.getreport_valuation_total_area());
		values.put(report_valuation_total_proj_cost, ce.getreport_valuation_total_proj_cost());
		values.put(report_valuation_remarks, ce.getreport_valuation_remarks());
		// Inserting Row
		db.insert(tbl_construction_ebm, null, values);
		db.close(); // Closing database connection
	}

	// Adding new Land_Improvements lot
	public void addLand_Improvements(Land_Improvements_API li) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Report_record_id, li.getrecord_id());
		values.put(report_date_inspected_month, li.getreport_date_inspected_month());
		values.put(report_date_inspected_day, li.getreport_date_inspected_day());
		values.put(report_date_inspected_year, li.getreport_date_inspected_year());
		values.put(report_id_association, li.getreport_id_association());
		values.put(report_id_tax, li.getreport_id_tax());
		values.put(report_id_lra, li.getreport_id_lra());
		values.put(report_id_lot_config, li.getreport_id_lot_config());
		values.put(report_id_subd_map, li.getreport_id_subd_map());
		values.put(report_id_nbrhood_checking, li.getreport_id_nbrhood_checking());
		values.put(report_imp_street_name, li.getreport_imp_street_name());
		values.put(report_imp_street, li.getreport_imp_street());
		values.put(report_imp_sidewalk, li.getreport_imp_sidewalk());
		values.put(report_imp_curb, li.getreport_imp_curb());
		values.put(report_imp_drainage, li.getreport_imp_drainage());
		values.put(report_imp_street_lights, li.getreport_imp_street_lights());
		values.put(report_physical_corner_lot, li.getreport_physical_corner_lot());
		values.put(report_physical_non_corner_lot, li.getreport_physical_non_corner_lot());
		values.put(report_physical_perimeter_lot, li.getreport_physical_perimeter_lot());
		values.put(report_physical_intersected_lot, li.getreport_physical_intersected_lot());
		values.put(report_physical_interior_with_row, li.getreport_physical_interior_with_row());
		values.put(report_physical_landlocked, li.getreport_physical_landlocked());
		values.put(report_lotclass_per_tax_dec, li.getreport_lotclass_per_tax_dec());
		values.put(report_lotclass_actual_usage, li.getreport_lotclass_actual_usage());
		values.put(report_lotclass_neighborhood, li.getreport_lotclass_neighborhood());
		values.put(report_lotclass_highest_best_use, li.getreport_lotclass_highest_best_use());
		values.put(report_physical_shape, li.getreport_physical_shape());
		values.put(report_physical_frontage, li.getreport_physical_frontage());
		values.put(report_physical_depth, li.getreport_physical_depth());
		values.put(report_physical_road, li.getreport_physical_road());
		values.put(report_physical_elevation, li.getreport_physical_elevation());
		values.put(report_physical_terrain, li.getreport_physical_terrain());
		values.put(report_landmark_1, li.getreport_landmark_1());
		values.put(report_landmark_2, li.getreport_landmark_2());
		values.put(report_landmark_3, li.getreport_landmark_3());
		values.put(report_landmark_4, li.getreport_landmark_4());
		values.put(report_landmark_5, li.getreport_landmark_5());
		values.put(report_distance_1, li.getreport_distance_1());
		values.put(report_distance_2, li.getreport_distance_2());
		values.put(report_distance_3, li.getreport_distance_3());
		values.put(report_distance_4, li.getreport_distance_4());
		values.put(report_distance_5, li.getreport_distance_5());
		values.put(report_util_electricity, li.getreport_util_electricity());
		values.put(report_util_water, li.getreport_util_water());
		values.put(report_util_telephone, li.getreport_util_telephone());
		values.put(report_util_garbage, li.getreport_util_garbage());
		values.put(report_trans_jeep, li.getreport_trans_jeep());
		values.put(report_trans_bus, li.getreport_trans_bus());
		values.put(report_trans_taxi, li.getreport_trans_taxi());
		values.put(report_trans_tricycle, li.getreport_trans_tricycle());
		values.put(report_faci_recreation, li.getreport_faci_recreation());
		values.put(report_faci_security, li.getreport_faci_security());
		values.put(report_faci_clubhouse, li.getreport_faci_clubhouse());
		values.put(report_faci_pool, li.getreport_faci_pool());
		values.put(report_bound_right, li.getreport_bound_right());
		values.put(report_bound_left, li.getreport_bound_left());
		values.put(report_bound_rear, li.getreport_bound_rear());
		values.put(report_bound_front, li.getreport_bound_front());
		values.put(report_comp1_date_month, li.getreport_comp1_date_month());
		values.put(report_comp1_date_day, li.getreport_comp1_date_day());
		values.put(report_comp1_date_year, li.getreport_comp1_date_year());
		values.put(report_comp1_source, li.getreport_comp1_source());
		values.put(report_comp1_contact_no, li.getreport_comp1_contact_no());
		values.put(report_comp1_location, li.getreport_comp1_location());
		values.put(report_comp1_area, li.getreport_comp1_area());
		values.put(report_comp1_base_price, li.getreport_comp1_base_price());
		values.put(report_comp1_price_sqm, li.getreport_comp1_price_sqm());
		values.put(report_comp1_discount_rate, li.getreport_comp1_discount_rate());
		values.put(report_comp1_discounts, li.getreport_comp1_discounts());
		values.put(report_comp1_selling_price, li.getreport_comp1_selling_price());
		values.put(report_comp1_rec_location, li.getreport_comp1_rec_location());
		values.put(report_comp1_rec_size, li.getreport_comp1_rec_size());
		values.put(report_comp1_rec_shape, li.getreport_comp1_rec_shape());
		values.put(report_comp1_rec_corner_influence, li.getreport_comp1_rec_corner_influence());
		values.put(report_comp1_rec_tru_lot, li.getreport_comp1_rec_tru_lot());
		values.put(report_comp1_rec_elevation, li.getreport_comp1_rec_elevation());
		values.put(report_comp1_rec_degree_of_devt, li.getreport_comp1_rec_degree_of_devt());
		values.put(report_comp1_concluded_adjustment, li.getreport_comp1_concluded_adjustment());
		values.put(report_comp1_adjusted_value, li.getreport_comp1_adjusted_value());
		values.put(report_comp2_date_month, li.getreport_comp2_date_month());
		values.put(report_comp2_date_day, li.getreport_comp2_date_day());
		values.put(report_comp2_date_year, li.getreport_comp2_date_year());
		values.put(report_comp2_source, li.getreport_comp2_source());
		values.put(report_comp2_contact_no, li.getreport_comp2_contact_no());
		values.put(report_comp2_location, li.getreport_comp2_location());
		values.put(report_comp2_area, li.getreport_comp2_area());
		values.put(report_comp2_base_price, li.getreport_comp2_base_price());
		values.put(report_comp2_price_sqm, li.getreport_comp2_price_sqm());
		values.put(report_comp2_discount_rate, li.getreport_comp2_discount_rate());
		values.put(report_comp2_discounts, li.getreport_comp2_discounts());
		values.put(report_comp2_selling_price, li.getreport_comp2_selling_price());
		values.put(report_comp2_rec_location, li.getreport_comp2_rec_location());
		values.put(report_comp2_rec_size, li.getreport_comp2_rec_size());
		values.put(report_comp2_rec_shape, li.getreport_comp2_rec_shape());
		values.put(report_comp2_rec_corner_influence, li.getreport_comp2_rec_corner_influence());
		values.put(report_comp2_rec_tru_lot, li.getreport_comp2_rec_tru_lot());
		values.put(report_comp2_rec_elevation, li.getreport_comp2_rec_elevation());
		values.put(report_comp2_rec_degree_of_devt, li.getreport_comp2_rec_degree_of_devt());
		values.put(report_comp2_concluded_adjustment, li.getreport_comp2_concluded_adjustment());
		values.put(report_comp2_adjusted_value, li.getreport_comp2_adjusted_value());
		values.put(report_comp3_date_month, li.getreport_comp3_date_month());
		values.put(report_comp3_date_day, li.getreport_comp3_date_day());
		values.put(report_comp3_date_year, li.getreport_comp3_date_year());
		values.put(report_comp3_source, li.getreport_comp3_source());
		values.put(report_comp3_contact_no, li.getreport_comp3_contact_no());
		values.put(report_comp3_location, li.getreport_comp3_location());
		values.put(report_comp3_area, li.getreport_comp3_area());
		values.put(report_comp3_base_price, li.getreport_comp3_base_price());
		values.put(report_comp3_price_sqm, li.getreport_comp3_price_sqm());
		values.put(report_comp3_discount_rate, li.getreport_comp3_discount_rate());
		values.put(report_comp3_discounts, li.getreport_comp3_discounts());
		values.put(report_comp3_selling_price, li.getreport_comp3_selling_price());
		values.put(report_comp3_rec_location, li.getreport_comp3_rec_location());
		values.put(report_comp3_rec_size, li.getreport_comp3_rec_size());
		values.put(report_comp3_rec_shape, li.getreport_comp3_rec_shape());
		values.put(report_comp3_rec_corner_influence, li.getreport_comp3_rec_corner_influence());
		values.put(report_comp3_rec_tru_lot, li.getreport_comp3_rec_tru_lot());
		values.put(report_comp3_rec_elevation, li.getreport_comp3_rec_elevation());
		values.put(report_comp3_rec_degree_of_devt, li.getreport_comp3_rec_degree_of_devt());
		values.put(report_comp3_concluded_adjustment, li.getreport_comp3_concluded_adjustment());
		values.put(report_comp3_adjusted_value, li.getreport_comp3_adjusted_value());
		values.put(report_comp4_date_month, li.getreport_comp4_date_month());
		values.put(report_comp4_date_day, li.getreport_comp4_date_day());
		values.put(report_comp4_date_year, li.getreport_comp4_date_year());
		values.put(report_comp4_source, li.getreport_comp4_source());
		values.put(report_comp4_contact_no, li.getreport_comp4_contact_no());
		values.put(report_comp4_location, li.getreport_comp4_location());
		values.put(report_comp4_area, li.getreport_comp4_area());
		values.put(report_comp4_base_price, li.getreport_comp4_base_price());
		values.put(report_comp4_price_sqm, li.getreport_comp4_price_sqm());
		values.put(report_comp4_discount_rate, li.getreport_comp4_discount_rate());
		values.put(report_comp4_discounts, li.getreport_comp4_discounts());
		values.put(report_comp4_selling_price, li.getreport_comp4_selling_price());
		values.put(report_comp4_rec_location, li.getreport_comp4_rec_location());
		values.put(report_comp4_rec_size, li.getreport_comp4_rec_size());
		values.put(report_comp4_rec_shape, li.getreport_comp4_rec_shape());
		values.put(report_comp4_rec_corner_influence, li.getreport_comp4_rec_corner_influence());
		values.put(report_comp4_rec_tru_lot, li.getreport_comp4_rec_tru_lot());
		values.put(report_comp4_rec_elevation, li.getreport_comp4_rec_elevation());
		values.put(report_comp4_rec_degree_of_devt, li.getreport_comp4_rec_degree_of_devt());
		values.put(report_comp4_concluded_adjustment, li.getreport_comp4_concluded_adjustment());
		values.put(report_comp4_adjusted_value, li.getreport_comp4_adjusted_value());
		values.put(report_comp5_date_month, li.getreport_comp5_date_month());
		values.put(report_comp5_date_day, li.getreport_comp5_date_day());
		values.put(report_comp5_date_year, li.getreport_comp5_date_year());
		values.put(report_comp5_source, li.getreport_comp5_source());
		values.put(report_comp5_contact_no, li.getreport_comp5_contact_no());
		values.put(report_comp5_location, li.getreport_comp5_location());
		values.put(report_comp5_area, li.getreport_comp5_area());
		values.put(report_comp5_base_price, li.getreport_comp5_base_price());
		values.put(report_comp5_price_sqm, li.getreport_comp5_price_sqm());
		values.put(report_comp5_discount_rate, li.getreport_comp5_discount_rate());
		values.put(report_comp5_discounts, li.getreport_comp5_discounts());
		values.put(report_comp5_selling_price, li.getreport_comp5_selling_price());
		values.put(report_comp5_rec_location, li.getreport_comp5_rec_location());
		values.put(report_comp5_rec_size, li.getreport_comp5_rec_size());
		values.put(report_comp5_rec_shape, li.getreport_comp5_rec_shape());
		values.put(report_comp5_rec_corner_influence, li.getreport_comp5_rec_corner_influence());
		values.put(report_comp5_rec_tru_lot, li.getreport_comp5_rec_tru_lot());
		values.put(report_comp5_rec_elevation, li.getreport_comp5_rec_elevation());
		values.put(report_comp5_rec_degree_of_devt, li.getreport_comp5_rec_degree_of_devt());
		values.put(report_comp5_concluded_adjustment, li.getreport_comp5_concluded_adjustment());
		values.put(report_comp5_adjusted_value, li.getreport_comp5_adjusted_value());
		values.put(report_comp_average, li.getreport_comp_average());
		values.put(report_comp_rounded_to, li.getreport_comp_rounded_to());
		values.put(report_zonal_location, li.getreport_zonal_location());
		values.put(report_zonal_lot_classification, li.getreport_zonal_lot_classification());
		values.put(report_zonal_value, li.getreport_zonal_value());
		values.put(report_value_total_area, li.getreport_value_total_area());
		values.put(report_value_total_deduction, li.getreport_value_total_deduction());
		values.put(report_value_total_net_area, li.getreport_value_total_net_area());
		values.put(report_value_total_landimp_value, li.getreport_value_total_landimp_value());
		values.put(report_imp_value_total_imp_value, li.getreport_imp_value_total_imp_value());
		values.put(report_final_value_total_appraised_value_land_imp, li.getreport_final_value_total_appraised_value_land_imp());
		values.put(report_final_value_recommended_deductions_desc, li.getreport_final_value_recommended_deductions_desc());
		values.put(report_final_value_recommended_deductions_rate, li.getreport_final_value_recommended_deductions_rate());
		values.put(report_final_value_recommended_deductions_amt, li.getreport_final_value_recommended_deductions_amt());
		values.put(report_final_value_net_appraised_value, li.getreport_final_value_net_appraised_value());
		values.put(report_final_value_quick_sale_value_rate, li.getreport_final_value_quick_sale_value_rate());
		values.put(report_final_value_quick_sale_value_amt, li.getreport_final_value_quick_sale_value_amt());
		values.put(report_factors_of_concern, li.getreport_factors_of_concern());
		values.put(report_suggested_corrective_actions, li.getreport_suggested_corrective_actions());
		values.put(report_remarks, li.getreport_remarks());
		values.put(report_requirements, li.getreport_requirements());
		values.put(report_time_inspected, li.getreport_time_inspected());
		values.put(report_comp1_rec_time_element, li.getreport_comp1_rec_time_element());
		values.put(report_comp2_rec_time_element, li.getreport_comp2_rec_time_element());
		values.put(report_comp3_rec_time_element, li.getreport_comp3_rec_time_element());
		values.put(report_comp4_rec_time_element, li.getreport_comp4_rec_time_element());
		values.put(report_comp5_rec_time_element, li.getreport_comp5_rec_time_element());
		values.put(report_comp1_remarks, li.getreport_comp1_remarks());
		values.put(report_comp2_remarks, li.getreport_comp2_remarks());
		values.put(report_comp3_remarks, li.getreport_comp3_remarks());
		values.put(report_comp4_remarks, li.getreport_comp4_remarks());
		values.put(report_comp5_remarks, li.getreport_comp5_remarks());
		values.put(report_concerns_minor_1, li.getreport_concerns_minor_1());
		values.put(report_concerns_minor_2, li.getreport_concerns_minor_2());
		values.put(report_concerns_minor_3, li.getreport_concerns_minor_3());
		values.put(report_concerns_minor_others, li.getreport_concerns_minor_others());
		values.put(report_concerns_minor_others_desc, li.getreport_concerns_minor_others_desc());
		values.put(report_concerns_major_1, li.getreport_concerns_major_1());
		values.put(report_concerns_major_2, li.getreport_concerns_major_2());
		values.put(report_concerns_major_3, li.getreport_concerns_major_3());
		values.put(report_concerns_major_4, li.getreport_concerns_major_4());
		values.put(report_concerns_major_5, li.getreport_concerns_major_5());
		values.put(report_concerns_major_6, li.getreport_concerns_major_6());
		values.put(report_concerns_major_others, li.getreport_concerns_major_others());
		values.put(report_concerns_major_others_desc, li.getreport_concerns_major_others_desc());
		values.put(report_prev_date, li.getreport_prev_date());
		values.put(report_prev_appraiser, li.getreport_prev_appraiser());
		values.put(report_prev_appraiser, li.getreport_prev_appraiser());
		values.put(report_landimp_td_no, li.getreport_landimp_td_no());

		// Inserting Row
		db.insert(tbl_land_improvements, null, values);
		db.close(); // Closing database connection
	}

	// Adding new report_filename
	public void addReport_filename(Report_filename rf) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Report_record_id, rf.getrecord_id());
		values.put(report_uid, rf.getuid());
		values.put(report_file, rf.getfile());
		values.put(report_filename, rf.getfilename());
		values.put(report_appraisal_type, rf.getappraisal_type());
		values.put(report_candidate_done, rf.getcandidate_done());
		// Inserting Row
		db.insert(tbl_report_filename, null, values);
		db.close(); // Closing database connection
	}

	// Getting All images
	public List<Images> getAllImages() {
		List<Images> imagesList = new ArrayList<Images>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_images;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Images images = new Images();
				images.setID(Integer.parseInt(cursor.getString(0)));
				images.setpath(cursor.getString(1));
				images.setfilename(cursor.getString(2));
				// Adding images
				imagesList.add(images);
			} while (cursor.moveToNext());
		}
		db.close();
		// return account list
		return imagesList;
	}
	
	// Getting All images
	public List<Images> getAllImages(String like_filename) {
		List<Images> imagesList = new ArrayList<Images>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_images + " WHERE filename like \'%" + like_filename + "%\'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Images images = new Images();
				images.setID(Integer.parseInt(cursor.getString(0)));
				images.setpath(cursor.getString(1));
				images.setfilename(cursor.getString(2));
				// Adding images
				imagesList.add(images);
			} while (cursor.moveToNext());
		}
		db.close();
		// return account list
		return imagesList;
	}

	// Getting All cities
	public List<Cities> getAllCities() {
		List<Cities> citiesList = new ArrayList<Cities>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_cities;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Cities cities = new Cities();
				cities.setID(Integer.parseInt(cursor.getString(0)));
				cities.setname(cursor.getString(1));
				cities.setprovince_id(cursor.getString(2));

				citiesList.add(cities);
			} while (cursor.moveToNext());
		}
		db.close();
		// return account list
		return citiesList;
	}

	// Getting single provinces
	public List<Provinces> getProvinces_single(String prov_id) {
		List<Provinces> provincesList = new ArrayList<Provinces>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_provinces + " WHERE "
				+ KEY_ID + " =  \"" + prov_id + "\"";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Provinces im = new Provinces();
				im.setID(Integer.parseInt(cursor.getString(0)));
				im.setname(cursor.getString(1));
				im.setregion_id(cursor.getString(2));
				// Adding provinces
				provincesList.add(im);
			} while (cursor.moveToNext());
		}
		db.close();
		return provincesList;
	}

	// Getting single Region
	public List<Regions> getRegion_single(String reg_id) {
		List<Regions> regionsList = new ArrayList<Regions>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_regions + " WHERE "
				+ KEY_ID + " =  \"" + reg_id + "\"";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Regions im = new Regions();
				im.setID(Integer.parseInt(cursor.getString(0)));
				im.setname(cursor.getString(1));
				// Adding region
				regionsList.add(im);
			} while (cursor.moveToNext());
		}
		db.close();
		return regionsList;
	}

	public int getImageCount() {
		int count = 0;
		String selectQuery = "Select * FROM " + tbl_images;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor != null && !cursor.isClosed()) {
			count = cursor.getCount();
			cursor.close();
		}
		db.close();
		return count;
	}

	// Getting All request form
	public List<Request_Form> getAllRequest_Form() {
		List<Request_Form> request_formList = new ArrayList<Request_Form>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_request_form;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Request_Form request_form = new Request_Form();
				request_form.setID(Integer.parseInt(cursor.getString(0)));
				request_form.setrf_date_requested_month(cursor.getString(1));
				request_form.setrf_date_requested_day(cursor.getString(2));
				request_form.setrf_date_requested_year(cursor.getString(3));
				request_form.setrf_classification(cursor.getString(4));
				request_form.setrf_account_fname(cursor.getString(5));
				request_form.setrf_account_mname(cursor.getString(6));
				request_form.setrf_account_lname(cursor.getString(7));
				request_form.setrf_requesting_party(cursor.getString(8));
				// Adding images
				request_formList.add(request_form);
			} while (cursor.moveToNext());
		}
		db.close();
		// return account list
		return request_formList;
	}

	public int getRequest_FormCount() {
		int count = 0;
		String selectQuery = "Select * FROM " + tbl_request_form;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor != null && !cursor.isClosed()) {
			count = cursor.getCount();
			cursor.close();
		}
		db.close();
		return count;

	}

	// Getting All request type
	public List<Request_Type> getAllRequest_Type(String rf_id) {
		List<Request_Type> request_typeList = new ArrayList<Request_Type>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_request_type + " WHERE "
				+ KEY_rf_id + " =  \"" + rf_id + "\"";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Request_Type request_type = new Request_Type();
				request_type.setID(Integer.parseInt(cursor.getString(0)));
				request_type.setrf_id(cursor.getString(1));
				request_type.setrt_control_no(cursor.getString(2));
				request_type.setrt_nature_of_appraisal(cursor.getString(3));
				request_type.setrt_appraisal_type(cursor.getString(4));
				request_type.setrt_pref_ins_date1_month(cursor.getString(5));
				request_type.setrt_pref_ins_date1_day(cursor.getString(6));
				request_type.setrt_pref_ins_date1_year(cursor.getString(7));
				request_type.setrt_pref_ins_date2_month(cursor.getString(8));
				request_type.setrt_pref_ins_date2_day(cursor.getString(9));
				request_type.setrt_pref_ins_date2_year(cursor.getString(10));
				request_type.setrt_mv_year(cursor.getString(11));
				request_type.setrt_mv_make(cursor.getString(12));
				request_type.setrt_mv_model(cursor.getString(13));
				request_type.setrt_mv_odometer(cursor.getString(14));
				request_type.setrt_mv_vin(cursor.getString(15));
				request_type.setrt_me_quantity(cursor.getString(16));
				request_type.setrt_me_type(cursor.getString(17));
				request_type.setrt_me_manufacturer(cursor.getString(18));
				request_type.setrt_me_model(cursor.getString(19));
				request_type.setrt_me_serial(cursor.getString(20));
				request_type.setrt_me_age(cursor.getString(21));
				request_type.setrt_me_condition(cursor.getString(22));
				// Adding images
				request_typeList.add(request_type);
			} while (cursor.moveToNext());
		}
		db.close();
		// return account list
		return request_typeList;
	}

	public int getRequest_TypeCount(String rf_id) {
		int count = 0;
		String selectQuery = "Select * FROM " + tbl_request_type + " WHERE "
				+ KEY_rf_id + " =  \"" + rf_id + "\"";

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor != null && !cursor.isClosed()) {
			count = cursor.getCount();
			cursor.close();
		}
		db.close();
		return count;

	}

	// Getting All report accepted jobs
	public List<Report_Accepted_Jobs> getAllReport_Accepted_Jobs() {
		List<Report_Accepted_Jobs> report_accepted_jobsList = new ArrayList<Report_Accepted_Jobs>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_report_accepted_jobs;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Report_Accepted_Jobs report_accepted_jobs = new Report_Accepted_Jobs();
				report_accepted_jobs
						.setID(Integer.parseInt(cursor.getString(0)));
				report_accepted_jobs.setrecord_id(cursor.getString(1));
				report_accepted_jobs.setdr_month(cursor.getString(2));
				report_accepted_jobs.setdr_day(cursor.getString(3));
				report_accepted_jobs.setdr_year(cursor.getString(4));
				report_accepted_jobs.setclassification(cursor.getString(5));
				report_accepted_jobs.setfname(cursor.getString(6));
				report_accepted_jobs.setmname(cursor.getString(7));
				report_accepted_jobs.setlname(cursor.getString(8));
				report_accepted_jobs.setrequesting_party(cursor.getString(9));
				report_accepted_jobs.setcontrol_no(cursor.getString(10));
				report_accepted_jobs.setappraisal_type(cursor.getString(11));
				report_accepted_jobs.setnature_appraisal(cursor.getString(12));
				report_accepted_jobs.setins_date1_month(cursor.getString(13));
				report_accepted_jobs.setins_date1_day(cursor.getString(14));
				report_accepted_jobs.setins_date1_year(cursor.getString(15));
				report_accepted_jobs.setins_date2_month(cursor.getString(16));
				report_accepted_jobs.setins_date2_day(cursor.getString(17));
				report_accepted_jobs.setins_date2_year(cursor.getString(18));
				report_accepted_jobs.setcom_month(cursor.getString(19));
				report_accepted_jobs.setcom_day(cursor.getString(20));
				report_accepted_jobs.setcom_year(cursor.getString(21));
				report_accepted_jobs.settct_no(cursor.getString(22));
				report_accepted_jobs.setunit_no(cursor.getString(23));
				report_accepted_jobs.setbuilding_name(cursor.getString(24));
				report_accepted_jobs.setstreet_no(cursor.getString(25));
				report_accepted_jobs.setstreet_name(cursor.getString(26));
				report_accepted_jobs.setvillage(cursor.getString(27));
				report_accepted_jobs.setdistrict(cursor.getString(28));
				report_accepted_jobs.setzip_code(cursor.getString(29));
				report_accepted_jobs.setcity(cursor.getString(30));
				report_accepted_jobs.setprovince(cursor.getString(31));
				report_accepted_jobs.setregion(cursor.getString(32));
				report_accepted_jobs.setcountry(cursor.getString(33));
				// Adding images
				report_accepted_jobsList.add(report_accepted_jobs);
			} while (cursor.moveToNext());
		}
		db.close();
		// returnreport_accepted_jobsList
		return report_accepted_jobsList;
	}
	public List<Report_Submitted_Jobs> getAllReport_Submitted_Jobs() {
		List<Report_Submitted_Jobs> report_submitted_jobsList = new ArrayList<Report_Submitted_Jobs>();
		// Select All Query
		String selectQuery = "Select * FROM tbl_report_submitted order by id desc";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Report_Submitted_Jobs report_submitted_jobs = new Report_Submitted_Jobs();
				report_submitted_jobs.setID(Integer.parseInt(cursor.getString(0)));
				report_submitted_jobs.setrecord_id(cursor.getString(1));
				report_submitted_jobs.setfirst_name(cursor.getString(2));
				report_submitted_jobs.setmiddle_name(cursor.getString(3));
				report_submitted_jobs.setlast_name(cursor.getString(4));
				report_submitted_jobs.setappraisal_type(cursor.getString(5));
				report_submitted_jobs.setstatus(cursor.getString(6));
				// Adding images
				report_submitted_jobsList.add(report_submitted_jobs);
			} while (cursor.moveToNext());
		}
		db.close();
		// returnreport_accepted_jobsList
		return report_submitted_jobsList;
	}

	// Getting selected report accepted jobs
	public List<Report_Accepted_Jobs> getReport_Accepted_Jobs(String record_id) {
		List<Report_Accepted_Jobs> report_accepted_jobsList = new ArrayList<Report_Accepted_Jobs>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_report_accepted_jobs
				+ " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Report_Accepted_Jobs report_accepted_jobs = new Report_Accepted_Jobs();
				report_accepted_jobs
						.setID(Integer.parseInt(cursor.getString(0)));
				report_accepted_jobs.setrecord_id(cursor.getString(1));
				report_accepted_jobs.setdr_month(cursor.getString(2));
				report_accepted_jobs.setdr_day(cursor.getString(3));
				report_accepted_jobs.setdr_year(cursor.getString(4));
				report_accepted_jobs.setclassification(cursor.getString(5));
				report_accepted_jobs.setfname(cursor.getString(6));
				report_accepted_jobs.setmname(cursor.getString(7));
				report_accepted_jobs.setlname(cursor.getString(8));
				report_accepted_jobs.setrequesting_party(cursor.getString(9));
				report_accepted_jobs.setcontrol_no(cursor.getString(10));
				report_accepted_jobs.setappraisal_type(cursor.getString(11));
				report_accepted_jobs.setnature_appraisal(cursor.getString(12));
				report_accepted_jobs.setins_date1_month(cursor.getString(13));
				report_accepted_jobs.setins_date1_day(cursor.getString(14));
				report_accepted_jobs.setins_date1_year(cursor.getString(15));
				report_accepted_jobs.setins_date2_month(cursor.getString(16));
				report_accepted_jobs.setins_date2_day(cursor.getString(17));
				report_accepted_jobs.setins_date2_year(cursor.getString(18));
				report_accepted_jobs.setcom_month(cursor.getString(19));
				report_accepted_jobs.setcom_day(cursor.getString(20));
				report_accepted_jobs.setcom_year(cursor.getString(21));
				report_accepted_jobs.settct_no(cursor.getString(22));
				report_accepted_jobs.setunit_no(cursor.getString(23));
				report_accepted_jobs.setbuilding_name(cursor.getString(24));
				report_accepted_jobs.setstreet_no(cursor.getString(25));
				report_accepted_jobs.setstreet_name(cursor.getString(26));
				report_accepted_jobs.setvillage(cursor.getString(27));
				report_accepted_jobs.setdistrict(cursor.getString(28));
				report_accepted_jobs.setzip_code(cursor.getString(29));
				report_accepted_jobs.setcity(cursor.getString(30));
				report_accepted_jobs.setprovince(cursor.getString(31));
				report_accepted_jobs.setregion(cursor.getString(32));
				report_accepted_jobs.setcountry(cursor.getString(33));
				report_accepted_jobs.setcounter(cursor.getString(34));
				report_accepted_jobs.setvehicle_type(cursor.getString(35));
				report_accepted_jobs.setcar_model(cursor.getString(36));
				report_accepted_jobs.setlot_no(cursor.getString(37));
				report_accepted_jobs.setblock_no(cursor.getString(38));
				report_accepted_jobs.setcar_loan_purpose(cursor.getString(39));
				report_accepted_jobs.setcar_mileage(cursor.getString(40));
				report_accepted_jobs.setcar_series(cursor.getString(41));
				report_accepted_jobs.setcar_color(cursor.getString(42));
				report_accepted_jobs.setcar_plate_no(cursor.getString(43));
				report_accepted_jobs.setcar_body_type(cursor.getString(44));
				report_accepted_jobs.setcar_displacement(cursor.getString(45));
				report_accepted_jobs.setcar_motor_no(cursor.getString(46));
				report_accepted_jobs.setcar_chassis_no(cursor.getString(47));
				report_accepted_jobs.setcar_cr_no(cursor.getString(48));
				report_accepted_jobs.setcar_cr_date_month(cursor.getString(49));
				report_accepted_jobs.setcar_cr_date_day(cursor.getString(50));
				report_accepted_jobs.setcar_cr_date_year(cursor.getString(51));
				report_accepted_jobs.setrework_reason(cursor.getString(52));
				report_accepted_jobs.setkind_of_appraisal(cursor.getString(53));
				report_accepted_jobs.setcar_no_cylinders(cursor.getString(54));
				report_accepted_jobs.setrequestor(cursor.getString(55));
				report_accepted_jobs.setapp_request_remarks(cursor.getString(56));
				report_accepted_jobs.setapp_branch_code(cursor.getString(57));

				report_accepted_jobsList.add(report_accepted_jobs);
			} while (cursor.moveToNext());
		}
		db.close();
		// returnreport_accepted_jobsList
		return report_accepted_jobsList;
	}

	// Getting selected report accepted jobs contacts
	public List<Report_Accepted_Jobs_Contacts> getReport_Accepted_Jobs_Contacts(
			String record_id) {
		List<Report_Accepted_Jobs_Contacts> report_accepted_jobs_contactsList = new ArrayList<Report_Accepted_Jobs_Contacts>();
		// Select All Query
		String selectQuery = "Select * FROM "
				+ tbl_report_accepted_jobs_contacts + " WHERE "
				+ Report_record_id + " =  \"" + record_id + "\"";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Report_Accepted_Jobs_Contacts report_accepted_jobs_Contacts = new Report_Accepted_Jobs_Contacts();
				report_accepted_jobs_Contacts.setID(Integer.parseInt(cursor
						.getString(0)));
				report_accepted_jobs_Contacts.setrecord_id(cursor.getString(1));
				report_accepted_jobs_Contacts.setcontact_person(cursor
						.getString(2));
				report_accepted_jobs_Contacts
						.setcontact_mobile_no_prefix(cursor.getString(3));
				report_accepted_jobs_Contacts.setcontact_mobile_no(cursor
						.getString(4));
				report_accepted_jobs_Contacts.setcontact_landline(cursor
						.getString(5));

				// Adding images
				report_accepted_jobs_contactsList
						.add(report_accepted_jobs_Contacts);
			} while (cursor.moveToNext());
		}
		db.close();
		// returnreport_accepted_jobsList contacts
		return report_accepted_jobs_contactsList;
	}

	// Getting single images
	public List<Images> getImages_single(String filename) {
		List<Images> imagesList = new ArrayList<Images>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_images + " WHERE "
				+ KEY_filename + " =  \"" + filename + "\"";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Images im = new Images();
				im.setID(Integer.parseInt(cursor.getString(0)));

				// Adding images
				imagesList.add(im);
			} while (cursor.moveToNext());
		}
		db.close();
		// returnreport_accepted_jobsList contacts
		return imagesList;
	}

	// Getting selected Townhouse_condo
	public List<Townhouse_Condo_API> getTownhouse_Condo(String record_id) {
		List<Townhouse_Condo_API> townhouse_condoList = new ArrayList<Townhouse_Condo_API>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_townhouse_condo + " WHERE "
				+ Report_record_id + " =  \"" + record_id + "\"";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Townhouse_Condo_API townhouse_condo = new Townhouse_Condo_API();
				townhouse_condo.setID(Integer.parseInt(cursor.getString(0)));
				townhouse_condo.setrecord_id(cursor.getString(1));
				townhouse_condo.setreport_cct_no(cursor.getString(2));
				townhouse_condo.setreport_area(cursor.getString(3));
				townhouse_condo.setreport_date_reg_month(cursor.getString(4));
				townhouse_condo.setreport_date_reg_day(cursor.getString(5));
				townhouse_condo.setreport_date_reg_year(cursor.getString(6));
				townhouse_condo.setreport_reg_owner(cursor.getString(7));
				townhouse_condo.setreport_homeowners_assoc(cursor.getString(8));
				townhouse_condo.setreport_admin_office(cursor.getString(9));
				townhouse_condo.setreport_tax_mapping(cursor.getString(10));
				townhouse_condo.setreport_lra_office(cursor.getString(11));
				townhouse_condo.setreport_subd_map(cursor.getString(12));
				townhouse_condo.setreport_lot_config(cursor.getString(13));
				townhouse_condo.setreport_street(cursor.getString(14));
				townhouse_condo.setreport_sidewalk(cursor.getString(15));
				townhouse_condo.setreport_gutter(cursor.getString(16));
				townhouse_condo.setreport_street_lights(cursor.getString(17));
				townhouse_condo.setreport_shape(cursor.getString(18));
				townhouse_condo.setreport_elevation(cursor.getString(19));
				townhouse_condo.setreport_road(cursor.getString(20));
				townhouse_condo.setreport_frontage(cursor.getString(21));
				townhouse_condo.setreport_depth(cursor.getString(22));
				townhouse_condo.setreport_electricity(cursor.getString(23));
				townhouse_condo.setreport_water(cursor.getString(24));
				townhouse_condo.setreport_telephone(cursor.getString(25));
				townhouse_condo.setreport_drainage(cursor.getString(26));
				townhouse_condo.setreport_garbage(cursor.getString(27));
				townhouse_condo.setreport_jeepneys(cursor.getString(28));
				townhouse_condo.setreport_bus(cursor.getString(29));
				townhouse_condo.setreport_taxi(cursor.getString(30));
				townhouse_condo.setreport_tricycle(cursor.getString(31));
				townhouse_condo.setreport_recreation_facilities(cursor
						.getString(32));
				townhouse_condo.setreport_security(cursor.getString(33));
				townhouse_condo.setreport_clubhouse(cursor.getString(34));
				townhouse_condo.setreport_swimming_pool(cursor.getString(35));
				townhouse_condo.setreport_community(cursor.getString(36));
				townhouse_condo.setreport_classification(cursor.getString(37));
				townhouse_condo.setreport_growth_rate(cursor.getString(38));
				townhouse_condo.setreport_adverse(cursor.getString(39));
				townhouse_condo.setreport_occupancy(cursor.getString(40));
				townhouse_condo.setreport_right(cursor.getString(41));
				townhouse_condo.setreport_left(cursor.getString(42));
				townhouse_condo.setreport_rear(cursor.getString(43));
				townhouse_condo.setreport_front(cursor.getString(44));
				townhouse_condo.setreport_desc(cursor.getString(45));
				townhouse_condo.setreport_total_area(cursor.getString(46));
				townhouse_condo.setreport_deduc(cursor.getString(47));
				townhouse_condo.setreport_net_area(cursor.getString(48));
				townhouse_condo.setreport_unit_value(cursor.getString(49));
				townhouse_condo.setreport_total_parking_area(cursor
						.getString(50));
				townhouse_condo.setreport_parking_area_value(cursor
						.getString(51));
				townhouse_condo.setreport_total_condo_value(cursor
						.getString(52));
				townhouse_condo.setreport_total_parking_value(cursor
						.getString(53));
				townhouse_condo.setreport_total_market_value(cursor
						.getString(54));
				townhouse_condo.setreport_date_inspected_month(cursor
						.getString(55));
				townhouse_condo.setreport_date_inspected_day(cursor
						.getString(56));
				townhouse_condo.setreport_date_inspected_year(cursor
						.getString(57));
				
				// Adding images
				townhouse_condoList.add(townhouse_condo);
			} while (cursor.moveToNext());
		}
		db.close();
		// return townhouse_condoList
		return townhouse_condoList;
	}
	
	// Getting selected Condo
	public List<Condo_API> getCondo(String record_id) {
		List<Condo_API> condoList = new ArrayList<Condo_API>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_condo + " WHERE "
				+ Report_record_id + " =  \"" + record_id + "\"";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Condo_API condo = new Condo_API();
				condo.setID(Integer.parseInt(cursor.getString(0)));
				condo.setrecord_id(cursor.getString(1));
				condo.setreport_date_inspected_month(cursor.getString(2));
				condo.setreport_date_inspected_day(cursor.getString(3));
				condo.setreport_date_inspected_year(cursor.getString(4));
				condo.setreport_id_admin(cursor.getString(5));
				condo.setreport_id_unit_numbering(cursor.getString(6));
				condo.setreport_id_bldg_plan(cursor.getString(7));
				condo.setreport_unitclass_per_tax_dec(cursor.getString(8));
				condo.setreport_unitclass_actual_usage(cursor.getString(9));
				condo.setreport_unitclass_neighborhood(cursor.getString(10));
				condo.setreport_unitclass_highest_best_use(cursor.getString(11));
				condo.setreport_trans_jeep(cursor.getString(12));
				condo.setreport_trans_bus(cursor.getString(13));
				condo.setreport_trans_taxi(cursor.getString(14));
				condo.setreport_trans_tricycle(cursor.getString(15));
				condo.setreport_faci_function_room(cursor.getString(16));
				condo.setreport_faci_gym(cursor.getString(17));
				condo.setreport_faci_sauna(cursor.getString(18));
				condo.setreport_faci_pool(cursor.getString(19));
				condo.setreport_faci_fire_alarm(cursor.getString(20));
				condo.setreport_faci_cctv(cursor.getString(21));
				condo.setreport_faci_elevator(cursor.getString(22));
				condo.setreport_util_electricity(cursor.getString(23));
				condo.setreport_util_water(cursor.getString(24));
				condo.setreport_util_telephone(cursor.getString(25));
				condo.setreport_util_garbage(cursor.getString(26));
				condo.setreport_landmarks_1(cursor.getString(27));
				condo.setreport_landmarks_2(cursor.getString(28));
				condo.setreport_landmarks_3(cursor.getString(29));
				condo.setreport_landmarks_4(cursor.getString(30));
				condo.setreport_landmarks_5(cursor.getString(31));
				condo.setreport_distance_1(cursor.getString(32));
				condo.setreport_distance_2(cursor.getString(33));
				condo.setreport_distance_3(cursor.getString(34));
				condo.setreport_distance_4(cursor.getString(35));
				condo.setreport_distance_5(cursor.getString(36));
				condo.setreport_zonal_location(cursor.getString(37));
				condo.setreport_zonal_lot_classification(cursor.getString(38));
				condo.setreport_zonal_value(cursor.getString(39));
				condo.setreport_final_value_total_appraised_value(cursor
						.getString(40));
				condo.setreport_final_value_recommended_deductions_desc(cursor
						.getString(41));
				condo.setreport_final_value_recommended_deductions_rate(cursor
						.getString(42));
				condo.setreport_final_value_recommended_deductions_amt(cursor
						.getString(43));
				condo.setreport_final_value_net_appraised_value(cursor
						.getString(44));
				condo.setreport_final_value_quick_sale_value_rate(cursor
						.getString(45));
				condo.setreport_final_value_quick_sale_value_amt(cursor
						.getString(46));
				condo.setreport_factors_of_concern(cursor.getString(47));
				condo.setreport_suggested_corrective_actions(cursor.getString(48));
				condo.setreport_remarks(cursor.getString(49));
				condo.setreport_requirements(cursor.getString(50));
				condo.setreport_time_inspected(cursor.getString(51));
				
				condo.setreport_comp1_date_month(cursor.getString(52));
				condo.setreport_comp1_date_day(cursor.getString(53));
				condo.setreport_comp1_date_year(cursor.getString(54));
				condo.setreport_comp1_source(cursor.getString(55));
				condo.setreport_comp1_contact_no(cursor.getString(56));
				condo.setreport_comp1_location(cursor.getString(57));
				condo.setreport_comp1_area(cursor.getString(58));
				condo.setreport_comp1_base_price(cursor.getString(59));
				condo.setreport_comp1_price_sqm(cursor.getString(60));
				condo.setreport_comp1_discount_rate(cursor.getString(61));
				condo.setreport_comp1_discounts(cursor.getString(62));
				condo.setreport_comp1_selling_price(cursor.getString(63));
				condo.setreport_comp1_rec_location(cursor.getString(64));
				condo.setreport_comp1_rec_unit_floor_location(cursor.getString(65));
				condo.setreport_comp1_rec_orientation(cursor.getString(66));
				condo.setreport_comp1_rec_size(cursor.getString(67));
				condo.setreport_comp1_rec_unit_condition(cursor.getString(68));
				condo.setreport_comp1_rec_amenities(cursor.getString(69));
				condo.setreport_comp1_rec_unit_features(cursor.getString(70));
				condo.setreport_comp1_concluded_adjustment(cursor.getString(71));
				condo.setreport_comp1_adjusted_value(cursor.getString(72));
				condo.setreport_comp2_date_month(cursor.getString(73));
				condo.setreport_comp2_date_day(cursor.getString(74));
				condo.setreport_comp2_date_year(cursor.getString(75));
				condo.setreport_comp2_source(cursor.getString(76));
				condo.setreport_comp2_contact_no(cursor.getString(77));
				condo.setreport_comp2_location(cursor.getString(78));
				condo.setreport_comp2_area(cursor.getString(79));
				condo.setreport_comp2_base_price(cursor.getString(80));
				condo.setreport_comp2_price_sqm(cursor.getString(81));
				condo.setreport_comp2_discount_rate(cursor.getString(82));
				condo.setreport_comp2_discounts(cursor.getString(83));
				condo.setreport_comp2_selling_price(cursor.getString(84));
				condo.setreport_comp2_rec_location(cursor.getString(85));
				condo.setreport_comp2_rec_unit_floor_location(cursor.getString(86));
				condo.setreport_comp2_rec_orientation(cursor.getString(87));
				condo.setreport_comp2_rec_size(cursor.getString(88));
				condo.setreport_comp2_rec_unit_condition(cursor.getString(89));
				condo.setreport_comp2_rec_amenities(cursor.getString(90));
				condo.setreport_comp2_rec_unit_features(cursor.getString(91));
				condo.setreport_comp2_concluded_adjustment(cursor.getString(92));
				condo.setreport_comp2_adjusted_value(cursor.getString(93));
				condo.setreport_comp3_date_month(cursor.getString(94));
				condo.setreport_comp3_date_day(cursor.getString(95));
				condo.setreport_comp3_date_year(cursor.getString(96));
				condo.setreport_comp3_source(cursor.getString(97));
				condo.setreport_comp3_contact_no(cursor.getString(98));
				condo.setreport_comp3_location(cursor.getString(99));
				condo.setreport_comp3_area(cursor.getString(100));
				condo.setreport_comp3_base_price(cursor.getString(101));
				condo.setreport_comp3_price_sqm(cursor.getString(102));
				condo.setreport_comp3_discount_rate(cursor.getString(103));
				condo.setreport_comp3_discounts(cursor.getString(104));
				condo.setreport_comp3_selling_price(cursor.getString(105));
				condo.setreport_comp3_rec_location(cursor.getString(106));
				condo.setreport_comp3_rec_unit_floor_location(cursor.getString(107));
				condo.setreport_comp3_rec_orientation(cursor.getString(108));
				condo.setreport_comp3_rec_size(cursor.getString(109));
				condo.setreport_comp3_rec_unit_condition(cursor.getString(110));
				condo.setreport_comp3_rec_amenities(cursor.getString(111));
				condo.setreport_comp3_rec_unit_features(cursor.getString(112));
				condo.setreport_comp3_concluded_adjustment(cursor.getString(113));
				condo.setreport_comp3_adjusted_value(cursor.getString(114));
				condo.setreport_comp4_date_month(cursor.getString(115));
				condo.setreport_comp4_date_day(cursor.getString(116));
				condo.setreport_comp4_date_year(cursor.getString(117));
				condo.setreport_comp4_source(cursor.getString(118));
				condo.setreport_comp4_contact_no(cursor.getString(119));
				condo.setreport_comp4_location(cursor.getString(120));
				condo.setreport_comp4_area(cursor.getString(121));
				condo.setreport_comp4_base_price(cursor.getString(122));
				condo.setreport_comp4_price_sqm(cursor.getString(123));
				condo.setreport_comp4_discount_rate(cursor.getString(124));
				condo.setreport_comp4_discounts(cursor.getString(125));
				condo.setreport_comp4_selling_price(cursor.getString(126));
				condo.setreport_comp4_rec_location(cursor.getString(127));
				condo.setreport_comp4_rec_unit_floor_location(cursor.getString(128));
				condo.setreport_comp4_rec_orientation(cursor.getString(129));
				condo.setreport_comp4_rec_size(cursor.getString(130));
				condo.setreport_comp4_rec_unit_condition(cursor.getString(131));
				condo.setreport_comp4_rec_amenities(cursor.getString(132));
				condo.setreport_comp4_rec_unit_features(cursor.getString(133));
				condo.setreport_comp4_concluded_adjustment(cursor.getString(134));
				condo.setreport_comp4_adjusted_value(cursor.getString(135));
				condo.setreport_comp5_date_month(cursor.getString(136));
				condo.setreport_comp5_date_day(cursor.getString(137));
				condo.setreport_comp5_date_year(cursor.getString(138));
				condo.setreport_comp5_source(cursor.getString(139));
				condo.setreport_comp5_contact_no(cursor.getString(140));
				condo.setreport_comp5_location(cursor.getString(141));
				condo.setreport_comp5_area(cursor.getString(142));
				condo.setreport_comp5_base_price(cursor.getString(143));
				condo.setreport_comp5_price_sqm(cursor.getString(144));
				condo.setreport_comp5_discount_rate(cursor.getString(145));
				condo.setreport_comp5_discounts(cursor.getString(146));
				condo.setreport_comp5_selling_price(cursor.getString(147));
				condo.setreport_comp5_rec_location(cursor.getString(148));
				condo.setreport_comp5_rec_unit_floor_location(cursor.getString(149));
				condo.setreport_comp5_rec_orientation(cursor.getString(150));
				condo.setreport_comp5_rec_size(cursor.getString(151));
				condo.setreport_comp5_rec_unit_condition(cursor.getString(152));
				condo.setreport_comp5_rec_amenities(cursor.getString(153));
				condo.setreport_comp5_rec_unit_features(cursor.getString(154));
				condo.setreport_comp5_concluded_adjustment(cursor.getString(155));
				condo.setreport_comp5_adjusted_value(cursor.getString(156));
				condo.setreport_comp_average(cursor.getString(157));
				condo.setreport_comp_rounded_to(cursor.getString(158));
				condo.setreport_comp1_rec_time_element(cursor.getString(159));
				condo.setreport_comp2_rec_time_element(cursor.getString(160));
				condo.setreport_comp3_rec_time_element(cursor.getString(161));
				condo.setreport_comp4_rec_time_element(cursor.getString(162));
				condo.setreport_comp5_rec_time_element(cursor.getString(163));
				condo.setreport_comp1_remarks(cursor.getString(164));
				condo.setreport_comp2_remarks(cursor.getString(165));
				condo.setreport_comp3_remarks(cursor.getString(166));
				condo.setreport_comp4_remarks(cursor.getString(167));
				condo.setreport_comp5_remarks(cursor.getString(168));
				//ADDED From IAN
				condo.setreport_prev_date(cursor.getString(169));
				condo.setreport_prev_appraiser(cursor.getString(170));
				condo.setreport_prev_total_appraised_value(cursor.getString(171));
				condoList.add(condo);
			} while (cursor.moveToNext());
		}
		db.close();
		// return condoList
		return condoList;
	}
	
	// Getting selected Land improvements
	public List<Townhouse_API> getTownhouse(String record_id) {
		List<Townhouse_API> townhouseList = new ArrayList<Townhouse_API>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_townhouse
				+ " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Townhouse_API li = new Townhouse_API();
				li.setID(Integer.parseInt(cursor.getString(0)));
				li.setrecord_id(cursor.getString(1));
				li.setreport_date_inspected_month(cursor.getString(2));
				li.setreport_date_inspected_day(cursor.getString(3));
				li.setreport_date_inspected_year(cursor.getString(4));
				li.setreport_id_association(cursor.getString(5));
				li.setreport_id_tax(cursor.getString(6));
				li.setreport_id_lra(cursor.getString(7));
				li.setreport_id_lot_config(cursor.getString(8));
				li.setreport_id_subd_map(cursor.getString(9));
				li.setreport_id_nbrhood_checking(cursor.getString(10));
				li.setreport_imp_street_name(cursor.getString(11));
				li.setreport_imp_street(cursor.getString(12));
				li.setreport_imp_sidewalk(cursor.getString(13));
				li.setreport_imp_curb(cursor.getString(14));
				li.setreport_imp_drainage(cursor.getString(15));
				li.setreport_imp_street_lights(cursor.getString(16));
				li.setreport_physical_corner_lot(cursor.getString(17));
				li.setreport_physical_non_corner_lot(cursor.getString(18));
				li.setreport_physical_perimeter_lot(cursor.getString(19));
				li.setreport_physical_intersected_lot(cursor.getString(20));
				li.setreport_physical_interior_with_row(cursor.getString(21));
				li.setreport_physical_landlocked(cursor.getString(22));
				li.setreport_lotclass_per_tax_dec(cursor.getString(23));
				li.setreport_lotclass_actual_usage(cursor.getString(24));
				li.setreport_lotclass_neighborhood(cursor.getString(25));
				li.setreport_lotclass_highest_best_use(cursor.getString(26));
				li.setreport_physical_shape(cursor.getString(27));
				li.setreport_physical_frontage(cursor.getString(28));
				li.setreport_physical_depth(cursor.getString(29));
				li.setreport_physical_road(cursor.getString(30));
				li.setreport_physical_elevation(cursor.getString(31));
				li.setreport_physical_terrain(cursor.getString(32));
				li.setreport_landmark_1(cursor.getString(33));
				li.setreport_landmark_2(cursor.getString(34));
				li.setreport_landmark_3(cursor.getString(35));
				li.setreport_landmark_4(cursor.getString(36));
				li.setreport_landmark_5(cursor.getString(37));
				li.setreport_distance_1(cursor.getString(38));
				li.setreport_distance_2(cursor.getString(39));
				li.setreport_distance_3(cursor.getString(40));
				li.setreport_distance_4(cursor.getString(41));
				li.setreport_distance_5(cursor.getString(42));
				li.setreport_util_electricity(cursor.getString(43));
				li.setreport_util_water(cursor.getString(44));
				li.setreport_util_telephone(cursor.getString(45));
				li.setreport_util_garbage(cursor.getString(46));
				li.setreport_trans_jeep(cursor.getString(47));
				li.setreport_trans_bus(cursor.getString(48));
				li.setreport_trans_taxi(cursor.getString(49));
				li.setreport_trans_tricycle(cursor.getString(50));
				li.setreport_faci_recreation(cursor.getString(51));
				li.setreport_faci_security(cursor.getString(52));
				li.setreport_faci_clubhouse(cursor.getString(53));
				li.setreport_faci_pool(cursor.getString(54));
				li.setreport_bound_right(cursor.getString(55));
				li.setreport_bound_left(cursor.getString(56));
				li.setreport_bound_rear(cursor.getString(57));
				li.setreport_bound_front(cursor.getString(58));
				
				li.setreport_comp1_date_month(cursor.getString(59));
				li.setreport_comp1_date_day(cursor.getString(60));
				li.setreport_comp1_date_year(cursor.getString(61));
				li.setreport_comp1_source(cursor.getString(62));
				li.setreport_comp1_contact_no(cursor.getString(63));
				li.setreport_comp1_location(cursor.getString(64));
				li.setreport_comp1_area(cursor.getString(65));
				li.setreport_comp1_base_price(cursor.getString(66));
				li.setreport_comp1_price_sqm(cursor.getString(67));
				li.setreport_comp1_discount_rate(cursor.getString(68));
				li.setreport_comp1_discounts(cursor.getString(69));
				li.setreport_comp1_selling_price(cursor.getString(70));
				li.setreport_comp1_rec_location(cursor.getString(71));
				li.setreport_comp1_rec_lot_size(cursor.getString(72));
				li.setreport_comp1_rec_floor_area(cursor.getString(73));
				li.setreport_comp1_rec_unit_condition(cursor.getString(74));
				li.setreport_comp1_rec_unit_features(cursor.getString(75));

				li.setreport_comp1_rec_degree_of_devt(cursor.getString(76));
				li.setreport_comp1_concluded_adjustment(cursor.getString(77));
				li.setreport_comp1_adjusted_value(cursor.getString(78));
				li.setreport_comp2_date_month(cursor.getString(79));
				li.setreport_comp2_date_day(cursor.getString(80));
				li.setreport_comp2_date_year(cursor.getString(81));
				li.setreport_comp2_source(cursor.getString(82));
				li.setreport_comp2_contact_no(cursor.getString(83));
				li.setreport_comp2_location(cursor.getString(84));
				li.setreport_comp2_area(cursor.getString(85));
				li.setreport_comp2_base_price(cursor.getString(86));
				li.setreport_comp2_price_sqm(cursor.getString(87));
				li.setreport_comp2_discount_rate(cursor.getString(88));
				li.setreport_comp2_discounts(cursor.getString(89));
				li.setreport_comp2_selling_price(cursor.getString(90));
				li.setreport_comp2_rec_location(cursor.getString(91));
				li.setreport_comp2_rec_lot_size(cursor.getString(92));
				li.setreport_comp2_rec_floor_area(cursor.getString(93));
				li.setreport_comp2_rec_unit_condition(cursor.getString(94));
				li.setreport_comp2_rec_unit_features(cursor.getString(95));

				li.setreport_comp2_rec_degree_of_devt(cursor.getString(96));
				li.setreport_comp2_concluded_adjustment(cursor.getString(97));
				li.setreport_comp2_adjusted_value(cursor.getString(98));
				li.setreport_comp3_date_month(cursor.getString(99));
				li.setreport_comp3_date_day(cursor.getString(100));
				li.setreport_comp3_date_year(cursor.getString(101));
				li.setreport_comp3_source(cursor.getString(102));
				li.setreport_comp3_contact_no(cursor.getString(103));
				li.setreport_comp3_location(cursor.getString(104));
				li.setreport_comp3_area(cursor.getString(105));
				li.setreport_comp3_base_price(cursor.getString(106));
				li.setreport_comp3_price_sqm(cursor.getString(107));
				li.setreport_comp3_discount_rate(cursor.getString(108));
				li.setreport_comp3_discounts(cursor.getString(109));
				li.setreport_comp3_selling_price(cursor.getString(110));
				li.setreport_comp3_rec_location(cursor.getString(111));
				li.setreport_comp3_rec_lot_size(cursor.getString(112));
				li.setreport_comp3_rec_floor_area(cursor.getString(113));
				li.setreport_comp3_rec_unit_condition(cursor.getString(114));
				li.setreport_comp3_rec_unit_features(cursor.getString(115));

				li.setreport_comp3_rec_degree_of_devt(cursor.getString(116));
				li.setreport_comp3_concluded_adjustment(cursor.getString(117));
				li.setreport_comp3_adjusted_value(cursor.getString(118));
				li.setreport_comp4_date_month(cursor.getString(119));
				li.setreport_comp4_date_day(cursor.getString(120));
				li.setreport_comp4_date_year(cursor.getString(121));
				li.setreport_comp4_source(cursor.getString(122));
				li.setreport_comp4_contact_no(cursor.getString(123));
				li.setreport_comp4_location(cursor.getString(124));
				li.setreport_comp4_area(cursor.getString(125));
				li.setreport_comp4_base_price(cursor.getString(126));
				li.setreport_comp4_price_sqm(cursor.getString(127));
				li.setreport_comp4_discount_rate(cursor.getString(128));
				li.setreport_comp4_discounts(cursor.getString(129));
				li.setreport_comp4_selling_price(cursor.getString(130));
				li.setreport_comp4_rec_location(cursor.getString(131));
				li.setreport_comp4_rec_lot_size(cursor.getString(132));
				li.setreport_comp4_rec_floor_area(cursor.getString(133));
				li.setreport_comp4_rec_unit_condition(cursor.getString(134));
				li.setreport_comp4_rec_unit_features(cursor.getString(135));

				li.setreport_comp4_rec_degree_of_devt(cursor.getString(136));
				li.setreport_comp4_concluded_adjustment(cursor.getString(137));
				li.setreport_comp4_adjusted_value(cursor.getString(138));
				li.setreport_comp5_date_month(cursor.getString(139));
				li.setreport_comp5_date_day(cursor.getString(140));
				li.setreport_comp5_date_year(cursor.getString(141));
				li.setreport_comp5_source(cursor.getString(142));
				li.setreport_comp5_contact_no(cursor.getString(143));
				li.setreport_comp5_location(cursor.getString(144));
				li.setreport_comp5_area(cursor.getString(145));
				li.setreport_comp5_base_price(cursor.getString(146));
				li.setreport_comp5_price_sqm(cursor.getString(147));
				li.setreport_comp5_discount_rate(cursor.getString(148));
				li.setreport_comp5_discounts(cursor.getString(149));
				li.setreport_comp5_selling_price(cursor.getString(150));
				li.setreport_comp5_rec_location(cursor.getString(151));
				li.setreport_comp5_rec_lot_size(cursor.getString(152));
				li.setreport_comp5_rec_floor_area(cursor.getString(153));
				li.setreport_comp5_rec_unit_condition(cursor.getString(154));
				li.setreport_comp5_rec_unit_features(cursor.getString(155));

				li.setreport_comp5_rec_degree_of_devt(cursor.getString(156));
				li.setreport_comp5_concluded_adjustment(cursor.getString(157));
				li.setreport_comp5_adjusted_value(cursor.getString(158));
				li.setreport_comp_average(cursor.getString(159));
				li.setreport_comp_rounded_to(cursor.getString(160));
				li.setreport_zonal_location(cursor.getString(161));
				li.setreport_zonal_lot_classification(cursor.getString(162));
				li.setreport_zonal_value(cursor.getString(163));
				li.setreport_value_total_area(cursor.getString(164));
				li.setreport_value_total_deduction(cursor.getString(165));
				li.setreport_value_total_net_area(cursor.getString(166));
				li.setreport_value_total_landimp_value(cursor.getString(167));
				li.setreport_imp_value_total_imp_value(cursor.getString(168));
				li.setreport_final_value_total_appraised_value_land_imp(cursor.getString(169));
				li.setreport_final_value_recommended_deductions_desc(cursor.getString(170));
				li.setreport_final_value_recommended_deductions_rate(cursor.getString(171));
				li.setreport_final_value_recommended_deductions_amt(cursor.getString(172));
				li.setreport_final_value_net_appraised_value(cursor.getString(173));
				li.setreport_final_value_quick_sale_value_rate(cursor.getString(174));
				li.setreport_final_value_quick_sale_value_amt(cursor.getString(175));
				li.setreport_factors_of_concern(cursor.getString(176));
				li.setreport_suggested_corrective_actions(cursor.getString(177));
				li.setreport_remarks(cursor.getString(178));
				li.setreport_requirements(cursor.getString(179));
				li.setreport_time_inspected(cursor.getString(180));

				li.setreport_comp1_remarks(cursor.getString(181));
				li.setreport_comp2_remarks(cursor.getString(182));
				li.setreport_comp3_remarks(cursor.getString(183));
				li.setreport_comp4_remarks(cursor.getString(184));
				li.setreport_comp5_remarks(cursor.getString(185));
				
				li.setreport_comp1_lot_area(cursor.getString(186));
				li.setreport_comp2_lot_area(cursor.getString(187));
				li.setreport_comp3_lot_area(cursor.getString(188));
				li.setreport_comp4_lot_area(cursor.getString(189));
				li.setreport_comp5_lot_area(cursor.getString(190));
				
				li.setreport_concerns_minor_1(cursor.getString(191));
				li.setreport_concerns_minor_2(cursor.getString(192));
				li.setreport_concerns_minor_3(cursor.getString(193));
				li.setreport_concerns_minor_others(cursor.getString(194));
				li.setreport_concerns_minor_others_desc(cursor.getString(195));
				li.setreport_concerns_major_1(cursor.getString(196));
				li.setreport_concerns_major_2(cursor.getString(197));
				li.setreport_concerns_major_3(cursor.getString(198));
				li.setreport_concerns_major_4(cursor.getString(199));
				li.setreport_concerns_major_5(cursor.getString(200));
				li.setreport_concerns_major_6(cursor.getString(201));
				li.setreport_concerns_major_others(cursor.getString(202));
				li.setreport_concerns_major_others_desc(cursor.getString(203));

				//ADDED From IAN
				li.setreport_prev_date(cursor.getString(204));
				li.setreport_prev_appraiser(cursor.getString(205));
				li.setreport_prev_total_appraised_value(cursor.getString(206));
				li.setreport_final_value_recommended_deductions_other(cursor.getString(207));
				li.setreport_comp1_rec_corner_influence(cursor.getString(208));
				li.setreport_comp2_rec_corner_influence(cursor.getString(209));
				li.setreport_comp3_rec_corner_influence(cursor.getString(210));
				li.setreport_comp4_rec_corner_influence(cursor.getString(211));
				li.setreport_comp5_rec_corner_influence(cursor.getString(212));
				li.setreport_townhouse_td_no(cursor.getString(213));
				// Adding Townhouse
				townhouseList.add(li);
			} while (cursor.moveToNext());
		}
		db.close();
		// return townhouseList
		return townhouseList;
	}

	// Getting selected Vacant Lot
	public List<Vacant_Lot_API> getVacant_Lot(String record_id) {
		List<Vacant_Lot_API> vacant_lotList = new ArrayList<Vacant_Lot_API>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_vacant_lot
				+ " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Vacant_Lot_API vl = new Vacant_Lot_API();
				vl.setID(Integer.parseInt(cursor.getString(0)));
				vl.setrecord_id(cursor.getString(1));
				vl.setreport_date_inspected_month(cursor.getString(2));
				vl.setreport_date_inspected_day(cursor.getString(3));
				vl.setreport_date_inspected_year(cursor.getString(4));
				vl.setreport_id_association(cursor.getString(5));
				vl.setreport_id_tax(cursor.getString(6));
				vl.setreport_id_lra(cursor.getString(7));
				vl.setreport_id_lot_config(cursor.getString(8));
				vl.setreport_id_subd_map(cursor.getString(9));
				vl.setreport_id_nbrhood_checking(cursor.getString(10));
				vl.setreport_imp_street_name(cursor.getString(11));
				vl.setreport_imp_street(cursor.getString(12));
				vl.setreport_imp_sidewalk(cursor.getString(13));
				vl.setreport_imp_curb(cursor.getString(14));
				vl.setreport_imp_drainage(cursor.getString(15));
				vl.setreport_imp_street_lights(cursor.getString(16));
				vl.setreport_physical_corner_lot(cursor.getString(17));
				vl.setreport_physical_non_corner_lot(cursor.getString(18));
				vl.setreport_physical_perimeter_lot(cursor.getString(19));
				vl.setreport_physical_intersected_lot(cursor.getString(20));
				vl.setreport_physical_interior_with_row(cursor.getString(21));
				vl.setreport_physical_landlocked(cursor.getString(22));
				vl.setreport_lotclass_per_tax_dec(cursor.getString(23));
				vl.setreport_lotclass_actual_usage(cursor.getString(24));
				vl.setreport_lotclass_neighborhood(cursor.getString(25));
				vl.setreport_lotclass_highest_best_use(cursor.getString(26));
				vl.setreport_physical_shape(cursor.getString(27));
				vl.setreport_physical_frontage(cursor.getString(28));
				vl.setreport_physical_depth(cursor.getString(29));
				vl.setreport_physical_road(cursor.getString(30));
				vl.setreport_physical_elevation(cursor.getString(31));
				vl.setreport_physical_terrain(cursor.getString(32));
				vl.setreport_landmark_1(cursor.getString(33));
				vl.setreport_landmark_2(cursor.getString(34));
				vl.setreport_landmark_3(cursor.getString(35));
				vl.setreport_landmark_4(cursor.getString(36));
				vl.setreport_landmark_5(cursor.getString(37));
				vl.setreport_distance_1(cursor.getString(38));
				vl.setreport_distance_2(cursor.getString(39));
				vl.setreport_distance_3(cursor.getString(40));
				vl.setreport_distance_4(cursor.getString(41));
				vl.setreport_distance_5(cursor.getString(42));
				vl.setreport_util_electricity(cursor.getString(43));
				vl.setreport_util_water(cursor.getString(44));
				vl.setreport_util_telephone(cursor.getString(45));
				vl.setreport_util_garbage(cursor.getString(46));
				vl.setreport_trans_jeep(cursor.getString(47));
				vl.setreport_trans_bus(cursor.getString(48));
				vl.setreport_trans_taxi(cursor.getString(49));
				vl.setreport_trans_tricycle(cursor.getString(50));
				vl.setreport_faci_recreation(cursor.getString(51));
				vl.setreport_faci_security(cursor.getString(52));
				vl.setreport_faci_clubhouse(cursor.getString(53));
				vl.setreport_faci_pool(cursor.getString(54));
				vl.setreport_bound_right(cursor.getString(55));
				vl.setreport_bound_left(cursor.getString(56));
				vl.setreport_bound_rear(cursor.getString(57));
				vl.setreport_bound_front(cursor.getString(58));
				vl.setreport_prev_date(cursor.getString(59));
				vl.setreport_prev_appraiser(cursor.getString(60));
				vl.setreport_prev_total_appraised_value(cursor.getString(61));
				
				vl.setreport_comp1_date_month(cursor.getString(62));
				vl.setreport_comp1_date_day(cursor.getString(63));
				vl.setreport_comp1_date_year(cursor.getString(64));
				vl.setreport_comp1_source(cursor.getString(65));
				vl.setreport_comp1_contact_no(cursor.getString(66));
				vl.setreport_comp1_location(cursor.getString(67));
				vl.setreport_comp1_area(cursor.getString(68));
				vl.setreport_comp1_base_price(cursor.getString(69));
				vl.setreport_comp1_price_sqm(cursor.getString(70));
				vl.setreport_comp1_discount_rate(cursor.getString(71));
				vl.setreport_comp1_discounts(cursor.getString(72));
				vl.setreport_comp1_selling_price(cursor.getString(73));
				vl.setreport_comp1_rec_location(cursor.getString(74));
				vl.setreport_comp1_rec_size(cursor.getString(75));
				vl.setreport_comp1_rec_accessibility(cursor.getString(76));
				vl.setreport_comp1_rec_amenities(cursor.getString(77));
				vl.setreport_comp1_rec_terrain(cursor.getString(78));

				vl.setreport_comp1_rec_shape(cursor.getString(79));
				vl.setreport_comp1_rec_corner_influence(cursor.getString(80));
				vl.setreport_comp1_rec_tru_lot(cursor.getString(81));
				vl.setreport_comp1_rec_elevation(cursor.getString(82));
				vl.setreport_comp1_rec_degree_of_devt(cursor.getString(83));
				vl.setreport_comp1_concluded_adjustment(cursor.getString(84));
				vl.setreport_comp1_adjusted_value(cursor.getString(85));
				vl.setreport_comp2_date_month(cursor.getString(86));
				vl.setreport_comp2_date_day(cursor.getString(87));
				vl.setreport_comp2_date_year(cursor.getString(88));
				vl.setreport_comp2_source(cursor.getString(89));
				vl.setreport_comp2_contact_no(cursor.getString(90));
				vl.setreport_comp2_location(cursor.getString(91));
				vl.setreport_comp2_area(cursor.getString(92));
				vl.setreport_comp2_base_price(cursor.getString(93));
				vl.setreport_comp2_price_sqm(cursor.getString(94));
				vl.setreport_comp2_discount_rate(cursor.getString(95));
				vl.setreport_comp2_discounts(cursor.getString(96));
				vl.setreport_comp2_selling_price(cursor.getString(97));
				vl.setreport_comp2_rec_location(cursor.getString(98));
				vl.setreport_comp2_rec_size(cursor.getString(99));
				vl.setreport_comp2_rec_accessibility(cursor.getString(100));
				vl.setreport_comp2_rec_amenities(cursor.getString(101));
				vl.setreport_comp2_rec_terrain(cursor.getString(102));

				vl.setreport_comp2_rec_shape(cursor.getString(103));
				vl.setreport_comp2_rec_corner_influence(cursor.getString(104));
				vl.setreport_comp2_rec_tru_lot(cursor.getString(105));
				vl.setreport_comp2_rec_elevation(cursor.getString(106));
				vl.setreport_comp2_rec_degree_of_devt(cursor.getString(107));
				vl.setreport_comp2_concluded_adjustment(cursor.getString(108));
				vl.setreport_comp2_adjusted_value(cursor.getString(109));
				vl.setreport_comp3_date_month(cursor.getString(110));
				vl.setreport_comp3_date_day(cursor.getString(111));
				vl.setreport_comp3_date_year(cursor.getString(112));
				vl.setreport_comp3_source(cursor.getString(113));
				vl.setreport_comp3_contact_no(cursor.getString(114));
				vl.setreport_comp3_location(cursor.getString(115));
				vl.setreport_comp3_area(cursor.getString(116));
				vl.setreport_comp3_base_price(cursor.getString(117));
				vl.setreport_comp3_price_sqm(cursor.getString(118));
				vl.setreport_comp3_discount_rate(cursor.getString(119));
				vl.setreport_comp3_discounts(cursor.getString(120));
				vl.setreport_comp3_selling_price(cursor.getString(121));
				vl.setreport_comp3_rec_location(cursor.getString(122));
				vl.setreport_comp3_rec_size(cursor.getString(123));
				vl.setreport_comp3_rec_accessibility(cursor.getString(124));
				vl.setreport_comp3_rec_amenities(cursor.getString(125));
				vl.setreport_comp3_rec_terrain(cursor.getString(126));

				vl.setreport_comp3_rec_shape(cursor.getString(127));
				vl.setreport_comp3_rec_corner_influence(cursor.getString(128));
				vl.setreport_comp3_rec_tru_lot(cursor.getString(129));
				vl.setreport_comp3_rec_elevation(cursor.getString(130));
				vl.setreport_comp3_rec_degree_of_devt(cursor.getString(131));
				vl.setreport_comp3_concluded_adjustment(cursor.getString(132));
				vl.setreport_comp3_adjusted_value(cursor.getString(133));
				vl.setreport_comp4_date_month(cursor.getString(134));
				vl.setreport_comp4_date_day(cursor.getString(135));
				vl.setreport_comp4_date_year(cursor.getString(136));
				vl.setreport_comp4_source(cursor.getString(137));
				vl.setreport_comp4_contact_no(cursor.getString(138));
				vl.setreport_comp4_location(cursor.getString(139));
				vl.setreport_comp4_area(cursor.getString(140));
				vl.setreport_comp4_base_price(cursor.getString(141));
				vl.setreport_comp4_price_sqm(cursor.getString(142));
				vl.setreport_comp4_discount_rate(cursor.getString(143));
				vl.setreport_comp4_discounts(cursor.getString(144));
				vl.setreport_comp4_selling_price(cursor.getString(145));
				vl.setreport_comp4_rec_location(cursor.getString(146));
				vl.setreport_comp4_rec_size(cursor.getString(147));
				vl.setreport_comp4_rec_accessibility(cursor.getString(148));
				vl.setreport_comp4_rec_amenities(cursor.getString(149));
				vl.setreport_comp4_rec_terrain(cursor.getString(150));

				vl.setreport_comp4_rec_shape(cursor.getString(151));
				vl.setreport_comp4_rec_corner_influence(cursor.getString(152));
				vl.setreport_comp4_rec_tru_lot(cursor.getString(153));
				vl.setreport_comp4_rec_elevation(cursor.getString(154));
				vl.setreport_comp4_rec_degree_of_devt(cursor.getString(155));
				vl.setreport_comp4_concluded_adjustment(cursor.getString(156));
				vl.setreport_comp4_adjusted_value(cursor.getString(157));
				vl.setreport_comp5_date_month(cursor.getString(158));
				vl.setreport_comp5_date_day(cursor.getString(159));
				vl.setreport_comp5_date_year(cursor.getString(160));
				vl.setreport_comp5_source(cursor.getString(161));
				vl.setreport_comp5_contact_no(cursor.getString(162));
				vl.setreport_comp5_location(cursor.getString(163));
				vl.setreport_comp5_area(cursor.getString(164));
				vl.setreport_comp5_base_price(cursor.getString(165));
				vl.setreport_comp5_price_sqm(cursor.getString(166));
				vl.setreport_comp5_discount_rate(cursor.getString(167));
				vl.setreport_comp5_discounts(cursor.getString(168));
				vl.setreport_comp5_selling_price(cursor.getString(169));
				vl.setreport_comp5_rec_location(cursor.getString(170));
				vl.setreport_comp5_rec_size(cursor.getString(171));
				vl.setreport_comp5_rec_accessibility(cursor.getString(172));
				vl.setreport_comp5_rec_amenities(cursor.getString(173));
				vl.setreport_comp5_rec_terrain(cursor.getString(174));

				vl.setreport_comp5_rec_shape(cursor.getString(175));
				vl.setreport_comp5_rec_corner_influence(cursor.getString(176));
				vl.setreport_comp5_rec_tru_lot(cursor.getString(177));
				vl.setreport_comp5_rec_elevation(cursor.getString(178));
				vl.setreport_comp5_rec_degree_of_devt(cursor.getString(179));
				vl.setreport_comp5_concluded_adjustment(cursor.getString(180));
				vl.setreport_comp5_adjusted_value(cursor.getString(181));
				vl.setreport_comp_average(cursor.getString(182));
				vl.setreport_comp_rounded_to(cursor.getString(183));
				vl.setreport_zonal_location(cursor.getString(184));
				vl.setreport_zonal_lot_classification(cursor.getString(185));
				vl.setreport_zonal_value(cursor.getString(186));
				vl.setreport_value_total_area(cursor.getString(187));
				vl.setreport_value_total_deduction(cursor.getString(188));
				vl.setreport_value_total_net_area(cursor.getString(189));
				vl.setreport_value_total_landimp_value(cursor.getString(190));
				vl.setreport_imp_value_total_imp_value(cursor.getString(191));
				vl.setreport_final_value_total_appraised_value_land_imp(cursor.getString(192));
				vl.setreport_final_value_recommended_deductions_desc(cursor.getString(193));
				vl.setreport_final_value_recommended_deductions_rate(cursor.getString(194));
				vl.setreport_final_value_recommended_deductions_amt(cursor.getString(195));
				vl.setreport_final_value_net_appraised_value(cursor.getString(196));
				vl.setreport_final_value_quick_sale_value_rate(cursor.getString(197));
				vl.setreport_final_value_quick_sale_value_amt(cursor.getString(198));
				vl.setreport_factors_of_concern(cursor.getString(199));
				vl.setreport_suggested_corrective_actions(cursor.getString(200));
				vl.setreport_remarks(cursor.getString(201));
				vl.setreport_requirements(cursor.getString(202));
				vl.setreport_time_inspected(cursor.getString(203));
				vl.setreport_comp1_rec_time_element(cursor.getString(204));
				vl.setreport_comp2_rec_time_element(cursor.getString(205));
				vl.setreport_comp3_rec_time_element(cursor.getString(206));
				vl.setreport_comp4_rec_time_element(cursor.getString(207));
				vl.setreport_comp5_rec_time_element(cursor.getString(208));
				vl.setreport_comp1_rec_others(cursor.getString(209));
				vl.setreport_comp2_rec_others(cursor.getString(210));
				vl.setreport_comp3_rec_others(cursor.getString(211));
				vl.setreport_comp4_rec_others(cursor.getString(212));
				vl.setreport_comp5_rec_others(cursor.getString(213));

				vl.setreport_comp1_remarks(cursor.getString(214));
				vl.setreport_comp2_remarks(cursor.getString(215));
				vl.setreport_comp3_remarks(cursor.getString(216));
				vl.setreport_comp4_remarks(cursor.getString(217));
				vl.setreport_comp5_remarks(cursor.getString(218));
				
				vl.setreport_concerns_minor_1(cursor.getString(219));
				vl.setreport_concerns_minor_2(cursor.getString(220));
				vl.setreport_concerns_minor_3(cursor.getString(221));
				vl.setreport_concerns_minor_others(cursor.getString(222));
				vl.setreport_concerns_minor_others_desc(cursor.getString(223));
				vl.setreport_concerns_major_1(cursor.getString(224));
				vl.setreport_concerns_major_2(cursor.getString(225));
				vl.setreport_concerns_major_3(cursor.getString(226));
				vl.setreport_concerns_major_4(cursor.getString(227));
				vl.setreport_concerns_major_5(cursor.getString(228));
				vl.setreport_concerns_major_6(cursor.getString(229));
				vl.setreport_concerns_major_others(cursor.getString(230));
				vl.setreport_concerns_major_others_desc(cursor.getString(231));
				vl.setreport_final_value_recommended_deductions_other(cursor.getString(232));

				// Adding Land Improvements
				vacant_lotList.add(vl);
			} while (cursor.moveToNext());
		}
		db.close();
		// return vacant_lotList
		return vacant_lotList;
	}

	// Getting selected Land improvements
	public List<Land_Improvements_API> getLand_Improvements(String record_id) {
		List<Land_Improvements_API> land_improvementsList = new ArrayList<Land_Improvements_API>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_land_improvements
				+ " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Land_Improvements_API li = new Land_Improvements_API();
				li.setID(Integer.parseInt(cursor.getString(0)));
				li.setrecord_id(cursor.getString(1));
				li.setreport_date_inspected_month(cursor.getString(2));
				li.setreport_date_inspected_day(cursor.getString(3));
				li.setreport_date_inspected_year(cursor.getString(4));
				li.setreport_id_association(cursor.getString(5));
				li.setreport_id_tax(cursor.getString(6));
				li.setreport_id_lra(cursor.getString(7));
				li.setreport_id_lot_config(cursor.getString(8));
				li.setreport_id_subd_map(cursor.getString(9));
				li.setreport_id_nbrhood_checking(cursor.getString(10));
				li.setreport_imp_street_name(cursor.getString(11));
				li.setreport_imp_street(cursor.getString(12));
				li.setreport_imp_sidewalk(cursor.getString(13));
				li.setreport_imp_curb(cursor.getString(14));
				li.setreport_imp_drainage(cursor.getString(15));
				li.setreport_imp_street_lights(cursor.getString(16));
				li.setreport_physical_corner_lot(cursor.getString(17));
				li.setreport_physical_non_corner_lot(cursor.getString(18));
				li.setreport_physical_perimeter_lot(cursor.getString(19));
				li.setreport_physical_intersected_lot(cursor.getString(20));
				li.setreport_physical_interior_with_row(cursor.getString(21));
				li.setreport_physical_landlocked(cursor.getString(22));
				li.setreport_lotclass_per_tax_dec(cursor.getString(23));
				li.setreport_lotclass_actual_usage(cursor.getString(24));
				li.setreport_lotclass_neighborhood(cursor.getString(25));
				li.setreport_lotclass_highest_best_use(cursor.getString(26));
				li.setreport_physical_shape(cursor.getString(27));
				li.setreport_physical_frontage(cursor.getString(28));
				li.setreport_physical_depth(cursor.getString(29));
				li.setreport_physical_road(cursor.getString(30));
				li.setreport_physical_elevation(cursor.getString(31));
				li.setreport_physical_terrain(cursor.getString(32));
				li.setreport_landmark_1(cursor.getString(33));
				li.setreport_landmark_2(cursor.getString(34));
				li.setreport_landmark_3(cursor.getString(35));
				li.setreport_landmark_4(cursor.getString(36));
				li.setreport_landmark_5(cursor.getString(37));
				li.setreport_distance_1(cursor.getString(38));
				li.setreport_distance_2(cursor.getString(39));
				li.setreport_distance_3(cursor.getString(40));
				li.setreport_distance_4(cursor.getString(41));
				li.setreport_distance_5(cursor.getString(42));
				li.setreport_util_electricity(cursor.getString(43));
				li.setreport_util_water(cursor.getString(44));
				li.setreport_util_telephone(cursor.getString(45));
				li.setreport_util_garbage(cursor.getString(46));
				li.setreport_trans_jeep(cursor.getString(47));
				li.setreport_trans_bus(cursor.getString(48));
				li.setreport_trans_taxi(cursor.getString(49));
				li.setreport_trans_tricycle(cursor.getString(50));
				li.setreport_faci_recreation(cursor.getString(51));
				li.setreport_faci_security(cursor.getString(52));
				li.setreport_faci_clubhouse(cursor.getString(53));
				li.setreport_faci_pool(cursor.getString(54));
				li.setreport_bound_right(cursor.getString(55));
				li.setreport_bound_left(cursor.getString(56));
				li.setreport_bound_rear(cursor.getString(57));
				li.setreport_bound_front(cursor.getString(58));
				
				li.setreport_comp1_date_month(cursor.getString(59));
				li.setreport_comp1_date_day(cursor.getString(60));
				li.setreport_comp1_date_year(cursor.getString(61));
				li.setreport_comp1_source(cursor.getString(62));
				li.setreport_comp1_contact_no(cursor.getString(63));
				li.setreport_comp1_location(cursor.getString(64));
				li.setreport_comp1_area(cursor.getString(65));
				li.setreport_comp1_base_price(cursor.getString(66));
				li.setreport_comp1_price_sqm(cursor.getString(67));
				li.setreport_comp1_discount_rate(cursor.getString(68));
				li.setreport_comp1_discounts(cursor.getString(69));
				li.setreport_comp1_selling_price(cursor.getString(70));
				li.setreport_comp1_rec_location(cursor.getString(71));
				li.setreport_comp1_rec_size(cursor.getString(72));
				li.setreport_comp1_rec_shape(cursor.getString(73));
				li.setreport_comp1_rec_corner_influence(cursor.getString(74));
				li.setreport_comp1_rec_tru_lot(cursor.getString(75));
				li.setreport_comp1_rec_elevation(cursor.getString(76));
				li.setreport_comp1_rec_degree_of_devt(cursor.getString(77));
				li.setreport_comp1_concluded_adjustment(cursor.getString(78));
				li.setreport_comp1_adjusted_value(cursor.getString(79));
				li.setreport_comp2_date_month(cursor.getString(80));
				li.setreport_comp2_date_day(cursor.getString(81));
				li.setreport_comp2_date_year(cursor.getString(82));
				li.setreport_comp2_source(cursor.getString(83));
				li.setreport_comp2_contact_no(cursor.getString(84));
				li.setreport_comp2_location(cursor.getString(85));
				li.setreport_comp2_area(cursor.getString(86));
				li.setreport_comp2_base_price(cursor.getString(87));
				li.setreport_comp2_price_sqm(cursor.getString(88));
				li.setreport_comp2_discount_rate(cursor.getString(89));
				li.setreport_comp2_discounts(cursor.getString(90));
				li.setreport_comp2_selling_price(cursor.getString(91));
				li.setreport_comp2_rec_location(cursor.getString(92));
				li.setreport_comp2_rec_size(cursor.getString(93));
				li.setreport_comp2_rec_shape(cursor.getString(94));
				li.setreport_comp2_rec_corner_influence(cursor.getString(95));
				li.setreport_comp2_rec_tru_lot(cursor.getString(96));
				li.setreport_comp2_rec_elevation(cursor.getString(97));
				li.setreport_comp2_rec_degree_of_devt(cursor.getString(98));
				li.setreport_comp2_concluded_adjustment(cursor.getString(99));
				li.setreport_comp2_adjusted_value(cursor.getString(100));
				li.setreport_comp3_date_month(cursor.getString(101));
				li.setreport_comp3_date_day(cursor.getString(102));
				li.setreport_comp3_date_year(cursor.getString(103));
				li.setreport_comp3_source(cursor.getString(104));
				li.setreport_comp3_contact_no(cursor.getString(105));
				li.setreport_comp3_location(cursor.getString(106));
				li.setreport_comp3_area(cursor.getString(107));
				li.setreport_comp3_base_price(cursor.getString(108));
				li.setreport_comp3_price_sqm(cursor.getString(109));
				li.setreport_comp3_discount_rate(cursor.getString(110));
				li.setreport_comp3_discounts(cursor.getString(111));
				li.setreport_comp3_selling_price(cursor.getString(112));
				li.setreport_comp3_rec_location(cursor.getString(113));
				li.setreport_comp3_rec_size(cursor.getString(114));
				li.setreport_comp3_rec_shape(cursor.getString(115));
				li.setreport_comp3_rec_corner_influence(cursor.getString(116));
				li.setreport_comp3_rec_tru_lot(cursor.getString(117));
				li.setreport_comp3_rec_elevation(cursor.getString(118));
				li.setreport_comp3_rec_degree_of_devt(cursor.getString(119));
				li.setreport_comp3_concluded_adjustment(cursor.getString(120));
				li.setreport_comp3_adjusted_value(cursor.getString(121));
				li.setreport_comp4_date_month(cursor.getString(122));
				li.setreport_comp4_date_day(cursor.getString(123));
				li.setreport_comp4_date_year(cursor.getString(124));
				li.setreport_comp4_source(cursor.getString(125));
				li.setreport_comp4_contact_no(cursor.getString(126));
				li.setreport_comp4_location(cursor.getString(127));
				li.setreport_comp4_area(cursor.getString(128));
				li.setreport_comp4_base_price(cursor.getString(129));
				li.setreport_comp4_price_sqm(cursor.getString(130));
				li.setreport_comp4_discount_rate(cursor.getString(131));
				li.setreport_comp4_discounts(cursor.getString(132));
				li.setreport_comp4_selling_price(cursor.getString(133));
				li.setreport_comp4_rec_location(cursor.getString(134));
				li.setreport_comp4_rec_size(cursor.getString(135));
				li.setreport_comp4_rec_shape(cursor.getString(136));
				li.setreport_comp4_rec_corner_influence(cursor.getString(137));
				li.setreport_comp4_rec_tru_lot(cursor.getString(138));
				li.setreport_comp4_rec_elevation(cursor.getString(139));
				li.setreport_comp4_rec_degree_of_devt(cursor.getString(140));
				li.setreport_comp4_concluded_adjustment(cursor.getString(141));
				li.setreport_comp4_adjusted_value(cursor.getString(142));
				li.setreport_comp5_date_month(cursor.getString(143));
				li.setreport_comp5_date_day(cursor.getString(144));
				li.setreport_comp5_date_year(cursor.getString(145));
				li.setreport_comp5_source(cursor.getString(146));
				li.setreport_comp5_contact_no(cursor.getString(147));
				li.setreport_comp5_location(cursor.getString(148));
				li.setreport_comp5_area(cursor.getString(149));
				li.setreport_comp5_base_price(cursor.getString(150));
				li.setreport_comp5_price_sqm(cursor.getString(151));
				li.setreport_comp5_discount_rate(cursor.getString(152));
				li.setreport_comp5_discounts(cursor.getString(153));
				li.setreport_comp5_selling_price(cursor.getString(154));
				li.setreport_comp5_rec_location(cursor.getString(155));
				li.setreport_comp5_rec_size(cursor.getString(156));
				li.setreport_comp5_rec_shape(cursor.getString(157));
				li.setreport_comp5_rec_corner_influence(cursor.getString(158));
				li.setreport_comp5_rec_tru_lot(cursor.getString(159));
				li.setreport_comp5_rec_elevation(cursor.getString(160));
				li.setreport_comp5_rec_degree_of_devt(cursor.getString(161));
				li.setreport_comp5_concluded_adjustment(cursor.getString(162));
				li.setreport_comp5_adjusted_value(cursor.getString(163));
				li.setreport_comp_average(cursor.getString(164));
				li.setreport_comp_rounded_to(cursor.getString(165));
				li.setreport_zonal_location(cursor.getString(166));
				li.setreport_zonal_lot_classification(cursor.getString(167));
				li.setreport_zonal_value(cursor.getString(168));
				li.setreport_value_total_area(cursor.getString(169));
				li.setreport_value_total_deduction(cursor.getString(170));
				li.setreport_value_total_net_area(cursor.getString(171));
				li.setreport_value_total_landimp_value(cursor.getString(172));
				li.setreport_imp_value_total_imp_value(cursor.getString(173));
				li.setreport_final_value_total_appraised_value_land_imp(cursor.getString(174));
				li.setreport_final_value_recommended_deductions_desc(cursor.getString(175));
				li.setreport_final_value_recommended_deductions_rate(cursor.getString(176));
				li.setreport_final_value_recommended_deductions_amt(cursor.getString(177));
				li.setreport_final_value_net_appraised_value(cursor.getString(178));
				li.setreport_final_value_quick_sale_value_rate(cursor.getString(179));
				li.setreport_final_value_quick_sale_value_amt(cursor.getString(180));
				li.setreport_factors_of_concern(cursor.getString(181));
				li.setreport_suggested_corrective_actions(cursor.getString(182));
				li.setreport_remarks(cursor.getString(183));
				li.setreport_requirements(cursor.getString(184));
				li.setreport_time_inspected(cursor.getString(185));
				li.setreport_comp1_rec_time_element(cursor.getString(186));
				li.setreport_comp2_rec_time_element(cursor.getString(187));
				li.setreport_comp3_rec_time_element(cursor.getString(188));
				li.setreport_comp4_rec_time_element(cursor.getString(189));
				li.setreport_comp5_rec_time_element(cursor.getString(190));
				li.setreport_comp1_remarks(cursor.getString(191));
				li.setreport_comp2_remarks(cursor.getString(192));
				li.setreport_comp3_remarks(cursor.getString(193));
				li.setreport_comp4_remarks(cursor.getString(194));
				li.setreport_comp5_remarks(cursor.getString(195));
				
				li.setreport_concerns_minor_1(cursor.getString(196));
				li.setreport_concerns_minor_2(cursor.getString(197));
				li.setreport_concerns_minor_3(cursor.getString(198));
				li.setreport_concerns_minor_others(cursor.getString(199));
				li.setreport_concerns_minor_others_desc(cursor.getString(200));
				li.setreport_concerns_major_1(cursor.getString(201));
				li.setreport_concerns_major_2(cursor.getString(202));
				li.setreport_concerns_major_3(cursor.getString(203));
				li.setreport_concerns_major_4(cursor.getString(204));
				li.setreport_concerns_major_5(cursor.getString(205));
				li.setreport_concerns_major_6(cursor.getString(206));
				li.setreport_concerns_major_others(cursor.getString(207));
				li.setreport_concerns_major_others_desc(cursor.getString(208));
				//ADDED From IAN
				li.setreport_prev_date(cursor.getString(209));
				li.setreport_prev_appraiser(cursor.getString(210));
				li.setreport_prev_total_appraised_value(cursor.getString(211));
				li.setreport_final_value_recommended_deductions_other(cursor.getString(212));
				li.setreport_landimp_td_no(cursor.getString(213));

				// Adding Land Improvements
				land_improvementsList.add(li);
			} while (cursor.moveToNext());
		}
		db.close();
		// return land_improvementsList
		return land_improvementsList;
	}

	// Getting selected Motor_Vehicle
	public List<Motor_Vehicle_API> getMotor_Vehicle(String record_id) {
		List<Motor_Vehicle_API> motor_vehicleList = new ArrayList<Motor_Vehicle_API>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_motor_vehicle + " WHERE "
				+ Report_record_id + " =  \"" + record_id + "\"";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Motor_Vehicle_API mv = new Motor_Vehicle_API();
				mv.setID(Integer.parseInt(cursor.getString(0)));
				mv.setrecord_id(cursor.getString(1));
				mv.setreport_interior_dashboard(cursor.getString(2));
				mv.setreport_interior_sidings(cursor.getString(3));
				mv.setreport_interior_seats(cursor.getString(4));
				mv.setreport_interior_windows(cursor.getString(5));
				mv.setreport_interior_ceiling(cursor.getString(6));
				mv.setreport_interior_instrument(cursor.getString(7));
				mv.setreport_bodytype_grille(cursor.getString(8));
				mv.setreport_bodytype_headlight(cursor.getString(9));
				mv.setreport_bodytype_fbumper(cursor.getString(10));
				mv.setreport_bodytype_hood(cursor.getString(11));
				mv.setreport_bodytype_rf_fender(cursor.getString(12));
				mv.setreport_bodytype_rf_door(cursor.getString(13));
				mv.setreport_bodytype_rr_door(cursor.getString(14));
				mv.setreport_bodytype_rr_fender(cursor.getString(15));
				mv.setreport_bodytype_backdoor(cursor.getString(16));
				mv.setreport_bodytype_taillight(cursor.getString(17));
				mv.setreport_bodytype_r_bumper(cursor.getString(18)); 
				mv.setreport_bodytype_lr_fender(cursor.getString(19));
				mv.setreport_bodytype_lr_door(cursor.getString(20));
				mv.setreport_bodytype_lf_door(cursor.getString(21));
				mv.setreport_bodytype_lf_fender(cursor.getString(22));
				mv.setreport_bodytype_top(cursor.getString(23));
				mv.setreport_bodytype_paint(cursor.getString(24));
				mv.setreport_bodytype_flooring(cursor.getString(25));
				mv.setreport_misc_stereo(cursor.getString(26));
				mv.setreport_misc_tools(cursor.getString(27));
				mv.setreport_misc_speakers(cursor.getString(28));
				mv.setreport_misc_airbag(cursor.getString(29));
				mv.setreport_misc_tires(cursor.getString(30));
				mv.setreport_misc_mag_wheels(cursor.getString(31));
				mv.setreport_misc_carpet(cursor.getString(32));
				mv.setreport_misc_others(cursor.getString(33));
				mv.setreport_enginearea_fuel(cursor.getString(34));
				mv.setreport_enginearea_transmission1(cursor.getString(35));
				mv.setreport_enginearea_transmission2(cursor.getString(36));
				mv.setreport_enginearea_chassis(cursor.getString(37));
				mv.setreport_enginearea_battery(cursor.getString(38));
				mv.setreport_enginearea_electrical(cursor.getString(39));
				mv.setreport_enginearea_engine(cursor.getString(40));
				mv.setreport_verification(cursor.getString(41));
				mv.setreport_verification_district_office(cursor.getString(42));
				mv.setreport_verification_encumbrance(cursor.getString(43));
				mv.setreport_verification_registered_owner(cursor.getString(44));
				mv.setreport_place_inspected(cursor.getString(45));
				mv.setreport_market_valuation_fair_value(cursor.getString(46));
				mv.setreport_market_valuation_min(cursor.getString(47));
				mv.setreport_market_valuation_max(cursor.getString(48));
				mv.setreport_market_valuation_remarks(cursor.getString(49));
				mv.setreport_date_inspected_month(cursor.getString(50));
				mv.setreport_date_inspected_day(cursor.getString(51));
				mv.setreport_date_inspected_year(cursor.getString(52));
				mv.setreport_time_inspected(cursor.getString(53));


				mv.setreport_comp1_source(cursor.getString(54));
				mv.setreport_comp1_tel_no(cursor.getString(55));
				mv.setreport_comp1_unit_model(cursor.getString(56));
				mv.setreport_comp1_mileage(cursor.getString(57));
				mv.setreport_comp1_price_min(cursor.getString(58));
				mv.setreport_comp1_price_max(cursor.getString(59));
				mv.setreport_comp1_price(cursor.getString(60));
				mv.setreport_comp1_less_amt(cursor.getString(61));
				mv.setreport_comp1_less_net(cursor.getString(62));
				mv.setreport_comp1_add_amt(cursor.getString(63));
				mv.setreport_comp1_add_net(cursor.getString(64));
				mv.setreport_comp1_time_adj_value(cursor.getString(65));
				mv.setreport_comp1_time_adj_amt(cursor.getString(66));
				mv.setreport_comp1_adj_value(cursor.getString(67));
				mv.setreport_comp1_adj_amt(cursor.getString(68));
				mv.setreport_comp1_index_price(cursor.getString(69));

				mv.setreport_comp2_source(cursor.getString(70));
				mv.setreport_comp2_tel_no(cursor.getString(71));
				mv.setreport_comp2_unit_model(cursor.getString(72));
				mv.setreport_comp2_mileage(cursor.getString(73));
				mv.setreport_comp2_price_min(cursor.getString(74));
				mv.setreport_comp2_price_max(cursor.getString(75));
				mv.setreport_comp2_price(cursor.getString(76));
				mv.setreport_comp2_less_amt(cursor.getString(77));
				mv.setreport_comp2_less_net(cursor.getString(78));
				mv.setreport_comp2_add_amt(cursor.getString(79));
				mv.setreport_comp2_add_net(cursor.getString(80));
				mv.setreport_comp2_time_adj_value(cursor.getString(81));
				mv.setreport_comp2_time_adj_amt(cursor.getString(82));
				mv.setreport_comp2_adj_value(cursor.getString(83));
				mv.setreport_comp2_adj_amt(cursor.getString(84));
				mv.setreport_comp2_index_price(cursor.getString(85));



				mv.setreport_comp3_source(cursor.getString(86));
				mv.setreport_comp3_tel_no(cursor.getString(87));
				mv.setreport_comp3_unit_model(cursor.getString(88));
				mv.setreport_comp3_mileage(cursor.getString(89));
				mv.setreport_comp3_price_min(cursor.getString(90));
				mv.setreport_comp3_price_max(cursor.getString(91));
				mv.setreport_comp3_price(cursor.getString(92));
				mv.setreport_comp3_less_amt(cursor.getString(93));
				mv.setreport_comp3_less_net(cursor.getString(94));
				mv.setreport_comp3_add_amt(cursor.getString(95));
				mv.setreport_comp3_add_net(cursor.getString(96));
				mv.setreport_comp3_time_adj_value(cursor.getString(97));
				mv.setreport_comp3_time_adj_amt(cursor.getString(98));
				mv.setreport_comp3_adj_value(cursor.getString(99));
				mv.setreport_comp3_adj_amt(cursor.getString(100));
				mv.setreport_comp3_index_price(cursor.getString(101));


				mv.setreport_comp_ave_index_price(cursor.getString(102));
				mv.setreport_rec_lux_car(cursor.getString(103));
				mv.setreport_rec_mile_factor(cursor.getString(104));
				mv.setreport_rec_taxi_use(cursor.getString(105));
				mv.setreport_rec_for_hire(cursor.getString(106));
				mv.setreport_rec_condition(cursor.getString(107));
				mv.setreport_rec_repo(cursor.getString(108));
				mv.setreport_rec_other_val(cursor.getString(109));
				mv.setreport_rec_other_desc(cursor.getString(110));

				mv.setreport_rec_other2_val(cursor.getString(145));
				mv.setreport_rec_other2_desc(cursor.getString(146));
				mv.setreport_rec_other3_val(cursor.getString(147));
				mv.setreport_rec_other3_desc(cursor.getString(148));

				mv.setreport_rec_market_resistance_rec_total(cursor.getString(111));
				mv.setreport_rec_market_resistance_total(cursor.getString(112));
				mv.setreport_rec_market_resistance_net_total(cursor.getString(113));
				mv.setreport_rep_stereo(cursor.getString(114));
				mv.setreport_rep_speakers(cursor.getString(115));
				mv.setreport_rep_tires_pcs(cursor.getString(116));
				mv.setreport_rep_tires(cursor.getString(117));
				mv.setreport_rep_side_mirror_pcs(cursor.getString(118));
				mv.setreport_rep_side_mirror(cursor.getString(119));
				mv.setreport_rep_light(cursor.getString(120));
				mv.setreport_rep_tools(cursor.getString(121));
				mv.setreport_rep_battery(cursor.getString(122));
				mv.setreport_rep_plates(cursor.getString(123));
				mv.setreport_rep_bumpers(cursor.getString(124));
				mv.setreport_rep_windows(cursor.getString(125));
				mv.setreport_rep_body(cursor.getString(126));
				mv.setreport_rep_engine_wash(cursor.getString(127));
				mv.setreport_rep_other_desc(cursor.getString(128));
				mv.setreport_rep_other(cursor.getString(129));
				mv.setreport_rep_total(cursor.getString(130));
				mv.setreport_appraised_value(cursor.getString(131));
				mv.setreport_verification_file_no(cursor.getString(132));
				mv.setreport_verification_first_reg_date_month(cursor.getString(133));
				mv.setreport_verification_first_reg_date_day(cursor.getString(134));
				mv.setreport_verification_first_reg_date_year(cursor.getString(135));
				mv.setreport_comp1_mileage_value(cursor.getString(136));
				mv.setreport_comp2_mileage_value(cursor.getString(137));
				mv.setreport_comp3_mileage_value(cursor.getString(138));
				mv.setreport_comp1_mileage_amt(cursor.getString(139));
				mv.setreport_comp2_mileage_amt(cursor.getString(140));
				mv.setreport_comp3_mileage_amt(cursor.getString(141));
				mv.setreport_comp1_adj_desc(cursor.getString(142));
				mv.setreport_comp2_adj_desc(cursor.getString(143));
				mv.setreport_comp3_adj_desc(cursor.getString(144));
				mv.setvalrep_mv_bodytype_trunk(cursor.getString(145));
				mv.setvalrep_mv_market_valuation_previous_remarks(cursor.getString(146));


				mv.setvalrep_mv_comp1_new_unit_value(cursor.getString(147));
				mv.setvalrep_mv_comp2_new_unit_value(cursor.getString(148));
				mv.setvalrep_mv_comp3_new_unit_value(cursor.getString(149));
				mv.setvalrep_mv_comp1_new_unit_amt(cursor.getString(150));
				mv.setvalrep_mv_comp2_new_unit_amt(cursor.getString(151));
				mv.setvalrep_mv_comp3_new_unit_amt(cursor.getString(152));
				mv.setvalrep_mv_comp_add_index_price_desc(cursor.getString(153));
				mv.setvalrep_mv_comp_add_index_price(cursor.getString(154));
				mv.setvalrep_mv_comp_temp_ave_index_price(cursor.getString(155));
				mv.setvalrep_mv_rep_other2_desc(cursor.getString(156));
				mv.setvalrep_mv_rep_other2(cursor.getString(157));
				mv.setvalrep_mv_rep_other3_desc(cursor.getString(158));
				mv.setvalrep_mv_rep_other3(cursor.getString(159));

				motor_vehicleList.add(mv);
			} while (cursor.moveToNext());
		}
		db.close();
		// return townhouse_condoList
		return motor_vehicleList;
	}
	// Getting selected index price
	public List<Motor_Vehicle_API> getMotor_Vehicle_IndexPrice(String record_id) {
		List<Motor_Vehicle_API> motor_vehiclelist = new ArrayList<Motor_Vehicle_API>();
		// Select All Query
		String selectQuery = "Select coalesce(report_comp1_index_price,0),coalesce(report_comp2_index_price,0),coalesce(report_comp3_index_price,0) FROM " + tbl_motor_vehicle
				+ " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
		Log.e("", selectQuery);
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Motor_Vehicle_API mv = new Motor_Vehicle_API();
				mv.setreport_comp1_index_price(cursor.getString(0));
				mv.setreport_comp2_index_price(cursor.getString(1));
				mv.setreport_comp3_index_price(cursor.getString(2));


				// Adding Land Improvements
				motor_vehiclelist.add(mv);
			} while (cursor.moveToNext());
		}
		db.close();
		// return vacant_lotList
		return motor_vehiclelist;
	}

	// Getting Min Min price
	public String getMotor_Vehicle_minPrice(String record_id) {
		String result="0";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cur = db.rawQuery("SELECT min(coalesce(cast(report_comp1_index_price as decimal),0),coalesce(cast(report_comp2_index_price as decimal),0),coalesce(cast(report_comp3_index_price as decimal),0)) FROM " + tbl_motor_vehicle
				+ " WHERE " + Report_record_id + " =  \"" + record_id + "\"", null);
		if(cur.moveToFirst())
		{
			result= cur.getString(0);
		}


		db.close();

		return result;
	}

	// Getting MAx Max price
	public String getMotor_Vehicle_maxPrice(String record_id) {
		String result="0";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cur1 = db.rawQuery("SELECT MAX(coalesce(cast(report_comp1_index_price as decimal),0),coalesce(cast(report_comp2_index_price as decimal),0),coalesce(cast(report_comp3_index_price as decimal),0)) FROM " + tbl_motor_vehicle
				+ " WHERE " + Report_record_id + " =  \"" + record_id + "\"", null);
		if(cur1.moveToFirst())
		{
			result= cur1.getString(0);
		}


		db.close();

		return result;
	}

	// Getting selected ppcr
	public List<Ppcr_API> getPpcr(String record_id) {
		List<Ppcr_API> ppcrList = new ArrayList<Ppcr_API>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_ppcr + " WHERE "
				+ Report_record_id + " =  \"" + record_id + "\"";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToLast()) {
			do {
				Ppcr_API ppcr = new Ppcr_API();
				ppcr.setID(Integer.parseInt(cursor.getString(0)));
				ppcr.setrecord_id(cursor.getString(1));
				ppcr.setreport_date_inspected_month(cursor.getString(2));
				ppcr.setreport_date_inspected_day(cursor.getString(3));
				ppcr.setreport_date_inspected_year(cursor.getString(4));
				ppcr.setreport_time_inspected(cursor.getString(5));
				ppcr.setreport_registered_owner(cursor.getString(6));
				ppcr.setreport_project_type(cursor.getString(7));
				ppcr.setreport_is_condo(cursor.getString(8));
				ppcr.setreport_remarks(cursor.getString(9));
				ppcrList.add(ppcr);
			} while (cursor.moveToNext());
		}
		db.close();
		// return ppcrList
		return ppcrList;
	}

	// Getting selected Construction Ebm
	public List<Construction_Ebm_API> getConstruction_Ebm(String record_id) {
		List<Construction_Ebm_API> construction_ebmList = new ArrayList<Construction_Ebm_API>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_construction_ebm
				+ " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				// page1
				Construction_Ebm_API ce = new Construction_Ebm_API();
				ce.setID(Integer.parseInt(cursor.getString(0)));
				ce.setrecord_id(cursor.getString(1));
				ce.setreport_date_inspected_month(cursor.getString(2));
				ce.setreport_date_inspected_day(cursor.getString(3));
				ce.setreport_date_inspected_year(cursor.getString(4));
				ce.setreport_time_inspected(cursor.getString(5));
				ce.setreport_project_type(cursor.getString(6));
				ce.setreport_floor_area(cursor.getString(7));
				ce.setreport_storeys(cursor.getString(8));
				ce.setreport_expected_economic_life(cursor.getString(9));
				ce.setreport_type_of_housing_unit(cursor.getString(10));
				ce.setreport_const_type_reinforced_concrete(cursor.getString(11));
				ce.setreport_const_type_semi_concrete(cursor.getString(12));
				ce.setreport_const_type_mixed_materials(cursor.getString(13));
				ce.setreport_foundation_concrete(cursor.getString(14));
				ce.setreport_foundation_other(cursor.getString(15));
				ce.setreport_foundation_other_value(cursor.getString(16));
				ce.setreport_post_concrete(cursor.getString(17));
				ce.setreport_post_concrete_timber(cursor.getString(18));
				ce.setreport_post_steel(cursor.getString(19));
				ce.setreport_post_other(cursor.getString(20));
				ce.setreport_post_other_value(cursor.getString(21));
				ce.setreport_beams_concrete(cursor.getString(22));
				ce.setreport_beams_timber(cursor.getString(23));
				ce.setreport_beams_steel(cursor.getString(24));
				ce.setreport_beams_other(cursor.getString(25));
				ce.setreport_beams_other_value(cursor.getString(26));
				ce.setreport_floors_concrete(cursor.getString(27));
				ce.setreport_floors_tiles(cursor.getString(28));
				ce.setreport_floors_tiles_cement(cursor.getString(29));
				ce.setreport_floors_laminated_wood(cursor.getString(30));
				ce.setreport_floors_ceramic_tiles(cursor.getString(31));
				ce.setreport_floors_wood_planks(cursor.getString(32));
				ce.setreport_floors_marble_washout(cursor.getString(33));
				ce.setreport_floors_concrete_boards(cursor.getString(34));
				ce.setreport_floors_granite_tiles(cursor.getString(35));
				ce.setreport_floors_marble_wood(cursor.getString(36));
				ce.setreport_floors_carpet(cursor.getString(37));
				ce.setreport_floors_other(cursor.getString(38));
				ce.setreport_floors_other_value(cursor.getString(39));
				ce.setreport_walls_chb(cursor.getString(40));
				ce.setreport_walls_chb_cement(cursor.getString(41));
				ce.setreport_walls_anay(cursor.getString(42));
				ce.setreport_walls_chb_wood(cursor.getString(43));
				ce.setreport_walls_precast(cursor.getString(44));
				ce.setreport_walls_decorative_stone(cursor.getString(45));
				ce.setreport_walls_adobe(cursor.getString(46));
				ce.setreport_walls_ceramic_tiles(cursor.getString(47));
				ce.setreport_walls_cast_in_place(cursor.getString(48));
				ce.setreport_walls_sandblast(cursor.getString(49));
				ce.setreport_walls_mactan_stone(cursor.getString(50));
				ce.setreport_walls_painted(cursor.getString(51));
				ce.setreport_walls_other(cursor.getString(52));
				ce.setreport_walls_other_value(cursor.getString(53));
				ce.setreport_partitions_chb(cursor.getString(54));
				ce.setreport_partitions_painted_cement(cursor.getString(55));
				ce.setreport_partitions_anay(cursor.getString(56));
				ce.setreport_partitions_wood_boards(cursor.getString(57));
				ce.setreport_partitions_precast(cursor.getString(58));
				ce.setreport_partitions_decorative_stone(cursor.getString(59));
				ce.setreport_partitions_adobe(cursor.getString(60));
				ce.setreport_partitions_granite(cursor.getString(61));
				ce.setreport_partitions_cast_in_place(cursor.getString(62));
				ce.setreport_partitions_sandblast(cursor.getString(63));
				ce.setreport_partitions_mactan_stone(cursor.getString(64));
				ce.setreport_partitions_ceramic_tiles(cursor.getString(65));
				ce.setreport_partitions_chb_plywood(cursor.getString(66));
				ce.setreport_partitions_hardiflex(cursor.getString(67));
				ce.setreport_partitions_wallpaper(cursor.getString(68));
				ce.setreport_partitions_painted(cursor.getString(69));
				ce.setreport_partitions_other(cursor.getString(70));
				ce.setreport_partitions_other_value(cursor.getString(71));
				ce.setreport_windows_steel_casement(cursor.getString(72));
				ce.setreport_windows_fixed_view(cursor.getString(73));
				ce.setreport_windows_analok_sliding(cursor.getString(74));
				ce.setreport_windows_alum_glass(cursor.getString(75));
				ce.setreport_windows_aluminum_sliding(cursor.getString(76));
				ce.setreport_windows_awning_type(cursor.getString(77));
				ce.setreport_windows_powder_coated(cursor.getString(78));
				ce.setreport_windows_wooden_frame(cursor.getString(79));
				ce.setreport_windows_other(cursor.getString(80));
				ce.setreport_windows_other_value(cursor.getString(81));
				ce.setreport_doors_wood_panel(cursor.getString(82));
				ce.setreport_doors_pvc(cursor.getString(83));
				ce.setreport_doors_analok_sliding(cursor.getString(84));
				ce.setreport_doors_screen_door(cursor.getString(85));
				ce.setreport_doors_flush(cursor.getString(86));
				ce.setreport_doors_molded_door(cursor.getString(87));
				ce.setreport_doors_aluminum_sliding(cursor.getString(88));
				ce.setreport_doors_flush_french(cursor.getString(89));
				ce.setreport_doors_other(cursor.getString(90));
				ce.setreport_doors_other_value(cursor.getString(91));
				ce.setreport_ceiling_plywood(cursor.getString(92));
				ce.setreport_ceiling_painted_gypsum(cursor.getString(93));
				ce.setreport_ceiling_soffit_slab(cursor.getString(94));
				ce.setreport_ceiling_metal_deck(cursor.getString(95));
				ce.setreport_ceiling_hardiflex(cursor.getString(96));
				ce.setreport_ceiling_plywood_tg(cursor.getString(97));
				ce.setreport_ceiling_plywood_pvc(cursor.getString(98));
				ce.setreport_ceiling_painted(cursor.getString(99));
				ce.setreport_ceiling_with_cornice(cursor.getString(100));
				ce.setreport_ceiling_with_moulding(cursor.getString(101));
				ce.setreport_ceiling_drop_ceiling(cursor.getString(102));
				ce.setreport_ceiling_other(cursor.getString(103));
				ce.setreport_ceiling_other_value(cursor.getString(104));
				ce.setreport_roof_pre_painted(cursor.getString(105));
				ce.setreport_roof_rib_type(cursor.getString(106));
				ce.setreport_roof_tilespan(cursor.getString(107));
				ce.setreport_roof_tegula_asphalt(cursor.getString(108));
				ce.setreport_roof_tegula_longspan(cursor.getString(109));
				ce.setreport_roof_tegula_gi(cursor.getString(110));
				ce.setreport_roof_steel_concrete(cursor.getString(111));
				ce.setreport_roof_polycarbonate(cursor.getString(112));
				ce.setreport_roof_on_steel_trusses(cursor.getString(113));
				ce.setreport_roof_on_wooden_trusses(cursor.getString(114));
				ce.setreport_roof_other(cursor.getString(115));
				ce.setreport_roof_other_value(cursor.getString(116));
				ce.setreport_valuation_total_area(cursor.getString(117));
				ce.setreport_valuation_total_proj_cost(cursor.getString(118));
				ce.setreport_valuation_remarks(cursor.getString(119));

				// Adding Construction_Ebm
				construction_ebmList.add(ce);
			} while (cursor.moveToNext());
		}
		db.close();
		// return cinstruction_ebmList
		return construction_ebmList;
	}

	// Getting selected report filename
	public List<Report_filename> getReport_filename(String record_id) {
		List<Report_filename> Report_filenameList = new ArrayList<Report_filename>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_report_filename + " WHERE "
				+ Report_record_id + " =  \"" + record_id + "\"";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Report_filename rf = new Report_filename();
				rf.setID(Integer.parseInt(cursor.getString(0)));
				rf.setrecord_id(cursor.getString(1));
				rf.setuid(cursor.getString(2));
				rf.setfile(cursor.getString(3));
				rf.setfilename(cursor.getString(4));
				rf.setappraisal_type(cursor.getString(5));
				rf.setcandidate_done(cursor.getString(6));
				// Adding images
				Report_filenameList.add(rf);
			} while (cursor.moveToNext());
		}
		db.close();
		// returnreport_accepted_jobsList contacts
		return Report_filenameList;
	}

	// Getting single request type movelast
	public Request_Type getRequest_Type_Last() {
		String query = "Select * FROM " + tbl_request_type;

		SQLiteDatabase db = this.getWritableDatabase();

		Cursor cursor = db.rawQuery(query, null);

		Request_Type request_type = new Request_Type();
		cursor.moveToLast();
		request_type.setID(Integer.parseInt(cursor.getString(0)));
		cursor.close();
		db.close();
		// return Image
		return request_type;
	}

	// images move to last
	public Images getImages_Last() {
		String query = "Select * FROM " + tbl_images;

		SQLiteDatabase db = this.getWritableDatabase();
		Images images = new Images();
		Cursor cursor = db.rawQuery(query, null);
		if (cursor.getCount() == 0) {
			images = null;
		} else {
			cursor.moveToLast();
			images.setID(Integer.parseInt(cursor.getString(0)));
		}
		cursor.close();
		db.close();
		// return Image
		return images;
	}

	// get single request_form
	public Request_Form getRequest_Form(int id) {
		String query = "Select * FROM " + tbl_request_form + " WHERE " + KEY_ID
				+ " =  \"" + id + "\"";

		SQLiteDatabase db = this.getWritableDatabase();

		Cursor cursor = db.rawQuery(query, null);

		Request_Form request_form = new Request_Form();

		if (cursor.moveToFirst()) {
			cursor.moveToFirst();
			request_form.setID(Integer.parseInt(cursor.getString(0)));
			request_form.setrf_date_requested_month(cursor.getString(1));
			request_form.setrf_date_requested_day(cursor.getString(2));
			request_form.setrf_date_requested_year(cursor.getString(3));
			request_form.setrf_classification(cursor.getString(4));
			request_form.setrf_account_fname(cursor.getString(5));
			request_form.setrf_account_mname(cursor.getString(6));
			request_form.setrf_account_lname(cursor.getString(7));
			request_form.setrf_requesting_party(cursor.getString(8));
			cursor.close();
		} else {
			request_form = null;
		}
		// return request_form
		db.close();
		return request_form;
	}

	// get single request_type
	public Request_Type getRequest_Type(int id) {
		String query = "Select * FROM " + tbl_request_type + " WHERE " + KEY_ID
				+ " =  \"" + id + "\"";

		SQLiteDatabase db = this.getWritableDatabase();

		Cursor cursor = db.rawQuery(query, null);

		Request_Type request_type = new Request_Type();

		if (cursor.moveToFirst()) {
			cursor.moveToFirst();
			request_type.setID(Integer.parseInt(cursor.getString(0)));
			request_type.setrt_control_no(cursor.getString(2));
			request_type.setrt_nature_of_appraisal(cursor.getString(3));
			request_type.setrt_appraisal_type(cursor.getString(4));
			request_type.setrt_pref_ins_date1_month(cursor.getString(5));
			request_type.setrt_pref_ins_date1_day(cursor.getString(6));
			request_type.setrt_pref_ins_date1_year(cursor.getString(7));
			request_type.setrt_pref_ins_date2_month(cursor.getString(8));
			request_type.setrt_pref_ins_date2_day(cursor.getString(9));
			request_type.setrt_pref_ins_date2_year(cursor.getString(10));
			request_type.setrt_mv_year(cursor.getString(11));
			request_type.setrt_mv_make(cursor.getString(12));
			request_type.setrt_mv_model(cursor.getString(13));
			request_type.setrt_mv_odometer(cursor.getString(14));
			request_type.setrt_mv_vin(cursor.getString(15));
			request_type.setrt_me_quantity(cursor.getString(16));
			request_type.setrt_me_type(cursor.getString(17));
			request_type.setrt_me_manufacturer(cursor.getString(18));
			request_type.setrt_me_model(cursor.getString(19));
			request_type.setrt_me_serial(cursor.getString(20));
			request_type.setrt_me_age(cursor.getString(21));
			request_type.setrt_me_condition(cursor.getString(22));
			cursor.close();
		} else {
			request_type = null;
		}
		// return request_type
		db.close();
		return request_type;
	}

	// get single collateral address
	public Request_Collateral_Address getCollateral_Address(int id) {
		String query = "Select * FROM " + tbl_request_collateral_address
				+ " WHERE " + KEY_rt_id + " =  \"" + id + "\"";

		SQLiteDatabase db = this.getWritableDatabase();

		Cursor cursor = db.rawQuery(query, null);

		Request_Collateral_Address collateral_address = new Request_Collateral_Address();

		if (cursor.moveToFirst()) {
			cursor.moveToFirst();
			collateral_address.setID(Integer.parseInt(cursor.getString(0)));
			collateral_address.setrt_id(cursor.getString(1));
			collateral_address.setrq_tct_no(cursor.getString(2));
			collateral_address.setrq_unit_no(cursor.getString(3));
			collateral_address.setrq_building_name(cursor.getString(4));
			collateral_address.setrq_street_no(cursor.getString(5));
			collateral_address.setrq_street_name(cursor.getString(6));
			collateral_address.setrq_village(cursor.getString(7));
			collateral_address.setrq_barangay(cursor.getString(8));
			collateral_address.setrq_zip_code(cursor.getString(9));
			collateral_address.setrq_city(cursor.getString(10));
			collateral_address.setrq_province(cursor.getString(11));
			collateral_address.setrq_region(cursor.getString(12));
			collateral_address.setrq_country(cursor.getString(13));
			cursor.close();
		} else {
			collateral_address = null;
		}
		// return collateral_address
		db.close();
		return collateral_address;
	}

	// Getting All request contact person
	public List<Request_Contact_Persons> getAllRequest_Contact_Persons(String id) {
		List<Request_Contact_Persons> request_contact_personsList = new ArrayList<Request_Contact_Persons>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_request_contact_persons
				+ " WHERE " + KEY_rt_id + " =  \"" + id + "\"";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Request_Contact_Persons request_contact_persons = new Request_Contact_Persons();
				request_contact_persons.setID(Integer.parseInt(cursor
						.getString(0)));
				request_contact_persons.setrt_id(cursor.getString(1));
				request_contact_persons.setrq_contact_person(cursor
						.getString(2));
				request_contact_persons.setrq_contact_mobile_no_prefix(cursor
						.getString(3));
				request_contact_persons.setrq_contact_mobile_no(cursor
						.getString(4));
				request_contact_persons.setrq_contact_landline(cursor
						.getString(5));
				request_contact_personsList.add(request_contact_persons);
			} while (cursor.moveToNext());
		}
		db.close();
		// return account list
		return request_contact_personsList;
	}

	// Getting All request attachments
	public List<Request_Attachments> getAllRequest_Attachments(String id) {
		List<Request_Attachments> request_attachmentsList = new ArrayList<Request_Attachments>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_request_attachments
				+ " WHERE " + KEY_rt_id + " =  \"" + id + "\"";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Request_Attachments request_attachments = new Request_Attachments();
				request_attachments
						.setID(Integer.parseInt(cursor.getString(0)));
				request_attachments.setrt_id(cursor.getString(1));
				request_attachments.setrq_uid(cursor.getString(2));
				request_attachments.setrq_file(cursor.getString(3));
				request_attachments.setrq_filename(cursor.getString(4));
				request_attachments.setrq_appraisal_type(cursor.getString(5));
				request_attachments.setrq_candidate_done(cursor.getString(6));
				request_attachmentsList.add(request_attachments);
			} while (cursor.moveToNext());
		}
		db.close();
		// return account list
		return request_attachmentsList;
	}

	// Updating single images table
	public void updateImages(String old_filename, String new_filename) {

		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_filename, new_filename);

		// updating row
		db.update(tbl_images, values, KEY_filename + " = ?",
				new String[] { old_filename });
		db.close();
	}

	// Deleting single images
	public int deleteImages(Images images) {
		SQLiteDatabase db = this.getWritableDatabase();
		// updating row
		return db.delete(tbl_images, KEY_filename + " = ?",
				new String[]{String.valueOf(images.getfilename())});
	}

	// Updating single Request form
	public int updateRequest_Form(Request_Form request_form, String id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_rf_date_requested_month,
				request_form.getrf_date_requested_month());
		values.put(KEY_rf_date_requested_day,
				request_form.getrf_date_requested_day());
		values.put(KEY_rf_date_requested_year,
				request_form.getrf_date_requested_year());
		values.put(KEY_rf_classification, request_form.getrf_classification());
		values.put(KEY_rf_account_fname, request_form.getrf_account_fname());
		values.put(KEY_rf_account_mname, request_form.getrf_account_mname());
		values.put(KEY_rf_account_lname, request_form.getrf_account_lname());
		values.put(KEY_rf_requesting_party,
				request_form.getrf_requesting_party());
		// updating row
		return db.update(tbl_request_form, values, KEY_ID + " = ?",
				new String[]{id});
	}

	// Updating single contact person edit
	public int updateContact_Person_edit(
			Request_Contact_Persons request_contact_persons, String id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_rq_contact_person,
				request_contact_persons.getrq_contact_person());
		values.put(KEY_rq_contact_mobile_no_prefix,
				request_contact_persons.getrq_contact_mobile_no_prefix());
		values.put(KEY_rq_contact_mobile_no,
				request_contact_persons.getrq_contact_mobile_no());
		values.put(KEY_rq_contact_landline,
				request_contact_persons.getrq_contact_landline());

		// updating row
		return db.update(tbl_request_contact_persons, values, KEY_ID + " = ?",
				new String[] { id });
	}

	// Updating single Request Type
	public int updateRequest_Type(Request_Type request_type, String id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_rt_control_no, request_type.getrt_control_no());
		values.put(KEY_rt_nature_of_appraisal,
				request_type.getrt_nature_of_appraisal());
		values.put(KEY_rt_appraisal_type, request_type.getrt_appraisal_type());
		values.put(KEY_rt_pref_ins_date1_month,
				request_type.getrt_pref_ins_date1_month());
		values.put(KEY_rt_pref_ins_date1_day,
				request_type.getrt_pref_ins_date1_day());
		values.put(KEY_rt_pref_ins_date1_year,
				request_type.getrt_pref_ins_date1_year());
		values.put(KEY_rt_pref_ins_date2_month,
				request_type.getrt_pref_ins_date2_month());
		values.put(KEY_rt_pref_ins_date2_day,
				request_type.getrt_pref_ins_date2_day());
		values.put(KEY_rt_pref_ins_date2_year,
				request_type.getrt_pref_ins_date2_year());
		values.put(KEY_rt_mv_year, request_type.getrt_mv_year());
		values.put(KEY_rt_mv_make, request_type.getrt_mv_make());
		values.put(KEY_rt_mv_model, request_type.getrt_mv_model());
		values.put(KEY_rt_mv_odometer, request_type.getrt_mv_odometer());
		values.put(KEY_rt_mv_vin, request_type.getrt_mv_vin());
		values.put(KEY_rt_me_quantity, request_type.getrt_me_quantity());
		values.put(KEY_rt_me_type, request_type.getrt_me_type());
		values.put(KEY_rt_me_manufacturer, request_type.getrt_me_manufacturer());
		values.put(KEY_rt_me_model, request_type.getrt_me_model());
		values.put(KEY_rt_me_serial, request_type.getrt_me_serial());
		values.put(KEY_rt_me_age, request_type.getrt_me_age());
		values.put(KEY_rt_me_condition, request_type.getrt_me_condition());

		// updating row
		return db.update(tbl_request_type, values, KEY_ID + " = ?",
				new String[] { id });
	}

	// Updating single Collateral Address
	public int updateRequest_Collateral_Address(
			Request_Collateral_Address collateral_address, String id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_rq_tct_no, collateral_address.getrq_tct_no());
		values.put(KEY_rq_unit_no, collateral_address.getrq_unit_no());
		values.put(KEY_rq_building_name,
				collateral_address.getrq_building_name());
		values.put(KEY_rq_street_no, collateral_address.getrq_street_no());
		values.put(KEY_rq_street_name, collateral_address.getrq_street_name());
		values.put(KEY_rq_village, collateral_address.getrq_village());
		values.put(KEY_rq_barangay, collateral_address.getrq_barangay());
		values.put(KEY_rq_zip_code, collateral_address.getrq_zip_code());
		values.put(KEY_rq_city, collateral_address.getrq_city());
		values.put(KEY_rq_province, collateral_address.getrq_province());
		values.put(KEY_rq_country, collateral_address.getrq_country());
		// updating row
		return db.update(tbl_request_collateral_address, values, KEY_ID
				+ " = ?", new String[] { id });
	}

	// Updating main Collateral Address
	public int updateCollateral_Address(
			Report_Accepted_Jobs ca, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Report_unit_no, ca.getunit_no());
		values.put(Report_building_name, ca.getbuilding_name());
		values.put(Report_lot_no, ca.getlot_no());
		values.put(Report_block_no, ca.getblock_no());
		values.put(Report_street_no, ca.getstreet_no());
		values.put(Report_street_name, ca.getstreet_name());
		values.put(Report_village, ca.getvillage());
		values.put(Report_district, ca.getdistrict());
		values.put(Report_zip_code, ca.getzip_code());
		values.put(Report_city, ca.getcity());
		values.put(Report_province, ca.getprovince());
		values.put(Report_region, ca.getregion());
		values.put(Report_country, ca.getcountry());
		// updating row
		return db.update(tbl_report_accepted_jobs, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating app_details
	public int updateApp_Details(
			Report_Accepted_Jobs app, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		/*values.put("dr_month", app.getdr_month());
		values.put("dr_day", app.getdr_day());
		values.put("dr_year", app.getdr_year());*/
		values.put("fname", app.getfname());
		values.put("mname", app.getmname());
		values.put("lname", app.getlname());
		values.put("requesting_party", app.getrequesting_party());
		values.put("requestor", app.getrequestor());

		// updating row
		return db.update(tbl_report_accepted_jobs, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating mv._details
	public int updateCar_Details(
			Report_Accepted_Jobs mv, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_car_loan_purpose, mv.getcar_loan_purpose());
		values.put(report_vehicle_type, mv.getvehicle_type());
		values.put(report_car_model, mv.getcar_model());
		values.put(report_car_mileage, mv.getcar_mileage());
		values.put(report_car_series, mv.getcar_series());
		values.put(report_car_color, mv.getcar_color());
		values.put(report_car_plate_no, mv.getcar_plate_no());
		values.put(report_car_body_type, mv.getcar_body_type());
		values.put(report_car_displacement, mv.getcar_displacement());
		values.put(report_car_motor_no, mv.getcar_motor_no());
		values.put(report_car_chassis_no, mv.getcar_chassis_no());
		values.put(report_car_no_cylinders, mv.getcar_no_cylinders());
		values.put(report_car_cr_no, mv.getcar_cr_no());
		values.put(report_car_cr_date_month, mv.getcar_cr_date_month());
		values.put(report_car_cr_date_day, mv.getcar_cr_date_day());
		values.put(report_car_cr_date_year, mv.getcar_cr_date_year());
		// updating row
		return db.update(tbl_report_accepted_jobs, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	
	// Deleting single Request form
	public int deleteRequest_Form(Request_Form request_form) {
		SQLiteDatabase db = this.getWritableDatabase();
		// updating row
		return db.delete(tbl_request_form, KEY_ID + " = ?",
				new String[] { String.valueOf(request_form.getID()) });
	}

	// Deleting all Request Type
	public int deleteRequest_Type(Request_Type request_type) {
		SQLiteDatabase db = this.getWritableDatabase();
		// updating row
		return db.delete(tbl_request_type, KEY_rf_id + " = ?",
				new String[] { String.valueOf(request_type.getID()) });
	}

	// Deleting all Collateral address
	public int deleteRequest_Collateral_Address(
			Request_Collateral_Address request_collateral_address) {
		SQLiteDatabase db = this.getWritableDatabase();
		// updating row
		return db.delete(tbl_request_collateral_address, KEY_rt_id + " = ?",
				new String[] { String.valueOf(request_collateral_address
						.getrt_id()) });
	}

	// Deleting all Contact Person
	public int deleteRequest_Contact_Persons(
			Request_Contact_Persons request_contact_persons) {
		SQLiteDatabase db = this.getWritableDatabase();
		// updating row
		return db.delete(tbl_request_contact_persons, KEY_rt_id + " = ?",
				new String[]{String.valueOf(request_contact_persons
						.getrt_id())});
	}

	// Deleting all Contact Person
	public int deleteRequest_Contact_Persons_edit(
			Request_Contact_Persons request_contact_persons) {
		SQLiteDatabase db = this.getWritableDatabase();
		// updating row
		return db
				.delete(tbl_request_contact_persons, KEY_ID + " = ?",
						new String[]{String.valueOf(request_contact_persons
								.getID())});
	}

	// Deleting single Request Type by id
	public int deleteRequest_Type_byID(Request_Type request_type) {
		SQLiteDatabase db = this.getWritableDatabase();
		// updating row
		return db.delete(tbl_request_type, KEY_ID + " = ?",
				new String[]{String.valueOf(request_type.getID())});
	}

	// Deleting single Request collateral address by id
	public int deleteRequest_Collateral_Address_byID(
			Request_Collateral_Address collateral_address) {
		SQLiteDatabase db = this.getWritableDatabase();
		// updating row
		return db.delete(tbl_request_collateral_address, KEY_ID + " = ?",
				new String[] { String.valueOf(collateral_address.getID()) });
	}

	// Getting All regions
	public List<Regions> getAllRegions() {
		List<Regions> regionsList = new ArrayList<Regions>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_regions;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Regions regions = new Regions();
				regions.setID(Integer.parseInt(cursor.getString(0)));
				regions.setname(cursor.getString(1));
				// Adding regions
				regionsList.add(regions);
			} while (cursor.moveToNext());
		}
		db.close();
		// return account list
		return regionsList;
	}

	// Getting All required attachments
	public List<Required_Attachments> getAllRequired_Attachments_byid(
			String appraisal_type) {
		List<Required_Attachments> required_attachmentsList = new ArrayList<Required_Attachments>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_required_attachments
				+ " WHERE " + KEY_appraisal_type + " =  \"" + appraisal_type
				+ "\"";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Required_Attachments required_attachments = new Required_Attachments();
				required_attachments
						.setID(Integer.parseInt(cursor.getString(0)));
				required_attachments.setattachment(cursor.getString(1));
				required_attachments.setremarks(cursor.getString(2));
				required_attachments.setappraisal_type(cursor.getString(3));
				required_attachmentsList.add(required_attachments);
			} while (cursor.moveToNext());
		}
		db.close();
		// return account list
		return required_attachmentsList;
	}
	
	// Adding new report_filename
	public void addAttachments(Attachments_API at) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Report_record_id, at.getrecord_id());
		values.put(report_uid, at.getuid());
		values.put(report_appraisal_type, at.getappraisal_type());
		values.put(report_file, at.getfile());
		values.put(report_file_name, at.getfile_name());
		// Inserting Row
		db.insert(tbl_attachments, null, values);
		db.close(); // Closing database connection
	}
	// Getting All required attachments
	public List<Attachments_API> getAll_Attachments(
			String record_id) {
		List<Attachments_API> attachmentsList = new ArrayList<Attachments_API>();
		// Select All Query
		String selectQuery = "Select * FROM " + tbl_attachments
				+ " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Attachments_API at = new Attachments_API();
				at.setID(Integer.parseInt(cursor.getString(0)));
				at.setrecord_id(cursor.getString(1));
				at.setuid(cursor.getString(2));
				at.setappraisal_type(cursor.getString(3));
				at.setfile(cursor.getString(4));
				at.setfile_name(cursor.getString(5));
				attachmentsList.add(at);
			} while (cursor.moveToNext());
		}
		db.close();
		// return account list
		return attachmentsList;
	}

	// Updating single Townhouse_condo page1
	public int updateTownhouse_Condo_page1(Townhouse_Condo_API townhouse_condo,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();

		values.put(report_cct_no, townhouse_condo.getreport_cct_no());
		values.put(report_area, townhouse_condo.getreport_area());
		values.put(report_date_reg_month,
				townhouse_condo.getreport_date_reg_month());
		values.put(report_date_reg_day,
				townhouse_condo.getreport_date_reg_day());
		values.put(report_date_reg_year,
				townhouse_condo.getreport_date_reg_year());
		values.put(report_reg_owner, townhouse_condo.getreport_reg_owner());
		values.put(report_homeowners_assoc,
				townhouse_condo.getreport_homeowners_assoc());
		values.put(report_admin_office,
				townhouse_condo.getreport_admin_office());
		values.put(report_tax_mapping, townhouse_condo.getreport_tax_mapping());
		values.put(report_lra_office, townhouse_condo.getreport_lra_office());
		values.put(report_subd_map, townhouse_condo.getreport_subd_map());
		values.put(report_lot_config, townhouse_condo.getreport_lot_config());
		values.put(report_street, townhouse_condo.getreport_street());
		values.put(report_sidewalk, townhouse_condo.getreport_sidewalk());
		values.put(report_gutter, townhouse_condo.getreport_gutter());
		values.put(report_street_lights,
				townhouse_condo.getreport_street_lights());
		values.put(report_date_inspected_month,
				townhouse_condo.getreport_date_inspected_month());
		values.put(report_date_inspected_day,
				townhouse_condo.getreport_date_inspected_day());
		values.put(report_date_inspected_year,
				townhouse_condo.getreport_date_inspected_year());
		// updating row
		return db.update(tbl_townhouse_condo, values,
				Report_record_id + " = ?", new String[] { record_id });
	}

	// Updating single Townhouse_condo page2
	public int updateTownhouse_Condo_page2(Townhouse_Condo_API townhouse_condo,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_shape, townhouse_condo.getreport_shape());
		values.put(report_elevation, townhouse_condo.getreport_elevation());
		values.put(report_road, townhouse_condo.getreport_road());
		values.put(report_frontage, townhouse_condo.getreport_frontage());
		values.put(report_depth, townhouse_condo.getreport_depth());
		values.put(report_electricity, townhouse_condo.getreport_electricity());
		values.put(report_water, townhouse_condo.getreport_water());
		values.put(report_telephone, townhouse_condo.getreport_telephone());
		values.put(report_drainage, townhouse_condo.getreport_drainage());
		values.put(report_garbage, townhouse_condo.getreport_garbage());
		values.put(report_jeepneys, townhouse_condo.getreport_jeepneys());
		values.put(report_bus, townhouse_condo.getreport_bus());
		values.put(report_taxi, townhouse_condo.getreport_taxi());
		values.put(report_tricycle, townhouse_condo.getreport_tricycle());
		values.put(report_recreation_facilities,
				townhouse_condo.getreport_recreation_facilities());
		values.put(report_security, townhouse_condo.getreport_security());
		values.put(report_clubhouse, townhouse_condo.getreport_clubhouse());
		values.put(report_swimming_pool,
				townhouse_condo.getreport_swimming_pool());

		// updating row
		return db.update(tbl_townhouse_condo, values,
				Report_record_id + " = ?", new String[] { record_id });
	}

	// Updating single Townhouse_condo page3
	public int updateTownhouse_Condo_page3(Townhouse_Condo_API townhouse_condo,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_community, townhouse_condo.getreport_community());
		values.put(report_classification,
				townhouse_condo.getreport_classification());
		values.put(report_growth_rate, townhouse_condo.getreport_growth_rate());
		values.put(report_adverse, townhouse_condo.getreport_adverse());
		values.put(report_occupancy, townhouse_condo.getreport_occupancy());
		values.put(report_right, townhouse_condo.getreport_right());
		values.put(report_left, townhouse_condo.getreport_left());
		values.put(report_rear, townhouse_condo.getreport_rear());
		values.put(report_front, townhouse_condo.getreport_front());
		values.put(report_desc, townhouse_condo.getreport_desc());
		values.put(report_total_area, townhouse_condo.getreport_total_area());
		values.put(report_deduc, townhouse_condo.getreport_deduc());
		values.put(report_net_area, townhouse_condo.getreport_net_area());
		values.put(report_unit_value, townhouse_condo.getreport_unit_value());
		values.put(report_total_parking_area,
				townhouse_condo.getreport_total_parking_area());
		values.put(report_parking_area_value,
				townhouse_condo.getreport_parking_area_value());
		values.put(report_total_condo_value,
				townhouse_condo.getreport_total_condo_value());
		values.put(report_total_parking_value,
				townhouse_condo.getreport_total_parking_value());
		values.put(report_total_market_value,
				townhouse_condo.getreport_total_market_value());
		// updating row
		return db.update(tbl_townhouse_condo, values,
				Report_record_id + " = ?", new String[] { record_id });
	}

	// Updating single MV
	public int updateMotor_Vehicle_page1(Motor_Vehicle_API mv, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_interior_dashboard, mv.getreport_interior_dashboard());
		values.put(report_interior_sidings, mv.getreport_interior_sidings());
		values.put(report_interior_seats, mv.getreport_interior_seats());
		values.put(report_interior_windows, mv.getreport_interior_windows());
		values.put(report_interior_ceiling, mv.getreport_interior_ceiling());
		values.put(report_interior_instrument, mv.getreport_interior_instrument());
		values.put(report_bodytype_grille, mv.getreport_bodytype_grille());
		values.put(report_bodytype_headlight, mv.getreport_bodytype_headlight());
		values.put(report_bodytype_fbumper, mv.getreport_bodytype_fbumper());
		values.put(report_bodytype_hood, mv.getreport_bodytype_hood());
		values.put(report_bodytype_rf_fender, mv.getreport_bodytype_rf_fender());
		values.put(report_bodytype_rf_door, mv.getreport_bodytype_rf_door());
		values.put(report_bodytype_rr_door, mv.getreport_bodytype_rr_door());
		values.put(report_bodytype_rr_fender, mv.getreport_bodytype_rr_fender());
		values.put(report_bodytype_backdoor, mv.getreport_bodytype_backdoor());
		values.put(report_bodytype_taillight, mv.getreport_bodytype_taillight());
		values.put(report_bodytype_r_bumper, mv.getreport_bodytype_r_bumper());
		values.put(report_bodytype_lr_fender, mv.getreport_bodytype_lr_fender());
		values.put(report_bodytype_lr_door, mv.getreport_bodytype_lr_door());
		values.put(report_bodytype_lf_door, mv.getreport_bodytype_lf_door());
		values.put(report_bodytype_lf_fender, mv.getreport_bodytype_lf_fender());
		values.put(report_bodytype_top, mv.getreport_bodytype_top());
		values.put(report_bodytype_paint, mv.getreport_bodytype_paint());
		values.put(report_bodytype_flooring, mv.getreport_bodytype_flooring());
		values.put(report_misc_stereo, mv.getreport_misc_stereo());
		values.put(report_misc_tools, mv.getreport_misc_tools());
		values.put(report_misc_speakers, mv.getreport_misc_speakers());
		values.put(report_misc_airbag, mv.getreport_misc_airbag());
		values.put(report_misc_tires, mv.getreport_misc_tires());
		values.put(report_misc_mag_wheels, mv.getreport_misc_mag_wheels());
		values.put(report_misc_carpet, mv.getreport_misc_carpet());
		values.put(report_misc_others, mv.getreport_misc_others());
		values.put(report_enginearea_fuel, mv.getreport_enginearea_fuel());
		values.put(report_enginearea_transmission1, mv.getreport_enginearea_transmission1());
		values.put(report_enginearea_transmission2, mv.getreport_enginearea_transmission2());
		values.put(report_enginearea_chassis, mv.getreport_enginearea_chassis());
		values.put(report_enginearea_battery, mv.getreport_enginearea_battery());
		values.put(report_enginearea_electrical, mv.getreport_enginearea_electrical());
		values.put(report_enginearea_engine, mv.getreport_enginearea_engine());
		values.put(report_date_inspected_month, mv.getreport_date_inspected_month());
		values.put(report_date_inspected_day, mv.getreport_date_inspected_day());
		values.put(report_date_inspected_year, mv.getreport_date_inspected_year());
		values.put(report_time_inspected, mv.getreport_time_inspected());
		values.put(valrep_mv_bodytype_trunk, mv.getvalrep_mv_bodytype_trunk());
		// updating row
		return db.update(tbl_motor_vehicle, values, Report_record_id + " = ?",
				new String[] { record_id });
	}
	// Updating single MV
	public int updateMotor_Vehicle_page2(Motor_Vehicle_API mv, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(report_verification, mv.getreport_verification());
		values.put(report_verification_district_office, mv.getreport_verification_district_office());
		values.put(report_verification_encumbrance, mv.getreport_verification_encumbrance());
		values.put(report_verification_registered_owner, mv.getreport_verification_registered_owner());
		values.put(report_place_inspected, mv.getreport_place_inspected());
		values.put(report_market_valuation_fair_value, mv.getreport_market_valuation_fair_value());
		values.put(report_market_valuation_min, mv.getreport_market_valuation_min());
		values.put(report_market_valuation_max, mv.getreport_market_valuation_max());
		values.put(report_market_valuation_remarks, mv.getreport_market_valuation_remarks());

		values.put(report_verification_file_no, mv.getreport_verification_file_no());
		values.put(report_verification_first_reg_date_month, mv.getreport_verification_first_reg_date_month());
		values.put(report_verification_first_reg_date_day, mv.getreport_verification_first_reg_date_day());
		values.put(report_verification_first_reg_date_year, mv.getreport_verification_first_reg_date_year());
		values.put(valrep_mv_market_valuation_previous_remarks, mv.getvalrep_mv_market_valuation_previous_remarks());
		//ADDED By IAN

		// updating row
		return db.update(tbl_motor_vehicle, values, Report_record_id + " = ?",
				new String[] { record_id });
	}
	// Updating single MV
	public int updateMotor_Vehicle_MinMax(Motor_Vehicle_API mv, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(report_market_valuation_min, mv.getreport_market_valuation_min());
		values.put(report_market_valuation_max, mv.getreport_market_valuation_max());
		
		// updating row
		return db.update(tbl_motor_vehicle, values, Report_record_id + " = ?",
				new String[] { record_id });
	}

	// Updating single report filename
	public int updateReport_filename(Report_filename rf, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_uid, rf.getuid());
		values.put(report_file, rf.getfile());
		values.put(report_filename, rf.getfilename());
		values.put(report_appraisal_type, rf.getappraisal_type());
		values.put(report_candidate_done, rf.getcandidate_done());

		// updating row
		return db.update(tbl_report_filename, values,
				Report_record_id + " = ?", new String[] { record_id });
	}
	
/**
 * updating of Townhouse
 */
	
	// Updating single land improvements 2 Lot Details
	public int updateTownhouse_page2(Townhouse_API li,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();

		values.put(report_date_inspected_month,li.getreport_date_inspected_month());
		values.put(report_date_inspected_day, li.getreport_date_inspected_day());
		values.put(report_date_inspected_year,li.getreport_date_inspected_year());
		values.put(report_id_association, li.getreport_id_association());
		values.put(report_id_tax, li.getreport_id_tax());
		values.put(report_id_lra, li.getreport_id_lra());
		values.put(report_id_lot_config, li.getreport_id_lot_config());
		values.put(report_id_subd_map, li.getreport_id_subd_map());
		values.put(report_id_nbrhood_checking, li.getreport_id_nbrhood_checking());
		values.put(report_imp_street_name, li.getreport_imp_street_name());
		values.put(report_imp_street, li.getreport_imp_street());
		values.put(report_imp_sidewalk, li.getreport_imp_sidewalk());
		values.put(report_imp_curb, li.getreport_imp_curb());
		values.put(report_imp_drainage, li.getreport_imp_drainage());
		values.put(report_imp_street_lights, li.getreport_imp_street_lights());
		values.put(report_physical_corner_lot, li.getreport_physical_corner_lot());
		values.put(report_physical_non_corner_lot, li.getreport_physical_non_corner_lot());
		values.put(report_physical_perimeter_lot, li.getreport_physical_perimeter_lot());
		values.put(report_physical_intersected_lot, li.getreport_physical_intersected_lot());
		values.put(report_physical_interior_with_row, li.getreport_physical_interior_with_row());
		values.put(report_physical_landlocked, li.getreport_physical_landlocked());
		values.put(report_lotclass_per_tax_dec, li.getreport_lotclass_per_tax_dec());
		values.put(report_lotclass_actual_usage, li.getreport_lotclass_actual_usage());
		values.put(report_lotclass_neighborhood, li.getreport_lotclass_neighborhood());
		values.put(report_lotclass_highest_best_use, li.getreport_lotclass_highest_best_use());
		values.put(report_physical_shape, li.getreport_physical_shape());
		values.put(report_physical_frontage, li.getreport_physical_frontage());
		values.put(report_physical_depth, li.getreport_physical_depth());
		values.put(report_physical_road, li.getreport_physical_road());
		values.put(report_physical_elevation, li.getreport_physical_elevation());
		values.put(report_physical_terrain, li.getreport_physical_terrain());
		values.put(report_landmark_1, li.getreport_landmark_1());
		values.put(report_landmark_2, li.getreport_landmark_2());
		values.put(report_landmark_3, li.getreport_landmark_3());
		values.put(report_landmark_4, li.getreport_landmark_4());
		values.put(report_landmark_5, li.getreport_landmark_5());
		values.put(report_distance_1, li.getreport_distance_1());
		values.put(report_distance_2, li.getreport_distance_2());
		values.put(report_distance_3, li.getreport_distance_3());
		values.put(report_distance_4, li.getreport_distance_4());
		values.put(report_distance_5, li.getreport_distance_5());
		values.put(report_util_electricity, li.getreport_util_electricity());
		values.put(report_util_water, li.getreport_util_water());
		values.put(report_util_telephone, li.getreport_util_telephone());
		values.put(report_util_garbage, li.getreport_util_garbage());
		values.put(report_trans_jeep, li.getreport_trans_jeep());
		values.put(report_trans_bus, li.getreport_trans_bus());
		values.put(report_trans_taxi, li.getreport_trans_taxi());
		values.put(report_trans_tricycle, li.getreport_trans_tricycle());
		values.put(report_faci_recreation, li.getreport_faci_recreation());
		values.put(report_faci_security, li.getreport_faci_security());
		values.put(report_faci_clubhouse, li.getreport_faci_clubhouse());
		values.put(report_faci_pool, li.getreport_faci_pool());
		values.put(report_bound_right, li.getreport_bound_right());
		values.put(report_bound_left, li.getreport_bound_left());
		values.put(report_bound_rear, li.getreport_bound_rear());
		values.put(report_bound_front, li.getreport_bound_front());
		values.put(report_time_inspected, li.getreport_time_inspected());
		//ADDED BY Ian
		values.put(report_prev_date, li.getreport_prev_date());
		values.put(report_prev_appraiser, li.getreport_prev_appraiser());
		// updating row
		return db.update(tbl_townhouse, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating single land improvements 2 Lot Details
	public int updateTownhouse_page2_StreetName(Townhouse_API li,
									 String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();


		values.put(report_imp_street_name, li.getreport_imp_street_name());

		// updating row
		return db.update(tbl_townhouse, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating single land imp comp1
	public int updateTownhouse_comp1(Townhouse_API li, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp1_date_month, li.getreport_comp1_date_month());
		values.put(report_comp1_date_day, li.getreport_comp1_date_day());
		values.put(report_comp1_date_year, li.getreport_comp1_date_year());
		values.put(report_comp1_source, li.getreport_comp1_source());
		values.put(report_comp1_contact_no, li.getreport_comp1_contact_no());
		values.put(report_comp1_location, li.getreport_comp1_location()); 
		values.put(report_comp1_lot_area, li.getreport_comp1_lot_area());
		values.put(report_comp1_area, li.getreport_comp1_area());
		values.put(report_comp1_base_price, li.getreport_comp1_base_price());
		values.put(report_comp1_price_sqm, li.getreport_comp1_price_sqm());
		values.put(report_comp1_discount_rate, li.getreport_comp1_discount_rate());
		values.put(report_comp1_discounts, li.getreport_comp1_discounts());
		values.put(report_comp1_selling_price, li.getreport_comp1_selling_price());
		values.put(report_comp1_rec_location, li.getreport_comp1_rec_location());
		values.put(report_comp1_rec_lot_size, li.getreport_comp1_rec_lot_size());
		values.put(report_comp1_rec_floor_area, li.getreport_comp1_rec_floor_area());
		values.put(report_comp1_rec_unit_condition, li.getreport_comp1_rec_unit_condition());
		values.put(report_comp1_rec_unit_features, li.getreport_comp1_rec_unit_features());
		values.put(report_comp1_rec_degree_of_devt, li.getreport_comp1_rec_degree_of_devt());
		values.put(report_comp1_rec_corner_influence, li.getreport_comp1_rec_corner_influence());
		values.put(report_comp1_concluded_adjustment, li.getreport_comp1_concluded_adjustment());
		values.put(report_comp1_adjusted_value, li.getreport_comp1_adjusted_value());
		values.put(report_comp1_remarks, li.getreport_comp1_remarks());
		// updating row
		return db.update(tbl_townhouse, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	
	// Updating single land imp comp2
	public int updateTownhouse_comp2(Townhouse_API li, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp2_date_month, li.getreport_comp2_date_month());
		values.put(report_comp2_date_day, li.getreport_comp2_date_day());
		values.put(report_comp2_date_year, li.getreport_comp2_date_year());
		values.put(report_comp2_source, li.getreport_comp2_source());
		values.put(report_comp2_contact_no, li.getreport_comp2_contact_no());
		values.put(report_comp2_location, li.getreport_comp2_location()); 
		values.put(report_comp2_lot_area, li.getreport_comp2_lot_area());
		values.put(report_comp2_area, li.getreport_comp2_area());
		values.put(report_comp2_base_price, li.getreport_comp2_base_price());
		values.put(report_comp2_price_sqm, li.getreport_comp2_price_sqm());
		values.put(report_comp2_discount_rate, li.getreport_comp2_discount_rate());
		values.put(report_comp2_discounts, li.getreport_comp2_discounts());
		values.put(report_comp2_selling_price, li.getreport_comp2_selling_price());
		values.put(report_comp2_rec_location, li.getreport_comp2_rec_location());
		values.put(report_comp2_rec_lot_size, li.getreport_comp2_rec_lot_size());
		values.put(report_comp2_rec_floor_area, li.getreport_comp2_rec_floor_area());
		values.put(report_comp2_rec_unit_condition, li.getreport_comp2_rec_unit_condition());
		values.put(report_comp2_rec_unit_features, li.getreport_comp2_rec_unit_features());
		values.put(report_comp2_rec_degree_of_devt, li.getreport_comp2_rec_degree_of_devt());
		values.put(report_comp2_rec_corner_influence, li.getreport_comp2_rec_corner_influence());
		values.put(report_comp2_concluded_adjustment, li.getreport_comp2_concluded_adjustment());
		values.put(report_comp2_adjusted_value, li.getreport_comp2_adjusted_value());
		values.put(report_comp2_remarks, li.getreport_comp2_remarks());
		// updating row
		return db.update(tbl_townhouse, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	
	// Updating single land imp comp3
	public int updateTownhouse_comp3(Townhouse_API li, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp3_date_month, li.getreport_comp3_date_month());
		values.put(report_comp3_date_day, li.getreport_comp3_date_day());
		values.put(report_comp3_date_year, li.getreport_comp3_date_year());
		values.put(report_comp3_source, li.getreport_comp3_source());
		values.put(report_comp3_contact_no, li.getreport_comp3_contact_no());
		values.put(report_comp3_location, li.getreport_comp3_location()); 
		values.put(report_comp3_lot_area, li.getreport_comp3_lot_area());
		values.put(report_comp3_area, li.getreport_comp3_area());
		values.put(report_comp3_base_price, li.getreport_comp3_base_price());
		values.put(report_comp3_price_sqm, li.getreport_comp3_price_sqm());
		values.put(report_comp3_discount_rate, li.getreport_comp3_discount_rate());
		values.put(report_comp3_discounts, li.getreport_comp3_discounts());
		values.put(report_comp3_selling_price, li.getreport_comp3_selling_price());
		values.put(report_comp3_rec_location, li.getreport_comp3_rec_location());
		values.put(report_comp3_rec_lot_size, li.getreport_comp3_rec_lot_size());
		values.put(report_comp3_rec_floor_area, li.getreport_comp3_rec_floor_area());
		values.put(report_comp3_rec_unit_condition, li.getreport_comp3_rec_unit_condition());
		values.put(report_comp3_rec_unit_features, li.getreport_comp3_rec_unit_features());
		values.put(report_comp3_rec_degree_of_devt, li.getreport_comp3_rec_degree_of_devt());
		values.put(report_comp3_rec_corner_influence, li.getreport_comp3_rec_corner_influence());
		values.put(report_comp3_concluded_adjustment, li.getreport_comp3_concluded_adjustment());
		values.put(report_comp3_adjusted_value, li.getreport_comp3_adjusted_value());
		values.put(report_comp3_remarks, li.getreport_comp3_remarks());
		// updating row
		return db.update(tbl_townhouse, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	
	// Updating single land imp comp4
	public int updateTownhouse_comp4(Townhouse_API li, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp4_date_month, li.getreport_comp4_date_month());
		values.put(report_comp4_date_day, li.getreport_comp4_date_day());
		values.put(report_comp4_date_year, li.getreport_comp4_date_year());
		values.put(report_comp4_source, li.getreport_comp4_source());
		values.put(report_comp4_contact_no, li.getreport_comp4_contact_no());
		values.put(report_comp4_location, li.getreport_comp4_location()); 
		values.put(report_comp4_lot_area, li.getreport_comp4_lot_area());
		values.put(report_comp4_area, li.getreport_comp4_area());
		values.put(report_comp4_base_price, li.getreport_comp4_base_price());
		values.put(report_comp4_price_sqm, li.getreport_comp4_price_sqm());
		values.put(report_comp4_discount_rate, li.getreport_comp4_discount_rate());
		values.put(report_comp4_discounts, li.getreport_comp4_discounts());
		values.put(report_comp4_selling_price, li.getreport_comp4_selling_price());
		values.put(report_comp4_rec_location, li.getreport_comp4_rec_location());
		values.put(report_comp4_rec_lot_size, li.getreport_comp4_rec_lot_size());
		values.put(report_comp4_rec_floor_area, li.getreport_comp4_rec_floor_area());
		values.put(report_comp4_rec_unit_condition, li.getreport_comp4_rec_unit_condition());
		values.put(report_comp4_rec_unit_features, li.getreport_comp4_rec_unit_features());
		values.put(report_comp4_rec_degree_of_devt, li.getreport_comp4_rec_degree_of_devt());
		values.put(report_comp4_rec_corner_influence, li.getreport_comp4_rec_corner_influence());
		values.put(report_comp4_concluded_adjustment, li.getreport_comp4_concluded_adjustment());
		values.put(report_comp4_adjusted_value, li.getreport_comp4_adjusted_value());
		values.put(report_comp4_remarks, li.getreport_comp4_remarks());
		// updating row
		return db.update(tbl_townhouse, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	
	// Updating single land imp comp5
	public int updateTownhouse_comp5(Townhouse_API li, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp5_date_month, li.getreport_comp5_date_month());
		values.put(report_comp5_date_day, li.getreport_comp5_date_day());
		values.put(report_comp5_date_year, li.getreport_comp5_date_year());
		values.put(report_comp5_source, li.getreport_comp5_source());
		values.put(report_comp5_contact_no, li.getreport_comp5_contact_no());
		values.put(report_comp5_location, li.getreport_comp5_location()); 
		values.put(report_comp5_lot_area, li.getreport_comp5_lot_area());
		values.put(report_comp5_area, li.getreport_comp5_area());
		values.put(report_comp5_base_price, li.getreport_comp5_base_price());
		values.put(report_comp5_price_sqm, li.getreport_comp5_price_sqm());
		values.put(report_comp5_discount_rate, li.getreport_comp5_discount_rate());
		values.put(report_comp5_discounts, li.getreport_comp5_discounts());
		values.put(report_comp5_selling_price, li.getreport_comp5_selling_price());
		values.put(report_comp5_rec_location, li.getreport_comp5_rec_location());
		values.put(report_comp5_rec_lot_size, li.getreport_comp5_rec_lot_size());
		values.put(report_comp5_rec_floor_area, li.getreport_comp5_rec_floor_area());
		values.put(report_comp5_rec_unit_condition, li.getreport_comp5_rec_unit_condition());
		values.put(report_comp5_rec_unit_features, li.getreport_comp5_rec_unit_features());
		values.put(report_comp5_rec_degree_of_devt, li.getreport_comp5_rec_degree_of_devt());
		values.put(report_comp5_rec_corner_influence, li.getreport_comp5_rec_corner_influence());
		values.put(report_comp5_concluded_adjustment, li.getreport_comp5_concluded_adjustment());
		values.put(report_comp5_adjusted_value, li.getreport_comp5_adjusted_value());
		values.put(report_comp5_remarks, li.getreport_comp5_remarks());
		// updating row
		return db.update(tbl_townhouse, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	
	// Updating single townhouse comparatives
	public int updateTownhouse_comparatives(Townhouse_API vl, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp_average, vl.getreport_comp_average());
		values.put(report_comp_rounded_to, vl.getreport_comp_rounded_to());
		// updating row
		return db.update(tbl_townhouse, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	
	// Updating townhouse report summary
	public int updateTownhouse_Summary(Townhouse_API li,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_final_value_total_appraised_value_land_imp, li.getreport_final_value_total_appraised_value_land_imp());
		values.put(report_final_value_recommended_deductions_desc, li.getreport_final_value_recommended_deductions_desc());
		values.put(report_final_value_recommended_deductions_rate, li.getreport_final_value_recommended_deductions_rate());
		values.put(report_final_value_recommended_deductions_other, li.getreport_final_value_recommended_deductions_other());
		values.put(report_final_value_recommended_deductions_amt, li.getreport_final_value_recommended_deductions_amt());
		values.put(report_final_value_net_appraised_value, li.getreport_final_value_net_appraised_value());
		values.put(report_final_value_quick_sale_value_rate, li.getreport_final_value_quick_sale_value_rate());
		values.put(report_final_value_quick_sale_value_rate, li.getreport_final_value_quick_sale_value_rate());
		values.put(report_final_value_quick_sale_value_amt, li.getreport_final_value_quick_sale_value_amt());
		values.put(report_factors_of_concern, li.getreport_factors_of_concern());
		values.put(report_suggested_corrective_actions, li.getreport_suggested_corrective_actions());
		values.put(report_remarks, li.getreport_remarks());
		values.put(report_requirements, li.getreport_requirements());
		values.put(report_concerns_minor_1, li.getreport_concerns_minor_1());
		values.put(report_concerns_minor_2, li.getreport_concerns_minor_2());
		values.put(report_concerns_minor_3, li.getreport_concerns_minor_3());
		values.put(report_concerns_minor_others, li.getreport_concerns_minor_others());
		values.put(report_concerns_minor_others_desc, li.getreport_concerns_minor_others_desc());
		values.put(report_concerns_major_1, li.getreport_concerns_major_1());
		values.put(report_concerns_major_2, li.getreport_concerns_major_2());
		values.put(report_concerns_major_3, li.getreport_concerns_major_3());
		values.put(report_concerns_major_4, li.getreport_concerns_major_4());
		values.put(report_concerns_major_5, li.getreport_concerns_major_5());
		values.put(report_concerns_major_6, li.getreport_concerns_major_6());
		values.put(report_concerns_major_others, li.getreport_concerns_major_others());
		values.put(report_concerns_major_others_desc, li.getreport_concerns_major_others_desc());
		
		// updating row
		return db.update(tbl_townhouse, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	
	// Updating townhouse report summary
	public int updateTownhouse_Summary_Total(Townhouse_API li,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(report_final_value_total_appraised_value_land_imp, li.getreport_final_value_total_appraised_value_land_imp());
		return db.update(tbl_townhouse, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	
	// Update total valuation
	public int updateTownhouse_Total_Valuation(Townhouse_API li,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_value_total_area, li.getreport_value_total_area());
		values.put(report_value_total_net_area, li.getreport_value_total_net_area());
		values.put(report_value_total_landimp_value, li.getreport_value_total_landimp_value());
		
		// updating row
		return db.update(tbl_townhouse, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	
	// Update total valuation cost total
	public int updateTownhouse_Total_Valuation_Main(Townhouse_API li,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_imp_value_total_imp_value, li.getreport_imp_value_total_imp_value());
		// updating row
		return db.update(tbl_townhouse, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	
	// Updating land imp Zonal Values
	public int updateTownhouse_Zonal(Townhouse_API li,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_zonal_location, li.getreport_zonal_location());
		values.put(report_zonal_lot_classification, li.getreport_zonal_lot_classification());
		values.put(report_zonal_value, li.getreport_zonal_value());
		
		// updating row
		return db.update(tbl_townhouse, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
/**
* updating of condo page2
*/	
	// Updating single condo page2
	public int updateCondo_page2(Condo_API condo, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_date_inspected_month,
				condo.getreport_date_inspected_month());
		values.put(report_date_inspected_day,
				condo.getreport_date_inspected_day());
		values.put(report_date_inspected_year,
				condo.getreport_date_inspected_year());
		values.put(report_id_admin, condo.getreport_id_admin());
		values.put(report_id_unit_numbering,
				condo.getreport_id_unit_numbering());
		values.put(report_id_bldg_plan, condo.getreport_id_bldg_plan());
		values.put(report_unitclass_per_tax_dec,
				condo.getreport_unitclass_per_tax_dec());
		values.put(report_unitclass_actual_usage,
				condo.getreport_unitclass_actual_usage());
		values.put(report_unitclass_neighborhood,
				condo.getreport_unitclass_neighborhood());
		values.put(report_unitclass_highest_best_use,
				condo.getreport_unitclass_highest_best_use());
		values.put(report_trans_jeep, condo.getreport_trans_jeep());
		values.put(report_trans_bus, condo.getreport_trans_bus());
		values.put(report_trans_taxi, condo.getreport_trans_taxi());
		values.put(report_trans_tricycle, condo.getreport_trans_tricycle());
		values.put(report_faci_function_room,
				condo.getreport_faci_function_room());
		values.put(report_faci_gym, condo.getreport_faci_gym());
		values.put(report_faci_sauna, condo.getreport_faci_sauna());
		values.put(report_faci_pool, condo.getreport_faci_pool());
		values.put(report_faci_fire_alarm, condo.getreport_faci_fire_alarm());
		values.put(report_faci_cctv, condo.getreport_faci_cctv());
		values.put(report_faci_elevator, condo.getreport_faci_elevator());
		values.put(report_util_electricity, condo.getreport_util_electricity());
		values.put(report_util_water, condo.getreport_util_water());
		values.put(report_util_telephone, condo.getreport_util_telephone());
		values.put(report_util_garbage, condo.getreport_util_garbage());
		values.put(report_landmarks_1, condo.getreport_landmarks_1());
		values.put(report_landmarks_2, condo.getreport_landmarks_2());
		values.put(report_landmarks_3, condo.getreport_landmarks_3());
		values.put(report_landmarks_4, condo.getreport_landmarks_4());
		values.put(report_landmarks_5, condo.getreport_landmarks_5());
		values.put(report_distance_1, condo.getreport_distance_1());
		values.put(report_distance_2, condo.getreport_distance_2());
		values.put(report_distance_3, condo.getreport_distance_3());
		values.put(report_distance_4, condo.getreport_distance_4());
		values.put(report_distance_5, condo.getreport_distance_5());
		values.put(report_time_inspected, condo.getreport_time_inspected());

		//ADDED BY Ian
		values.put(report_prev_date, condo.getreport_prev_date());
		values.put(report_prev_appraiser, condo.getreport_prev_appraiser());
		// updating row
		return db.update(tbl_condo, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}

	// Updating single condo comp1
	public int updateCondo_comp1(Condo_API vl, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp1_date_month, vl.getreport_comp1_date_month());
		values.put(report_comp1_date_day, vl.getreport_comp1_date_day());
		values.put(report_comp1_date_year, vl.getreport_comp1_date_year());
		values.put(report_comp1_source, vl.getreport_comp1_source());
		values.put(report_comp1_contact_no, vl.getreport_comp1_contact_no());
		values.put(report_comp1_location, vl.getreport_comp1_location()); 
		values.put(report_comp1_area, vl.getreport_comp1_area());
		values.put(report_comp1_base_price, vl.getreport_comp1_base_price());
		values.put(report_comp1_price_sqm, vl.getreport_comp1_price_sqm());
		values.put(report_comp1_discount_rate, vl.getreport_comp1_discount_rate());
		values.put(report_comp1_discounts, vl.getreport_comp1_discounts());
		values.put(report_comp1_selling_price, vl.getreport_comp1_selling_price());
		values.put(report_comp1_rec_location, vl.getreport_comp1_rec_location());
		values.put(report_comp1_rec_unit_floor_location, vl.getreport_comp1_rec_unit_floor_location());
		values.put(report_comp1_rec_orientation, vl.getreport_comp1_rec_orientation());
		values.put(report_comp1_rec_size, vl.getreport_comp1_rec_size());
		values.put(report_comp1_rec_unit_condition, vl.getreport_comp1_rec_unit_condition());
		values.put(report_comp1_rec_amenities, vl.getreport_comp1_rec_amenities());
		values.put(report_comp1_rec_unit_features, vl.getreport_comp1_rec_unit_features());
		values.put(report_comp1_rec_time_element, vl.getreport_comp1_rec_time_element());
		values.put(report_comp1_concluded_adjustment, vl.getreport_comp1_concluded_adjustment());
		values.put(report_comp1_adjusted_value, vl.getreport_comp1_adjusted_value());
		values.put(report_comp1_remarks, vl.getreport_comp1_remarks());
		// updating row
		return db.update(tbl_condo, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating single condo comp2
	public int updateCondo_comp2(Condo_API vl, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp2_date_month, vl.getreport_comp2_date_month());
		values.put(report_comp2_date_day, vl.getreport_comp2_date_day());
		values.put(report_comp2_date_year, vl.getreport_comp2_date_year());
		values.put(report_comp2_source, vl.getreport_comp2_source());
		values.put(report_comp2_contact_no, vl.getreport_comp2_contact_no());
		values.put(report_comp2_location, vl.getreport_comp2_location()); 
		values.put(report_comp2_area, vl.getreport_comp2_area());
		values.put(report_comp2_base_price, vl.getreport_comp2_base_price());
		values.put(report_comp2_price_sqm, vl.getreport_comp2_price_sqm());
		values.put(report_comp2_discount_rate, vl.getreport_comp2_discount_rate());
		values.put(report_comp2_discounts, vl.getreport_comp2_discounts());
		values.put(report_comp2_selling_price, vl.getreport_comp2_selling_price());
		values.put(report_comp2_rec_location, vl.getreport_comp2_rec_location());
		values.put(report_comp2_rec_unit_floor_location, vl.getreport_comp2_rec_unit_floor_location());
		values.put(report_comp2_rec_orientation, vl.getreport_comp2_rec_orientation());
		values.put(report_comp2_rec_size, vl.getreport_comp2_rec_size());
		values.put(report_comp2_rec_unit_condition, vl.getreport_comp2_rec_unit_condition());
		values.put(report_comp2_rec_amenities, vl.getreport_comp2_rec_amenities());
		values.put(report_comp2_rec_unit_features, vl.getreport_comp2_rec_unit_features());
		values.put(report_comp2_rec_time_element, vl.getreport_comp2_rec_time_element());
		values.put(report_comp2_concluded_adjustment, vl.getreport_comp2_concluded_adjustment());
		values.put(report_comp2_adjusted_value, vl.getreport_comp2_adjusted_value());
		values.put(report_comp2_remarks, vl.getreport_comp2_remarks());
		// updating row
		return db.update(tbl_condo, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating single condo comp3
	public int updateCondo_comp3(Condo_API vl, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp3_date_month, vl.getreport_comp3_date_month());
		values.put(report_comp3_date_day, vl.getreport_comp3_date_day());
		values.put(report_comp3_date_year, vl.getreport_comp3_date_year());
		values.put(report_comp3_source, vl.getreport_comp3_source());
		values.put(report_comp3_contact_no, vl.getreport_comp3_contact_no());
		values.put(report_comp3_location, vl.getreport_comp3_location()); 
		values.put(report_comp3_area, vl.getreport_comp3_area());
		values.put(report_comp3_base_price, vl.getreport_comp3_base_price());
		values.put(report_comp3_price_sqm, vl.getreport_comp3_price_sqm());
		values.put(report_comp3_discount_rate, vl.getreport_comp3_discount_rate());
		values.put(report_comp3_discounts, vl.getreport_comp3_discounts());
		values.put(report_comp3_selling_price, vl.getreport_comp3_selling_price());
		values.put(report_comp3_rec_location, vl.getreport_comp3_rec_location());
		values.put(report_comp3_rec_unit_floor_location, vl.getreport_comp3_rec_unit_floor_location());
		values.put(report_comp3_rec_orientation, vl.getreport_comp3_rec_orientation());
		values.put(report_comp3_rec_size, vl.getreport_comp3_rec_size());
		values.put(report_comp3_rec_unit_condition, vl.getreport_comp3_rec_unit_condition());
		values.put(report_comp3_rec_amenities, vl.getreport_comp3_rec_amenities());
		values.put(report_comp3_rec_unit_features, vl.getreport_comp3_rec_unit_features());
		values.put(report_comp3_rec_time_element, vl.getreport_comp3_rec_time_element());
		values.put(report_comp3_concluded_adjustment, vl.getreport_comp3_concluded_adjustment());
		values.put(report_comp3_adjusted_value, vl.getreport_comp3_adjusted_value());
		values.put(report_comp3_remarks, vl.getreport_comp3_remarks());
		// updating row
		return db.update(tbl_condo, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating single condo comp4
	public int updateCondo_comp4(Condo_API vl, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp4_date_month, vl.getreport_comp4_date_month());
		values.put(report_comp4_date_day, vl.getreport_comp4_date_day());
		values.put(report_comp4_date_year, vl.getreport_comp4_date_year());
		values.put(report_comp4_source, vl.getreport_comp4_source());
		values.put(report_comp4_contact_no, vl.getreport_comp4_contact_no());
		values.put(report_comp4_location, vl.getreport_comp4_location()); 
		values.put(report_comp4_area, vl.getreport_comp4_area());
		values.put(report_comp4_base_price, vl.getreport_comp4_base_price());
		values.put(report_comp4_price_sqm, vl.getreport_comp4_price_sqm());
		values.put(report_comp4_discount_rate, vl.getreport_comp4_discount_rate());
		values.put(report_comp4_discounts, vl.getreport_comp4_discounts());
		values.put(report_comp4_selling_price, vl.getreport_comp4_selling_price());
		values.put(report_comp4_rec_location, vl.getreport_comp4_rec_location());
		values.put(report_comp4_rec_unit_floor_location, vl.getreport_comp4_rec_unit_floor_location());
		values.put(report_comp4_rec_orientation, vl.getreport_comp4_rec_orientation());
		values.put(report_comp4_rec_size, vl.getreport_comp4_rec_size());
		values.put(report_comp4_rec_unit_condition, vl.getreport_comp4_rec_unit_condition());
		values.put(report_comp4_rec_amenities, vl.getreport_comp4_rec_amenities());
		values.put(report_comp4_rec_unit_features, vl.getreport_comp4_rec_unit_features());
		values.put(report_comp4_rec_time_element, vl.getreport_comp4_rec_time_element());
		values.put(report_comp4_concluded_adjustment, vl.getreport_comp4_concluded_adjustment());
		values.put(report_comp4_adjusted_value, vl.getreport_comp4_adjusted_value());
		values.put(report_comp4_remarks, vl.getreport_comp4_remarks());
		// updating row
		return db.update(tbl_condo, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating single condo comp5
	public int updateCondo_comp5(Condo_API vl, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp5_date_month, vl.getreport_comp5_date_month());
		values.put(report_comp5_date_day, vl.getreport_comp5_date_day());
		values.put(report_comp5_date_year, vl.getreport_comp5_date_year());
		values.put(report_comp5_source, vl.getreport_comp5_source());
		values.put(report_comp5_contact_no, vl.getreport_comp5_contact_no());
		values.put(report_comp5_location, vl.getreport_comp5_location()); 
		values.put(report_comp5_area, vl.getreport_comp5_area());
		values.put(report_comp5_base_price, vl.getreport_comp5_base_price());
		values.put(report_comp5_price_sqm, vl.getreport_comp5_price_sqm());
		values.put(report_comp5_discount_rate, vl.getreport_comp5_discount_rate());
		values.put(report_comp5_discounts, vl.getreport_comp5_discounts());
		values.put(report_comp5_selling_price, vl.getreport_comp5_selling_price());
		values.put(report_comp5_rec_location, vl.getreport_comp5_rec_location());
		values.put(report_comp5_rec_unit_floor_location, vl.getreport_comp5_rec_unit_floor_location());
		values.put(report_comp5_rec_orientation, vl.getreport_comp5_rec_orientation());
		values.put(report_comp5_rec_size, vl.getreport_comp5_rec_size());
		values.put(report_comp5_rec_unit_condition, vl.getreport_comp5_rec_unit_condition());
		values.put(report_comp5_rec_amenities, vl.getreport_comp5_rec_amenities());
		values.put(report_comp5_rec_unit_features, vl.getreport_comp5_rec_unit_features());
		values.put(report_comp5_rec_time_element, vl.getreport_comp5_rec_time_element());
		values.put(report_comp5_concluded_adjustment, vl.getreport_comp5_concluded_adjustment());
		values.put(report_comp5_adjusted_value, vl.getreport_comp5_adjusted_value());
		values.put(report_comp5_remarks, vl.getreport_comp5_remarks());
		// updating row
		return db.update(tbl_condo, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating single condo comparatives
	public int updateCondo_comparatives(Condo_API vl, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp_average, vl.getreport_comp_average());
		values.put(report_comp_rounded_to, vl.getreport_comp_rounded_to());
		// updating row
		return db.update(tbl_condo, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	
	//updating condo summary
	public int updateCondo_Summary(Condo_API c,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_final_value_total_appraised_value, c.getreport_final_value_total_appraised_value());
		values.put(report_final_value_recommended_deductions_desc, c.getreport_final_value_recommended_deductions_desc());
		values.put(report_final_value_recommended_deductions_rate, c.getreport_final_value_recommended_deductions_rate());
		values.put(report_final_value_recommended_deductions_amt, c.getreport_final_value_recommended_deductions_amt());
		values.put(report_final_value_net_appraised_value, c.getreport_final_value_net_appraised_value());
		values.put(report_final_value_quick_sale_value_rate, c.getreport_final_value_quick_sale_value_rate());
		values.put(report_final_value_quick_sale_value_amt, c.getreport_final_value_quick_sale_value_amt());
		values.put(report_factors_of_concern, c.getreport_factors_of_concern());
		values.put(report_suggested_corrective_actions, c.getreport_suggested_corrective_actions());
		values.put(report_remarks, c.getreport_remarks());
		values.put(report_requirements, c.getreport_requirements());
		
		// updating row
		return db.update(tbl_condo, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	//updating condo summary
	public int updateCondo_Summary_Total(Condo_API c,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_final_value_total_appraised_value, c.getreport_final_value_total_appraised_value());
		// updating row
		return db.update(tbl_condo, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating Zonal Values
	public int updateCondo_Zonal(Condo_API c,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_zonal_location, c.getreport_zonal_location());
		values.put(report_zonal_lot_classification, c.getreport_zonal_lot_classification());
		values.put(report_zonal_value, c.getreport_zonal_value());
		
		// updating row
		return db.update(tbl_condo, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	
	
/**
* updating of Vacant Lot
*/

	// Updating single vacant lot page2
	public int updateVacant_Lot_page2(Vacant_Lot_API vl, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_date_inspected_month,vl.getreport_date_inspected_month());
		values.put(report_date_inspected_day, vl.getreport_date_inspected_day());
		values.put(report_date_inspected_year,vl.getreport_date_inspected_year());
		values.put(report_id_association, vl.getreport_id_association());
		values.put(report_id_tax, vl.getreport_id_tax());
		values.put(report_id_lra, vl.getreport_id_lra());
		values.put(report_id_lot_config, vl.getreport_id_lot_config());
		values.put(report_id_subd_map, vl.getreport_id_subd_map());
		values.put(report_id_nbrhood_checking, vl.getreport_id_nbrhood_checking());
		values.put(report_imp_street_name, vl.getreport_imp_street_name());
		values.put(report_imp_street, vl.getreport_imp_street());
		values.put(report_imp_sidewalk, vl.getreport_imp_sidewalk());
		values.put(report_imp_curb, vl.getreport_imp_curb());
		values.put(report_imp_drainage, vl.getreport_imp_drainage());
		values.put(report_imp_street_lights, vl.getreport_imp_street_lights());
		values.put(report_physical_corner_lot, vl.getreport_physical_corner_lot());
		values.put(report_physical_non_corner_lot, vl.getreport_physical_non_corner_lot());
		values.put(report_physical_perimeter_lot, vl.getreport_physical_perimeter_lot());
		values.put(report_physical_intersected_lot, vl.getreport_physical_intersected_lot());
		values.put(report_physical_interior_with_row, vl.getreport_physical_interior_with_row());
		values.put(report_physical_landlocked, vl.getreport_physical_landlocked());
		values.put(report_lotclass_per_tax_dec, vl.getreport_lotclass_per_tax_dec());
		values.put(report_lotclass_actual_usage, vl.getreport_lotclass_actual_usage());
		values.put(report_lotclass_neighborhood, vl.getreport_lotclass_neighborhood());
		values.put(report_lotclass_highest_best_use, vl.getreport_lotclass_highest_best_use());
		values.put(report_physical_shape, vl.getreport_physical_shape());
		values.put(report_physical_frontage, vl.getreport_physical_frontage());
		values.put(report_physical_depth, vl.getreport_physical_depth());
		values.put(report_physical_road, vl.getreport_physical_road());
		values.put(report_physical_elevation, vl.getreport_physical_elevation());
		values.put(report_physical_terrain, vl.getreport_physical_terrain());
		values.put(report_landmark_1, vl.getreport_landmark_1());
		values.put(report_landmark_2, vl.getreport_landmark_2());
		values.put(report_landmark_3, vl.getreport_landmark_3());
		values.put(report_landmark_4, vl.getreport_landmark_4());
		values.put(report_landmark_5, vl.getreport_landmark_5());
		values.put(report_distance_1, vl.getreport_distance_1());
		values.put(report_distance_2, vl.getreport_distance_2());
		values.put(report_distance_3, vl.getreport_distance_3());
		values.put(report_distance_4, vl.getreport_distance_4());
		values.put(report_distance_5, vl.getreport_distance_5());
		values.put(report_util_electricity, vl.getreport_util_electricity());
		values.put(report_util_water, vl.getreport_util_water());
		values.put(report_util_telephone, vl.getreport_util_telephone());
		values.put(report_util_garbage, vl.getreport_util_garbage());
		values.put(report_trans_jeep, vl.getreport_trans_jeep());
		values.put(report_trans_bus, vl.getreport_trans_bus());
		values.put(report_trans_taxi, vl.getreport_trans_taxi());
		values.put(report_trans_tricycle, vl.getreport_trans_tricycle());
		values.put(report_faci_recreation, vl.getreport_faci_recreation());
		values.put(report_faci_security, vl.getreport_faci_security());
		values.put(report_faci_clubhouse, vl.getreport_faci_clubhouse());
		values.put(report_faci_pool, vl.getreport_faci_pool());
		values.put(report_bound_right, vl.getreport_bound_right());
		values.put(report_bound_left, vl.getreport_bound_left());
		values.put(report_bound_rear, vl.getreport_bound_rear());
		values.put(report_bound_front, vl.getreport_bound_front());
		values.put(report_prev_date, vl.getreport_prev_date());
		values.put(report_prev_appraiser, vl.getreport_prev_appraiser());
		values.put(report_time_inspected, vl.getreport_time_inspected());
		
		// updating row
		return db.update(tbl_vacant_lot, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating single vacant lot page2
	public int updateVacant_Lot_page2_streetName(Vacant_Lot_API vl, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();

		values.put(report_imp_street_name, vl.getreport_imp_street_name());


		// updating row
		return db.update(tbl_vacant_lot, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating single vacant comp1
	public int updateVacant_Lot_comp1(Vacant_Lot_API vl, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp1_date_month, vl.getreport_comp1_date_month());
		values.put(report_comp1_date_day, vl.getreport_comp1_date_day());
		values.put(report_comp1_date_year, vl.getreport_comp1_date_year());
		values.put(report_comp1_source, vl.getreport_comp1_source());
		values.put(report_comp1_contact_no, vl.getreport_comp1_contact_no());
		values.put(report_comp1_location, vl.getreport_comp1_location()); 
		values.put(report_comp1_area, vl.getreport_comp1_area());
		values.put(report_comp1_base_price, vl.getreport_comp1_base_price());
		values.put(report_comp1_price_sqm, vl.getreport_comp1_price_sqm());
		values.put(report_comp1_discount_rate, vl.getreport_comp1_discount_rate());
		values.put(report_comp1_discounts, vl.getreport_comp1_discounts());
		values.put(report_comp1_selling_price, vl.getreport_comp1_selling_price());
		values.put(report_comp1_rec_location, vl.getreport_comp1_rec_location());
		values.put(report_comp1_rec_size, vl.getreport_comp1_rec_size());

		values.put(report_comp1_rec_accessibility, vl.getreport_comp1_rec_accessibility());
		values.put(report_comp1_rec_amenities, vl.getreport_comp1_rec_amenities());
		values.put(report_comp1_rec_terrain, vl.getreport_comp1_rec_terrain());

		values.put(report_comp1_rec_shape, vl.getreport_comp1_rec_shape());
		values.put(report_comp1_rec_corner_influence, vl.getreport_comp1_rec_corner_influence());
		values.put(report_comp1_rec_tru_lot, vl.getreport_comp1_rec_tru_lot());
		values.put(report_comp1_rec_elevation, vl.getreport_comp1_rec_elevation());
		values.put(report_comp1_rec_time_element, vl.getreport_comp1_rec_time_element());
		values.put(report_comp1_rec_others, vl.getreport_comp1_rec_others());

		values.put(report_comp1_rec_degree_of_devt, vl.getreport_comp1_rec_degree_of_devt());
		values.put(report_comp1_concluded_adjustment, vl.getreport_comp1_concluded_adjustment());
		values.put(report_comp1_adjusted_value, vl.getreport_comp1_adjusted_value());
		values.put(report_comp1_remarks, vl.getreport_comp1_remarks());
		// updating row
		return db.update(tbl_vacant_lot, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating single vacant comp2
	public int updateVacant_Lot_comp2(Vacant_Lot_API vl, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp2_date_month, vl.getreport_comp2_date_month());
		values.put(report_comp2_date_day, vl.getreport_comp2_date_day());
		values.put(report_comp2_date_year, vl.getreport_comp2_date_year());
		values.put(report_comp2_source, vl.getreport_comp2_source());
		values.put(report_comp2_contact_no, vl.getreport_comp2_contact_no());
		values.put(report_comp2_location, vl.getreport_comp2_location()); 
		values.put(report_comp2_area, vl.getreport_comp2_area());
		values.put(report_comp2_base_price, vl.getreport_comp2_base_price());
		values.put(report_comp2_price_sqm, vl.getreport_comp2_price_sqm());
		values.put(report_comp2_discount_rate, vl.getreport_comp2_discount_rate());
		values.put(report_comp2_discounts, vl.getreport_comp2_discounts());
		values.put(report_comp2_selling_price, vl.getreport_comp2_selling_price());
		values.put(report_comp2_rec_location, vl.getreport_comp2_rec_location());
		values.put(report_comp2_rec_size, vl.getreport_comp2_rec_size());

		values.put(report_comp2_rec_accessibility, vl.getreport_comp2_rec_accessibility());
		values.put(report_comp2_rec_amenities, vl.getreport_comp2_rec_amenities());
		values.put(report_comp2_rec_terrain, vl.getreport_comp2_rec_terrain());

		values.put(report_comp2_rec_shape, vl.getreport_comp2_rec_shape());
		values.put(report_comp2_rec_corner_influence, vl.getreport_comp2_rec_corner_influence());
		values.put(report_comp2_rec_tru_lot, vl.getreport_comp2_rec_tru_lot());
		values.put(report_comp2_rec_elevation, vl.getreport_comp2_rec_elevation());
		values.put(report_comp2_rec_time_element, vl.getreport_comp2_rec_time_element());

		values.put(report_comp2_rec_others, vl.getreport_comp2_rec_others());

		values.put(report_comp2_rec_degree_of_devt, vl.getreport_comp2_rec_degree_of_devt());
		values.put(report_comp2_concluded_adjustment, vl.getreport_comp2_concluded_adjustment());
		values.put(report_comp2_adjusted_value, vl.getreport_comp2_adjusted_value());
		values.put(report_comp2_remarks, vl.getreport_comp2_remarks());
		// updating row
		return db.update(tbl_vacant_lot, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating single vacant comp3
	public int updateVacant_Lot_comp3(Vacant_Lot_API vl, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp3_date_month, vl.getreport_comp3_date_month());
		values.put(report_comp3_date_day, vl.getreport_comp3_date_day());
		values.put(report_comp3_date_year, vl.getreport_comp3_date_year());
		values.put(report_comp3_source, vl.getreport_comp3_source());
		values.put(report_comp3_contact_no, vl.getreport_comp3_contact_no());
		values.put(report_comp3_location, vl.getreport_comp3_location()); 
		values.put(report_comp3_area, vl.getreport_comp3_area());
		values.put(report_comp3_base_price, vl.getreport_comp3_base_price());
		values.put(report_comp3_price_sqm, vl.getreport_comp3_price_sqm());
		values.put(report_comp3_discount_rate, vl.getreport_comp3_discount_rate());
		values.put(report_comp3_discounts, vl.getreport_comp3_discounts());
		values.put(report_comp3_selling_price, vl.getreport_comp3_selling_price());
		values.put(report_comp3_rec_location, vl.getreport_comp3_rec_location());
		values.put(report_comp3_rec_size, vl.getreport_comp3_rec_size());

		values.put(report_comp3_rec_accessibility, vl.getreport_comp3_rec_accessibility());
		values.put(report_comp3_rec_amenities, vl.getreport_comp3_rec_amenities());
		values.put(report_comp3_rec_terrain, vl.getreport_comp3_rec_terrain());

		values.put(report_comp3_rec_shape, vl.getreport_comp3_rec_shape());
		values.put(report_comp3_rec_corner_influence, vl.getreport_comp3_rec_corner_influence());
		values.put(report_comp3_rec_tru_lot, vl.getreport_comp3_rec_tru_lot());
		values.put(report_comp3_rec_elevation, vl.getreport_comp3_rec_elevation());
		values.put(report_comp3_rec_time_element, vl.getreport_comp3_rec_time_element());

		values.put(report_comp3_rec_others, vl.getreport_comp3_rec_others());

		values.put(report_comp3_rec_degree_of_devt, vl.getreport_comp3_rec_degree_of_devt());
		values.put(report_comp3_concluded_adjustment, vl.getreport_comp3_concluded_adjustment());
		values.put(report_comp3_adjusted_value, vl.getreport_comp3_adjusted_value());
		values.put(report_comp3_remarks, vl.getreport_comp3_remarks());
		// updating row
		return db.update(tbl_vacant_lot, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating single vacant comp4
	public int updateVacant_Lot_comp4(Vacant_Lot_API vl, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp4_date_month, vl.getreport_comp4_date_month());
		values.put(report_comp4_date_day, vl.getreport_comp4_date_day());
		values.put(report_comp4_date_year, vl.getreport_comp4_date_year());
		values.put(report_comp4_source, vl.getreport_comp4_source());
		values.put(report_comp4_contact_no, vl.getreport_comp4_contact_no());
		values.put(report_comp4_location, vl.getreport_comp4_location()); 
		values.put(report_comp4_area, vl.getreport_comp4_area());
		values.put(report_comp4_base_price, vl.getreport_comp4_base_price());
		values.put(report_comp4_price_sqm, vl.getreport_comp4_price_sqm());
		values.put(report_comp4_discount_rate, vl.getreport_comp4_discount_rate());
		values.put(report_comp4_discounts, vl.getreport_comp4_discounts());
		values.put(report_comp4_selling_price, vl.getreport_comp4_selling_price());
		values.put(report_comp4_rec_location, vl.getreport_comp4_rec_location());
		values.put(report_comp4_rec_size, vl.getreport_comp4_rec_size());

		values.put(report_comp4_rec_accessibility, vl.getreport_comp4_rec_accessibility());
		values.put(report_comp4_rec_amenities, vl.getreport_comp4_rec_amenities());
		values.put(report_comp4_rec_terrain, vl.getreport_comp4_rec_terrain());

		values.put(report_comp4_rec_shape, vl.getreport_comp4_rec_shape());
		values.put(report_comp4_rec_corner_influence, vl.getreport_comp4_rec_corner_influence());
		values.put(report_comp4_rec_tru_lot, vl.getreport_comp4_rec_tru_lot());
		values.put(report_comp4_rec_elevation, vl.getreport_comp4_rec_elevation());
		values.put(report_comp4_rec_time_element, vl.getreport_comp4_rec_time_element());

		values.put(report_comp4_rec_others, vl.getreport_comp4_rec_others());

		values.put(report_comp4_rec_degree_of_devt, vl.getreport_comp4_rec_degree_of_devt());
		values.put(report_comp4_concluded_adjustment, vl.getreport_comp4_concluded_adjustment());
		values.put(report_comp4_adjusted_value, vl.getreport_comp4_adjusted_value());
		values.put(report_comp4_remarks, vl.getreport_comp4_remarks());
		// updating row
		return db.update(tbl_vacant_lot, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating single vacant comp5
	public int updateVacant_Lot_comp5(Vacant_Lot_API vl, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp5_date_month, vl.getreport_comp5_date_month());
		values.put(report_comp5_date_day, vl.getreport_comp5_date_day());
		values.put(report_comp5_date_year, vl.getreport_comp5_date_year());
		values.put(report_comp5_source, vl.getreport_comp5_source());
		values.put(report_comp5_contact_no, vl.getreport_comp5_contact_no());
		values.put(report_comp5_location, vl.getreport_comp5_location()); 
		values.put(report_comp5_area, vl.getreport_comp5_area());
		values.put(report_comp5_base_price, vl.getreport_comp5_base_price());
		values.put(report_comp5_price_sqm, vl.getreport_comp5_price_sqm());
		values.put(report_comp5_discount_rate, vl.getreport_comp5_discount_rate());
		values.put(report_comp5_discounts, vl.getreport_comp5_discounts());
		values.put(report_comp5_selling_price, vl.getreport_comp5_selling_price());
		values.put(report_comp5_rec_location, vl.getreport_comp5_rec_location());
		values.put(report_comp5_rec_size, vl.getreport_comp5_rec_size());

		values.put(report_comp5_rec_accessibility, vl.getreport_comp5_rec_accessibility());
		values.put(report_comp5_rec_amenities, vl.getreport_comp5_rec_amenities());
		values.put(report_comp5_rec_terrain, vl.getreport_comp5_rec_terrain());

		values.put(report_comp5_rec_shape, vl.getreport_comp5_rec_shape());
		values.put(report_comp5_rec_corner_influence, vl.getreport_comp5_rec_corner_influence());
		values.put(report_comp5_rec_tru_lot, vl.getreport_comp5_rec_tru_lot());
		values.put(report_comp5_rec_elevation, vl.getreport_comp5_rec_elevation());
		values.put(report_comp5_rec_time_element, vl.getreport_comp5_rec_time_element());

		values.put(report_comp5_rec_others, vl.getreport_comp5_rec_others());

		values.put(report_comp5_rec_degree_of_devt, vl.getreport_comp5_rec_degree_of_devt());
		values.put(report_comp5_concluded_adjustment, vl.getreport_comp5_concluded_adjustment());
		values.put(report_comp5_adjusted_value, vl.getreport_comp5_adjusted_value());
		values.put(report_comp5_remarks, vl.getreport_comp5_remarks());
		// updating row
		return db.update(tbl_vacant_lot, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating single vacant comparatives
	public int updateVacant_Lot_comparatives(Vacant_Lot_API vl, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp_average, vl.getreport_comp_average());
		values.put(report_comp_rounded_to, vl.getreport_comp_rounded_to());
		// updating row
		return db.update(tbl_vacant_lot, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}

	// Updating single vacant lot summary
	public int updateVacant_Lot_Summary(Vacant_Lot_API vl, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_final_value_total_appraised_value_land_imp, vl.getreport_final_value_total_appraised_value_land_imp());
		values.put(report_final_value_recommended_deductions_desc, vl.getreport_final_value_recommended_deductions_desc());
		values.put(report_final_value_recommended_deductions_rate, vl.getreport_final_value_recommended_deductions_rate());
		values.put(report_final_value_recommended_deductions_other, vl.getreport_final_value_recommended_deductions_other());
		values.put(report_final_value_recommended_deductions_amt, vl.getreport_final_value_recommended_deductions_amt());
		values.put(report_final_value_net_appraised_value, vl.getreport_final_value_net_appraised_value());
		values.put(report_final_value_quick_sale_value_rate, vl.getreport_final_value_quick_sale_value_rate());
		values.put(report_final_value_quick_sale_value_amt, vl.getreport_final_value_quick_sale_value_amt());
		values.put(report_factors_of_concern, vl.getreport_factors_of_concern());
		values.put(report_suggested_corrective_actions, vl.getreport_suggested_corrective_actions());
		values.put(report_remarks, vl.getreport_remarks());
		values.put(report_requirements, vl.getreport_requirements());
		values.put(report_concerns_minor_1, vl.getreport_concerns_minor_1());
		values.put(report_concerns_minor_2, vl.getreport_concerns_minor_2());
		values.put(report_concerns_minor_3, vl.getreport_concerns_minor_3());
		values.put(report_concerns_minor_others, vl.getreport_concerns_minor_others());
		values.put(report_concerns_minor_others_desc, vl.getreport_concerns_minor_others_desc());
		values.put(report_concerns_major_1, vl.getreport_concerns_major_1());
		values.put(report_concerns_major_2, vl.getreport_concerns_major_2());
		values.put(report_concerns_major_3, vl.getreport_concerns_major_3());
		values.put(report_concerns_major_4, vl.getreport_concerns_major_4());
		values.put(report_concerns_major_5, vl.getreport_concerns_major_5());
		values.put(report_concerns_major_6, vl.getreport_concerns_major_6());
		values.put(report_concerns_major_others, vl.getreport_concerns_major_others());
		values.put(report_concerns_major_others_desc, vl.getreport_concerns_major_others_desc());
		// updating row
		return db.update(tbl_vacant_lot, values, Report_record_id + " = ?",
				new String[] { record_id });
	}
	// Updating single vacant lot summary
	public int updateVacant_Lot_Summary_Total(Vacant_Lot_API vl, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_final_value_total_appraised_value_land_imp, vl.getreport_final_value_total_appraised_value_land_imp());
		return db.update(tbl_vacant_lot, values, Report_record_id + " = ?",
				new String[] { record_id });
	}
	
	// Update total valuation
	public int updateVacant_Lot_Total_Valuation(Vacant_Lot_API vl,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_value_total_area, vl.getreport_value_total_area());
		values.put(report_value_total_deduction, vl.getreport_value_total_deduction());
		values.put(report_value_total_net_area, vl.getreport_value_total_net_area());
		values.put(report_value_total_landimp_value, vl.getreport_value_total_landimp_value());
		
		// updating row
		return db.update(tbl_vacant_lot, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating Zonal Values
	public int updateVacant_Lot_Zonal(Vacant_Lot_API vl,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_zonal_location, vl.getreport_zonal_location());
		values.put(report_zonal_lot_classification, vl.getreport_zonal_lot_classification());
		values.put(report_zonal_value, vl.getreport_zonal_value());
		
		// updating row
		return db.update(tbl_vacant_lot, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	
	/**
	 * updating of Land Imps
	 */
	
	// Updating single land improvements 2 Lot Details
	public int updateLand_Improvements_page2(Land_Improvements_API li,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();

		values.put(report_date_inspected_month,li.getreport_date_inspected_month());
		values.put(report_date_inspected_day, li.getreport_date_inspected_day());
		values.put(report_date_inspected_year,li.getreport_date_inspected_year());
		values.put(report_id_association, li.getreport_id_association());
		values.put(report_id_tax, li.getreport_id_tax());
		values.put(report_id_lra, li.getreport_id_lra());
		values.put(report_id_lot_config, li.getreport_id_lot_config());
		values.put(report_id_subd_map, li.getreport_id_subd_map());
		values.put(report_id_nbrhood_checking, li.getreport_id_nbrhood_checking());
		values.put(report_imp_street_name, li.getreport_imp_street_name());
		values.put(report_imp_street, li.getreport_imp_street());
		values.put(report_imp_sidewalk, li.getreport_imp_sidewalk());
		values.put(report_imp_curb, li.getreport_imp_curb());
		values.put(report_imp_drainage, li.getreport_imp_drainage());
		values.put(report_imp_street_lights, li.getreport_imp_street_lights());
		values.put(report_physical_corner_lot, li.getreport_physical_corner_lot());
		values.put(report_physical_non_corner_lot, li.getreport_physical_non_corner_lot());
		values.put(report_physical_perimeter_lot, li.getreport_physical_perimeter_lot());
		values.put(report_physical_intersected_lot, li.getreport_physical_intersected_lot());
		values.put(report_physical_interior_with_row, li.getreport_physical_interior_with_row());
		values.put(report_physical_landlocked, li.getreport_physical_landlocked());
		values.put(report_lotclass_per_tax_dec, li.getreport_lotclass_per_tax_dec());
		values.put(report_lotclass_actual_usage, li.getreport_lotclass_actual_usage());
		values.put(report_lotclass_neighborhood, li.getreport_lotclass_neighborhood());
		values.put(report_lotclass_highest_best_use, li.getreport_lotclass_highest_best_use());
		values.put(report_physical_shape, li.getreport_physical_shape());
		values.put(report_physical_frontage, li.getreport_physical_frontage());
		values.put(report_physical_depth, li.getreport_physical_depth());
		values.put(report_physical_road, li.getreport_physical_road());
		values.put(report_physical_elevation, li.getreport_physical_elevation());
		values.put(report_physical_terrain, li.getreport_physical_terrain());
		values.put(report_landmark_1, li.getreport_landmark_1());
		values.put(report_landmark_2, li.getreport_landmark_2());
		values.put(report_landmark_3, li.getreport_landmark_3());
		values.put(report_landmark_4, li.getreport_landmark_4());
		values.put(report_landmark_5, li.getreport_landmark_5());
		values.put(report_distance_1, li.getreport_distance_1());
		values.put(report_distance_2, li.getreport_distance_2());
		values.put(report_distance_3, li.getreport_distance_3());
		values.put(report_distance_4, li.getreport_distance_4());
		values.put(report_distance_5, li.getreport_distance_5());
		values.put(report_util_electricity, li.getreport_util_electricity());
		values.put(report_util_water, li.getreport_util_water());
		values.put(report_util_telephone, li.getreport_util_telephone());
		values.put(report_util_garbage, li.getreport_util_garbage());
		values.put(report_trans_jeep, li.getreport_trans_jeep());
		values.put(report_trans_bus, li.getreport_trans_bus());
		values.put(report_trans_taxi, li.getreport_trans_taxi());
		values.put(report_trans_tricycle, li.getreport_trans_tricycle());
		values.put(report_faci_recreation, li.getreport_faci_recreation());
		values.put(report_faci_security, li.getreport_faci_security());
		values.put(report_faci_clubhouse, li.getreport_faci_clubhouse());
		values.put(report_faci_pool, li.getreport_faci_pool());
		values.put(report_bound_right, li.getreport_bound_right());
		values.put(report_bound_left, li.getreport_bound_left());
		values.put(report_bound_rear, li.getreport_bound_rear());
		values.put(report_bound_front, li.getreport_bound_front());
		values.put(report_time_inspected, li.getreport_time_inspected());

		//ADDED BY Ian
		values.put(report_prev_date, li.getreport_prev_date());
		values.put(report_prev_appraiser, li.getreport_prev_appraiser());
		//values.put(report_prev_total_appraised_value, li.getreport_prev_total_appraised_value());
		
		// updating row
		return db.update(tbl_land_improvements, values, Report_record_id
				+ " = ?", new String[] { record_id });
		
	}

	// Updating single land improvements 2 Lot Details
	public int updateLand_Improvements_page2_StreetName(Land_Improvements_API li,
											 String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();


		values.put(report_imp_street_name, li.getreport_imp_street_name());


		// updating row
		return db.update(tbl_land_improvements, values, Report_record_id
				+ " = ?", new String[] { record_id });

	}


	/**
	 *
	 * MOTOR VEHICLE COMPARATIVE ANALYSIS
	 */



	// Updating single land imp comp1
	public int updateMotor_Vehicle_comp1(Motor_Vehicle_API li, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp1_source, li.getreport_comp1_source());
		values.put(report_comp1_tel_no, li.getreport_comp1_tel_no());
		values.put(report_comp1_unit_model, li.getreport_comp1_unit_model());
		values.put(report_comp1_mileage, li.getreport_comp1_mileage());
		values.put(report_comp1_price_min, li.getreport_comp1_price_min());
		values.put(report_comp1_price_max, li.getreport_comp1_price_max());
		values.put(report_comp1_price, li.getreport_comp1_price());
		values.put(report_comp1_less_amt, li.getreport_comp1_less_amt());
		values.put(report_comp1_less_net, li.getreport_comp1_less_net());
		values.put(report_comp1_add_amt, li.getreport_comp1_add_amt());
		values.put(report_comp1_add_net, li.getreport_comp1_add_net());
		values.put(report_comp1_time_adj_value, li.getreport_comp1_time_adj_value());
		values.put(report_comp1_time_adj_amt, li.getreport_comp1_time_adj_amt());
		values.put(report_comp1_adj_value, li.getreport_comp1_adj_value());
		values.put(report_comp1_adj_amt, li.getreport_comp1_adj_amt());
		values.put(report_comp1_index_price, li.getreport_comp1_index_price());
		values.put(report_comp1_mileage_value, li.getreport_comp1_mileage_value());
		values.put(report_comp1_mileage_amt, li.getreport_comp1_mileage_amt());
		values.put(report_comp1_adj_desc, li.getreport_comp1_adj_desc());
		values.put("valrep_mv_comp1_new_unit_value", li.getvalrep_mv_comp1_new_unit_value());
		values.put("valrep_mv_comp1_new_unit_amt", li.getvalrep_mv_comp1_new_unit_amt());

		// updating row
		return db.update(tbl_motor_vehicle, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}

	// Updating single land imp comp2
	public int updateMotor_Vehicle_comp2(Motor_Vehicle_API li, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp2_source, li.getreport_comp2_source());
		values.put(report_comp2_tel_no, li.getreport_comp2_tel_no());
		values.put(report_comp2_unit_model, li.getreport_comp2_unit_model());
		values.put(report_comp2_mileage, li.getreport_comp2_mileage());
		values.put(report_comp2_price_min, li.getreport_comp2_price_min());
		values.put(report_comp2_price_max, li.getreport_comp2_price_max());
		values.put(report_comp2_price, li.getreport_comp2_price());
		values.put(report_comp2_less_amt, li.getreport_comp2_less_amt());
		values.put(report_comp2_less_net, li.getreport_comp2_less_net());
		values.put(report_comp2_add_amt, li.getreport_comp2_add_amt());
		values.put(report_comp2_add_net, li.getreport_comp2_add_net());
		values.put(report_comp2_time_adj_value, li.getreport_comp2_time_adj_value());
		values.put(report_comp2_time_adj_amt, li.getreport_comp2_time_adj_amt());
		values.put(report_comp2_adj_value, li.getreport_comp2_adj_value());
		values.put(report_comp2_adj_amt, li.getreport_comp2_adj_amt());
		values.put(report_comp2_index_price, li.getreport_comp2_index_price());
		values.put(report_comp2_mileage_value, li.getreport_comp2_mileage_value());
		values.put(report_comp2_mileage_amt, li.getreport_comp2_mileage_amt());
		values.put(report_comp2_adj_desc, li.getreport_comp2_adj_desc());

		values.put("valrep_mv_comp2_new_unit_value", li.getvalrep_mv_comp2_new_unit_value());
		values.put("valrep_mv_comp2_new_unit_amt", li.getvalrep_mv_comp2_new_unit_amt());



		// updating row
		return db.update(tbl_motor_vehicle, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}

	// Updating single land imp comp3
	public int updateMotor_Vehicle_comp3(Motor_Vehicle_API li, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp3_source, li.getreport_comp3_source());
		values.put(report_comp3_tel_no, li.getreport_comp3_tel_no());
		values.put(report_comp3_unit_model, li.getreport_comp3_unit_model());
		values.put(report_comp3_mileage, li.getreport_comp3_mileage());
		values.put(report_comp3_price_min, li.getreport_comp3_price_min());
		values.put(report_comp3_price_max, li.getreport_comp3_price_max());
		values.put(report_comp3_price, li.getreport_comp3_price());
		values.put(report_comp3_less_amt, li.getreport_comp3_less_amt());
		values.put(report_comp3_less_net, li.getreport_comp3_less_net());
		values.put(report_comp3_add_amt, li.getreport_comp3_add_amt());
		values.put(report_comp3_add_net, li.getreport_comp3_add_net());
		values.put(report_comp3_time_adj_value, li.getreport_comp3_time_adj_value());
		values.put(report_comp3_time_adj_amt, li.getreport_comp3_time_adj_amt());
		values.put(report_comp3_adj_value, li.getreport_comp3_adj_value());
		values.put(report_comp3_adj_amt, li.getreport_comp3_adj_amt());
		values.put(report_comp3_index_price, li.getreport_comp3_index_price());
		values.put(report_comp3_mileage_value, li.getreport_comp3_mileage_value());
		values.put(report_comp3_mileage_amt, li.getreport_comp3_mileage_amt());
		values.put(report_comp3_adj_desc, li.getreport_comp3_adj_desc());

		values.put("valrep_mv_comp3_new_unit_value", li.getvalrep_mv_comp3_new_unit_value());
		values.put("valrep_mv_comp3_new_unit_amt", li.getvalrep_mv_comp3_new_unit_amt());

		// updating row
		return db.update(tbl_motor_vehicle, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}

	/**
	 *
	 * LAND IMPROVEMENTS COMPARATIVE ANALYSIS
	 */



	// Updating single land imp comp1
	public int updateLand_Improvements_comp1(Land_Improvements_API li, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp1_date_month, li.getreport_comp1_date_month());
		values.put(report_comp1_date_day, li.getreport_comp1_date_day());
		values.put(report_comp1_date_year, li.getreport_comp1_date_year());
		values.put(report_comp1_source, li.getreport_comp1_source());
		values.put(report_comp1_contact_no, li.getreport_comp1_contact_no());
		values.put(report_comp1_location, li.getreport_comp1_location()); 
		values.put(report_comp1_area, li.getreport_comp1_area());
		values.put(report_comp1_base_price, li.getreport_comp1_base_price());
		values.put(report_comp1_price_sqm, li.getreport_comp1_price_sqm());
		values.put(report_comp1_discount_rate, li.getreport_comp1_discount_rate());
		values.put(report_comp1_discounts, li.getreport_comp1_discounts());
		values.put(report_comp1_selling_price, li.getreport_comp1_selling_price());
		values.put(report_comp1_rec_location, li.getreport_comp1_rec_location());
		values.put(report_comp1_rec_size, li.getreport_comp1_rec_size());
		values.put(report_comp1_rec_shape, li.getreport_comp1_rec_shape());
		values.put(report_comp1_rec_corner_influence, li.getreport_comp1_rec_corner_influence());
		values.put(report_comp1_rec_tru_lot, li.getreport_comp1_rec_tru_lot());
		values.put(report_comp1_rec_elevation, li.getreport_comp1_rec_elevation());
		values.put(report_comp1_rec_time_element, li.getreport_comp1_rec_time_element());
		values.put(report_comp1_rec_degree_of_devt, li.getreport_comp1_rec_degree_of_devt());
		values.put(report_comp1_concluded_adjustment, li.getreport_comp1_concluded_adjustment());
		values.put(report_comp1_adjusted_value, li.getreport_comp1_adjusted_value());
		values.put(report_comp1_remarks, li.getreport_comp1_remarks());
		// updating row
		return db.update(tbl_land_improvements, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating single land imp comp2
	public int updateLand_Improvements_comp2(Land_Improvements_API li, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp2_date_month, li.getreport_comp2_date_month());
		values.put(report_comp2_date_day, li.getreport_comp2_date_day());
		values.put(report_comp2_date_year, li.getreport_comp2_date_year());
		values.put(report_comp2_source, li.getreport_comp2_source());
		values.put(report_comp2_contact_no, li.getreport_comp2_contact_no());
		values.put(report_comp2_location, li.getreport_comp2_location()); 
		values.put(report_comp2_area, li.getreport_comp2_area());
		values.put(report_comp2_base_price, li.getreport_comp2_base_price());
		values.put(report_comp2_price_sqm, li.getreport_comp2_price_sqm());
		values.put(report_comp2_discount_rate, li.getreport_comp2_discount_rate());
		values.put(report_comp2_discounts, li.getreport_comp2_discounts());
		values.put(report_comp2_selling_price, li.getreport_comp2_selling_price());
		values.put(report_comp2_rec_location, li.getreport_comp2_rec_location());
		values.put(report_comp2_rec_size, li.getreport_comp2_rec_size());
		values.put(report_comp2_rec_shape, li.getreport_comp2_rec_shape());
		values.put(report_comp2_rec_corner_influence, li.getreport_comp2_rec_corner_influence());
		values.put(report_comp2_rec_tru_lot, li.getreport_comp2_rec_tru_lot());
		values.put(report_comp2_rec_elevation, li.getreport_comp2_rec_elevation());
		values.put(report_comp2_rec_time_element, li.getreport_comp2_rec_time_element());
		values.put(report_comp2_rec_degree_of_devt, li.getreport_comp2_rec_degree_of_devt());
		values.put(report_comp2_concluded_adjustment, li.getreport_comp2_concluded_adjustment());
		values.put(report_comp2_adjusted_value, li.getreport_comp2_adjusted_value());
		values.put(report_comp2_remarks, li.getreport_comp2_remarks());
		// updating row
		return db.update(tbl_land_improvements, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating single land imp comp3
	public int updateLand_Improvements_comp3(Land_Improvements_API li, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp3_date_month, li.getreport_comp3_date_month());
		values.put(report_comp3_date_day, li.getreport_comp3_date_day());
		values.put(report_comp3_date_year, li.getreport_comp3_date_year());
		values.put(report_comp3_source, li.getreport_comp3_source());
		values.put(report_comp3_contact_no, li.getreport_comp3_contact_no());
		values.put(report_comp3_location, li.getreport_comp3_location()); 
		values.put(report_comp3_area, li.getreport_comp3_area());
		values.put(report_comp3_base_price, li.getreport_comp3_base_price());
		values.put(report_comp3_price_sqm, li.getreport_comp3_price_sqm());
		values.put(report_comp3_discount_rate, li.getreport_comp3_discount_rate());
		values.put(report_comp3_discounts, li.getreport_comp3_discounts());
		values.put(report_comp3_selling_price, li.getreport_comp3_selling_price());
		values.put(report_comp3_rec_location, li.getreport_comp3_rec_location());
		values.put(report_comp3_rec_size, li.getreport_comp3_rec_size());
		values.put(report_comp3_rec_shape, li.getreport_comp3_rec_shape());
		values.put(report_comp3_rec_corner_influence, li.getreport_comp3_rec_corner_influence());
		values.put(report_comp3_rec_tru_lot, li.getreport_comp3_rec_tru_lot());
		values.put(report_comp3_rec_elevation, li.getreport_comp3_rec_elevation());
		values.put(report_comp3_rec_time_element, li.getreport_comp3_rec_time_element());
		values.put(report_comp3_rec_degree_of_devt, li.getreport_comp3_rec_degree_of_devt());
		values.put(report_comp3_concluded_adjustment, li.getreport_comp3_concluded_adjustment());
		values.put(report_comp3_adjusted_value, li.getreport_comp3_adjusted_value());
		values.put(report_comp3_remarks, li.getreport_comp3_remarks());
		// updating row
		return db.update(tbl_land_improvements, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating single land imp comp4
	public int updateLand_Improvements_comp4(Land_Improvements_API li, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp4_date_month, li.getreport_comp4_date_month());
		values.put(report_comp4_date_day, li.getreport_comp4_date_day());
		values.put(report_comp4_date_year, li.getreport_comp4_date_year());
		values.put(report_comp4_source, li.getreport_comp4_source());
		values.put(report_comp4_contact_no, li.getreport_comp4_contact_no());
		values.put(report_comp4_location, li.getreport_comp4_location()); 
		values.put(report_comp4_area, li.getreport_comp4_area());
		values.put(report_comp4_base_price, li.getreport_comp4_base_price());
		values.put(report_comp4_price_sqm, li.getreport_comp4_price_sqm());
		values.put(report_comp4_discount_rate, li.getreport_comp4_discount_rate());
		values.put(report_comp4_discounts, li.getreport_comp4_discounts());
		values.put(report_comp4_selling_price, li.getreport_comp4_selling_price());
		values.put(report_comp4_rec_location, li.getreport_comp4_rec_location());
		values.put(report_comp4_rec_size, li.getreport_comp4_rec_size());
		values.put(report_comp4_rec_shape, li.getreport_comp4_rec_shape());
		values.put(report_comp4_rec_corner_influence, li.getreport_comp4_rec_corner_influence());
		values.put(report_comp4_rec_tru_lot, li.getreport_comp4_rec_tru_lot());
		values.put(report_comp4_rec_elevation, li.getreport_comp4_rec_elevation());
		values.put(report_comp4_rec_time_element, li.getreport_comp4_rec_time_element());
		values.put(report_comp4_rec_degree_of_devt, li.getreport_comp4_rec_degree_of_devt());
		values.put(report_comp4_concluded_adjustment, li.getreport_comp4_concluded_adjustment());
		values.put(report_comp4_adjusted_value, li.getreport_comp4_adjusted_value());
		values.put(report_comp4_remarks, li.getreport_comp4_remarks());
		// updating row
		return db.update(tbl_land_improvements, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating single land imp comp5
	public int updateLand_Improvements_comp5(Land_Improvements_API li, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp5_date_month, li.getreport_comp5_date_month());
		values.put(report_comp5_date_day, li.getreport_comp5_date_day());
		values.put(report_comp5_date_year, li.getreport_comp5_date_year());
		values.put(report_comp5_source, li.getreport_comp5_source());
		values.put(report_comp5_contact_no, li.getreport_comp5_contact_no());
		values.put(report_comp5_location, li.getreport_comp5_location()); 
		values.put(report_comp5_area, li.getreport_comp5_area());
		values.put(report_comp5_base_price, li.getreport_comp5_base_price());
		values.put(report_comp5_price_sqm, li.getreport_comp5_price_sqm());
		values.put(report_comp5_discount_rate, li.getreport_comp5_discount_rate());
		values.put(report_comp5_discounts, li.getreport_comp5_discounts());
		values.put(report_comp5_selling_price, li.getreport_comp5_selling_price());
		values.put(report_comp5_rec_location, li.getreport_comp5_rec_location());
		values.put(report_comp5_rec_size, li.getreport_comp5_rec_size());
		values.put(report_comp5_rec_shape, li.getreport_comp5_rec_shape());
		values.put(report_comp5_rec_corner_influence, li.getreport_comp5_rec_corner_influence());
		values.put(report_comp5_rec_tru_lot, li.getreport_comp5_rec_tru_lot());
		values.put(report_comp5_rec_elevation, li.getreport_comp5_rec_elevation());
		values.put(report_comp5_rec_time_element, li.getreport_comp5_rec_time_element());
		values.put(report_comp5_rec_degree_of_devt, li.getreport_comp5_rec_degree_of_devt());
		values.put(report_comp5_concluded_adjustment, li.getreport_comp5_concluded_adjustment());
		values.put(report_comp5_adjusted_value, li.getreport_comp5_adjusted_value());
		values.put(report_comp5_remarks, li.getreport_comp5_remarks());
		// updating row
		return db.update(tbl_land_improvements, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating single vacant comparatives
	public int updateLand_Improvements_comparatives(Land_Improvements_API vl, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_comp_average, vl.getreport_comp_average());
		values.put(report_comp_rounded_to, vl.getreport_comp_rounded_to());
		// updating row
		return db.update(tbl_land_improvements, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating single land improvements comparatives
	public int updateMotor_Vehicle_comparatives(Motor_Vehicle_API vl, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put("valrep_mv_comp_temp_ave_index_price", vl.getvalrep_mv_comp_temp_ave_index_price());
		values.put("valrep_mv_comp_add_index_price_desc", vl.getvalrep_mv_comp_add_index_price_desc());
		values.put("valrep_mv_comp_add_index_price", vl.getvalrep_mv_comp_add_index_price());
		values.put(report_comp_ave_index_price, vl.getreport_comp_ave_index_price());

		values.put("valrep_mv_rep_other2_desc", vl.getvalrep_mv_rep_other2_desc());
		values.put("valrep_mv_rep_other2", vl.getvalrep_mv_rep_other2());
		values.put("valrep_mv_rep_other3_desc", vl.getvalrep_mv_rep_other3_desc());
		values.put("valrep_mv_rep_other3", vl.getvalrep_mv_rep_other3());

		values.put(report_rec_lux_car, vl.getreport_rec_lux_car());
		values.put(report_rec_mile_factor, vl.getreport_rec_mile_factor());
		values.put(report_rec_taxi_use, vl.getreport_rec_taxi_use());
		values.put(report_rec_for_hire, vl.getreport_rec_for_hire());
		values.put(report_rec_condition, vl.getreport_rec_condition());
		values.put(report_rec_repo, vl.getreport_rec_repo());
		values.put(report_rec_other_val, vl.getreport_rec_other_val());
		values.put(report_rec_other_desc, vl.getreport_rec_other_desc());
		values.put(report_rec_other2_val, vl.getreport_rec_other2_val());
		values.put(report_rec_other2_desc, vl.getreport_rec_other2_desc());
		values.put(report_rec_other3_val, vl.getreport_rec_other3_val());
		values.put(report_rec_other3_desc, vl.getreport_rec_other3_desc());
		values.put(report_rec_market_resistance_rec_total, vl.getreport_rec_market_resistance_rec_total());
		values.put(report_rec_market_resistance_total, vl.getreport_rec_market_resistance_total());
		values.put(report_rep_stereo, vl.getreport_rep_stereo());
		values.put(report_rep_speakers, vl.getreport_rep_speakers());
		values.put(report_rep_tires_pcs, vl.getreport_rep_tires_pcs());
		values.put(report_rep_tires, vl.getreport_rep_tires());
		values.put(report_rep_side_mirror_pcs, vl.getreport_rep_side_mirror_pcs());
		values.put(report_rep_side_mirror, vl.getreport_rep_side_mirror());
		values.put(report_rep_light, vl.getreport_rep_light());
		values.put(report_rep_tools, vl.getreport_rep_tools());
		values.put(report_rep_battery, vl.getreport_rep_battery());
		values.put(report_rep_plates, vl.getreport_rep_plates());
		values.put(report_rep_bumpers, vl.getreport_rep_bumpers());
		values.put(report_rep_windows, vl.getreport_rep_windows());
		values.put(report_rep_body, vl.getreport_rep_body());
		values.put(report_rep_engine_wash, vl.getreport_rep_engine_wash());
		values.put(report_rep_other_desc, vl.getreport_rep_other_desc());
		values.put(report_rep_other, vl.getreport_rep_other());
		values.put(report_rep_total, vl.getreport_rep_total());
		values.put(report_appraised_value, vl.getreport_appraised_value());

		values.put(report_market_valuation_min, vl.getreport_market_valuation_min());
		values.put(report_market_valuation_max, vl.getreport_market_valuation_max());
		values.put(report_market_valuation_fair_value, vl.getreport_market_valuation_fair_value());
		// updating row
		return db.update(tbl_motor_vehicle, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}

	// Updating single land improvements comparatives
	public int updateMotor_Vehicle_comparatives_Ave(Motor_Vehicle_API vl, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put("valrep_mv_comp_temp_ave_index_price", vl.getvalrep_mv_comp_temp_ave_index_price());
		values.put("valrep_mv_comp_add_index_price", vl.getvalrep_mv_comp_add_index_price());
		values.put(report_comp_ave_index_price, vl.getreport_comp_ave_index_price());

		// updating row
		return db.update(tbl_motor_vehicle, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Updating land imp report summary
	public int updateLand_Improvements_Summary(Land_Improvements_API li,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_final_value_total_appraised_value_land_imp, li.getreport_final_value_total_appraised_value_land_imp());
		values.put(report_final_value_recommended_deductions_desc, li.getreport_final_value_recommended_deductions_desc());
		values.put(report_final_value_recommended_deductions_rate, li.getreport_final_value_recommended_deductions_rate());
		values.put(report_final_value_recommended_deductions_other, li.getreport_final_value_recommended_deductions_other());
		values.put(report_final_value_recommended_deductions_amt, li.getreport_final_value_recommended_deductions_amt());
		values.put(report_final_value_net_appraised_value, li.getreport_final_value_net_appraised_value());
		values.put(report_final_value_quick_sale_value_rate, li.getreport_final_value_quick_sale_value_rate());
		values.put(report_final_value_quick_sale_value_amt, li.getreport_final_value_quick_sale_value_amt());
		values.put(report_factors_of_concern, li.getreport_factors_of_concern());
		values.put(report_suggested_corrective_actions, li.getreport_suggested_corrective_actions());
		values.put(report_remarks, li.getreport_remarks());
		values.put(report_requirements, li.getreport_requirements());
		values.put(report_concerns_minor_1, li.getreport_concerns_minor_1());
		values.put(report_concerns_minor_2, li.getreport_concerns_minor_2());
		values.put(report_concerns_minor_3, li.getreport_concerns_minor_3());
		values.put(report_concerns_minor_others, li.getreport_concerns_minor_others());
		values.put(report_concerns_minor_others_desc, li.getreport_concerns_minor_others_desc());
		values.put(report_concerns_major_1, li.getreport_concerns_major_1());
		values.put(report_concerns_major_2, li.getreport_concerns_major_2());
		values.put(report_concerns_major_3, li.getreport_concerns_major_3());
		values.put(report_concerns_major_4, li.getreport_concerns_major_4());
		values.put(report_concerns_major_5, li.getreport_concerns_major_5());
		values.put(report_concerns_major_6, li.getreport_concerns_major_6());
		values.put(report_concerns_major_others, li.getreport_concerns_major_others());
		values.put(report_concerns_major_others_desc, li.getreport_concerns_major_others_desc());
		
		// updating row
		return db.update(tbl_land_improvements, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	
	// Updating land imp report summary
	public int updateLand_Improvements_Summary_Total(Land_Improvements_API li,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(report_final_value_total_appraised_value_land_imp, li.getreport_final_value_total_appraised_value_land_imp());
		return db.update(tbl_land_improvements, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	
	// Update total valuation
	public int updateLand_Improvements_Total_Valuation(Land_Improvements_API li,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_value_total_area, li.getreport_value_total_area());
		values.put(report_value_total_deduction, li.getreport_value_total_deduction());
		values.put(report_value_total_net_area, li.getreport_value_total_net_area());
		values.put(report_value_total_landimp_value, li.getreport_value_total_landimp_value());
		
		// updating row
		return db.update(tbl_land_improvements, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	// Update total valuation cost total
	public int updateLand_Improvements_Total_Valuation_Main(Land_Improvements_API li,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_imp_value_total_imp_value, li.getreport_imp_value_total_imp_value());
		// updating row
		return db.update(tbl_land_improvements, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}

	//getting the total land value
	public String getLand_Improvements_API_Total_imp_value(String record_id) {
		String result="0";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cur = db.rawQuery("SELECT coalesce(report_imp_value_total_imp_value,0) FROM " + tbl_land_improvements
				+ " WHERE " + Report_record_id + " =  \"" + record_id + "\"", null);
		if(cur.moveToFirst())
		{
			result= cur.getString(0);
		}


		db.close();

		return result;
	}


	
	// Updating land imp Zonal Values
	public int updateLand_Improvements_Zonal(Land_Improvements_API li,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_zonal_location, li.getreport_zonal_location());
		values.put(report_zonal_lot_classification, li.getreport_zonal_lot_classification());
		values.put(report_zonal_value, li.getreport_zonal_value());
		
		// updating row
		return db.update(tbl_land_improvements, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}

	// Updating single construction ebm 1
	public int updateConstruction_Ebm_page1(Construction_Ebm_API ce,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();

		values.put(report_date_inspected_month,ce.getreport_date_inspected_month());
		values.put(report_date_inspected_day, ce.getreport_date_inspected_day());
		values.put(report_date_inspected_year,ce.getreport_date_inspected_year());
		values.put(report_time_inspected,ce.getreport_time_inspected());
		values.put(report_project_type, ce.getreport_project_type());
		values.put(report_floor_area, ce.getreport_floor_area());
		values.put(report_storeys, ce.getreport_storeys());
		values.put(report_expected_economic_life, ce.getreport_expected_economic_life());
		values.put(report_type_of_housing_unit, ce.getreport_type_of_housing_unit());
		values.put(report_const_type_reinforced_concrete, ce.getreport_const_type_reinforced_concrete());
		values.put(report_const_type_semi_concrete, ce.getreport_const_type_semi_concrete());
		values.put(report_const_type_mixed_materials, ce.getreport_const_type_mixed_materials());
		// updating row
		return db.update(tbl_construction_ebm, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
/**
 * 
 */

	// Updating single construction ebm 2
	public int updateConstruction_Ebm_page2(Construction_Ebm_API ce,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		
		values.put(report_foundation_concrete, ce.getreport_foundation_concrete());
		values.put(report_foundation_other, ce.getreport_foundation_other());
		values.put(report_foundation_other_value, ce.getreport_foundation_other_value());
		values.put(report_post_concrete, ce.getreport_post_concrete());
		values.put(report_post_concrete_timber, ce.getreport_post_concrete_timber());
		values.put(report_post_steel, ce.getreport_post_steel());
		values.put(report_post_other, ce.getreport_post_other());
		values.put(report_post_other_value, ce.getreport_post_other_value());
		values.put(report_beams_concrete, ce.getreport_beams_concrete());
		values.put(report_beams_timber, ce.getreport_beams_timber());
		values.put(report_beams_steel, ce.getreport_beams_steel());
		values.put(report_beams_other, ce.getreport_beams_other());
		values.put(report_beams_other_value, ce.getreport_beams_other_value());
		values.put(report_floors_concrete, ce.getreport_floors_concrete());
		values.put(report_floors_tiles, ce.getreport_floors_tiles());
		values.put(report_floors_tiles_cement, ce.getreport_floors_tiles_cement());
		values.put(report_floors_laminated_wood, ce.getreport_floors_laminated_wood());
		values.put(report_floors_ceramic_tiles, ce.getreport_floors_ceramic_tiles());
		values.put(report_floors_wood_planks, ce.getreport_floors_wood_planks());
		values.put(report_floors_marble_washout, ce.getreport_floors_marble_washout());
		values.put(report_floors_concrete_boards, ce.getreport_floors_concrete_boards());
		values.put(report_floors_granite_tiles, ce.getreport_floors_granite_tiles());
		values.put(report_floors_marble_wood, ce.getreport_floors_marble_wood());
		values.put(report_floors_carpet, ce.getreport_floors_carpet());
		values.put(report_floors_other, ce.getreport_floors_other());
		values.put(report_floors_other_value, ce.getreport_floors_other_value());
		values.put(report_walls_chb, ce.getreport_walls_chb());
		values.put(report_walls_chb_cement, ce.getreport_walls_chb_cement());
		values.put(report_walls_anay, ce.getreport_walls_anay());
		values.put(report_walls_chb_wood, ce.getreport_walls_chb_wood());
		values.put(report_walls_precast, ce.getreport_walls_precast());
		values.put(report_walls_decorative_stone, ce.getreport_walls_decorative_stone());
		values.put(report_walls_adobe, ce.getreport_walls_adobe());
		values.put(report_walls_ceramic_tiles, ce.getreport_walls_ceramic_tiles());
		values.put(report_walls_cast_in_place, ce.getreport_walls_cast_in_place());
		values.put(report_walls_sandblast, ce.getreport_walls_sandblast());
		values.put(report_walls_mactan_stone, ce.getreport_walls_mactan_stone());
		values.put(report_walls_painted, ce.getreport_walls_painted());
		values.put(report_walls_other, ce.getreport_walls_other());
		values.put(report_walls_other_value, ce.getreport_walls_other_value());
		values.put(report_partitions_chb, ce.getreport_partitions_chb());
		values.put(report_partitions_painted_cement, ce.getreport_partitions_painted_cement());
		values.put(report_partitions_anay, ce.getreport_partitions_anay());
		values.put(report_partitions_wood_boards, ce.getreport_partitions_wood_boards());
		values.put(report_partitions_precast, ce.getreport_partitions_precast());
		values.put(report_partitions_decorative_stone, ce.getreport_partitions_decorative_stone());
		values.put(report_partitions_adobe, ce.getreport_partitions_adobe());
		values.put(report_partitions_granite, ce.getreport_partitions_granite());
		values.put(report_partitions_cast_in_place, ce.getreport_partitions_cast_in_place());
		values.put(report_partitions_sandblast, ce.getreport_partitions_sandblast());
		values.put(report_partitions_mactan_stone, ce.getreport_partitions_mactan_stone());
		values.put(report_partitions_ceramic_tiles, ce.getreport_partitions_ceramic_tiles());
		values.put(report_partitions_chb_plywood, ce.getreport_partitions_chb_plywood());
		values.put(report_partitions_hardiflex, ce.getreport_partitions_hardiflex());
		values.put(report_partitions_wallpaper, ce.getreport_partitions_wallpaper());
		values.put(report_partitions_painted, ce.getreport_partitions_painted());
		values.put(report_partitions_other, ce.getreport_partitions_other());
		values.put(report_partitions_other_value, ce.getreport_partitions_other_value());
		values.put(report_windows_steel_casement, ce.getreport_windows_steel_casement());
		values.put(report_windows_fixed_view, ce.getreport_windows_fixed_view());
		values.put(report_windows_analok_sliding, ce.getreport_windows_analok_sliding());
		values.put(report_windows_alum_glass, ce.getreport_windows_alum_glass());
		values.put(report_windows_aluminum_sliding, ce.getreport_windows_aluminum_sliding());
		values.put(report_windows_awning_type, ce.getreport_windows_awning_type());
		values.put(report_windows_powder_coated, ce.getreport_windows_powder_coated());
		values.put(report_windows_wooden_frame, ce.getreport_windows_wooden_frame());
		values.put(report_windows_other, ce.getreport_windows_other());
		values.put(report_windows_other_value, ce.getreport_windows_other_value());
		values.put(report_doors_wood_panel, ce.getreport_doors_wood_panel());
		values.put(report_doors_pvc, ce.getreport_doors_pvc());
		values.put(report_doors_analok_sliding, ce.getreport_doors_analok_sliding());
		values.put(report_doors_screen_door, ce.getreport_doors_screen_door());
		values.put(report_doors_flush, ce.getreport_doors_flush());
		values.put(report_doors_molded_door, ce.getreport_doors_molded_door());
		values.put(report_doors_aluminum_sliding, ce.getreport_doors_aluminum_sliding());
		values.put(report_doors_flush_french, ce.getreport_doors_flush_french());
		values.put(report_doors_other, ce.getreport_doors_other());
		values.put(report_doors_other_value, ce.getreport_doors_other_value());
		values.put(report_ceiling_plywood, ce.getreport_ceiling_plywood());
		values.put(report_ceiling_painted_gypsum, ce.getreport_ceiling_painted_gypsum());
		values.put(report_ceiling_soffit_slab, ce.getreport_ceiling_soffit_slab());
		values.put(report_ceiling_metal_deck, ce.getreport_ceiling_metal_deck());
		values.put(report_ceiling_hardiflex, ce.getreport_ceiling_hardiflex());
		values.put(report_ceiling_plywood_tg, ce.getreport_ceiling_plywood_tg());
		values.put(report_ceiling_plywood_pvc, ce.getreport_ceiling_plywood_pvc());
		values.put(report_ceiling_painted, ce.getreport_ceiling_painted());
		values.put(report_ceiling_with_cornice, ce.getreport_ceiling_with_cornice());
		values.put(report_ceiling_with_moulding, ce.getreport_ceiling_with_moulding());
		values.put(report_ceiling_drop_ceiling, ce.getreport_ceiling_drop_ceiling());
		values.put(report_ceiling_other, ce.getreport_ceiling_other());
		values.put(report_ceiling_other_value, ce.getreport_ceiling_other_value());
		values.put(report_roof_pre_painted, ce.getreport_roof_pre_painted());
		values.put(report_roof_rib_type, ce.getreport_roof_rib_type());
		values.put(report_roof_tilespan, ce.getreport_roof_tilespan());
		values.put(report_roof_tegula_asphalt, ce.getreport_roof_tegula_asphalt());
		values.put(report_roof_tegula_longspan, ce.getreport_roof_tegula_longspan());
		values.put(report_roof_tegula_gi, ce.getreport_roof_tegula_gi());
		values.put(report_roof_steel_concrete, ce.getreport_roof_steel_concrete());
		values.put(report_roof_polycarbonate, ce.getreport_roof_polycarbonate());
		values.put(report_roof_on_steel_trusses, ce.getreport_roof_on_steel_trusses());
		values.put(report_roof_on_wooden_trusses, ce.getreport_roof_on_wooden_trusses());
		values.put(report_roof_other, ce.getreport_roof_other());
		values.put(report_roof_other_value, ce.getreport_roof_other_value());

		// updating row
		return db.update(tbl_construction_ebm, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}
	
	// Updating single construction ebm 4
	public int updateConstruction_Ebm_page4(Construction_Ebm_API ce,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_valuation_remarks, ce.getreport_valuation_remarks());
		// updating row
		return db.update(tbl_construction_ebm, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}

	// Updating single construction ebm 1
	public int updateConstruction_Ebm_Total(Construction_Ebm_API ce,
			String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_valuation_total_area, ce.getreport_valuation_total_area());
		values.put(report_valuation_total_proj_cost, ce.getreport_valuation_total_proj_cost());
		// updating row
		return db.update(tbl_construction_ebm, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}

	// Updating single ppcr
	public int updatePpcr(Ppcr_API ppcr, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(report_date_inspected_month, ppcr.getreport_date_inspected_month());
		values.put(report_date_inspected_day, ppcr.getreport_date_inspected_day());
		values.put(report_date_inspected_year, ppcr.getreport_date_inspected_year());
		values.put(report_time_inspected, ppcr.getreport_time_inspected());
		values.put(report_registered_owner, ppcr.getreport_registered_owner());
		values.put(report_project_type, ppcr.getreport_project_type());
		values.put(report_is_condo, ppcr.getreport_is_condo());
		values.put(report_remarks, ppcr.getreport_remarks());
		// updating row
		return db.update(tbl_ppcr, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}


	// Deleting request accepted jobs
	public int deleteReport_Accepted_Jobs(Report_Accepted_Jobs raj) {
		SQLiteDatabase db = this.getWritableDatabase();
		// updating row
		return db.delete(tbl_report_accepted_jobs, Report_record_id + " = ?",
				new String[] { String.valueOf(raj.getrecord_id()) });
	}

	// Deleting request accepted jobs contacts
	public int deleteReport_Accepted_Jobs_Contacts(
			Report_Accepted_Jobs_Contacts raj) {
		SQLiteDatabase db = this.getWritableDatabase();
		// updating row
		return db.delete(tbl_report_accepted_jobs_contacts, Report_record_id
				+ " = ?", new String[] { String.valueOf(raj.getrecord_id()) });
	}

	// Deleting report filename
	public int deleteReport_filename(Report_filename rf) {
		SQLiteDatabase db = this.getWritableDatabase();
		// updating row
		return db.delete(tbl_report_filename, Report_record_id + " = ?",
				new String[] { String.valueOf(rf.getrecord_id()) });
	}

	// Deleting vacant lot
	public int deleteVacant_Lot_API(Vacant_Lot_API vl) {
		SQLiteDatabase db = this.getWritableDatabase();
		// updating row
		return db.delete(tbl_vacant_lot, Report_record_id + " = ?",
				new String[] { String.valueOf(vl.getrecord_id()) });
	}

	// Deleting Townhouse Condo
	public int deleteTownhouse_Condo_API(Townhouse_Condo_API tc) {
		SQLiteDatabase db = this.getWritableDatabase();
		// updating row
		return db.delete(tbl_townhouse_condo, Report_record_id + " = ?",
				new String[] { String.valueOf(tc.getrecord_id()) });
	}
	
	// Deleting Townhouse
	public int deleteTownhouse_API(Townhouse_API tc) {
		SQLiteDatabase db = this.getWritableDatabase();
		// updating row
		return db.delete(tbl_townhouse, Report_record_id + " = ?",
				new String[] { String.valueOf(tc.getrecord_id()) });
	}

	// Deleting Motor Vehicle
	public int deleteMotor_Vehicle_API(Motor_Vehicle_API mv) {
		SQLiteDatabase db = this.getWritableDatabase();
		// updating row
		return db.delete(tbl_motor_vehicle, Report_record_id + " = ?",
				new String[] { String.valueOf(mv.getrecord_id()) });
	}

	// Deleting Land Improvements
	public int deleteLand_Improvements_API(Land_Improvements_API li) {
		SQLiteDatabase db = this.getWritableDatabase();
		// updating row
		return db.delete(tbl_land_improvements, Report_record_id + " = ?",
				new String[] { String.valueOf(li.getrecord_id()) });
	}

	// Deleting Construction ebm
	public int deleteConstruction_Ebm_API(Construction_Ebm_API ce) {
		SQLiteDatabase db = this.getWritableDatabase();
		// updating row
		return db.delete(tbl_construction_ebm, Report_record_id + " = ?",
				new String[] { String.valueOf(ce.getrecord_id()) });
	}
	
	// Deleting Condo
	public int deleteCondo_API(Condo_API ce) {
		SQLiteDatabase db = this.getWritableDatabase();
		// updating row
		return db.delete(tbl_condo, Report_record_id + " = ?",
				new String[] { String.valueOf(ce.getrecord_id()) });
	}
	
	// Deleting Ppcr
	public int deletePpcr_API(Ppcr_API ppcr) {
		SQLiteDatabase db = this.getWritableDatabase();
		// updating row
		return db.delete(tbl_ppcr, Report_record_id + " = ?",
				new String[] { String.valueOf(ppcr.getrecord_id()) });
	}
	
	
	/*
	 * CRUD Logs
	 */
	
	public void addLogs(Logs logs) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Report_record_id, logs.getrecord_id());
		values.put("date_submitted", logs.getdate_submitted());
		values.put("status", logs.getstatus());
		values.put("status_reason", logs.getstatus_reason());
		db.insert(tbl_submission_logs, null, values);
		db.close(); // Closing database connection
	}
	
	public List<Logs> getLogs(String record_id) {
		List<Logs> logList = new ArrayList<Logs>();
		String selectQuery = "Select * FROM " + tbl_submission_logs + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				Logs logs = new Logs();
				logs.setID(Integer.parseInt(cursor.getString(0)));
				logs.setrecord_id(cursor.getString(1));
				logs.setdate_submitted(cursor.getString(2));
				logs.setstatus(cursor.getString(3));
				logs.setstatus_reason(cursor.getString(4));
				logList.add(logs);
			} while (cursor.moveToNext());
		}
		db.close();
		return logList;
	}
	
	public List<Logs> getLogsLatest(String record_id) {
		List<Logs> logList = new ArrayList<Logs>();
		String status_reason = "";
		String selectQuery = "Select * FROM " + tbl_submission_logs + " WHERE " + Report_record_id + " =  \"" + record_id +
				"\" ORDER BY id DESC LIMIT 1";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				Logs logs = new Logs();
				logs.setID(Integer.parseInt(cursor.getString(0)));
				logs.setrecord_id(cursor.getString(1));
				logs.setdate_submitted(cursor.getString(2));
				logs.setstatus(cursor.getString(3));
				logs.setstatus_reason(cursor.getString(4));
				logList.add(logs);
			} while (cursor.moveToNext());
		} 
		db.close();
		return logList;
	}
	
	public int deleteLogs(Logs logs) {
		SQLiteDatabase db = this.getWritableDatabase();
		// updating row
		return db.delete(tbl_submission_logs, Report_record_id + " = ?",
				new String[] { String.valueOf(logs.getrecord_id()) });
	}
	
	
	public void log_status(String record_id,
			boolean online,
			boolean connectedToServer,
			boolean fastInternet,
			boolean fieldsComplete,
			boolean sent
			){
		//get current date and time and submit to CC
		TimeZone tz = TimeZone.getTimeZone(gs.gmt);
		Calendar c = Calendar.getInstance(tz);
		
		int cMonth = c.get(Calendar.MONTH);//starts with 0 for january
		int cDay = c.get(Calendar.DAY_OF_MONTH);
		int cYear = c.get(Calendar.YEAR);
		
//		int cAmPm = c.get(Calendar.AM_PM);
//		
//		String sAmPm="";
//		if (cAmPm==0){
//			sAmPm = "AM";
//		} else if (cAmPm==1){
//			sAmPm = "PM";
//		}
		
		
		String cTime = String.format("%02d" , c.get(Calendar.HOUR_OF_DAY))+":"+
				String.format("%02d" , c.get(Calendar.MINUTE));
		
		gs.date_submitted = String.valueOf(cMonth+1)+"-"+String.valueOf(cDay)+"-"+String.valueOf(cYear)+" "+cTime;

		
		if (!sent){
			gs.status_reason = "Disconnected";
			gs.status = "Failed";
			
			if (!fieldsComplete){
				gs.status_reason = gs.status_reason_ary[3];
				gs.status = "Failed";
				
				if (!fastInternet){
					gs.status_reason = gs.status_reason_ary[2];
					gs.status = "Failed";					
					
					if (!connectedToServer){
						gs.status_reason = gs.status_reason_ary[1];
						gs.status = "Failed";						
						
						if (!online){
							gs.status_reason = gs.status_reason_ary[0];
							gs.status = "Failed";
						}
					}
				}
			}
		}else{
			gs.status_reason = gs.status_reason_ary[4];
			gs.status = "Success";
		}
			
		
		addLogs(new Logs(record_id, gs.date_submitted, gs.status, gs.status_reason));
	}
	//August 18 2017
	//methods
	public void addRecord(ArrayList<String> value, ArrayList<String> fields, String table){

		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();

		for (int i = 0; i < value.size(); i++) {
			values.put(fields.get(i).toString(), value.get(i).toString());
		}

		db.insert(table, null, values);
		//  db.close();
	}
	//fields
	ArrayList<String> common_details = new ArrayList<String>();

	public ArrayList<String> MV_free_fields_mobile(){
		common_details.clear();
		common_details.add("record_id");
		common_details.add("report_interior_dashboard");
		common_details.add("report_interior_sidings");
		common_details.add("report_interior_seats");
		common_details.add("report_interior_windows");
		common_details.add("report_interior_ceiling");
		common_details.add("report_interior_instrument");
		common_details.add("report_bodytype_grille");
		common_details.add("report_bodytype_headlight");
		common_details.add("report_bodytype_fbumper");
		common_details.add("report_bodytype_hood");
		common_details.add("report_bodytype_rf_fender");
		common_details.add("report_bodytype_rf_door");
		common_details.add("report_bodytype_rr_door");
		common_details.add("report_bodytype_rr_fender");
		common_details.add("report_bodytype_backdoor");
		common_details.add("report_bodytype_taillight");
		common_details.add("report_bodytype_r_bumper");
		common_details.add("report_bodytype_lr_fender");
		common_details.add("report_bodytype_lr_door");
		common_details.add("report_bodytype_lf_door");
		common_details.add("report_bodytype_lf_fender");
		common_details.add("report_bodytype_top");
		common_details.add("report_bodytype_paint");
		common_details.add("report_bodytype_flooring");
		common_details.add("report_misc_stereo");
		common_details.add("report_misc_tools");
		common_details.add("report_misc_speakers");
		common_details.add("report_misc_airbag");
		common_details.add("report_misc_tires");
		common_details.add("report_misc_mag_wheels");
		common_details.add("report_misc_carpet");
		common_details.add("report_misc_others");
		common_details.add("report_enginearea_fuel");
		common_details.add("report_enginearea_transmission1");
		common_details.add("report_enginearea_transmission2");
		common_details.add("report_enginearea_chassis");
		common_details.add("report_enginearea_battery");
		common_details.add("report_enginearea_electrical");
		common_details.add("report_enginearea_engine");
		common_details.add("report_verification");
		common_details.add("report_verification_district_office");
		common_details.add("report_verification_encumbrance");
		common_details.add("report_verification_registered_owner");
		common_details.add("report_place_inspected");
		common_details.add("report_market_valuation_fair_value");
		common_details.add("report_market_valuation_min");
		common_details.add("report_market_valuation_max");
		common_details.add("report_market_valuation_remarks");
		common_details.add("report_date_inspected_month");
		common_details.add("report_date_inspected_day");
		common_details.add("report_date_inspected_year");
		common_details.add("report_time_inspected");
		common_details.add("report_comp1_source");
		common_details.add("report_comp1_tel_no");
		common_details.add("report_comp1_unit_model");
		common_details.add("report_comp1_mileage");
		common_details.add("report_comp1_price_min");
		common_details.add("report_comp1_price_max");
		common_details.add("report_comp1_price");
		common_details.add("report_comp1_less_amt");
		common_details.add("report_comp1_less_net");
		common_details.add("report_comp1_add_amt");
		common_details.add("report_comp1_add_net");
		common_details.add("report_comp1_time_adj_value");
		common_details.add("report_comp1_time_adj_amt");
		common_details.add("report_comp1_adj_value");
		common_details.add("report_comp1_adj_amt");
		common_details.add("report_comp1_index_price");
		common_details.add("report_comp2_source");
		common_details.add("report_comp2_tel_no");
		common_details.add("report_comp2_unit_model");
		common_details.add("report_comp2_mileage");
		common_details.add("report_comp2_price_min");
		common_details.add("report_comp2_price_max");
		common_details.add("report_comp2_price");
		common_details.add("report_comp2_less_amt");
		common_details.add("report_comp2_less_net");
		common_details.add("report_comp2_add_amt");
		common_details.add("report_comp2_add_net");
		common_details.add("report_comp2_time_adj_value");
		common_details.add("report_comp2_time_adj_amt");
		common_details.add("report_comp2_adj_value");
		common_details.add("report_comp2_adj_amt");
		common_details.add("report_comp2_index_price");
		common_details.add("report_comp3_source");
		common_details.add("report_comp3_tel_no");
		common_details.add("report_comp3_unit_model");
		common_details.add("report_comp3_mileage");
		common_details.add("report_comp3_price_min");
		common_details.add("report_comp3_price_max");
		common_details.add("report_comp3_price");
		common_details.add("report_comp3_less_amt");
		common_details.add("report_comp3_less_net");
		common_details.add("report_comp3_add_amt");
		common_details.add("report_comp3_add_net");
		common_details.add("report_comp3_time_adj_value");
		common_details.add("report_comp3_time_adj_amt");
		common_details.add("report_comp3_adj_value");
		common_details.add("report_comp3_adj_amt");
		common_details.add("report_comp3_index_price");
		common_details.add("report_comp_ave_index_price");
		common_details.add("report_rec_lux_car");
		common_details.add("report_rec_mile_factor");
		common_details.add("report_rec_taxi_use");
		common_details.add("report_rec_for_hire");
		common_details.add("report_rec_condition");
		common_details.add("report_rec_repo");
		common_details.add("report_rec_other_val");
		common_details.add("report_rec_other_desc");
		common_details.add("report_rec_market_resistance_rec_total");
		common_details.add("report_rec_market_resistance_total");
		common_details.add("report_rec_market_resistance_net_total");
		common_details.add("report_rep_stereo");
		common_details.add("report_rep_speakers");
		common_details.add("report_rep_tires_pcs");
		common_details.add("report_rep_tires");
		common_details.add("report_rep_side_mirror_pcs");
		common_details.add("report_rep_side_mirror");
		common_details.add("report_rep_light");
		common_details.add("report_rep_tools");
		common_details.add("report_rep_battery");
		common_details.add("report_rep_plates");
		common_details.add("report_rep_bumpers");
		common_details.add("report_rep_windows");
		common_details.add("report_rep_body");
		common_details.add("report_rep_engine_wash");
		common_details.add("report_rep_other_desc");
		common_details.add("report_rep_other");
		common_details.add("report_rep_total");
		common_details.add("report_appraised_value");
		common_details.add("report_verification_file_no");
		common_details.add("report_verification_first_reg_date_month");
		common_details.add("report_verification_first_reg_date_day");
		common_details.add("report_verification_first_reg_date_year");
		common_details.add("report_comp1_mileage_value");
		common_details.add("report_comp2_mileage_value");
		common_details.add("report_comp3_mileage_value");
		common_details.add("report_comp1_mileage_amt");
		common_details.add("report_comp2_mileage_amt");
		common_details.add("report_comp3_mileage_amt");
		common_details.add("report_comp1_adj_desc");
		common_details.add("report_comp2_adj_desc");
		common_details.add("report_comp3_adj_desc");
		common_details.add("report_rec_other2_val");
		common_details.add("report_rec_other2_desc");
		common_details.add("report_rec_other3_val");
		common_details.add("report_rec_other3_desc");
		common_details.add("valrep_mv_bodytype_trunk");
		common_details.add("valrep_mv_market_valuation_previous_remarks");
		common_details.add("valrep_mv_comp1_new_unit_value");
		common_details.add("valrep_mv_comp2_new_unit_value");
		common_details.add("valrep_mv_comp3_new_unit_value");
		common_details.add("valrep_mv_comp1_new_unit_amt");
		common_details.add("valrep_mv_comp2_new_unit_amt");
		common_details.add("valrep_mv_comp3_new_unit_amt");
		common_details.add("valrep_mv_comp_add_index_price_desc");
		common_details.add("valrep_mv_comp_add_index_price");
		common_details.add("valrep_mv_comp_temp_ave_index_price");
		common_details.add("valrep_mv_rep_other2_desc");
		common_details.add("valrep_mv_rep_other2");
		common_details.add("valrep_mv_rep_other3_desc");
		common_details.add("valrep_mv_rep_other3");
		return common_details;
	}
	public String[] MV_free_fields_cc = new String [] {
			"valrep_mv_interior_dashboard",
			"valrep_mv_interior_sidings",
			"valrep_mv_interior_seats",
			"valrep_mv_interior_windows",
			"valrep_mv_interior_ceiling",
			"valrep_mv_interior_instrument",
			"valrep_mv_bodytype_grille",
			"valrep_mv_bodytype_headlight",
			"valrep_mv_bodytype_fbumper",
			"valrep_mv_bodytype_hood",
			"valrep_mv_bodytype_rf_fender",
			"valrep_mv_bodytype_rf_door",
			"valrep_mv_bodytype_rr_door",
			"valrep_mv_bodytype_rr_fender",
			"valrep_mv_bodytype_backdoor",
			"valrep_mv_bodytype_taillight",
			"valrep_mv_bodytype_r_bumper",
			"valrep_mv_bodytype_lr_fender",
			"valrep_mv_bodytype_lr_door",
			"valrep_mv_bodytype_lf_door",
			"valrep_mv_bodytype_lf_fender",
			"valrep_mv_bodytype_top",
			"valrep_mv_bodytype_paint",
			"valrep_mv_bodytype_flooring",
			"valrep_mv_misc_stereo",
			"valrep_mv_misc_tools",
			"valrep_mv_misc_speakers",
			"valrep_mv_misc_airbag",
			"valrep_mv_misc_tires",
			"valrep_mv_misc_mag_wheels",
			"valrep_mv_misc_carpet",
			"valrep_mv_misc_others",
			"valrep_mv_enginearea_fuel",
			"valrep_mv_enginearea_transmission1",
			"valrep_mv_enginearea_transmission2",
			"valrep_mv_enginearea_chassis",
			"valrep_mv_enginearea_battery",
			"valrep_mv_enginearea_electrical",
			"valrep_mv_enginearea_engine",
			"valrep_mv_verification",
			"valrep_mv_verification_district_office",
			"valrep_mv_verification_encumbrance",
			"valrep_mv_verification_registered_owner",
			"valrep_mv_place_inspected",
			"valrep_mv_market_valuation_fair_value",
			"valrep_mv_market_valuation_min",
			"valrep_mv_market_valuation_max",
			"valrep_mv_market_valuation_remarks",
			"valrep_mv_date_inspected_month",
			"valrep_mv_date_inspected_day",
			"valrep_mv_date_inspected_year",
			"valrep_mv_time_inspected",
			"valrep_mv_comp1_source",
			"valrep_mv_comp1_tel_no",
			"valrep_mv_comp1_unit_model",
			"valrep_mv_comp1_mileage",
			"valrep_mv_comp1_price_min",
			"valrep_mv_comp1_price_max",
			"valrep_mv_comp1_price",
			"valrep_mv_comp1_less_amt",
			"valrep_mv_comp1_less_net",
			"valrep_mv_comp1_add_amt",
			"valrep_mv_comp1_add_net",
			"valrep_mv_comp1_time_adj_value",
			"valrep_mv_comp1_time_adj_amt",
			"valrep_mv_comp1_adj_value",
			"valrep_mv_comp1_adj_amt",
			"valrep_mv_comp1_index_price",
			"valrep_mv_comp2_source",
			"valrep_mv_comp2_tel_no",
			"valrep_mv_comp2_unit_model",
			"valrep_mv_comp2_mileage",
			"valrep_mv_comp2_price_min",
			"valrep_mv_comp2_price_max",
			"valrep_mv_comp2_price",
			"valrep_mv_comp2_less_amt",
			"valrep_mv_comp2_less_net",
			"valrep_mv_comp2_add_amt",
			"valrep_mv_comp2_add_net",
			"valrep_mv_comp2_time_adj_value",
			"valrep_mv_comp2_time_adj_amt",
			"valrep_mv_comp2_adj_value",
			"valrep_mv_comp2_adj_amt",
			"valrep_mv_comp2_index_price",
			"valrep_mv_comp3_source",
			"valrep_mv_comp3_tel_no",
			"valrep_mv_comp3_unit_model",
			"valrep_mv_comp3_mileage",
			"valrep_mv_comp3_price_min",
			"valrep_mv_comp3_price_max",
			"valrep_mv_comp3_price",
			"valrep_mv_comp3_less_amt",
			"valrep_mv_comp3_less_net",
			"valrep_mv_comp3_add_amt",
			"valrep_mv_comp3_add_net",
			"valrep_mv_comp3_time_adj_value",
			"valrep_mv_comp3_time_adj_amt",
			"valrep_mv_comp3_adj_value",
			"valrep_mv_comp3_adj_amt",
			"valrep_mv_comp3_index_price",
			"valrep_mv_comp_ave_index_price",
			"valrep_mv_rec_lux_car",
			"valrep_mv_rec_mile_factor",
			"valrep_mv_rec_taxi_use",
			"valrep_mv_rec_for_hire",
			"valrep_mv_rec_condition",
			"valrep_mv_rec_repo",
			"valrep_mv_rec_other_val",
			"valrep_mv_rec_other_desc",
			"valrep_mv_rec_market_resistance_rec_total",
			"valrep_mv_rec_market_resistance_total",
			"valrep_mv_rec_market_resistance_net_total",
			"valrep_mv_rep_stereo",
			"valrep_mv_rep_speakers",
			"valrep_mv_rep_tires_pcs",
			"valrep_mv_rep_tires",
			"valrep_mv_rep_side_mirror_pcs",
			"valrep_mv_rep_side_mirror",
			"valrep_mv_rep_light",
			"valrep_mv_rep_tools",
			"valrep_mv_rep_battery",
			"valrep_mv_rep_plates",
			"valrep_mv_rep_bumpers",
			"valrep_mv_rep_windows",
			"valrep_mv_rep_body",
			"valrep_mv_rep_engine_wash",
			"valrep_mv_rep_other_desc",
			"valrep_mv_rep_other",
			"valrep_mv_rep_total",
			"valrep_mv_appraised_value",
			"valrep_mv_verification_file_no",
			"valrep_mv_verification_first_reg_date_month",
			"valrep_mv_verification_first_reg_date_day",
			"valrep_mv_verification_first_reg_date_year",
			"valrep_mv_comp1_mileage_value",
			"valrep_mv_comp2_mileage_value",
			"valrep_mv_comp3_mileage_value",
			"valrep_mv_comp1_mileage_amt",
			"valrep_mv_comp2_mileage_amt",
			"valrep_mv_comp3_mileage_amt",
			"valrep_mv_comp1_adj_desc",
			"valrep_mv_comp2_adj_desc",
			"valrep_mv_comp3_adj_desc",
			"valrep_mv_rec_other2_val",
			"valrep_mv_rec_other2_desc",
			"valrep_mv_rec_other3_val",
			"valrep_mv_rec_other3_desc",
			"valrep_mv_bodytype_trunk",
			"valrep_mv_market_valuation_previous_remarks",
			"valrep_mv_comp1_new_unit_value",
			"valrep_mv_comp2_new_unit_value",
			"valrep_mv_comp3_new_unit_value",
			"valrep_mv_comp1_new_unit_amt",
			"valrep_mv_comp2_new_unit_amt",
			"valrep_mv_comp3_new_unit_amt",
			"valrep_mv_comp_add_index_price_desc",
			"valrep_mv_comp_add_index_price",
			"valrep_mv_comp_temp_ave_index_price",
			"valrep_mv_rep_other2_desc",
			"valrep_mv_rep_other2",
			"valrep_mv_rep_other3_desc",
			"valrep_mv_rep_other3"

	};
	public ArrayList<String> MV_prev_fields_mobile(){
		common_details.clear();
		common_details.add("record_id");
		common_details.add("report_prev_date");
		common_details.add("report_prev_appraiser");
		common_details.add("report_prev_requestor");
		common_details.add("report_prev_appraised_value");
		return common_details;
	}


	public String[] MV_prev_fields_cc = new String [] {
			"valrep_mv_prev_date",
			"valrep_mv_prev_appraiser",
			"valrep_mv_prev_requestor",
			"valrep_mv_prev_appraised_value"

	};
}