package com.gds.appraisalmaybank.database;

public class Townhouse_Condo_API {

	// private variables
	int id;
	String record_id;
	String report_cct_no;
	String report_area;
	String report_date_reg_month;
	String report_date_reg_day;
	String report_date_reg_year;
	String report_reg_owner;
	String report_homeowners_assoc;
	String report_admin_office;
	String report_tax_mapping;
	String report_lra_office;
	String report_subd_map;
	String report_lot_config;
	String report_street;
	String report_sidewalk;
	String report_gutter;
	String report_street_lights;
	String report_shape;
	String report_elevation;
	String report_road;
	String report_frontage;
	String report_depth;
	String report_electricity;
	String report_water;
	String report_telephone;
	String report_drainage;
	String report_garbage;
	String report_jeepneys;
	String report_bus;
	String report_taxi;
	String report_tricycle;
	String report_recreation_facilities;
	String report_security;
	String report_clubhouse;
	String report_swimming_pool;
	String report_community;
	String report_classification;
	String report_growth_rate;
	String report_adverse;
	String report_occupancy;
	String report_right;
	String report_left;
	String report_rear;
	String report_front;
	String report_desc;
	String report_total_area;
	String report_deduc;
	String report_net_area;
	String report_unit_value;
	String report_total_parking_area;
	String report_parking_area_value;
	String report_total_condo_value;
	String report_total_parking_value;
	String report_total_market_value;
	String report_date_inspected_month;
	String report_date_inspected_day;
	String report_date_inspected_year;

	// Empty constructor
	public Townhouse_Condo_API() {

	}

	// constructor
	public Townhouse_Condo_API(int id, String record_id, String report_cct_no,
			String report_area, String report_date_reg_month,
			String report_date_reg_day, String report_date_reg_year,
			String report_reg_owner, String report_homeowners_assoc,
			String report_admin_office, String report_tax_mapping,
			String report_lra_office, String report_subd_map,
			String report_lot_config, String report_street,
			String report_sidewalk, String report_gutter,
			String report_street_lights, String report_shape,
			String report_elevation, String report_road,
			String report_frontage, String report_depth,
			String report_electricity, String report_water,
			String report_telephone, String report_drainage,
			String report_garbage, String report_jeepneys, String report_bus,
			String report_taxi, String report_tricycle,
			String report_recreation_facilities, String report_security,
			String report_clubhouse, String report_swimming_pool,
			String report_community, String report_classification,
			String report_growth_rate, String report_adverse,
			String report_occupancy, String report_right, String report_left,
			String report_rear, String report_front, String report_desc,
			String report_total_area, String report_deduc,
			String report_net_area, String report_unit_value,
			String report_total_parking_area, String report_parking_area_value,
			String report_total_condo_value, String report_total_parking_value,
			String report_total_market_value,
			String report_date_inspected_month,
			String report_date_inspected_day, String report_date_inspected_year) {
		this.id = id;
		this.record_id = record_id;
		this.report_cct_no = report_cct_no;
		this.report_area = report_area;
		this.report_date_reg_month = report_date_reg_month;
		this.report_date_reg_day = report_date_reg_day;
		this.report_date_reg_year = report_date_reg_year;
		this.report_reg_owner = report_reg_owner;
		this.report_homeowners_assoc = report_homeowners_assoc;
		this.report_admin_office = report_admin_office;
		this.report_tax_mapping = report_tax_mapping;
		this.report_lra_office = report_lra_office;
		this.report_subd_map = report_subd_map;
		this.report_lot_config = report_lot_config;
		this.report_street = report_street;
		this.report_sidewalk = report_sidewalk;
		this.report_gutter = report_gutter;
		this.report_street_lights = report_street_lights;
		this.report_shape = report_shape;
		this.report_elevation = report_elevation;
		this.report_road = report_road;
		this.report_frontage = report_frontage;
		this.report_depth = report_depth;
		this.report_electricity = report_electricity;
		this.report_water = report_water;
		this.report_telephone = report_telephone;
		this.report_drainage = report_drainage;
		this.report_garbage = report_garbage;
		this.report_jeepneys = report_jeepneys;
		this.report_bus = report_bus;
		this.report_taxi = report_taxi;
		this.report_tricycle = report_tricycle;
		this.report_recreation_facilities = report_recreation_facilities;
		this.report_security = report_security;
		this.report_clubhouse = report_clubhouse;
		this.report_swimming_pool = report_swimming_pool;
		this.report_community = report_community;
		this.report_classification = report_classification;
		this.report_growth_rate = report_growth_rate;
		this.report_adverse = report_adverse;
		this.report_occupancy = report_occupancy;
		this.report_right = report_right;
		this.report_left = report_left;
		this.report_rear = report_rear;
		this.report_front = report_front;
		this.report_desc = report_desc;
		this.report_total_area = report_total_area;
		this.report_deduc = report_deduc;
		this.report_net_area = report_net_area;
		this.report_unit_value = report_unit_value;
		this.report_total_parking_area = report_total_parking_area;
		this.report_parking_area_value = report_parking_area_value;
		this.report_total_condo_value = report_total_condo_value;
		this.report_total_parking_value = report_total_parking_value;
		this.report_total_market_value = report_total_market_value;
		this.report_date_inspected_month = report_date_inspected_month;
		this.report_date_inspected_day = report_date_inspected_day;
		this.report_date_inspected_year = report_date_inspected_year;
	}

	// constructor
	public Townhouse_Condo_API(String record_id, String report_cct_no,
			String report_area, String report_date_reg_month,
			String report_date_reg_day, String report_date_reg_year,
			String report_reg_owner, String report_homeowners_assoc,
			String report_admin_office, String report_tax_mapping,
			String report_lra_office, String report_subd_map,
			String report_lot_config, String report_street,
			String report_sidewalk, String report_gutter,
			String report_street_lights, String report_shape,
			String report_elevation, String report_road,
			String report_frontage, String report_depth,
			String report_electricity, String report_water,
			String report_telephone, String report_drainage,
			String report_garbage, String report_jeepneys, String report_bus,
			String report_taxi, String report_tricycle,
			String report_recreation_facilities, String report_security,
			String report_clubhouse, String report_swimming_pool,
			String report_community, String report_classification,
			String report_growth_rate, String report_adverse,
			String report_occupancy, String report_right, String report_left,
			String report_rear, String report_front, String report_desc,
			String report_total_area, String report_deduc,
			String report_net_area, String report_unit_value,
			String report_total_parking_area, String report_parking_area_value,
			String report_total_condo_value, String report_total_parking_value,
			String report_total_market_value,
			String report_date_inspected_month,
			String report_date_inspected_day, String report_date_inspected_year) {

		this.record_id = record_id;
		this.report_cct_no = report_cct_no;
		this.report_area = report_area;
		this.report_date_reg_month = report_date_reg_month;
		this.report_date_reg_day = report_date_reg_day;
		this.report_date_reg_year = report_date_reg_year;
		this.report_reg_owner = report_reg_owner;
		this.report_homeowners_assoc = report_homeowners_assoc;
		this.report_admin_office = report_admin_office;
		this.report_tax_mapping = report_tax_mapping;
		this.report_lra_office = report_lra_office;
		this.report_subd_map = report_subd_map;
		this.report_lot_config = report_lot_config;
		this.report_street = report_street;
		this.report_sidewalk = report_sidewalk;
		this.report_gutter = report_gutter;
		this.report_street_lights = report_street_lights;
		this.report_shape = report_shape;
		this.report_elevation = report_elevation;
		this.report_road = report_road;
		this.report_frontage = report_frontage;
		this.report_depth = report_depth;
		this.report_electricity = report_electricity;
		this.report_water = report_water;
		this.report_telephone = report_telephone;
		this.report_drainage = report_drainage;
		this.report_garbage = report_garbage;
		this.report_jeepneys = report_jeepneys;
		this.report_bus = report_bus;
		this.report_taxi = report_taxi;
		this.report_tricycle = report_tricycle;
		this.report_recreation_facilities = report_recreation_facilities;
		this.report_security = report_security;
		this.report_clubhouse = report_clubhouse;
		this.report_swimming_pool = report_swimming_pool;
		this.report_community = report_community;
		this.report_classification = report_classification;
		this.report_growth_rate = report_growth_rate;
		this.report_adverse = report_adverse;
		this.report_occupancy = report_occupancy;
		this.report_right = report_right;
		this.report_left = report_left;
		this.report_rear = report_rear;
		this.report_front = report_front;
		this.report_desc = report_desc;
		this.report_total_area = report_total_area;
		this.report_deduc = report_deduc;
		this.report_net_area = report_net_area;
		this.report_unit_value = report_unit_value;
		this.report_total_parking_area = report_total_parking_area;
		this.report_parking_area_value = report_parking_area_value;
		this.report_total_condo_value = report_total_condo_value;
		this.report_total_parking_value = report_total_parking_value;
		this.report_total_market_value = report_total_market_value;
		this.report_date_inspected_month = report_date_inspected_month;
		this.report_date_inspected_day = report_date_inspected_day;
		this.report_date_inspected_year = report_date_inspected_year;
	}

	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	public String getrecord_id() {
		return this.record_id;
	}

	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}

	public String getreport_cct_no() {
		return this.report_cct_no;
	}

	public void setreport_cct_no(String report_cct_no) {
		this.report_cct_no = report_cct_no;
	}

	public String getreport_area() {
		return this.report_area;
	}

	public void setreport_area(String report_area) {
		this.report_area = report_area;
	}

	public String getreport_date_reg_month() {
		return this.report_date_reg_month;
	}

	public void setreport_date_reg_month(String report_date_reg_month) {
		this.report_date_reg_month = report_date_reg_month;
	}

	public String getreport_date_reg_day() {
		return this.report_date_reg_day;
	}

	public void setreport_date_reg_day(String report_date_reg_day) {
		this.report_date_reg_day = report_date_reg_day;
	}

	public String getreport_date_reg_year() {
		return this.report_date_reg_year;
	}

	public void setreport_date_reg_year(String report_date_reg_year) {
		this.report_date_reg_year = report_date_reg_year;
	}

	public String getreport_reg_owner() {
		return this.report_reg_owner;
	}

	public void setreport_reg_owner(String report_reg_owner) {
		this.report_reg_owner = report_reg_owner;
	}

	public String getreport_homeowners_assoc() {
		return this.report_homeowners_assoc;
	}

	public void setreport_homeowners_assoc(String report_homeowners_assoc) {
		this.report_homeowners_assoc = report_homeowners_assoc;
	}

	public String getreport_admin_office() {
		return this.report_admin_office;
	}

	public void setreport_admin_office(String report_admin_office) {
		this.report_admin_office = report_admin_office;
	}

	public String getreport_tax_mapping() {
		return this.report_tax_mapping;
	}

	public void setreport_tax_mapping(String report_tax_mapping) {
		this.report_tax_mapping = report_tax_mapping;
	}

	public String getreport_lra_office() {
		return this.report_lra_office;
	}

	public void setreport_lra_office(String report_lra_office) {
		this.report_lra_office = report_lra_office;
	}

	public String getreport_subd_map() {
		return this.report_subd_map;
	}

	public void setreport_subd_map(String report_subd_map) {
		this.report_subd_map = report_subd_map;
	}

	public String getreport_lot_config() {
		return this.report_lot_config;
	}

	public void setreport_lot_config(String report_lot_config) {
		this.report_lot_config = report_lot_config;
	}

	public String getreport_street() {
		return this.report_street;
	}

	public void setreport_street(String report_street) {
		this.report_street = report_street;
	}

	public String getreport_sidewalk() {
		return this.report_sidewalk;
	}

	public void setreport_sidewalk(String report_sidewalk) {
		this.report_sidewalk = report_sidewalk;
	}

	public String getreport_gutter() {
		return this.report_gutter;
	}

	public void setreport_gutter(String report_gutter) {
		this.report_gutter = report_gutter;
	}

	public String getreport_street_lights() {
		return this.report_street_lights;
	}

	public void setreport_street_lights(String report_street_lights) {
		this.report_street_lights = report_street_lights;
	}

	public String getreport_shape() {
		return this.report_shape;
	}

	public void setreport_shape(String report_shape) {
		this.report_shape = report_shape;
	}

	public String getreport_elevation() {
		return this.report_elevation;
	}

	public void setreport_elevation(String report_elevation) {
		this.report_elevation = report_elevation;
	}

	public String getreport_road() {
		return this.report_road;
	}

	public void setreport_road(String report_road) {
		this.report_road = report_road;
	}

	public String getreport_frontage() {
		return this.report_frontage;
	}

	public void setreport_frontage(String report_frontage) {
		this.report_frontage = report_frontage;
	}

	public String getreport_depth() {
		return this.report_depth;
	}

	public void setreport_depth(String report_depth) {
		this.report_depth = report_depth;
	}

	public String getreport_electricity() {
		return this.report_electricity;
	}

	public void setreport_electricity(String report_electricity) {
		this.report_electricity = report_electricity;
	}

	public String getreport_water() {
		return this.report_water;
	}

	public void setreport_water(String report_water) {
		this.report_water = report_water;
	}

	public String getreport_telephone() {
		return this.report_telephone;
	}

	public void setreport_telephone(String report_telephone) {
		this.report_telephone = report_telephone;
	}

	public String getreport_drainage() {
		return this.report_drainage;
	}

	public void setreport_drainage(String report_drainage) {
		this.report_drainage = report_drainage;
	}

	public String getreport_garbage() {
		return this.report_garbage;
	}

	public void setreport_garbage(String report_garbage) {
		this.report_garbage = report_garbage;
	}

	public String getreport_jeepneys() {
		return this.report_jeepneys;
	}

	public void setreport_jeepneys(String report_jeepneys) {
		this.report_jeepneys = report_jeepneys;
	}

	public String getreport_bus() {
		return this.report_bus;
	}

	public void setreport_bus(String report_bus) {
		this.report_bus = report_bus;
	}

	public String getreport_taxi() {
		return this.report_bus;
	}

	public void setreport_taxi(String report_taxi) {
		this.report_taxi = report_taxi;
	}

	public String getreport_tricycle() {
		return this.report_tricycle;
	}

	public void setreport_tricycle(String report_tricycle) {
		this.report_tricycle = report_tricycle;
	}

	public String getreport_recreation_facilities() {
		return this.report_recreation_facilities;
	}

	public void setreport_recreation_facilities(
			String report_recreation_facilities) {
		this.report_recreation_facilities = report_recreation_facilities;
	}

	public String getreport_security() {
		return this.report_security;
	}

	public void setreport_security(String report_security) {
		this.report_security = report_security;
	}

	public String getreport_clubhouse() {
		return this.report_clubhouse;
	}

	public void setreport_clubhouse(String report_clubhouse) {
		this.report_clubhouse = report_clubhouse;
	}

	public String getreport_swimming_pool() {
		return this.report_swimming_pool;
	}

	public void setreport_swimming_pool(String report_swimming_pool) {
		this.report_swimming_pool = report_swimming_pool;
	}

	public String getreport_community() {
		return this.report_community;
	}

	public void setreport_community(String report_community) {
		this.report_community = report_community;
	}

	public String getreport_classification() {
		return this.report_classification;
	}

	public void setreport_classification(String report_classification) {
		this.report_classification = report_classification;
	}

	public String getreport_growth_rate() {
		return this.report_growth_rate;
	}

	public void setreport_growth_rate(String report_growth_rate) {
		this.report_growth_rate = report_growth_rate;
	}

	public String getreport_adverse() {
		return this.report_adverse;
	}

	public void setreport_adverse(String report_adverse) {
		this.report_adverse = report_adverse;
	}

	public String getreport_occupancy() {
		return this.report_occupancy;
	}

	public void setreport_occupancy(String report_occupancy) {
		this.report_occupancy = report_occupancy;
	}

	public String getreport_right() {
		return this.report_right;
	}

	public void setreport_right(String report_right) {
		this.report_right = report_right;
	}

	public String getreport_left() {
		return this.report_left;
	}

	public void setreport_left(String report_left) {
		this.report_left = report_left;
	}

	public String getreport_rear() {
		return this.report_rear;
	}

	public void setreport_rear(String report_rear) {
		this.report_rear = report_rear;
	}

	public String getreport_front() {
		return this.report_front;
	}

	public void setreport_front(String report_front) {
		this.report_front = report_front;
	}

	public String getreport_desc() {
		return this.report_desc;
	}

	public void setreport_desc(String report_desc) {
		this.report_desc = report_desc;
	}

	public String getreport_total_area() {
		return this.report_total_area;
	}

	public void setreport_total_area(String report_total_area) {
		this.report_total_area = report_total_area;
	}

	public String getreport_deduc() {
		return this.report_deduc;
	}

	public void setreport_deduc(String report_deduc) {
		this.report_deduc = report_deduc;
	}

	public String getreport_net_area() {
		return this.report_net_area;
	}

	public void setreport_net_area(String report_net_area) {
		this.report_net_area = report_net_area;
	}

	public String getreport_unit_value() {
		return this.report_unit_value;
	}

	public void setreport_unit_value(String report_unit_value) {
		this.report_unit_value = report_unit_value;
	}

	public String getreport_total_parking_area() {
		return this.report_total_parking_area;
	}

	public void setreport_total_parking_area(String report_total_parking_area) {
		this.report_total_parking_area = report_total_parking_area;
	}

	public String getreport_parking_area_value() {
		return this.report_parking_area_value;
	}

	public void setreport_parking_area_value(String report_parking_area_value) {
		this.report_parking_area_value = report_parking_area_value;
	}

	public String getreport_total_condo_value() {
		return this.report_total_condo_value;
	}

	public void setreport_total_condo_value(String report_total_condo_value) {
		this.report_total_condo_value = report_total_condo_value;
	}

	public String getreport_total_parking_value() {
		return this.report_total_parking_value;
	}

	public void setreport_total_parking_value(String report_total_parking_value) {
		this.report_total_parking_value = report_total_parking_value;
	}

	public String getreport_total_market_value() {
		return this.report_total_market_value;
	}

	public void setreport_total_market_value(String report_total_market_value) {
		this.report_total_market_value = report_total_market_value;
	}

	public String getreport_date_inspected_month() {
		return this.report_date_inspected_month;
	}

	public void setreport_date_inspected_month(
			String report_date_inspected_month) {
		this.report_date_inspected_month = report_date_inspected_month;
	}

	public String getreport_date_inspected_day() {
		return this.report_date_inspected_day;
	}

	public void setreport_date_inspected_day(String report_date_inspected_day) {
		this.report_date_inspected_day = report_date_inspected_day;
	}

	public String getreport_date_inspected_year() {
		return this.report_date_inspected_year;
	}

	public void setreport_date_inspected_year(String report_date_inspected_year) {
		this.report_date_inspected_year = report_date_inspected_year;
	}

}
