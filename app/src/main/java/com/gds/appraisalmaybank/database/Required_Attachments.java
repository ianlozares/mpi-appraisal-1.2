package com.gds.appraisalmaybank.database;

public class Required_Attachments {

	// private variables
	int id;
	String attachment;
	String remarks;
	String appraisal_type;

	// Empty constructor
	public Required_Attachments() {

	}

	// constructor
	public Required_Attachments(int id, String attachment, String remarks,
			String appraisal_type) {
		this.id = id;
		this.attachment = attachment;
		this.remarks = remarks;
		this.appraisal_type = appraisal_type;
	}

	// constructor
	public Required_Attachments(String attachment, String remarks,
			String appraisal_type) {

		this.attachment = attachment;
		this.remarks = remarks;
		this.appraisal_type = appraisal_type;
	}

	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	// getting attachment
	public String getattachment() {
		return this.attachment;
	}

	// setting attachment
	public void setattachment(String attachment) {
		this.attachment = attachment;
	}

	// getting remarks
	public String getremarks() {
		return this.remarks;
	}

	// setting remarks
	public void setremarks(String remarks) {
		this.remarks = remarks;
	}

	// getting appraisal_type
	public String getappraisal_type() {
		return this.appraisal_type;
	}

	// setting appraisal_type
	public void setappraisal_type(String appraisal_type) {
		this.appraisal_type = appraisal_type;
	}

}
