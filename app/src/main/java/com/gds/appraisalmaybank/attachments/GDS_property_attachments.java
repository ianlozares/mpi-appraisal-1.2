package com.gds.appraisalmaybank.attachments;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Images;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ianlo on 13/05/2016.
 */
public class GDS_property_attachments extends Activity {
    DatabaseHandler db = new DatabaseHandler(this);

    Session_Timer st = new Session_Timer(this);
    ActionBar actionBar;

    private int count;
    Global gs;
    private boolean[] thumbnailsselection;
    private String[] arrPath;
    private int[] arrId;
    public String[] arrlabel;
    public String[] arrsize;
    String pdf_name;
    String time_stamp="",time_xml="";
    List<String> selectedImages = new ArrayList<String>();
    List<String> selectedLabel = new ArrayList<String>();
    List<String> selectedSize = new ArrayList<String>();
    private static final String TAG_RECORD_ID = "record_id";
    ImageView image;
    EditText label;
    Spinner size;

    int id, image_column_index;

    DatabaseHandler2 db2 = new DatabaseHandler2(this);
    GDS_methods gds = new GDS_methods();
    String record_id;
    SharedPreferences loginPreferences;

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_TAKE_PHOTO = 1;

    Cursor imagecursor;

    protected Cursor mCursor;
    protected int columnIndex, thumbnail_image_id;
    protected GridView mGridView;
    protected ImageAdapter mAdapter;
    //int position;
    File storageDir;

    AlertDialog.Builder builder, image_builder;
    AlertDialog createpdf_form, image_form;


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // TODO Auto-generated method stub
        gds.menuItems(menu, "Property Attachments");

        menu.findItem(R.id.action_camera).setVisible(true);
        menu.findItem(R.id.action_collage).setVisible(true);
        menu.findItem(R.id.action_done).setVisible(true);
        menu.findItem(R.id.action_refresh).setVisible(true);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // TODO Auto-generated method stub
        getMenuInflater().inflate(R.menu.reportsmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        actionBar = getActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffc80b")));
        actionBar.setIcon(R.mipmap.gds_icon);
        actionBar.setTitle("Property Attachments");
        setContentView(R.layout.gds_property_attachments_grid);
        st.resetDisconnectTimer();
        Intent i = getIntent();
        record_id = i.getStringExtra(TAG_RECORD_ID);

        builder = new AlertDialog.Builder(GDS_property_attachments.this);
        image_builder = new AlertDialog.Builder(GDS_property_attachments.this);
        final String[] columns = { MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID };

        imagecursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                columns,
                null,
                null,
                MediaStore.Images.Media.DATE_ADDED + " DESC");

        image_column_index = imagecursor.getColumnIndex(MediaStore.Images.Media._ID);
        this.count = imagecursor.getCount();
        this.arrPath = new String[this.count];
        this.arrId = new int[this.count];
        this.arrlabel = new String[this.count];
        this.arrsize = new String[this.count];
        this.thumbnailsselection = new boolean[this.count];

        // Get the GridView layout
        mGridView = (GridView) findViewById(R.id.gridView);
        mAdapter = new ImageAdapter(this);
        mGridView.setAdapter(mAdapter);

        // loginPreferences = getSharedPreferences("loginPrefs", 0);

        String where ="WHERE record_id = args";
        String[] args = new String[1];
        args[0] = record_id;
        String act_lname = db2.getRecord("lname", where, args, "tbl_report_accepted_jobs").get(0);
        String act_fname = db2.getRecord("fname", where, args, "tbl_report_accepted_jobs").get(0);
        String act_rmonth = db2.getRecord("dr_month", where, args, "tbl_report_accepted_jobs").get(0);
        String act_rday = db2.getRecord("dr_day", where, args, "tbl_report_accepted_jobs").get(0);
        String act_ryear = db2.getRecord("dr_year", where, args, "tbl_report_accepted_jobs").get(0);
        String app_main_id = db2.getRecord("app_main_id", where, args, "tbl_report_accepted_jobs").get(0);

        pdf_name = app_main_id + "_"
                + "Unit Pictures" + "_0.pdf";

        // new getTimestamp().execute();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        Log.e("backpress", "backpress");
        finish();
        super.onBackPressed();
    }

    private void dispatchTakePictureIntent() {
        Log.e("dispatch", "step 1");
        //Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Intent takePictureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                //...
                Log.e("",""+ex);
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }

        }
    }

    String mCurrentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        Log.e("create image file", "step 2");
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "" + timeStamp + "_";

        storageDir = new File(Environment.getExternalStorageDirectory() + "/gds_appraisal");

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        mCurrentPhotoPath = image.toString();
        return image;
    }

    private void galleryAddPic() {

        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, contentUri);
        sendBroadcast(mediaScanIntent);

        image_builder.setTitle("Property Image")
                .setCancelable(false)
                .setMessage("Do you want to capture another picture?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        // TODO Auto-generated method stub

                        dispatchTakePictureIntent();

                    }

                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        // TODO Auto-generated method stub
                        gds.reloadClass(getIntent(), GDS_property_attachments.this);
                    }

                });

        image_form = image_builder.create();
        image_form.show();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        Log.e("onActivityResult", "onActivityResult");
        //if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            Log.e("REQUEST_IMAGE_CAPTURE && RESULT_OK", "final");
            galleryAddPic();
        }

    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub

        switch(item.getItemId()){

            case R.id.action_refresh:
                gds.reloadClass(getIntent(), GDS_property_attachments.this);
                return true;

            case R.id.action_camera:
                dispatchTakePictureIntent();
                return true;

            case R.id.action_collage:
                try{
                    Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.asus.collage");
                    startActivity(launchIntent);
                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Please install Photo Collage - Layout Editor from Playstore.", Toast.LENGTH_SHORT).show();
                    final String appPackageName = "com.asus.collage"; // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }
                return true;

            case R.id.action_done:

                final int len = thumbnailsselection.length;
                int cnt = 0;


                selectedImages.clear();
                selectedLabel.clear();
                selectedSize.clear();

                for (int i =0; i<len; i++){
                    if (thumbnailsselection[i]){
                        cnt++;
                        selectedImages.add(arrPath[i]);
                        selectedLabel.add(arrlabel[i]);
                        selectedSize.add(arrsize[i]);

                    }
                }

                if (cnt == 0){
                    Toast.makeText(getApplicationContext(),
                            "Please select at least one image",
                            Toast.LENGTH_LONG).show();
                } else {

                    create_pdf();
                    db_pdf();
                }

                return true;

            default:

                return true;

        }

    }

    //thumbnail
    class ImageAdapter extends BaseAdapter {

        private Context mContext;

        public ImageAdapter(Context context) {
            mContext = context;
        }

        @Override
        public int getCount() {
            return count;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        // Convert DP to PX
        // Source: http://stackoverflow.com/a/8490361
        public int dpToPx(int dps) {
            final float scale = getResources().getDisplayMetrics().density;
            int pixels = (int) (dps * scale + 0.5f);

            return pixels;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView;
            CheckBox checkBox;
            int imageID = 0;

            // Want the width/height of the items
            // to be 120dp
            int wPixel = dpToPx(120);
            int hPixel = dpToPx(120);

            if (convertView == null) {
                // If convertView is null then inflate the appropriate layout file
                convertView = LayoutInflater.from(mContext).inflate(R.layout.grid_item_objview, null);
            }
            else {

            }

            imageView = (ImageView) convertView.findViewById(R.id.iv_image);
            checkBox = (CheckBox) convertView.findViewById(R.id.cb_box);

            // Set height and width constraints for the image view
            imageView.setLayoutParams(new LinearLayout.LayoutParams(wPixel, hPixel));

            imagecursor.moveToPosition(position);
            id = imagecursor.getInt(image_column_index);
            arrId[position] = id;

            arrPath[position] = imagecursor.getString(imagecursor.getColumnIndex(MediaStore.Images.Media.DATA));


            imageView.setImageBitmap(MediaStore.Images.Thumbnails.getThumbnail(
                    getApplicationContext().getContentResolver(), id,
                    MediaStore.Images.Thumbnails.MICRO_KIND, null));

            checkBox.setChecked(thumbnailsselection[position]);

            final int cb_index = position;
            checkBox.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    CheckBox cb = (CheckBox) v;
                    if (thumbnailsselection[cb_index]){
                        cb.setChecked(false);
                        thumbnailsselection[cb_index] = false;
                    } else {
                        cb.setChecked(true);
                        thumbnailsselection[cb_index] = true;

                        builder.setTitle("Page details")
                                .setCancelable(false)
                                .setView(getLayoutInflater().inflate(R.layout.pdf_selectedimage_objview, null))
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        // TODO Auto-generated method stub

                                        arrlabel[cb_index] = label.getText().toString();
                                        arrsize[cb_index] = size.getSelectedItem().toString();

                                    }

                                });

                        createpdf_form = builder.create();
                        createpdf_form.show();

                        image = (ImageView) createpdf_form.findViewById(R.id.iv_image);
                        label = (EditText) createpdf_form.findViewById(R.id.et_label);
                        size = (Spinner) createpdf_form.findViewById(R.id.sp_size);

                        image.setImageBitmap(MediaStore.Images.Thumbnails.getThumbnail(
                                getApplicationContext().getContentResolver(), arrId[cb_index],
                                MediaStore.Images.Thumbnails.MICRO_KIND, null));

                    }
                }
            });

            // Image should be cropped towards the center
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

            // Set Padding for images
            imageView.setPadding(8, 8, 8, 8);

            // Crop the image to fit within its padding
            imageView.setCropToPadding(true);

            return convertView;
        }
    }

    public void create_pdf() {
        // step 1: creation of a document-object
        Document document = new Document();

        try {
            Log.e("try", "try");
            // step 2:
            // we create a writer that listens to the document
            // and directs a PDF-stream to a file
            PdfWriter.getInstance(document, new FileOutputStream(
                    android.os.Environment.getExternalStorageDirectory()
                            + java.io.File.separator + "gds_appraisal"
                            + java.io.File.separator + pdf_name));

            Log.e("file output", android.os.Environment.getExternalStorageDirectory()
                    + java.io.File.separator + "gds_appraisal"
                    + java.io.File.separator + pdf_name);


            // step 3: we open the document
            document.open();

            Log.e("size", ""+selectedImages.size());

            for (int x = 0; x < selectedImages.size(); x++) {
                if (!selectedImages.get(x).equals("")) {
                    document.setPageSize(PageSize.LETTER);
                    document.newPage();
                    // image
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    /*BitmapFactory.Options opts = new BitmapFactory.Options();
                    opts.inScaled = false;
                    Bitmap bitmap = BitmapFactory
                            .decodeFile(selectedImages.get(x),opts);
                    String size = null;
                    if(selectedSize.get(x).contentEquals("Half page")){
                     size = "500-365";
                    }else if(selectedSize.get(x).contentEquals("Whole page")){
                       size = "500-620";
                    }
                    String filenameArray[] = size.split("\\-");
                    int width = Integer.parseInt(filenameArray[0]);
                    int height = Integer.parseInt(filenameArray[1]);
                    Bitmap out = Bitmap.createScaledBitmap(bitmap, width,
                            height, false);
                    out.compress(Bitmap.CompressFormat.PNG,
                            100, stream);*/

                    // Get the source image's dimensions
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(selectedImages.get(x), options);

                    int srcWidth = options.outWidth;
                    int srcHeight = options.outHeight;
                    int desiredWidth=0;

                    if(selectedSize.get(x).contentEquals("Half page")){
                        // size = "500-365";
                        desiredWidth=365;
                    }else if(selectedSize.get(x).contentEquals("Whole page")){
                        // size = "500-620";
                        desiredWidth=620;
                    }
                    // Only scale if the source is big enough. This code is just trying to fit a image into a certain width.
                    if(desiredWidth > srcWidth){
                        desiredWidth = srcWidth;
                    }

// Calculate the correct inSampleSize/scale value. This helps reduce memory use. It should be a power of 2
// from: http://stackoverflow.com/questions/477572/android-strange-out-of-memory-issue/823966#823966
                    int inSampleSize = 1;
                    while(srcWidth / 2 > desiredWidth){
                        srcWidth /= 2;
                        srcHeight /= 2;
                        inSampleSize *= 2;
                    }
                    float desiredScale = (float) desiredWidth / srcWidth;
// Decode with inSampleSize
                    options.inJustDecodeBounds = false;
                    options.inDither = false;
                    options.inSampleSize = inSampleSize;
                    options.inScaled = false;
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    Bitmap sampledSrcBitmap = BitmapFactory.decodeFile(selectedImages.get(x), options);

// Resize
                    Matrix matrix = new Matrix();
                    matrix.postScale(desiredScale, desiredScale);
                    Bitmap scaledBitmap = Bitmap.createBitmap(sampledSrcBitmap, 0, 0, sampledSrcBitmap.getWidth(), sampledSrcBitmap.getHeight(), matrix, true);
                    sampledSrcBitmap = null;


                    scaledBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    scaledBitmap = null;

                    Image jpg = Image.getInstance(stream.toByteArray());
                    jpg.setAlignment(com.lowagie.text.Image.MIDDLE);
                    document.add(jpg);

                    Paragraph[] p = new Paragraph[1];
                    String printSpace = "";
                    if(x==0)
                        printSpace = "\n\n\n";
                    p[0] = new Paragraph(printSpace + selectedLabel.get(x));
                    p[0].setAlignment(Element.ALIGN_CENTER);
                    document.add(p[0]);

                }
            }


            Toast.makeText(getApplicationContext(),
                    pdf_name+" created.",
                    Toast.LENGTH_LONG).show();


        } catch (DocumentException de) {
            System.err.println(de.getMessage());
            Log.e("DocumentException de", "DocumentException de");
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
            Log.e("IOException ioe", "IOException ioe");
        }

        // step 5: we close the document
        document.close();


    }

    public void db_pdf() {
        File myDirectory = new File(Environment.getExternalStorageDirectory() + "/gds_appraisal");
        List<Images> report_accepted_jobs = db.getImages_single(pdf_name);
        if (report_accepted_jobs.isEmpty()) {
            db.addImages(new Images(myDirectory.toString(), pdf_name));
        }

       /* Toast.makeText(getApplicationContext(), pdf_name + " created",
                Toast.LENGTH_SHORT).show();
        finish();*/
        finish();
    }
    public void onUserInteraction(){
        st.resetDisconnectTimer();
    }
    @Override
    public void onStop() {
        super.onStop();
        st.stopDisconnectTimer();
    }
    @Override
    public void onResume() {
        super.onResume();
        st.resetDisconnectTimer();
    }


}
