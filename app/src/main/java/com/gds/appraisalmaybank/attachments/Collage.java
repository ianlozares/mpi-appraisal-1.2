package com.gds.appraisalmaybank.attachments;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Images;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.Session_Timer;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by ianlo on 12/07/2016.
 */
public class Collage extends AppCompatActivity implements View.OnClickListener {

     String dir = "gds_appraisal";
    String pdf_name = "_photos.pdf";//record_id or account name +_photos.pdf
    String record_id;//record_id or account name
//    DatabaseHandler db;
Session_Timer st = new Session_Timer(this);
    int counter = -1;
    int activeViewCounter = -1;
    int ivID = 0;
    int tvID = 0;
    DatabaseHandler2 db2 = new DatabaseHandler2(this);
    String appraisal_type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.collage);
        st.resetDisconnectTimer();
        setTitle("Create Attachment");
        getSupportActionBar().setSubtitle("Collage");
//        getSupportActionBar().setIcon(R.drawable.ic_username);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);//for icon
//        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        container = (LinearLayout) findViewById(R.id.container);
        Intent i = getIntent();
        record_id = i.getStringExtra("record_id");

        /*file_prefix = "file_prefix";//fetch your record id or account name
        pdf_name = file_prefix+pdf_name;*/


        String where ="WHERE record_id = args";
        String[] args = new String[1];
        args[0] = record_id;
        String act_lname = db2.getRecord("lname", where, args, "tbl_report_accepted_jobs").get(0);
        String act_fname = db2.getRecord("fname", where, args, "tbl_report_accepted_jobs").get(0);
        String act_rmonth = db2.getRecord("dr_month", where, args, "tbl_report_accepted_jobs").get(0);
        String act_rday = db2.getRecord("dr_day", where, args, "tbl_report_accepted_jobs").get(0);
        String act_ryear = db2.getRecord("dr_year", where, args, "tbl_report_accepted_jobs").get(0);
        String app_main_id = db2.getRecord("app_main_id", where, args, "tbl_report_accepted_jobs").get(0);
         appraisal_type = db2.getRecord("appraisal_type", where, args, "tbl_report_accepted_jobs").get(0);

        if(appraisal_type.contentEquals("motor_vehicle")){
            pdf_name = app_main_id + "_"
                    + "Unit Pictures" + "_0.pdf";
        }else{
            pdf_name = app_main_id + "_"
                    + "Property Pictures" + "_0.pdf";
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_collage, menu);
        return true;
    }


    Context context = this;
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_camera:
                if (!checkCamPermission()) {
                    requestCamPermission();
                } else {
                    dispatchTakePictureIntent();//start camera
                }
                break;
            case R.id.menu_restart:
                new AlertDialog.Builder(this)
                        .setTitle("Start a new collage")
                        .setMessage("This will clear all images and start a new collage creation")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                                startActivity(getIntent());
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .setIcon(R.drawable.restart_black48)
                        .show();
                break;
            case R.id.img1:
                counter++;
                inflate1(this);
                break;
            case R.id.img2:
                counter++;
                inflate2(this);
                break;
            case R.id.img4:
                counter++;
                inflate4(this);
                break;
            case R.id.action_pdf:

                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.yes_no_dialog);

                Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
                Button btn_no = (Button) dialog.findViewById(R.id.btn_no);
                // set the custom dialog components - text, image and button
                TextView text = (TextView) dialog.findViewById(R.id.tv_question);
                dialog.show();
                if(appraisal_type.contentEquals("motor_vehicle")){
                    text.setText("Are you sure you want to create the Unit Pictures into pdf?");
                }else{
                    text.setText("Are you sure you want to create the Property Pictures into pdf?");
                }

                // if button is clicked, close the custom dialog
                btn_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (container.getChildCount() > 0) {
                            create_pdf();
                            db_pdf();
                        } else {
                            Snackbar.make(container,"Please add a page and attach a photo.",Snackbar.LENGTH_LONG).show();
                        }
                        dialog.dismiss();
                    }
                });
                btn_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });


                break;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onClick(View arg0) {
        // TODO Auto-generated method stub
        switch (arg0.getId()) {
//            case R.id.logout:
//                startActivity(new Intent(getApplicationContext(), Add_Copra_Buying.class));
//                finish();
//                break;
            default:
                break;
        }
    }


    private boolean checkCamPermission(){
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }

    private static final int PERMISSION_CAM_REQUEST_CODE = 0;//request code permisson of camera should not be the same as select photo
    private void requestCamPermission(){
//        startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", getPackageName(), null)));//direct to app settings permission
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.CAMERA)){
            Snackbar.make(container,"Camera permission allows us to capture photos.",Snackbar.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA},PERMISSION_CAM_REQUEST_CODE);//prompt
        } else {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA},PERMISSION_CAM_REQUEST_CODE);
        }
    }

    static final int REQUEST_TAKE_PHOTO = 0;//request code of camera and select photo should not be the same

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                //...
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            } else {
//                Toast.makeText(getApplicationContext(), "Null", Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(), "Click again the camera button", Toast.LENGTH_LONG).show();
            }

        }
    }


    String mCurrentPhotoPath;
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "GDS_" + timeStamp;
//        File storageDir = new File(Environment.getExternalStorageDirectory() + getString(R.string.directory, Property_Pictures.this));
        File storageDir = new File(Environment.getExternalStorageDirectory() +"/"+ dir);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
//        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mCurrentPhotoPath = image.toString();
        return image;

    }

    private void galleryAddPic() {
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, contentUri);
        sendBroadcast(mediaScanIntent);
    }




    // this is the action code we use in our intent,
    // this way we know we're looking at the response from our own action
    private static final int SELECT_PICTURE = 1;
    private String selectedImagePath;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO) {
            galleryAddPic();
        }
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                selectedImagePath = getPath(selectedImageUri);
//                Glide.with(context).load(selectedImagePath).into(selectedImageVIew);
                setImageToView();
            }
        }
    }

    /**
     * helper to retrieve the path of an image URI
     */
    public String getPath(Uri uri) {
        // just some safety built in
        if( uri == null ) {
            // TODO perform some logging or show user feedback
            return null;
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if( cursor != null ){
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        // this is our fallback here
        return uri.getPath();
    }


    void openGallery(){
        if (!checkPermission()) {
            requestPermission();
        } else {
            // in onCreate or any event where your want the user to
            // select a file
            Intent intent = new Intent();
            intent.setType("image/*");
//                    intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.setAction(Intent.ACTION_PICK);//direct to gallery instead of folders with recent
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
        }
    }

    void setImageToView(){

        //load to imageview
        Glide.with(context).load(selectedImagePath).into(
//                (ImageView) container.getChildAt(0).findViewById(R.id.iv0)
                (ImageView) container.getChildAt( activeViewCounter ).findViewById( ivID )
        );

        //ask for label
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        final EditText edittext = new EditText(getBaseContext());

//        alert.setIcon(R.drawable.question_black24);
        alert.setTitle("Label");
        alert.setMessage("Enter description of photo:");

        edittext.setBackgroundColor(getResources().getColor(R.color.white ));
        edittext.requestFocus();
        edittext.setLines(4);
        edittext.setMinLines(4);
        edittext.setMaxLines(4);
        edittext.setTextColor(getResources().getColor(R.color.black));
        edittext.setPadding(15, 15, 15, 15);

        alert.setView(edittext);

        alert.setPositiveButton("Set Label", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
//                TextView tv = (TextView) container.getChildAt(0).findViewById(R.id.tv0);
                TextView tv = (TextView) container.getChildAt( activeViewCounter ).findViewById( tvID );
                tv.setText(edittext.getText().toString());
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        alert.show();
    }

    LinearLayout container;
    LayoutInflater layoutInflater;
    View addView;

    void inflate1(final Context context){
        layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        addView = layoutInflater.inflate(R.layout.inflater_1img, null);

        final ImageView iv0 = (ImageView) addView.findViewById(R.id.iv0);
        iv0.setScaleType(ImageView.ScaleType.FIT_XY);
        final TextView tv0 = (TextView) addView.findViewById(R.id.tv0);
        final TextView tv_counter = (TextView) addView.findViewById(R.id.tv_counter);
        final TextView tv_footer = (TextView) addView.findViewById(R.id.tv_footer);

        addView.setEnabled(false);
        addView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
            }
        });

        tv_counter.setText(""+counter);
        tv_footer.setText("Attachments - Page "+(counter+1));

        iv0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
                ivID = v.getId();
                tvID = tv0.getId();
                openGallery();
            }
        });
        iv0.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
//                Glide.with(context).load(R.drawable.blank_add).into(iv0);
                Glide.with(context).load(R.drawable.blank_add).into((ImageView) v);//get clicked iv
                tv0.setText("");
                return true;
            }
        });

        container.addView(addView);
    }


    void inflate2(final Context context){
        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(R.layout.inflater_2img, null);

        final ImageView iv0 = (ImageView) addView.findViewById(R.id.iv0);
        iv0.setScaleType(ImageView.ScaleType.FIT_XY);
        final TextView tv0 = (TextView) addView.findViewById(R.id.tv0);

        final ImageView iv1 = (ImageView) addView.findViewById(R.id.iv1);
        iv1.setScaleType(ImageView.ScaleType.FIT_XY);
        final TextView tv1 = (TextView) addView.findViewById(R.id.tv1);

        final TextView tv_counter = (TextView) addView.findViewById(R.id.tv_counter);
        final TextView tv_footer = (TextView) addView.findViewById(R.id.tv_footer);

        addView.setEnabled(false);
        addView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
            }
        });

        tv_counter.setText(""+counter);
        tv_footer.setText("Attachments - Page "+(counter+1));

        iv0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
                ivID = v.getId();
                tvID = tv0.getId();
                openGallery();
            }
        });
        iv0.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
//                Glide.with(context).load(R.drawable.blank_add).into(iv0);
                Glide.with(context).load(R.drawable.blank_add).into((ImageView) v);//get clicked iv
                tv0.setText("");
                return true;
            }
        });

        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
                ivID = v.getId();
                tvID = tv1.getId();
                openGallery();
            }
        });
        iv1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
//                Glide.with(context).load(R.drawable.blank_add).into(iv0);
                Glide.with(context).load(R.drawable.blank_add).into((ImageView) v);//get clicked iv
                tv1.setText("");
                return true;
            }
        });

        container.addView(addView);
    }

    void inflate4(final Context context){
        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(R.layout.inflater_4img, null);

        final ImageView iv0 = (ImageView) addView.findViewById(R.id.iv0);
        iv0.setScaleType(ImageView.ScaleType.FIT_XY);
        final TextView tv0 = (TextView) addView.findViewById(R.id.tv0);

        final ImageView iv1 = (ImageView) addView.findViewById(R.id.iv1);
        iv1.setScaleType(ImageView.ScaleType.FIT_XY);
        final TextView tv1 = (TextView) addView.findViewById(R.id.tv1);

        final ImageView iv2 = (ImageView) addView.findViewById(R.id.iv2);
        iv2.setScaleType(ImageView.ScaleType.FIT_XY);
        final TextView tv2 = (TextView) addView.findViewById(R.id.tv2);

        final ImageView iv3 = (ImageView) addView.findViewById(R.id.iv3);
        iv3.setScaleType(ImageView.ScaleType.FIT_XY);
        final TextView tv3 = (TextView) addView.findViewById(R.id.tv3);

        final TextView tv_counter = (TextView) addView.findViewById(R.id.tv_counter);
        final TextView tv_footer = (TextView) addView.findViewById(R.id.tv_footer);

        addView.setEnabled(false);
        addView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

            }
        });

        tv_counter.setText(""+counter);
        tv_footer.setText("Attachments - Page "+(counter+1));

        iv0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
                ivID = v.getId();
                tvID = tv0.getId();
                openGallery();
            }
        });
        iv0.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
//                Glide.with(context).load(R.drawable.blank_add).into(iv0);
                Glide.with(context).load(R.drawable.blank_add).into((ImageView) v);//get clicked iv
                tv0.setText("");
                return true;
            }
        });

        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
                ivID = v.getId();
                tvID = tv1.getId();
                openGallery();
            }
        });
        iv1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
//                Glide.with(context).load(R.drawable.blank_add).into(iv0);
                Glide.with(context).load(R.drawable.blank_add).into((ImageView) v);//get clicked iv
                tv1.setText("");
                return true;
            }
        });

        iv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
                ivID = v.getId();
                tvID = tv2.getId();
                openGallery();
            }
        });
        iv2.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
//                Glide.with(context).load(R.drawable.blank_add).into(iv0);
                Glide.with(context).load(R.drawable.blank_add).into((ImageView) v);//get clicked iv
                tv2.setText("");
                return true;
            }
        });

        iv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeViewCounter = Integer.parseInt(tv_counter.getText().toString());
                ivID = v.getId();
                tvID = tv3.getId();
                openGallery();
            }
        });
        iv3.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
//                Glide.with(context).load(R.drawable.blank_add).into(iv0);
                Glide.with(context).load(R.drawable.blank_add).into((ImageView) v);//get clicked iv
                tv3.setText("");
                return true;
            }
        });

        container.addView(addView);
    }


    void create_pdf() {
        //Then take the screen shot
        /*Bitmap screen;
        View v1 = container.getRootView();
        v1.setDrawingCacheEnabled(true);
        screen = Bitmap.createBitmap(v1.getDrawingCache());
        v1.setDrawingCacheEnabled(false);*/

//        LinearLayout view = (LinearLayout)findViewById(R.id.container);
        /*LinearLayout view = (LinearLayout) container.getChildAt(1);
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        Bitmap screen = view.getDrawingCache();*/

        //Now create the name of your PDF file that you will generate
//        File pdfFile = new File(new Main().dir, "test.pdf");
        try {
            Document  document = new Document();

            PdfWriter.getInstance(document, new FileOutputStream(
                    android.os.Environment.getExternalStorageDirectory()
                            + java.io.File.separator + dir
                            + java.io.File.separator + pdf_name));
            document.open();

            int childcount_container = container.getChildCount();
            for (int i = 0; i < childcount_container; i++) { //loop for inflated view

                //try ian
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;

                //convert a layout/view to bitmap
                LinearLayout view = (LinearLayout) container.getChildAt(i);//instead of a single layout
                view.setDrawingCacheEnabled(true);
                view.buildDrawingCache();
                //view.getDrawingCache();
               //Bitmap screen = view.getDrawingCache();//causes View too large to fit into drawing cache if image is too large

                //capture a large off-screen view in it's entirety:
                //prevent View too large to fit into drawing cache
                Bitmap screen = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);

                Canvas c = new Canvas(screen);
                //view.layout(0, 0, view.getLayoutParams().width, view.getLayoutParams().height);
                view.setLayoutParams(new LinearLayout.LayoutParams(view.getLayoutParams().width, view.getLayoutParams().height));
                view.draw(c);

                document.setPageSize(PageSize.LEGAL);//
                document.newPage();//
                screen = Bitmap.createScaledBitmap(screen,
                        600,//width
                        950,//height
                        false);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                screen.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                addImage(document,byteArray);
            }

            document.close();


            //open pdf reader
            //else download pdf
            if (canDisplayPdf(this)){
                final File attachment_path = new File(Environment.getExternalStorageDirectory() + "/" + dir + "/");
                final File prop_pictures = new File(attachment_path, pdf_name);
                Intent viewPDF = new Intent(Intent.ACTION_VIEW);
                viewPDF.setDataAndType(Uri.fromFile(prop_pictures), "application/pdf");
                context.startActivity(viewPDF);
                finish();
            } else {
                Snackbar snackbar = Snackbar
                        .make(container, "Your phone doesn\'t have a PDF file viewer.", Snackbar.LENGTH_INDEFINITE)
                        .setActionTextColor(getResources().getColor(R.color.colorPrimary))
                        .setAction("DOWNLOAD HERE", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                final String appPackageName = "com.adobe.reader";
                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                                }

                            }
                        });

                snackbar.show();
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private static void addImage(Document document, byte[] byteArray)
    {
        Image image = null;
        try
        {
            image = Image.getInstance(byteArray);
            image.setAlignment(Image.MIDDLE);
            document.add(image);
        }
        catch (BadElementException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (MalformedURLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }





    private boolean checkPermission(){
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }

    private static final int PERMISSION_REQUEST_CODE = 1;
    private void requestPermission(){
//        startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", getPackageName(), null)));//direct to app settings permission
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.READ_EXTERNAL_STORAGE)){
            Snackbar.make(container,"Read external storage permission allows us to create attachments.",Snackbar.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},PERMISSION_REQUEST_CODE);//prompt
        } else {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    startActivity(new Intent(this, Property_Pictures.class));
                    openGallery();
                } else {
                    Snackbar.make(container,"You should allow Read external storage permission for creating attachments.",Snackbar.LENGTH_LONG).show();
                }
                break;
            case PERMISSION_CAM_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    startActivity(new Intent(this, Property_Pictures.class));
                    dispatchTakePictureIntent();//open camera
                } else {
                    Snackbar.make(container,"You should allow Read external storage permission for creating attachments.",Snackbar.LENGTH_LONG).show();
                }
                break;
        }
    }



    /**
     * Check if the supplied context can render PDF files via some installed application that reacts to a intent
     * with the pdf mime type and viewing action.
     *
     * @param context
     * @return
     */
    public static final String MIME_TYPE_PDF = "application/pdf";
    public static boolean canDisplayPdf(Context context) {
        PackageManager packageManager = context.getPackageManager();
        Intent testIntent = new Intent(Intent.ACTION_VIEW);
        testIntent.setType(MIME_TYPE_PDF);
        if (packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY).size() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public void db_pdf() {
        DatabaseHandler db = new DatabaseHandler(this);
        File myDirectory = new File(Environment.getExternalStorageDirectory() + "/gds_appraisal");
        List<Images> report_accepted_jobs = db.getImages_single(pdf_name);
        if (report_accepted_jobs.isEmpty()) {
            db.addImages(new Images(myDirectory.toString(), pdf_name));
        }

       /* Toast.makeText(getApplicationContext(), pdf_name + " created",
                Toast.LENGTH_SHORT).show();
        finish();*/
        finish();
    }
    @Override
    public void onUserInteraction(){
        st.resetDisconnectTimer();
    }
    @Override
    public void onStop() {
        super.onStop();
        st.stopDisconnectTimer();
    }
    @Override
    public void onResume() {
        super.onResume();
        st.resetDisconnectTimer();
    }


}