package com.gds.appraisalmaybank.attachments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.json.JSONParser;
import com.gds.appraisalmaybank.json.TLS12SocketFactory;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by ianlozares on 07/12/2016.
 */

public class Attachments_Inflater extends AppCompatActivity implements View.OnClickListener {

    Global gv = new Global();
    GDS_methods gds = new GDS_methods();
    DatabaseHandler2 db2 = new DatabaseHandler2(this);
    String record_id, uid;
    String attachments_tag = "QweAsdZxc";
    String dir = "gds_appraisal";


    private static final String TAG_UID = "uid";
    private static final String TAG_RECORD_ID = "record_id";
    private static final String TAG_PASS_STAT = "pass_stat";
    String pass_stat="";

    LinearLayout container_attachments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.attachments_inflater);

        // getting record_id from intent / previous class
        Intent i = getIntent();
        uid = i.getStringExtra(TAG_UID);
        record_id = i.getStringExtra(TAG_RECORD_ID);
        pass_stat = i.getStringExtra(TAG_PASS_STAT);


        setTitle("Uploaded Attachments");
        getSupportActionBar().setIcon(R.mipmap.gds_icon);
        getSupportActionBar().setDisplayShowHomeEnabled(true);//for icon
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        container_attachments = (LinearLayout) findViewById(R.id.container_attachments);



        if (pass_stat.equals("online")){
            new LoadAll().execute();
        } else if (pass_stat.equals("offline")){
            display_attachments();
        }
    }


    /**
     * Fetch Details
     */
    JSONParser jsonParser = new JSONParser();
    JSONObject jsonResponse = new JSONObject();
    JSONObject jsonResponseFromAttachment = new JSONObject();
    String json = "";
    private ProgressDialog pDialog;

    @Override
    public void onClick(View v) {

    }

    class LoadAll extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Attachments_Inflater.this);
            pDialog.setMessage("Loading List. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            //fetch attachment from MySQL
            //build params
            ArrayList<String> jsonfieldattachment = new ArrayList<>();
            ArrayList<String> jsonvalueattachment = new ArrayList<>();
            jsonfieldattachment.clear();
            jsonfieldattachment.add("uid");
            jsonfieldattachment.add("auth_key");

            jsonvalueattachment.clear();
            jsonvalueattachment.add(uid);
            jsonvalueattachment.add(attachments_tag);

                /*if (gv.get_files_url.matches("https(.*)")) {
                    jsonResponseFromAttachment = jsonParser.makeHttpsRequestTLS(gv.get_files_url, gv.certificate_name, "POST", jsonfieldattachment, jsonvalueattachment);
                } else if (gv.get_files_url.matches("http(.*)")) {
                    jsonResponseFromAttachment = jsonParser.makeHttpRequest(gv.get_files_url, "POST", jsonfieldattachment, jsonvalueattachment);
                }
                Log.e("FromAttachment", jsonResponseFromAttachment.toString());*/
            if (gv.get_files_url.matches("https(.*)")) {
                jsonResponseFromAttachment = gds.makeHttpsRequest(gv.get_files_url, "POST", jsonfieldattachment, jsonvalueattachment);
            } else if (gv.get_files_url.matches("http(.*)")) {
//                    jsonResponseFromAttachment = gds.makeHttpRequest(gs.get_files_url, "POST", params);
                jsonResponseFromAttachment = gds.makeHttpsRequest(gv.get_files_url, "POST", jsonfieldattachment, jsonvalueattachment);
            }
            Log.e("FromAttachment", jsonResponseFromAttachment.toString());

            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all tasks
            display_attachments();
            pDialog.dismiss();
        }
    }

    /**
     * Fetch Details
     */
    JSONArray jsonArrayLevel1;
    JSONObject jsonObjLevel1;

    String file_name;
    File attachment_path = new File(Environment.getExternalStorageDirectory()+"/"+ dir +"/");
    PopupMenu popup;

    void display_attachments() {
        ArrayList<String> attUid = new ArrayList<>();
        ArrayList<String> attFile = new ArrayList<>();
        ArrayList<String> attFileName = new ArrayList<>();
        ArrayList<String> attAppraisalType = new ArrayList<>();
        ArrayList<String> attPath = new ArrayList<>();//path

        if (pass_stat.equals("online")){
            //json
            try {
                //get appraisal type from level1 array
                //attachments
                jsonArrayLevel1 = jsonResponseFromAttachment.getJSONArray("files");
                for (int a = 0; a < jsonArrayLevel1.length(); a++) {
                    jsonObjLevel1 = new JSONObject();
                    jsonObjLevel1 = jsonArrayLevel1.getJSONObject(a);

                    //attachments
                    attUid.add(getJsonString(jsonObjLevel1, "uid"));
                    attFile.add(getJsonString(jsonObjLevel1, "file"));
                    attFileName.add(getJsonString(jsonObjLevel1, "file_name"));
                    attAppraisalType.add(getJsonString(jsonObjLevel1, "appraisal_type"));

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (pass_stat.equals("offline")){
            //sqlite
            String where = "WHERE record_id = args";
            String[] args = {record_id};

            attUid = db2.getRecord("uid", where, args, "tbl_attachments");
            attFile = db2.getRecord("file", where, args, "tbl_attachments");
            attFileName = db2.getRecord("file_name", where, args, "tbl_attachments");
            attAppraisalType = db2.getRecord("appraisal_type", where, args, "tbl_attachments");
            attPath = db2.getRecord("file", where, args, "tbl_attachments");
        }





        Log.e("attUid.size()",""+attUid.size());
        if (attUid.size() > 0) {
            if(!attUid.get(0).contentEquals("")){
                for (int x = 0; x < attUid.size(); x++) {
                   // if (!attFile.get(x).contentEquals("Property Pictures")) {
                        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View addView = layoutInflater.inflate(R.layout.inflater_attachment_display, null);

                        final TextView item_file_type = (TextView) addView.findViewById(R.id.item_file_type);
                        final TextView item_file_name = (TextView) addView.findViewById(R.id.item_file_name);
                        final Button item_download = (Button) addView.findViewById(R.id.item_download);
                        final Button item_view = (Button) addView.findViewById(R.id.item_view);

                        //display
                        item_file_type.setText(""+attFile.get(x));
                        item_file_name.setText(""+attFileName.get(x));


                        File attachment_path_check = new File(Environment.getExternalStorageDirectory() + "/"+ dir +"/");
                        File jpg_attachment_check = new File(attachment_path_check, attFileName.get(x));
                        Log.e("xxx",attachment_path_check.toString()+" "+attFileName.get(x)+" "+jpg_attachment_check.exists());
                        //hide show buttons
                        if (jpg_attachment_check.exists()){
                            item_view.setVisibility(View.VISIBLE);
                            item_download.setVisibility(View.GONE);
                        } else {
                            item_view.setVisibility(View.GONE);
                            item_download.setVisibility(View.VISIBLE);
                        }

                        if (item_file_type.getText().toString().contentEquals("TCT")) {//2 options for TCT
                            item_view.setCompoundDrawablesWithIntrinsicBounds(R.drawable.view_file_gray24, 0, R.drawable.menu_dot_gray24, 0);


                            String substr = attFileName.get(x).substring(attFileName.get(x).length() - 3);
                            String newstr = "";
                            if (substr.equals("pdf")) {
                                newstr = attFileName.get(x).replace(".pdf",".jpg");//change last 4 chars to jpg
                            } else if (substr.equals("PDF")){
                                newstr = attFileName.get(x).replace(".PDF",".jpg");//change last 4 chars to jpg
                            }
                            Log.e("newstr",newstr);

                            attachment_path_check = new File(Environment.getExternalStorageDirectory() + "/"+ dir +"/");
                            jpg_attachment_check = new File(attachment_path_check, newstr);
                            Log.e("jpg_attachment_check",jpg_attachment_check.toString());
                            //hide show buttons
                            Log.e("exist???",""+jpg_attachment_check.exists());
                            if (jpg_attachment_check.exists()){
                                item_view.setVisibility(View.VISIBLE);
                                item_download.setVisibility(View.GONE);
                            } else {
                                item_view.setVisibility(View.GONE);
                                item_download.setVisibility(View.VISIBLE);
                            }

                        }

                        addView.setEnabled(false);

                        addView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // TODO Auto-generated method stub
                            }
                        });

                        item_download.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                file_name = item_file_name.getText().toString();
                                file_to_download = item_file_name.getText().toString();

                                file_name_jpg = item_file_name.getText().toString();
                                if (item_file_type.getText().toString().contentEquals("TCT")) {//2 options for TCT
                                    isTCT = true;
                                } else {//if not TCT, direct DL
                                    isTCT = false;
                                }

                                download_url = gv.pdf_loc_url+file_to_download;
                                download_url = download_url.replaceAll(" ", "%20");
                                new DownloadFileFromURL().execute();

                            }
                        });

                        item_view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {


                                if (item_file_type.getText().toString().contentEquals("TCT")) {//2 options for TCT
                                    file_name = item_file_name.getText().toString();
                                    //Creating the instance of PopupMenu
                                    popup = new PopupMenu(Attachments_Inflater.this, item_view);
                                    //Inflating the Popup using xml file
                                    popup.getMenuInflater().inflate(R.menu.menu_view_attachment, popup.getMenu());

                                    //registering popup with OnMenuItemClickListener
                                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                        public boolean onMenuItemClick(MenuItem item) {

                                            Intent intent;

                                            //convert selected pdf filename jpg
                                            String substr = file_name.substring(file_name.length() - 3);
                                            if (substr.equals("pdf")) {
                                                file_name = file_name.replace(".pdf",".jpg");//change last 4 chars to pdf
                                            } else if (substr.equals("PDF")){
                                                file_name = file_name.replace(".PDF",".jpg");//change last 4 chars to pdf
                                            }
                                            file_name_jpg = file_name;
                                            jpg_attachment = new File(attachment_path, file_name_jpg);



                                            if (item.getItemId() == R.id.menu_view){

                                                //view image in gallery view
                                            /*intent = new Intent(Intent.ACTION_VIEW);
                                            intent.setDataAndType( Uri.fromFile(jpg_attachment), "image*//*" );
                                            try {
                                                startActivity(intent);
                                            } catch (ActivityNotFoundException e) {
                                                Toast.makeText(Attachments_Inflater.this,
                                                        "Can't open image",
                                                        Toast.LENGTH_SHORT).show();
                                            }*/

                                                file_name = item_file_name.getText().toString();
                                                //view image in pdf viewer
                                                final File attachment_path = new File(Environment.getExternalStorageDirectory() + "/"+ dir +"/" + file_name);
                                                intent = new Intent(Intent.ACTION_VIEW);
                                                intent.setDataAndType(Uri.fromFile(attachment_path), "application/pdf" );
                                                try {
                                                    startActivity(intent);
                                                } catch (ActivityNotFoundException e) {
                                                    Toast.makeText(Attachments_Inflater.this,
                                                            "No Application Available to View PDF",
                                                            Toast.LENGTH_SHORT).show();
                                                }

                                            }

                                            if (item.getItemId() == R.id.menu_plotting){

                                                Activity activity = (Activity) Attachments_Inflater.this;

                                                try {
                                                    File file2 = new File(attachment_path+"/"+file_name_jpg);
                                                    intent = new Intent().setAction(Intent.ACTION_SEND);
                                                    intent.putExtra(Intent.EXTRA_TEXT, file2.getAbsolutePath());//2nd param jpg
                                                    Log.e("Open in GDS Plotting", file2.toString());//2nd param jpg

                                                    intent.putExtra("logged_in", "true");
                                                    intent.setType("text/plain");
                                                    intent.setPackage("com.gds.projects.plotter");
                                                    activity.startActivityForResult(intent, 4);
                                                } catch (Exception e) {
                                                    e.printStackTrace();

                                                    final String plotting_tool = "com.gds.projects.plotter";

                                                    try {
                                                        activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + plotting_tool)));
                                                    } catch (android.content.ActivityNotFoundException anfe) {
                                                        activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + plotting_tool)));
                                                    }

                                                }
                                            }

                                            return true;
                                        }
                                    });
                                    popup.show();//showing popup menu
                                } else {
                                    file_name = item_file_name.getText().toString();
                                    //view image in pdf viewer
                                    final File attachment_path = new File(Environment.getExternalStorageDirectory() + "/"+ dir +"/" + file_name);
                                    Intent intent = new Intent(Intent.ACTION_VIEW);
                                    intent.setDataAndType(Uri.fromFile(attachment_path), "application/pdf" );
                                    try {
                                        startActivity(intent);
                                    } catch (ActivityNotFoundException e) {
                                        Toast.makeText(Attachments_Inflater.this,
                                                "No Application Available to View PDF",
                                                Toast.LENGTH_SHORT).show();
                                    }

                                }


                            }
                        });

                        container_attachments.addView(addView);
                   //}
                }
            }else{
                LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View addView = layoutInflater.inflate(R.layout.inflater_attachment_display, null);

                final TextView item_file_type = (TextView) addView.findViewById(R.id.item_file_type);
                final Button item_download = (Button) addView.findViewById(R.id.item_download);
                final Button item_view = (Button) addView.findViewById(R.id.item_view);

                //display
//                item_file_type.setText(attFile.get(x));
                item_file_type.setText("No Attachments Found");
                item_file_type.setGravity(Gravity.CENTER);
                item_file_type.setTextColor(getResources().getColor(R.color.gray));
                item_download.setVisibility(View.GONE);
                item_view.setVisibility(View.GONE);
                container_attachments.addView(addView);
            }

        } else {
            LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View addView = layoutInflater.inflate(R.layout.inflater_attachment_display, null);

            final TextView item_file_type = (TextView) addView.findViewById(R.id.item_file_type);
            final Button item_download = (Button) addView.findViewById(R.id.item_download);
            final Button item_view = (Button) addView.findViewById(R.id.item_view);

            //display
//                item_file_type.setText(attFile.get(x));
            item_file_type.setText("No Attachments Found");
            item_file_type.setGravity(Gravity.CENTER);
            item_file_type.setTextColor(getResources().getColor(R.color.gray));
            item_download.setVisibility(View.GONE);
            item_view.setVisibility(View.GONE);
            container_attachments.addView(addView);
        }
    }


    //has checker
    String getJsonString(JSONObject jsonObject, String fieldname){
        try {
            if(jsonObject.has(fieldname)) {
                Log.e("getJsonString", fieldname + " found");
                return nullToBlank(jsonObject.getString(fieldname));
            } else {
                Log.e("getJsonString", fieldname +" does not exist");
                return "";
            }
        } catch (JSONException e) {
            e.printStackTrace();
//            return e.toString();
            return "";
        }
    }
    //nullConverter
    String nullToBlank(String s){
        if(s.contentEquals("null") || s.isEmpty() || s.length() == 0 || s == null){
            Log.e("value is null", "");
            return "";
        }
        return s;
    }


    /**
     * Convert pdf to jpg php then download it
     */
    JSONObject jsonConvertResponse = new JSONObject();
    int success_convert = 0;

    class ConvertPDFtoJPG extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Attachments_Inflater.this);
            pDialog.setMessage("Converting file. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
        protected String doInBackground(String... args) {
            //build params
            ArrayList<String> jsonfield = new ArrayList<>();
            jsonfield.clear();
            jsonfield.add("file_name");
            jsonfield.add("auth_key");
            ArrayList<String> jsonvalue = new ArrayList<>();
            jsonvalue.clear();
            jsonvalue.add(file_to_download);
            jsonvalue.add(attachments_tag);

            if (gv.update_url.matches("https(.*)")) {
//                jsonConvertResponse = jsonParser.makeHttpsRequestTLS(gv.pdf_converter_url, gv.certificate_name, "POST", jsonfield, jsonvalue);
                jsonConvertResponse = gds.makeHttpsRequest(gv.pdf_to_jpg_converter_url, "POST", jsonfield, jsonvalue);
            } else if (gv.update_url.matches("http(.*)")) {
                jsonConvertResponse = gds.makeHttpsRequest(gv.pdf_to_jpg_converter_url, "POST", jsonfield, jsonvalue);
            }
            Log.e("jsonConvertResponse", jsonConvertResponse.toString());

            try {
                success_convert = jsonConvertResponse.getInt("success");
                Log.e("success", ""+success_convert);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Log.e("JSONException", ""+e);
                Log.e("success", ""+success_convert);
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Exception", ""+e);
                Log.e("success", ""+success_convert);
            }

            return null;
        }
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            pDialog.dismiss();
            if (success_convert == 1) {
                download_url = gv.pdf_loc_url+file_name_jpg;
                download_url = download_url.replaceAll(" ", "%20");
//                new DownloadFileFromURL().execute();
                new DownloadJPGFromURL().execute();
            } else {
                Toast.makeText(getApplicationContext(), "Failed to convert file", Toast.LENGTH_LONG).show();
            }
        }
    }

    /*
        Download File
    */

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type:
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.setCanceledOnTouchOutside(false);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    // Progress dialog type (0 - for Horizontal progress bar)
    public static final int progress_bar_type = 0;
    String download_url, file_to_download;
    String file_name_jpg;
    File jpg_attachment;
    boolean isTCT = false;

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                // Create a new trust manager that trust all certificates
                TrustManager[] trustAllCerts = new TrustManager[]{
                        new X509TrustManager() {
                            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                return null;
                            }
                            public void checkClientTrusted(
                                    java.security.cert.X509Certificate[] certs, String authType) {
                            }
                            public void checkServerTrusted(
                                    java.security.cert.X509Certificate[] certs, String authType) {
                            }
                        }
                };

// Activate the new trust manager
                try {
                    SSLContext sc = SSLContext.getInstance("TLSv1.2");
                    sc.init(null, trustAllCerts, new java.security.SecureRandom());
                    HttpsURLConnection.setDefaultSSLSocketFactory(new TLS12SocketFactory(sc.getSocketFactory()));

                   /* SSLContext sc = SSLContext.getInstance("TLS");
                    sc.init(null, trustAllCerts, new java.security.SecureRandom());
                    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());*/


                } catch (Exception e) {
                }
// And as before now you can use URL and URLConnection
//                URL url = new URL(gv.pdf_loc_url + file_to_download);//url+filename.extension
                URL url = new URL(download_url);

                Log.e("download_url",url.toString());
                URLConnection connection = url.openConnection();
                InputStream input = connection.getInputStream();
                int lenghtOfFile = connection.getContentLength();

                // Output stream to write file
                OutputStream output = null;
                //
                /*if (isTCT) {
                    output = new FileOutputStream(Environment.getExternalStorageDirectory() + "/"+ dir +"/"+file_name_jpg);
                }
                if (!isTCT) {
                    output = new FileOutputStream(Environment.getExternalStorageDirectory() + "/"+ dir +"/"+file_name);
                }*/
                output = new FileOutputStream(Environment.getExternalStorageDirectory() + "/"+ dir +"/"+file_name);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            removeDialog(progress_bar_type);
            Toast.makeText(getApplicationContext(), "Attachment Downloaded", Toast.LENGTH_LONG).show();
            container_attachments.removeAllViews();
            display_attachments();
            if (isTCT) {
                //convert filename of .pdf to .jpg
                String substr = file_name_jpg.substring(file_name_jpg.length() - 3);
                if (substr.equals("pdf")) {
                    file_name_jpg = file_name_jpg.replace(".pdf",".jpg");//change last 4 chars to jpg
                } else if (substr.equals("PDF")){
                    file_name_jpg = file_name_jpg.replace(".PDF",".jpg");//change last 4 chars to jpg
                }
                new ConvertPDFtoJPG().execute();
            }
        }

    }


    class DownloadJPGFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                // Create a new trust manager that trust all certificates
                TrustManager[] trustAllCerts = new TrustManager[]{
                        new X509TrustManager() {
                            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                return null;
                            }
                            public void checkClientTrusted(
                                    java.security.cert.X509Certificate[] certs, String authType) {
                            }
                            public void checkServerTrusted(
                                    java.security.cert.X509Certificate[] certs, String authType) {
                            }
                        }
                };

// Activate the new trust manager
                try {
                    SSLContext sc = SSLContext.getInstance("TLSv1.2");
                    sc.init(null, trustAllCerts, new java.security.SecureRandom());
                    HttpsURLConnection.setDefaultSSLSocketFactory(new TLS12SocketFactory(sc.getSocketFactory()));

                   /* SSLContext sc = SSLContext.getInstance("TLS");
                    sc.init(null, trustAllCerts, new java.security.SecureRandom());
                    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());*/


                } catch (Exception e) {
                }

// And as before now you can use URL and URLConnection
//                URL url = new URL(gv.pdf_loc_url + file_to_download);//url+filename.extension
                URL url = new URL(download_url);

                Log.e("download_url",url.toString());
                URLConnection connection = url.openConnection();
                InputStream input = connection.getInputStream();
                int lenghtOfFile = connection.getContentLength();

                // Output stream to write file
                OutputStream output = null;
                if (isTCT) {
                    output = new FileOutputStream(Environment.getExternalStorageDirectory() + "/"+ dir +"/"+file_name_jpg);
                }
                if (!isTCT) {
                    output = new FileOutputStream(Environment.getExternalStorageDirectory() + "/"+ dir +"/"+file_name);
                }

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            removeDialog(progress_bar_type);
            Toast.makeText(getApplicationContext(), "Attachment Downloaded", Toast.LENGTH_LONG).show();
            container_attachments.removeAllViews();
            display_attachments();
        }

    }
}
