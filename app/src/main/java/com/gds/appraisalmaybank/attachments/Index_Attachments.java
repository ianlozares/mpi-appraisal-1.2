package com.gds.appraisalmaybank.attachments;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Images;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.Session_Timer;

public class Index_Attachments extends ListActivity {

	// ArrayList thats going to hold the search results
	ArrayList<HashMap<String, Object>> searchResults;

	// ArrayList that will hold the original Data
	ArrayList<HashMap<String, Object>> originalValues;
	LayoutInflater inflater;
	String[] names;
	Integer[] photos;
	String item;
	int length = 0, i = 0;
	DatabaseHandler db = new DatabaseHandler(this);
	Global gs;
	String dir = "gds_appraisal";
	String[] commandArray = new String[] { "Edit", "Delete" };
	String new_filename;
	Session_Timer st = new Session_Timer(this);
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.attachments_list);

		st.resetDisconnectTimer();
		final EditText searchBox = (EditText) findViewById(R.id.searchBox);
		ListView playerListView = (ListView) findViewById(android.R.id.list);
		gs = ((Global) getApplicationContext());
		// onclickitem
		playerListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub

				item = ((TextView) arg1.findViewById(R.id.txt_image_list))
						.getText().toString();
				Toast.makeText(getApplicationContext(), item,
						Toast.LENGTH_SHORT).show();

				String filenameArray[] = item.split("\\.");
				String extension = filenameArray[filenameArray.length - 1];
				gs.attachment_file_name = item;
				if (extension.equals("jpg")) {
					startActivity(new Intent(Index_Attachments.this,
							Attachments_Image.class));
				} else if (extension.equals("pdf")) {
					File myDirectory = new File(Environment
							.getExternalStorageDirectory() + "/" + dir + "/");

					File file = new File(myDirectory, item);

					if (file.exists()) {
						Uri path = Uri.fromFile(file);
						Intent intent = new Intent(Intent.ACTION_VIEW);
						intent.setDataAndType(path, "application/pdf");
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

						try {
							startActivity(intent);
						} catch (ActivityNotFoundException e) {
							Toast.makeText(Index_Attachments.this,
									"No Application Available to View PDF",
									Toast.LENGTH_SHORT).show();
						}
					}
				}

			}

		});
		playerListView.setLongClickable(true);
		playerListView
				.setOnItemLongClickListener(new OnItemLongClickListener() {
					@Override
					public boolean onItemLongClick(AdapterView<?> arg0,
							View arg1, final int arg2, long arg3) {
						item = ((TextView) arg1
								.findViewById(R.id.txt_image_list)).getText()
								.toString();
						Toast.makeText(getApplicationContext(), item,
								Toast.LENGTH_SHORT).show();
						Edit_delete();
						return true;

					}
				});
		// get the LayoutInflater for inflating the customomView
		// this will be used in the custom adapter
		inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		Log.d("Reading: ", "Count images..");

		length = db.getImageCount();
		// initialize the array
		names = new String[length];
		photos = new Integer[length];

		Log.d("Reading: ", "Reading images..");
		List<Images> images2 = db.getAllImages();
		if (!images2.isEmpty()) {
			for (Images im : images2) {
				names[i] = im.getfilename();
				photos[i] = R.drawable.monotoneright_white;
				i++;
			}

		}

		originalValues = new ArrayList<HashMap<String, Object>>();

		// temporary HashMap for populating the
		// Items in the ListView
		HashMap<String, Object> temp;

		// total number of rows in the ListView
		int noOfPlayers = names.length;

		// now populate the ArrayList players
		for (int i = 0; i < noOfPlayers; i++) {
			temp = new HashMap<String, Object>();

			temp.put("name", names[i]);
			temp.put("photo", photos[i]);

			// add the row to the ArrayList
			originalValues.add(temp);
		}

		// searchResults=OriginalValues initially
		searchResults = new ArrayList<HashMap<String, Object>>(originalValues);

		// create the adapter
		// first param-the context
		// second param-the id of the layout file
		// you will be using to fill a row
		// third param-the set of values that
		// will populate the ListView
		final CustomAdapter adapter = new CustomAdapter(this,
				R.layout.attachments_list_layout, searchResults);

		// finally,set the adapter to the default ListView
		playerListView.setAdapter(adapter);
		searchBox.addTextChangedListener(new TextWatcher() {

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// get the text in the EditText
				String searchString = searchBox.getText().toString();
				int textLength = searchString.length();
				searchResults.clear();

				for (int i = 0; i < originalValues.size(); i++) {
					String playerName = originalValues.get(i).get("name")
							.toString();
					if (textLength <= playerName.length()) {
						// compare the String in EditText with Names in the
						// ArrayList
						if (searchString.equalsIgnoreCase(playerName.substring(
								0, textLength)))
							searchResults.add(originalValues.get(i));
					}
				}

				adapter.notifyDataSetChanged();
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			public void afterTextChanged(Editable s) {

			}
		});
	}

	// define your custom adapter
	private class CustomAdapter extends ArrayAdapter<HashMap<String, Object>> {

		public CustomAdapter(Context context, int textViewResourceId,
				ArrayList<HashMap<String, Object>> Strings) {

			// let android do the initializing :)
			super(context, textViewResourceId, Strings);
		}

		// class for caching the views in a row
		private class ViewHolder {
			ImageView photo;
			TextView name;

		}

		ViewHolder viewHolder;

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			if (convertView == null) {
				convertView = inflater.inflate(
						R.layout.attachments_list_layout, null);
				viewHolder = new ViewHolder();

				// cache the views
				viewHolder.photo = (ImageView) convertView
						.findViewById(R.id.mono);
				viewHolder.name = (TextView) convertView
						.findViewById(R.id.txt_image_list);

				// link the cached views to the convertview
				convertView.setTag(viewHolder);

			} else
				viewHolder = (ViewHolder) convertView.getTag();

			int photoId = (Integer) searchResults.get(position).get("photo");

			// set the data to be displayed
			viewHolder.photo.setImageDrawable(getResources().getDrawable(
					photoId));
			viewHolder.name.setText(searchResults.get(position).get("name")
					.toString());
			// return the view to be displayed
			return convertView;
		}

	}

	private void Edit_delete() {
		android.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Actions :");
		builder.setItems(commandArray, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int index) {
				if (index == 0) {
					// update Images table
					new_filename();
					dialog.dismiss();
				} else if (index == 1) {
					delete();
					dialog.dismiss();
				}
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public void new_filename() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Filename :");

		// Set up the input
		final EditText input = new EditText(this);
		// Specify the type of input expected; this, for example, sets the input
		// as a password, and will mask the text
		input.setInputType(InputType.TYPE_CLASS_TEXT);
		builder.setView(input);

		// Set up the buttons
		builder.setPositiveButton("Update",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						String filenameArray[] = item.split("\\.");
						String extension = filenameArray[filenameArray.length - 1];
						if (extension.equals("jpg")) {
							new_filename = input.getText().toString() + ".jpg";
						} else {
							new_filename = input.getText().toString() + ".pdf";
						}
						db.updateImages(item, new_filename);

						File old_file = new File(Environment
								.getExternalStorageDirectory()
								+ "/"
								+ dir
								+ "/" + item);

						File new_file = new File(Environment
								.getExternalStorageDirectory()
								+ "/"
								+ dir
								+ "/" + new_filename);
						old_file.renameTo(new_file);
						reload();
					}
				});

		builder.show();
	}

	public void delete_file() {
		File file = new File(Environment.getExternalStorageDirectory() + "/"
				+ dir + "/" + item);
		file.delete();
	}

	public void delete_db_filename() {
		Images images2 = new Images();
		images2.setfilename(item);
		db.deleteImages(images2);
		db.close();
	}

	private void delete() {
		// TODO Auto-generated method stub
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:
					// Yes button clicked
					delete_file();
					Toast.makeText(getApplicationContext(), "File Deleted",
							Toast.LENGTH_SHORT).show();
					delete_db_filename();
					reload();
					break;

				case DialogInterface.BUTTON_NEGATIVE:
					// No button clicked
					break;
				}
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Delete this file?")
				.setPositiveButton("Yes", dialogClickListener)
				.setNegativeButton("No", dialogClickListener).show();

	}

	public void reload() {
		// reload activity
		Intent intent = getIntent();
		overridePendingTransition(0, 0);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		finish();
		overridePendingTransition(0, 0);
		startActivity(intent);
	}
	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}
}