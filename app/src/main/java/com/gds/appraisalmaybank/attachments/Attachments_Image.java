package com.gds.appraisalmaybank.attachments;

import java.io.File;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.widget.ImageView;
import android.widget.TextView;

import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.Session_Timer;

public class Attachments_Image extends Activity {
	Global gs;
	ImageView attachments_image;
	String imagename;
	String dir = "gds_appraisal";
	TextView tv_image_name;

	Session_Timer st = new Session_Timer(this);
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.attachments_image);
		st.resetDisconnectTimer();
		gs = ((Global) getApplicationContext());
		tv_image_name = (TextView) findViewById(R.id.tv_image_name);
		attachments_image = (ImageView) findViewById(R.id.attachments_image);
		imagename = gs.attachment_file_name;
		File myDirectory = new File(Environment.getExternalStorageDirectory()
				+ "/" + dir + "/");

		File f = new File(myDirectory, imagename);

		Bitmap bmp = BitmapFactory.decodeFile(f.getAbsolutePath());
		attachments_image.setImageBitmap(bmp);
		
		tv_image_name.setText(imagename);
	}
	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}


}
