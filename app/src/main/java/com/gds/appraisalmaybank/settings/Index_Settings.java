package com.gds.appraisalmaybank.settings;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import com.gds.appraisalmaybank.main.R;

public class Index_Settings extends PreferenceActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings);
	}
}