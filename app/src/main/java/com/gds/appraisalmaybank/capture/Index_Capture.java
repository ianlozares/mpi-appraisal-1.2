package com.gds.appraisalmaybank.capture;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.Session_Timer;

public class Index_Capture extends Activity {
	// for prompts
	Context context = this;
	Button button;
	EditText result;
	String image_name;
	Session_Timer st = new Session_Timer(this);
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.attachments_image);
		image_name_prompts();
		st.resetDisconnectTimer();

	}

	public void image_name_prompts() {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView = li.inflate(R.layout.image_name_input, null);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);

		final EditText userInput = (EditText) promptsView
				.findViewById(R.id.et_image_name);

		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						SharedPreferences appPrefs = getSharedPreferences(
								"preference", MODE_PRIVATE);
						SharedPreferences.Editor prefsEditor = appPrefs.edit();
						prefsEditor.putString("image", userInput.getText()
								.toString() + ".jpg");
						prefsEditor.commit();
						startActivity(new Intent(Index_Capture.this,
								Image_Capture.class));
						finish();
					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								dialog.cancel();
								finish();
							}
						});
		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}


	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}


}
