package com.gds.appraisalmaybank.required_fields;



import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.gds.appraisalmaybank.database_new.DatabaseHandler_New;
import com.gds.appraisalmaybank.database_new.Tables;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


/**
 * Created by GDS LINK ASIA on 8/17/2017.
 */

public class Fields_Checker {


    Global gv = new Global();
    GDS_methods gds = new GDS_methods();
    ArrayList<String> req_field = new ArrayList<>();


    /**
     * Vacant Lot
     */
    ArrayList<String> vl_fields_set1(String record_id, Context context){//ownership

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

//        app_type_req_field.add(db.getRecord("record_id", Tables.vacant_lot_lot_details.table_name, "record_id", record_id));

        ArrayList<String> id = db.getRecordColumn("id", Tables.vacant_lot_lot_details.table_name, "record_id", record_id);

        if (!id.isEmpty()) { //if at least 1 is required

            for (int x = 0; x < id.size(); x++){
                //required fields inside
                app_type_req_field.add(db.getRecord("report_propdesc_tct_no", Tables.vacant_lot_lot_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_propdesc_lot", Tables.vacant_lot_lot_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_propdesc_block", Tables.vacant_lot_lot_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_propdesc_survey_nos", Tables.vacant_lot_lot_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_propdesc_area", Tables.vacant_lot_lot_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("valrep_land_propdesc_registry_date", Tables.vacant_lot_lot_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_propdesc_registered_owner", Tables.vacant_lot_lot_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_propdesc_deeds", Tables.vacant_lot_lot_details.table_name, "id", id.get(x)));
            }

        } else {
            app_type_req_field.add("");
        }

        id.clear();

        return app_type_req_field;

    }

    ArrayList<String> vl_fields_set2(String record_id, Context context){ //land details

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

/*for (int x = 0; x < cb_req_field_1.size(); x++){
            if (cb_req_field_1.get(x).contentEquals("true")){
                app_type_req_field.add(cb_req_field_1.get(x));
            }
        }*/


        ArrayList<String> cb_req_field = new ArrayList<>();
        //Physical Characteristic
        cb_req_field.clear();
        cb_req_field.add(db.getRecord("report_physical_corner_lot", Tables.vacant_lot.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_physical_non_corner_lot", Tables.vacant_lot.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_physical_perimeter_lot", Tables.vacant_lot.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_physical_intersected_lot", Tables.vacant_lot.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_physical_interior_with_row", Tables.vacant_lot.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_physical_landlocked", Tables.vacant_lot.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("valrep_land_physical_others", Tables.vacant_lot.table_name, "record_id", record_id));

        if(!cb_req_field.contains("true")){
            app_type_req_field.add("");
        }
        if (cb_req_field.get(cb_req_field.size()-1).contains("true")){
            app_type_req_field.add(db.getRecord("valrep_land_physical_others_desc", Tables.vacant_lot.table_name, "record_id", record_id));
        }
        //basis of identification
        cb_req_field.clear();
        cb_req_field.add(db.getRecord("report_id_association", Tables.vacant_lot.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_id_tax", Tables.vacant_lot.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_id_lra", Tables.vacant_lot.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_id_lot_config", Tables.vacant_lot.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_id_subd_map", Tables.vacant_lot.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_id_nbrhood_checking", Tables.vacant_lot.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("valrep_land_id_others", Tables.vacant_lot.table_name, "record_id", record_id));

        if(!cb_req_field.contains("true")){
            app_type_req_field.add("");
        }
        if (cb_req_field.get(cb_req_field.size()-1).contains("true")){
            app_type_req_field.add(db.getRecord("valrep_land_id_others_desc", Tables.vacant_lot.table_name, "record_id", record_id));
        }

        app_type_req_field.add(db.getRecord("valrep_land_road_right_of_way", Tables.vacant_lot.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_imp_street_name", Tables.vacant_lot.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_imp_curb", Tables.vacant_lot.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_imp_sidewalk", Tables.vacant_lot.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_imp_drainage", Tables.vacant_lot.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_imp_street_lights", Tables.vacant_lot.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_lotclass_per_tax_dec", Tables.vacant_lot.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_lotclass_actual_usage", Tables.vacant_lot.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_lotclass_neighborhood", Tables.vacant_lot.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_lotclass_highest_best_use", Tables.vacant_lot.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_landmark_1", Tables.vacant_lot.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_distance_1", Tables.vacant_lot.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_util_electricity", Tables.vacant_lot.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_util_water", Tables.vacant_lot.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_util_telephone", Tables.vacant_lot.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_util_garbage", Tables.vacant_lot.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bound_right", Tables.vacant_lot.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bound_left", Tables.vacant_lot.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bound_rear", Tables.vacant_lot.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bound_front", Tables.vacant_lot.table_name, "record_id", record_id));

        String nature_appraisal = db.getRecord("nature_appraisal", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id);

        if(nature_appraisal.contentEquals("Re-appraisal")) {
            app_type_req_field.add(db.getRecord("report_prev_date", Tables.vacant_lot.table_name, "record_id", record_id));
            app_type_req_field.add(db.getRecord("report_prev_appraiser", Tables.vacant_lot.table_name, "record_id", record_id));
        }

        return app_type_req_field;

    }

    ArrayList<String> vl_fields_set3(String record_id, Context context){//comparatives

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

//        app_type_req_field.add(db.getRecord("report_remarks", Tables.vacant_lot.table_name, "record_id", record_id));

        return app_type_req_field;

    }

    ArrayList<String> vl_fields_set4(String record_id, Context context){//market

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

        app_type_req_field.add(db.getRecord("record_id", Tables.vacant_lot_lot_valuation_details.table_name, "record_id", record_id));

        return app_type_req_field;

    }

    ArrayList<String> vl_fields_set5(String record_id, Context context){//summary

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

        app_type_req_field.add(db.getRecord("report_remarks", Tables.vacant_lot.table_name, "record_id", record_id));

        return app_type_req_field;

    }
    //prev appraisal
    ArrayList<String> vl_fields_set6(String record_id, Context context){//prev appraisal

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

        String nature_appraisal = db.getRecord("nature_appraisal", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id);
        if(nature_appraisal.contentEquals("Re-appraisal")) {
            ArrayList<String> id = db.getRecordColumn("id", Tables.vacant_lot_main_prev_appraisal.table_name, "record_id", record_id);

            if (!id.isEmpty()) { //if at least 1 is required

                for (int x = 0; x < id.size(); x++) {
                    //required fields inside
                    app_type_req_field.add(db.getRecord("report_main_prev_desc", Tables.vacant_lot_main_prev_appraisal.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_main_prev_area", Tables.vacant_lot_main_prev_appraisal.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_main_prev_unit_value", Tables.vacant_lot_main_prev_appraisal.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_main_prev_appraised_value", Tables.vacant_lot_main_prev_appraisal.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("valrep_land_prev_land_value", Tables.vacant_lot_main_prev_appraisal.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("valrep_land_prev_imp_value", Tables.vacant_lot_main_prev_appraisal.table_name, "id", id.get(x)));
                }

            } else {
                app_type_req_field.add("");
            }

            id.clear();
        }
        return app_type_req_field;

    }

    /**
     * Land Details
     */
    ArrayList<String> li_fields_set1(String record_id, Context context){//ownership

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

//        app_type_req_field.add(db.getRecord("record_id", Tables.vacant_lot_lot_details.table_name, "record_id", record_id));

        ArrayList<String> id = db.getRecordColumn("id", Tables.land_improvements_lot_details.table_name, "record_id", record_id);

        if (!id.isEmpty()) {//if at least 1 is required

            for (int x = 0; x < id.size(); x++){
                //required fields inside
                app_type_req_field.add(db.getRecord("report_propdesc_tct_no", Tables.land_improvements_lot_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_propdesc_lot", Tables.land_improvements_lot_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_propdesc_block", Tables.land_improvements_lot_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_propdesc_survey_nos", Tables.land_improvements_lot_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_propdesc_area", Tables.land_improvements_lot_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("valrep_landimp_propdesc_registry_date", Tables.land_improvements_lot_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_propdesc_registered_owner", Tables.land_improvements_lot_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_propdesc_registry_of_deeds", Tables.land_improvements_lot_details.table_name, "id", id.get(x)));
            }

        } else {
            app_type_req_field.add("");
        }

        id.clear();

        return app_type_req_field;

    }

    ArrayList<String> li_fields_set2(String record_id, Context context){//land details

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

        ArrayList<String> cb_req_field = new ArrayList<>();
        //Physical Characteristic
        cb_req_field.clear();
        cb_req_field.add(db.getRecord("report_physical_corner_lot", Tables.land_improvements.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_physical_non_corner_lot", Tables.land_improvements.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_physical_perimeter_lot", Tables.land_improvements.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_physical_intersected_lot", Tables.land_improvements.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_physical_interior_with_row", Tables.land_improvements.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_physical_landlocked", Tables.land_improvements.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("valrep_landimp_physical_others", Tables.land_improvements.table_name, "record_id", record_id));

        if(!cb_req_field.contains("true")){
            app_type_req_field.add("");
        }
        if (cb_req_field.get(cb_req_field.size()-1).contains("true")){
            app_type_req_field.add(db.getRecord("valrep_landimp_physical_others_desc", Tables.land_improvements.table_name, "record_id", record_id));
        }

        //basis of identification
        cb_req_field.clear();
        cb_req_field.add(db.getRecord("report_id_association", Tables.land_improvements.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_id_tax", Tables.land_improvements.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_id_lra", Tables.land_improvements.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_id_lot_config", Tables.land_improvements.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_id_subd_map", Tables.land_improvements.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_id_nbrhood_checking", Tables.land_improvements.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("valrep_landimp_id_others", Tables.land_improvements.table_name, "record_id", record_id));

        if(!cb_req_field.contains("true")){
            app_type_req_field.add("");
        }
        if (cb_req_field.get(cb_req_field.size()-1).contains("true")){
            app_type_req_field.add(db.getRecord("valrep_landimp_id_others_desc", Tables.land_improvements.table_name, "record_id", record_id));
        }

        app_type_req_field.add(db.getRecord("valrep_landimp_road_right_of_way", Tables.land_improvements.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_imp_street", Tables.land_improvements.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_imp_sidewalk", Tables.land_improvements.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_imp_curb", Tables.land_improvements.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_imp_drainage", Tables.land_improvements.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_imp_street_lights", Tables.land_improvements.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_lotclass_per_tax_dec", Tables.land_improvements.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_lotclass_actual_usage", Tables.land_improvements.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_lotclass_neighborhood", Tables.land_improvements.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_lotclass_highest_best_use", Tables.land_improvements.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_landmark_1", Tables.land_improvements.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_distance_1", Tables.land_improvements.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_util_electricity", Tables.land_improvements.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_util_water", Tables.land_improvements.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_util_telephone", Tables.land_improvements.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_util_garbage", Tables.land_improvements.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bound_right", Tables.land_improvements.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bound_left", Tables.land_improvements.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bound_rear", Tables.land_improvements.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bound_front", Tables.land_improvements.table_name, "record_id", record_id));

        String nature_appraisal = db.getRecord("nature_appraisal", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id);

        if(nature_appraisal.contentEquals("Re-appraisal")) {
            app_type_req_field.add(db.getRecord("report_prev_date", Tables.land_improvements.table_name, "record_id", record_id));
            app_type_req_field.add(db.getRecord("report_prev_appraiser", Tables.land_improvements.table_name, "record_id", record_id));
        }
        return app_type_req_field;

    }

    ArrayList<String> li_fields_set3(String record_id, Context context){//comparatives

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

//        app_type_req_field.add(db.getRecord("valrep_land_road_right_of_way", Tables.land_improvements.table_name, "record_id", record_id));

        return app_type_req_field;

    }

    ArrayList<String> li_fields_set4(String record_id, Context context){//desc of imp

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

//        app_type_req_field.add(db.getRecord("record_id", Tables.land_improvements_imp_details.table_name, "record_id", record_id));

        ArrayList<String> id = db.getRecordColumn("imp_details_id", Tables.land_improvements_imp_details.table_name, "record_id", record_id);

        if (!id.isEmpty()) {//if at least 1 is required

            for (int x = 0; x < id.size(); x++){
                //required fields inside
                app_type_req_field.add(db.getRecord("report_impsummary1_no_of_floors", Tables.land_improvements_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_impsummary1_no_of_bedrooms", Tables.land_improvements_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_impsummary1_building_desc", Tables.land_improvements_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(gds.nullCheck3(db.getRecord("report_impsummary1_fa", Tables.land_improvements_imp_details.table_name, "imp_details_id", id.get(x))));
                app_type_req_field.add(db.getRecord("report_ownership_of_property", Tables.land_improvements_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("valrep_landimp_impsummary1_no_of_tb", Tables.land_improvements_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_foundation", Tables.land_improvements_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_columns_posts", Tables.land_improvements_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_beams", Tables.land_improvements_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_exterior_walls", Tables.land_improvements_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_interior_walls", Tables.land_improvements_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_imp_flooring", Tables.land_improvements_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_doors", Tables.land_improvements_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_imp_windows", Tables.land_improvements_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_ceiling", Tables.land_improvements_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_imp_roofing", Tables.land_improvements_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_trusses", Tables.land_improvements_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("valrep_landimp_desc_stairs", Tables.land_improvements_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_economic_life", Tables.land_improvements_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_effective_age", Tables.land_improvements_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_occupants", Tables.land_improvements_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_confirmed_thru", Tables.land_improvements_imp_details.table_name, "imp_details_id", id.get(x)));


            }

        } else {
            app_type_req_field.add("");
        }

        return app_type_req_field;

    }

    ArrayList<String> li_fields_set5(String record_id, Context context){//market

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

        app_type_req_field.add(db.getRecord("record_id", Tables.land_improvements_lot_valuation_details.table_name, "record_id", record_id));

        return app_type_req_field;

    }

    ArrayList<String> li_fields_set6(String record_id, Context context){//cost

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();


//        app_type_req_field.add(db.getRecord("record_id", Tables.land_improvements_imp_valuation.table_name, "record_id", record_id));

        ArrayList<String> id = db.getRecordColumn("imp_valuation_id", Tables.land_improvements_imp_valuation.table_name, "record_id", record_id);

        if (!id.isEmpty()) {//if at least 1 is required

            for (int x = 0; x < id.size(); x++){
                //required fields inside
                app_type_req_field.add(db.getRecord("report_imp_value_description", Tables.land_improvements_imp_valuation.table_name, "imp_valuation_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_imp_value_total_area", Tables.land_improvements_imp_valuation.table_name, "imp_valuation_id", id.get(x)));
            }

        } else {
            app_type_req_field.add("");
        }

        return app_type_req_field;

    }

    ArrayList<String> li_fields_set7(String record_id, Context context){//summary

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

        app_type_req_field.add(db.getRecord("report_remarks", Tables.land_improvements.table_name, "record_id", record_id));

        return app_type_req_field;

    }
    //prev app
    ArrayList<String> li_fields_set8(String record_id, Context context){//prev appraisal

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

        String nature_appraisal = db.getRecord("nature_appraisal", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id);
        if(nature_appraisal.contentEquals("Re-appraisal")) {
            ArrayList<String> id = db.getRecordColumn("id", Tables.land_improvements_main_prev_appraisal.table_name, "record_id", record_id);

            if (!id.isEmpty()) { //if at least 1 is required

                for (int x = 0; x < id.size(); x++) {
                    //required fields inside
                    app_type_req_field.add(db.getRecord("report_main_prev_desc", Tables.land_improvements_main_prev_appraisal.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_main_prev_area", Tables.land_improvements_main_prev_appraisal.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_main_prev_unit_value", Tables.land_improvements_main_prev_appraisal.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_main_prev_appraised_value", Tables.land_improvements_main_prev_appraisal.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("valrep_land_prev_land_value", Tables.land_improvements_main_prev_appraisal.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("valrep_land_prev_imp_value", Tables.land_improvements_main_prev_appraisal.table_name, "id", id.get(x)));
                }

            } else {
                app_type_req_field.add("");
            }

            id.clear();
        }
        return app_type_req_field;

    }

    /**
     * Condo
     */

    ArrayList<String> condo_fields_set1(String record_id, Context context){//owner

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

//        app_type_req_field.add(db.getRecord("record_id", Tables.condo_title_details.table_name, "record_id", record_id));

        ArrayList<String> id = db.getRecordColumn("id", Tables.condo_title_details.table_name, "record_id", record_id);

        if (!id.isEmpty()) {//if at least 1 is required

            for (int x = 0; x < id.size(); x++){
                //required fields inside
                app_type_req_field.add(db.getRecord("report_propdesc_property_type", Tables.condo_title_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_propdesc_cct_no", Tables.condo_title_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_prodesc_unit_no", Tables.condo_title_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_prodesc_floor", Tables.condo_title_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_prodesc_bldg_name", Tables.condo_title_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_propdesc_area", Tables.condo_title_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("valrep_condo_propdesc_registry_date", Tables.condo_title_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_prodesc_reg_of_deeds", Tables.condo_title_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_propdesc_registered_owner", Tables.condo_title_details.table_name, "id", id.get(x)));
            }

        } else {
            app_type_req_field.add("");
        }

        return app_type_req_field;
    }

    ArrayList<String> condo_fields_set2(String record_id, Context context){//land details

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

        ArrayList<String> cb_req_field = new ArrayList<>();
        //basis of identification
        cb_req_field.clear();
        cb_req_field.add(db.getRecord("report_id_admin", Tables.condo.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_id_unit_numbering", Tables.condo.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_id_bldg_plan", Tables.condo.table_name, "record_id", record_id));

        if(!cb_req_field.contains("true")){
            app_type_req_field.add("");
        }

        app_type_req_field.add(db.getRecord("report_date_inspected_month", Tables.condo.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_date_inspected_day", Tables.condo.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_date_inspected_year", Tables.condo.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_unitclass_per_tax_dec", Tables.condo.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_unitclass_actual_usage", Tables.condo.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_unitclass_neighborhood", Tables.condo.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_unitclass_highest_best_use", Tables.condo.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_util_electricity", Tables.condo.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_util_water", Tables.condo.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_util_telephone", Tables.condo.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_util_garbage", Tables.condo.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_landmarks_1", Tables.condo.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_distance_1", Tables.condo.table_name, "record_id", record_id));

        String nature_appraisal = db.getRecord("nature_appraisal", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id);

        if(nature_appraisal.contentEquals("Re-appraisal")) {
            app_type_req_field.add(db.getRecord("report_prev_date", Tables.condo.table_name, "record_id", record_id));
            app_type_req_field.add(db.getRecord("report_prev_appraiser", Tables.condo.table_name, "record_id", record_id));
        }

        return app_type_req_field;

    }

    ArrayList<String> condo_fields_set3(String record_id, Context context){//comparatives

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

//        app_type_req_field.add(db.getRecord("record_id", Tables.condo.table_name, "record_id", record_id));

        return app_type_req_field;

    }

    ArrayList<String> condo_fields_set4(String record_id, Context context){//desc of imp

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

//        app_type_req_field.add(db.getRecord("record_id", Tables.condo_unit_details.table_name, "record_id", record_id));

        ArrayList<String> id = db.getRecordColumn("id", Tables.condo_unit_details.table_name, "record_id", record_id);

        if (!id.isEmpty()) {//if at least 1 is required

            for (int x = 0; x < id.size(); x++){
                //required fields inside
                app_type_req_field.add(db.getRecord("report_unit_description", Tables.condo_unit_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_unit_no_of_storeys", Tables.condo_unit_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_no_of_bedrooms", Tables.condo_unit_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_unit_unit_no", Tables.condo_unit_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_unit_floor_location", Tables.condo_unit_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_unit_floor_area", Tables.condo_unit_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_unit_interior_flooring", Tables.condo_unit_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_unit_interior_partitions", Tables.condo_unit_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_unit_interior_doors", Tables.condo_unit_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_unit_interior_windows", Tables.condo_unit_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_unit_interior_ceiling", Tables.condo_unit_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_unit_features", Tables.condo_unit_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_unit_occupants", Tables.condo_unit_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_unit_type_of_property", Tables.condo_unit_details.table_name, "id", id.get(x)));
            }

        } else {
            app_type_req_field.add("");
        }


        return app_type_req_field;

    }

    ArrayList<String> condo_fields_set5(String record_id, Context context){//market

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

//        app_type_req_field.add(db.getRecord("record_id", Tables.condo_unit_valuation.table_name, "record_id", record_id));

        return app_type_req_field;

    }

    ArrayList<String> condo_fields_set6(String record_id, Context context){//summary

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

        app_type_req_field.add(db.getRecord("report_remarks", Tables.condo.table_name, "record_id", record_id));

        return app_type_req_field;

    }
    //prev app
    ArrayList<String> condo_fields_set7(String record_id, Context context){//prev appraisal

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

        String nature_appraisal = db.getRecord("nature_appraisal", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id);
        if(nature_appraisal.contentEquals("Re-appraisal")) {
            ArrayList<String> id = db.getRecordColumn("id", Tables.condo_main_prev_appraisal.table_name, "record_id", record_id);

            if (!id.isEmpty()) { //if at least 1 is required

                for (int x = 0; x < id.size(); x++) {
                    //required fields inside
                    app_type_req_field.add(db.getRecord("report_main_prev_desc", Tables.condo_main_prev_appraisal.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_main_prev_area", Tables.condo_main_prev_appraisal.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_main_prev_unit_value", Tables.condo_main_prev_appraisal.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_main_prev_appraised_value", Tables.condo_main_prev_appraisal.table_name, "id", id.get(x)));
                }

            } else {
                app_type_req_field.add("");
            }

            id.clear();
        }
        return app_type_req_field;

    }

    /**
     * Ppcr
     */

    ArrayList<String> ppcr_fields_set1(String record_id, Context context){//owner

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

        app_type_req_field.add(db.getRecord("report_date_inspected_month", Tables.ppcr.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_date_inspected_day", Tables.ppcr.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_date_inspected_year", Tables.ppcr.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_project_type", Tables.ppcr.table_name, "record_id", record_id));

        return app_type_req_field;

    }

    ArrayList<String> ppcr_fields_set2(String record_id, Context context){//details

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

//        app_type_req_field.add(db.getRecord("record_id", Tables.ppcr_report_details.table_name, "record_id", record_id));


        String isCondo = db.getRecord("report_is_condo", Tables.ppcr.table_name, "record_id", record_id);

        if (isCondo.contentEquals("false")) {

            ArrayList<String> id = db.getRecordColumn("id", Tables.ppcr_report_details.table_name, "record_id", record_id);

            if (!id.isEmpty()) {//if at least 1 is required

                for (int x = 0; x < id.size(); x++){
                    //required fields inside
                    app_type_req_field.add(db.getRecord("report_value_foundation", Tables.ppcr_report_details.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_value_columns", Tables.ppcr_report_details.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_value_beams_girders", Tables.ppcr_report_details.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_value_surround_walls", Tables.ppcr_report_details.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_value_interior_partition", Tables.ppcr_report_details.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_value_roofing_works", Tables.ppcr_report_details.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_value_plumbing_rough_in", Tables.ppcr_report_details.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_value_electrical_rough_in", Tables.ppcr_report_details.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_value_ceiling_works", Tables.ppcr_report_details.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_value_doors_windows", Tables.ppcr_report_details.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_value_plastering_wall", Tables.ppcr_report_details.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_value_slab_include_finish", Tables.ppcr_report_details.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_value_painting_finishing", Tables.ppcr_report_details.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_value_plumbing_elect_fix", Tables.ppcr_report_details.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_value_utilities_tapping", Tables.ppcr_report_details.table_name, "id", id.get(x)));
                }

            } else {
                app_type_req_field.add("");
            }
        }

        return app_type_req_field;

    }

    /**
     * CEbm
     */

    ArrayList<String> cebm_fields_set1(String record_id, Context context){ //project description

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();
        ArrayList<String> cb_req_field = new ArrayList<>();
        //Type of Constructions
        cb_req_field.clear();
        cb_req_field.add(db.getRecord("report_const_type_reinforced_concrete", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_const_type_semi_concrete", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_const_type_mixed_materials", Tables.construction_ebm.table_name, "record_id", record_id));

        if(!cb_req_field.contains("true")){
            app_type_req_field.add("");
        }
        app_type_req_field.add(db.getRecord("report_date_inspected_month", Tables.construction_ebm.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_date_inspected_day", Tables.construction_ebm.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_date_inspected_year", Tables.construction_ebm.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_project_type", Tables.construction_ebm.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_floor_area", Tables.construction_ebm.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_storeys", Tables.construction_ebm.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_expected_economic_life", Tables.construction_ebm.table_name, "record_id", record_id));



        return app_type_req_field;

    }

    ArrayList<String> cebm_fields_set2(String record_id, Context context){//details of the proposed const

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

        ArrayList<String> cb_req_field = new ArrayList<>();
        //foundation
        cb_req_field.clear();
        cb_req_field.add(db.getRecord("report_foundation_concrete", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_foundation_other", Tables.construction_ebm.table_name, "record_id", record_id));

        if(!cb_req_field.contains("true")){
            app_type_req_field.add("");
        }

        if (cb_req_field.get(cb_req_field.size()-1).contains("true")){
            app_type_req_field.add(db.getRecord("report_foundation_other_value", Tables.construction_ebm.table_name, "record_id", record_id));
        }

        //column
        cb_req_field.clear();
        cb_req_field.add(db.getRecord("report_post_concrete", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_post_concrete_timber", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_post_steel", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_post_other", Tables.construction_ebm.table_name, "record_id", record_id));

        if(!cb_req_field.contains("true")){
            app_type_req_field.add("");
        }

        if (cb_req_field.get(cb_req_field.size()-1).contains("true")){
            app_type_req_field.add(db.getRecord("report_post_other_value", Tables.construction_ebm.table_name, "record_id", record_id));
        }

        //beams
        cb_req_field.clear();
        cb_req_field.add(db.getRecord("report_beams_concrete", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_beams_timber", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_beams_steel", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_beams_other", Tables.construction_ebm.table_name, "record_id", record_id));

        if(!cb_req_field.contains("true")){
            app_type_req_field.add("");
        }

        if (cb_req_field.get(cb_req_field.size()-1).contains("true")){
            app_type_req_field.add(db.getRecord("report_beams_other_value", Tables.construction_ebm.table_name, "record_id", record_id));
        }

        //floor
        cb_req_field.clear();
        cb_req_field.add(db.getRecord("report_floors_concrete", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_floors_tiles", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_floors_tiles_cement", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_floors_laminated_wood", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_floors_ceramic_tiles", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_floors_wood_planks", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_floors_marble_washout", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_floors_concrete_boards", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_floors_granite_tiles", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_floors_marble_wood", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_floors_carpet", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_floors_other", Tables.construction_ebm.table_name, "record_id", record_id));

        if(!cb_req_field.contains("true")){
            app_type_req_field.add("");
        }

        if (cb_req_field.get(cb_req_field.size()-1).contains("true")){
            app_type_req_field.add(db.getRecord("report_floors_other_value", Tables.construction_ebm.table_name, "record_id", record_id));
        }

        //walls
        cb_req_field.clear();
        cb_req_field.add(db.getRecord("report_walls_chb", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_walls_chb_cement", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_walls_anay", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_walls_chb_wood", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_walls_precast", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_walls_decorative_stone", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_walls_adobe", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_walls_ceramic_tiles", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_walls_cast_in_place", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_walls_sandblast", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_walls_mactan_stone", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_walls_painted", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_walls_other", Tables.construction_ebm.table_name, "record_id", record_id));

        if(!cb_req_field.contains("true")){
            app_type_req_field.add("");
        }

        if (cb_req_field.get(cb_req_field.size()-1).contains("true")){
            app_type_req_field.add(db.getRecord("report_walls_other_value", Tables.construction_ebm.table_name, "record_id", record_id));
        }

        //partitions
        cb_req_field.clear();
        cb_req_field.add(db.getRecord("report_partitions_painted_cement", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_partitions_anay", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_partitions_wood_boards", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_partitions_precast", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_partitions_decorative_stone", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_partitions_adobe", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_partitions_granite", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_partitions_cast_in_place", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_partitions_sandblast", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_partitions_mactan_stone", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_partitions_ceramic_tiles", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_partitions_chb_plywood", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_partitions_hardiflex", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_partitions_wallpaper", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_partitions_painted", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_partitions_other", Tables.construction_ebm.table_name, "record_id", record_id));

        if(!cb_req_field.contains("true")){
            app_type_req_field.add("");
        }

        if (cb_req_field.get(cb_req_field.size()-1).contains("true")){
            app_type_req_field.add(db.getRecord("report_partitions_other_value", Tables.construction_ebm.table_name, "record_id", record_id));
        }

        //windows
        cb_req_field.clear();
        cb_req_field.add(db.getRecord("report_windows_steel_casement", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_windows_fixed_view", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_windows_analok_sliding", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_windows_alum_glass", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_windows_aluminum_sliding", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_windows_awning_type", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_windows_powder_coated", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_windows_wooden_frame", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_windows_other", Tables.construction_ebm.table_name, "record_id", record_id));

        if(!cb_req_field.contains("true")){
            app_type_req_field.add("");
        }

        if (cb_req_field.get(cb_req_field.size()-1).contains("true")){
            app_type_req_field.add(db.getRecord("report_windows_other_value", Tables.construction_ebm.table_name, "record_id", record_id));
        }

        //doors
        cb_req_field.clear();
        cb_req_field.add(db.getRecord("report_doors_wood_panel", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_doors_pvc", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_doors_analok_sliding", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_doors_screen_door", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_doors_flush", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_doors_molded_door", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_doors_aluminum_sliding", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_doors_flush_french", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_doors_other", Tables.construction_ebm.table_name, "record_id", record_id));

        if(!cb_req_field.contains("true")){
            app_type_req_field.add("");
        }

        if (cb_req_field.get(cb_req_field.size()-1).contains("true")){
            app_type_req_field.add(db.getRecord("report_doors_other_value", Tables.construction_ebm.table_name, "record_id", record_id));
        }

        //ceiling
        cb_req_field.clear();
        cb_req_field.add(db.getRecord("report_ceiling_plywood", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_ceiling_painted_gypsum", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_ceiling_soffit_slab", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_ceiling_metal_deck", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_ceiling_hardiflex", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_ceiling_plywood_tg", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_ceiling_plywood_pvc", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_ceiling_painted", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_ceiling_with_cornice", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_ceiling_with_moulding", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_ceiling_drop_ceiling", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_ceiling_other", Tables.construction_ebm.table_name, "record_id", record_id));

        if(!cb_req_field.contains("true")){
            app_type_req_field.add("");
        }

        if (cb_req_field.get(cb_req_field.size()-1).contains("true")){
            app_type_req_field.add(db.getRecord("report_ceiling_other_value", Tables.construction_ebm.table_name, "record_id", record_id));
        }

        //roof
        cb_req_field.clear();
        cb_req_field.add(db.getRecord("report_roof_pre_painted", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_roof_rib_type", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_roof_tilespan", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_roof_tegula_asphalt", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_roof_tegula_longspan", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_roof_tegula_gi", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_roof_steel_concrete", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_roof_polycarbonate", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_roof_on_steel_trusses", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_roof_on_wooden_trusses", Tables.construction_ebm.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_roof_other", Tables.construction_ebm.table_name, "record_id", record_id));

        if(!cb_req_field.contains("true")){
            app_type_req_field.add("");
        }

        if (cb_req_field.get(cb_req_field.size()-1).contains("true")){
            app_type_req_field.add(db.getRecord("report_roof_other_value", Tables.construction_ebm.table_name, "record_id", record_id));
        }



        return app_type_req_field;

    }

    ArrayList<String> cebm_fields_set3(String record_id, Context context){//room list

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

//        app_type_req_field.add(db.getRecord("record_id", Tables.construction_ebm_room_list.table_name, "record_id", record_id));

        ArrayList<String> id = db.getRecordColumn("id", Tables.construction_ebm_room_list.table_name, "record_id", record_id);

        if (!id.isEmpty()) {//if at least 1 is required

            for (int x = 0; x < id.size(); x++){
                //required fields inside
                app_type_req_field.add(db.getRecord("report_room_list_area", Tables.construction_ebm_room_list.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_room_list_description", Tables.construction_ebm_room_list.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_room_list_floor", Tables.construction_ebm_room_list.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_room_list_rcn", Tables.construction_ebm_room_list.table_name, "id", id.get(x)));
            }

        } else {
            app_type_req_field.add("");
        }

        return app_type_req_field;

    }

    ArrayList<String> cebm_fields_set4(String record_id, Context context){//remarks

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

        app_type_req_field.add(db.getRecord("report_valuation_remarks", Tables.construction_ebm.table_name, "record_id", record_id));

        return app_type_req_field;

    }


    /**
     * Townhouse
     */

    ArrayList<String> th_fields_set1(String record_id, Context context){//owner

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

//        app_type_req_field.add(db.getRecord("record_id", Tables.townhouse_lot_details.table_name, "record_id", record_id));

        ArrayList<String> id = db.getRecordColumn("id", Tables.townhouse_lot_details.table_name, "record_id", record_id);

        if (!id.isEmpty()) {//if at least 1 is required

            for (int x = 0; x < id.size(); x++){
                //required fields inside
                app_type_req_field.add(db.getRecord("report_propdesc_tct_no", Tables.townhouse_lot_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_propdesc_lot", Tables.townhouse_lot_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_propdesc_block", Tables.townhouse_lot_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_propdesc_survey_nos", Tables.townhouse_lot_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_propdesc_area", Tables.townhouse_lot_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("valrep_townhouse_propdesc_registry_date", Tables.townhouse_lot_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_propdesc_registered_owner", Tables.townhouse_lot_details.table_name, "id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_propdesc_registry_of_deeds", Tables.townhouse_lot_details.table_name, "id", id.get(x)));
            }


        } else {
            app_type_req_field.add("");
        }

        return app_type_req_field;

    }

    ArrayList<String> th_fields_set2(String record_id, Context context){//land details

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

        ArrayList<String> cb_req_field = new ArrayList<>();
        //basis of identification
        cb_req_field.clear();
        cb_req_field.add(db.getRecord("report_id_association", Tables.townhouse.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_id_tax", Tables.townhouse.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_id_lra", Tables.townhouse.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_id_lot_config", Tables.townhouse.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_id_subd_map", Tables.townhouse.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_id_nbrhood_checking", Tables.townhouse.table_name, "record_id", record_id));

        if(!cb_req_field.contains("true")){
            app_type_req_field.add("");
        }

        //physical char
        cb_req_field.clear();
        cb_req_field.add(db.getRecord("report_physical_corner_lot", Tables.townhouse.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_physical_non_corner_lot", Tables.townhouse.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_physical_perimeter_lot", Tables.townhouse.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_physical_intersected_lot", Tables.townhouse.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_physical_interior_with_row", Tables.townhouse.table_name, "record_id", record_id));
        cb_req_field.add(db.getRecord("report_physical_landlocked", Tables.townhouse.table_name, "record_id", record_id));

        if(!cb_req_field.contains("true")){
            app_type_req_field.add("");
        }

        app_type_req_field.add(db.getRecord("report_date_inspected_month", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_date_inspected_day", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_date_inspected_year", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_imp_street_name", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_imp_street", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_imp_street_name", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_imp_sidewalk", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_imp_curb", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_imp_drainage", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_imp_street_lights", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_lotclass_per_tax_dec", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_lotclass_actual_usage", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_lotclass_neighborhood", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_lotclass_highest_best_use", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_util_electricity", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_util_water", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_util_telephone", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_util_garbage", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bound_right", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bound_left", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bound_rear", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bound_front", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_landmark_1", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_distance_1", Tables.townhouse.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("valrep_townhouse_road_right_of_way", Tables.townhouse.table_name, "record_id", record_id));

        String nature_appraisal = db.getRecord("nature_appraisal", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id);

        if(nature_appraisal.contentEquals("Re-appraisal")) {
            app_type_req_field.add(db.getRecord("report_prev_date", Tables.townhouse.table_name, "record_id", record_id));
            app_type_req_field.add(db.getRecord("report_prev_appraiser", Tables.townhouse.table_name, "record_id", record_id));
        }
        return app_type_req_field;

    }

    ArrayList<String> th_fields_set3(String record_id, Context context){//comparatives

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

//        app_type_req_field.add(db.getRecord("record_id", Tables.townhouse.table_name, "record_id", record_id));

        return app_type_req_field;

    }

    ArrayList<String> th_fields_set4a(String record_id, Context context){//desc of imp

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

//        app_type_req_field.add(db.getRecord("record_id", Tables.townhouse_imp_details.table_name, "record_id", record_id));

        ArrayList<String> id = db.getRecordColumn("imp_details_id", Tables.townhouse_imp_details.table_name, "record_id", record_id);

        if (!id.isEmpty()) {//if at least 1 is required

            for (int x = 0; x < id.size(); x++){
                //required fields inside
                app_type_req_field.add(db.getRecord("report_impsummary1_building_desc", Tables.townhouse_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_impsummary1_no_of_floors", Tables.townhouse_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_impsummary_no_of_bedroom", Tables.townhouse_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_foundation", Tables.townhouse_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_columns_posts", Tables.townhouse_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_beams", Tables.townhouse_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_exterior_walls", Tables.townhouse_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_interior_walls", Tables.townhouse_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_imp_flooring", Tables.townhouse_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_doors", Tables.townhouse_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_imp_windows", Tables.townhouse_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_ceiling", Tables.townhouse_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_imp_roofing", Tables.townhouse_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_trusses", Tables.townhouse_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_economic_life", Tables.townhouse_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_effective_age", Tables.townhouse_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_occupants", Tables.townhouse_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_imp_floor_area", Tables.townhouse_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_desc_confirmed_thru", Tables.townhouse_imp_details.table_name, "imp_details_id", id.get(x)));
                app_type_req_field.add(db.getRecord("report_ownership_of_property", Tables.townhouse_imp_details.table_name, "imp_details_id", id.get(x)));
            }

        } else {
            app_type_req_field.add("");
        }

        return app_type_req_field;

    }

    ArrayList<String> th_fields_set4b(String record_id, Context context){//desc of imp features

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

        ArrayList<String> id = db.getRecordColumn("imp_details_id", Tables.townhouse_imp_details.table_name, "record_id", record_id);

        if (!id.isEmpty()) { //if at least 1 is required

            for (int x = 0; x < id.size(); x++){

                ArrayList<String> f_id = db.getRecordColumn("imp_details_features_id", Tables.townhouse_imp_details_features.table_name, "imp_details_id", id.get(x));
                    if(!f_id.isEmpty()){
                        for (int y = 0; y < f_id.size(); y++){
                            //required fields inside
                            app_type_req_field.add(gds.nullCheck3(db.getRecord("report_desc_features_area", Tables.townhouse_imp_details_features.table_name, "imp_details_features_id", f_id.get(y))));

                        }
                    }else{
                        app_type_req_field.add("");
                    }

                }

        } else {
            app_type_req_field.add("");
        }

        return app_type_req_field;

    }

    ArrayList<String> th_fields_set5(String record_id, Context context){//market

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

//        app_type_req_field.add(db.getRecord("record_id", Tables.townhouse_lot_valuation_details.table_name, "record_id", record_id));

        return app_type_req_field;

    }

    ArrayList<String> th_fields_set6(String record_id, Context context){//summary

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

        app_type_req_field.add(db.getRecord("report_remarks", Tables.townhouse.table_name, "record_id", record_id));

        return app_type_req_field;

    }
    //prev app
    ArrayList<String> th_fields_set7(String record_id, Context context){//prev appraisal

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

        String nature_appraisal = db.getRecord("nature_appraisal", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id);
        if(nature_appraisal.contentEquals("Re-appraisal")) {
            ArrayList<String> id = db.getRecordColumn("id", Tables.townhouse_main_prev_appraisal.table_name, "record_id", record_id);

            if (!id.isEmpty()) { //if at least 1 is required

                for (int x = 0; x < id.size(); x++) {
                    //required fields inside
                    app_type_req_field.add(db.getRecord("report_main_prev_desc", Tables.townhouse_main_prev_appraisal.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_main_prev_area", Tables.townhouse_main_prev_appraisal.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_main_prev_unit_value", Tables.townhouse_main_prev_appraisal.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_main_prev_appraised_value", Tables.townhouse_main_prev_appraisal.table_name, "id", id.get(x)));
                }

            } else {
                app_type_req_field.add("");
            }

            id.clear();
        }
        return app_type_req_field;

    }

    /**
     * Motor Vehicle
     */



    ArrayList<String> mv_fields_set1(String record_id, Context context){//owner

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

        app_type_req_field.add(db.getRecord("vehicle_type", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("car_model", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("car_mileage", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("car_series", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("car_color", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("car_plate_no", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("car_body_type", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("car_displacement", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("car_motor_no", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("car_chassis_no", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("car_cr_no", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("car_no_of_cylinders", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("car_cr_date_month", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("car_cr_date_day", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("car_cr_date_year", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id));

        return app_type_req_field;

    }

    ArrayList<String> mv_fields_set2(String record_id, Context context){//owner

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

        app_type_req_field.add(db.getRecord("report_interior_dashboard", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_interior_sidings", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_interior_seats", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_interior_windows", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_interior_ceiling", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_interior_instrument", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bodytype_grille", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bodytype_headlight", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bodytype_fbumper", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bodytype_hood", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bodytype_rf_fender", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bodytype_rf_door", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bodytype_rr_door", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bodytype_rr_fender", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bodytype_backdoor", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bodytype_taillight", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bodytype_r_bumper", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bodytype_lr_fender", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bodytype_lr_door", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bodytype_lf_door", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bodytype_lf_fender", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bodytype_top", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bodytype_paint", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_bodytype_flooring", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("valrep_mv_bodytype_trunk", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_misc_stereo", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_misc_tools", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_misc_speakers", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_misc_airbag", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_misc_tires", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_misc_mag_wheels", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_misc_carpet", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_misc_others", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_enginearea_fuel", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_enginearea_transmission1", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_enginearea_chassis", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_enginearea_battery", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_enginearea_electrical", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_enginearea_engine", Tables.motor_vehicle.table_name, "record_id", record_id));

        return app_type_req_field;

    }
    ArrayList<String> mv_fields_set3a(String record_id, Context context){//owner

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

        app_type_req_field.add(db.getRecord("report_place_inspected", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_market_valuation_remarks", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("valrep_mv_market_valuation_previous_remarks", Tables.motor_vehicle.table_name, "record_id", record_id));

        return app_type_req_field;

    }
    ArrayList<String> mv_fields_set3b(String record_id, Context context){//owner

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

        app_type_req_field.add(db.getRecord("report_place_inspected", Tables.motor_vehicle.table_name, "record_id", record_id));
        app_type_req_field.add(db.getRecord("report_market_valuation_remarks", Tables.motor_vehicle.table_name, "record_id", record_id));

        return app_type_req_field;

    }

    //prev app
    ArrayList<String> mv_fields_set4(String record_id, Context context){//prev appraisal

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        ArrayList<String> app_type_req_field = new ArrayList<>();

        String nature_appraisal = db.getRecord("nature_appraisal", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id);
        if(nature_appraisal.contentEquals("Re-appraisal")) {
            ArrayList<String> id = db.getRecordColumn("id", Tables.motor_vehicle_previous_appraisal.table_name, "record_id", record_id);

            if (!id.isEmpty()) { //if at least 1 is required

                for (int x = 0; x < id.size(); x++) {
                    //required fields inside
                    app_type_req_field.add(db.getRecord("report_prev_date", Tables.motor_vehicle_previous_appraisal.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_prev_appraiser", Tables.motor_vehicle_previous_appraisal.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_prev_requestor", Tables.motor_vehicle_previous_appraisal.table_name, "id", id.get(x)));
                    app_type_req_field.add(db.getRecord("report_prev_appraised_value", Tables.motor_vehicle_previous_appraisal.table_name, "id", id.get(x)));
                }

            } else {
                app_type_req_field.add("");
            }

            id.clear();
        }
        return app_type_req_field;

    }


    /*public boolean isRequiredFieldsComplete(String record_id, Context context){

        String app_type = new DatabaseHandler_New(context).getRecord("appraisal_type", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id);

        if (app_type.contentEquals( gv.appraisal_type_value[0] )) {//machinery_equipment

//            req_field = _fields_set1(record_id, context);

        }

        if (app_type.contentEquals( gv.appraisal_type_value[1] )) {//vacant_lot

            req_field = vl_fields_set1(record_id, context);
            req_field = vl_fields_set2(record_id, context);
            req_field = vl_fields_set3(record_id, context);
            req_field = vl_fields_set4(record_id, context);
            req_field = vl_fields_set5(record_id, context);

        }

        if (app_type.contentEquals( gv.appraisal_type_value[2] )) {//land_improvements

//            req_field = _fields_set1(record_id, context);

        }

        if (app_type.contentEquals( gv.appraisal_type_value[3] )) {//townhouse_condo

//            req_field = _fields_set1(record_id, context);

        }

        if (app_type.contentEquals( gv.appraisal_type_value[4] )) {//ppcr

//            req_field = _fields_set1(record_id, context);

        }

        if (app_type.contentEquals( gv.appraisal_type_value[5] )) {//construction_ebm

//            req_field = _fields_set1(record_id, context);

        }

        if (app_type.contentEquals( gv.appraisal_type_value[6] )) {//motor_vehicle

//            req_field = _fields_set1(record_id, context);

        }

        if (app_type.contentEquals( gv.appraisal_type_value[7] )) {//townhouse

//            req_field = _fields_set1(record_id, context);

        }

        for (int x = 0; x < req_field.size(); x++){
            if (req_field.get(x).contentEquals("")){
//                    dialog_incomplete(context);
                return false;
            }
        }

        return true;

    }*/

    String[] module_names = new String[]{};
    ArrayList<Integer> module_numbers = new ArrayList<>();

    public boolean isModuleComplete(String record_id, Context context){

        String app_type = new DatabaseHandler_New(context).getRecord("appraisal_type", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id);

        ArrayList<ArrayList> arrayLists = new ArrayList<>();

        if (app_type.contentEquals( gv.appraisal_type_value[0] )) {//machinery_equipment

            /*module_names = new String[]{
                    "Ownership",
            };*/
//            arrayLists.add(_fields_set1(record_id, context));

        }

        if (app_type.contentEquals( gv.appraisal_type_value[1] )) {//vacant_lot
            module_names = new String[]{
                    "- Ownership and Property Description",
                    "- Land Details",
                    "- Comparative Analysis",
                    "- Market Valuation",
                    "- Summary and Remarks",
            };

            String unacceptable = new DatabaseHandler_New(context).getRecord("app_unacceptable_collateral", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id);

            if(!unacceptable.contentEquals("true")){
                module_names = new String[]{
                        "- Ownership and Property Description",
                        "- Land Details",
                        "- Comparative Analysis",
                        "- Market Valuation",
                        "- Summary and Remarks",
                        "- Previous Appraisal",
                };

                arrayLists.add(vl_fields_set1(record_id, context));
                arrayLists.add(vl_fields_set2(record_id, context));
                arrayLists.add(vl_fields_set3(record_id, context));
                arrayLists.add(vl_fields_set4(record_id, context));
                arrayLists.add(vl_fields_set5(record_id, context));
                arrayLists.add(vl_fields_set6(record_id, context));

            } else {
                module_names = new String[]{
                        "- Summary and Remarks",
                };

                arrayLists.add(vl_fields_set5(record_id, context));
            }

        }

        if (app_type.contentEquals( gv.appraisal_type_value[2] )) {//land_improvements

            module_names = new String[]{
                    "- Ownership and Property Description",
                    "- Land Details",
                    "- Comparative Analysis",
                    "- Description of Improvements",
                    "- Market Valuation",
                    "- Cost Approach",
                    "- Summary and Remarks",
            };

            String unacceptable = new DatabaseHandler_New(context).getRecord("app_unacceptable_collateral", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id);

            if(!unacceptable.contentEquals("true")){
                module_names = new String[]{
                        "- Ownership and Property Description",
                        "- Land Details",
                        "- Comparative Analysis",
                        "- Description of Improvements",
                        "- Market Valuation",
                        "- Cost Approach",
                        "- Summary and Remarks",
                        "- Previous Appraisal",
                };

                arrayLists.add(li_fields_set1(record_id, context));
                arrayLists.add(li_fields_set2(record_id, context));
                arrayLists.add(li_fields_set3(record_id, context));
                arrayLists.add(li_fields_set4(record_id, context));
                arrayLists.add(li_fields_set5(record_id, context));
                arrayLists.add(li_fields_set6(record_id, context));
                arrayLists.add(li_fields_set7(record_id, context));
                arrayLists.add(li_fields_set8(record_id, context));

            } else {
                module_names = new String[]{
                        "- Summary and Remarks",
                };
                arrayLists.add(li_fields_set7(record_id, context));
            }

        }

        if (app_type.contentEquals( gv.appraisal_type_value[3] )) {//townhouse_condo

            module_names = new String[]{
                    "- Ownership and Property Description",
                    "- Unit Details",
                    "- Comparative Analysis",
                    "- Description of Condo Unit/s",
                    "- Market Valuation",
                    "- Summary and Remarks",
                    "- Previous Appraisal",
            };
            arrayLists.add(condo_fields_set1(record_id, context));
            arrayLists.add(condo_fields_set2(record_id, context));
            arrayLists.add(condo_fields_set3(record_id, context));
            arrayLists.add(condo_fields_set4(record_id, context));
            arrayLists.add(condo_fields_set5(record_id, context));
            arrayLists.add(condo_fields_set6(record_id, context));
            arrayLists.add(condo_fields_set7(record_id, context));

        }

        if (app_type.contentEquals( gv.appraisal_type_value[4] )) {//ppcr

            module_names = new String[]{
                    "- Details",
                    "- Visit History",
            };
            arrayLists.add(ppcr_fields_set1(record_id, context));
            arrayLists.add(ppcr_fields_set2(record_id, context));

        }

        if (app_type.contentEquals( gv.appraisal_type_value[5] )) {//construction_ebm

            module_names = new String[]{
                    "- Project Description",
                    "- Details of the Proposed Construction",
                    "- Room List",
                    "- Remarks / Other Information",
            };
            arrayLists.add(cebm_fields_set1(record_id, context));
            arrayLists.add(cebm_fields_set2(record_id, context));
            arrayLists.add(cebm_fields_set3(record_id, context));
            arrayLists.add(cebm_fields_set4(record_id, context));

        }

        if (app_type.contentEquals( gv.appraisal_type_value[6] )) {//motor_vehicle

            module_names = new String[]{
                    "- Request Details",
                    "- Interior, Type of Body, Accessories, Engine Area",
                    "- Verification, Place Inspected",
                    /*"- Previous Appraisal",*/
            };
            arrayLists.add(mv_fields_set1(record_id, context));
            arrayLists.add(mv_fields_set2(record_id, context));

            String kind_of_appraisal = new DatabaseHandler_New(context).getRecord("kind_of_appraisal", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id);

            if(kind_of_appraisal.contentEquals("Table Appraisal")){
                arrayLists.add(mv_fields_set3a(record_id, context));
            }else{
                arrayLists.add(mv_fields_set3b(record_id, context));

            }

            //arrayLists.add(mv_fields_set4(record_id, context));

        }

        if (app_type.contentEquals( gv.appraisal_type_value[7] )) { //townhouse

            module_names = new String[]{
                    "- Ownership and Property Description",
                    "- Land Details",
                    "- Comparative Analysis",
                    "- Description of Improvements",
                    "- Features/Division",
                    "- Market Data Approach",
                    "- Summary and Remarks",
                    "- Previous Appraisal",
            };
            arrayLists.add(th_fields_set1(record_id, context));
            arrayLists.add(th_fields_set2(record_id, context));
            arrayLists.add(th_fields_set3(record_id, context));
            arrayLists.add(th_fields_set4a(record_id, context));
            arrayLists.add(th_fields_set4b(record_id, context));
            arrayLists.add(th_fields_set5(record_id, context));
            arrayLists.add(th_fields_set6(record_id, context));
            arrayLists.add(th_fields_set7(record_id, context));

        }

        boolean noBlank = true;

        Log.e("arrayLists", arrayLists.size()+"");
        if (!arrayLists.isEmpty()) {
            for (int a = 0; a < arrayLists.size(); a++) {

                req_field = arrayLists.get(a);
//                module_numbers.clear();


                for (int x = 0; x < req_field.size(); x++){
                    if (req_field.get(x).contentEquals("")){

                        //                    dialog_incomplete(context, a);
                        module_numbers.add(a);

                        /*if (x == req_field.size()-1) {
                            dialog_incomplete(context, module_numbers);
                            Log.e("module_numbers", module_numbers.toString());
                            return false;
                        }*/
                        noBlank = false;

                    }
                }

                if (a == arrayLists.size()-1) {
                    if (!noBlank) {
                        dialog_incomplete(context, module_numbers);
                    }
                    Log.e("module_numbers", module_numbers.toString());
                    return noBlank;
                }

            }
        }

        return true;

    }

    void dialog_incomplete(Context context, ArrayList<Integer> arrayList){
        /*new android.support.v7.app.AlertDialog.Builder(context)
                .setTitle("Incomplete Required Fields")
                .setMessage("You must fill up all required fields in module "+module_number+" before you can submit this record")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setIcon(R.drawable.warning_white)
                .show();*/

        String inc_modules = "";

        Log.e("arrayList", arrayList.toString());
// add elements to al, including duplicates
        Set<Integer> hs = new HashSet<>();
        hs.addAll(arrayList);
        arrayList.clear();
        arrayList.addAll(hs);

        for (int x = 0; x < arrayList.size(); x++){
            int a = arrayList.get(x);
            inc_modules = inc_modules+"\n"+ module_names[a];
        }

        // custom dialog
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.warning_dialog);

        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_question);
        text.setText("You must fill up all required fields in"+inc_modules+"\nbefore you can submit this record");
        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);

        // if button is clicked, close the custom dialog
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }



    //field checker
    public boolean checker(
            EditText[] etfields,
            AutoCompleteTextView[] actfields,
            Spinner[] spfields
    ){

        //prompt all edittext
        if (etfields != null) {
            for(int i=0; i<etfields.length; i++){
                EditText currentField=etfields[i];
                if(currentField.getText().toString().length()<=0){
                    currentField.setError("Fill up this field");
//					currentField.setBackgroundColor(Color.RED);
                } else {
//                    currentField.setBackgroundResource(R.drawable.item_frame);
                    currentField.setError(null);
//                    currentField.setBackgroundColor(Color.TRANSPARENT);
                }
            }
        }

        if (etfields != null) {
            for(int i=0; i<etfields.length; i++){
                EditText currentField=etfields[i];
                if(currentField.getText().toString().length()<=0){
                    currentField.setError("Fill up this field");
//					currentField.setBackgroundColor(Color.RED);
                    return false;
                } else {
//                    currentField.setBackgroundResource(R.drawable.item_frame);
                    currentField.setError(null);
//                    currentField.setBackgroundColor(Color.TRANSPARENT);
                }
            }
        }

        //prompt all autocomplete
        if (actfields != null) {
            for(int i=0; i<actfields.length; i++){
                AutoCompleteTextView currentField=actfields[i];
                if(currentField.getText().toString().length()<=0){
                    currentField.setError("Fill up this field");
//					currentField.setBackgroundColor(Color.RED);
                } else {
                    currentField.setError(null);
//                    currentField.setBackgroundColor(Color.TRANSPARENT);
                }
            }
        }

        if (actfields != null) {
            for(int i=0; i<actfields.length; i++){
                AutoCompleteTextView currentField=actfields[i];
                if(currentField.getText().toString().length()<=0){
                    currentField.setError("Fill up this field");
//					currentField.setBackgroundColor(Color.RED);
                    return false;
                } else {
                    currentField.setError(null);
//                    currentField.setBackgroundColor(Color.TRANSPARENT);
                }
            }
        }

        //prompt all spinner
        if (spfields != null) {
            for(int i=0; i<spfields.length; i++){
                Spinner currentField=spfields[i];
                if(currentField.getSelectedItem().toString().length()<=0){
//                    currentField.setBackgroundColor(Color.RED);
//                    ((TextView)currentField.getSelectedView()).setError("Fill up this field");
                    try{
                        ((TextView)currentField.getSelectedView()).setError("Fill up this field");
                    }catch(NullPointerException e){
                        currentField.setBackgroundColor(Color.parseColor("#FF0000"));
                    }

                } else {
//                    ((TextView)currentField.getSelectedView()).setError("");
//                    currentField.setBackgroundColor(Color.TRANSPARENT);
                }
            }
        }

        if (spfields != null) {
            for(int i=0; i<spfields.length; i++){
                Spinner currentField=spfields[i];
                if(currentField.getSelectedItem().toString().length()<=0){
//                    currentField.setBackgroundColor(Color.RED);
//                    ((TextView)currentField.getSelectedView()).setError("Fill up this field");
                    try{
                        ((TextView)currentField.getSelectedView()).setError("Fill up this field");
                    }catch(NullPointerException e){
                        currentField.setBackgroundColor(Color.parseColor("#FF0000"));
                    }
                    return false;
                } else {
//                    ((TextView)currentField.getSelectedView()).setError("");
//                    currentField.setBackgroundColor(Color.TRANSPARENT);
                }
            }
        }

        return true;
    }


}