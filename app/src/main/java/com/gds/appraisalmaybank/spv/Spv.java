package com.gds.appraisalmaybank.spv;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.check_network.NetworkUtil;
import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Images;
import com.gds.appraisalmaybank.database.Report_Accepted_Jobs;
import com.gds.appraisalmaybank.database.Report_Accepted_Jobs_Contacts;
import com.gds.appraisalmaybank.database.Report_filename;
import com.gds.appraisalmaybank.database.Required_Attachments;
import com.gds.appraisalmaybank.database.TableObjects;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.json.TLSConnection;
import com.gds.appraisalmaybank.json.UserFunctions;
import com.gds.appraisalmaybank.main.Connectivity;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;
import com.gds.appraisalmaybank.required_fields.Fields_Checker;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by ianlozares on 26/10/2017.
 */

public class Spv extends Activity implements View.OnClickListener {

    GDS_methods gds = new GDS_methods();
    TableObjects to = new TableObjects();
    Session_Timer st = new Session_Timer(this);
    ActionBar actionBar;
    private static final String TAG_UID = "uid";
    private static final String TAG_RECORD_ID = "record_id";
    private static final String TAG_PASS_STAT = "pass_stat";
    String pass_stat = "offline";

    DatabaseHandler db = new DatabaseHandler(this);
    DatabaseHandler2 db2 = new DatabaseHandler2(this);
    Global gs;
    TLSConnection tlscon = new TLSConnection();
    ArrayList<String> field = new ArrayList<String>();
    ArrayList<String> value = new ArrayList<String>();
    // for attachments
    int responseCode;
    //for fileUpload checking
    boolean fileUploaded = false;
    String xml;
    String google_response = "";
    private ListView dlg_priority_lvw = null;
    Context context = this;
    String item, filename;
    String dir = "gds_appraisal";
    File myDirectory = new File(Environment.getExternalStorageDirectory() + "/"
            + dir + "/");

    //detect internet connection
    TextView tv_connection;
    boolean wifi = false;
    boolean data = false;
    boolean data_speed = false;

    private NetworkChangeReceiver receiver;
    private boolean isConnected = false;
    boolean globalSave = false;
    UserFunctions userFunction = new UserFunctions();
    String where = "WHERE record_id = args";
    String[] args = new String[1];


    String attachment_selected, uid;
    String db_app_uid, db_app_file, db_app_filename, db_app_appraisal_type,
            db_app_candidate_done;

    TextView tv_requested_month, tv_requested_day, tv_requested_year,
            tv_classification, tv_account_name,tv_company_name, tv_requesting_party, tv_requestor,
            tv_control_no, tv_appraisal_type, tv_nature_appraisal,
            tv_ins_month1, tv_ins_day1, tv_ins_year1, tv_ins_month2,
            tv_ins_day2, tv_ins_year2, tv_com_month, tv_com_day, tv_com_year,
            tv_tct_no, tv_unit_no, tv_bldg_name, tv_street_no, tv_street_name,
            tv_village, tv_district, tv_zip_code, tv_city, tv_province,
            tv_region, tv_country, tv_address,
            tv_attachment, tv_app_request_remarks, tv_app_kind_of_appraisal, tv_app_branch_code;
    LinearLayout ll_contact;
    EditText et_first_name, et_middle_name, et_last_name, et_requestor,app_account_company_name;
    CheckBox app_account_is_company;
    Spinner spinner_purpose_appraisal;
    EditText et_app_kind_of_appraisal;
    AutoCompleteTextView et_requesting_party;
    EditText report_date_requested;
    Button btn_update_app_details, btn_save_app_details, btn_cancel_app_details;

    String requested_month, requested_day, requested_year, classification,
            account_fname, account_mname, account_lname, requesting_party, requestor,
            control_no, appraisal_type, nature_appraisal, ins_month1, ins_day1,
            ins_year1, ins_month2, ins_day2, ins_year2, com_month, com_day,
            com_year, tct_no, unit_no, bldg_name, street_no, street_name,
            village, district, zip_code, city, province, region, country,
            counter, output_appraisal_type, output_street_name, output_village,
            output_city, output_province, app_request_remarks, app_branch_code, app_kind_of_appraisal,account_is_company,account_company_name;
    String lot_no, block_no;
    String record_id, pdf_name,rework_reason = "";

    ImageView btn_project_details,btn_model_house,btn_comparatives,btn_cost_approach,btn_summary_val,btn_note_remarks;
    Button btn_report_update_cc, btn_report_view_report,btn_manual_delete,btn_update_address, btn_save_address;
    ProgressDialog pDialog;
    Dialog myDialog;
    ImageView btn_select_pdf, btn_report_pdf,btn_mysql_attachments;

    TextView tv_address_hide;
    // uploaded attachments
    LinearLayout container_attachments;


    //address
    EditText et_unit_no, et_building_name, et_lot_no, et_block_no, et_street_no, et_street_name,
            et_village, et_district, et_zip_code, et_city, et_province,
            et_region, et_country;


    String uploaded_attachment;
    String url_webby;
    public static final int progress_bar_type = 0;

    boolean network_status,appStatReviewer = false;

    File uploaded_file;
    String[] commandArray = new String[]{"View Attachment in Browser",
            "Download Attachment"};

    boolean withAttachment = false, freshData = true;
    TextView tv_rework_reason_header, tv_rework_reason;

    public class NetworkChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            isNetworkAvailable(context);
        }

        private boolean isNetworkAvailable(final Context context) {
            ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null) {
                    for (int i = 0; i < info.length; i++) {
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            if (!isConnected) {
                                tv_connection.setText("Connected");
                                tv_connection.setBackgroundColor(Color.parseColor("#3399ff"));
                                isConnected = true;
                                // do your processing here ---
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        // Do something after 1s = 1000ms
                                        wifi = Connectivity.isConnectedWifi(context);
                                        data = Connectivity.isConnectedMobile(context);
                                        data_speed = Connectivity.isConnected(context);
                                        if (wifi) {
                                            tv_connection.setText(R.string.wifi_connected);
                                            tv_connection.setBackgroundColor(Color.parseColor("#39b476"));
                                        } else if (data) {
                                            if (data_speed) {
                                                tv_connection.setText(R.string.data_good);
                                                tv_connection.setBackgroundColor(Color.parseColor("#39b476"));
                                            } else if (data_speed == false) {
                                                tv_connection.setText(R.string.data_weak);
                                                tv_connection.setBackgroundColor(Color.parseColor("#FF9900"));
                                            }
                                        }
                                    }
                                }, 1000);

                            }
                            return true;
                        }
                    }
                }
            }
            tv_connection.setText("No Internet connection");
            tv_connection.setBackgroundColor(Color.parseColor("#CC0000"));
            isConnected = false;
            return false;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.spv);
        st.resetDisconnectTimer();
        Authenticator.setDefault(new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("gdsuser", "gdsuser01".toCharArray());
            }
        });
        actionBar = getActionBar();
        actionBar.setIcon(R.drawable.ic_spv);
        actionBar.setSubtitle("SPV - Land & Improvements");
        String open = "0";
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            open = bundle.getString("keyopen");
        }
        tv_connection = (TextView) findViewById(R.id.tv_connection);
        tv_connection.setVisibility(View.GONE);
		/*IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
		receiver = new NetworkChangeReceiver();
		registerReceiver(receiver, filter);*/

        gs = ((Global) getApplicationContext());
        container_attachments = (LinearLayout) findViewById(R.id.container_attachments);
        ll_contact = (LinearLayout) findViewById(R.id.container);
        tv_requested_month = (TextView) findViewById(R.id.tv_requested_month);
        tv_requested_day = (TextView) findViewById(R.id.tv_requested_day);
        tv_requested_year = (TextView) findViewById(R.id.tv_requested_year);
        tv_classification = (TextView) findViewById(R.id.tv_classification);
        tv_account_name = (TextView) findViewById(R.id.tv_account_name);
        tv_company_name = (TextView) findViewById(R.id.tv_company_name);
        tv_requesting_party = (TextView) findViewById(R.id.tv_requesting_party);
        tv_requestor = (TextView) findViewById(R.id.tv_requestor);
        tv_control_no = (TextView) findViewById(R.id.tv_control_no);
        tv_appraisal_type = (TextView) findViewById(R.id.tv_appraisal_type);
        tv_nature_appraisal = (TextView) findViewById(R.id.tv_nature_appraisal);
        tv_ins_month1 = (TextView) findViewById(R.id.tv_ins_month1);
        tv_ins_day1 = (TextView) findViewById(R.id.tv_ins_day1);
        tv_ins_year1 = (TextView) findViewById(R.id.tv_ins_year1);
        tv_ins_month2 = (TextView) findViewById(R.id.tv_ins_month2);
        tv_ins_day2 = (TextView) findViewById(R.id.tv_ins_day2);
        tv_ins_year2 = (TextView) findViewById(R.id.tv_ins_year2);
        tv_com_month = (TextView) findViewById(R.id.tv_com_month);
        tv_com_day = (TextView) findViewById(R.id.tv_com_day);
        tv_com_year = (TextView) findViewById(R.id.tv_com_year);
        tv_tct_no = (TextView) findViewById(R.id.tv_tct_no);
        tv_unit_no = (TextView) findViewById(R.id.tv_unit_no);
        tv_bldg_name = (TextView) findViewById(R.id.tv_bldg_name);
        tv_street_no = (TextView) findViewById(R.id.tv_street_no);
        tv_street_name = (TextView) findViewById(R.id.tv_street_name);
        tv_village = (TextView) findViewById(R.id.tv_village);
        tv_district = (TextView) findViewById(R.id.tv_district);
        tv_zip_code = (TextView) findViewById(R.id.tv_zip_code);
        tv_city = (TextView) findViewById(R.id.tv_city);
        tv_province = (TextView) findViewById(R.id.tv_province);
        tv_region = (TextView) findViewById(R.id.tv_region);
        tv_country = (TextView) findViewById(R.id.tv_country);
        tv_address = (TextView) findViewById(R.id.tv_address);
        tv_attachment = (TextView) findViewById(R.id.tv_attachment);

        btn_project_details = (ImageView) findViewById(R.id.btn_project_details);
        btn_model_house = (ImageView) findViewById(R.id.btn_model_house);
        btn_comparatives= (ImageView) findViewById(R.id.btn_comparatives);
        btn_cost_approach = (ImageView) findViewById(R.id.btn_cost_approach);
        btn_summary_val = (ImageView) findViewById(R.id.btn_summary_val);
        btn_note_remarks= (ImageView) findViewById(R.id.btn_note_remarks);

        btn_report_update_cc = (Button) findViewById(R.id.btn_report_update_cc);
        btn_report_view_report = (Button) findViewById(R.id.btn_report_view_report);
        btn_report_pdf = (ImageView) findViewById(R.id.btn_report_pdf);
        btn_select_pdf = (ImageView) findViewById(R.id.btn_select_pdf);
//app details
        btn_update_app_details = (Button) findViewById(R.id.btn_update_app_details);
        btn_update_app_details.setOnClickListener(this);
        tv_address_hide = (TextView) findViewById(R.id.tv_address_hide);
        tv_app_request_remarks = (TextView) findViewById(R.id.tv_app_request_remarks);
        tv_app_kind_of_appraisal = (TextView) findViewById(R.id.tv_app_kind_of_appraisal);
        tv_app_branch_code = (TextView) findViewById(R.id.tv_app_branch_code);


        btn_project_details.setOnClickListener(this);
        btn_model_house.setOnClickListener(this);
        btn_comparatives.setOnClickListener(this);
        btn_cost_approach.setOnClickListener(this);
        btn_summary_val.setOnClickListener(this);
        btn_note_remarks.setOnClickListener(this);

        btn_report_update_cc.setOnClickListener(this);
        btn_report_view_report.setOnClickListener(this);
        btn_report_pdf.setOnClickListener(this);
        btn_select_pdf.setOnClickListener(this);
        tv_attachment.setOnClickListener(this);


        //case center attachments
        btn_mysql_attachments = (ImageView) findViewById(R.id.btn_mysql_attachments);
        btn_mysql_attachments.setOnClickListener(this);

        //address
        btn_update_address = (Button) findViewById(R.id.btn_update_address);
        btn_update_address.setOnClickListener(this);


        tv_rework_reason_header = (TextView) findViewById(R.id.tv_rework_reason_header);
        tv_rework_reason = (TextView) findViewById(R.id.tv_rework_reason);

        btn_manual_delete = (Button) findViewById(R.id.btn_manual_delete);
        btn_manual_delete.setOnClickListener(this);

        record_id = gs.record_id;
        args[0] = record_id;
        uid = db2.getRecord("app_main_id", where, args, "tbl_report_accepted_jobs").get(0);

        fill_details();
        //uploaded_attachments();



    }
    public void fill_details() {

        List<Report_Accepted_Jobs> report_accepted_jobs = db
                .getReport_Accepted_Jobs(record_id);
        if (!report_accepted_jobs.isEmpty()) {
            for (Report_Accepted_Jobs im : report_accepted_jobs) {
                requested_month = im.getdr_month();
                requested_day = im.getdr_day();
                requested_year = im.getdr_year();
                classification = im.getclassification();
                account_fname = im.getfname();
                account_mname = im.getmname();
                account_lname = im.getlname();
                requesting_party = im.getrequesting_party();
                requestor = im.getrequestor();
                control_no = im.getcontrol_no();
                appraisal_type = im.getappraisal_type();
                nature_appraisal = im.getnature_appraisal();
                ins_month1 = im.getins_date1_month();
                ins_day1 = im.getins_date1_day();
                ins_year1 = im.getins_date1_year();
                ins_month2 = im.getins_date2_month();
                ins_day2 = im.getins_date2_day();
                ins_year2 = im.getins_date2_year();
                com_month = im.getcom_month();
                com_day = im.getcom_day();
                com_year = im.getcom_year();
                tct_no = im.gettct_no();
                unit_no = im.getunit_no();
                bldg_name = im.getbuilding_name();
                lot_no = im.getlot_no();
                block_no = im.getblock_no();
                street_no = im.getstreet_no();
                street_name = im.getstreet_name();
                village = im.getvillage();
                district = im.getdistrict();
                zip_code = im.getzip_code();
                city = im.getcity();
                province = im.getprovince();
                region = im.getregion();
                country = im.getcountry();
                counter = im.getcounter();
                app_request_remarks = im.getapp_request_remarks();
                app_kind_of_appraisal = im.getkind_of_appraisal();
                app_branch_code = im.getapp_branch_code();
                rework_reason = im.getrework_reason();
                args[0]=record_id;
                account_is_company=db2.getRecord("app_account_is_company",where,args,"tbl_report_accepted_jobs").get(0);
                account_company_name=db2.getRecord("app_account_company_name",where,args,"tbl_report_accepted_jobs").get(0);
            }

            args[0]=record_id;
            if(db2.getRecord("application_status",where,args,"tbl_report_accepted_jobs").get(0).contentEquals("for_rework")){
                tv_rework_reason_header.setVisibility(View.VISIBLE);
                tv_rework_reason.setVisibility(View.VISIBLE);
                tv_rework_reason.setText(rework_reason);
            }
            List<Report_Accepted_Jobs_Contacts> report_Accepted_Jobs_Contacts = db
                    .getReport_Accepted_Jobs_Contacts(record_id);
            if (!report_Accepted_Jobs_Contacts.isEmpty()) {
                for (Report_Accepted_Jobs_Contacts im : report_Accepted_Jobs_Contacts) {
                    LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
                            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View addView = layoutInflater.inflate(
                            R.layout.request_jobs_info_contact_layout, null);
                    final TextView tv_contact_person = (TextView) addView
                            .findViewById(R.id.tv_contact_person);
                    final TextView tv_contact_prefix = (TextView) addView
                            .findViewById(R.id.tv_contact_prefix);
                    final TextView tv_contact_mobile = (TextView) addView
                            .findViewById(R.id.tv_contact_mobile);
                    final TextView tv_contact_landline = (TextView) addView
                            .findViewById(R.id.tv_contact_landline);

                    tv_contact_person.setText(im.getcontact_person());
                    tv_contact_prefix.setText(im.getcontact_mobile_no_prefix());
                    tv_contact_mobile.setText(im.getcontact_mobile_no());
                    tv_contact_landline.setText(im.getcontact_landline());
                    ll_contact.addView(addView);
                }
            }
            List<Report_filename> rf = db.getReport_filename(record_id);
            if (!rf.isEmpty()) {
                for (Report_filename im : rf) {
                    tv_attachment.setText(im.getfilename());
                }
            }
            tv_requested_month.setText(requested_month + "/");
            tv_requested_day.setText(requested_day + "/");
            tv_requested_year.setText(requested_year);
            tv_classification.setText(classification);
            tv_account_name.setText(account_fname + " " + account_mname + " "
                    + account_lname);
            tv_company_name.setText(account_company_name);
            if(account_is_company.contentEquals("true")){
                actionBar.setTitle(account_company_name);
            }else{
                actionBar.setTitle(account_fname + " " + account_mname + " " + account_lname);
            }
            tv_requesting_party.setText(requesting_party);
            tv_requestor.setText(requestor);
            tv_control_no.setText(control_no);
            for (int x = 0; x < gs.appraisal_type_value.length; x++) {
                if (appraisal_type.equals(gs.appraisal_type_value[x])) {
                    output_appraisal_type = gs.appraisal_output[x];
                }
            }
            tv_appraisal_type.setText(output_appraisal_type);
            tv_nature_appraisal.setText(nature_appraisal);
            tv_ins_month1.setText(ins_month1 + "/");
            tv_ins_day1.setText(ins_day1 + "/");
            tv_ins_year1.setText(ins_year1);
            tv_ins_month2.setText(ins_month2 + "/");
            tv_ins_day2.setText(ins_day2 + "/");
            tv_ins_year2.setText(ins_year2);
            tv_com_month.setText(com_month + "/");
            tv_com_day.setText(com_day + "/");
            tv_com_year.setText(com_year);

            tv_tct_no.setText(tct_no);
            tv_unit_no.setText(unit_no);
            tv_bldg_name.setText(bldg_name);
            tv_street_no.setText(street_no);
            tv_street_name.setText(street_name);
            tv_village.setText(village);
            tv_district.setText(district);
            tv_zip_code.setText(zip_code);
            tv_city.setText(city);
            tv_province.setText(province);
            tv_region.setText(region);
            tv_country.setText(country);
            tv_app_request_remarks.setText(app_request_remarks);
            tv_app_kind_of_appraisal.setText(app_kind_of_appraisal);
            tv_app_branch_code.setText(app_branch_code);
            tv_address_hide.setText(street_name + " " + city + " " + region + " " + country);
            if (!street_name.equals("")) {
                output_street_name = street_name + ", ";
            } else {
                output_street_name = street_name;
            }
            if (!village.equals("")) {
                output_village = village + ", ";
            } else {
                output_village = village;
            }
            if (!city.equals("")) {
                output_city = city + ", ";
            } else {
                output_city = city;
            }
            if (!province.equals("")) {
                output_province = province + ", ";
            } else {
                output_province = province;
            }
            tv_address.setText(unit_no + " " + bldg_name + " " + lot_no + " " + block_no
                    + " " + output_street_name + output_village + district
                    + " " + output_city + region + " " + output_province
                    + country + " " + zip_code);
        }
    }
    public void uploaded_attachments() {
        // set value
        List<Required_Attachments> required_attachments = db
                .getAllRequired_Attachments_byid(String.valueOf(appraisal_type));
        if (!required_attachments.isEmpty()) {
            for (final Required_Attachments ra : required_attachments) {
                LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                final View addView = layoutInflater.inflate(
                        R.layout.report_uploaded_attachments_dynamic, null);
                final TextView tv_attachment = (TextView) addView
                        .findViewById(R.id.tv_attachment);
                final ImageView btn_view = (ImageView) addView
                        .findViewById(R.id.btn_view);
                tv_attachment.setText(ra.getattachment());
                btn_view.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        uploaded_attachment = uid + "_"
                                + ra.getattachment() + "_" + counter + ".pdf";
                        // directory
                        uploaded_file = new File(myDirectory,
                                uploaded_attachment);
                        uploaded_attachment = uploaded_attachment.replaceAll(
                                " ", "%20");

                        url_webby = gs.pdf_loc_url + uploaded_attachment;
                        view_pdf(uploaded_file);
                    }
                });
                container_attachments.addView(addView);

            }

        }

    }
    public void view_pdf(File file) {
        if (file.exists()) {
            Uri path = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(getApplicationContext(),
                        "No Application Available to View PDF",
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            NetworkUtil.getConnectivityStatusString(this);
            if (!NetworkUtil.status.equals("Network not available")) {
                new Attachment_validation().execute();
            } else {
                Toast.makeText(getApplicationContext(),
                        "Network not available", Toast.LENGTH_SHORT).show();
            }

        }
    }
    private class Attachment_validation extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pDialog = new ProgressDialog(Spv.this);
            pDialog.setMessage("Checking attachment..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            NetworkUtil.getConnectivityStatusString(Spv.this);
            if (!NetworkUtil.status.equals("Network not available")) {
                google_response = gds.google_ping();
                if (google_response.equals("200")) {
                    try {
                        URL obj = new URL(url_webby);
                        HttpsURLConnection con;
                        HttpURLConnection con2;
                        if (Global.type.contentEquals("tls")) {
                            con = tlscon.setUpHttpsConnection("" + obj);
                            con.setRequestMethod("GET");
                            con.setRequestProperty("User-Agent", "Mozilla/5.0");
                            responseCode = con.getResponseCode();
                        } else {
                            con2 = (HttpURLConnection) obj.openConnection();
                            con2.setRequestMethod("GET");
                            con2.setRequestProperty("User-Agent", "Mozilla/5.0");
                            responseCode = con2.getResponseCode();
                        }
                        network_status = true;
                        Log.e("", String.valueOf(responseCode));
                    } catch (MalformedURLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (ProtocolException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            } else {
                network_status = false;

            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            Log.e("", url_webby);
            if (network_status == false) {
                Toast.makeText(getApplicationContext(), "No network Available",
                        Toast.LENGTH_SHORT).show();
            } else if (network_status == true) {
                if (responseCode == 200) {
                    view_download();
                } else if (responseCode == 404) {
                    Toast.makeText(getApplicationContext(),
                            "Attachment doesn't exist", Toast.LENGTH_SHORT)
                            .show();
                }
            }
            pDialog.dismiss();

        }
    }
    private void view_download() {
        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Action");
        builder.setItems(commandArray, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int index) {
                if (index == 0) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
                            .parse(url_webby));

                    startActivity(browserIntent);
                    dialog.dismiss();
                } else if (index == 1) {
                    new DownloadFileFromURL().execute(url_webby);
                    dialog.dismiss();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
    /**
     * Background Async Task to download file
     */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                // Output stream to write file
                OutputStream output = new FileOutputStream(
                        uploaded_file.toString());

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            removeDialog(progress_bar_type);
            Toast.makeText(getApplicationContext(), "Attachment Downloaded",
                    Toast.LENGTH_LONG).show();
            view_pdf(uploaded_file);

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // TODO Auto-generated method stub
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        menu.findItem(R.id.map).setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {

            case R.id.map:
                view_map();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void view_map() {
        String add = tv_address_hide.getText().toString();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Spv.this);
        alertDialog.setTitle("Address");
        alertDialog.setMessage("Edit Address");

        final EditText input = new EditText(Spv.this);
        input.setText(add);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);
        alertDialog.setIcon(R.drawable.map_icon);

        alertDialog.setPositiveButton("Search",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String address = input.getText().toString();
                        Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + address);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        startActivity(mapIntent);

                    }
                });

        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }
    public void warning_dialog() {
        final Dialog dialog2 = new Dialog(context);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.setContentView(R.layout.warning_dialog);

        // set the custom dialog components - text, image and button
        TextView text2 = (TextView) dialog2.findViewById(R.id.tv_question);
        text2.setText("Please attach a file/pdf before you can submit the report");
        Button btn_ok = (Button) dialog2.findViewById(R.id.btn_ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });

        dialog2.show();
    }
    public void no_attachment() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.yes_no_dialog);

        Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btn_no = (Button) dialog.findViewById(R.id.btn_no);
        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_question);
        dialog.show();

        text.setText("Are you sure you want to submit ?");

        // if button is clicked, close the custom dialog
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                withAttachment = false;
                //new SendData().execute();
                //todo_new
                if (new Fields_Checker().isModuleComplete(record_id, context)) {
                    new SendData().execute();
                } else {
                    Toast.makeText(getApplicationContext(), "Module fields incomplete", Toast.LENGTH_LONG).show();
                }
                dialog.dismiss();
            }
        });
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
    public void check_attachment() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.yes_no_dialog);

        Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btn_no = (Button) dialog.findViewById(R.id.btn_no);
        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_question);
        text.setText("Are you sure you want to submit ?");
        //aug 17
        args[0] = record_id;
        if(db2.getRecord("application_status",where,args,"tbl_report_accepted_jobs").get(0).contentEquals("for_rework")){
            freshData = false;
        }else{
            freshData = true;
        }

        //check if with or without attachment
        if (((!tv_attachment.getText().toString().equals("")) && (freshData == false)) ||
                ((!tv_attachment.getText().toString().equals("")) && (freshData == true))) {
            dialog.show();
            withAttachment = true;
        } else if ((tv_attachment.getText().toString().equals("")) && (freshData == true)) {
            text.setText("Do you want to submit report without file/pdf attachment?");
            withAttachment = false;
            fileUploaded=true;
            dialog.dismiss();
            warning_dialog();
        } else if ((tv_attachment.getText().toString().equals("")) && (freshData == false)) {
            text.setText("Do you want to submit the report?");
            withAttachment = false;
            fileUploaded=true;
            dialog.show();
        }

        // if button is clicked, close the custom dialog
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((withAttachment == true) && (freshData == true)) ||
                        ((withAttachment == true) && (freshData == false)) ||
                        ((withAttachment == false) && (freshData == false))) {
                    //new SendData().execute();
                    //todo_new
                    if (new Fields_Checker().isModuleComplete(record_id, context)) {
                        new SendData().execute();
//                        Toast.makeText(getApplicationContext(), "Module fields complete", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Module fields incomplete", Toast.LENGTH_LONG).show();
                    }
                    dialog.dismiss();
                } else if ((withAttachment == false) && (freshData == true)) {
                    no_attachment();
                    dialog.dismiss();
                }
            }
        });
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
    public void custom_dialog_address() {
        myDialog = new Dialog(Spv.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.address_dialog);
        myDialog.setCancelable(true);

        android.view.WindowManager.LayoutParams params = myDialog.getWindow()
                .getAttributes();
        params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        params.width = LinearLayout.LayoutParams.FILL_PARENT;
        myDialog.getWindow().setAttributes(
                (android.view.WindowManager.LayoutParams) params);
        et_unit_no = (EditText) myDialog.findViewById(R.id.et_unit_no);
        et_building_name = (EditText) myDialog.findViewById(R.id.et_building_name);
        et_lot_no = (EditText) myDialog.findViewById(R.id.et_lot_no);
        et_block_no = (EditText) myDialog.findViewById(R.id.et_block_no);
        et_street_no = (EditText) myDialog.findViewById(R.id.et_street_no);
        et_street_name = (EditText) myDialog.findViewById(R.id.et_street_name);
        et_village = (EditText) myDialog.findViewById(R.id.et_village);
        et_district = (EditText) myDialog.findViewById(R.id.et_district);
        et_zip_code = (EditText) myDialog.findViewById(R.id.et_zip_code);
        et_city = (EditText) myDialog.findViewById(R.id.et_city);
        et_province = (EditText) myDialog.findViewById(R.id.et_province);
        et_region = (EditText) myDialog.findViewById(R.id.et_region);
        et_country = (EditText) myDialog.findViewById(R.id.et_country);

        btn_save_address = (Button) myDialog.findViewById(R.id.btn_save_address);
        btn_save_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method
                Report_Accepted_Jobs li = new Report_Accepted_Jobs();
                li.setunit_no(et_unit_no.getText().toString());
                li.setbuilding_name(et_building_name.getText().toString());
                li.setlot_no(et_lot_no.getText().toString());
                li.setblock_no(et_block_no.getText().toString());
                li.setstreet_no(et_street_no.getText().toString());
                li.setstreet_name(et_street_name.getText().toString());
                li.setvillage(et_village.getText().toString());
                li.setdistrict(et_district.getText().toString());
                li.setzip_code(et_zip_code.getText().toString());
                li.setcity(et_city.getText().toString());
                li.setprovince(et_province.getText().toString());
                li.setregion(et_region.getText().toString());
                li.setcountry(et_country.getText().toString());

                db.updateCollateral_Address(li, record_id);
                db.close();
                Toast.makeText(getApplicationContext(), "Saved",
                        Toast.LENGTH_SHORT).show();
                myDialog.dismiss();
                Intent intent = getIntent();
                intent.putExtra("keyopen", "0");
                finish();
                startActivity(intent);
            }
        });

        List<Report_Accepted_Jobs> li = db.getReport_Accepted_Jobs(String.valueOf(record_id));
        if (!li.isEmpty()) {
            for (Report_Accepted_Jobs im : li) {
                et_unit_no.setText(im.getunit_no());
                et_building_name.setText(im.getbuilding_name());
                et_lot_no.setText(im.getlot_no());
                et_block_no.setText(im.getblock_no());
                et_street_no.setText(im.getstreet_no());
                et_street_name.setText(im.getstreet_name());
                et_village.setText(im.getvillage());
                et_district.setText(im.getdistrict());
                et_zip_code.setText(im.getzip_code());
                et_city.setText(im.getcity());
                et_province.setText(im.getprovince());
                et_region.setText(im.getregion());
                et_country.setText(im.getcountry());
            }
        }

        myDialog.show();
    }
    public void custom_dialog_app_details() {
        myDialog = new Dialog(Spv.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.app_details_dialog);
        myDialog.setCancelable(true);

        android.view.WindowManager.LayoutParams params = myDialog.getWindow()
                .getAttributes();
        params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        params.width = LinearLayout.LayoutParams.FILL_PARENT;
        myDialog.getWindow().setAttributes(
                (android.view.WindowManager.LayoutParams) params);

        report_date_requested = (EditText) myDialog.findViewById(R.id.report_date_requested);
        et_first_name = (EditText) myDialog.findViewById(R.id.et_first_name);
        et_middle_name = (EditText) myDialog.findViewById(R.id.et_middle_name);
        et_last_name = (EditText) myDialog.findViewById(R.id.et_last_name);
        et_requesting_party = (AutoCompleteTextView) myDialog.findViewById(R.id.et_requesting_party);
        et_requestor = (EditText) myDialog.findViewById(R.id.et_requestor);
        app_account_company_name = (EditText) myDialog.findViewById(R.id.app_account_company_name);
        app_account_is_company = (CheckBox) myDialog.findViewById(R.id.app_account_is_company);
        spinner_purpose_appraisal = (Spinner) myDialog.findViewById(R.id.app_purpose_appraisal);
        et_app_kind_of_appraisal = (EditText) myDialog.findViewById(R.id.app_kind_of_appraisal);

        gds.autocomplete(et_requesting_party, getResources().getStringArray(R.array.requesting_party), this);
        if(app_account_is_company.isChecked()){
            app_account_company_name.setEnabled(true);
            et_first_name.setText("-");
            et_middle_name.setText("-");
            et_last_name.setText("-");

            et_first_name.setEnabled(false);
            et_middle_name.setEnabled(false);
            et_last_name.setEnabled(false);
        }else{
            app_account_company_name.setEnabled(false);
            app_account_company_name.setText("-");
            et_first_name.setEnabled(true);
            et_middle_name.setEnabled(true);
            et_last_name.setEnabled(true);
        }
        app_account_is_company.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    app_account_company_name.setEnabled(true);
                    et_first_name.setText("-");
                    et_middle_name.setText("-");
                    et_last_name.setText("-");
                    et_first_name.setEnabled(false);
                    et_middle_name.setEnabled(false);
                    et_last_name.setEnabled(false);
                } else {
                    app_account_company_name.setEnabled(false);
                    app_account_company_name.setText("-");
                    et_first_name.setEnabled(true);
                    et_middle_name.setEnabled(true);
                    et_last_name.setEnabled(true);
                }
            }
        });


        btn_save_app_details = (Button) myDialog.findViewById(R.id.btn_right);
        btn_cancel_app_details = (Button) myDialog.findViewById(R.id.btn_left);
        btn_save_app_details.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method

                boolean fieldsOK,fieldsOK2;
                gds.fill_in_error(new EditText[]{
                        et_first_name,et_last_name});
                fieldsOK = gds.validate(new EditText[]{
                        et_first_name,et_last_name});

                if(app_account_is_company.isChecked()){
                    gds.fill_in_error(new EditText[]{
                            app_account_company_name});
                    fieldsOK2 = gds.validate(new EditText[]{
                            app_account_company_name});
                }else{
                    fieldsOK2=true;
                }

                if(fieldsOK&&fieldsOK2) {
                    Report_Accepted_Jobs mv = new Report_Accepted_Jobs();


                    mv.setfname(et_first_name.getText().toString());
                    mv.setmname(et_middle_name.getText().toString());
                    mv.setlname(et_last_name.getText().toString());
                    mv.setrequesting_party(et_requesting_party.getText().toString());
                    mv.setrequestor(et_requestor.getText().toString());

                    db.updateApp_Details(mv, record_id);
                    field.clear();
                    field.add("app_account_is_company");
                    field.add("app_account_company_name");
                    field.add("nature_appraisal");
                    field.add("kind_of_appraisal");
                    value.clear();
                    value.add(gds.cbChecker(app_account_is_company));
                    value.add(app_account_company_name.getText().toString());
                    value.add(spinner_purpose_appraisal.getSelectedItem().toString());
                    value.add(et_app_kind_of_appraisal.getText().toString());
                    db2.updateRecord(value,field,"record_id = ?",new String[] { record_id },"tbl_report_accepted_jobs");
                    db.close();
                    Toast.makeText(getApplicationContext(), "Saved",
                            Toast.LENGTH_SHORT).show();
                    myDialog.dismiss();
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(),
                            "Please fill up required fields", Toast.LENGTH_SHORT).show();

                }

            }
        });

        btn_cancel_app_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method
                myDialog.dismiss();
            }
        });

        List<Report_Accepted_Jobs> report_accepted_jobs = db
                .getReport_Accepted_Jobs(record_id);
        if (!report_accepted_jobs.isEmpty()) {
            for (Report_Accepted_Jobs im : report_accepted_jobs) {
                et_first_name.setText(im.getfname());
                et_middle_name.setText(im.getmname());
                et_last_name.setText(im.getlname());
                et_requesting_party.setText(im.getrequesting_party());
                et_requestor.setText(im.getrequestor());
                gds.cbDisplay(app_account_is_company, db2.getRecord("app_account_is_company", where, args, "tbl_report_accepted_jobs").get(0));
                app_account_company_name.setText(db2.getRecord("app_account_company_name", where, args, "tbl_report_accepted_jobs").get(0));
                //valor2
                spinner_purpose_appraisal.setSelection(gds.spinnervalue(spinner_purpose_appraisal, db2.getRecord("nature_appraisal", where, args, "tbl_report_accepted_jobs").get(0)));
                et_app_kind_of_appraisal.setText(db2.getRecord("kind_of_appraisal", where, args, "tbl_report_accepted_jobs").get(0));

                // set current date into datepicker
                if ((!im.getdr_year().equals("")) && (!im.getdr_month().equals("")) && (!im.getdr_day().equals(""))) {
                    report_date_requested.setText(im.getdr_month() + "/" + im.getdr_day() + "/" + im.getdr_year());
                } else {
                    report_date_requested.setText("");
                }
            }
        }

        myDialog.show();
    }

    private List<HashMap<String, Object>> getPriorityList() {
        DatabaseHandler db = new DatabaseHandler(
                context.getApplicationContext());
        List<HashMap<String, Object>> priorityList = new ArrayList<HashMap<String, Object>>();

        List<Images> images = db.getAllImages(uid);//added account lname for specific search result like
        if (!images.isEmpty()) {
            for (Images im : images) {
                filename = im.getfilename();
                String filenameArray[] = filename.split("\\.");
                String extension = filenameArray[filenameArray.length - 1];
                if (extension.equals("pdf")) {
                    HashMap<String, Object> map1 = new HashMap<String, Object>();
                    map1.put("txt_pdf_list", im.getfilename());
                    map1.put("mono", R.drawable.attach_item);
                    priorityList.add(map1);
                }

            }

        }
        return priorityList;
    }

    public void checkFileSize() {
        myDialog = new Dialog(Spv.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.warning_dialog);
        myDialog.setCancelable(true);
        final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
        final Button btn_ok = (Button) myDialog.findViewById(R.id.btn_ok);
        tv_question.setText(R.string.file_size_too_large);
        //format double to 2 decimal place
        DecimalFormat df = new DecimalFormat("#.00");
        //get fileSize and convert to KB and MB
        File filenew = new File(myDirectory + "/" + item);//get path concat with slash and file name
        int file_size_kb = Integer.parseInt(String.valueOf(filenew.length() / 1024));
        double file_size_mb = file_size_kb / 1024.00;
        //custom toast
        Toast toast = Toast.makeText(context, item +
                "\nfile size in KB: " + file_size_kb + "KB" +
                "\nfile size in MB: " + df.format(file_size_mb) + "MB", Toast.LENGTH_LONG);
        View view = toast.getView();
        view.setBackgroundResource(R.color.toast_color);
        toast.show();


        if (file_size_kb > 6144) {//restrict the file size up to 6.0MB only
            //clear textView to restict uploading
            Toast.makeText(getApplicationContext(), "file size is too large",
                    Toast.LENGTH_SHORT).show();
            tv_attachment.setText("");
            myDialog.show();
        }

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }
    public void open_customdialog() {
        // dialog
        myDialog = new Dialog(context);
        myDialog.setTitle("Select Attachment");
        myDialog.setContentView(R.layout.request_pdf_list);
        // myDialog.setTitle("My Dialog");

        dlg_priority_lvw = (ListView) myDialog
                .findViewById(R.id.dlg_priority_lvw);
        // ListView
        SimpleAdapter adapter = new SimpleAdapter(context, getPriorityList(),
                R.layout.request_pdf_list_layout, new String[]{
                "txt_pdf_list", "mono"}, new int[]{
                R.id.txt_pdf_list, R.id.mono});
        dlg_priority_lvw.setAdapter(adapter);

        // ListView
        dlg_priority_lvw
                .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1,
                                            int arg2, long arg3) {

                        item = ((TextView) arg1.findViewById(R.id.txt_pdf_list))
                                .getText().toString();
                        attachment_selected = item;
                        tv_attachment.setText(attachment_selected);
                        // update

                        Report_filename rf = new Report_filename();
                        rf.setuid(uid);
                        rf.setfile("Property Pictures");
                        rf.setfilename(attachment_selected);
                        rf.setappraisal_type(appraisal_type + "_" + counter);
                        rf.setcandidate_done("true");
                        db.updateReport_filename(rf, record_id);
                        db.close();
                        myDialog.dismiss();
                        checkFileSize();
                    }
                });
        dlg_priority_lvw.setLongClickable(true);
        dlg_priority_lvw
                .setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> arg0,
                                                   View arg1, final int arg2, long arg3) {
                        item = ((TextView) arg1.findViewById(R.id.txt_pdf_list))
                                .getText().toString();
                        Toast.makeText(getApplicationContext(), item,
                                Toast.LENGTH_SHORT).show();

                        File file = new File(myDirectory, item);

                        if (file.exists()) {
                            Uri path = Uri.fromFile(file);
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(path, "application/pdf");
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                            try {
                                startActivity(intent);
                            } catch (ActivityNotFoundException e) {
                                Toast.makeText(getApplicationContext(),
                                        "No Application Available to View PDF",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }

                        return true;

                    }
                });
        myDialog.show();
    }

    public void open_pdf() {
        File file = new File(myDirectory, tv_attachment.getText().toString());

        if (file.exists()) {
            Uri path = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(getApplicationContext(),
                        "No Application Available to View PDF",
                        Toast.LENGTH_SHORT).show();
            }
        }

    }
    public void manual_delete_all() {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.delete_dialog);

        Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btn_no = (Button) dialog.findViewById(R.id.btn_no);
        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_question);
        dialog.show();

        text.setText("Do you want to delete this record?");

        // if button is clicked, close the custom dialog
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gds.deleteAllRecord(record_id,db2);

                dialog.dismiss();
                finish();
            }
        });
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
    @Override
    public void onClick(View v) {

    }
}
