package com.gds.appraisalmaybank.database_new;

/**
 * Created by GDS LINK ASIA on 1/10/2017.
 */

public class DatabaseTableObject {

    String table_name;
    String[] fields;
    String query;

    public DatabaseTableObject(String table_name, String[] fields) {

        this.table_name = table_name;
        this.fields = fields;

    }

    public DatabaseTableObject() {

    }

    public String createQuery(){

        String create_fields = "";
        for(int i = 0; i < fields.length; i++){


            if(fields[i].contentEquals("id"))
                create_fields += fields[i] + " INTEGER,";
            else
                create_fields += fields[i] + " TEXT,";

        }

        query = "CREATE TABLE IF NOT EXISTS " + table_name + "(" + create_fields + "PRIMARY KEY("+ fields[0] + "));";

        return query;
    }

    //cost approach sub
    public String create_land_improvements_imp_valuation_details(){
        query = "CREATE TABLE IF NOT EXISTS land_improvements_imp_valuation_details(imp_valuation_details_id INTEGER PRIMARY KEY, " +
                "record_id TEXT, imp_valuation_id TEXT, report_imp_value_kind TEXT, report_imp_value_area TEXT, " +
                "report_imp_value_cost_per_sqm TEXT, report_imp_value_reproduction_cost TEXT);";
        return query;
    }

    //cost approach added 5 new fields on 012517 by mark (valrep_landimp_imp_value_phys_cur_unit_value TEXT, valrep_landimp_imp_value_phys_cur_amt TEXT, valrep_landimp_imp_value_func_cur_unit_value TEXT, valrep_landimp_imp_value_func_cur_amt TEXT, valrep_landimp_imp_value_less_cur TEXT)
    public String create_land_improvements_imp_valuation(){
        query = "CREATE TABLE IF NOT EXISTS land_improvements_imp_valuation(imp_valuation_id INTEGER PRIMARY KEY, " +
                "record_id TEXT, report_imp_value_description TEXT, report_imp_value_total_area TEXT, " +
                "report_imp_value_total_reproduction_cost TEXT, report_imp_value_depreciation TEXT, " +
                "report_imp_value_less_reproduction TEXT, report_imp_value_depreciated_value TEXT, " +
                "valrep_landimp_imp_value_phys_cur_unit_value TEXT, valrep_landimp_imp_value_phys_cur_amt TEXT, " +
                "valrep_landimp_imp_value_func_cur_unit_value TEXT, valrep_landimp_imp_value_func_cur_amt TEXT, " +
                "valrep_landimp_imp_value_less_cur TEXT);";
        return query;
    }

    public String create_land_improvements_imp_details_features(){
        query = "CREATE TABLE IF NOT EXISTS land_improvements_imp_details_features(imp_details_features_id INTEGER PRIMARY KEY, " +
                "record_id TEXT, imp_details_id TEXT, report_impsummary1_building_desc TEXT, " +
                "report_desc_features_area TEXT,report_desc_features_area_desc TEXT);";
        return query;
    }

    public String create_land_improvements_imp_details(){
        query = "CREATE TABLE IF NOT EXISTS land_improvements_imp_details ( imp_details_id INTEGER PRIMARY KEY, " +
                "record_id TEXT, report_impsummary1_no_of_floors TEXT, report_impsummary1_building_desc TEXT, " +
                "report_impsummary1_erected_on_lot TEXT, report_impsummary1_fa TEXT, " +
                "report_impsummary1_fa_per_td TEXT, report_impsummary1_actual_utilization TEXT, " +
                "report_impsummary1_usage_declaration TEXT, report_impsummary1_owner TEXT, " +
                "report_impsummary1_socialized_housing TEXT, report_desc_property_type TEXT, " +
                "report_desc_foundation TEXT, report_desc_columns_posts TEXT, report_desc_beams TEXT, " +
                "report_desc_exterior_walls TEXT, report_desc_interior_walls TEXT, " +
                "report_desc_imp_flooring TEXT, report_desc_doors TEXT, report_desc_imp_windows TEXT, " +
                "report_desc_ceiling TEXT, report_desc_imp_roofing TEXT, report_desc_trusses TEXT, " +
                "report_desc_economic_life TEXT, report_desc_effective_age TEXT, report_desc_imp_remain_life TEXT, " +
                "report_desc_occupants TEXT, report_desc_owned_or_leased TEXT, report_desc_imp_floor_area TEXT, " +
                "report_desc_confirmed_thru TEXT, report_desc_observed_condition TEXT, " +
                "report_ownership_of_property TEXT, report_impsummary1_no_of_bedrooms TEXT, " +
                "valrep_landimp_impsummary1_no_of_tb TEXT, valrep_landimp_desc_stairs TEXT);";
        return query;
    }

    public String create_townhouse_imp_details_features(){
        query = "CREATE TABLE IF NOT EXISTS townhouse_imp_details_features ( imp_details_features_id INTEGER PRIMARY KEY, " +
                "record_id TEXT, imp_details_id TEXT, report_desc_features TEXT, " +
                "report_desc_features_area TEXT, report_desc_features_area_desc TEXT);";
        return query;
    }

    public String create_townhouse_imp_details(){
        query = "CREATE TABLE IF NOT EXISTS townhouse_imp_details ( imp_details_id INTEGER PRIMARY KEY, " +
                "record_id TEXT, report_impsummary1_no_of_floors TEXT, report_impsummary1_building_desc TEXT, " +
                "report_impsummary1_erected_on_lot TEXT, report_impsummary1_fa TEXT, " +
                "report_impsummary1_fa_per_td TEXT, report_impsummary1_actual_utilization TEXT, " +
                "report_impsummary1_usage_declaration TEXT, report_impsummary1_owner TEXT, " +
                "report_impsummary1_socialized_housing TEXT, report_desc_property_type TEXT, " +
                "report_desc_foundation TEXT, report_desc_columns_posts TEXT, report_desc_beams TEXT, " +
                "report_desc_exterior_walls TEXT, report_desc_interior_walls TEXT, " +
                "report_desc_imp_flooring TEXT, report_desc_doors TEXT, report_desc_imp_windows TEXT, " +
                "report_desc_ceiling TEXT, report_desc_imp_roofing TEXT, report_desc_trusses TEXT, " +
                "report_desc_economic_life TEXT, report_desc_effective_age TEXT, report_desc_imp_remain_life TEXT, " +
                "report_desc_occupants TEXT, report_desc_owned_or_leased TEXT, report_desc_imp_floor_area TEXT, " +
                "report_desc_confirmed_thru TEXT, report_desc_observed_condition TEXT, report_ownership_of_property TEXT, " +
                "report_impsummary_no_of_bedroom TEXT, valrep_townhouse_impsummary_no_of_tb TEXT);";
        return query;
    }
}
