package com.gds.appraisalmaybank.database_new;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by GDS LINK ASIA on 1/11/2017.
 */

public class DatabaseHandler_New extends SQLiteOpenHelper {

    Context context;
    private static final String DATABASE_NAME = "GDSAppraisalDb";
    private static final int DATABASE_VERSION = 2;

    public DatabaseHandler_New(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        Log.i("DatabaseHandler:", "constructed (created / opened)");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        Log.i("DatabaseHandler:", "Creating Tables ...");

        //add tables
        DatabaseTableObject condo = new DatabaseTableObject(Tables.condo.table_name, Tables.condo.fields);
        DatabaseTableObject condo_main_prev_appraisal = new DatabaseTableObject(Tables.condo_main_prev_appraisal.table_name, Tables.condo_main_prev_appraisal.fields);
        DatabaseTableObject condo_prev_appraisal = new DatabaseTableObject(Tables.condo_prev_appraisal.table_name, Tables.condo_prev_appraisal.fields);
        DatabaseTableObject condo_title_details = new DatabaseTableObject(Tables.condo_title_details.table_name, Tables.condo_title_details.fields);
        DatabaseTableObject condo_unit_details = new DatabaseTableObject(Tables.condo_unit_details.table_name, Tables.condo_unit_details.fields);
        DatabaseTableObject condo_unit_valuation = new DatabaseTableObject(Tables.condo_unit_valuation.table_name, Tables.condo_unit_valuation.fields);
        DatabaseTableObject construction_ebm = new DatabaseTableObject(Tables.construction_ebm.table_name, Tables.construction_ebm.fields);
        DatabaseTableObject construction_ebm_room_list = new DatabaseTableObject(Tables.construction_ebm_room_list.table_name, Tables.construction_ebm_room_list.fields);
        DatabaseTableObject db_check = new DatabaseTableObject(Tables.db_check.table_name, Tables.db_check.fields);
        DatabaseTableObject land_improvements = new DatabaseTableObject(Tables.land_improvements.table_name, Tables.land_improvements.fields);

        //tables with different id name
        /*DatabaseTableObject land_improvements_imp_details = new DatabaseTableObject(Tables.condo.table_name, Tables.condo.fields);
        DatabaseTableObject land_improvements_imp_details_features = new DatabaseTableObject(Tables.condo.table_name, Tables.condo.fields);
        DatabaseTableObject land_improvements_imp_details = new DatabaseTableObject(Tables.condo.table_name, Tables.condo.fields);
        DatabaseTableObject land_improvements_imp_valuation = new DatabaseTableObject(Tables.condo.table_name, Tables.condo.fields);
        DatabaseTableObject land_improvements_imp_valuation_details = new DatabaseTableObject(Tables.condo.table_name, Tables.condo.fields);*/

        DatabaseTableObject land_improvements_lot_details = new DatabaseTableObject(Tables.land_improvements_lot_details.table_name, Tables.land_improvements_lot_details.fields);
        DatabaseTableObject land_improvements_lot_valuation_details = new DatabaseTableObject(Tables.land_improvements_lot_valuation_details.table_name, Tables.land_improvements_lot_valuation_details.fields);
        DatabaseTableObject land_improvements_main_prev_appraisal = new DatabaseTableObject(Tables.land_improvements_main_prev_appraisal.table_name, Tables.land_improvements_main_prev_appraisal.fields);
        DatabaseTableObject land_improvements_prev_appraisal = new DatabaseTableObject(Tables.land_improvements_prev_appraisal.table_name, Tables.land_improvements_prev_appraisal.fields);
        DatabaseTableObject motor_vehicle = new DatabaseTableObject(Tables.motor_vehicle.table_name, Tables.motor_vehicle.fields);
        DatabaseTableObject motor_vehicle_ownership_history = new DatabaseTableObject(Tables.motor_vehicle_ownership_history.table_name, Tables.motor_vehicle_ownership_history.fields);
        DatabaseTableObject motor_vehicle_ownership_source = new DatabaseTableObject(Tables.motor_vehicle_ownership_source.table_name, Tables.motor_vehicle_ownership_source.fields);
        DatabaseTableObject motor_vehicle_previous_appraisal = new DatabaseTableObject(Tables.motor_vehicle_previous_appraisal.table_name, Tables.motor_vehicle_previous_appraisal.fields);
        DatabaseTableObject ppcr = new DatabaseTableObject(Tables.ppcr.table_name, Tables.ppcr.fields);
        DatabaseTableObject ppcr_report_details = new DatabaseTableObject(Tables.ppcr_report_details.table_name, Tables.ppcr_report_details.fields);
        DatabaseTableObject report_filename = new DatabaseTableObject(Tables.report_filename.table_name, Tables.report_filename.fields);
        DatabaseTableObject submission_logs = new DatabaseTableObject(Tables.submission_logs.table_name, Tables.submission_logs.fields);
        DatabaseTableObject tbl_attachments = new DatabaseTableObject(Tables.tbl_attachments.table_name, Tables.tbl_attachments.fields);
        DatabaseTableObject tbl_cities = new DatabaseTableObject(Tables.tbl_cities.table_name, Tables.tbl_cities.fields);
        DatabaseTableObject tbl_images = new DatabaseTableObject(Tables.tbl_images.table_name, Tables.tbl_images.fields);
        DatabaseTableObject tbl_provinces = new DatabaseTableObject(Tables.tbl_provinces.table_name, Tables.tbl_provinces.fields);
        DatabaseTableObject tbl_regions = new DatabaseTableObject(Tables.tbl_regions.table_name, Tables.tbl_regions.fields);
        DatabaseTableObject tbl_report_accepted_jobs = new DatabaseTableObject(Tables.tbl_report_accepted_jobs.table_name, Tables.tbl_report_accepted_jobs.fields);
        DatabaseTableObject tbl_report_accepted_jobs_contacts = new DatabaseTableObject(Tables.tbl_report_accepted_jobs_contacts.table_name, Tables.tbl_report_accepted_jobs_contacts.fields);
        DatabaseTableObject tbl_report_submitted = new DatabaseTableObject(Tables.tbl_report_submitted.table_name, Tables.tbl_report_submitted.fields);
        DatabaseTableObject tbl_request_attachments = new DatabaseTableObject(Tables.tbl_request_attachments.table_name, Tables.tbl_request_attachments.fields);
        DatabaseTableObject tbl_request_collateral_address = new DatabaseTableObject(Tables.tbl_request_collateral_address.table_name, Tables.tbl_request_collateral_address.fields);
        DatabaseTableObject tbl_request_contact_persons = new DatabaseTableObject(Tables.tbl_request_contact_persons.table_name, Tables.tbl_request_contact_persons.fields);
        DatabaseTableObject tbl_request_form = new DatabaseTableObject(Tables.tbl_request_form.table_name, Tables.tbl_request_form.fields);
        DatabaseTableObject tbl_request_type = new DatabaseTableObject(Tables.tbl_request_type.table_name, Tables.tbl_request_type.fields);
        DatabaseTableObject tbl_required_attachments = new DatabaseTableObject(Tables.tbl_required_attachments.table_name, Tables.tbl_required_attachments.fields);
        DatabaseTableObject townhouse = new DatabaseTableObject(Tables.townhouse.table_name, Tables.townhouse.fields);
        DatabaseTableObject townhouse_condo = new DatabaseTableObject(Tables.townhouse_condo.table_name, Tables.townhouse_condo.fields);

        //tables with different id name
        /*DatabaseTableObject townhouse_imp_details = new DatabaseTableObject(Tables.condo.table_name, Tables.condo.fields);
        DatabaseTableObject townhouse_imp_details_features = new DatabaseTableObject(Tables.condo.table_name, Tables.condo.fields);*/

        DatabaseTableObject townhouse_lot_details = new DatabaseTableObject(Tables.townhouse_lot_details.table_name, Tables.townhouse_lot_details.fields);
        DatabaseTableObject townhouse_lot_valuation_details = new DatabaseTableObject(Tables.townhouse_lot_valuation_details.table_name, Tables.townhouse_lot_valuation_details.fields);
        DatabaseTableObject townhouse_main_prev_appraisal = new DatabaseTableObject(Tables.townhouse_main_prev_appraisal.table_name, Tables.townhouse_main_prev_appraisal.fields);
        DatabaseTableObject townhouse_prev_appraisal = new DatabaseTableObject(Tables.townhouse_prev_appraisal.table_name, Tables.townhouse_prev_appraisal.fields);
        DatabaseTableObject vacant_lot = new DatabaseTableObject(Tables.vacant_lot.table_name, Tables.vacant_lot.fields);
        DatabaseTableObject vacant_lot_lot_details = new DatabaseTableObject(Tables.vacant_lot_lot_details.table_name, Tables.vacant_lot_lot_details.fields);
        DatabaseTableObject vacant_lot_lot_valuation_details = new DatabaseTableObject(Tables.vacant_lot_lot_valuation_details.table_name, Tables.vacant_lot_lot_valuation_details.fields);
        DatabaseTableObject vacant_lot_main_prev_appraisal = new DatabaseTableObject(Tables.vacant_lot_main_prev_appraisal.table_name, Tables.vacant_lot_main_prev_appraisal.fields);
        DatabaseTableObject vacant_lot_prev_appraisal = new DatabaseTableObject(Tables.vacant_lot_prev_appraisal.table_name, Tables.vacant_lot_prev_appraisal.fields);


        DatabaseTableObject waf = new DatabaseTableObject(Tables.waf.table_name, Tables.waf.fields);
        DatabaseTableObject waf_listing = new DatabaseTableObject(Tables.waf_listing.table_name, Tables.waf_listing.fields);

        //spv
        DatabaseTableObject spv = new DatabaseTableObject(Tables.spv.table_name, Tables.spv.fields);
        DatabaseTableObject valrep_landimp_imp_details = new DatabaseTableObject(Tables.valrep_landimp_imp_details.table_name, Tables.valrep_landimp_imp_details.fields);
        DatabaseTableObject valrep_spv_land_imp_comps_table = new DatabaseTableObject(Tables.valrep_spv_land_imp_comps_table.table_name, Tables.valrep_spv_land_imp_comps_table.fields);
        DatabaseTableObject valrep_landimp_imp_valuation = new DatabaseTableObject(Tables.valrep_landimp_imp_valuation.table_name, Tables.valrep_landimp_imp_valuation.fields);
        DatabaseTableObject valrep_landimp_imp_valuation_details = new DatabaseTableObject(Tables.valrep_landimp_imp_valuation_details.table_name, Tables.valrep_landimp_imp_valuation_details.fields);
        DatabaseTableObject valrep_spv_landimp_summary_of_value_table = new DatabaseTableObject(Tables.valrep_spv_landimp_summary_of_value_table.table_name, Tables.valrep_spv_landimp_summary_of_value_table.fields);

        //add queries
        db.execSQL(condo.createQuery());

        db.execSQL(condo_main_prev_appraisal.createQuery());
        db.execSQL(condo_prev_appraisal.createQuery());
        db.execSQL(condo_title_details.createQuery());
        db.execSQL(condo_unit_details.createQuery());
        db.execSQL(condo_unit_valuation.createQuery());
        db.execSQL(construction_ebm.createQuery());
        db.execSQL(construction_ebm_room_list.createQuery());
        db.execSQL(db_check.createQuery());
        db.execSQL(land_improvements.createQuery());

        //tables with different id name
        db.execSQL(new DatabaseTableObject().create_land_improvements_imp_valuation_details());
        db.execSQL(new DatabaseTableObject().create_land_improvements_imp_details_features());
        db.execSQL(new DatabaseTableObject().create_land_improvements_imp_details());
        db.execSQL(new DatabaseTableObject().create_land_improvements_imp_valuation());

        db.execSQL(land_improvements_lot_details.createQuery());
        db.execSQL(land_improvements_lot_valuation_details.createQuery());
        db.execSQL(land_improvements_main_prev_appraisal.createQuery());
        db.execSQL(land_improvements_prev_appraisal.createQuery());
        db.execSQL(motor_vehicle.createQuery());
        db.execSQL(motor_vehicle_ownership_history.createQuery());
        db.execSQL(motor_vehicle_ownership_source.createQuery());
        db.execSQL(motor_vehicle_previous_appraisal.createQuery());
        db.execSQL(ppcr.createQuery());
        db.execSQL(ppcr_report_details.createQuery());
        db.execSQL(report_filename.createQuery());
        db.execSQL(submission_logs.createQuery());
        db.execSQL(tbl_attachments.createQuery());
        db.execSQL(tbl_cities.createQuery());
        db.execSQL(tbl_images.createQuery());
        db.execSQL(tbl_provinces.createQuery());
        db.execSQL(tbl_regions.createQuery());
        db.execSQL(tbl_report_accepted_jobs.createQuery());
        db.execSQL(tbl_report_accepted_jobs_contacts.createQuery());
        db.execSQL(tbl_report_submitted.createQuery());
        db.execSQL(tbl_request_attachments.createQuery());
        db.execSQL(tbl_request_collateral_address.createQuery());
        db.execSQL(tbl_request_contact_persons.createQuery());
        db.execSQL(tbl_request_form.createQuery());
        db.execSQL(tbl_request_type.createQuery());
        db.execSQL(tbl_required_attachments.createQuery());
        db.execSQL(townhouse.createQuery());
        db.execSQL(townhouse_condo.createQuery());

        //tables with different id name
        db.execSQL(new DatabaseTableObject().create_townhouse_imp_details());
        db.execSQL(new DatabaseTableObject().create_townhouse_imp_details_features());

        db.execSQL(townhouse_lot_details.createQuery());
        db.execSQL(townhouse_lot_valuation_details.createQuery());
        db.execSQL(townhouse_main_prev_appraisal.createQuery());
        db.execSQL(townhouse_prev_appraisal.createQuery());
        db.execSQL(vacant_lot.createQuery());
        db.execSQL(vacant_lot_lot_details.createQuery());
        db.execSQL(vacant_lot_lot_valuation_details.createQuery());
        db.execSQL(vacant_lot_main_prev_appraisal.createQuery());
        db.execSQL(vacant_lot_prev_appraisal.createQuery());

        db.execSQL(waf.createQuery());
        db.execSQL(waf_listing.createQuery());

        //spv
        db.execSQL(spv.createQuery());
        db.execSQL(valrep_landimp_imp_details.createQuery());
        db.execSQL(valrep_spv_land_imp_comps_table.createQuery());
        db.execSQL(valrep_landimp_imp_valuation.createQuery());
        db.execSQL(valrep_landimp_imp_valuation_details.createQuery());
        db.execSQL(valrep_spv_landimp_summary_of_value_table.createQuery());

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void addRecord(ArrayList<String> value, String[] fields, String table){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        for (int i = 0; i < value.size(); i++) {
            values.put(fields[i+1].toString(), value.get(i).toString());
            Log.e(fields[i+1].toString(), value.get(i).toString());
        }

        Log.e("addRecord", table);

        db.insert(table, null, values);
        db.close();
    }

    /*
    ArrayList<String> fields = new ArrayList<>();

    fields.clear();
    fields.add("");
    fields.add("");

    ArrayList<String> values = new ArrayList<>();
    values.clear();
    values.add("");
    values.add("");
    db.addRecord(values, fields, "tablename");
     */
    public void addRecord(ArrayList<String> value, ArrayList<String> fields, String table){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        for (int i = 0; i < value.size(); i++) {
            values.put(fields.get(i).toString(), value.get(i).toString());
            Log.e(fields.get(i).toString(), value.get(i).toString());
        }

        Log.e("addRecord", table);

        db.insert(table, null, values);
        db.close();

    }

    /**
     values.add(et.getText().toString());
     fields.add("et");
     db.updateRecord(values, fields, "record_id = ?", new String[]{record_id}, Tables.land_improvements.table_name);
     */
    public void updateRecord(ArrayList<String> updatedvalues, ArrayList<String> fields, String whereClause, String[] whereArgs, String table){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        for(int i = 0; i < updatedvalues.size(); i++){
            values.put(fields.get(i).toString(), updatedvalues.get(i).toString());
            Log.e(fields.get(i).toString(), updatedvalues.get(i).toString());
        }

        Log.e("updateRecord", table);

        db.update(table, values, whereClause, whereArgs);
        db.close();


    }

    public void updateRecord(String updatedvalue, String fieldname, String whereClause, String[] whereArgs, String table){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        values.put(fieldname, updatedvalue);
        Log.e(fieldname, updatedvalue);

        Log.e("updateRecord", table);

        db.update(table, values, whereClause, whereArgs);
        db.close();

    }

    /**How to use:
     db.deleteRecord("buying_stations", "field1 = ? AND field2 = ?", new String[]{paramsCondition1, paramsCondition2} );
     db.deleteRecord("tablename", "1", null );//delete all records
     */
    public void deleteRecord(String table, String whereClause, String[] whereArgs){
        SQLiteDatabase db = this.getWritableDatabase();

        Log.e("deleteRecord", whereClause + " " + whereArgs + " " + table);

        db.delete(table, whereClause, whereArgs);
        db.close();
    }


    public ArrayList<String> getRecord(String fieldName, String whereClause, String[] whereArgs, String table) {
        ArrayList<String> results = new ArrayList<String>();
        String selectQuery = "Select " + fieldName + " FROM " + table;
        String args = "args", value = "No record found";

        String query = selectQuery;

        if(!whereClause.contentEquals("null")) {
            for (int i = 0; i < whereArgs.length; i++) {

                whereClause = whereClause.replaceFirst(args, "\"" + whereArgs[i] + "\"");

            }
            query = query + " " + whereClause;
        }

        Log.e("query", query);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);


        if (cursor.moveToFirst()) {
            do {
                // get  the  data into array,or class variable
                value = nullToBlank(cursor.getString(cursor.getColumnIndex(fieldName)));
                results.add(value);

            } while (cursor.moveToNext());
        }else{
            results.add(value);
        }

        db.close();

        Log.e("results", results.toString());

        return results;
    }

    String nullToBlank(String s){
        if(s.contentEquals("null") || s.isEmpty() || s.length() == 0 ){
            s = "";
        }
        return s;
    }


    /**
     * mark's method
     */

    /*
	 * reference
	 */
    public String getTest(String table){
        String result="";
        String selectQuery = "Select * FROM " + table;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int totalFields = cursor.getColumnCount() - 1; // loop starts with 0 while getColumnCount ends with the total number of fields
        int cid = cursor.getColumnIndex("record_id") - 1;
        String[] cols = cursor.getColumnNames();

        //result = String.valueOf( cols[0].toString() );
//		for (int a = 0; a < cols.length; a++){
//			result = result + " "+String.valueOf( cols[a].toString() );
//		}
        result = String.valueOf(cid);
        return result;
    }
    //row count of query
    public int getRowCount(String table){
        int cnt=0;
        String selectQuery = "SELECT rowid FROM " + table;
        Log.e("selectQuery", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        cnt = cursor.getCount();
        return cnt;
    }
    //return id's
    public ArrayList<String> getRowIDs(String table, String query){
        ArrayList<String> results = new ArrayList<String>();
        String selectQuery = "Select rowid FROM " + table + " WHERE "+query;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        String[] cols = cursor.getColumnNames();
        String fieldComponent;
        //result = String.valueOf( cols[0].toString() );
        try{
            if(cursor.moveToFirst()){
                do{
                    for (int x = 0; x < cols.length; x++){
                        fieldComponent = cursor.getString( cursor.getColumnIndex(cols[x]) );
                        results.add(fieldComponent);
                    }

                }while(cursor.moveToNext());

            } else {
                Log.e("Error cursor iteration","Error cursor iteration");
                cursor.close();
            }

        } catch (Exception e) {
            // TODO: handle exception
            Log.e("Field Name mismatch","One of your fieldname does not exist in sqlite db");
        } finally {
            db.close();
        }
        return results;
    }

    /**
     * get all column names (effective for creating blank records) skip id and record id index
     */
    public ArrayList<String> getAllColumn(String table){
        ArrayList<String> results = new ArrayList<String>();
        String selectQuery = "Select * FROM " + table;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        String[] cols = cursor.getColumnNames();
        //result = String.valueOf( cols[0].toString() );
        for (int a = 0; a < cols.length; a++){
            results.add(String.valueOf( cols[a].toString() ));
        }
        return results;
    }

    /**
     * get all values based on column
     */
    public ArrayList<String> getAllValue(String table, String where){
        ArrayList<String> results = new ArrayList<String>();
        String selectQuery = "Select * FROM " + table +" "+ where;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        String[] cols = cursor.getColumnNames();
        //variables
        String fieldComponent;
        try{

            if(cursor.moveToFirst()){
                do{

                    for (int x = 0; x < cols.length; x++){
                        fieldComponent = cursor.getString( cursor.getColumnIndex(cols[x]) );
                        results.add(fieldComponent);
                    }

                }while(cursor.moveToNext());

            } else {
                Log.e("Error cursor iteration","Error cursor iteration");
                cursor.close();
                results.add("No record found");
            }

        } catch (Exception e) {
            // TODO: handle exception
            Log.e("Field Name mismatch","One of your fieldname does not exist in sqlite db");
            results.add("No record found");
        } finally {
            db.close();
        }
        return results;
    }

    //field only
    /*how to use
    String output = db.getRecord("fieldName", "tablename", "whereField", "whereValue" );
     */
    public String getRecord(String fieldName, String table, String whereField, String whereValue){
        String results = "";
        String selectQuery = "";
        if((whereField.contentEquals(""))&&(whereValue.contentEquals("")))
            selectQuery = "Select "+fieldName+" FROM " + table;
        else
            selectQuery = "Select "+fieldName+" FROM " + table + " WHERE "+whereField+" = \"" + whereValue + "\"";
        Log.v("SQL", selectQuery);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        try{
            if(cursor.moveToFirst()){
                do{
                    results = cursor.getString( cursor.getColumnIndex(fieldName) );
                }while(cursor.moveToNext());

            } else {
                cursor.close();
            }
        }catch (Exception e) {
            // TODO: handle exception
            Log.e("invalid Field Name",fieldName);
        } finally {
            db.close();
        }
        return results;
    }

    //field only
    /*how to use
    String output = db.getRecordQuery("fieldName", "tablename", "query" );
     */
    public String getRecordQuery(String fieldName, String table, String whereQuery){
        String results = "";
        String selectQuery = "";
        selectQuery = "Select "+fieldName+" FROM " + table + " WHERE "+whereQuery;
        Log.v("SQL", selectQuery);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        try{
            if(cursor.moveToFirst()){
                do{
                    results = cursor.getString( cursor.getColumnIndex(fieldName) );
                }while(cursor.moveToNext());

            } else {
                cursor.close();
            }
        }catch (Exception e) {
            // TODO: handle exception
            Log.e("invalid Field Name",fieldName);
        } finally {
            db.close();
        }
        return results;
    }

    //column only
    /*how to use
        ArrayList<String> output = db.getRecordColumn("columnName", "tablename", "whereField", "whereValue");
     */
    public ArrayList<String> getRecordColumn(String columnName, String table, String whereField, String whereValue){
        ArrayList<String> results = new ArrayList<String>();
        String selectQuery = "";
        String fieldComponent;
        if((whereField.contentEquals(""))&&(whereValue.contentEquals("")))
            selectQuery = "Select "+columnName+" FROM " + table;
        else
            selectQuery = "Select "+columnName+" FROM " + table + " WHERE "+whereField+" = \"" + whereValue + "\"";
        Log.e("query",selectQuery);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        try{
            if(cursor.moveToFirst()){
                do{
                    fieldComponent = cursor.getString( cursor.getColumnIndex(columnName) );
                    results.add(fieldComponent);
                }while(cursor.moveToNext());

            } else {
                cursor.close();
            }
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("invalid Field Name",columnName);
        } finally {
            db.close();
        }

        return results;
    }

    //column only
    /*how to use
        ArrayList<String> output = db.getRecordColumnQuery("columnName", "tablename", "whereQuery");
     */
    public ArrayList<String> getRecordColumnQuery(String columnName, String table, String whereQuery){
        ArrayList<String> results = new ArrayList<String>();
        String selectQuery = "";
        String fieldComponent;
        selectQuery = "Select "+columnName+" FROM " + table + " "+whereQuery;
        Log.e("query",selectQuery);


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        try{
            if(cursor.moveToFirst()){
                do{
                    fieldComponent = cursor.getString( cursor.getColumnIndex(columnName) );
                    results.add(fieldComponent);
                }while(cursor.moveToNext());
            } else {
                cursor.close();
            }
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("invalid Field Name",columnName);
        } finally {
            db.close();
        }
        return results;
    }


    public ArrayList<String> getRecordColumnQueryJoin(String columnName, String query){
        ArrayList<String> results = new ArrayList<String>();
        String selectQuery = "";
        String fieldComponent;
        selectQuery = ""+query;
        Log.e("query",selectQuery);


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        try{
            if(cursor.moveToFirst()){
                do{
                    fieldComponent = cursor.getString( cursor.getColumnIndex(columnName) );
                    results.add(fieldComponent);
                }while(cursor.moveToNext());
            } else {
                cursor.close();
            }
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("invalid Field Name",columnName);
        } finally {
            db.close();
        }
        return results;
    }
}
