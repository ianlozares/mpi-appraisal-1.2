package com.gds.appraisalmaybank.ppcr;

import com.gds.appraisalmaybank.main.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Ppcr_fragment extends Fragment {
	public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";

	public static final Ppcr_fragment newInstance(String message) {
		Ppcr_fragment f = new Ppcr_fragment();
		Bundle bdl = new Bundle(1);
		bdl.putString(EXTRA_MESSAGE, message);
		f.setArguments(bdl);
		return f;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		String message = getArguments().getString(EXTRA_MESSAGE);
		View v = inflater.inflate(R.layout.ppcr_fragment, container, false);
		TextView ppcr_pages = (TextView) v.findViewById(R.id.ppcr_pages);
		TextView report_foundation_works = (TextView) v
				.findViewById(R.id.report_foundation_works);
		TextView report_columns = (TextView) v
				.findViewById(R.id.report_columns);
		TextView report_beams_girders = (TextView) v
				.findViewById(R.id.report_beams_girders);
		TextView report_sorrounding_walls = (TextView) v
				.findViewById(R.id.report_surrounding_walls);
		TextView report_interior_partitions = (TextView) v
				.findViewById(R.id.report_interior_partitions);
		TextView report_roofing_works = (TextView) v
				.findViewById(R.id.report_roofing_works);
		TextView report_plumbing_rough = (TextView) v
				.findViewById(R.id.report_plumbing_rough);
		TextView report_electrical_rough = (TextView) v
				.findViewById(R.id.report_electrical_rough);
		TextView report_ceiling_works = (TextView) v
				.findViewById(R.id.report_ceiling_works);
		TextView report_doors_windows = (TextView) v
				.findViewById(R.id.report_doors_windows);
		TextView report_plastering_wall = (TextView) v
				.findViewById(R.id.report_plastering_wall);
		TextView report_slab_including = (TextView) v
				.findViewById(R.id.report_slab_including);
		TextView report_painting_finishing = (TextView) v
				.findViewById(R.id.report_painting_finishing);
		TextView report_plumbing_elec = (TextView) v
				.findViewById(R.id.report_plumbing_elec);
		TextView report_utilities_tapping = (TextView) v
				.findViewById(R.id.report_utilities_tapping);
		TextView report_total = (TextView) v.findViewById(R.id.report_total);
		TextView report_inspection = (TextView) v
				.findViewById(R.id.report_inspection);
		TextView report_remarks = (TextView) v
				.findViewById(R.id.report_remarks);
		TextView report_date_inspected_month = (TextView) v
				.findViewById(R.id.report_date_inspected_month);
		TextView report_date_inspected_day = (TextView) v
				.findViewById(R.id.report_date_inspected_day);
		TextView report_date_inspected_year = (TextView) v
				.findViewById(R.id.report_date_inspected_year);

		String details_arrays[] = message.split("\\/");
		report_foundation_works.setText(details_arrays[0]);
		report_columns.setText(details_arrays[1]);
		report_beams_girders.setText(details_arrays[2]);
		report_sorrounding_walls.setText(details_arrays[3]);
		report_interior_partitions.setText(details_arrays[4]);
		report_roofing_works.setText(details_arrays[5]);
		report_plumbing_rough.setText(details_arrays[6]);
		report_electrical_rough.setText(details_arrays[7]);
		report_ceiling_works.setText(details_arrays[8]);
		report_doors_windows.setText(details_arrays[9]);
		report_plastering_wall.setText(details_arrays[10]);
		report_slab_including.setText(details_arrays[11]);
		report_painting_finishing.setText(details_arrays[12]);
		report_plumbing_elec.setText(details_arrays[13]);
		report_utilities_tapping.setText(details_arrays[14]);
		report_total.setText(details_arrays[15]);
		report_inspection.setText(details_arrays[16]);
		report_remarks.setText(details_arrays[17]);
		report_date_inspected_month.setText(details_arrays[18] + "/");
		report_date_inspected_day.setText(details_arrays[19] + "/");
		report_date_inspected_year.setText(details_arrays[20]);

		ppcr_pages.setText("Inspection " + details_arrays[22] + " of "
				+ details_arrays[21]);

		return v;
	}
}
