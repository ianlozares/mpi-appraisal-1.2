package com.gds.appraisalmaybank.ppcr;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database2.Ppcr_API_Details;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Ppcr_Visit extends ListActivity implements OnClickListener, TextWatcher {
	DatabaseHandler2 db = new DatabaseHandler2(this);
	GDS_methods gds = new GDS_methods();
	Button btn_add_visit, btn_save_visit;
	Button btn_create, btn_cancel, btn_save;

	Session_Timer st = new Session_Timer(this);
	DatePicker report_date_visited;
	EditText report_inspection;
	EditText report_foundation, report_columns, report_beams_girders,
			report_surround_walls, report_interior_partition,
			report_roofing_works, report_plumbing_rough_in,
			report_electrical_rough_in, report_ceiling_works,
			report_doors_windows, report_plastering_wall,
			report_slab_include_finish, report_painting_finishing,
			report_plumbing_elect_fix, report_utilities_tapping;
	TextView val_foundation, val_columns, val_beams_girders,
			val_surround_walls, val_interior_partition, val_roofing_works,
			val_plumbing_rough_in, val_electrical_rough_in, val_ceiling_works,
			val_doors_windows, val_plastering_wall, val_slab_include_finish,
			val_painting_finishing, val_plumbing_elect_fix,
			val_utilities_tapping;
	TextView c_foundation, c_columns, c_beams_girders, c_surround_walls,
			c_interior_partition, c_roofing_works, c_plumbing_rough_in,
			c_electrical_rough_in, c_ceiling_works, c_doors_windows,
			c_plastering_wall, c_slab_include_finish, c_painting_finishing,
			c_plumbing_elect_fix, c_utilities_tapping;
	EditText report_total;
	
	String record_id="";
	private static final String TAG_RECORD_ID = "record_id";
	
	Dialog myDialog;
	//create dialog

	//forListViewDeclarations
	private ProgressDialog pDialog;
	ArrayList<HashMap<String, String>> itemList;
	private static final String tag_ppcrd_id = "ppcrd_id";
	private static final String tag_date = "date";
	private static final String tag_nth = "nth";
	private static final String tag_total = "total";
	String ppcrd_id="", date="", nth="", total="";
	Double total_d=0.00;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.report_ppcr_visit);

		st.resetDisconnectTimer();
		btn_add_visit = (Button) findViewById(R.id.btn_add_visit);
		btn_add_visit.setOnClickListener(this);
		
		btn_save_visit = (Button) findViewById(R.id.btn_save_visit);
		btn_save_visit.setOnClickListener(this);
		
		
		// getting record_id from intent / previous class
		Intent i = getIntent();
		record_id = i.getStringExtra(TAG_RECORD_ID);
		//list
		itemList = new ArrayList<HashMap<String, String>>();
		// Loading in Background Thread
		new LoadAll().execute();
		//fill_total();
		// Get listview
		ListView lv = getListView();

		// on seleting single item
		// launching Edit item Screen
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// getting values from selected ListItem
				ppcrd_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
				view_details();
			}
		});
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

			public boolean onItemLongClick(AdapterView<?> parent, View view,
										   int position, long id) {
				// TODO Auto-generated method stub

				Log.v("long clicked", "pos: " + position);
				ppcrd_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
				delete_item();
				return true;
			}
		});
		
	}
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.btn_add_visit:
			add_new();
			break;
		case R.id.btn_save_visit:
			finish();
			break;
		default:
			break;
		}
	}
	
	public void editTextDeclarations(){
		
		report_date_visited = (DatePicker) myDialog.findViewById(R.id.report_date_visited);
		
		report_inspection = (EditText) myDialog.findViewById(R.id.report_inspection);
		
		report_foundation = (EditText) myDialog.findViewById(R.id.report_foundation);
		report_columns = (EditText) myDialog.findViewById(R.id.report_columns);
		report_beams_girders = (EditText) myDialog.findViewById(R.id.report_beams_girders);
		report_surround_walls = (EditText) myDialog.findViewById(R.id.report_surround_walls);
		report_interior_partition = (EditText) myDialog.findViewById(R.id.report_interior_partition);
		report_roofing_works = (EditText) myDialog.findViewById(R.id.report_roofing_works);
		report_plumbing_rough_in = (EditText) myDialog.findViewById(R.id.report_plumbing_rough_in);
		report_electrical_rough_in = (EditText) myDialog.findViewById(R.id.report_electrical_rough_in);
		report_ceiling_works = (EditText) myDialog.findViewById(R.id.report_ceiling_works);
		report_doors_windows = (EditText) myDialog.findViewById(R.id.report_doors_windows);
		report_plastering_wall = (EditText) myDialog.findViewById(R.id.report_plastering_wall);
		report_slab_include_finish = (EditText) myDialog.findViewById(R.id.report_slab_include_finish);
		report_painting_finishing = (EditText) myDialog.findViewById(R.id.report_painting_finishing);
		report_plumbing_elect_fix = (EditText) myDialog.findViewById(R.id.report_plumbing_elect_fix);
		report_utilities_tapping = (EditText) myDialog.findViewById(R.id.report_utilities_tapping);
		
		
		report_total = (EditText) myDialog.findViewById(R.id.report_total);
		report_total.addTextChangedListener(this);
		
		report_foundation.addTextChangedListener(this);
		report_columns.addTextChangedListener(this);
		report_beams_girders.addTextChangedListener(this);
		report_surround_walls.addTextChangedListener(this);
		report_interior_partition.addTextChangedListener(this);
		report_roofing_works.addTextChangedListener(this);
		report_plumbing_rough_in.addTextChangedListener(this);
		report_electrical_rough_in.addTextChangedListener(this);
		report_ceiling_works.addTextChangedListener(this);
		report_doors_windows.addTextChangedListener(this);
		report_plastering_wall.addTextChangedListener(this);
		report_slab_include_finish.addTextChangedListener(this);
		report_painting_finishing.addTextChangedListener(this);
		report_plumbing_elect_fix.addTextChangedListener(this);
		report_utilities_tapping.addTextChangedListener(this);
		
		c_foundation = (TextView) myDialog.findViewById(R.id.c_foundation);
		c_columns = (TextView) myDialog.findViewById(R.id.c_columns);
		c_beams_girders = (TextView) myDialog.findViewById(R.id.c_beams_girders);
		c_surround_walls = (TextView) myDialog.findViewById(R.id.c_surround_walls);
		c_interior_partition = (TextView) myDialog.findViewById(R.id.c_interior_partition);
		c_roofing_works = (TextView) myDialog.findViewById(R.id.c_roofing_works);
		c_plumbing_rough_in = (TextView) myDialog.findViewById(R.id.c_plumbing_rough_in);
		c_electrical_rough_in = (TextView) myDialog.findViewById(R.id.c_electrical_rough_in);
		c_ceiling_works = (TextView) myDialog.findViewById(R.id.c_ceiling_works);
		c_doors_windows = (TextView) myDialog.findViewById(R.id.c_doors_windows);
		c_plastering_wall = (TextView) myDialog.findViewById(R.id.c_plastering_wall);
		c_slab_include_finish = (TextView) myDialog.findViewById(R.id.c_slab_include_finish);
		c_painting_finishing = (TextView) myDialog.findViewById(R.id.c_painting_finishing);
		c_plumbing_elect_fix = (TextView) myDialog.findViewById(R.id.c_plumbing_elect_fix);
		c_utilities_tapping = (TextView) myDialog.findViewById(R.id.c_utilities_tapping);
		
		val_foundation = (TextView) myDialog.findViewById(R.id.val_foundation);
		val_columns = (TextView) myDialog.findViewById(R.id.val_columns);
		val_beams_girders = (TextView) myDialog.findViewById(R.id.val_beams_girders);
		val_surround_walls = (TextView) myDialog.findViewById(R.id.val_surround_walls);
		val_interior_partition = (TextView) myDialog.findViewById(R.id.val_interior_partition);
		val_roofing_works = (TextView) myDialog.findViewById(R.id.val_roofing_works);
		val_plumbing_rough_in = (TextView) myDialog.findViewById(R.id.val_plumbing_rough_in);
		val_electrical_rough_in = (TextView) myDialog.findViewById(R.id.val_electrical_rough_in);
		val_ceiling_works = (TextView) myDialog.findViewById(R.id.val_ceiling_works);
		val_doors_windows = (TextView) myDialog.findViewById(R.id.val_doors_windows);
		val_plastering_wall = (TextView) myDialog.findViewById(R.id.val_plastering_wall);
		val_slab_include_finish = (TextView) myDialog.findViewById(R.id.val_slab_include_finish);
		val_painting_finishing = (TextView) myDialog.findViewById(R.id.val_painting_finishing);
		val_plumbing_elect_fix = (TextView) myDialog.findViewById(R.id.val_plumbing_elect_fix);
		val_utilities_tapping = (TextView) myDialog.findViewById(R.id.val_utilities_tapping);
		
		val_foundation.setText("11.00");
		val_columns.setText("10.50");
		val_beams_girders.setText("8.50");
		val_surround_walls.setText("4.50");
		val_interior_partition.setText("6.00");
		val_roofing_works.setText("9.00");
		val_plumbing_rough_in.setText("4.00");
		val_electrical_rough_in.setText("4.50");
		val_ceiling_works.setText("6.00");
		val_doors_windows.setText("5.50");
		val_plastering_wall.setText("9.50");
		val_slab_include_finish.setText("7.90");
		val_painting_finishing.setText("6.10");
		val_plumbing_elect_fix.setText("5.00");
		val_utilities_tapping.setText("2.00");
		
		c_foundation.setText("0.00");
		c_columns.setText("0.00");
		c_beams_girders.setText("0.00");
		c_surround_walls.setText("0.00");
		c_interior_partition.setText("0.00");
		c_roofing_works.setText("0.00");
		c_plumbing_rough_in.setText("0.00");
		c_electrical_rough_in.setText("0.00");
		c_ceiling_works.setText("0.00");
		c_doors_windows.setText("0.00");
		c_plastering_wall.setText("0.00");
		c_slab_include_finish.setText("0.00");
		c_painting_finishing.setText("0.00");
		c_plumbing_elect_fix.setText("0.00");
		c_utilities_tapping.setText("0.00");
	}
	
	public void add_new(){
		/*Toast.makeText(getApplicationContext(), "Add New",
				Toast.LENGTH_SHORT).show();*/
		myDialog = new Dialog(Ppcr_Visit.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.report_ppcr_visit_form);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		
		btn_create = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_create.setText("Create");
		btn_cancel.setText("Cancel");
		editTextDeclarations();
		
		
		btn_create.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//list of EditTexts put in array
				if(report_inspection.getText().toString().contentEquals("")){
					report_inspection.setText("0");
				}



					//adjust for month
					String adjMonth="";
					if (String.valueOf(report_date_visited.getMonth()).length() == 1) {//if month is 1 digit add 0 to initial +1 to value
						if (String.valueOf(report_date_visited.getMonth())
								.equals("9")) {//if october==9 just add 1
							adjMonth = String.valueOf(report_date_visited.getMonth() + 1);
						} else {
							adjMonth = "0"+ String.valueOf(report_date_visited.getMonth() + 1);
						}
					} else {
						adjMonth = String.valueOf(report_date_visited.getMonth() + 1);
					}
					//add data in SQLite
					db.addPpcr_Details(new Ppcr_API_Details(
							record_id,
							adjMonth,
							String.valueOf(report_date_visited.getDayOfMonth()),
							String.valueOf(report_date_visited.getYear()),
							report_inspection.getText().toString(),
							report_foundation.getText().toString(),
							report_columns.getText().toString(),
							report_beams_girders.getText().toString(),
							report_surround_walls.getText().toString(),
							report_interior_partition.getText().toString(),
							report_roofing_works.getText().toString(),
							report_plumbing_rough_in.getText().toString(),
							report_electrical_rough_in.getText().toString(),
							report_ceiling_works.getText().toString(),
							report_doors_windows.getText().toString(),
							report_plastering_wall.getText().toString(),
							report_slab_include_finish.getText().toString(),
							report_painting_finishing.getText().toString(),
							report_plumbing_elect_fix.getText().toString(),
							report_utilities_tapping.getText().toString(),
							c_foundation.getText().toString(),
							c_columns.getText().toString(),
							c_beams_girders.getText().toString(),
							c_surround_walls.getText().toString(),
							c_interior_partition.getText().toString(),
							c_roofing_works.getText().toString(),
							c_plumbing_rough_in.getText().toString(),
							c_electrical_rough_in.getText().toString(),
							c_ceiling_works.getText().toString(),
							c_doors_windows.getText().toString(),
							c_plastering_wall.getText().toString(),
							c_slab_include_finish.getText().toString(),
							c_painting_finishing.getText().toString(),
							c_plumbing_elect_fix.getText().toString(),
							c_utilities_tapping.getText().toString(),
							report_total.getText().toString()));
					
						Toast.makeText(getApplicationContext(),"data created", Toast.LENGTH_SHORT).show();
						myDialog.dismiss();
						reloadClass();

				
			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});

		gds.fill_in_error(new EditText[]{report_foundation, report_columns, report_beams_girders,
				report_surround_walls, report_interior_partition,
				report_roofing_works, report_plumbing_rough_in,
				report_electrical_rough_in, report_ceiling_works,
				report_doors_windows, report_plastering_wall,
				report_slab_include_finish, report_painting_finishing,
				report_plumbing_elect_fix, report_utilities_tapping});
		myDialog.show();
	}
	
	public void view_details(){
		myDialog = new Dialog(Ppcr_Visit.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.report_ppcr_visit_form);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		
		btn_save = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_save.setText("Save");
		btn_cancel.setText("Cancel");
		editTextDeclarations();
		
		List<Ppcr_API_Details> pdList = db
				.getPpcr_Details(String.valueOf(record_id), String.valueOf(ppcrd_id));
		if (!pdList.isEmpty()) {
			for (Ppcr_API_Details im : pdList) {
				//date of inspection
				// set current date into datepicker
				if ((!im.getreport_visit_year().equals(""))||(!im.getreport_visit_month().equals(""))) {
					report_date_visited.init(
							Integer.parseInt(im.getreport_visit_year()),
							Integer.parseInt(im.getreport_visit_month())-1,
							Integer.parseInt(im.getreport_visit_day()),
							null);
				}
				report_inspection.setText(im.getreport_nth());
				report_foundation.setText(im.getreport_value_foundation());
				report_columns.setText(im.getreport_value_columns());
				report_beams_girders.setText(im.getreport_value_beams_girders());
				report_surround_walls.setText(im.getreport_value_surround_walls());
				report_interior_partition.setText(im.getreport_value_interior_partition());
				report_roofing_works.setText(im.getreport_value_roofing_works());
				report_plumbing_rough_in.setText(im.getreport_value_plumbing_rough_in());
				report_electrical_rough_in.setText(im.getreport_value_electrical_rough_in());
				report_ceiling_works.setText(im.getreport_value_ceiling_works());
				report_doors_windows.setText(im.getreport_value_doors_windows());
				report_plastering_wall.setText(im.getreport_value_plastering_wall());
				report_slab_include_finish.setText(im.getreport_value_slab_include_finish());
				report_painting_finishing.setText(im.getreport_value_painting_finishing());
				report_plumbing_elect_fix.setText(im.getreport_value_plumbing_elect_fix());
				report_utilities_tapping.setText(im.getreport_value_utilities_tapping());
				c_foundation.setText(im.getreport_completion_foundation());
				c_columns.setText(im.getreport_completion_columns());
				c_beams_girders.setText(im.getreport_completion_beams_girders());
				c_surround_walls.setText(im.getreport_completion_surround_walls());
				c_interior_partition.setText(im.getreport_completion_interior_partition());
				c_roofing_works.setText(im.getreport_completion_roofing_works());
				c_plumbing_rough_in.setText(im.getreport_completion_plumbing_rough_in());
				c_electrical_rough_in.setText(im.getreport_completion_electrical_rough_in());
				c_ceiling_works.setText(im.getreport_completion_ceiling_works());
				c_doors_windows.setText(im.getreport_completion_doors_windows());
				c_plastering_wall.setText(im.getreport_completion_plastering_wall());
				c_slab_include_finish.setText(im.getreport_completion_slab_include_finish());
				c_painting_finishing.setText(im.getreport_completion_painting_finishing());
				c_plumbing_elect_fix.setText(im.getreport_completion_plumbing_elect_fix());
				c_utilities_tapping.setText(im.getreport_completion_utilities_tapping());
				report_total.setText(im.getreport_total_value());
			}
		}
		
		
		btn_save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(report_inspection.getText().toString().contentEquals("")){
					report_inspection.setText("0");
				}

					//update data in SQLite
					Ppcr_API_Details pd = new Ppcr_API_Details();
					//date
					
					if (String.valueOf(report_date_visited.getMonth()).length() == 1) {//if month is 1 digit add 0 to initial +1 to value
						if (String.valueOf(report_date_visited.getMonth())
								.equals("9")) {//if october==9 just add 1
							pd.setreport_visit_month(String
									.valueOf(report_date_visited.getMonth() + 1));
						} else {
							pd.setreport_visit_month("0"
									+ String.valueOf(report_date_visited
											.getMonth() + 1));
						}
					} else {
						pd.setreport_visit_month(String
								.valueOf(report_date_visited.getMonth() + 1));
					}
					pd.setreport_visit_day(String.valueOf(report_date_visited.getDayOfMonth()));
					pd.setreport_visit_year(String.valueOf(report_date_visited.getYear()));
					
					pd.setreport_nth(report_inspection.getText().toString());
					
					pd.setreport_value_foundation(report_foundation.getText().toString());
					pd.setreport_value_columns(report_columns.getText().toString());
					pd.setreport_value_beams_girders(report_beams_girders.getText().toString());
					pd.setreport_value_surround_walls(report_surround_walls.getText().toString());
					pd.setreport_value_interior_partition(report_interior_partition.getText().toString());
					pd.setreport_value_roofing_works(report_roofing_works.getText().toString());
					pd.setreport_value_plumbing_rough_in(report_plumbing_rough_in.getText().toString());
					pd.setreport_value_electrical_rough_in(report_electrical_rough_in.getText().toString());
					pd.setreport_value_ceiling_works(report_ceiling_works.getText().toString());
					pd.setreport_value_doors_windows(report_doors_windows.getText().toString());
					pd.setreport_value_plastering_wall(report_plastering_wall.getText().toString());
					pd.setreport_value_slab_include_finish(report_slab_include_finish.getText().toString());
					pd.setreport_value_painting_finishing(report_painting_finishing.getText().toString());
					pd.setreport_value_plumbing_elect_fix(report_plumbing_elect_fix.getText().toString());
					pd.setreport_value_utilities_tapping(report_utilities_tapping.getText().toString());
					pd.setreport_completion_foundation(c_foundation.getText().toString());
					pd.setreport_completion_columns(c_columns.getText().toString());
					pd.setreport_completion_beams_girders(c_beams_girders.getText().toString());
					pd.setreport_completion_surround_walls(c_surround_walls.getText().toString());
					pd.setreport_completion_interior_partition(c_interior_partition.getText().toString());
					pd.setreport_completion_roofing_works(c_roofing_works.getText().toString());
					pd.setreport_completion_plumbing_rough_in(c_plumbing_rough_in.getText().toString());
					pd.setreport_completion_electrical_rough_in(c_electrical_rough_in.getText().toString());
					pd.setreport_completion_ceiling_works(c_ceiling_works.getText().toString());
					pd.setreport_completion_doors_windows(c_doors_windows.getText().toString());
					pd.setreport_completion_plastering_wall(c_plastering_wall.getText().toString());
					pd.setreport_completion_slab_include_finish(c_slab_include_finish.getText().toString());
					pd.setreport_completion_painting_finishing(c_painting_finishing.getText().toString());
					pd.setreport_completion_plumbing_elect_fix(c_plumbing_elect_fix.getText().toString());
					pd.setreport_completion_utilities_tapping(c_utilities_tapping.getText().toString());
					pd.setreport_total_value(report_total.getText().toString());
					db.updatePpcr_Details(pd, record_id, ppcrd_id);
					db.close();
					myDialog.dismiss();
					Toast.makeText(getApplicationContext(), "Saved",Toast.LENGTH_SHORT).show();
					reloadClass();

			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});


		gds.fill_in_error(new EditText[]{report_foundation, report_columns, report_beams_girders,
				report_surround_walls, report_interior_partition,
				report_roofing_works, report_plumbing_rough_in,
				report_electrical_rough_in, report_ceiling_works,
				report_doors_windows, report_plastering_wall,
				report_slab_include_finish, report_painting_finishing,
				report_plumbing_elect_fix, report_utilities_tapping});
		myDialog.show();
	}
	
	
	//EditText checker
	private boolean validate(EditText[] fields){
        for(int i=0; i<fields.length; i++){
            EditText currentField=fields[i];
            if(currentField.getText().toString().length()<=0){
                return false;
            }
        }
        return true;
	}
	public void reloadClass(){
		Intent intent = getIntent();
		finish();
		startActivity(intent);
	}
	public void delete_item(){
		myDialog = new Dialog(Ppcr_Visit.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.delete_dialog);
		myDialog.setCancelable(true);
		final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
		final Button btn_yes = (Button) myDialog.findViewById(R.id.btn_yes);
		final Button btn_no = (Button) myDialog.findViewById(R.id.btn_no);
		tv_question.setText("Are you sure you want to delete the selected item?");
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				db.deletePpcr_Details(record_id, ppcrd_id);
				db.close();
				myDialog.dismiss();
				reloadClass();
				Toast.makeText(getApplicationContext(), "Data Deleted",Toast.LENGTH_SHORT).show();
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		
		myDialog.show();
	}
	
	
	/**
	 * Background Async Task to Load all
	 * */
	class LoadAll extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Ppcr_Visit.this);
			pDialog.setMessage("Loading Lists. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All
		 * */
		protected String doInBackground(String... args) {

			List<Ppcr_API_Details> ppcrDetails = db.getPpcr_Details(record_id);
			if (!ppcrDetails.isEmpty()) {
				for (Ppcr_API_Details imppcr : ppcrDetails) {
					ppcrd_id = String.valueOf(imppcr.getID());
					record_id = imppcr.getrecord_id();
					date = imppcr.getreport_visit_month()
							+"/"+ imppcr.getreport_visit_day()
							+"/"+ imppcr.getreport_visit_year();
					nth = imppcr.getreport_nth();

					imppcr.getreport_value_foundation();
					imppcr.getreport_value_columns();
					imppcr.getreport_value_beams_girders();
					imppcr.getreport_value_surround_walls();
					imppcr.getreport_value_interior_partition();
					imppcr.getreport_value_roofing_works();
					imppcr.getreport_value_plumbing_rough_in();
					imppcr.getreport_value_electrical_rough_in();
					imppcr.getreport_value_ceiling_works();
					imppcr.getreport_value_doors_windows();
					imppcr.getreport_value_plastering_wall();
					imppcr.getreport_value_slab_include_finish();
					imppcr.getreport_value_painting_finishing();
					imppcr.getreport_value_plumbing_elect_fix();
					imppcr.getreport_value_utilities_tapping();
					
					imppcr.getreport_completion_foundation();
					imppcr.getreport_completion_columns();
					imppcr.getreport_completion_beams_girders();
					imppcr.getreport_completion_surround_walls();
					imppcr.getreport_completion_interior_partition();
					imppcr.getreport_completion_roofing_works();
					imppcr.getreport_completion_plumbing_rough_in();
					imppcr.getreport_completion_electrical_rough_in();
					imppcr.getreport_completion_ceiling_works();
					imppcr.getreport_completion_doors_windows();
					imppcr.getreport_completion_plastering_wall();
					imppcr.getreport_completion_slab_include_finish();
					imppcr.getreport_completion_painting_finishing();
					imppcr.getreport_completion_plumbing_elect_fix();
					imppcr.getreport_completion_utilities_tapping();
					
					total = imppcr.getreport_total_value();
					
					HashMap<String, String> map = new HashMap<String, String>();
					map.put(TAG_RECORD_ID, record_id);
					map.put(tag_ppcrd_id, ppcrd_id);
					map.put(tag_date, date);
					map.put(tag_nth, nth);
					map.put(tag_total, total);

					// adding HashList to ArrayList
					itemList.add(map);
				}
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					ListAdapter adapter = new SimpleAdapter(
							Ppcr_Visit.this, itemList,
							R.layout.report_ppcr_visit_list, new String[] { TAG_RECORD_ID, tag_ppcrd_id, tag_date,
									tag_nth, tag_total},
									new int[] { R.id.report_record_id, R.id.primary_key, R.id.item_date, R.id.item_nth, R.id.item_total});
					// updating listview
					setListAdapter(adapter);
				}
			});
			if(itemList.isEmpty()){
				add_new();
			}
		}
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub
	}
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
	}
	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		if (s == report_foundation.getEditableText()) {
        	if ((report_foundation.getText().length()!=0)&&(!report_foundation.getText().toString().equals("."))){
    			c_foundation.setText(String.valueOf(
						gds.format(gds.StringtoDouble(report_foundation.getText().toString())
    	        		* gds.StringtoDouble(val_foundation.getText().toString())/100)));
    		} else if (report_foundation.getText().length()==0){
    			c_foundation.setText("0.00");
    		}
        	if (report_foundation.getText().toString().startsWith(".")){
        		report_foundation.setText("0.00");
        	}
		}
		if (s == report_columns.getEditableText()) {
        	if ((report_columns.getText().length()!=0)&&(!report_columns.getText().toString().equals("."))){
    			c_columns.setText(String.valueOf(
						gds.format(gds.StringtoDouble(report_columns.getText().toString())
    	        		* gds.StringtoDouble(val_columns.getText().toString())/100)));
    		} else if (report_columns.getText().length()==0){
    			c_columns.setText("0.00");
    		}
        	if (report_columns.getText().toString().equals(".")){
        		report_columns.setText("0.00");
        	}
		}
		if (s == report_beams_girders.getEditableText()) {
        	if ((report_beams_girders.getText().length()!=0)&&(!report_beams_girders.getText().toString().equals("."))){
    			c_beams_girders.setText(String.valueOf(
						gds.format(gds.StringtoDouble(report_beams_girders.getText().toString())
    	        		* gds.StringtoDouble(val_beams_girders.getText().toString())/100)));
    		} else if (report_beams_girders.getText().length()==0){
    			c_beams_girders.setText("0.00");
    		}
        	if (report_beams_girders.getText().toString().equals(".")){
        		report_beams_girders.setText("0.00");
        	}
		}
		if (s == report_surround_walls.getEditableText()) {
        	if ((report_surround_walls.getText().length()!=0)&&(!report_surround_walls.getText().toString().equals("."))){
    			c_surround_walls.setText(String.valueOf(
						gds.format(gds.StringtoDouble(report_surround_walls.getText().toString())
    	        		* gds.StringtoDouble(val_surround_walls.getText().toString())/100)));
    		} else if (report_surround_walls.getText().length()==0){
    			c_surround_walls.setText("0.00");
    		}
        	if (report_surround_walls.getText().toString().equals(".")){
        		report_surround_walls.setText("0.00");
        	}
		}
		if (s == report_interior_partition.getEditableText()) {
        	if ((report_interior_partition.getText().length()!=0)&&(!report_interior_partition.getText().toString().equals("."))){
    			c_interior_partition.setText(String.valueOf(
    	        		gds.format(gds.StringtoDouble(report_interior_partition.getText().toString())
    	        		* gds.StringtoDouble(val_interior_partition.getText().toString())/100)));
    		} else if (report_interior_partition.getText().length()==0){
    			c_interior_partition.setText("0.00");
    		}
        	if (report_interior_partition.getText().toString().equals(".")){
        		report_interior_partition.setText("0.00");
        	}
		}
		if (s == report_roofing_works.getEditableText()) {
        	if ((report_roofing_works.getText().length()!=0)&&(!report_roofing_works.getText().toString().equals("."))){
    			c_roofing_works.setText(String.valueOf(
    	        		gds.format(gds.StringtoDouble(report_roofing_works.getText().toString())
    	        		* gds.StringtoDouble(val_roofing_works.getText().toString())/100)));
    		} else if (report_roofing_works.getText().length()==0){
    			c_roofing_works.setText("0.00");
    		}
        	if (report_roofing_works.getText().toString().equals(".")){
        		report_roofing_works.setText("0.00");
        	}
		}
		if (s == report_plumbing_rough_in.getEditableText()) {
        	if ((report_plumbing_rough_in.getText().length()!=0)&&(!report_plumbing_rough_in.getText().toString().equals("."))){
    			c_plumbing_rough_in.setText(String.valueOf(
    	        		gds.format(gds.StringtoDouble(report_plumbing_rough_in.getText().toString())
    	        		* gds.StringtoDouble(val_plumbing_rough_in.getText().toString())/100)));
    		} else if (report_plumbing_rough_in.getText().length()==0){
    			c_plumbing_rough_in.setText("0.00");
    		}
        	if (report_plumbing_rough_in.getText().toString().equals(".")){
        		report_plumbing_rough_in.setText("0.00");
        	}
		}
		if (s == report_electrical_rough_in.getEditableText()) {
        	if ((report_electrical_rough_in.getText().length()!=0)&&(!report_electrical_rough_in.getText().toString().equals("."))){
    			c_electrical_rough_in.setText(String.valueOf(
    	        		gds.format(gds.StringtoDouble(report_electrical_rough_in.getText().toString())
    	        		* gds.StringtoDouble(val_electrical_rough_in.getText().toString())/100)));
    		} else if (report_electrical_rough_in.getText().length()==0){
    			c_electrical_rough_in.setText("0.00");
    		}
        	if (report_electrical_rough_in.getText().toString().equals(".")){
        		report_electrical_rough_in.setText("0.00");
        	}
		}
		if (s == report_ceiling_works.getEditableText()) {
        	if ((report_ceiling_works.getText().length()!=0)&&(!report_ceiling_works.getText().toString().equals("."))){
    			c_ceiling_works.setText(String.valueOf(
    	        		gds.format(gds.StringtoDouble(report_ceiling_works.getText().toString())
    	        		* gds.StringtoDouble(val_ceiling_works.getText().toString())/100)));
    		} else if (report_ceiling_works.getText().length()==0){
    			c_ceiling_works.setText("0.00");
    		}
        	if (report_ceiling_works.getText().toString().equals(".")){
        		report_ceiling_works.setText("0.00");
        	}
		}
		if (s == report_doors_windows.getEditableText()) {
        	if ((report_doors_windows.getText().length()!=0)&&(!report_doors_windows.getText().toString().equals("."))){
    			c_doors_windows.setText(String.valueOf(
    	        		gds.format(gds.StringtoDouble(report_doors_windows.getText().toString())
    	        		* gds.StringtoDouble(val_doors_windows.getText().toString())/100)));
    		} else if (report_doors_windows.getText().length()==0){
    			c_doors_windows.setText("0.00");
    		}
        	if (report_doors_windows.getText().toString().equals(".")){
        		report_doors_windows.setText("0.00");
        	}
		}
		if (s == report_plastering_wall.getEditableText()) {
        	if ((report_plastering_wall.getText().length()!=0)&&(!report_plastering_wall.getText().toString().equals("."))){
    			c_plastering_wall.setText(String.valueOf(
    	        		gds.format(gds.StringtoDouble(report_plastering_wall.getText().toString())
    	        		* gds.StringtoDouble(val_plastering_wall.getText().toString())/100)));
    		} else if (report_plastering_wall.getText().length()==0){
    			c_plastering_wall.setText("0.00");
    		}
        	if (report_plastering_wall.getText().toString().equals(".")){
        		report_plastering_wall.setText("0.00");
        	}
		}
		if (s == report_slab_include_finish.getEditableText()) {
        	if ((report_slab_include_finish.getText().length()!=0)&&(!report_slab_include_finish.getText().toString().equals("."))){
    			c_slab_include_finish.setText(String.valueOf(
    	        		gds.format(gds.StringtoDouble(report_slab_include_finish.getText().toString())
    	        		* gds.StringtoDouble(val_slab_include_finish.getText().toString())/100)));
    		} else if (report_slab_include_finish.getText().length()==0){
    			c_slab_include_finish.setText("0.00");
    		}
        	if (report_slab_include_finish.getText().toString().equals(".")){
        		report_slab_include_finish.setText("0.00");
        	}
		}
		if (s == report_painting_finishing.getEditableText()) {
        	if ((report_painting_finishing.getText().length()!=0)&&(!report_painting_finishing.getText().toString().equals("."))){
    			c_painting_finishing.setText(String.valueOf(
    	        		gds.format(gds.StringtoDouble(report_painting_finishing.getText().toString())
    	        		* gds.StringtoDouble(val_painting_finishing.getText().toString())/100)));
    		} else if (report_painting_finishing.getText().length()==0){
    			c_painting_finishing.setText("0.00");
    		}
        	if (report_painting_finishing.getText().toString().equals(".")){
        		report_painting_finishing.setText("0.00");
        	}
		}
		if (s == report_plumbing_elect_fix.getEditableText()) {
        	if ((report_plumbing_elect_fix.getText().length()!=0)&&(!report_plumbing_elect_fix.getText().toString().equals("."))){
    			c_plumbing_elect_fix.setText(String.valueOf(
    	        		gds.format(gds.StringtoDouble(report_plumbing_elect_fix.getText().toString())
    	        		* gds.StringtoDouble(val_plumbing_elect_fix.getText().toString())/100)));
    		} else if (report_plumbing_elect_fix.getText().length()==0){
    			c_plumbing_elect_fix.setText("0.00");
    		}
        	if (report_plumbing_elect_fix.getText().toString().equals(".")){
        		report_plumbing_elect_fix.setText("0.00");
        	}
		}
		if (s == report_utilities_tapping.getEditableText()) {
        	if ((report_utilities_tapping.getText().length()!=0)&&(!report_utilities_tapping.getText().toString().equals("."))){
    			c_utilities_tapping.setText(String.valueOf(
    	        		gds.format(gds.StringtoDouble(report_utilities_tapping.getText().toString())
    	        		* gds.StringtoDouble(val_utilities_tapping.getText().toString())/100)));
    		} else if (report_utilities_tapping.getText().length()==0){
    			c_utilities_tapping.setText("0.00");
    		}
        	if (report_utilities_tapping.getText().toString().equals(".")){
        		report_utilities_tapping.setText("0.00");
        	}
		}

		if ((s == report_foundation.getEditableText())
				|| (s == report_columns.getEditableText())
				|| (s == report_beams_girders.getEditableText())
				|| (s == report_surround_walls.getEditableText())
				|| (s == report_interior_partition.getEditableText())
				|| (s == report_roofing_works.getEditableText())
				|| (s == report_plumbing_rough_in.getEditableText())
				|| (s == report_electrical_rough_in.getEditableText())
				|| (s == report_ceiling_works.getEditableText())
				|| (s == report_doors_windows.getEditableText())
				|| (s == report_plastering_wall.getEditableText())
				|| (s == report_slab_include_finish.getEditableText())
				|| (s == report_painting_finishing.getEditableText())
				|| (s == report_plumbing_elect_fix.getEditableText())
				|| (s == report_utilities_tapping.getEditableText()))
		{
			if ((report_foundation.getText().length()!=0)
				|| (report_columns.getText().length()!=0)
				|| (report_beams_girders.getText().length()!=0)
				|| (report_surround_walls.getText().length()!=0)
				|| (report_interior_partition.getText().length()!=0)
				|| (report_roofing_works.getText().length()!=0)
				|| (report_plumbing_rough_in.getText().length()!=0)
				|| (report_electrical_rough_in.getText().length()!=0)
				|| (report_ceiling_works.getText().length()!=0)
				|| (report_doors_windows.getText().length()!=0)
				|| (report_plastering_wall.getText().length()!=0)
				|| (report_slab_include_finish.getText().length()!=0)
				|| (report_painting_finishing.getText().length()!=0)
				|| (report_plumbing_elect_fix.getText().length()!=0)
				|| (report_utilities_tapping.getText().length()!=0))
			{
				report_total.setText(
						gds.format(
								gds.StringtoDouble(c_foundation.getText().toString())
								+ gds.StringtoDouble(c_columns.getText().toString())
								+ gds.StringtoDouble(c_beams_girders.getText().toString())
								+ gds.StringtoDouble(c_surround_walls.getText().toString())
								+ gds.StringtoDouble(c_interior_partition.getText().toString())
								+ gds.StringtoDouble(c_roofing_works.getText().toString())
								+ gds.StringtoDouble(c_plumbing_rough_in.getText().toString())
								+ gds.StringtoDouble(c_electrical_rough_in.getText().toString())
								+ gds.StringtoDouble(c_ceiling_works.getText().toString())
								+ gds.StringtoDouble(c_doors_windows.getText().toString())
								+ gds.StringtoDouble(c_plastering_wall.getText().toString())
								+ gds.StringtoDouble(c_slab_include_finish.getText().toString())
								+ gds.StringtoDouble(c_painting_finishing.getText().toString())
								+ gds.StringtoDouble(c_plumbing_elect_fix.getText().toString())
								+ gds.StringtoDouble(c_utilities_tapping.getText().toString())
								));
			} else if ((report_foundation.getText().length()==0)
					&& (report_columns.getText().length()==0)
					&& (report_beams_girders.getText().length()==0)
					&& (report_surround_walls.getText().length()==0)
					&& (report_interior_partition.getText().length()==0)
					&& (report_roofing_works.getText().length()==0)
					&& (report_plumbing_rough_in.getText().length()==0)
					&& (report_electrical_rough_in.getText().length()==0)
					&& (report_ceiling_works.getText().length()==0)
					&& (report_doors_windows.getText().length()==0)
					&& (report_plastering_wall.getText().length()==0)
					&& (report_slab_include_finish.getText().length()==0)
					&& (report_painting_finishing.getText().length()==0)
					&& (report_plumbing_elect_fix.getText().length()==0)
					&& (report_utilities_tapping.getText().length()==0)){
				report_total.setText("0.00");
			}
		}
		total = report_total.getText().toString();
	}

	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();

	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}


}