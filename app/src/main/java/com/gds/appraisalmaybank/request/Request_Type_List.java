package com.gds.appraisalmaybank.request;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.check_network.NetworkUtil;
import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Request_Attachments;
import com.gds.appraisalmaybank.database.Request_Collateral_Address;
import com.gds.appraisalmaybank.database.Request_Contact_Persons;
import com.gds.appraisalmaybank.database.Request_Form;
import com.gds.appraisalmaybank.database.Request_Type;
import com.gds.appraisalmaybank.json.MyHttpClient;
import com.gds.appraisalmaybank.json.TLSConnection;
import com.gds.appraisalmaybank.json.UserFunctions;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;

import org.apache.http.NameValuePair;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class Request_Type_List extends ListActivity implements OnClickListener {

	// ArrayList thats going to hold the search results
	ArrayList<HashMap<String, Object>> searchResults;

	// ArrayList that will hold the original Data
	ArrayList<HashMap<String, Object>> originalValues;
	LayoutInflater inflater;
	String[] names;
	Integer[] photos;
	String[] id;
	String[] app_type_output;
	String item;
	int length = 0, i = 0;
	DatabaseHandler db = new DatabaseHandler(this);
	Global gs;
	TLSConnection tlscon = new TLSConnection();
	//for tls
	JSONObject jsonResponse = new JSONObject();
	ArrayList<String> field = new ArrayList<>();
	ArrayList<String> value = new ArrayList<>();
	GDS_methods gds = new GDS_methods();
	String dir = "gds_appraisal";
	Button rt_btn_add, rt_btn_send;
	String request_form_record;
	int db_id, rt_id;
	String rt_id2;
	String db_date_requested_month, db_date_requested_day,
			db_date_requested_year, db_classification, db_account_fname,
			db_account_mname, db_account_lname, db_requesting_party;
	String db_cntrl_no, db_request_appraisal, db_purpose_appraisal,
			db_pref_inspection_date1_month, db_pref_inspection_date1_day,
			db_pref_inspection_date1_year, db_app_pref_inspection_date2_month,
			db_app_pref_inspection_date2_day,
			db_app_pref_inspection_date2_year;
	String db_mv_year, db_mv_make, db_mv_model, db_mv_odometer, db_mv_vin,
			db_me_quantity, db_me_type, db_me_manufacturer, db_me_model,
			db_me_serial, db_me_age, db_me_condition;
	String db_tct_no, db_unit_no, db_building_name, db_street_no,
			db_street_name, db_village, db_barangay, db_zip_code, db_city,
			db_province, db_region, db_country;
	String db_app_contact_person_name, db_app_mobile_prefix, db_app_mobile_no,
			db_app_telephone_no;
	String db_app_uid, db_app_file, db_app_filename, db_app_appraisal_type,
			db_app_candidate_done;
	ProgressDialog pDialog;
	String[] commandArray = new String[] { "View", "Edit", "Delete" };
	int serverResponseCode = 0;
	String output_appraisal_type;
	// for api
	String xml;
	DefaultHttpClient httpClient = new MyHttpClient(this);
	String query;
	Context context = this;
	String google_response = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.request_type_list);
		ListView playerListView = (ListView) findViewById(android.R.id.list);
		gs = ((Global) getApplicationContext());
		rt_btn_add = (Button) findViewById(R.id.rt_btn_add);
		rt_btn_send = (Button) findViewById(R.id.rt_btn_send);
		rt_btn_add.setOnClickListener(this);
		rt_btn_send.setOnClickListener(this);
		request_form_record = gs.request_form_record;
		db_id = Integer.parseInt(request_form_record);

		// onclickitem
		playerListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				arg1.setSelected(true);
				item = ((TextView) arg1.findViewById(R.id.txt_image_list))
						.getText().toString();
				String extension = id[arg2];
				if (extension.equals("")) {
					Toast.makeText(getApplicationContext(), "no record found",
							Toast.LENGTH_SHORT).show();
				} else {
					gs.request_form_record = extension;
					view_edit_delete();
				}

			}

		});
		// get the LayoutInflater for inflating the customomView
		// this will be used in the custom adapter
		inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		Log.d("Reading: ", "Count type..");

		length = db.getRequest_TypeCount(request_form_record);
		// initialize the array
		names = new String[length];
		app_type_output = new String[length];
		photos = new Integer[length];
		id = new String[length];
		Log.d("Reading: ", "Reading type..");
		List<Request_Type> request_type = db
				.getAllRequest_Type(request_form_record);
		if (!request_type.isEmpty()) {
			for (Request_Type im : request_type) {
				names[i] = im.getrt_appraisal_type();
				for (int x = 0; x < gs.appraisal_type_value.length; x++) {
					if (im.getrt_appraisal_type().equals(
							gs.appraisal_type_value[x])) {
						output_appraisal_type = gs.appraisal_output[x];
					}
				}
				app_type_output[i] = output_appraisal_type;
				photos[i] = R.drawable.monotoneright_white;
				id[i] = String.valueOf(im.getID());
				i++;
			}

		} else {
			names = new String[1];
			app_type_output = new String[1];
			photos = new Integer[1];
			id = new String[1];

			names[0] = "no record found";
			app_type_output[0] = "no record found";
			photos[0] = R.drawable.monotoneright_white;
			id[0] = "";
			rt_btn_send.setEnabled(false);
		}
		originalValues = new ArrayList<HashMap<String, Object>>();

		// temporary HashMap for populating the
		// Items in the ListView
		HashMap<String, Object> temp;

		// total number of rows in the ListView
		int noOfPlayers = names.length;

		// now populate the ArrayList players
		for (int i = 0; i < noOfPlayers; i++) {
			temp = new HashMap<String, Object>();

			temp.put("name", app_type_output[i]);
			temp.put("photo", photos[i]);

			// add the row to the ArrayList
			originalValues.add(temp);
		}

		// searchResults=OriginalValues initially
		searchResults = new ArrayList<HashMap<String, Object>>(originalValues);

		// create the adapter
		// first param-the context
		// second param-the id of the layout file
		// you will be using to fill a row
		// third param-the set of values that
		// will populate the ListView
		final CustomAdapter adapter = new CustomAdapter(this,
				R.layout.request_type_list_layout, searchResults);

		// finally,set the adapter to the default ListView
		playerListView.setAdapter(adapter);

	}

	// define your custom adapter
	private class CustomAdapter extends ArrayAdapter<HashMap<String, Object>> {

		public CustomAdapter(Context context, int textViewResourceId,
				ArrayList<HashMap<String, Object>> Strings) {

			// let android do the initializing :)
			super(context, textViewResourceId, Strings);
		}

		// class for caching the views in a row
		private class ViewHolder {
			ImageView photo;
			TextView name;

		}

		ViewHolder viewHolder;

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			if (convertView == null) {
				convertView = inflater.inflate(
						R.layout.request_type_list_layout, null);
				viewHolder = new ViewHolder();

				// cache the views
				viewHolder.photo = (ImageView) convertView
						.findViewById(R.id.mono);
				viewHolder.name = (TextView) convertView
						.findViewById(R.id.txt_image_list);

				// link the cached views to the convertview
				convertView.setTag(viewHolder);

			} else
				viewHolder = (ViewHolder) convertView.getTag();

			int photoId = (Integer) searchResults.get(position).get("photo");

			// set the data to be displayed
			viewHolder.photo.setImageDrawable(getResources().getDrawable(
					photoId));
			viewHolder.name.setText(searchResults.get(position).get("name")
					.toString());
			// return the view to be displayed
			return convertView;
		}

	}

	public void get_db_request() {
		Request_Form request_form = db.getRequest_Form(db_id);
		if (request_form != null) {
			db_id = request_form.getID();
			db_date_requested_month = request_form.getrf_date_requested_month();
			db_date_requested_day = request_form.getrf_date_requested_day();
			db_date_requested_year = request_form.getrf_date_requested_year();
			db_classification = request_form.getrf_classification();
			db_account_fname = request_form.getrf_account_fname();
			db_account_mname = request_form.getrf_account_mname();
			db_account_lname = request_form.getrf_account_lname();
			db_requesting_party = request_form.getrf_requesting_party();

		}
	}

	public void get_db_collateral_address() {
		Request_Collateral_Address collateral_address = db
				.getCollateral_Address(rt_id);
		if (collateral_address != null) {
			db_tct_no = collateral_address.getrq_tct_no();
			db_unit_no = collateral_address.getrq_unit_no();
			db_building_name = collateral_address.getrq_building_name();
			db_street_no = collateral_address.getrq_street_no();
			db_street_name = collateral_address.getrq_street_name();
			db_village = collateral_address.getrq_village();
			db_barangay = collateral_address.getrq_barangay();
			db_zip_code = collateral_address.getrq_zip_code();
			db_city = collateral_address.getrq_city();
			db_province = collateral_address.getrq_province();
			db_region = collateral_address.getrq_region();
			db_country = collateral_address.getrq_country();

		}
	}

	public void create_json() {
		JSONObject record_temp = new JSONObject();
		JSONObject request_form = new JSONObject();
		JSONArray appraisal_request = new JSONArray();
		JSONArray app_contact_person = new JSONArray();
		JSONArray app_attachments = new JSONArray();
		JSONObject appraisal_attachments = new JSONObject();
		JSONObject appraisal_attachments_data = new JSONObject();
		JSONObject appraisal_contact_person = new JSONObject();
		JSONObject appraisal_type = new JSONObject();
		get_db_request();
		try {
			// request form
			request_form.put("application_status", "appraisal_request_mobile");
			request_form
					.put("app_daterequested_month", db_date_requested_month);
			request_form.put("app_daterequested_day", db_date_requested_day);
			request_form.put("app_daterequested_year", db_date_requested_year);
			request_form.put("app_endorsed", db_classification);
			request_form.put("app_account_first_name", db_account_fname);
			request_form.put("app_account_middle_name", db_account_mname);
			request_form.put("app_account_last_name", db_account_lname);
			request_form.put("app_requesting_party", db_requesting_party);
			request_form.put("appraisal_request", appraisal_request);
			// for request type for contact person
			List<Request_Type> request_type = db
					.getAllRequest_Type(request_form_record);
			if (!request_type.isEmpty()) {
				for (Request_Type im : request_type) {
					rt_id = im.getID();
					db_cntrl_no = im.getrt_control_no();
					db_request_appraisal = im.getrt_appraisal_type();
					db_purpose_appraisal = im.getrt_nature_of_appraisal();
					db_pref_inspection_date1_month = im
							.getrt_pref_ins_date1_month();
					db_pref_inspection_date1_day = im
							.getrt_pref_ins_date1_day();
					db_pref_inspection_date1_year = im
							.getrt_pref_ins_date1_year();
					db_app_pref_inspection_date2_month = im
							.getrt_pref_ins_date2_month();
					db_app_pref_inspection_date2_day = im
							.getrt_pref_ins_date2_day();
					db_app_pref_inspection_date2_year = im
							.getrt_pref_ins_date2_year();

					db_mv_year = im.getrt_mv_year();
					db_mv_make = im.getrt_mv_make();
					db_mv_model = im.getrt_mv_model();
					db_mv_odometer = im.getrt_mv_odometer();
					db_mv_vin = im.getrt_mv_vin();
					db_me_quantity = im.getrt_me_quantity();
					db_me_type = im.getrt_me_type();
					db_me_manufacturer = im.getrt_me_manufacturer();
					db_me_model = im.getrt_me_model();
					db_me_serial = im.getrt_me_serial();
					db_me_age = im.getrt_me_age();
					db_me_condition = im.getrt_me_condition();

					appraisal_type = new JSONObject();
					appraisal_type.put("app_control_no", "");
					appraisal_type.put("app_request_appraisal",
							db_request_appraisal);
					appraisal_type.put("app_purpose_appraisal",
							db_purpose_appraisal);
					appraisal_type.put("app_application_reference",
							db_pref_inspection_date1_month);
					appraisal_type.put("app_pref_inspection_date1_day",
							db_pref_inspection_date1_day);
					appraisal_type.put("app_pref_inspection_date1_year",
							db_pref_inspection_date1_year);
					appraisal_type.put("app_pref_inspection_date2_month",
							db_app_pref_inspection_date2_month);
					appraisal_type.put("app_pref_inspection_date2_day",
							db_app_pref_inspection_date2_day);
					appraisal_type.put("app_pref_inspection_date2_year",
							db_app_pref_inspection_date2_year);
					// mv/mv
					appraisal_type.put("app_vehicle_type", db_mv_make);
					appraisal_type.put("app_car_year", db_mv_year);
					appraisal_type.put("app_car_vin", db_mv_vin);
					appraisal_type.put("app_car_model", db_mv_model);
					appraisal_type.put("app_car_odometer_reading",
							db_mv_odometer);

					appraisal_type.put("app_me_quantity", db_me_quantity);
					appraisal_type.put("app_me_manufacturer",
							db_me_manufacturer);
					appraisal_type.put("app_me_serial_no", db_me_serial);
					appraisal_type.put("app_me_condition", db_me_condition);
					appraisal_type.put("app_me_type", db_me_type);
					appraisal_type.put("app_me_model", db_me_model);
					appraisal_type.put("app_me_age", db_me_age);

					rt_id2 = String.valueOf(rt_id);
					// for request type
					List<Request_Contact_Persons> request_contact_persons = db
							.getAllRequest_Contact_Persons(rt_id2);
					if (!request_contact_persons.isEmpty()) {
						app_contact_person = new JSONArray();
						for (Request_Contact_Persons ct : request_contact_persons) {
							db_app_contact_person_name = ct
									.getrq_contact_person();
							db_app_mobile_prefix = ct
									.getrq_contact_mobile_no_prefix();
							db_app_mobile_no = ct.getrq_contact_mobile_no();
							db_app_telephone_no = ct.getrq_contact_landline();

							appraisal_contact_person = new JSONObject();

							appraisal_contact_person.put(
									"app_contact_person_name",
									db_app_contact_person_name);
							appraisal_contact_person.put("app_mobile_prefix",
									db_app_mobile_prefix);
							appraisal_contact_person.put("app_mobile_no",
									db_app_mobile_no);
							appraisal_contact_person.put("app_telephone_no",
									db_app_telephone_no);

							app_contact_person.put(appraisal_contact_person);

							appraisal_type.put("app_contact_person",
									app_contact_person);
						}
					}

					get_db_collateral_address();
					JSONObject app_collateral_address = new JSONObject();
					app_collateral_address.put("app_tct_no", db_tct_no);
					app_collateral_address.put("app_unit_no", db_unit_no);
					app_collateral_address.put("app_bldg", db_building_name);
					app_collateral_address.put("app_street_no", db_street_no);
					app_collateral_address.put("app_street_name",
							db_street_name);
					app_collateral_address.put("app_village", db_village);
					app_collateral_address.put("app_district", db_barangay);
					app_collateral_address.put("app_zip", db_zip_code);
					app_collateral_address.put("app_city", db_city);
					app_collateral_address.put("app_province", db_province);
					app_collateral_address.put("app_region", db_region);
					app_collateral_address.put("app_country", db_country);
					appraisal_type.put("app_collateral_address",
							app_collateral_address);

					appraisal_request.put(appraisal_type);

					// attachments
					List<Request_Attachments> request_attachments = db
							.getAllRequest_Attachments(rt_id2);
					if (!request_attachments.isEmpty()) {
						for (Request_Attachments attachments : request_attachments) {
							db_app_uid = attachments.getrq_uid();
							db_app_file = attachments.getrq_file();
							db_app_filename = attachments.getrq_filename();
							db_app_appraisal_type = attachments
									.getrq_appraisal_type();
							db_app_candidate_done = attachments
									.getrq_candidate_done();

							appraisal_attachments = new JSONObject();
							appraisal_attachments.put("uid", db_app_uid);
							appraisal_attachments.put("file", db_app_file);
							appraisal_attachments.put("filename",
									db_app_filename);
							appraisal_attachments.put("appraisal_type",
									db_app_appraisal_type);
							appraisal_attachments.put("candidate_done",
									db_app_candidate_done);
							app_attachments.put(appraisal_attachments);
							appraisal_attachments_data.put("attachments",
									app_attachments);

						}
					}
				}

			}

			// for whole record
			record_temp.put("record", request_form);
			String record = record_temp.toString();
			String attachment = appraisal_attachments_data.toString();
			Log.e("", record.toString());
			Log.e("", attachment.toString());
			UserFunctions userFunction = new UserFunctions();
			// userFunction.SendCaseCenter(record);
			userFunction.SendCaseCenter_Attachments(attachment);

			// for API
			//for tls
			field.clear();
			field.add("auth_token");
			field.add("data");

			value.clear();
			value.add(gs.auth_token);
			value.add(record);

			List<NameValuePair> params = new ArrayList<NameValuePair>();

			if(Global.type.contentEquals("tls")){
				jsonResponse = gds.makeHttpsRequest(gs.create_url, "POST", field, value);
			}else{
				params.add(new BasicNameValuePair("auth_token", gs.auth_token));
				params.add(new BasicNameValuePair("data", record));
				// getting JSON string from URL
				jsonResponse = gds.makeHttpRequest(gs.create_url, "POST", params);
			}
			xml = jsonResponse.toString();
			/*HttpPost httpPost = new HttpPost(gs.create_url);
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("auth_token", gs.auth_token));
			params.add(new BasicNameValuePair("data", record));
			httpPost.setEntity(new UrlEncodedFormEntity(params));
			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity httpEntity = response.getEntity();
			xml = EntityUtils.toString(httpEntity);*/
			Log.e("", xml);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void upload_attachment() {

		List<Request_Type> request_type = db
				.getAllRequest_Type(request_form_record);
		if (!request_type.isEmpty()) {
			for (Request_Type im : request_type) {
				rt_id = im.getID();
				// attachments
				List<Request_Attachments> request_attachments = db
						.getAllRequest_Attachments(String.valueOf(rt_id));
				if (!request_attachments.isEmpty()) {
					for (Request_Attachments attachments : request_attachments) {
						db_app_uid = attachments.getrq_uid();
						db_app_file = attachments.getrq_file();
						db_app_filename = attachments.getrq_filename();
						db_app_appraisal_type = attachments
								.getrq_appraisal_type();
						db_app_candidate_done = attachments
								.getrq_candidate_done();
						// upload pdf
						uploadFile(Environment.getExternalStorageDirectory()
								+ "/" + dir + "/" + db_app_filename);
					}
				}
			}
		}

	}

	public void delete_request_type() {
		// delete record on request type
		Request_Type request_type1 = new Request_Type();
		request_type1.setID(Integer.parseInt(gs.request_form_record));
		db.deleteRequest_Type_byID(request_type1);
		db.close();

		// delete collateral address
		Request_Collateral_Address collateral_address = new Request_Collateral_Address();
		collateral_address.setID(Integer.parseInt(gs.request_form_record));
		db.deleteRequest_Collateral_Address_byID(collateral_address);
		db.close();

		// delete all contact person
		Request_Contact_Persons request_contact_persons = new Request_Contact_Persons();
		request_contact_persons.setrt_id(gs.request_form_record);
		db.deleteRequest_Contact_Persons(request_contact_persons);
		db.close();
	}

	private void delete() {
		// TODO Auto-generated method stub
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:
					// Yes button clicked
					delete_request_type();
					Toast.makeText(getApplicationContext(),
							"Request Type Deleted", Toast.LENGTH_SHORT).show();
					finish();
					break;

				case DialogInterface.BUTTON_NEGATIVE:
					// No button clicked
					break;
				}
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Delete this file?")
				.setPositiveButton("Yes", dialogClickListener)
				.setNegativeButton("No", dialogClickListener).show();

	}

	private class SendData extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(Request_Type_List.this);
			pDialog.setMessage("Sending data..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.setCanceledOnTouchOutside(false);
			pDialog.show();
		}

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			NetworkUtil.getConnectivityStatusString(getApplicationContext());
			if (NetworkUtil.status.equals("Network not available")) {
				return false;
			} else {
				google_response=gds.google_ping();
				if (google_response.equals("200")) {
					create_json();
					upload_attachment();
					return true;
				}
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			pDialog.dismiss();
			if (result.equals(true)) {
				if (google_response.equals("200")) {
					Toast.makeText(getApplicationContext(),
							"Successfully Registered", Toast.LENGTH_LONG)
							.show();
				} else {
					Toast.makeText(getApplicationContext(),
							"Network not available", Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(getApplicationContext(),
						"Network not available", Toast.LENGTH_SHORT).show();
			}

		}

	}

	public void view_edit_delete() {
		android.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Action");
		builder.setItems(commandArray, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int index) {
				if (index == 0) {
					startActivity(new Intent(Request_Type_List.this,
							View_Request_Type.class));
					dialog.dismiss();
				} else if (index == 1) {
					startActivity(new Intent(Request_Type_List.this,
							Edit_Request_Type.class));
					finish();
					dialog.dismiss();
				} else if (index == 2) {
					delete();
				}
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public int uploadFile(String sourceFileUri) {
		String upLoadServerUri = "http://dv-dev.gdslinkasia.com:2999/appraisal_attachment_api/upload_media_test.php";
		String fileName = sourceFileUri;

		HttpsURLConnection conn = null;
		HttpURLConnection conn2=null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		File sourceFile = new File(sourceFileUri);
		if (!sourceFile.isFile()) {
			Log.e("uploadFile", "Source File Does not exist");
			return 0;
		}
		try { // open a URL connection to the Servlet
			FileInputStream fileInputStream = new FileInputStream(sourceFile);
			URL url = new URL(upLoadServerUri);
			if(Global.type.contentEquals("tls")){

				conn = tlscon.setUpHttpsConnection(""+url);

				conn.setDoInput(true); // Allow Inputs
				conn.setDoOutput(true); // Allow Outputs
				conn.setUseCaches(false); // Don't use a Cached Copy
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Connection", "Keep-Alive");
				conn.setRequestProperty("ENCTYPE", "multipart/form-data");
				conn.setRequestProperty("Content-Type",
						"multipart/form-data;boundary=" + boundary);
				conn.setRequestProperty("uploaded_file", fileName);
				dos = new DataOutputStream(conn.getOutputStream());
			}else{

				conn2 = (HttpURLConnection) url.openConnection();
				conn2.setDoInput(true); // Allow Inputs
				conn2.setDoOutput(true); // Allow Outputs
				conn2.setUseCaches(false); // Don't use a Cached Copy
				conn2.setRequestMethod("POST");
				conn2.setRequestProperty("Connection", "Keep-Alive");
				conn2.setRequestProperty("ENCTYPE", "multipart/form-data");
				conn2.setRequestProperty("Content-Type",
						"multipart/form-data;boundary=" + boundary);
				conn2.setRequestProperty("uploaded_file", fileName);
				dos = new DataOutputStream(conn2.getOutputStream());
			}

			dos.writeBytes(twoHyphens + boundary + lineEnd);
			dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
					+ fileName + "\"" + lineEnd);
			dos.writeBytes(lineEnd);

			bytesAvailable = fileInputStream.available(); // create a buffer of
															// maximum size

			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];

			// read file and write it into form...
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);

			while (bytesRead > 0) {
				dos.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}

			// send multipart form data necesssary after file data...
			dos.writeBytes(lineEnd);
			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			// Responses from the server (code and message)
			String serverResponseMessage="";
			if(Global.type.contentEquals("tls")){
				serverResponseCode = conn.getResponseCode();
				serverResponseMessage = conn.getResponseMessage();
			}else{
				serverResponseCode = conn2.getResponseCode();
				serverResponseMessage = conn2.getResponseMessage();
			}

			Log.i("uploadFile", "HTTP Response is : " + serverResponseMessage
					+ ": " + serverResponseCode);
			if (serverResponseCode == 200) {
				runOnUiThread(new Runnable() {
					public void run() {
						Log.e("", db_app_filename.toString());
					}
				});
			}

			// close the streams //
			fileInputStream.close();
			dos.flush();
			dos.close();

		} catch (MalformedURLException ex) {
			ex.printStackTrace();
			Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("Upload file to server Exception",
					"Exception : " + e.getMessage(), e);
		}
		return serverResponseCode;
	}

	public void update_dialog() {
		// custom dialog
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.yes_no_dialog);

		// set the custom dialog components - text, image and button
		TextView text = (TextView) dialog.findViewById(R.id.tv_question);
		text.setText("Are you sure you want to submit ?");
		Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
		Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

		// if button is clicked, close the custom dialog
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new SendData().execute();
				dialog.dismiss();
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	/*public void google_ping() {
		try {
			URL obj = new URL("https://www.google.com.ph/?gws_rd=cr&ei=td9kUrP0H8mjigenuoHAAg");
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");

			// add request header
			con.setRequestProperty("User-Agent", "Mozilla/5.0");

			google_response = String.valueOf(con.getResponseCode());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.rt_btn_add:
			gs.request_type_index = i;
			startActivity(new Intent(Request_Type_List.this,
					Request_Type_add.class));
			finish();
			break;
		case R.id.rt_btn_send:
			update_dialog();
			break;

		default:
			break;

		}
	}
}