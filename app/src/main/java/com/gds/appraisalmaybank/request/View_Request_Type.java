package com.gds.appraisalmaybank.request;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Request_Type;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;

public class View_Request_Type extends Activity implements OnClickListener {
	EditText rt_cntrl_no, rt_et_vehicle_type, rt_et_car_model;
	Spinner rt_spinner_nature_appraisal, rt_spinner_appraisal_type;
	DatePicker rt_dp_inspection_date1, rt_dp_inspection_date2;
	TextView rt_tv_vehicle_type, rt_tv_car_model;
	Button rt_btn_next;
	Global gs;
	int request_form_record;
	DatabaseHandler db = new DatabaseHandler(this);

	String db_cntrl_no, db_nature_appraisal, db_appraisal_type,
			db_pref_inspection_date1_month, db_pref_inspection_date1_day,
			db_pref_inspection_date1_year, db_pref_inspection_date2_month,
			db_pref_inspection_date2_day, db_pref_inspection_date2_year,
			db_vehicle_type, db_car_model;
	int month1, year1, day1, month2, year2, day2;
	int classification_position;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.request_type);
		gs = ((Global) getApplicationContext());
		rt_cntrl_no = (EditText) findViewById(R.id.rt_cntrl_no);
		rt_dp_inspection_date1 = (DatePicker) findViewById(R.id.rt_dp_inspection_date1);
		rt_dp_inspection_date2 = (DatePicker) findViewById(R.id.rt_dp_inspection_date2);
		rt_spinner_nature_appraisal = (Spinner) findViewById(R.id.rt_spinner_nature_appraisal);
		rt_spinner_appraisal_type = (Spinner) findViewById(R.id.rt_spinner_appraisal_type);

		rt_btn_next = (Button) findViewById(R.id.rt_btn_next);
		rt_cntrl_no.setEnabled(false);
		rt_et_vehicle_type.setEnabled(false);
		rt_et_car_model.setEnabled(false);
		rt_dp_inspection_date1.setEnabled(false);
		rt_dp_inspection_date2.setEnabled(false);
		rt_spinner_nature_appraisal.setEnabled(false);
		rt_spinner_appraisal_type.setEnabled(false);
		rt_btn_next.setOnClickListener(this);

		request_form_record = Integer.parseInt(gs.request_form_record);
		set_data();

	}

	public void get_db_request() {
		Request_Type request_type = db.getRequest_Type(request_form_record);
		if (request_type != null) {
			db_cntrl_no = request_type.getrt_control_no();
			db_nature_appraisal = request_type.getrt_nature_of_appraisal();
			db_appraisal_type = request_type.getrt_appraisal_type();
			db_pref_inspection_date1_month = request_type
					.getrt_pref_ins_date1_month();
			db_pref_inspection_date1_day = request_type
					.getrt_pref_ins_date1_day();
			db_pref_inspection_date1_year = request_type
					.getrt_pref_ins_date1_year();
			db_pref_inspection_date2_month = request_type
					.getrt_pref_ins_date2_month();
			db_pref_inspection_date2_day = request_type
					.getrt_pref_ins_date2_day();
			db_pref_inspection_date2_year = request_type
					.getrt_pref_ins_date2_year();

		}
	}

	public void set_data() {
		get_db_request();
		rt_cntrl_no.setText(db_cntrl_no);
		// nature of appraisal
		for (int x = 0; x < gs.appraisal_type_nature_appraisal.length; x++) {
			if (gs.appraisal_type_nature_appraisal[x]
					.equals(db_nature_appraisal)) {
				classification_position = x;
			}
		}
		rt_spinner_nature_appraisal.setSelection(classification_position);
		// appraisal type
		for (int x = 0; x < gs.appraisal_type_value.length; x++) {
			if (gs.appraisal_type_value[x].equals(db_appraisal_type)) {
				classification_position = x;
			}
		}
		rt_spinner_appraisal_type.setSelection(classification_position);
		year1 = Integer.parseInt(db_pref_inspection_date1_year);
		month1 = Integer.parseInt(db_pref_inspection_date1_month) - 1;
		day1 = Integer.parseInt(db_pref_inspection_date1_day);

		year2 = Integer.parseInt(db_pref_inspection_date2_year);
		month2 = Integer.parseInt(db_pref_inspection_date2_month) - 1;
		day2 = Integer.parseInt(db_pref_inspection_date2_day);
		// set date into datepicker
		rt_dp_inspection_date1.init(year1, month1, day1, null);
		rt_dp_inspection_date2.init(year2, month2, day2, null);

		rt_et_vehicle_type.setText(db_vehicle_type);
		rt_et_car_model.setText(db_car_model);
		gs.appraisal_type = db_appraisal_type.toString();
		if (db_appraisal_type.equals("motor_vehicle")) {
			rt_et_vehicle_type.setVisibility(View.VISIBLE);
			rt_et_car_model.setVisibility(View.VISIBLE);
			rt_tv_vehicle_type.setVisibility(View.VISIBLE);
			rt_tv_car_model.setVisibility(View.VISIBLE);
		} else {
			rt_et_vehicle_type.setVisibility(View.GONE);
			rt_et_car_model.setVisibility(View.GONE);
			rt_tv_vehicle_type.setVisibility(View.GONE);
			rt_tv_car_model.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.rt_btn_next:
			startActivity(new Intent(View_Request_Type.this,
					View_Request_Type2.class));
			finish();
			break;

		default:
			break;

		}
	}

}
