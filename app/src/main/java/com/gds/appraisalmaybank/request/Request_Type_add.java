package com.gds.appraisalmaybank.request;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableRow;

import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;

public class Request_Type_add extends Activity implements OnClickListener {
	Global gs;
	EditText rt_cntrl_no, rt_et_mv_year, rt_et_mv_make, rt_et_mv_model,
			rt_et_mv_odometer, rt_et_mv_vin, rt_et_me_quantity, rt_et_me_type,
			rt_et_me_manufacturer, rt_et_me_model, rt_et_me_serial,
			rt_et_me_age, rt_et_me_condition;
	TableRow tr_mv_year, tr_mv_make, tr_mv_model, tr_mv_odometer, tr_mv_vin,
			tr_me_quantity, tr_me_type, tr_me_manufacturer, tr_me_model,
			tr_me_serial, tr_me_age, tr_me_condition;
	Spinner rt_spinner_nature_appraisal, rt_spinner_appraisal_type;
	DatePicker rt_dp_inspection_date1, rt_dp_inspection_date2;
	Button rt_btn_next;
	String rf_id, cntrol_no, nature_appraisal, appraisal_type, amount,
			inspection_date1_month, inspection_date1_day,
			inspection_date1_year, inspection_date2_month,
			inspection_date2_day, inspection_date2_year;
	int appraisal_type_position;
	String mv_year, mv_make, mv_model, mv_odometer, mv_vin, me_quantity,
			me_type, me_manufacturer, me_model, me_serial, me_age,
			me_condition;
	public static Request_Type_add instance = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		instance = this;
		setContentView(R.layout.request_type);
		gs = ((Global) getApplicationContext());
		rt_cntrl_no = (EditText) findViewById(R.id.rt_cntrl_no);
		rt_et_mv_year = (EditText) findViewById(R.id.rt_et_mv_year);
		rt_et_mv_make = (EditText) findViewById(R.id.rt_et_mv_make);
		rt_et_mv_model = (EditText) findViewById(R.id.rt_et_mv_model);
		rt_et_mv_odometer = (EditText) findViewById(R.id.rt_et_mv_odometer);
		rt_et_mv_vin = (EditText) findViewById(R.id.rt_et_mv_vin);
		rt_et_me_quantity = (EditText) findViewById(R.id.rt_et_me_quantity);
		rt_et_me_type = (EditText) findViewById(R.id.rt_et_me_type);
		rt_et_me_manufacturer = (EditText) findViewById(R.id.rt_et_me_manufacturer);
		rt_et_me_model = (EditText) findViewById(R.id.rt_et_me_model);
		rt_et_me_serial = (EditText) findViewById(R.id.rt_et_me_serial);
		rt_et_me_age = (EditText) findViewById(R.id.rt_et_me_age);
		rt_et_me_condition = (EditText) findViewById(R.id.rt_et_me_condition);
		rt_btn_next = (Button) findViewById(R.id.rt_btn_next);
		tr_mv_year = (TableRow) findViewById(R.id.tr_mv_year);
		tr_mv_make = (TableRow) findViewById(R.id.tr_mv_make);
		tr_mv_model = (TableRow) findViewById(R.id.tr_mv_model);
		tr_mv_odometer = (TableRow) findViewById(R.id.tr_mv_odometer);
		tr_mv_vin = (TableRow) findViewById(R.id.tr_mv_vin);
		tr_me_quantity = (TableRow) findViewById(R.id.tr_me_quantity);
		tr_me_type = (TableRow) findViewById(R.id.tr_me_type);
		tr_me_manufacturer = (TableRow) findViewById(R.id.tr_me_manufacturer);
		tr_me_model = (TableRow) findViewById(R.id.tr_me_model);
		tr_me_serial = (TableRow) findViewById(R.id.tr_me_serial);
		tr_me_age = (TableRow) findViewById(R.id.tr_me_age);
		tr_me_condition = (TableRow) findViewById(R.id.tr_me_condition);
		rt_spinner_nature_appraisal = (Spinner) findViewById(R.id.rt_spinner_nature_appraisal);
		rt_spinner_appraisal_type = (Spinner) findViewById(R.id.rt_spinner_appraisal_type);
		rt_dp_inspection_date1 = (DatePicker) findViewById(R.id.rt_dp_inspection_date1);
		rt_dp_inspection_date2 = (DatePicker) findViewById(R.id.rt_dp_inspection_date2);
		rt_btn_next.setOnClickListener(this);
		rt_spinner_appraisal_type
				.setOnItemSelectedListener(new OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView<?> parentView,
							View selectedItemView, int position, long id) {
						// your code here
						if (rt_spinner_appraisal_type.getSelectedItem()
								.toString().equals("Motor Vehicle")) {
							tr_mv_year.setVisibility(View.VISIBLE);
							tr_mv_make.setVisibility(View.VISIBLE);
							tr_mv_model.setVisibility(View.VISIBLE);
							tr_mv_odometer.setVisibility(View.VISIBLE);
							tr_mv_vin.setVisibility(View.VISIBLE);

						} else {
							tr_mv_year.setVisibility(View.GONE);
							tr_mv_make.setVisibility(View.GONE);
							tr_mv_model.setVisibility(View.GONE);
							tr_mv_odometer.setVisibility(View.GONE);
							tr_mv_vin.setVisibility(View.GONE);
						}

						if (rt_spinner_appraisal_type.getSelectedItem()
								.toString().equals("Machinery and Equipment")) {
							tr_me_quantity.setVisibility(View.VISIBLE);
							tr_me_type.setVisibility(View.VISIBLE);
							tr_me_manufacturer.setVisibility(View.VISIBLE);
							tr_me_model.setVisibility(View.VISIBLE);
							tr_me_serial.setVisibility(View.VISIBLE);
							tr_me_age.setVisibility(View.VISIBLE);
							tr_me_condition.setVisibility(View.VISIBLE);
						} else {
							tr_me_quantity.setVisibility(View.GONE);
							tr_me_type.setVisibility(View.GONE);
							tr_me_manufacturer.setVisibility(View.GONE);
							tr_me_model.setVisibility(View.GONE);
							tr_me_serial.setVisibility(View.GONE);
							tr_me_age.setVisibility(View.GONE);
							tr_me_condition.setVisibility(View.GONE);
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> parentView) {
						// your code here
					}

				});

	}

	public void get_value() {
		rf_id = gs.request_form_record;
		cntrol_no = rt_cntrl_no.getText().toString();
		nature_appraisal = rt_spinner_nature_appraisal.getSelectedItem()
				.toString();
		appraisal_type = gs.appraisal_type_value[rt_spinner_appraisal_type
				.getSelectedItemPosition()];
		inspection_date1_month = String.valueOf(rt_dp_inspection_date1
				.getMonth() + 1);
		inspection_date1_day = String.valueOf(rt_dp_inspection_date1
				.getDayOfMonth());
		inspection_date1_year = String
				.valueOf(rt_dp_inspection_date1.getYear());
		inspection_date2_month = String.valueOf(rt_dp_inspection_date2
				.getMonth() + 1);
		inspection_date2_day = String.valueOf(rt_dp_inspection_date2
				.getDayOfMonth());
		inspection_date2_year = String
				.valueOf(rt_dp_inspection_date2.getYear());
		mv_year = rt_et_mv_year.getText().toString();
		mv_make = rt_et_mv_make.getText().toString();
		mv_model = rt_et_mv_model.getText().toString();
		mv_odometer = rt_et_mv_odometer.getText().toString();
		mv_vin = rt_et_mv_vin.getText().toString();
		me_quantity = rt_et_me_quantity.getText().toString();
		me_type = rt_et_me_type.getText().toString();
		me_manufacturer = rt_et_me_manufacturer.getText().toString();
		me_model = rt_et_me_model.getText().toString();
		me_serial = rt_et_me_serial.getText().toString();
		me_age = rt_et_me_age.getText().toString();
		me_condition = rt_et_me_condition.getText().toString();
	}

	public void value_global() {
		get_value();
		gs.rf_id = rf_id;
		gs.cntrol_no = cntrol_no;
		gs.nature_appraisal = nature_appraisal;
		gs.appraisal_type = appraisal_type;
		if (inspection_date1_month.length() == 1) {
			gs.inspection_date1_month = "0" + inspection_date1_month;
		} else {
			gs.inspection_date1_month = inspection_date1_month;
		}
		gs.inspection_date1_day = inspection_date1_day;
		gs.inspection_date1_year = inspection_date1_year;
		if (inspection_date2_month.length() == 1) {
			gs.inspection_date2_month = "0" + inspection_date2_month;
		} else {
			gs.inspection_date2_month = inspection_date2_month;
		}
		gs.inspection_date2_day = inspection_date2_day;
		gs.inspection_date2_year = inspection_date2_year;
		gs.mv_year = mv_year;
		gs.mv_make = mv_make;
		gs.mv_model = mv_model;
		gs.mv_odometer = mv_odometer;
		gs.mv_vin = mv_vin;
		gs.me_quantity = me_quantity;
		gs.me_type = me_type;
		gs.me_manufacturer = me_manufacturer;
		gs.me_model = me_model;
		gs.me_serial = me_serial;
		gs.me_age = me_age;
		gs.me_condition = me_condition;
	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub
		super.finish();
		instance = null;
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.rt_btn_next:
			value_global();
			startActivity(new Intent(Request_Type_add.this,
					Request_Type2_add.class));
			break;

		default:
			break;

		}
	}

}
