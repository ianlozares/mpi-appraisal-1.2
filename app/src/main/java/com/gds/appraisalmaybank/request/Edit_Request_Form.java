package com.gds.appraisalmaybank.request;

import java.util.Calendar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Request_Form;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;

public class Edit_Request_Form extends Activity implements OnClickListener {
	DatePicker rq_dp_datetoday;
	Spinner rq_spinner_calssification;
	EditText rq_et_fname, rq_et_mname, rq_et_lname, rq_et_requesting_party;
	Button rq_btn_register;
	DatabaseHandler db = new DatabaseHandler(this);
	String db_date_requested_month, db_date_requested_day,
			db_date_requested_year, db_classification, db_account_fname,
			db_account_mname, db_account_lname, db_requesting_party;
	String rf_month, rf_day, rf_year, rf_classification, rf_fname, rf_mname,
			rf_lname, rf_requesting_party;
	String rf_month_temp, rf_day_temp;
	Global gs;
	int request_form_record, month, year, day;
	int classification_position;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.request_form);

		rq_dp_datetoday = (DatePicker) findViewById(R.id.rq_dp_datetoday);
		rq_spinner_calssification = (Spinner) findViewById(R.id.rq_spinner_calssification);
		rq_et_fname = (EditText) findViewById(R.id.rq_et_fname);
		rq_et_mname = (EditText) findViewById(R.id.rq_et_mname);
		rq_et_lname = (EditText) findViewById(R.id.rq_et_lname);
		rq_et_requesting_party = (EditText) findViewById(R.id.rq_et_requesting_party);
		rq_btn_register = (Button) findViewById(R.id.rq_btn_register);

		gs = ((Global) getApplicationContext());
		rq_btn_register.setText("Update Data");
		rq_btn_register.setOnClickListener(this);
		request_form_record = Integer.parseInt(gs.request_form_record);
		set_data();
		setCurrentDateOnView();
	}

	public void setCurrentDateOnView() {
		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);

		// set current date into datepicker
		rq_dp_datetoday.init(year, month, day, null);
		rq_dp_datetoday.setEnabled(false);
	}

	public void get_db_request() {
		Request_Form request_form = db.getRequest_Form(request_form_record);
		if (request_form != null) {
			db_date_requested_month = request_form.getrf_date_requested_month();
			db_date_requested_day = request_form.getrf_date_requested_day();
			db_date_requested_year = request_form.getrf_date_requested_year();
			db_classification = request_form.getrf_classification();
			db_account_fname = request_form.getrf_account_fname();
			db_account_mname = request_form.getrf_account_mname();
			db_account_lname = request_form.getrf_account_lname();
			db_requesting_party = request_form.getrf_requesting_party();

		}
	}

	public void set_data() {
		get_db_request();
		year = Integer.parseInt(db_date_requested_year);
		month = Integer.parseInt(db_date_requested_month) - 1;
		day = Integer.parseInt(db_date_requested_day);
		// set date into datepicker
		rq_dp_datetoday.init(year, month, day, null);
		// classification
		for (int x = 0; x < gs.appraisal_form_classification.length; x++) {
			if (gs.appraisal_form_classification[x].equals(db_classification)) {
				classification_position = x;
			}
		}
		rq_spinner_calssification.setSelection(classification_position);
		rq_et_fname.setText(db_account_fname);
		rq_et_mname.setText(db_account_mname);
		rq_et_lname.setText(db_account_lname);
		rq_et_requesting_party.setText(db_requesting_party);
	}

	public void db_update_request_form() {
		rf_month_temp = String.valueOf(rq_dp_datetoday.getMonth() + 1);
		if (rf_month_temp.length() == 1) {
			rf_month = "0" + rf_month_temp;
		} else {
			rf_month = rf_month_temp;
		}
		rf_day_temp = String.valueOf(rq_dp_datetoday.getDayOfMonth());
		if (rf_day_temp.length() == 1) {
			rf_day = "0" + rf_day_temp;
		} else {
			rf_day = rf_day_temp;
		}
		rf_year = String.valueOf(rq_dp_datetoday.getYear());
		rf_classification = rq_spinner_calssification.getSelectedItem()
				.toString();
		rf_fname = rq_et_fname.getText().toString();
		rf_mname = rq_et_mname.getText().toString();
		rf_lname = rq_et_lname.getText().toString();
		rf_requesting_party = rq_et_requesting_party.getText().toString();

		// update Request Form table
		Request_Form request_form = new Request_Form();
		request_form.setrf_date_requested_month(rf_month);
		request_form.setrf_date_requested_day(rf_day);
		request_form.setrf_date_requested_year(rf_year);
		request_form.setrf_classification(rf_classification);
		request_form.setrf_account_fname(rf_fname);
		request_form.setrf_account_mname(rf_mname);
		request_form.setrf_account_lname(rf_lname);
		request_form.setrf_requesting_party(rf_requesting_party);
		db.updateRequest_Form(request_form, gs.request_form_record);
		db.close();
		Toast.makeText(getApplicationContext(), "Record Updated",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.rq_btn_register:
			db_update_request_form();
			startActivity(new Intent(this, Index_Request.class));
			finish();
			break;

		default:
			break;

		}

	}
}
