package com.gds.appraisalmaybank.request;

import java.io.File;
import java.util.List;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Request_Attachments;
import com.gds.appraisalmaybank.database.Request_Collateral_Address;
import com.gds.appraisalmaybank.database.Request_Contact_Persons;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;

public class View_Request_Type2 extends Activity implements OnClickListener {
	EditText rt_et_tct_no, rt_et_unit_no, rt_et_building_name, rt_et_street_no,
			rt_et_street_name, rt_et_village, rt_et_district, rt_et_zip_codes,
			rt_et_province, rt_et_region, rt_et_country;
	Spinner rt_spinner_mobile_prefix;
	TableRow rt_tv_collateral_address, row_tct, row_unit_no, row_building_name,
			row_street_no, row_street_name, row_village, row_district,
			row_zip_codes, row_city, row_province, row_region, row_country,
			row_contact, row_request_create_pdf;
	Button rt_btn_register, rt_btn_add_contact;
	AutoCompleteTextView rt_et_city;
	TextView tv_add_contact;
	Button btn_request_create_pdf;
	LinearLayout rt_container_contact, rt_container_attachment;
	Global gs;
	DatabaseHandler db = new DatabaseHandler(this);
	int request_form_record;

	String tct_no, unit_no, building_name, street_no, street_name, village,
			district, zip_codes, city, province, region, country;
	String db_app_contact_person_name, db_app_mobile_prefix, db_app_mobile_no,
			db_app_telephone_no, db_rq_file, db_rq_filename;
	// for attachments
	ListView dlg_priority_lvw = null;

	String attachment_name, attachment_counter;
	String month, day, year;
	String month_temp, day_temp;
	String file_desc;
	int request_type_index;
	String file_selected;
	String attachment_selected;
	String appraisal_type_attachment;
	String dir = "gds_appraisal";
	String item, filename;
	Dialog myDialog;
	Context context = this;
	boolean view_check = false;
	int i = 0;
	String uid;
	File myDirectory = new File(Environment.getExternalStorageDirectory() + "/"
			+ dir + "/");

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.request_page_type2);
		rt_et_tct_no = (EditText) findViewById(R.id.rt_et_tct_no);
		rt_et_unit_no = (EditText) findViewById(R.id.rt_et_unit_no);
		rt_et_building_name = (EditText) findViewById(R.id.rt_et_building_name);
		rt_et_street_no = (EditText) findViewById(R.id.rt_et_street_no);
		rt_et_street_name = (EditText) findViewById(R.id.rt_et_street_name);
		rt_et_village = (EditText) findViewById(R.id.rt_et_village);
		rt_et_district = (EditText) findViewById(R.id.rt_et_district);
		rt_et_zip_codes = (EditText) findViewById(R.id.rt_et_zip_codes);
		rt_et_city = (AutoCompleteTextView) findViewById(R.id.rt_et_city);
		rt_et_province = (EditText) findViewById(R.id.rt_et_province);
		rt_et_region = (EditText) findViewById(R.id.rt_et_region);
		rt_et_country = (EditText) findViewById(R.id.rt_et_country);
		rt_tv_collateral_address = (TableRow) findViewById(R.id.rt_tv_collateral_address);
		row_tct = (TableRow) findViewById(R.id.row_tct);
		row_unit_no = (TableRow) findViewById(R.id.row_unit_no);
		row_building_name = (TableRow) findViewById(R.id.row_building_name);
		row_street_no = (TableRow) findViewById(R.id.row_street_no);
		row_street_name = (TableRow) findViewById(R.id.row_street_name);
		row_village = (TableRow) findViewById(R.id.row_village);
		row_district = (TableRow) findViewById(R.id.row_district);
		row_zip_codes = (TableRow) findViewById(R.id.row_zip_codes);
		row_city = (TableRow) findViewById(R.id.row_city);
		row_province = (TableRow) findViewById(R.id.row_province);
		row_region = (TableRow) findViewById(R.id.row_region);
		row_country = (TableRow) findViewById(R.id.row_country);
		rt_container_contact = (LinearLayout) findViewById(R.id.rt_container_contact);
		rt_container_attachment = (LinearLayout) findViewById(R.id.rt_container_attachment);
		gs = ((Global) getApplicationContext());
		rt_btn_add_contact = (Button) findViewById(R.id.rt_btn_add_contact);
		btn_request_create_pdf = (Button) findViewById(R.id.btn_request_create_pdf);
		rt_btn_register = (Button) findViewById(R.id.rt_btn_register);
		row_contact = (TableRow) findViewById(R.id.row_contact);
		row_request_create_pdf = (TableRow) findViewById(R.id.row_request_create_pdf);
		rt_btn_register.setOnClickListener(this);
		request_form_record = Integer.parseInt(gs.request_form_record);

		set_data();
		// check_motor_vehicle();
		hide_disable_objects();
		fill_contact_person();
		fill_request_attachments();
	}

	public void fill_contact_person() {
		// for request type
		List<Request_Contact_Persons> request_contact_persons = db
				.getAllRequest_Contact_Persons(String
						.valueOf(request_form_record));
		if (!request_contact_persons.isEmpty()) {
			for (Request_Contact_Persons ct : request_contact_persons) {
				db_app_contact_person_name = ct.getrq_contact_person();
				db_app_mobile_prefix = ct.getrq_contact_mobile_no_prefix();
				db_app_mobile_no = ct.getrq_contact_mobile_no();
				db_app_telephone_no = ct.getrq_contact_landline();

				LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				final View addView = layoutInflater.inflate(
						R.layout.request_jobs_info_contact_layout, null);

				final TextView tv_contact_person = (TextView) addView
						.findViewById(R.id.tv_contact_person);
				final TextView tv_contact_prefix = (TextView) addView
						.findViewById(R.id.tv_contact_prefix);
				final TextView tv_contact_mobile = (TextView) addView
						.findViewById(R.id.tv_contact_mobile);
				final TextView tv_contact_landline = (TextView) addView
						.findViewById(R.id.tv_contact_landline);

				tv_contact_person.setText(ct.getrq_contact_person());
				tv_contact_prefix.setText(ct.getrq_contact_mobile_no_prefix());
				tv_contact_mobile.setText(ct.getrq_contact_mobile_no());
				tv_contact_landline.setText(ct.getrq_contact_landline());

				rt_container_contact.addView(addView);
			}
		}

	}

	public void hide_disable_objects() {
		row_contact.setVisibility(View.GONE);
		row_request_create_pdf.setVisibility(View.GONE);

		// disable
		rt_et_tct_no.setEnabled(false);
		rt_et_unit_no.setEnabled(false);
		rt_et_building_name.setEnabled(false);
		rt_et_street_no.setEnabled(false);
		rt_et_street_name.setEnabled(false);
		rt_et_village.setEnabled(false);
		rt_et_district.setEnabled(false);
		rt_et_zip_codes.setEnabled(false);
		rt_et_province.setEnabled(false);
		rt_et_region.setEnabled(false);
		rt_et_country.setEnabled(false);
		rt_et_city.setEnabled(false);
		// rename
		rt_btn_register.setText("Back");
	}

	public void fill_request_attachments() {
		List<Request_Attachments> request_attachments = db
				.getAllRequest_Attachments(String.valueOf(request_form_record));
		if (!request_attachments.isEmpty()) {
			for (Request_Attachments ct : request_attachments) {
				db_rq_file = ct.getrq_file();
				db_rq_filename = ct.getrq_filename();

				LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				final View addView = layoutInflater.inflate(
						R.layout.request_attachment_layout, null);

				final TextView tv_name_attachment = (TextView) addView
						.findViewById(R.id.tv_name_attachment);
				final TextView tv_attachment = (TextView) addView
						.findViewById(R.id.tv_attachment);

				final ImageView btn_select_pdf = (ImageView) addView
						.findViewById(R.id.btn_select_pdf);
				btn_select_pdf.setVisibility(View.GONE);
				tv_name_attachment.setText(db_rq_file);
				tv_attachment.setText(db_rq_filename);

				tv_attachment.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						File file = new File(myDirectory, tv_attachment
								.getText().toString());

						if (file.exists()) {
							Uri path = Uri.fromFile(file);
							Intent intent = new Intent(Intent.ACTION_VIEW);
							intent.setDataAndType(path, "application/pdf");
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

							try {
								startActivity(intent);
							} catch (ActivityNotFoundException e) {
								Toast.makeText(getApplicationContext(),
										"No Application Available to View PDF",
										Toast.LENGTH_SHORT).show();
							}
						}

					}
				});
				rt_container_attachment.addView(addView);
			}
		}
	}

	/*
	 * public void check_motor_vehicle() { if
	 * (gs.appraisal_type.equals("motor_vehicle")) {
	 * rt_et_tct_no.setVisibility(View.GONE);
	 * rt_et_unit_no.setVisibility(View.GONE);
	 * rt_et_building_name.setVisibility(View.GONE);
	 * rt_et_street_no.setVisibility(View.GONE);
	 * rt_et_street_name.setVisibility(View.GONE);
	 * rt_et_village.setVisibility(View.GONE);
	 * rt_et_district.setVisibility(View.GONE);
	 * rt_et_zip_codes.setVisibility(View.GONE);
	 * rt_et_city.setVisibility(View.GONE);
	 * rt_et_province.setVisibility(View.GONE);
	 * rt_et_country.setVisibility(View.GONE);
	 * rt_tv_collateral_address.setVisibility(View.GONE); } }
	 */
	public void get_db_request() {
		Request_Collateral_Address collateral_address = db
				.getCollateral_Address(request_form_record);
		if (collateral_address != null) {
			tct_no = collateral_address.getrq_tct_no();
			unit_no = collateral_address.getrq_unit_no();
			building_name = collateral_address.getrq_building_name();
			street_no = collateral_address.getrq_street_no();
			street_name = collateral_address.getrq_street_name();
			village = collateral_address.getrq_village();
			district = collateral_address.getrq_barangay();
			zip_codes = collateral_address.getrq_zip_code();
			city = collateral_address.getrq_city();
			province = collateral_address.getrq_province();
			region = collateral_address.getrq_region();
			country = collateral_address.getrq_country();

		}
	}

	public void set_data() {
		get_db_request();
		rt_et_tct_no.setText(tct_no);
		rt_et_unit_no.setText(unit_no);
		rt_et_building_name.setText(building_name);
		rt_et_street_no.setText(street_no);
		rt_et_street_name.setText(street_name);
		rt_et_village.setText(village);
		rt_et_district.setText(district);
		rt_et_zip_codes.setText(zip_codes);
		rt_et_city.setText(city);
		rt_et_province.setText(province);
		rt_et_region.setText(region);
		rt_et_country.setText(country);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.rt_btn_register:
			finish();
			break;
		default:
			break;
		}
	}

}
