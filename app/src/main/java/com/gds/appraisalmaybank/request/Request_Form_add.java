package com.gds.appraisalmaybank.request;

import java.util.Calendar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Request_Form;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.Session_Timer;

public class Request_Form_add extends Activity implements OnClickListener {

	Spinner rq_spinner_calssification;
	DatePicker rq_dp_datetoday;
	EditText rq_et_fname, rq_et_mname, rq_et_lname, rq_et_requesting_party;
	Button rq_btn_register;
	int year, month, day;
	DatabaseHandler db = new DatabaseHandler(this);
	String rf_month_temp, rf_day_temp;
	String rf_month, rf_day, rf_year, rf_classification, rf_fname, rf_mname,
			rf_lname, rf_requesting_party;
	// dialog
	final Context context = this;
	Session_Timer st = new Session_Timer(this);
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.request_form);
		st.resetDisconnectTimer();
		rq_spinner_calssification = (Spinner) findViewById(R.id.rq_spinner_calssification);
		rq_dp_datetoday = (DatePicker) findViewById(R.id.rq_dp_datetoday);
		rq_et_fname = (EditText) findViewById(R.id.rq_et_fname);
		rq_et_mname = (EditText) findViewById(R.id.rq_et_mname);
		rq_et_lname = (EditText) findViewById(R.id.rq_et_lname);
		rq_et_requesting_party = (EditText) findViewById(R.id.rq_et_requesting_party);
		rq_btn_register = (Button) findViewById(R.id.rq_btn_register);
		setCurrentDateOnView();
		rq_btn_register.setOnClickListener(this);
	}

	public void setCurrentDateOnView() {
		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);

		// set current date into datepicker
		rq_dp_datetoday.init(year, month, day, null);
		rq_dp_datetoday.setEnabled(false);
	}

	public void db_add_request_form() {
		rf_month_temp = String.valueOf(rq_dp_datetoday.getMonth() + 1);
		if (rf_month_temp.length() == 1) {
			rf_month = "0" + rf_month_temp;
		} else {
			rf_month = rf_month_temp;
		}

		rf_day_temp = String.valueOf(rq_dp_datetoday.getDayOfMonth());
		if (rf_day_temp.length() == 1) {
			rf_day = "0" + rf_day_temp;
		} else {
			rf_day = rf_day_temp;
		}
		rf_year = String.valueOf(rq_dp_datetoday.getYear());
		rf_classification = rq_spinner_calssification.getSelectedItem()
				.toString();
		rf_fname = rq_et_fname.getText().toString();

		rf_mname = rq_et_mname.getText().toString();
		rf_lname = rq_et_lname.getText().toString();
		rf_requesting_party = rq_et_requesting_party.getText().toString();

		db.addRequest_Form(new Request_Form(rf_month, rf_day, rf_year,
				rf_classification, rf_fname, rf_mname, rf_lname,
				rf_requesting_party));
	}

	public void dialog() {
		// custom dialog
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.yes_no_dialog);

		// set the custom dialog components - text, image and button
		TextView text = (TextView) dialog.findViewById(R.id.tv_question);
		text.setText("Are you sure on this information ?");
		Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
		Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

		// if button is clicked, close the custom dialog
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (textview_validation() == true) {
					db_add_request_form();
					startActivity(new Intent(Request_Form_add.this,
							Index_Request.class));
					finish();

				}
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	public boolean textview_validation() {
		if (rq_et_fname.getText().toString().length() == 0) {
			rq_et_fname.setError("First name is required!");
			return false;
		}
		if (rq_et_lname.getText().toString().length() == 0) {
			rq_et_lname.setError("Last name is required!");
			return false;
		}
		return true;
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.rq_btn_register:
			dialog();
			break;

		default:
			break;
		}
	}

	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}


}
