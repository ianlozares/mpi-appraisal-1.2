package com.gds.appraisalmaybank.request;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Request_Collateral_Address;
import com.gds.appraisalmaybank.database.Request_Contact_Persons;
import com.gds.appraisalmaybank.database.Request_Form;
import com.gds.appraisalmaybank.database.Request_Type;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.Session_Timer;

public class Index_Request extends ListActivity implements OnClickListener {

	// ArrayList thats going to hold the search results
	ArrayList<HashMap<String, Object>> searchResults;

	// ArrayList that will hold the original Data
	ArrayList<HashMap<String, Object>> originalValues;
	LayoutInflater inflater;
	String[][] names;
	String[] fname_lname;
	Integer[] photos;
	String item;
	int length = 0, i = 0;
	DatabaseHandler db = new DatabaseHandler(this);
	Global gs;
	String dir = "gds_appraisal";
	Button btn_add_new_form;
	String[] commandArray = new String[] { "View", "Edit", "Delete" };
	String fname, lname;
	String values;
	Session_Timer st = new Session_Timer(this);
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.index_request_form_list);
		st.resetDisconnectTimer();
		final EditText searchBox = (EditText) findViewById(R.id.searchBox);
		ListView playerListView = (ListView) findViewById(android.R.id.list);
		gs = ((Global) getApplicationContext());
		btn_add_new_form = (Button) findViewById(R.id.btn_add_new_form);
		btn_add_new_form.setOnClickListener(this);

		// onclickitem
		playerListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				arg1.setSelected(true);
				values = ((TextView) arg1.findViewById(R.id.txt_values))
						.getText().toString();
				if (values.equals("")) {
					Toast.makeText(getApplicationContext(), "no record found",
							Toast.LENGTH_SHORT).show();
				} else {
					gs.request_form_record = values;
					String nameArray[] = fname_lname[arg2].split("\\.");
					String fname = nameArray[0];
					String lname = nameArray[1];
					gs.request_form_fname = fname;
					gs.request_form_lname = lname;
					SharedPreferences appPrefs = getSharedPreferences(
							"preference", MODE_PRIVATE);
					SharedPreferences.Editor prefsEditor = appPrefs.edit();
					prefsEditor.putString("fname", fname);
					prefsEditor.putString("lname", lname);

					prefsEditor.commit();
					startActivity(new Intent(Index_Request.this,
							Request_Type_List.class));
				}

			}

		});
		playerListView.setLongClickable(true);
		playerListView
				.setOnItemLongClickListener(new OnItemLongClickListener() {
					@Override
					public boolean onItemLongClick(AdapterView<?> arg0,
							View arg1, final int arg2, long arg3) {
						arg1.setSelected(true);
						values = ((TextView) arg1.findViewById(R.id.txt_values))
								.getText().toString();
						if (values.equals("")) {
							Toast.makeText(getApplicationContext(),
									"no record found", Toast.LENGTH_SHORT)
									.show();
						} else {
							gs.request_form_record = values;
							view_edit_delete();
						}

						return true;

					}
				});
		// get the LayoutInflater for inflating the customomView
		// this will be used in the custom adapter
		inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		Log.d("Reading: ", "Count form..");

		length = db.getRequest_FormCount();
		// initialize the array
		names = new String[length][3];
		photos = new Integer[length];
		fname_lname = new String[length];
		Log.d("Reading: ", "Reading forms..");
		List<Request_Form> request_form = db.getAllRequest_Form();
		if (!request_form.isEmpty()) {
			for (Request_Form im : request_form) {
				names[i][0] = string_checker(im.getrf_account_fname())
						+ string_checker(im.getrf_account_mname())
						+ string_checker(im.getrf_account_lname());
				names[i][1] = im.getrf_date_requested_month() + "/"
						+ im.getrf_date_requested_day() + "/"
						+ im.getrf_date_requested_year();
				names[i][2] = String.valueOf(im.getID());
				photos[i] = R.drawable.monotoneright_white;
				fname_lname[i] = im.getrf_account_fname() + "."
						+ im.getrf_account_lname();
				i++;
			}

		} else {
			names = new String[1][3];
			photos = new Integer[1];
			names[0][0] = "no record found";
			names[0][1] = "";
			names[0][2] = "";
			photos[0] = R.drawable.monotoneright_white;
		}
		db.close();

		originalValues = new ArrayList<HashMap<String, Object>>();

		// temporary HashMap for populating the
		// Items in the ListView
		HashMap<String, Object> temp;

		// total number of rows in the ListView
		int noOfPlayers = names.length;
		int index = 1;
		// now populate the ArrayList players
		for (int i = 0; i < noOfPlayers; i++) {
			temp = new HashMap<String, Object>();

			temp.put("name", names[i][0]);
			temp.put("txt_date", names[i][1]);
			temp.put("txt_values", names[i][2]);
			temp.put("photo", photos[i]);
			if (noOfPlayers == 1) {
				temp.put("txt_index", "");
			} else {
				temp.put("txt_index", index + ".");
			}
			// add the row to the ArrayList
			index++;
			originalValues.add(temp);
		}

		// searchResults=OriginalValues initially
		searchResults = new ArrayList<HashMap<String, Object>>(originalValues);

		// create the adapter
		// first param-the context
		// second param-the id of the layout file
		// you will be using to fill a row
		// third param-the set of values that
		// will populate the ListView
		final CustomAdapter adapter = new CustomAdapter(this,
				R.layout.index_request_form_list_layout, searchResults);

		// finally,set the adapter to the default ListView
		playerListView.setAdapter(adapter);
		searchBox.addTextChangedListener(new TextWatcher() {

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// get the text in the EditText
				String searchString = searchBox.getText().toString();
				int textLength = searchString.length();
				searchResults.clear();

				for (int i = 0; i < originalValues.size(); i++) {
					String playerName = originalValues.get(i).get("name")
							.toString();
					if (textLength <= playerName.length()) {
						// compare the String in EditText with Names in the
						// ArrayList
						if (searchString.equalsIgnoreCase(playerName.substring(
								0, textLength)))
							searchResults.add(originalValues.get(i));
					}
				}

				adapter.notifyDataSetChanged();
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			public void afterTextChanged(Editable s) {

			}
		});

	}

	// define your custom adapter
	private class CustomAdapter extends ArrayAdapter<HashMap<String, Object>> {

		public CustomAdapter(Context context, int textViewResourceId,
				ArrayList<HashMap<String, Object>> Strings) {

			// let android do the initializing :)
			super(context, textViewResourceId, Strings);
		}

		// class for caching the views in a row
		private class ViewHolder {
			ImageView photo;
			TextView name;
			TextView txt_date;
			TextView txt_values;
			TextView txt_index;
		}

		ViewHolder viewHolder;

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			if (convertView == null) {
				convertView = inflater.inflate(
						R.layout.index_request_form_list_layout, null);
				viewHolder = new ViewHolder();

				// cache the views
				viewHolder.photo = (ImageView) convertView
						.findViewById(R.id.mono);
				viewHolder.name = (TextView) convertView
						.findViewById(R.id.txt_image_list);
				viewHolder.txt_date = (TextView) convertView
						.findViewById(R.id.txt_date);
				viewHolder.txt_values = (TextView) convertView
						.findViewById(R.id.txt_values);
				viewHolder.txt_index = (TextView) convertView
						.findViewById(R.id.txt_index);
				// link the cached views to the convertview
				convertView.setTag(viewHolder);

			} else
				viewHolder = (ViewHolder) convertView.getTag();

			int photoId = (Integer) searchResults.get(position).get("photo");

			// set the data to be displayed
			viewHolder.photo.setImageDrawable(getResources().getDrawable(
					photoId));
			viewHolder.name.setText(searchResults.get(position).get("name")
					.toString());
			viewHolder.txt_date.setText(searchResults.get(position)
					.get("txt_date").toString());
			viewHolder.txt_values.setText(searchResults.get(position)
					.get("txt_values").toString());
			viewHolder.txt_index.setText(searchResults.get(position)
					.get("txt_index").toString());
			// return the view to be displayed
			return convertView;
		}

	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.btn_add_new_form:
			startActivity(new Intent(Index_Request.this, Request_Form_add.class));
			finish();
			break;

		default:
			break;

		}
	}

	public void view_edit_delete() {
		android.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Actions :");
		builder.setItems(commandArray, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int index) {
				if (index == 0) {
					startActivity(new Intent(Index_Request.this,
							View_Request_Form.class));
					dialog.dismiss();
				} else if (index == 1) {
					startActivity(new Intent(Index_Request.this,
							Edit_Request_Form.class));
					finish();
					dialog.dismiss();
				} else if (index == 2) {
					delete();
				}
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public void delete_request_form() {
		// delete record on request form
		Request_Form request_form = new Request_Form();
		request_form.setID(Integer.parseInt(gs.request_form_record));
		db.deleteRequest_Form(request_form);
		db.close();
		// get all request type rt_id
		List<Request_Type> request_type = db
				.getAllRequest_Type(gs.request_form_record);
		if (!request_type.isEmpty()) {
			for (Request_Type im : request_type) {
				// delete collateral address
				Request_Collateral_Address request_collateral_address = new Request_Collateral_Address();
				request_collateral_address.setrt_id(String.valueOf(im.getID()));
				db.deleteRequest_Collateral_Address(request_collateral_address);

				// delete contact person
				Request_Contact_Persons request_contact_persons = new Request_Contact_Persons();
				request_contact_persons.setrt_id(String.valueOf(im.getID()));
				db.deleteRequest_Contact_Persons(request_contact_persons);
			}

		}
		db.close();
		// delete record on request type
		Request_Type request_type1 = new Request_Type();
		request_type1.setID(Integer.parseInt(gs.request_form_record));
		db.deleteRequest_Type(request_type1);
		db.close();
	}

	private void delete() {
		// TODO Auto-generated method stub
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:
					// Yes button clicked
					delete_request_form();
					Toast.makeText(getApplicationContext(),
							"Request Form Deleted", Toast.LENGTH_SHORT).show();
					reload();
					break;

				case DialogInterface.BUTTON_NEGATIVE:
					// No button clicked
					break;
				}
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Delete this record?")
				.setPositiveButton("Yes", dialogClickListener)
				.setNegativeButton("No", dialogClickListener).show();

	}

	public String string_checker(String old_string) {
		String new_string = old_string;
		if (old_string.equals(" ")) {
			new_string = "";
		} else if (old_string.equals("")) {
			new_string = "";
		} else {
			new_string = old_string + " ";
		}

		return new_string;
	}

	public void reload() {
		// reload activity
		Intent intent = getIntent();
		overridePendingTransition(0, 0);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		finish();
		overridePendingTransition(0, 0);
		startActivity(intent);
	}
	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}
}