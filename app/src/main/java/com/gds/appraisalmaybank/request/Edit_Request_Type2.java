package com.gds.appraisalmaybank.request;

import java.io.File;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;

public class Edit_Request_Type2 extends Activity implements OnClickListener {
	EditText rt_et_tct_no, rt_et_unit_no, rt_et_building_name, rt_et_street_no,
	rt_et_street_name, rt_et_village, rt_et_district, rt_et_zip_codes,
	rt_et_province, rt_et_region, rt_et_country;
	Spinner rt_spinner_mobile_prefix;
	TableRow rt_tv_collateral_address, row_tct, row_unit_no, row_building_name,
		row_street_no, row_street_name, row_village, row_district,
		row_zip_codes, row_city, row_province, row_region, row_country,
		row_contact, row_request_create_pdf;
	Button rt_btn_register, rt_btn_add_contact;
	AutoCompleteTextView rt_et_city;
	TextView tv_add_contact;
	Button btn_request_create_pdf;
	LinearLayout rt_container_contact, rt_container_attachment;
	Global gs;
	DatabaseHandler db = new DatabaseHandler(this);
	int request_form_record;
	
	String tct_no, unit_no, building_name, street_no, street_name, village,
		district, zip_codes, city, province, region, country;
	String db_app_contact_person_name, db_app_mobile_prefix, db_app_mobile_no,
		db_app_telephone_no, db_rq_file, db_rq_filename;
	// for attachments
	ListView dlg_priority_lvw = null;
   	
	String attachment_name, attachment_counter;
	String month, day, year;
	String month_temp, day_temp;
	String file_desc;
	int request_type_index;
	String file_selected;
	String attachment_selected;
	String appraisal_type_attachment;
	String dir = "gds_appraisal";
	String item, filename;
	Dialog myDialog;
	Context context = this;
	boolean view_check = false;
	int i = 0;
	String uid;
	File myDirectory = new File(Environment.getExternalStorageDirectory() + "/"
		+ dir + "/");

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.request_page_type2);
		rt_et_tct_no = (EditText) findViewById(R.id.rt_et_tct_no);
		rt_et_unit_no = (EditText) findViewById(R.id.rt_et_unit_no);
		rt_et_building_name = (EditText) findViewById(R.id.rt_et_building_name);
		rt_et_street_no = (EditText) findViewById(R.id.rt_et_street_no);
		rt_et_street_name = (EditText) findViewById(R.id.rt_et_street_name);
		rt_et_village = (EditText) findViewById(R.id.rt_et_village);
		rt_et_district = (EditText) findViewById(R.id.rt_et_district);
		rt_et_zip_codes = (EditText) findViewById(R.id.rt_et_zip_codes);
		rt_et_city = (AutoCompleteTextView) findViewById(R.id.rt_et_city);
		rt_et_province = (EditText) findViewById(R.id.rt_et_province);
		rt_et_region = (EditText) findViewById(R.id.rt_et_region);
		rt_et_country = (EditText) findViewById(R.id.rt_et_country);
		rt_tv_collateral_address = (TableRow) findViewById(R.id.rt_tv_collateral_address);
		row_tct = (TableRow) findViewById(R.id.row_tct);
		row_unit_no = (TableRow) findViewById(R.id.row_unit_no);
		row_building_name = (TableRow) findViewById(R.id.row_building_name);
		row_street_no = (TableRow) findViewById(R.id.row_street_no);
		row_street_name = (TableRow) findViewById(R.id.row_street_name);
		row_village = (TableRow) findViewById(R.id.row_village);
		row_district = (TableRow) findViewById(R.id.row_district);
		row_zip_codes = (TableRow) findViewById(R.id.row_zip_codes);
		row_city = (TableRow) findViewById(R.id.row_city);
		row_province = (TableRow) findViewById(R.id.row_province);
		row_region = (TableRow) findViewById(R.id.row_region);
		row_country = (TableRow) findViewById(R.id.row_country);
		rt_container_contact = (LinearLayout) findViewById(R.id.rt_container_contact);
		rt_container_attachment = (LinearLayout) findViewById(R.id.rt_container_attachment);
		gs = ((Global) getApplicationContext());
		rt_btn_add_contact = (Button) findViewById(R.id.rt_btn_add_contact);
		btn_request_create_pdf = (Button) findViewById(R.id.btn_request_create_pdf);
		rt_btn_register = (Button) findViewById(R.id.rt_btn_register);
		row_contact = (TableRow) findViewById(R.id.row_contact);
		row_request_create_pdf = (TableRow) findViewById(R.id.row_request_create_pdf);
		rt_btn_register.setOnClickListener(this);
		request_form_record = Integer.parseInt(gs.request_form_record);
		
		//fill_contact_person();
		//fill_spinner();
		//set_data();
		//check_motor_vehicle();
	}

	/*public void add_contact() {
		LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View addView = layoutInflater.inflate(
				R.layout.request_page_type2_dynamic_contact, null);

		final TextView rt_tv_contact_person = (TextView) addView
				.findViewById(R.id.rt_tv_contact_person);
		final TextView rt_tv_mobile_prefix = (TextView) addView
				.findViewById(R.id.rt_tv_mobile_prefix);
		final TextView rt_tv_mobile = (TextView) addView
				.findViewById(R.id.rt_tv_mobile);
		final TextView rt_tv_landline = (TextView) addView
				.findViewById(R.id.rt_tv_landline);

		rt_tv_contact_person.setText(rt_et_contact_person.getText().toString());
		rt_tv_mobile_prefix.setText(rt_spinner_mobile_prefix.getSelectedItem()
				.toString());
		rt_tv_mobile.setText(rt_et_mobile.getText().toString());
		rt_tv_landline.setText(rt_et_landline.getText().toString());

		final TextView tv_contact = (TextView) addView
				.findViewById(R.id.tv_contact);
		tv_contact.setText(String.valueOf(counter));
		counter++;

		ContactsArray_contact_person.add(rt_et_contact_person.getText()
				.toString());
		ContactsArray_mobile_prefix.add(rt_spinner_mobile_prefix
				.getSelectedItem().toString());
		ContactsArray_mobile.add(rt_et_mobile.getText().toString());
		ContactsArray_landline.add(rt_et_landline.getText().toString());

		Button buttonRemove = (Button) addView.findViewById(R.id.remove);

		buttonRemove.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((LinearLayout) addView.getParent()).removeView(addView);

				ContactsArray_contact_person.set(
						Integer.parseInt(tv_contact.getText().toString()), "");
				ContactsArray_mobile_prefix.set(
						Integer.parseInt(tv_contact.getText().toString()), "");
				ContactsArray_mobile.set(
						Integer.parseInt(tv_contact.getText().toString()), "");
				ContactsArray_landline.set(
						Integer.parseInt(tv_contact.getText().toString()), "");
				Toast.makeText(getApplicationContext(), "Contact Removed",
						Toast.LENGTH_SHORT).show();
			}
		});

		rt_container_contact.addView(addView);

	}

	public void fill_contact_person() {
		// for request type
		List<Request_Contact_Persons> request_contact_persons = db
				.getAllRequest_Contact_Persons(String
						.valueOf(request_form_record));
		if (!request_contact_persons.isEmpty()) {
			for (Request_Contact_Persons ct : request_contact_persons) {
				db_app_contact_id = String.valueOf(ct.getID());
				db_app_contact_person_name = ct.getrq_contact_person();
				db_app_mobile_prefix = ct.getrq_contact_mobile_no_prefix();
				db_app_mobile_no = ct.getrq_contact_mobile_no();
				db_app_telephone_no = ct.getrq_contact_landline();

				LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				final View addView = layoutInflater.inflate(
						R.layout.request_page_type3, null);

				final EditText rt_et_contact_person_edit = (EditText) addView
						.findViewById(R.id.rt_et_contact_person_edit);
				final EditText rt_et_mobile_edit = (EditText) addView
						.findViewById(R.id.rt_et_mobile_edit);
				final EditText rt_et_landline_edit = (EditText) addView
						.findViewById(R.id.rt_et_landline_edit);
				final Spinner rt_spinner_mobile_prefix_edit = (Spinner) addView
						.findViewById(R.id.rt_spinner_mobile_prefix_edit);
				final Button remove_edit = (Button) addView
						.findViewById(R.id.remove_edit);
				final TextView tv_contact_edit = (TextView) addView
						.findViewById(R.id.tv_contact_edit);
				final TextView tv_contact_id_edit = (TextView) addView
						.findViewById(R.id.tv_contact_id_edit);

				// fill spinner
				for (int x = 0; x < 95; x++) {
					SpinnerArray.add("0" + String.valueOf(prefix++));
				}
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
						android.R.layout.simple_spinner_item, SpinnerArray);
				adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				rt_spinner_mobile_prefix_edit.setAdapter(adapter);

				rt_et_contact_person_edit.setText(db_app_contact_person_name);
				rt_et_mobile_edit.setText(db_app_mobile_no);
				rt_et_landline_edit.setText(db_app_telephone_no);
				tv_contact_id_edit.setText(db_app_contact_id);
				for (int x = 0; x < SpinnerArray.size(); x++) {
					if (SpinnerArray.get(x).equals(db_app_mobile_prefix)) {
						classification_position = x;
					}
				}
				rt_spinner_mobile_prefix_edit
						.setSelection(classification_position);
				tv_contact_edit.setText(String.valueOf(counter));
				counter++;
				ContactsArray_id.add(db_app_contact_id);
				ContactsArray_status.add("1");
				// edit
				addView.setEnabled(false);
				addView.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						// update Request contact person
						Request_Contact_Persons request_contact_persons = new Request_Contact_Persons();
						request_contact_persons
								.setrq_contact_person(rt_et_contact_person_edit
										.getText().toString());
						request_contact_persons
								.setrq_contact_mobile_no_prefix(rt_spinner_mobile_prefix_edit
										.getSelectedItem().toString());
						request_contact_persons
								.setrq_contact_mobile_no(rt_et_mobile_edit
										.getText().toString());
						request_contact_persons
								.setrq_contact_landline(rt_et_landline_edit
										.getText().toString());
						db.updateContact_Person_edit(request_contact_persons,
								tv_contact_id_edit.getText().toString());
						db.close();
					}
				});

				// remove
				remove_edit.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						((LinearLayout) addView.getParent())
								.removeView(addView);
						ContactsArray_status.set(
								Integer.parseInt(tv_contact_edit.getText()
										.toString()), "2");
						Toast.makeText(getApplicationContext(),
								"Contact Removed", Toast.LENGTH_SHORT).show();
					}
				});

				rt_container_edit_contact.addView(addView);

			}
		}

	}

	public void fill_spinner() {
		for (int x = 0; x < 95; x++) {
			SpinnerArray.add("0" + String.valueOf(prefix++));
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, SpinnerArray);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		rt_spinner_mobile_prefix.setAdapter(adapter);
	}

	public void check_motor_vehicle() {
		if (gs.appraisal_type.equals("motor_vehicle")) {
			rt_et_tct_no.setVisibility(View.GONE);
			rt_et_unit_no.setVisibility(View.GONE);
			rt_et_building_name.setVisibility(View.GONE);
			rt_et_street_no.setVisibility(View.GONE);
			rt_et_street_name.setVisibility(View.GONE);
			rt_et_village.setVisibility(View.GONE);
			rt_et_district.setVisibility(View.GONE);
			rt_et_zip_codes.setVisibility(View.GONE);
			rt_et_city.setVisibility(View.GONE);
			rt_et_province.setVisibility(View.GONE);
			rt_et_country.setVisibility(View.GONE);
			rt_tv_collateral_address.setVisibility(View.GONE);
		}
	}

	public void get_db_request() {
		Request_Collateral_Address collateral_address = db
				.getCollateral_Address(request_form_record);
		if (collateral_address != null) {
			tct_no = collateral_address.getrq_tct_no();
			unit_no = collateral_address.getrq_unit_no();
			building_name = collateral_address.getrq_building_name();
			street_no = collateral_address.getrq_street_no();
			street_name = collateral_address.getrq_street_name();
			village = collateral_address.getrq_village();
			district = collateral_address.getrq_barangay();
			zip_codes = collateral_address.getrq_zip_code();
			city = collateral_address.getrq_city();
			province = collateral_address.getrq_province();
			country = collateral_address.getrq_country();

		}
	}

	public void set_data() {
		get_db_request();
		rt_et_tct_no.setText(tct_no);
		rt_et_unit_no.setText(unit_no);
		rt_et_building_name.setText(building_name);
		rt_et_street_no.setText(street_no);
		rt_et_street_name.setText(street_name);
		rt_et_village.setText(village);
		rt_et_district.setText(district);
		rt_et_zip_codes.setText(zip_codes);
		rt_et_city.setText(city);
		rt_et_province.setText(province);
		rt_et_country.setText(country);
	}

	public void get_value_global() {
		cntrol_no = gs.cntrol_no;
		nature_appraisal = gs.nature_appraisal;
		appraisal_type = gs.appraisal_type;
		inspection_date1_month = gs.inspection_date1_month;
		inspection_date1_day = gs.inspection_date1_day;
		inspection_date1_year = gs.inspection_date1_year;
		inspection_date2_month = gs.inspection_date2_month;
		inspection_date2_day = gs.inspection_date2_day;
		inspection_date2_year = gs.inspection_date2_year;
		vehicle_type = gs.vehicle_type;
		car_model = gs.car_model;
	}

	public void get_value_address() {
		tct_no = rt_et_tct_no.getText().toString();
		unit_no = rt_et_unit_no.getText().toString();
		building_name = rt_et_building_name.getText().toString();
		street_no = rt_et_street_no.getText().toString();
		street_name = rt_et_street_name.getText().toString();
		village = rt_et_village.getText().toString();
		district = rt_et_district.getText().toString();
		zip_codes = rt_et_zip_codes.getText().toString();
		city = rt_et_city.getText().toString();
		province = rt_et_province.getText().toString();
		country = rt_et_country.getText().toString();
	}

	public void update_db() {
		// update Request Type
		get_value_global();
		Request_Type request_type = new Request_Type();
		request_type.setrt_control_no(cntrol_no);
		request_type.setrt_nature_of_appraisal(nature_appraisal);
		request_type.setrt_appraisal_type(appraisal_type);
		request_type.setrt_pref_ins_date1_month(inspection_date1_month);
		request_type.setrt_pref_ins_date1_day(inspection_date1_day);
		request_type.setrt_pref_ins_date1_year(inspection_date1_year);
		request_type.setrt_pref_ins_date2_month(inspection_date2_month);
		request_type.setrt_pref_ins_date2_day(inspection_date2_day);
		request_type.setrt_pref_ins_date2_year(inspection_date2_year);
		request_type.setrt_vehicle_type(vehicle_type);
		request_type.setrt_car_model(car_model);
		db.updateRequest_Type(request_type, gs.request_form_record);
		db.close();

		// collateral address
		get_value_address();
		Request_Collateral_Address collateral_address = new Request_Collateral_Address();
		collateral_address.setrq_tct_no(tct_no);
		collateral_address.setrq_unit_no(unit_no);
		collateral_address.setrq_building_name(building_name);
		collateral_address.setrq_street_no(street_no);
		collateral_address.setrq_street_name(street_name);
		collateral_address.setrq_village(village);
		collateral_address.setrq_barangay(district);
		collateral_address.setrq_zip_code(zip_codes);
		collateral_address.setrq_city(city);
		collateral_address.setrq_province(province);
		collateral_address.setrq_country(country);
		db.updateRequest_Collateral_Address(collateral_address,
				gs.request_form_record);
		db.close();

		// delete contact person
		for (int x = 0; x < ContactsArray_status.size(); x++) {
			if (ContactsArray_status.get(x).equals("2")) {
				// delete
				Request_Contact_Persons request_contact_persons = new Request_Contact_Persons();
				request_contact_persons.setID(Integer.parseInt(ContactsArray_id
						.get(x)));
				db.deleteRequest_Contact_Persons_edit(request_contact_persons);
				db.close();
			}
		}
		// update
		int childcount = rt_container_edit_contact.getChildCount();
		for (int i = 0; i < childcount; i++) {
			View view = rt_container_edit_contact.getChildAt(i);
			view.performClick();
		}
		// add contact
		for (int x = 0; x < ContactsArray_contact_person.size(); x++) {
			if (!ContactsArray_contact_person.get(x).equals("")) {
				db.addRequest_Contact_Persons(new Request_Contact_Persons(
						String.valueOf(request_form_record),
						ContactsArray_contact_person.get(x),
						ContactsArray_mobile_prefix.get(x),
						ContactsArray_mobile.get(x), ContactsArray_landline
								.get(x)));
			}
		}

		Toast.makeText(getApplicationContext(), "Record Updated",
				Toast.LENGTH_SHORT).show();

	}*/

	@Override
	public void onClick(View v) {
		
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.rt_btn_register:
			//update_db();
			finish();
			break;
		case R.id.rt_btn_add_contact:
			//add_contact();
			Toast.makeText(getApplicationContext(), "Contact Added",
					Toast.LENGTH_SHORT).show();
			break;
		default:
			break;
		}
	}
}
