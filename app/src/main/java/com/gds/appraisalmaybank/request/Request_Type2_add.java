package com.gds.appraisalmaybank.request;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import com.gds.appraisalmaybank.database.Cities;
import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Images;
import com.gds.appraisalmaybank.database.Provinces;
import com.gds.appraisalmaybank.database.Regions;
import com.gds.appraisalmaybank.database.Request_Attachments;
import com.gds.appraisalmaybank.database.Request_Collateral_Address;
import com.gds.appraisalmaybank.database.Request_Contact_Persons;
import com.gds.appraisalmaybank.database.Request_Type;
import com.gds.appraisalmaybank.database.Required_Attachments;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;

public class Request_Type2_add extends Activity implements OnClickListener {
	EditText rt_et_contact_person, rt_et_mobile, rt_et_landline, rt_et_tct_no,
			rt_et_unit_no, rt_et_building_name, rt_et_street_no,
			rt_et_street_name, rt_et_village, rt_et_district, rt_et_zip_codes,
			rt_et_province, rt_et_region, rt_et_country;
	Spinner rt_spinner_mobile_prefix;
	TableRow rt_tv_collateral_address, row_tct, row_unit_no, row_building_name,
			row_street_no, row_street_name, row_village, row_district,
			row_zip_codes, row_city, row_province, row_region, row_country;
	Button rt_btn_register, rt_btn_add_contact;
	AutoCompleteTextView rt_et_city;
	Button btn_request_create_pdf;
	LinearLayout rt_container_contact, rt_container_attachment;
	int counter = 0;

	String tct_no, unit_no, building_name, street_no, street_name, village,
			district, zip_codes, city, province, region, country;

	String rf_id, cntrol_no, nature_appraisal, cost_center, appraisal_type,
			inspection_date1_month, inspection_date1_day,
			inspection_date1_year, inspection_date2_month,
			inspection_date2_day, inspection_date2_year;
	String mv_year, mv_make, mv_model, mv_odometer, mv_vin, me_quantity,
			me_type, me_manufacturer, me_model, me_serial, me_age,
			me_condition;
	String rt_id;
	int prefix = 905;
	Global gs;
	List<String> SpinnerArray = new ArrayList<String>();
	DatabaseHandler db = new DatabaseHandler(this);
	// for attachments
	ListView dlg_priority_lvw = null;

	String attachment_name, attachment_counter;
	String month, day, year;
	String month_temp, day_temp;
	String file_desc;
	int request_type_index;
	String file_selected;
	String attachment_selected;
	String appraisal_type_attachment;
	String dir = "gds_appraisal";
	String item, filename;
	Dialog myDialog;
	Context context = this;
	boolean view_check = false;
	int i = 0;
	String uid;
	File myDirectory = new File(Environment.getExternalStorageDirectory() + "/"
			+ dir + "/");
	List<String> cities_array = new ArrayList<String>();
	List<String> cities_array_provid = new ArrayList<String>();
	List<String> provinces_array = new ArrayList<String>();
	List<String> provinces_array_region = new ArrayList<String>();
	String reg_id;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.request_page_type2);

		rt_et_tct_no = (EditText) findViewById(R.id.rt_et_tct_no);
		rt_et_unit_no = (EditText) findViewById(R.id.rt_et_unit_no);
		rt_et_building_name = (EditText) findViewById(R.id.rt_et_building_name);
		rt_et_street_no = (EditText) findViewById(R.id.rt_et_street_no);
		rt_et_street_name = (EditText) findViewById(R.id.rt_et_street_name);
		rt_et_village = (EditText) findViewById(R.id.rt_et_village);
		rt_et_district = (EditText) findViewById(R.id.rt_et_district);
		rt_et_zip_codes = (EditText) findViewById(R.id.rt_et_zip_codes);
		rt_et_city = (AutoCompleteTextView) findViewById(R.id.rt_et_city);
		rt_et_province = (EditText) findViewById(R.id.rt_et_province);
		rt_et_region = (EditText) findViewById(R.id.rt_et_region);
		rt_et_country = (EditText) findViewById(R.id.rt_et_country);
		rt_tv_collateral_address = (TableRow) findViewById(R.id.rt_tv_collateral_address);
		row_tct = (TableRow) findViewById(R.id.row_tct);
		row_unit_no = (TableRow) findViewById(R.id.row_unit_no);
		row_building_name = (TableRow) findViewById(R.id.row_building_name);
		row_street_no = (TableRow) findViewById(R.id.row_street_no);
		row_street_name = (TableRow) findViewById(R.id.row_street_name);
		row_village = (TableRow) findViewById(R.id.row_village);
		row_district = (TableRow) findViewById(R.id.row_district);
		row_zip_codes = (TableRow) findViewById(R.id.row_zip_codes);
		row_city = (TableRow) findViewById(R.id.row_city);
		row_province = (TableRow) findViewById(R.id.row_province);
		row_region = (TableRow) findViewById(R.id.row_region);
		row_country = (TableRow) findViewById(R.id.row_country);
		rt_container_contact = (LinearLayout) findViewById(R.id.rt_container_contact);
		rt_container_attachment = (LinearLayout) findViewById(R.id.rt_container_attachment);
		gs = ((Global) getApplicationContext());
		check_motor_vehicle();
		rt_btn_add_contact = (Button) findViewById(R.id.rt_btn_add_contact);
		btn_request_create_pdf = (Button) findViewById(R.id.btn_request_create_pdf);
		rt_btn_register = (Button) findViewById(R.id.rt_btn_register);
		rt_btn_register.setOnClickListener(this);
		rt_btn_add_contact.setOnClickListener(this);
		btn_request_create_pdf.setOnClickListener(this);

		required_dynamic_attachments();
		// set date today
		setCurrentDateOnView();

		// cities
		List<Cities> city = db.getAllCities();
		if (!city.isEmpty()) {
			for (Cities im : city) {
				cities_array.add(im.getname());
				cities_array_provid.add(im.getprovince_id());
			}

		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_dropdown_item_1line, cities_array);
		rt_et_city.setAdapter(adapter);

		rt_et_city.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				// city
				for (int x = 0; x < cities_array.size(); x++) {
					if (rt_et_city.getText().toString()
							.equals(cities_array.get(x))) {

						List<Provinces> provinces = db
								.getProvinces_single(cities_array_provid.get(x));
						if (!provinces.isEmpty()) {
							for (Provinces prov : provinces) {
								rt_et_province.setText(prov.getname());
								reg_id = prov.getregion_id();
								// region
								List<Regions> regions = db
										.getRegion_single(reg_id);
								if (!regions.isEmpty()) {
									for (Regions reg : regions) {
										rt_et_region.setText(reg.getname());
										rt_et_country.setText("Philippines");
									}
								}
							}
						}

					}
				}

			}
		});

	}

	public void fill_spinner() {
		// for mobile prefix
		for (int x = 0; x < 95; x++) {
			SpinnerArray.add("0" + String.valueOf(prefix++));
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, SpinnerArray);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		rt_spinner_mobile_prefix.setAdapter(adapter);

	}

	public void add_contact() {
		LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View addView = layoutInflater.inflate(
				R.layout.request_page_type2_dynamic_contact, null);

		final EditText rt_et_contact_person = (EditText) addView
				.findViewById(R.id.rt_et_contact_person);
		final EditText rt_et_mobile = (EditText) addView
				.findViewById(R.id.rt_et_mobile);
		final EditText rt_et_landline = (EditText) addView
				.findViewById(R.id.rt_et_landline);
		rt_spinner_mobile_prefix = (Spinner) addView
				.findViewById(R.id.rt_spinner_mobile_prefix);
		addView.setEnabled(false);
		fill_spinner();
		addView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				db.addRequest_Contact_Persons(new Request_Contact_Persons(
						rt_id, rt_et_contact_person.getText().toString(),
						rt_spinner_mobile_prefix.getSelectedItem().toString(),
						rt_et_mobile.getText().toString(), rt_et_landline
								.getText().toString()));
			}
		});
		Button contact_remove = (Button) addView
				.findViewById(R.id.contact_remove);

		contact_remove.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((LinearLayout) addView.getParent()).removeView(addView);
			}
		});

		rt_container_contact.addView(addView);
	}

	public void check_motor_vehicle() {
		if (gs.appraisal_type.equals("motor_vehicle")) {
			row_tct.setVisibility(View.GONE);
			row_unit_no.setVisibility(View.GONE);
			row_building_name.setVisibility(View.GONE);
			row_street_no.setVisibility(View.GONE);
			row_street_name.setVisibility(View.GONE);
			row_village.setVisibility(View.GONE);
			row_district.setVisibility(View.GONE);
			row_zip_codes.setVisibility(View.GONE);
			row_city.setVisibility(View.GONE);
			row_province.setVisibility(View.GONE);
			row_region.setVisibility(View.GONE);
			row_country.setVisibility(View.GONE);
			rt_tv_collateral_address.setVisibility(View.GONE);
		}
	}

	public void get_value() {
		tct_no = rt_et_tct_no.getText().toString();
		unit_no = rt_et_unit_no.getText().toString();
		building_name = rt_et_building_name.getText().toString();
		street_no = rt_et_street_no.getText().toString();
		street_name = rt_et_street_name.getText().toString();
		village = rt_et_village.getText().toString();
		district = rt_et_district.getText().toString();
		zip_codes = rt_et_zip_codes.getText().toString();
		city = rt_et_city.getText().toString();
		province = rt_et_province.getText().toString();
		region = rt_et_region.getText().toString();
		country = rt_et_country.getText().toString();
	}

	public void get_value_global() {
		rf_id = gs.rf_id;
		cntrol_no = gs.cntrol_no;
		nature_appraisal = gs.nature_appraisal;
		appraisal_type = gs.appraisal_type;
		inspection_date1_month = gs.inspection_date1_month;
		inspection_date1_day = gs.inspection_date1_day;
		inspection_date1_year = gs.inspection_date1_year;
		inspection_date2_month = gs.inspection_date2_month;
		inspection_date2_day = gs.inspection_date2_day;
		inspection_date2_year = gs.inspection_date2_year;
		mv_year = gs.mv_year;
		mv_make = gs.mv_make;
		mv_model = gs.mv_model;
		mv_odometer = gs.mv_odometer;
		mv_vin = gs.mv_vin;
		me_quantity = gs.me_quantity;
		me_type = gs.me_type;
		me_manufacturer = gs.me_manufacturer;
		me_model = gs.me_model;
		me_serial = gs.me_serial;
		me_age = gs.me_age;
		me_condition = gs.me_condition;
	}

	public void add_db_type() {
		get_value();
		get_value_global();
		// request type
		db.addRequest_Type(new Request_Type(rf_id, cntrol_no, nature_appraisal,
				cost_center, appraisal_type, inspection_date1_month,
				inspection_date1_day, inspection_date1_year,
				inspection_date2_month, inspection_date2_day,
				inspection_date2_year, mv_year, mv_make, mv_model, mv_odometer,
				mv_vin, me_quantity, me_type, me_manufacturer, me_model,
				me_serial, me_age, me_condition));

		Request_Type request_type = db.getRequest_Type_Last();
		if (request_type != null) {
			rt_id = String.valueOf(request_type.getID());
		}
		// address
		db.addRequest_Collateral_Address(new Request_Collateral_Address(rt_id,
				tct_no, unit_no, building_name, street_no, street_name,
				village, district, zip_codes, city, province, region, country));
		// add attachments
		view_check = false;
		int childcount = rt_container_attachment.getChildCount();
		for (int i = 0; i < childcount; i++) {
			View view = rt_container_attachment.getChildAt(i);
			view.performClick();
		}
		// contact person
		int childcount_contact = rt_container_contact.getChildCount();
		for (int i = 0; i < childcount_contact; i++) {
			View view = rt_container_contact.getChildAt(i);
			view.performClick();
		}
	}

	public void dialog() {
		// custom dialog
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.yes_no_dialog);

		// set the custom dialog components - text, image and button
		TextView text = (TextView) dialog.findViewById(R.id.tv_question);
		text.setText("Are you sure on this information ?");
		Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
		Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

		// if button is clicked, close the custom dialog
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				add_db_type();
				if (Request_Type_add.instance != null) {
					try {
						Request_Type_add.instance.finish();
					} catch (Exception e) {
					}
				}
				finish();
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	public void setCurrentDateOnView() {
		final Calendar c = Calendar.getInstance();
		year = String.valueOf(c.get(Calendar.YEAR));
		month_temp = String.valueOf(c.get(Calendar.MONTH) + 1);
		day_temp = String.valueOf(c.get(Calendar.DAY_OF_MONTH));

		if (month_temp.length() == 1) {
			month = "0" + month_temp;
		} else {
			month = month_temp;
		}
		if (day_temp.length() == 1) {
			day = "0" + day_temp;
		} else {
			day = day_temp;
		}
	}

	public void create_pdfname_global() {
		request_type_index = gs.request_type_index;
		attachment_name = gs.request_form_lname + "_" + gs.request_form_fname
				+ "_" + month + day + year + "_" + file_desc + "_"
				+ request_type_index;
		gs.attachment_file_name = attachment_name;
		SharedPreferences appPrefs = getSharedPreferences("preference",
				MODE_PRIVATE);
		SharedPreferences.Editor prefsEditor = appPrefs.edit();
		prefsEditor.putString("pdf", attachment_name);
		prefsEditor.commit();
		startActivity(new Intent(Request_Type2_add.this,
				Request_Attachments_Pdf_create.class));
	}

	public void open_customdialog() {
		// dialog
		myDialog = new Dialog(context);
		myDialog.setTitle("Select Attachment");
		myDialog.setContentView(R.layout.request_pdf_list);
		// myDialog.setTitle("My Dialog");

		dlg_priority_lvw = (ListView) myDialog
				.findViewById(R.id.dlg_priority_lvw);
		// ListView
		SimpleAdapter adapter = new SimpleAdapter(context, getPriorityList(),
				R.layout.request_pdf_list_layout, new String[] {
						"txt_pdf_list", "mono" }, new int[] {
						R.id.txt_pdf_list, R.id.mono });
		dlg_priority_lvw.setAdapter(adapter);

		// ListView
		dlg_priority_lvw
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {

						item = ((TextView) arg1.findViewById(R.id.txt_pdf_list))
								.getText().toString();
						attachment_selected = item;
						view_check = true;
						View view = rt_container_attachment.getChildAt(Integer
								.parseInt(attachment_counter));
						view.performClick();
						myDialog.dismiss();
					}
				});
		dlg_priority_lvw.setLongClickable(true);
		dlg_priority_lvw
				.setOnItemLongClickListener(new OnItemLongClickListener() {
					@Override
					public boolean onItemLongClick(AdapterView<?> arg0,
							View arg1, final int arg2, long arg3) {
						item = ((TextView) arg1.findViewById(R.id.txt_pdf_list))
								.getText().toString();
						Toast.makeText(getApplicationContext(), item,
								Toast.LENGTH_SHORT).show();

						File file = new File(myDirectory, item);

						if (file.exists()) {
							Uri path = Uri.fromFile(file);
							Intent intent = new Intent(Intent.ACTION_VIEW);
							intent.setDataAndType(path, "application/pdf");
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

							try {
								startActivity(intent);
							} catch (ActivityNotFoundException e) {
								Toast.makeText(getApplicationContext(),
										"No Application Available to View PDF",
										Toast.LENGTH_SHORT).show();
							}
						}

						return true;

					}
				});
		myDialog.show();
	}

	private List<HashMap<String, Object>> getPriorityList() {
		DatabaseHandler db = new DatabaseHandler(
				context.getApplicationContext());
		List<HashMap<String, Object>> priorityList = new ArrayList<HashMap<String, Object>>();
		List<Images> images = db.getAllImages();
		if (!images.isEmpty()) {
			for (Images im : images) {
				filename = im.getfilename();
				String filenameArray[] = filename.split("\\.");
				String extension = filenameArray[filenameArray.length - 1];
				if (extension.equals("pdf")) {
					HashMap<String, Object> map1 = new HashMap<String, Object>();
					map1.put("txt_pdf_list", im.getfilename());
					map1.put("mono", R.drawable.monotoneright_white);
					priorityList.add(map1);
				}

			}

		}
		return priorityList;
	}

	public void required_dynamic_attachments() {
		// set value
		List<Required_Attachments> required_attachments = db
				.getAllRequired_Attachments_byid(String
						.valueOf(gs.appraisal_type));
		if (!required_attachments.isEmpty()) {
			for (Required_Attachments ra : required_attachments) {
				LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				final View addView = layoutInflater.inflate(
						R.layout.request_attachment_layout, null);

				final TextView tv_name_attachment = (TextView) addView
						.findViewById(R.id.tv_name_attachment);
				final TextView tv_attachment = (TextView) addView
						.findViewById(R.id.tv_attachment);
				final TextView tv_attach_counter = (TextView) addView
						.findViewById(R.id.tv_attach_counter);

				final ImageView btn_select_pdf = (ImageView) addView
						.findViewById(R.id.btn_select_pdf);

				tv_attach_counter.setText(String.valueOf(i));
				i++;
				tv_name_attachment.setText(ra.getattachment().toString());
				tv_name_attachment
						.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								if (btn_select_pdf.isShown()) {
									btn_select_pdf.setVisibility(View.GONE);
								} else {
									btn_select_pdf.setVisibility(View.VISIBLE);
								}
							}
						});
				btn_select_pdf.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						attachment_counter = tv_attach_counter.getText()
								.toString();
						open_customdialog();

					}
				});
				addView.setEnabled(false);
				addView.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (view_check == true) {
							tv_attachment.setText(attachment_selected);
						} else if (view_check == false) {
							// insert to db
							uid = gs.request_form_lname + "_"
									+ gs.request_form_fname + "_" + month + day
									+ year;
							db.addRequest_Attachments(new Request_Attachments(
									rt_id, uid, tv_name_attachment.getText()
											.toString(), tv_attachment
											.getText().toString(),
									gs.appraisal_type + "_"
											+ request_type_index, "true"));
						}
					}
				});
				tv_attachment.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						File file = new File(myDirectory, tv_attachment
								.getText().toString());

						if (file.exists()) {
							Uri path = Uri.fromFile(file);
							Intent intent = new Intent(Intent.ACTION_VIEW);
							intent.setDataAndType(path, "application/pdf");
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

							try {
								startActivity(intent);
							} catch (ActivityNotFoundException e) {
								Toast.makeText(getApplicationContext(),
										"No Application Available to View PDF",
										Toast.LENGTH_SHORT).show();
							}
						}

					}
				});
				rt_container_attachment.addView(addView);

			}

		}

	}

	public void open_customdialog_attachments() {
		// dialog
		myDialog = new Dialog(context);
		myDialog.setTitle("Select Attachment");
		myDialog.setContentView(R.layout.request_pdf_list);
		// myDialog.setTitle("My Dialog");

		dlg_priority_lvw = (ListView) myDialog
				.findViewById(R.id.dlg_priority_lvw);
		// ListView
		SimpleAdapter adapter = new SimpleAdapter(context, getPriorityList2(),
				R.layout.request_pdf_list_layout, new String[] {
						"txt_attachment", "mono" }, new int[] {
						R.id.txt_pdf_list, R.id.mono });
		dlg_priority_lvw.setAdapter(adapter);

		// ListView
		dlg_priority_lvw
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {

						item = ((TextView) arg1.findViewById(R.id.txt_pdf_list))
								.getText().toString();
						myDialog.dismiss();
						// dialog
						file_desc = item;
						create_pdfname_global();
					}
				});
		myDialog.show();
	}

	private List<HashMap<String, Object>> getPriorityList2() {
		DatabaseHandler db = new DatabaseHandler(
				context.getApplicationContext());
		List<HashMap<String, Object>> priorityList = new ArrayList<HashMap<String, Object>>();
		// set value
		List<Required_Attachments> required_attachments = db
				.getAllRequired_Attachments_byid(String
						.valueOf(gs.appraisal_type));
		if (!required_attachments.isEmpty()) {
			for (Required_Attachments ra : required_attachments) {
				HashMap<String, Object> map1 = new HashMap<String, Object>();
				map1.put("txt_attachment", ra.getattachment());
				map1.put("mono", R.drawable.monotoneright_white);
				priorityList.add(map1);
			}
		}
		return priorityList;
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.rt_btn_register:
			dialog();
			break;
		case R.id.rt_btn_add_contact:
			add_contact();
			break;
		case R.id.btn_request_create_pdf:
			open_customdialog_attachments();
			break;

		default:
			break;
		}
	}
}
