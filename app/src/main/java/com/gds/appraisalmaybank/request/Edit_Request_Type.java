package com.gds.appraisalmaybank.request;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Request_Type;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;

public class Edit_Request_Type extends Activity implements OnClickListener {
	EditText rt_cntrl_no, rt_et_vehicle_type, rt_et_car_model;
	Spinner rt_spinner_nature_appraisal, rt_spinner_appraisal_type;
	DatePicker rt_dp_inspection_date1, rt_dp_inspection_date2;
	TextView rt_tv_vehicle_type, rt_tv_car_model;
	Button rt_btn_next;
	Global gs;
	int request_form_record;
	DatabaseHandler db = new DatabaseHandler(this);

	String db_cntrl_no, db_nature_appraisal, db_appraisal_type,
			db_pref_inspection_date1_month, db_pref_inspection_date1_day,
			db_pref_inspection_date1_year, db_pref_inspection_date2_month,
			db_pref_inspection_date2_day, db_pref_inspection_date2_year,
			db_vehicle_type, db_car_model;
	int month1, year1, day1, month2, year2, day2;
	int classification_position;
	String cntrol_no, vehicle_type, car_model, nature_appraisal,
			appraisal_type, amount, inspection_date1_month,
			inspection_date1_day, inspection_date1_year,
			inspection_date2_month, inspection_date2_day,
			inspection_date2_year;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.request_type);
		gs = ((Global) getApplicationContext());
		rt_cntrl_no = (EditText) findViewById(R.id.rt_cntrl_no);
		rt_dp_inspection_date1 = (DatePicker) findViewById(R.id.rt_dp_inspection_date1);
		rt_dp_inspection_date2 = (DatePicker) findViewById(R.id.rt_dp_inspection_date2);
		rt_spinner_nature_appraisal = (Spinner) findViewById(R.id.rt_spinner_nature_appraisal);
		rt_spinner_appraisal_type = (Spinner) findViewById(R.id.rt_spinner_appraisal_type);
		rt_btn_next = (Button) findViewById(R.id.rt_btn_next);
		rt_btn_next.setOnClickListener(this);
		request_form_record = Integer.parseInt(gs.request_form_record);
		set_data();

	}

	public void get_db_request() {
		Request_Type request_type = db.getRequest_Type(request_form_record);
		if (request_type != null) {
			db_cntrl_no = request_type.getrt_control_no();
			db_nature_appraisal = request_type.getrt_nature_of_appraisal();
			db_appraisal_type = request_type.getrt_appraisal_type();
			db_pref_inspection_date1_month = request_type
					.getrt_pref_ins_date1_month();
			db_pref_inspection_date1_day = request_type
					.getrt_pref_ins_date1_day();
			db_pref_inspection_date1_year = request_type
					.getrt_pref_ins_date1_year();
			db_pref_inspection_date2_month = request_type
					.getrt_pref_ins_date2_month();
			db_pref_inspection_date2_day = request_type
					.getrt_pref_ins_date2_day();
			db_pref_inspection_date2_year = request_type
					.getrt_pref_ins_date2_year();

		}
	}

	public void set_data() {
		get_db_request();
		rt_cntrl_no.setText(db_cntrl_no);
		// nature of appraisal
		for (int x = 0; x < gs.appraisal_type_nature_appraisal.length; x++) {
			if (gs.appraisal_type_nature_appraisal[x]
					.equals(db_nature_appraisal)) {
				classification_position = x;
			}
		}
		rt_spinner_nature_appraisal.setSelection(classification_position);
		// appraisal type
		for (int x = 0; x < gs.appraisal_type_value.length; x++) {
			if (gs.appraisal_type_value[x].equals(db_appraisal_type)) {
				classification_position = x;
			}
		}
		rt_spinner_appraisal_type.setSelection(classification_position);
		year1 = Integer.parseInt(db_pref_inspection_date1_year);
		month1 = Integer.parseInt(db_pref_inspection_date1_month) - 1;
		day1 = Integer.parseInt(db_pref_inspection_date1_day);

		year2 = Integer.parseInt(db_pref_inspection_date2_year);
		month2 = Integer.parseInt(db_pref_inspection_date2_month) - 1;
		day2 = Integer.parseInt(db_pref_inspection_date2_day);
		// set date into datepicker
		rt_dp_inspection_date1.init(year1, month1, day1, null);
		rt_dp_inspection_date2.init(year2, month2, day2, null);

		rt_et_vehicle_type.setText(db_vehicle_type);
		rt_et_car_model.setText(db_car_model);
		gs.appraisal_type = db_appraisal_type.toString();
		if (db_appraisal_type.equals("motor_vehicle")) {
			rt_et_vehicle_type.setVisibility(View.VISIBLE);
			rt_et_car_model.setVisibility(View.VISIBLE);
			rt_tv_vehicle_type.setVisibility(View.VISIBLE);
			rt_tv_car_model.setVisibility(View.VISIBLE);
		} else {
			rt_et_vehicle_type.setVisibility(View.GONE);
			rt_et_car_model.setVisibility(View.GONE);
			rt_tv_vehicle_type.setVisibility(View.GONE);
			rt_tv_car_model.setVisibility(View.GONE);
		}
	}

	public void get_value() {
		cntrol_no = rt_cntrl_no.getText().toString();
		nature_appraisal = rt_spinner_nature_appraisal.getSelectedItem()
				.toString();
		appraisal_type = gs.appraisal_type_value[rt_spinner_appraisal_type
				.getSelectedItemPosition()];
		inspection_date1_month = String.valueOf(rt_dp_inspection_date1
				.getMonth() + 1);
		inspection_date1_day = String.valueOf(rt_dp_inspection_date1
				.getDayOfMonth());
		inspection_date1_year = String
				.valueOf(rt_dp_inspection_date1.getYear());
		inspection_date2_month = String.valueOf(rt_dp_inspection_date2
				.getMonth() + 1);
		inspection_date2_day = String.valueOf(rt_dp_inspection_date2
				.getDayOfMonth());
		inspection_date2_year = String
				.valueOf(rt_dp_inspection_date2.getYear());
		vehicle_type = rt_et_vehicle_type.getText().toString();
		car_model = rt_et_car_model.getText().toString();
	}

	public void value_global() {
		get_value();
		gs.cntrol_no = cntrol_no;
		gs.nature_appraisal = nature_appraisal;
		gs.appraisal_type = appraisal_type;
		if (inspection_date1_month.length() == 1) {
			gs.inspection_date1_month = "0" + inspection_date1_month;
		} else {
			gs.inspection_date1_month = inspection_date1_month;
		}
		gs.inspection_date1_day = inspection_date1_day;
		gs.inspection_date1_year = inspection_date1_year;
		if (inspection_date2_month.length() == 1) {
			gs.inspection_date2_month = "0" + inspection_date2_month;
		} else {
			gs.inspection_date2_month = inspection_date2_month;
		}
		gs.inspection_date2_day = inspection_date2_day;
		gs.inspection_date2_year = inspection_date2_year;
		gs.vehicle_type = vehicle_type;
		gs.car_model = car_model;
	}

	public void dialog() {
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:
					// Yes button clicked
					value_global();
					startActivity(new Intent(Edit_Request_Type.this,
							Edit_Request_Type2.class));
					finish();
					break;

				case DialogInterface.BUTTON_NEGATIVE:
					// No button clicked
					dialog.dismiss();
					break;
				}
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Are you sure on information ?")
				.setPositiveButton("Yes", dialogClickListener)
				.setNegativeButton("No", dialogClickListener).show();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.rt_btn_next:
			dialog();
			break;

		default:
			break;

		}
	}

}
