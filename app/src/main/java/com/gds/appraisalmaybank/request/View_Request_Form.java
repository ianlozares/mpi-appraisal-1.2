package com.gds.appraisalmaybank.request;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Request_Form;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;

public class View_Request_Form extends Activity implements OnClickListener {
	DatePicker rq_dp_datetoday;
	Spinner rq_spinner_calssification;
	EditText rq_et_fname, rq_et_mname, rq_et_lname, rq_et_requesting_party;
	Button rq_btn_register;
	DatabaseHandler db = new DatabaseHandler(this);
	String db_date_requested_month, db_date_requested_day,
			db_date_requested_year, db_classification, db_account_fname,
			db_account_mname, db_account_lname, db_requesting_party;
	Global gs;
	int request_form_record, month, year, day;
	int classification_position;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.request_form);

		rq_dp_datetoday = (DatePicker) findViewById(R.id.rq_dp_datetoday);
		rq_spinner_calssification = (Spinner) findViewById(R.id.rq_spinner_calssification);
		rq_et_fname = (EditText) findViewById(R.id.rq_et_fname);
		rq_et_mname = (EditText) findViewById(R.id.rq_et_mname);
		rq_et_lname = (EditText) findViewById(R.id.rq_et_lname);
		rq_et_requesting_party = (EditText) findViewById(R.id.rq_et_requesting_party);
		rq_btn_register = (Button) findViewById(R.id.rq_btn_register);

		gs = ((Global) getApplicationContext());

		rq_dp_datetoday.setEnabled(false);
		rq_spinner_calssification.setEnabled(false);
		rq_et_fname.setEnabled(false);
		rq_et_mname.setEnabled(false);
		rq_et_lname.setEnabled(false);
		rq_et_requesting_party.setEnabled(false);
		rq_btn_register.setText("Back");
		rq_btn_register.setOnClickListener(this);
		request_form_record = Integer.parseInt(gs.request_form_record);
		set_data();
	}

	public void get_db_request() {
		Request_Form request_form = db.getRequest_Form(request_form_record);
		if (request_form != null) {
			db_date_requested_month = request_form.getrf_date_requested_month();
			db_date_requested_day = request_form.getrf_date_requested_day();
			db_date_requested_year = request_form.getrf_date_requested_year();
			db_classification = request_form.getrf_classification();
			db_account_fname = request_form.getrf_account_fname();
			db_account_mname = request_form.getrf_account_mname();
			db_account_lname = request_form.getrf_account_lname();
			db_requesting_party = request_form.getrf_requesting_party();

		}
	}

	public void set_data() {
		get_db_request();
		year = Integer.parseInt(db_date_requested_year);
		month = Integer.parseInt(db_date_requested_month) - 1;
		day = Integer.parseInt(db_date_requested_day);
		// set date into datepicker
		rq_dp_datetoday.init(year, month, day, null);
		// classification
		for (int x = 0; x < gs.appraisal_form_classification.length; x++) {
			if (gs.appraisal_form_classification[x].equals(db_classification)) {
				classification_position = x;
			}
		}
		rq_spinner_calssification.setSelection(classification_position);
		rq_et_fname.setText(db_account_fname);
		rq_et_mname.setText(db_account_mname);
		rq_et_lname.setText(db_account_lname);
		rq_et_requesting_party.setText(db_requesting_party);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.rq_btn_register:
			finish();
			break;

		default:
			break;

		}

	}
}
