package com.gds.appraisalmaybank.townhouse_condo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.check_network.NetworkUtil;
import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Images;
import com.gds.appraisalmaybank.database.Report_Accepted_Jobs;
import com.gds.appraisalmaybank.database.Report_Accepted_Jobs_Contacts;
import com.gds.appraisalmaybank.database.Report_filename;
import com.gds.appraisalmaybank.database.Required_Attachments;
import com.gds.appraisalmaybank.database.Townhouse_Condo_API;
import com.gds.appraisalmaybank.database.Vacant_Lot_API;
import com.gds.appraisalmaybank.json.MyHttpClient;
import com.gds.appraisalmaybank.json.UserFunctions;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Townhouse_condo extends Activity implements OnClickListener {
	TextView tv_requested_month, tv_requested_day, tv_requested_year,
			tv_classification, tv_account_name, tv_requesting_party,
			tv_control_no, tv_appraisal_type, tv_nature_appraisal,
			tv_ins_month1, tv_ins_day1, tv_ins_year1, tv_ins_month2,
			tv_ins_day2, tv_ins_year2, tv_com_month, tv_com_day, tv_com_year,
			tv_tct_no, tv_unit_no, tv_bldg_name, tv_street_no, tv_street_name,
			tv_village, tv_district, tv_zip_code, tv_city, tv_province,
			tv_region, tv_country, tv_address, tv_attachment;
	Session_Timer st = new Session_Timer(this);
	LinearLayout ll_contact;
	DatabaseHandler db = new DatabaseHandler(this);
	Global gs;
	GDS_methods gds = new GDS_methods();
	String requested_month, requested_day, requested_year, classification,
			account_fname, account_mname, account_lname, requesting_party,
			control_no, appraisal_type, nature_appraisal, ins_month1, ins_day1,
			ins_year1, ins_month2, ins_day2, ins_year2, com_month, com_day,
			com_year, tct_no, unit_no, bldg_name, street_no, street_name,
			village, district, zip_code, city, province, region, country,
			counter, output_appraisal_type, output_street_name, output_village,
			output_city, output_province;
	String record_id, pdf_name;
	ImageView btn_report_page1, btn_report_page2, btn_report_page3;
	Button btn_report_update_cc;
	ProgressDialog pDialog;
	ImageView btn_select_pdf, btn_report_pdf;

	// for attachments
	int responseCode;
	String google_response = "";
	ListView dlg_priority_lvw = null;
	Context context = this;
	String item, filename;
	String dir = "gds_appraisal";
	File myDirectory = new File(Environment.getExternalStorageDirectory() + "/"
			+ dir + "/");
	String attachment_selected, uid, file, candidate_done;
	int serverResponseCode = 0;
	String db_app_uid, db_app_file, db_app_filename, db_app_appraisal_type,
			db_app_candidate_done;
	// page1
	Dialog myDialog;
	EditText report_cct_no, report_area, report_reg_owner;
	DatePicker report_date_reg, report_date_inspected;
	CheckBox report_homeowners_assoc, report_admin_office, report_tax_mapping,
			report_lra_office, report_subd_map, report_lot_config;
	RadioButton report_street_concrete, report_street_macadem,
			report_sidewalk_concrete, report_sidewalk_unpaved,
			report_gutter_concrete, report_gutter_unpaved,
			report_street_lights_public_private, report_street_lights_none;
	RadioGroup radio_street, radio_sidewalk, radio_gutter, radio_street_lights;
	Button btn_update_page1;
	// page2
	EditText report_shape, report_elevation, report_road, report_frontage,
			report_depth;
	EditText report_electricity, report_water, report_telephone,
			report_drainage, report_garbage;
	CheckBox report_jeepneys, report_bus, report_taxi, report_tricycle;
	CheckBox report_recreation_facilities, report_security, report_clubhouse,
			report_swimming_pool;
	Button btn_update_page2;
	// page3
	EditText report_community, report_classification, report_growth_rate,
			report_adverse, report_occupancy;
	EditText report_right, report_left, report_rear, report_front;
	EditText report_desc, report_total_area, report_deduc, report_net_area,
			report_unit_value, report_total_parking_area,
			report_parking_area_value, report_total_condo_value,
			report_total_parking_value, report_total_market_value;
	Button btn_update_page3;
	// uploaded attachments
	LinearLayout container_attachments;
	String uploaded_attachment;
	String url_webby;
	UserFunctions userFunction = new UserFunctions();
	File uploaded_file;
	String[] commandArray = new String[] { "View Attachment in Browser",
			"Download Attachment" };
	// for API
	String xml;
	DefaultHttpClient httpClient = new MyHttpClient(this);
	boolean network_status;
	// Progress dialog type (0 - for Horizontal progress bar)
	public static final int progress_bar_type = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.townhouse_condo);
		st.resetDisconnectTimer();
		gs = ((Global) getApplicationContext());
		container_attachments = (LinearLayout) findViewById(R.id.container_attachments);
		ll_contact = (LinearLayout) findViewById(R.id.container);
		tv_requested_month = (TextView) findViewById(R.id.tv_requested_month);
		tv_requested_day = (TextView) findViewById(R.id.tv_requested_day);
		tv_requested_year = (TextView) findViewById(R.id.tv_requested_year);
		tv_classification = (TextView) findViewById(R.id.tv_classification);
		tv_account_name = (TextView) findViewById(R.id.tv_account_name);
		tv_requesting_party = (TextView) findViewById(R.id.tv_requesting_party);
		tv_control_no = (TextView) findViewById(R.id.tv_control_no);
		tv_appraisal_type = (TextView) findViewById(R.id.tv_appraisal_type);
		tv_nature_appraisal = (TextView) findViewById(R.id.tv_nature_appraisal);
		tv_ins_month1 = (TextView) findViewById(R.id.tv_ins_month1);
		tv_ins_day1 = (TextView) findViewById(R.id.tv_ins_day1);
		tv_ins_year1 = (TextView) findViewById(R.id.tv_ins_year1);
		tv_ins_month2 = (TextView) findViewById(R.id.tv_ins_month2);
		tv_ins_day2 = (TextView) findViewById(R.id.tv_ins_day2);
		tv_ins_year2 = (TextView) findViewById(R.id.tv_ins_year2);
		tv_com_month = (TextView) findViewById(R.id.tv_com_month);
		tv_com_day = (TextView) findViewById(R.id.tv_com_day);
		tv_com_year = (TextView) findViewById(R.id.tv_com_year);
		tv_tct_no = (TextView) findViewById(R.id.tv_tct_no);
		tv_unit_no = (TextView) findViewById(R.id.tv_unit_no);
		tv_bldg_name = (TextView) findViewById(R.id.tv_bldg_name);
		tv_street_no = (TextView) findViewById(R.id.tv_street_no);
		tv_street_name = (TextView) findViewById(R.id.tv_street_name);
		tv_village = (TextView) findViewById(R.id.tv_village);
		tv_district = (TextView) findViewById(R.id.tv_district);
		tv_zip_code = (TextView) findViewById(R.id.tv_zip_code);
		tv_city = (TextView) findViewById(R.id.tv_city);
		tv_province = (TextView) findViewById(R.id.tv_province);
		tv_region = (TextView) findViewById(R.id.tv_region);
		tv_country = (TextView) findViewById(R.id.tv_country);
		tv_address = (TextView) findViewById(R.id.tv_address);
		tv_attachment = (TextView) findViewById(R.id.tv_attachment);
		btn_report_page1 = (ImageView) findViewById(R.id.btn_report_page1);
		btn_report_page2 = (ImageView) findViewById(R.id.btn_report_page2);
		btn_report_page3 = (ImageView) findViewById(R.id.btn_report_page3);
		btn_report_update_cc = (Button) findViewById(R.id.btn_report_update_cc);
		btn_report_pdf = (ImageView) findViewById(R.id.btn_report_pdf);
		btn_select_pdf = (ImageView) findViewById(R.id.btn_select_pdf);
		btn_report_page1.setOnClickListener(this);
		btn_report_page2.setOnClickListener(this);
		btn_report_page3.setOnClickListener(this);
		btn_report_update_cc.setOnClickListener(this);
		btn_report_pdf.setOnClickListener(this);
		btn_select_pdf.setOnClickListener(this);
		tv_attachment.setOnClickListener(this);
		record_id = gs.record_id;
		fill_details();
		uploaded_attachments();
	}

	public void fill_details() {
		List<Report_Accepted_Jobs> report_accepted_jobs = db
				.getReport_Accepted_Jobs(record_id);
		if (!report_accepted_jobs.isEmpty()) {
			for (Report_Accepted_Jobs im : report_accepted_jobs) {
				requested_month = im.getdr_month();
				requested_day = im.getdr_day();
				requested_year = im.getdr_year();
				classification = im.getclassification();
				account_fname = im.getfname();
				account_mname = im.getmname();
				account_lname = im.getlname();
				requesting_party = im.getrequesting_party();
				control_no = im.getcontrol_no();
				appraisal_type = im.getappraisal_type();
				nature_appraisal = im.getnature_appraisal();
				ins_month1 = im.getins_date1_month();
				ins_day1 = im.getins_date1_day();
				ins_year1 = im.getins_date1_year();
				ins_month2 = im.getins_date2_month();
				ins_day2 = im.getins_date2_day();
				ins_year2 = im.getins_date2_year();
				com_month = im.getcom_month();
				com_day = im.getcom_day();
				com_year = im.getcom_year();
				tct_no = im.gettct_no();
				unit_no = im.getunit_no();
				bldg_name = im.getbuilding_name();
				street_no = im.getstreet_no();
				street_name = im.getstreet_name();
				village = im.getvillage();
				district = im.getdistrict();
				zip_code = im.getzip_code();
				city = im.getcity();
				province = im.getprovince();
				region = im.getregion();
				country = im.getcountry();
				counter = im.getcounter();
			}

			List<Report_Accepted_Jobs_Contacts> report_Accepted_Jobs_Contacts = db
					.getReport_Accepted_Jobs_Contacts(record_id);
			if (!report_Accepted_Jobs_Contacts.isEmpty()) {
				for (Report_Accepted_Jobs_Contacts im : report_Accepted_Jobs_Contacts) {
					LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
							.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					final View addView = layoutInflater.inflate(
							R.layout.request_jobs_info_contact_layout, null);

					final TextView tv_contact_person = (TextView) addView
							.findViewById(R.id.tv_contact_person);
					final TextView tv_contact_prefix = (TextView) addView
							.findViewById(R.id.tv_contact_prefix);
					final TextView tv_contact_mobile = (TextView) addView
							.findViewById(R.id.tv_contact_mobile);
					final TextView tv_contact_landline = (TextView) addView
							.findViewById(R.id.tv_contact_landline);

					tv_contact_person.setText(im.getcontact_person());
					tv_contact_prefix.setText(im.getcontact_mobile_no_prefix());
					tv_contact_mobile.setText(im.getcontact_mobile_no());
					tv_contact_landline.setText(im.getcontact_landline());
					ll_contact.addView(addView);
				}
			}
			List<Report_filename> rf = db.getReport_filename(record_id);
			if (!rf.isEmpty()) {
				for (Report_filename im : rf) {
					tv_attachment.setText(im.getfilename());
				}
			}
			tv_requested_month.setText(requested_month + "/");
			tv_requested_day.setText(requested_day + "/");
			tv_requested_year.setText(requested_year);
			tv_classification.setText(classification);
			tv_account_name.setText(account_fname + " " + account_mname + " "
					+ account_lname);
			tv_requesting_party.setText(requesting_party);
			tv_control_no.setText(control_no);
			for (int x = 0; x < gs.appraisal_type_value.length; x++) {
				if (appraisal_type.equals(gs.appraisal_type_value[x])) {
					output_appraisal_type = gs.appraisal_output[x];
				}
			}
			tv_appraisal_type.setText(output_appraisal_type);
			tv_nature_appraisal.setText(nature_appraisal);
			tv_ins_month1.setText(ins_month1 + "/");
			tv_ins_day1.setText(ins_day1 + "/");
			tv_ins_year1.setText(ins_year1);
			tv_ins_month2.setText(ins_month2 + "/");
			tv_ins_day2.setText(ins_day2 + "/");
			tv_ins_year2.setText(ins_year2);
			tv_com_month.setText(com_month + "/");
			tv_com_day.setText(com_day + "/");
			tv_com_year.setText(com_year);

			tv_tct_no.setText(tct_no);
			tv_unit_no.setText(unit_no);
			tv_bldg_name.setText(bldg_name);
			tv_street_no.setText(street_no);
			tv_street_name.setText(street_name);
			tv_village.setText(village);
			tv_district.setText(district);
			tv_zip_code.setText(zip_code);
			tv_city.setText(city);
			tv_province.setText(province);
			tv_region.setText(region);
			tv_country.setText(country);
			if (!street_name.equals("")) {
				output_street_name = street_name + ", ";
			} else {
				output_street_name = street_name;
			}
			if (!village.equals("")) {
				output_village = village + ", ";
			} else {
				output_village = village;
			}
			if (!city.equals("")) {
				output_city = city + ", ";
			} else {
				output_city = city;
			}
			if (!province.equals("")) {
				output_province = province + ", ";
			} else {
				output_province = province;
			}
			tv_address.setText(unit_no + " " + bldg_name + " " + street_no
					+ " " + output_street_name + output_village + district
					+ " " + output_city + region + " " + output_province
					+ country + " " + zip_code);
		}
	}

	public void custom_dialog_page1() {
		myDialog = new Dialog(Townhouse_condo.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.report_townhouse_condo_page1);
		// myDialog.setTitle("My Dialog");
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.FILL_PARENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		report_date_inspected = (DatePicker) myDialog
				.findViewById(R.id.report_date_inspected);
		report_cct_no = (EditText) myDialog.findViewById(R.id.report_cct_no);
		report_area = (EditText) myDialog.findViewById(R.id.report_area);
		report_reg_owner = (EditText) myDialog
				.findViewById(R.id.report_reg_owner);
		report_date_reg = (DatePicker) myDialog
				.findViewById(R.id.report_date_reg);
		report_homeowners_assoc = (CheckBox) myDialog
				.findViewById(R.id.report_homeowners_assoc);
		report_admin_office = (CheckBox) myDialog
				.findViewById(R.id.report_admin_office);
		report_tax_mapping = (CheckBox) myDialog
				.findViewById(R.id.report_tax_mapping);
		report_lra_office = (CheckBox) myDialog
				.findViewById(R.id.report_lra_office);
		report_subd_map = (CheckBox) myDialog
				.findViewById(R.id.report_subd_map);
		report_lot_config = (CheckBox) myDialog
				.findViewById(R.id.report_lot_config);
		report_street_concrete = (RadioButton) myDialog
				.findViewById(R.id.report_street_concrete);
		report_street_macadem = (RadioButton) myDialog
				.findViewById(R.id.report_street_macadem);
		report_sidewalk_concrete = (RadioButton) myDialog
				.findViewById(R.id.report_sidewalk_concrete);
		report_sidewalk_unpaved = (RadioButton) myDialog
				.findViewById(R.id.report_sidewalk_unpaved);
		report_gutter_concrete = (RadioButton) myDialog
				.findViewById(R.id.report_gutter_concrete);
		report_gutter_unpaved = (RadioButton) myDialog
				.findViewById(R.id.report_gutter_unpaved);
		report_street_lights_public_private = (RadioButton) myDialog
				.findViewById(R.id.report_street_lights_public_private);
		report_street_lights_none = (RadioButton) myDialog
				.findViewById(R.id.report_street_lights_none);

		btn_update_page1 = (Button) myDialog
				.findViewById(R.id.btn_update_page1);
		btn_update_page1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// update
				Townhouse_Condo_API tc = new Townhouse_Condo_API();
				tc.setreport_cct_no(report_cct_no.getText().toString());
				tc.setreport_area(report_area.getText().toString());
				if (String.valueOf(report_date_inspected.getMonth()).length() == 1) {
					tc.setreport_date_inspected_month("0"
							+ String.valueOf(report_date_inspected.getMonth() + 1));
				} else {
					tc.setreport_date_inspected_month(String
							.valueOf(report_date_inspected.getMonth() + 1));
				}

				tc.setreport_date_inspected_day(String
						.valueOf(report_date_inspected.getDayOfMonth()));
				tc.setreport_date_inspected_year(String
						.valueOf(report_date_inspected.getYear()));

				if (String.valueOf(report_date_reg.getMonth()).length() == 1) {
					tc.setreport_date_reg_month("0"
							+ String.valueOf(report_date_reg.getMonth() + 1));
				} else {
					tc.setreport_date_reg_month(String.valueOf(report_date_reg
							.getMonth() + 1));
				}
				tc.setreport_date_reg_day(String.valueOf(report_date_reg
						.getDayOfMonth()));
				tc.setreport_date_reg_year(String.valueOf(report_date_reg
						.getYear()));
				tc.setreport_reg_owner(report_reg_owner.getText().toString());
				// checkbox
				if (report_homeowners_assoc.isChecked()) {
					tc.setreport_homeowners_assoc("true");
				} else {
					tc.setreport_homeowners_assoc("");
				}
				if (report_admin_office.isChecked()) {
					tc.setreport_admin_office("true");
				} else {
					tc.setreport_admin_office("");
				}
				if (report_tax_mapping.isChecked()) {
					tc.setreport_tax_mapping("true");
				} else {
					tc.setreport_tax_mapping("");
				}
				if (report_lra_office.isChecked()) {
					tc.setreport_lra_office("true");
				} else {
					tc.setreport_lra_office("");
				}
				if (report_subd_map.isChecked()) {
					tc.setreport_subd_map("true");
				} else {
					tc.setreport_subd_map("");
				}
				if (report_lot_config.isChecked()) {
					tc.setreport_lot_config("true");
				} else {
					tc.setreport_lot_config("");
				}
				// radio
				if (report_street_concrete.isChecked()) {
					tc.setreport_street("Concrete");
				} else if (report_street_macadem.isChecked()) {
					tc.setreport_street("Macadem");
				} else {
					tc.setreport_street("");
				}

				if (report_sidewalk_concrete.isChecked()) {
					tc.setreport_sidewalk("Concrete");
				} else if (report_sidewalk_unpaved.isChecked()) {
					tc.setreport_sidewalk("Unpaved");
				} else {
					tc.setreport_sidewalk("");
				}

				if (report_gutter_concrete.isChecked()) {
					tc.setreport_gutter("Concrete");
				} else if (report_gutter_unpaved.isChecked()) {
					tc.setreport_gutter("Unpaved");
				} else {
					tc.setreport_gutter("");
				}

				if (report_street_lights_public_private.isChecked()) {
					tc.setreport_street_lights("Public_Private");
				} else if (report_street_lights_none.isChecked()) {
					tc.setreport_street_lights("None");
				} else {
					tc.setreport_street_lights("");
				}

				db.updateTownhouse_Condo_page1(tc, record_id);
				db.close();
				Toast.makeText(getApplicationContext(), "Saved",
						Toast.LENGTH_SHORT).show();
				myDialog.dismiss();
			}
		});
		List<Townhouse_Condo_API> townhouse_condo = db
				.getTownhouse_Condo(record_id);
		if (!townhouse_condo.isEmpty()) {
			for (Townhouse_Condo_API im : townhouse_condo) {

				report_cct_no.setText(im.getreport_cct_no());
				report_area.setText(im.getreport_area());
				report_reg_owner.setText(im.getreport_reg_owner());
				// set current date into datepicker
				if (!im.getreport_date_reg_year().equals("")) {
					report_date_reg
							.init(Integer
									.parseInt(im.getreport_date_reg_year()),
									Integer.parseInt(im
											.getreport_date_reg_month()) - 1,
									Integer.parseInt(im
											.getreport_date_reg_day()), null);
				}
				if (!im.getreport_date_inspected_month().equals("")) {
					report_date_inspected
							.init(Integer.parseInt(im
									.getreport_date_inspected_year()),
									Integer.parseInt(im
											.getreport_date_inspected_month()) - 1,
									Integer.parseInt(im
											.getreport_date_inspected_day()),
									null);
				}

				if (im.getreport_homeowners_assoc().equals("true")) {
					report_homeowners_assoc.setChecked(true);
				}
				if (im.getreport_admin_office().equals("true")) {
					report_admin_office.setChecked(true);
				}
				if (im.getreport_tax_mapping().equals("true")) {
					report_tax_mapping.setChecked(true);
				}
				if (im.getreport_lra_office().equals("true")) {
					report_lra_office.setChecked(true);
				}

				if (im.getreport_subd_map().equals("true")) {
					report_subd_map.setChecked(true);
				}
				if (im.getreport_lot_config().equals("true")) {
					report_lot_config.setChecked(true);
				}

				if (im.getreport_street().equals("Concrete")) {
					report_street_concrete.setChecked(true);
				} else if (im.getreport_street().equals("Macadem")) {
					report_street_macadem.setChecked(true);
				}
				if (im.getreport_sidewalk().equals("Concrete")) {
					report_sidewalk_concrete.setChecked(true);
				} else if (im.getreport_sidewalk().equals("Unpaved")) {
					report_sidewalk_unpaved.setChecked(true);
				}
				if (im.getreport_gutter().equals("Concrete")) {
					report_gutter_concrete.setChecked(true);
				} else if (im.getreport_gutter().equals("Unpaved")) {
					report_gutter_unpaved.setChecked(true);
				}
				if (im.getreport_street_lights().equals("Public_Private")) {
					report_street_lights_public_private.setChecked(true);
				} else if (im.getreport_street_lights().equals("None")) {
					report_street_lights_none.setChecked(true);
				}

			}
		}

		myDialog.show();
	}

	public void custom_dialog_page2() {
		myDialog = new Dialog(Townhouse_condo.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.report_townhouse_condo_page2);
		// myDialog.setTitle("My Dialog");
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.FILL_PARENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);

		report_shape = (EditText) myDialog.findViewById(R.id.report_shape);
		report_elevation = (EditText) myDialog
				.findViewById(R.id.report_elevation);
		report_road = (EditText) myDialog.findViewById(R.id.report_road);
		report_frontage = (EditText) myDialog
				.findViewById(R.id.report_frontage);
		report_depth = (EditText) myDialog.findViewById(R.id.report_depth);
		report_electricity = (EditText) myDialog
				.findViewById(R.id.report_electricity);
		report_water = (EditText) myDialog.findViewById(R.id.report_water);
		report_telephone = (EditText) myDialog
				.findViewById(R.id.report_telephone);
		report_drainage = (EditText) myDialog
				.findViewById(R.id.report_drainage);
		report_garbage = (EditText) myDialog.findViewById(R.id.report_garbage);
		report_jeepneys = (CheckBox) myDialog
				.findViewById(R.id.report_jeepneys);
		report_bus = (CheckBox) myDialog.findViewById(R.id.report_bus);
		report_taxi = (CheckBox) myDialog.findViewById(R.id.report_taxi);
		report_tricycle = (CheckBox) myDialog
				.findViewById(R.id.report_tricycle);
		report_recreation_facilities = (CheckBox) myDialog
				.findViewById(R.id.report_recreation_facilities);
		report_security = (CheckBox) myDialog
				.findViewById(R.id.report_security);
		report_clubhouse = (CheckBox) myDialog
				.findViewById(R.id.report_clubhouse);
		report_swimming_pool = (CheckBox) myDialog
				.findViewById(R.id.report_swimming_pool);
		btn_update_page2 = (Button) myDialog
				.findViewById(R.id.btn_update_page2);
		btn_update_page2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Townhouse_Condo_API tc = new Townhouse_Condo_API();
				tc.setreport_shape(report_shape.getText().toString());
				tc.setreport_elevation(report_elevation.getText().toString());
				tc.setreport_road(report_road.getText().toString());
				tc.setreport_frontage(report_frontage.getText().toString());
				tc.setreport_depth(report_depth.getText().toString());
				tc.setreport_electricity(report_electricity.getText()
						.toString());
				tc.setreport_water(report_water.getText().toString());
				tc.setreport_telephone(report_telephone.getText().toString());
				tc.setreport_drainage(report_drainage.getText().toString());
				tc.setreport_garbage(report_garbage.getText().toString());

				if (report_jeepneys.isChecked()) {
					tc.setreport_jeepneys("true");
				} else {
					tc.setreport_jeepneys("");
				}
				if (report_bus.isChecked()) {
					tc.setreport_bus("true");
				} else {
					tc.setreport_bus("");
				}
				if (report_taxi.isChecked()) {
					tc.setreport_taxi("true");
				} else {
					tc.setreport_taxi("");
				}
				if (report_tricycle.isChecked()) {
					tc.setreport_tricycle("true");
				} else {
					tc.setreport_tricycle("");
				}
				if (report_recreation_facilities.isChecked()) {
					tc.setreport_recreation_facilities("true");
				} else {
					tc.setreport_recreation_facilities("");
				}
				if (report_security.isChecked()) {
					tc.setreport_security("true");
				} else {
					tc.setreport_security("");
				}
				if (report_clubhouse.isChecked()) {
					tc.setreport_clubhouse("true");
				} else {
					tc.setreport_clubhouse("");
				}
				if (report_swimming_pool.isChecked()) {
					tc.setreport_swimming_pool("true");
				} else {
					tc.setreport_swimming_pool("");
				}
				db.updateTownhouse_Condo_page2(tc, record_id);
				db.close();
				Toast.makeText(getApplicationContext(), "Saved",
						Toast.LENGTH_SHORT).show();
				myDialog.dismiss();
			}
		});
		List<Townhouse_Condo_API> townhouse_condo = db
				.getTownhouse_Condo(record_id);
		if (!townhouse_condo.isEmpty()) {
			for (Townhouse_Condo_API im : townhouse_condo) {
				report_shape.setText(im.getreport_shape());
				report_elevation.setText(im.getreport_elevation());
				report_road.setText(im.getreport_road());
				report_frontage.setText(im.getreport_frontage());
				report_depth.setText(im.getreport_depth());
				report_electricity.setText(im.getreport_electricity());
				report_water.setText(im.getreport_water());
				report_telephone.setText(im.getreport_telephone());
				report_drainage.setText(im.getreport_drainage());
				report_garbage.setText(im.getreport_garbage());

				if (im.getreport_jeepneys().equals("true")) {
					report_jeepneys.setChecked(true);
				}
				if (im.getreport_bus().equals("true")) {
					report_bus.setChecked(true);
				}
				if (im.getreport_taxi().equals("true")) {
					report_taxi.setChecked(true);
				}
				if (im.getreport_tricycle().equals("true")) {
					report_tricycle.setChecked(true);
				}
				if (im.getreport_recreation_facilities().equals("true")) {
					report_recreation_facilities.setChecked(true);
				}
				if (im.getreport_security().equals("true")) {
					report_security.setChecked(true);
				}
				if (im.getreport_clubhouse().equals("true")) {
					report_clubhouse.setChecked(true);
				}
				if (im.getreport_swimming_pool().equals("true")) {
					report_swimming_pool.setChecked(true);
				}

			}
		}

		myDialog.show();
	}

	public void custom_dialog_page3() {
		myDialog = new Dialog(Townhouse_condo.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.report_townhouse_condo_page3);
		// myDialog.setTitle("My Dialog");
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.FILL_PARENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);

		report_community = (EditText) myDialog
				.findViewById(R.id.report_community);
		report_classification = (EditText) myDialog
				.findViewById(R.id.report_classification);
		report_growth_rate = (EditText) myDialog
				.findViewById(R.id.report_growth_rate);
		report_adverse = (EditText) myDialog.findViewById(R.id.report_adverse);
		report_occupancy = (EditText) myDialog
				.findViewById(R.id.report_occupancy);

		report_right = (EditText) myDialog.findViewById(R.id.report_right);
		report_left = (EditText) myDialog.findViewById(R.id.report_left);
		report_rear = (EditText) myDialog.findViewById(R.id.report_rear);
		report_front = (EditText) myDialog.findViewById(R.id.report_front);

		report_desc = (EditText) myDialog.findViewById(R.id.report_desc);
		report_total_area = (EditText) myDialog
				.findViewById(R.id.report_total_area);
		report_deduc = (EditText) myDialog.findViewById(R.id.report_deduc);
		report_net_area = (EditText) myDialog
				.findViewById(R.id.report_net_area);
		report_unit_value = (EditText) myDialog
				.findViewById(R.id.report_unit_value);
		report_total_parking_area = (EditText) myDialog
				.findViewById(R.id.report_total_parking_area);
		report_parking_area_value = (EditText) myDialog
				.findViewById(R.id.report_parking_area_value);
		report_total_condo_value = (EditText) myDialog
				.findViewById(R.id.report_total_condo_value);
		report_total_parking_value = (EditText) myDialog
				.findViewById(R.id.report_total_parking_value);
		report_total_market_value = (EditText) myDialog
				.findViewById(R.id.report_total_market_value);
		btn_update_page3 = (Button) myDialog
				.findViewById(R.id.btn_update_page3);
		btn_update_page3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Townhouse_Condo_API tc = new Townhouse_Condo_API();
				tc.setreport_community(report_community.getText().toString());
				tc.setreport_classification(report_classification.getText()
						.toString());
				tc.setreport_growth_rate(report_growth_rate.getText()
						.toString());
				tc.setreport_adverse(report_adverse.getText().toString());
				tc.setreport_occupancy(report_occupancy.getText().toString());

				tc.setreport_right(report_right.getText().toString());
				tc.setreport_left(report_left.getText().toString());
				tc.setreport_rear(report_rear.getText().toString());
				tc.setreport_front(report_front.getText().toString());

				tc.setreport_desc(report_desc.getText().toString());
				tc.setreport_total_area(report_total_area.getText().toString());
				tc.setreport_deduc(report_deduc.getText().toString());
				tc.setreport_net_area(report_net_area.getText().toString());
				tc.setreport_unit_value(report_unit_value.getText().toString());
				tc.setreport_total_parking_area(report_total_parking_area
						.getText().toString());
				tc.setreport_parking_area_value(report_parking_area_value
						.getText().toString());
				tc.setreport_total_condo_value(report_total_condo_value
						.getText().toString());
				tc.setreport_total_parking_value(report_total_parking_value
						.getText().toString());
				tc.setreport_total_market_value(report_total_market_value
						.getText().toString());

				db.updateTownhouse_Condo_page3(tc, record_id);
				db.close();
				Toast.makeText(getApplicationContext(), "Saved",
						Toast.LENGTH_SHORT).show();
				myDialog.dismiss();
			}
		});
		List<Townhouse_Condo_API> townhouse_condo = db
				.getTownhouse_Condo(record_id);
		if (!townhouse_condo.isEmpty()) {
			for (Townhouse_Condo_API im : townhouse_condo) {
				report_community.setText(im.getreport_community());
				report_classification.setText(im.getreport_classification());
				report_growth_rate.setText(im.getreport_growth_rate());
				report_adverse.setText(im.getreport_adverse());
				report_occupancy.setText(im.getreport_occupancy());

				report_right.setText(im.getreport_right());
				report_left.setText(im.getreport_left());
				report_rear.setText(im.getreport_rear());
				report_front.setText(im.getreport_front());

				report_desc.setText(im.getreport_desc());
				report_total_area.setText(im.getreport_total_area());
				report_deduc.setText(im.getreport_deduc());
				report_net_area.setText(im.getreport_net_area());
				report_unit_value.setText(im.getreport_unit_value());
				report_total_parking_area.setText(im
						.getreport_total_parking_area());
				report_parking_area_value.setText(im
						.getreport_parking_area_value());
				report_total_condo_value.setText(im
						.getreport_total_condo_value());
				report_total_parking_value.setText(im
						.getreport_total_parking_value());
				report_total_market_value.setText(im
						.getreport_total_market_value());

			}
		}

		myDialog.show();
	}

	public void create_json() {
		List<Townhouse_Condo_API> townhouse_condo = db
				.getTownhouse_Condo(record_id);
		if (!townhouse_condo.isEmpty()) {
			for (Townhouse_Condo_API im : townhouse_condo) {
				JSONObject query_temp = new JSONObject();
				JSONObject report = new JSONObject();
				JSONObject record_temp = new JSONObject();
				JSONObject appraisal_attachments = new JSONObject();
				JSONObject appraisal_attachments_data = new JSONObject();
				JSONArray app_attachments = new JSONArray();
				try {
					query_temp.put("system.record_id", record_id);
					// report.put("application_status", "report_review");
					report.put("valrep_condo_bound_front", im.getreport_front());
					report.put("valrep_condo_bound_left", im.getreport_left());
					report.put("valrep_condo_bound_rear", im.getreport_rear());
					report.put("valrep_condo_bound_right", im.getreport_right());
					report.put("valrep_condo_faci_clubhouse",
							im.getreport_clubhouse());
					report.put("valrep_condo_faci_pool",
							im.getreport_swimming_pool());
					report.put("valrep_condo_faci_recreation",
							im.getreport_recreation_facilities());
					report.put("valrep_condo_faci_security",
							im.getreport_security());
					report.put("valrep_condo_hood_adverse",
							im.getreport_adverse());
					report.put("valrep_condo_hood_classification",
							im.getreport_classification());
					report.put("valrep_condo_hood_community",
							im.getreport_community());
					report.put("valrep_condo_hood_growth_rate",
							im.getreport_growth_rate());
					report.put("valrep_condo_hood_occupancy",
							im.getreport_occupancy());
					report.put("valrep_condo_id_admin",
							im.getreport_admin_office());
					report.put("valrep_condo_id_association",
							im.getreport_homeowners_assoc());
					report.put("valrep_condo_id_lot_config",
							im.getreport_lot_config());
					report.put("valrep_condo_id_lra", im.getreport_lra_office());
					report.put("valrep_condo_id_subd_map",
							im.getreport_subd_map());
					report.put("valrep_condo_id_tax",
							im.getreport_tax_mapping());
					report.put("valrep_condo_imp_curb", im.getreport_gutter());
					report.put("valrep_condo_imp_sidewalk",
							im.getreport_sidewalk());
					report.put("valrep_condo_imp_street", im.getreport_street());
					report.put("valrep_condo_imp_street_lights",
							im.getreport_street_lights());
					report.put("valrep_condo_physical_depth",
							im.getreport_depth());
					report.put("valrep_condo_physical_elevation",
							im.getreport_elevation());
					report.put("valrep_condo_physical_frontage",
							im.getreport_frontage());
					report.put("valrep_condo_physical_road",
							im.getreport_road());
					report.put("valrep_condo_physical_shape",
							im.getreport_shape());
					report.put("valrep_condo_propdesc_area",
							im.getreport_area());
					report.put("valrep_condo_propdesc_cct_no",
							im.getreport_cct_no());
					report.put("valrep_condo_propdesc_registered_owner",
							im.getreport_reg_owner());
					report.put("valrep_condo_propdesc_registry_date_day",
							im.getreport_date_reg_day());
					report.put("valrep_condo_propdesc_registry_date_month",
							im.getreport_date_reg_month());
					report.put("valrep_condo_propdesc_registry_date_year",
							im.getreport_date_reg_year());
					report.put("valrep_condo_trans_bus", im.getreport_bus());
					report.put("valrep_condo_trans_jeep",
							im.getreport_jeepneys());
					report.put("valrep_condo_trans_taxi", im.getreport_taxi());
					report.put("valrep_condo_trans_tricycle",
							im.getreport_tricycle());
					report.put("valrep_condo_util_drainage",
							im.getreport_drainage());
					report.put("valrep_condo_util_electricity",
							im.getreport_electricity());
					report.put("valrep_condo_util_garbage",
							im.getreport_garbage());
					report.put("valrep_condo_util_telephone",
							im.getreport_telephone());
					report.put("valrep_condo_util_water", im.getreport_water());
					report.put("valrep_condo_value_deduction",
							im.getreport_deduc());
					report.put("valrep_condo_value_desc", im.getreport_desc());
					report.put("valrep_condo_value_net_area",
							im.getreport_net_area());
					report.put("valrep_condo_value_parking_value",
							im.getreport_parking_area_value());
					report.put("valrep_condo_value_total_area",
							im.getreport_total_area());
					report.put("valrep_condo_value_total_condo_value",
							im.getreport_total_condo_value());
					report.put("valrep_condo_value_total_market_value",
							im.getreport_total_market_value());
					report.put("valrep_condo_value_total_parking_area",
							im.getreport_total_parking_area());
					report.put("valrep_condo_value_total_parking_value",
							im.getreport_total_parking_value());
					report.put("valrep_condo_value_unit_value",
							im.getreport_unit_value());
					report.put("valrep_condo_date_inspected_month",
							im.getreport_date_inspected_month());
					report.put("valrep_condo_date_inspected_day",
							im.getreport_date_inspected_day());
					report.put("valrep_condo_date_inspected_year",
							im.getreport_date_inspected_year());
					// for whole record
					record_temp.put("record", report);

					// for attachment
					// attachments
					List<Report_filename> rf = db.getReport_filename(record_id);
					if (!rf.isEmpty()) {
						for (Report_filename report_file : rf) {
							db_app_uid = report_file.getuid();
							db_app_file = report_file.getfile();
							db_app_filename = report_file.getfilename();
							db_app_appraisal_type = report_file
									.getappraisal_type();
							db_app_candidate_done = report_file
									.getcandidate_done();

							appraisal_attachments = new JSONObject();
							appraisal_attachments.put("uid", db_app_uid);
							appraisal_attachments.put("file", db_app_file);
							appraisal_attachments.put("filename",
									db_app_filename);
							appraisal_attachments.put("appraisal_type",
									db_app_appraisal_type);
							appraisal_attachments.put("candidate_done",
									db_app_candidate_done);
							app_attachments.put(appraisal_attachments);
							appraisal_attachments_data.put("attachments",
									app_attachments);
							String query = query_temp.toString();
							String record = record_temp.toString();
							UserFunctions userFunction = new UserFunctions();
							// attachments
							String attachment = appraisal_attachments_data
									.toString();
							userFunction.SendCaseCenter_Attachments(attachment);

							// API
							//for tls
							JSONObject jsonResponse = new JSONObject();
							ArrayList<String> field = new ArrayList<>();
							ArrayList<String> value = new ArrayList<>();
							field.clear();
							field.add("auth_token");
							field.add("query");
							field.add("data");

							value.clear();
							value.add(gs.auth_token);
							value.add(query);
							value.add(record);

							if(Global.type.contentEquals("tls")){
								jsonResponse = gds.makeHttpsRequest(gs.update_url, "POST", field, value);
								xml = jsonResponse.toString();
							}else{
								xml=gds.http_posting(field,value,gs.update_url,Townhouse_condo.this);
							}

							/*HttpPost httpPost = new HttpPost(gs.update_url);
							List<NameValuePair> params = new ArrayList<NameValuePair>();
							params.add(new BasicNameValuePair("auth_token",
									gs.auth_token));
							params.add(new BasicNameValuePair("query", query));
							params.add(new BasicNameValuePair("data", record));
							httpPost.setEntity(new UrlEncodedFormEntity(params));
							HttpResponse response = httpClient
									.execute(httpPost);
							HttpEntity httpEntity = response.getEntity();
							xml = EntityUtils.toString(httpEntity);*/
							Log.e("respone", xml);
						}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Log.e("", e.toString());
				}

			}
		}

	}

	private class SendData extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(Townhouse_condo.this);
			pDialog.setMessage("Updating data..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.setCanceledOnTouchOutside(false);
			pDialog.show();
		}

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			NetworkUtil.getConnectivityStatusString(Townhouse_condo.this);
			if (!NetworkUtil.status.equals("Network not available")) {
				google_ping();
				if (google_response.equals("200")) {
					create_json();
					db_delete_all();
					upload_attachment();
					network_status = true;
				}
			} else {
				network_status = false;

			}

			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			pDialog.dismiss();
			if (network_status == false) {
				Toast.makeText(getApplicationContext(),
						"Network not available", Toast.LENGTH_SHORT).show();
			} else if (network_status == true) {
				if (google_response.equals("200")) {
					Toast.makeText(getApplicationContext(), "Data Updated",
							Toast.LENGTH_LONG).show();
					finish();
				}
			}

		}

	}

	public void create_pdf() {
		pdf_name = account_lname + "_" + account_fname + "_"
				+ requested_month.replaceAll("\\/", "")
				+ requested_day.replaceAll("\\/", "") + requested_year + "_"
				+ "Property Pictures" + "_" + counter;
		SharedPreferences appPrefs = getSharedPreferences("preference",
				MODE_PRIVATE);
		SharedPreferences.Editor prefsEditor = appPrefs.edit();
		prefsEditor.putString("pdf", pdf_name);
		prefsEditor.putString("fname", account_fname);
		prefsEditor.putString("lname", account_lname);
		prefsEditor.commit();
		startActivity(new Intent(Townhouse_condo.this,
				Pdf_Townhouse_condo.class));

	}

	public void open_customdialog() {
		// dialog
		myDialog = new Dialog(context);
		myDialog.setTitle("Select Attachment");
		myDialog.setContentView(R.layout.request_pdf_list);
		// myDialog.setTitle("My Dialog");

		dlg_priority_lvw = (ListView) myDialog
				.findViewById(R.id.dlg_priority_lvw);
		// ListView
		SimpleAdapter adapter = new SimpleAdapter(context, getPriorityList(),
				R.layout.request_pdf_list_layout, new String[] {
						"txt_pdf_list", "mono" }, new int[] {
						R.id.txt_pdf_list, R.id.mono });
		dlg_priority_lvw.setAdapter(adapter);

		// ListView
		dlg_priority_lvw
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {

						item = ((TextView) arg1.findViewById(R.id.txt_pdf_list))
								.getText().toString();
						attachment_selected = item;
						tv_attachment.setText(attachment_selected);
						// update
						uid = account_lname + "_" + account_fname + "_"
								+ requested_month.replaceAll("\\/", "")
								+ requested_day.replaceAll("\\/", "")
								+ requested_year;

						Report_filename rf = new Report_filename();
						rf.setuid(uid);
						rf.setfile("Property Pictures");
						rf.setfilename(attachment_selected);
						rf.setappraisal_type(appraisal_type + "_" + counter);
						rf.setcandidate_done("true");
						db.updateReport_filename(rf, record_id);
						db.close();
						myDialog.dismiss();
					}
				});
		dlg_priority_lvw.setLongClickable(true);
		dlg_priority_lvw
				.setOnItemLongClickListener(new OnItemLongClickListener() {
					@Override
					public boolean onItemLongClick(AdapterView<?> arg0,
							View arg1, final int arg2, long arg3) {
						item = ((TextView) arg1.findViewById(R.id.txt_pdf_list))
								.getText().toString();
						Toast.makeText(getApplicationContext(), item,
								Toast.LENGTH_SHORT).show();

						File file = new File(myDirectory, item);

						if (file.exists()) {
							Uri path = Uri.fromFile(file);
							Intent intent = new Intent(Intent.ACTION_VIEW);
							intent.setDataAndType(path, "application/pdf");
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

							try {
								startActivity(intent);
							} catch (ActivityNotFoundException e) {
								Toast.makeText(getApplicationContext(),
										"No Application Available to View PDF",
										Toast.LENGTH_SHORT).show();
							}
						}

						return true;

					}
				});
		myDialog.show();
	}

	private List<HashMap<String, Object>> getPriorityList() {
		DatabaseHandler db = new DatabaseHandler(
				context.getApplicationContext());
		List<HashMap<String, Object>> priorityList = new ArrayList<HashMap<String, Object>>();
		List<Images> images = db.getAllImages();
		if (!images.isEmpty()) {
			for (Images im : images) {
				filename = im.getfilename();
				String filenameArray[] = filename.split("\\.");
				String extension = filenameArray[filenameArray.length - 1];
				if (extension.equals("pdf")) {
					HashMap<String, Object> map1 = new HashMap<String, Object>();
					map1.put("txt_pdf_list", im.getfilename());
					map1.put("mono", R.drawable.monotoneright_white);
					priorityList.add(map1);
				}

			}

		}
		return priorityList;
	}

	public void upload_attachment() {
		// upload pdf
		uploadFile(Environment.getExternalStorageDirectory() + "/" + dir + "/"
				+ tv_attachment.getText().toString());
	}

	public int uploadFile(String sourceFileUri) {
		//String upLoadServerUri = "http://dv-dev.gdslinkasia.com:2999/appraisal_attachment_api/upload_media_test.php";
		String upLoadServerUri = "http://dev.gdslinkasia.com:8080/upload_pdf.php";
		String fileName = sourceFileUri;

		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		File sourceFile = new File(sourceFileUri);
		if (!sourceFile.isFile()) {
			Log.e("uploadFile", "Source File Does not exist");
			return 0;
		}
		try { // open a URL connection to the Servlet
			FileInputStream fileInputStream = new FileInputStream(sourceFile);
			URL url = new URL(upLoadServerUri);
			conn = (HttpURLConnection) url.openConnection(); // Open a HTTP
																// connection to
																// the URL
			conn.setDoInput(true); // Allow Inputs
			conn.setDoOutput(true); // Allow Outputs
			conn.setUseCaches(false); // Don't use a Cached Copy
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("ENCTYPE", "multipart/form-data");
			conn.setRequestProperty("Content-Type",
					"multipart/form-data;boundary=" + boundary);
			conn.setRequestProperty("uploaded_file", fileName);
			dos = new DataOutputStream(conn.getOutputStream());

			dos.writeBytes(twoHyphens + boundary + lineEnd);
			dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
					+ fileName + "\"" + lineEnd);
			dos.writeBytes(lineEnd);

			bytesAvailable = fileInputStream.available(); // create a buffer of
															// maximum size

			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];

			// read file and write it into form...
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);

			while (bytesRead > 0) {
				dos.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}

			// send multipart form data necesssary after file data...
			dos.writeBytes(lineEnd);
			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			// Responses from the server (code and message)
			serverResponseCode = conn.getResponseCode();
			String serverResponseMessage = conn.getResponseMessage();

			Log.i("uploadFile", "HTTP Response is : " + serverResponseMessage
					+ ": " + serverResponseCode);
			if (serverResponseCode == 200) {
				runOnUiThread(new Runnable() {
					public void run() {
					}
				});
			}

			// close the streams //
			fileInputStream.close();
			dos.flush();
			dos.close();

		} catch (MalformedURLException ex) {
			ex.printStackTrace();
			Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("Upload file to server Exception",
					"Exception : " + e.getMessage(), e);
		}
		return serverResponseCode;
	}

	public void open_pdf() {
		File file = new File(myDirectory, tv_attachment.getText().toString());

		if (file.exists()) {
			Uri path = Uri.fromFile(file);
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setDataAndType(path, "application/pdf");
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			try {
				startActivity(intent);
			} catch (ActivityNotFoundException e) {
				Toast.makeText(getApplicationContext(),
						"No Application Available to View PDF",
						Toast.LENGTH_SHORT).show();
			}
		}

	}

	public void uploaded_attachments() {
		// set value
		List<Required_Attachments> required_attachments = db
				.getAllRequired_Attachments_byid(String.valueOf(appraisal_type));
		if (!required_attachments.isEmpty()) {
			for (final Required_Attachments ra : required_attachments) {
				LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

				final View addView = layoutInflater.inflate(
						R.layout.report_uploaded_attachments_dynamic, null);
				final TextView tv_attachment = (TextView) addView
						.findViewById(R.id.tv_attachment);
				final ImageView btn_view = (ImageView) addView
						.findViewById(R.id.btn_view);
				tv_attachment.setText(ra.getattachment());
				btn_view.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						uploaded_attachment = pdf_name = account_lname + "_"
								+ account_fname + "_" + requested_month
								+ requested_day + requested_year + "_"
								+ ra.getattachment() + "_" + counter + ".pdf";
						// directory
						uploaded_file = new File(myDirectory,
								uploaded_attachment);
						uploaded_attachment = uploaded_attachment.replaceAll(
								" ", "%20");
						//url_webby = "http://dv-dev.gdslinkasia.com/attachments/"
						//		+ uploaded_attachment;
						url_webby = "http://dev.gdslinkasia.com:8080/pdf_attachments/"
								+ uploaded_attachment;

						view_pdf(uploaded_file);
					}
				});
				container_attachments.addView(addView);

			}

		}

	}

	private void view_download() {
		android.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Action");
		builder.setItems(commandArray, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int index) {
				if (index == 0) {
					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
							.parse(url_webby));
					startActivity(browserIntent);
					dialog.dismiss();
				} else if (index == 1) {
					new DownloadFileFromURL().execute(url_webby);
					dialog.dismiss();
				}
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public void view_pdf(File file) {
		if (file.exists()) {
			Uri path = Uri.fromFile(file);
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setDataAndType(path, "application/pdf");
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			try {
				startActivity(intent);
			} catch (ActivityNotFoundException e) {
				Toast.makeText(getApplicationContext(),
						"No Application Available to View PDF",
						Toast.LENGTH_SHORT).show();
			}
		} else {
			NetworkUtil.getConnectivityStatusString(this);
			if (!NetworkUtil.status.equals("Network not available")) {
				new Attachment_validation().execute();
			} else {
				Toast.makeText(getApplicationContext(),
						"Network not available", Toast.LENGTH_SHORT).show();
			}
		}
	}

	public void db_delete_all() {
		Report_Accepted_Jobs raj = new Report_Accepted_Jobs();
		raj.setrecord_id(record_id);
		db.deleteReport_Accepted_Jobs(raj);
		db.close();

		Report_Accepted_Jobs_Contacts rajc = new Report_Accepted_Jobs_Contacts();
		rajc.setrecord_id(record_id);
		db.deleteReport_Accepted_Jobs_Contacts(rajc);
		db.close();

		Report_filename rf = new Report_filename();
		rf.setrecord_id(record_id);
		db.deleteReport_filename(rf);
		db.close();

		Vacant_Lot_API va = new Vacant_Lot_API();
		va.setrecord_id(record_id);
		db.deleteVacant_Lot_API(va);
		db.close();
	}

	public void update_dialog() {
		// custom dialog
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.yes_no_dialog);

		// set the custom dialog components - text, image and button
		TextView text = (TextView) dialog.findViewById(R.id.tv_question);
		text.setText("Are you sure you want to submit ?");
		Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
		Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

		// if button is clicked, close the custom dialog
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new SendData().execute();
				dialog.dismiss();
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	private class Attachment_validation extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(Townhouse_condo.this);
			pDialog.setMessage("Checking attachment..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.setCanceledOnTouchOutside(false);
			pDialog.show();
		}

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			NetworkUtil.getConnectivityStatusString(Townhouse_condo.this);
			if (!NetworkUtil.status.equals("Network not available")) {
				google_ping();
				if (google_response.equals("200")) {
					try {
						URL obj = new URL(url_webby);
						HttpURLConnection con = (HttpURLConnection) obj
								.openConnection();

						// optional default is GET
						con.setRequestMethod("GET");

						// add request header
						con.setRequestProperty("User-Agent", "Mozilla/5.0");
						network_status = true;
						responseCode = con.getResponseCode();
						Log.e("", String.valueOf(responseCode));
					} catch (MalformedURLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ProtocolException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				network_status = false;

			}
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			Log.e("", url_webby);
			if (network_status == false) {
				Toast.makeText(getApplicationContext(), "No network Available",
						Toast.LENGTH_SHORT).show();
			} else if (network_status == true) {
				if (responseCode == 200) {
					view_download();
				} else if (responseCode == 404) {
					Toast.makeText(getApplicationContext(),
							"Attachment doesn't exist", Toast.LENGTH_SHORT)
							.show();
				}
			}
			pDialog.dismiss();

		}
	}

	public void google_ping() {
		try {
			URL obj = new URL(
					"https://www.google.com.ph/?gws_rd=cr&ei=td9kUrP0H8mjigenuoHAAg");
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");

			// add request header
			con.setRequestProperty("User-Agent", "Mozilla/5.0");

			google_response = String.valueOf(con.getResponseCode());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Showing Dialog
	 * */
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case progress_bar_type:
			pDialog = new ProgressDialog(this);
			pDialog.setMessage("Downloading file. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setMax(100);
			pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pDialog.setCancelable(true);
			pDialog.setCanceledOnTouchOutside(false);
			pDialog.show();
			return pDialog;
		default:
			return null;
		}
	}

	/**
	 * Background Async Task to download file
	 * */
	class DownloadFileFromURL extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Bar Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showDialog(progress_bar_type);
		}

		/**
		 * Downloading file in background thread
		 * */
		@Override
		protected String doInBackground(String... f_url) {
			int count;
			try {
				URL url = new URL(f_url[0]);
				URLConnection conection = url.openConnection();
				conection.connect();
				// getting file length
				int lenghtOfFile = conection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(),
						8192);

				// Output stream to write file
				OutputStream output = new FileOutputStream(
						uploaded_file.toString());

				byte data[] = new byte[1024];

				long total = 0;

				while ((count = input.read(data)) != -1) {
					total += count;
					// publishing the progress....
					// After this onProgressUpdate will be called
					publishProgress("" + (int) ((total * 100) / lenghtOfFile));

					// writing data to file
					output.write(data, 0, count);
				}

				// flushing output
				output.flush();

				// closing streams
				output.close();
				input.close();

			} catch (Exception e) {
				Log.e("Error: ", e.getMessage());
			}

			return null;
		}

		/**
		 * Updating progress bar
		 * */
		protected void onProgressUpdate(String... progress) {
			// setting progress percentage
			pDialog.setProgress(Integer.parseInt(progress[0]));
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		@Override
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after the file was downloaded
			removeDialog(progress_bar_type);
			Toast.makeText(getApplicationContext(), "Attachment Downloaded",
					Toast.LENGTH_LONG).show();
			view_pdf(uploaded_file);

		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_report_page1:
			custom_dialog_page1();
			break;
		case R.id.btn_report_page2:
			custom_dialog_page2();
			break;
		case R.id.btn_report_page3:
			custom_dialog_page3();
			break;
		case R.id.btn_report_update_cc:
			update_dialog();
			break;
		case R.id.btn_report_pdf:
			create_pdf();
			break;
		case R.id.btn_select_pdf:
			open_customdialog();
			break;
		case R.id.tv_attachment:
			open_pdf();
			break;
		default:
			break;
		}
	}

	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}


}
