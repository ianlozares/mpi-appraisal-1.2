package com.gds.appraisalmaybank.townhouse_condo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Images;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.Session_Timer;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Pdf_Townhouse_condo extends Activity implements OnClickListener {
	DatabaseHandler db = new DatabaseHandler(this);
	TextView tv_attachment;
	Button pdf_create;
	ImageView buttonAdd, buttoncapture, buttonAddFromGallery;
	LinearLayout container;
	RelativeLayout container2;
	ArrayList<EditText> myEditTextList = new ArrayList<EditText>();
	int counter = 0;
	Global gs;
	Session_Timer st = new Session_Timer(this);

	List<String> ImagesArray = new ArrayList<String>();
	List<String> ImagesArray_text = new ArrayList<String>();
	List<String> ImagesArraySize = new ArrayList<String>();
	// for prompts
	Context context = this;
	Button button;
	EditText result;
	String image_name, pdf_name, filename, item;
	String dir = "gds_appraisal";
	Dialog myDialog;
	ListView dlg_priority_lvw = null;
	String[] commandArray = new String[] { "Whole Page", "Half Page" };
	int height, width;
	String size;
	String image_desc;
	int counter_pdf = 0;
	
	//selecting image from gallery
	private static final int SELECT_PICTURE = 1;
    String selectedImagePath;
    String imageSource="";
    String dirFromGallery="";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.request_attachments_pdf_create);
		st.resetDisconnectTimer();
		tv_attachment = (TextView) findViewById(R.id.tv_attachment);
		buttoncapture = (ImageView) findViewById(R.id.capture);
		buttonAdd = (ImageView) findViewById(R.id.add);
		buttonAddFromGallery = (ImageView) findViewById(R.id.add_from_gallery);
		pdf_create = (Button) findViewById(R.id.pdf_create);
		container = (LinearLayout) findViewById(R.id.container);
		container2 = (RelativeLayout) findViewById(R.id.container2);
		gs = ((Global) getApplicationContext());
		SharedPreferences appPrefs = getSharedPreferences("preference",
				MODE_PRIVATE);
		pdf_name = appPrefs.getString("pdf", "") + ".pdf";
		tv_attachment.setText(pdf_name);
		buttonAdd.setOnClickListener(this);
		buttonAddFromGallery.setOnClickListener(this);
		pdf_create.setOnClickListener(this);
		buttoncapture.setOnClickListener(this);
		pdf_create.setEnabled(false);
	}

	public void add_image() {
		String myDirectory = "";
		//String myDirectory = Environment.getExternalStorageDirectory() + "/"
		//		+ dir + "/";
		if (imageSource.contentEquals("fromGdsFolder")){
			myDirectory = Environment.getExternalStorageDirectory() + "/"
					+ dir + "/";
		} else if (imageSource.contentEquals("fromOtherFolder")){
			myDirectory = dirFromGallery + "/";
		}
		
		LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View addView = layoutInflater.inflate(R.layout.report_pdf_layout,
				null);
		final ImageView textOut = (ImageView) addView
				.findViewById(R.id.textout);
		//Bitmap bmp = BitmapFactory.decodeFile(myDirectory + "/" + item);
		//textOut.setImageBitmap(bmp);
		
		//preventoing Garbage collector error
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 8;
		Bitmap bmp = BitmapFactory.decodeFile(myDirectory + "/" + item,options);
		textOut.setImageBitmap(bmp);
		
		final TextView et_pdf = (TextView) addView.findViewById(R.id.et_pdf);
		final TextView tv_pdf = (TextView) addView.findViewById(R.id.tv_pdf);
		et_pdf.setText(image_desc);
		tv_pdf.setText(String.valueOf(counter));
		counter++;
		ImagesArray_text.add(image_desc);
		ImagesArray.add(myDirectory + "/" + item);
		// button remove
		Button buttonRemove = (Button) addView.findViewById(R.id.remove);
		buttonRemove.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((LinearLayout) addView.getParent()).removeView(addView);
				ImagesArray.set(Integer.parseInt(tv_pdf.getText().toString()),
						"");

			}
		});

		container.addView(addView);
		
	}

	public void open_customdialog() {
		// dialog
		myDialog = new Dialog(context);
		myDialog.setTitle("Select Image");
		myDialog.setContentView(R.layout.pdf_customdialog);
		// myDialog.setTitle("My Dialog");

		dlg_priority_lvw = (ListView) myDialog
				.findViewById(R.id.dlg_priority_lvw);
		// ListView
		SimpleAdapter adapter = new SimpleAdapter(context, getPriorityList(),
				R.layout.pdf_customdialog_layout, new String[] {
						"list_priority_img", "list_priority_value" },
				new int[] { R.id.list_priority_img, R.id.list_priority_value });
		dlg_priority_lvw.setAdapter(adapter);

		// ListView
		dlg_priority_lvw
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {

						item = ((TextView) arg1
								.findViewById(R.id.list_priority_value))
								.getText().toString();
						myDialog.dismiss();
						Toast.makeText(getApplicationContext(), item,Toast.LENGTH_SHORT).show();
						// dialog
						image_desc_prompts();
					}
				});
		myDialog.show();
	}

	public void image_desc_prompts() {
		// custom dialog
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.input_dialog);

		// set the custom dialog components - text, image and button
		TextView text = (TextView) dialog.findViewById(R.id.tv_question);
		final EditText txt_remarks = (EditText) dialog
				.findViewById(R.id.txt_remarks);
		text.setText("Image Name");
		Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
		Button btn_no = (Button) dialog.findViewById(R.id.btn_no);
		btn_yes.setText("OK");
		// if button is clicked, close the custom dialog
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				image_desc = txt_remarks.getText().toString();
				dialog.dismiss();
				Resize_dialog();
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	private void Resize_dialog() {
		android.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Size of Image:");
		builder.setItems(commandArray, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int index) {
				Toast.makeText(Pdf_Townhouse_condo.this,
						commandArray[index] + " Selected", Toast.LENGTH_SHORT)
						.show();
				if (index == 0) {
					ImagesArraySize.add("500" + "-" + "620");
					dialog.dismiss();
					pdf_create.setEnabled(true);

				} else if (index == 1) {
					ImagesArraySize.add("500" + "-" + "365");
					dialog.dismiss();
					pdf_create.setEnabled(true);
				}
				add_image();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public void create_pdf() {
		// step 1: creation of a document-object
		Document document = new Document();

		try {
			// step 2:
			// we create a writer that listens to the document
			// and directs a PDF-stream to a file
			//this will create pdf file in gds_appraisal
			PdfWriter writer= PdfWriter.getInstance(document, new FileOutputStream(
					android.os.Environment.getExternalStorageDirectory()
							+ java.io.File.separator + "gds_appraisal"
							+ java.io.File.separator + pdf_name));

			//put password on the pdf
			//writer.setEncryption("concretepage".getBytes(), "cp123".getBytes(), PdfWriter.ALLOW_COPY, PdfWriter.STANDARD_ENCRYPTION_40);
			//writer.createXmpMetadata();

			// step 3: we open the document
			document.open();

			for (int x = 0; x < ImagesArray.size(); x++) {
				if (!ImagesArray.get(x).equals("")) {
					document.setPageSize(PageSize.LETTER);
					document.newPage();
					// image
					ByteArrayOutputStream stream = new ByteArrayOutputStream();
					Bitmap bitmap = BitmapFactory
							.decodeFile(ImagesArray.get(x));
					size = ImagesArraySize.get(x);
					String filenameArray[] = size.split("\\-");
					width = Integer.parseInt(filenameArray[0]);
					height = Integer.parseInt(filenameArray[1]);
					Bitmap out = Bitmap.createScaledBitmap(bitmap, width,
							height, false);
					out.compress(Bitmap.CompressFormat.JPEG /* FileType */,
							100 /* Ratio */, stream);
					Image jpg = Image.getInstance(stream.toByteArray());
					jpg.setAlignment(Image.MIDDLE);
					document.add(jpg);

					// text
					if (counter_pdf == 0) {
						Paragraph[] p = new Paragraph[1];
						p[0] = new Paragraph("\n\n\n" + ImagesArray_text.get(x));
						p[0].setAlignment(Element.ALIGN_CENTER);
						document.add(p[0]);
					} else {
						Paragraph[] p = new Paragraph[1];
						p[0] = new Paragraph(ImagesArray_text.get(x));
						p[0].setAlignment(Element.ALIGN_CENTER);
						document.add(p[0]);
					}
					counter_pdf++;

				}
			}
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}

		// step 5: we close the document
		document.close();
	}

	public void db_pdf() {
		File myDirectory = new File(Environment.getExternalStorageDirectory()
				+ "/" + dir + "/");
		List<Images> report_accepted_jobs = db.getImages_single(pdf_name);
		if (report_accepted_jobs.isEmpty()) {
			db.addImages(new Images(myDirectory.toString(), pdf_name));
		}

		Toast.makeText(getApplicationContext(), pdf_name + " created",
				Toast.LENGTH_SHORT).show();
		finish();
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.add:
			imageSource="fromGdsFolder";
			open_customdialog();
			break;
		case R.id.add_from_gallery:
			//refresh_gallery
			//sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
			
			imageSource="fromOtherFolder";
			open_gallery();
            break;
		case R.id.pdf_create:
			create_pdf();
			db_pdf();
			break;
		case R.id.capture:
			startActivity(new Intent(Pdf_Townhouse_condo.this,
					Pdf_Capture.class));
			break;

		default:
			break;
		}
	}

	private List<HashMap<String, Object>> getPriorityList() {
		DatabaseHandler db = new DatabaseHandler(
				context.getApplicationContext());
		List<HashMap<String, Object>> priorityList = new ArrayList<HashMap<String, Object>>();
		List<Images> images = db.getAllImages();
		if (!images.isEmpty()) {
			for (Images im : images) {
				filename = im.getfilename();
				String filenameArray[] = filename.split("\\.");
				String extension = filenameArray[filenameArray.length - 1];
				if (extension.equals("jpg")) {
					HashMap<String, Object> map1 = new HashMap<String, Object>();
					map1.put("list_priority_img",
							im.getpath() + "/" + im.getfilename());
					map1.put("list_priority_value", im.getfilename());
					priorityList.add(map1);
				}

			}

		}
		return priorityList;
	}
	
	/*
	 * for selecting image from gallery
	 */
	
	public void open_gallery(){
		Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"), SELECT_PICTURE);
	}
	
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null && data.getData() != null) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                //selectedImagePath = getPath(selectedImageUri);
                selectedImagePath = getRealPathFromURI(selectedImageUri);
                //item.setImageURI(selectedImageUri);
                
                File f = new File(selectedImagePath);
                item = f.getName();//getImageName
                dirFromGallery = f.getParent();//getImageParentFolder
                
                Toast.makeText(getApplicationContext(), item,Toast.LENGTH_SHORT).show();
				// dialog
				image_desc_prompts();
            }
        }
    }
 
    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
    
	public String getRealPathFromURI(Uri contentUri) {
		Cursor cursor = null;
		try {
			String[] proj = { MediaStore.Images.Media.DATA };
			cursor = context.getContentResolver().query(contentUri, proj, null,
					null, null);
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}

}
