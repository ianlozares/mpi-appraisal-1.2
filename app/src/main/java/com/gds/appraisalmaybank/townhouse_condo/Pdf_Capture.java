package com.gds.appraisalmaybank.townhouse_condo;

import java.io.File;
import java.io.FileOutputStream;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.Toast;
import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Images;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.methods.Session_Timer;

public class Pdf_Capture extends Activity {
	String image_name;
	String dir = "gds_appraisal";
	Uri fileUri;
	int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	File file;
	DatabaseHandler db = new DatabaseHandler(this);
	Global gs;
	int id = 0;
	String fname, lname;

	Session_Timer st = new Session_Timer(this);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		st.resetDisconnectTimer();
		gs = ((Global) getApplicationContext());
		set_image_name();
		Toast.makeText(getApplicationContext(), image_name, Toast.LENGTH_LONG)
				.show();
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		File myDirectory = new File(Environment.getExternalStorageDirectory()
				+ "/" + dir + "/");

		file = new File(myDirectory, image_name);
		fileUri = Uri.fromFile(file);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

		// start the image capture Intent
		startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				// Image captured and saved to fileUri specified in the Intent
				File myDirectory = new File(
						Environment.getExternalStorageDirectory() + "/" + dir
								+ "/");
				// insert to db
				db.addImages(new Images(myDirectory.toString(), image_name));
				// resize image
				Bitmap b = BitmapFactory.decodeFile(Environment
						.getExternalStorageDirectory()
						+ "/"
						+ dir
						+ "/"
						+ image_name);
				Bitmap out = Bitmap.createScaledBitmap(b, 400, 320, false);

				File file = new File(Environment.getExternalStorageDirectory()
						+ "/" + dir + "/" + image_name);
				FileOutputStream fOut;
				try {
					fOut = new FileOutputStream(file);
					out.compress(Bitmap.CompressFormat.PNG, 100, fOut);
					fOut.flush();
					fOut.close();
					b.recycle();
					out.recycle();

				} catch (Exception e) { // TODO

				}
				// new activity
				finish();
			} else if (resultCode == RESULT_CANCELED) {
				// User cancelled the image capture
				Toast.makeText(getApplicationContext(), "cancelled",
						Toast.LENGTH_SHORT).show();
				finish();
			} else {
				// Image capture failed, advise user
				Toast.makeText(getApplicationContext(), "failed",
						Toast.LENGTH_SHORT).show();
				finish();
			}
		}

	}

	public void set_image_name() {
		Images images = db.getImages_Last();
		if (images != null) {
			id = images.getID();
		} else {
			id = 0;
		}
		SharedPreferences appPrefs = getSharedPreferences("preference",
				MODE_PRIVATE);
		fname = appPrefs.getString("fname", "");
		lname = appPrefs.getString("lname", "");
		image_name = lname + fname + "_" + id + ".jpg";

	}

	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}


}
