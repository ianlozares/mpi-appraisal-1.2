package com.gds.appraisalmaybank.report;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.check_network.NetworkUtil;
import com.gds.appraisalmaybank.main.Connectivity;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

/**
 * Created by ianlo on 25/07/2016.
 */
public class View_Report extends Activity{

    Session_Timer st = new Session_Timer(this);
    WebView mWebView;
    String record_id,appraisal_type,pdf;
    Dialog myDialog;
    private ProgressDialog progressBar;
    GDS_methods gds = new GDS_methods();
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_report);
        mWebView = (WebView) findViewById(R.id.activity_main_webview);


        st.resetDisconnectTimer();
        Intent i = getIntent();
        record_id = i.getStringExtra("record_id");
        appraisal_type = i.getStringExtra("appraisal_type");

        progressBar =new ProgressDialog(View_Report.this);
        progressBar.setIndeterminate(true);
        progressBar.setMessage("Generating report...");

        NetworkUtil.getConnectivityStatusString(View_Report.this);
        if (!NetworkUtil.status.equals("Network not available")) {
                if(Connectivity.isConnectedFast(getBaseContext())){
                    progressBar.show();
                    final AlertDialog alertDialog = new AlertDialog.Builder(this,AlertDialog.THEME_DEVICE_DEFAULT_LIGHT).create();

                   // mWebView.setWebViewClient(new WebViewClient());
                    WebSettings webSettings = mWebView.getSettings();
                    webSettings.setJavaScriptEnabled(true);
                    webSettings.setBuiltInZoomControls(true);

                    mWebView.setWebViewClient(new WebViewClient() {
                        public boolean shouldOverrideUrlLoading(WebView view, String url) {
                            Log.i("TAG", "Processing webview url click...");
                            view.loadUrl(url);
                            return true;
                        }

                        public void onPageFinished(WebView view, String url) {
                            Log.i("TAG", "Finished loading URL: " + url);
                            if (progressBar.isShowing()) {
                                progressBar.dismiss();
                            }
                        }

                        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                            Log.e("TAG", "Error: " + description);
                            alertDialog.setTitle("Connection Error");
                            alertDialog.setMessage("Please check your internet connection or contact your administrator");
                            alertDialog.setCancelable(false);
                            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                    return;
                                }
                            });
                            alertDialog.show();
                            mWebView.loadUrl("file:///android_asset/myerrorpage.html");
                        }
                    });
                    if(Global.cert_name.contentEquals("mpi_uat.crt")) {
                        if (appraisal_type.contentEquals("land_improvements")) {
                            pdf = "https://mpiuat-report.gdslinkasia.com:8082/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/LotWithImprovements.pdf?record_id=" + record_id;
                        } else if (appraisal_type.contentEquals("townhouse_condo")) {
                            pdf = "https://mpiuat-report.gdslinkasia.com:8082/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/Condo.pdf?record_id=" + record_id;
                        } else if (appraisal_type.contentEquals("construction_ebm")) {
                            pdf = "https://mpiuat-report.gdslinkasia.com:8082/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/EBM.pdf?record_id=" + record_id;
                        } else if (appraisal_type.contentEquals("motor_vehicle")) {
                            pdf = "https://mpiuat-report.gdslinkasia.com:8082/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/MotorVehicle.pdf?record_id=" + record_id;
                        } else if (appraisal_type.contentEquals("ppcr")) {
                            pdf = "https://mpiuat-report.gdslinkasia.com:8082/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/PPCR.pdf?record_id=" + record_id;
                        } else if (appraisal_type.contentEquals("townhouse")) {
                            pdf = "https://mpiuat-report.gdslinkasia.com:8082/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/Townhouse.pdf?record_id=" + record_id;
                        } else if (appraisal_type.contentEquals("vacant_lot")) {
                            pdf = "https://mpiuat-report.gdslinkasia.com:8082/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/LotOnly.pdf?record_id=" + record_id;
                        } else {
                            pdf = "https://mpiuat-report.gdslinkasia.com:8082/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/LotWithImprovements.pdf?record_id=" + record_id;
                        }
                    }else if(Global.cert_name.contentEquals("mpi_prod.crt")){
                        if (appraisal_type.contentEquals("land_improvements")) {
                            pdf = "https://mpiprd-report.gdslinkasia.com:8082/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/LotWithImprovements.pdf?record_id=" + record_id;
                        } else if (appraisal_type.contentEquals("townhouse_condo")) {
                            pdf = "https://mpiprd-report.gdslinkasia.com:8082/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/Condo.pdf?record_id=" + record_id;
                        } else if (appraisal_type.contentEquals("construction_ebm")) {
                            pdf = "https://mpiprd-report.gdslinkasia.com:8082/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/EBM.pdf?record_id=" + record_id;
                        } else if (appraisal_type.contentEquals("motor_vehicle")) {
                            pdf = "https://mpiprd-report.gdslinkasia.com:8082/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/MotorVehicle.pdf?record_id=" + record_id;
                        } else if (appraisal_type.contentEquals("ppcr")) {
                            pdf = "https://mpiprd-report.gdslinkasia.com:8082/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/PPCR.pdf?record_id=" + record_id;
                        } else if (appraisal_type.contentEquals("townhouse")) {
                            pdf = "https://mpiprd-report.gdslinkasia.com:8082/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/Townhouse.pdf?record_id=" + record_id;
                        } else if (appraisal_type.contentEquals("vacant_lot")) {
                            pdf = "https://mpiprd-report.gdslinkasia.com:8082/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/LotOnly.pdf?record_id=" + record_id;
                        } else {
                            pdf = "https://mpiprd-report.gdslinkasia.com:8082/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/LotWithImprovements.pdf?record_id=" + record_id;
                        }
                    } else{
                        if (appraisal_type.contentEquals("land_improvements")) {
                            pdf = "http://dv-dev.gdslinkasia.com/MaybankAppraisalWebApp/ValuationReport.aspx?url1=http://reports.gdslinkasia.com:8080/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/LotWithImprovements.pdf?record_id=" + record_id;
                        } else if (appraisal_type.contentEquals("townhouse_condo")) {
                            pdf = "http://dv-dev.gdslinkasia.com/MaybankAppraisalWebApp/ValuationReport.aspx?url1=http://reports.gdslinkasia.com:8080/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/Condo.pdf?record_id=" + record_id;
                        } else if (appraisal_type.contentEquals("construction_ebm")) {
                            pdf = "http://dv-dev.gdslinkasia.com/MaybankAppraisalWebApp/ValuationReport.aspx?url1=http://reports.gdslinkasia.com:8080/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/EBM.pdf?record_id=" + record_id;
                        } else if (appraisal_type.contentEquals("motor_vehicle")) {
                            pdf = "http://dv-dev.gdslinkasia.com/MaybankAppraisalWebApp/ValuationReport.aspx?url1=http://reports.gdslinkasia.com:8080/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/MotorVehicle.pdf?record_id=" + record_id;
                        } else if (appraisal_type.contentEquals("ppcr")) {
                            pdf = "http://dv-dev.gdslinkasia.com/MaybankAppraisalWebApp/ValuationReport.aspx?url1=http://reports.gdslinkasia.com:8080/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/PPCR.pdf?record_id=" + record_id;
                        } else if (appraisal_type.contentEquals("townhouse")) {
                            pdf = "http://dv-dev.gdslinkasia.com/MaybankAppraisalWebApp/ValuationReport.aspx?url1=http://reports.gdslinkasia.com:8080/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/Townhouse.pdf?record_id=" + record_id;
                        } else if (appraisal_type.contentEquals("vacant_lot")) {
                            pdf = "http://dv-dev.gdslinkasia.com/MaybankAppraisalWebApp/ValuationReport.aspx?url1=http://reports.gdslinkasia.com:8080/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/LotOnly.pdf?record_id=" + record_id;
                        } else {
                            pdf = "http://dv-dev.gdslinkasia.com/MaybankAppraisalWebApp/ValuationReport.aspx?url1=http://reports.gdslinkasia.com:8080/jasperserver/rest_v2/reports/reports/Maybank_Appraisal/LotWithImprovements.pdf?record_id=" + record_id;
                        }
                    }
                    mWebView.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + pdf);
                    Log.e("PDF URL",""+pdf);
                }else{
                    runOnUiThread(new Runnable() {
                        public void run() {

                            Toast toast = Toast.makeText(getBaseContext(), "Slow Internet connection.", Toast.LENGTH_LONG);
                            View view = toast.getView();
                            view.setBackgroundResource(R.color.toast_red);
                            toast.show();
                            serverError();
                        }
                    });

                }



        }else{
            runOnUiThread(new Runnable() {
                public void run() {

                    Toast toast = Toast.makeText(getBaseContext(), "Network not available.", Toast.LENGTH_LONG);
                    View view = toast.getView();
                    view.setBackgroundResource(R.color.toast_red);
                    toast.show();
                    serverError();
                }
            });

        }

    }


    public void serverError(){

        myDialog = new Dialog(View_Report.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.warning_dialog);
        myDialog.setCancelable(true);
        final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
        final Button btn_ok = (Button) myDialog.findViewById(R.id.btn_ok);

        tv_question.setText(R.string.server_error);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.show();
    }

    @Override
    public void onUserInteraction(){
        st.resetDisconnectTimer();
    }
    @Override
    public void onStop() {
        super.onStop();
        st.stopDisconnectTimer();
    }
    @Override
    public void onResume() {
        super.onResume();
        st.resetDisconnectTimer();
    }



}
