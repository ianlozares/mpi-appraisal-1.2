package com.gds.appraisalmaybank.json;

import android.util.Log;

import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.methods.GDS_methods;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class UserFunctions {

	private JSONParser jsonParser;

	ArrayList<String> field = new ArrayList<>();
	ArrayList<String> value = new ArrayList<>();
	GDS_methods gds = new GDS_methods();

	private static String registerURL = Global.server_url+"/maybank_philippines_incorporated/appraisal/records/create.json";
	private static String loginURL = Global.server_url+"/maybank_philippines_incorporated/appraisal/records.json";
	private static String readURL = Global.server_url+"/maybank_philippines_incorporated/appraisal/records/show.json";
	private static String updateURL = Global.server_url+"/maybank_philippines_incorporated/appraisal/records/update.json";
	public String auth_token = Global.auth_token;



	// constructor
	public UserFunctions() {
		jsonParser = new JSONParser();
	}

	public JSONObject SendCaseCenter(String record) {
		// Building Parameters
		/*

		params.add(new BasicNameValuePair("auth_token", auth_token));
		params.add(new BasicNameValuePair("data", record));
		String a = params.toString();
		Log.e("", a);*/
		//for tls
		field.clear();
		field.add("auth_token");
		field.add("data");

		value.clear();
		value.add(auth_token);
		value.add(record);
		// getting JSON Object
		JSONObject json;
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		if(Global.type.contentEquals("tls")){
			json = gds.makeHttpsRequest(registerURL, "POST", field, value);
		}else{
			params.add(new BasicNameValuePair("auth_token", auth_token));
			params.add(new BasicNameValuePair("data", record));
			json = gds.makeHttpRequest(registerURL, "POST", params);
		}

		return json;
	}

	public JSONObject SendCaseCenter_Attachments(String data) {
		// Building Parameters
		/*List<NameValuePair> params = new ArrayList<NameValuePair>();

		params.add(new BasicNameValuePair("tag", "QFEgR3KsBJRKcYSy2nCp"));
		params.add(new BasicNameValuePair("attachments_data", data));
		String a = params.toString();
		Log.e("", a);*/
		//for tls
		field.clear();
		field.add("tag");
		field.add("attachments_data");

		value.clear();
		value.add("QFEgR3KsBJRKcYSy2nCp");
		value.add(data);
		// getting JSON Object
		JSONObject json;
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		if(Global.type.contentEquals("tls")){
			json = gds.makeHttpsRequest(Global.registerURL_attachments, "POST", field, value);
		}else{
			params.add(new BasicNameValuePair("tag", "QFEgR3KsBJRKcYSy2nCp"));
			params.add(new BasicNameValuePair("attachments_data", data));
			json = gds.makeHttpRequest(Global.registerURL_attachments, "POST", params);
		}
		Log.e("json from attachment",""+json);
		return json;
	}
	
	public JSONObject CaseCenterResponse(String query, String only) {
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("auth_token", auth_token));
		params.add(new BasicNameValuePair("query", query));
		params.add(new BasicNameValuePair("limit", "0"));
		params.add(new BasicNameValuePair("only", only));
		JSONObject json = jsonParser.getJSONFromUrl(loginURL, params);
		// return json
		String a = params.toString();
		Log.e("", a);
		return json;
	}

	public JSONObject CaseCenterRead(String query) {
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("auth_token", auth_token));
		params.add(new BasicNameValuePair("query", query));
		JSONObject json = jsonParser.getJSONFromUrl(readURL, params);
		// return json
		String a = params.toString();
		Log.e("", a);
		return json;
	}

	public JSONObject CaseCenterUpdate(String query, String data) {
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("auth_token", auth_token));
		params.add(new BasicNameValuePair("query", query));
		params.add(new BasicNameValuePair("data", data));
		JSONObject json = jsonParser.getJSONFromUrl(updateURL, params);
		// return json
		String a = params.toString();
		Log.e("", a);
		return json;
	}

	public void DownloadFile(String fileURL, File directory) {
		try {

			FileOutputStream f = new FileOutputStream(directory);
			URL u = new URL(fileURL);
			HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setDoOutput(true);
			c.connect();

			InputStream in = c.getInputStream();

			byte[] buffer = new byte[1024];
			int len1 = 0;
			while ((len1 = in.read(buffer)) > 0) {
				f.write(buffer, 0, len1);
			}
			f.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	public void downloadFileFromServer(File filename, String urlString)
			throws MalformedURLException, IOException {
		BufferedInputStream in = null;
		FileOutputStream fout = null;
		try {
			URL url = new URL(urlString);

			in = new BufferedInputStream(url.openStream());
			fout = new FileOutputStream(filename);

			byte data[] = new byte[1024];
			int count;
			while ((count = in.read(data, 0, 1024)) != -1) {
				fout.write(data, 0, count);
				System.out.println(count);
			}
		} finally {
			if (in != null)
				in.close();
			if (fout != null)
				fout.close();
		}
		System.out.println("Done");
	}
}
