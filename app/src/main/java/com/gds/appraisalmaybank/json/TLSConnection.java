package com.gds.appraisalmaybank.json;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.Splash_Screen;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

/**
 * Created by GDS: Ian Lozares on 3/2/2016.
 */
public class TLSConnection {




    //IAN
    /**
     * Test
     */
    /**
     * Set up a connection to littlesvr.ca using HTTPS. An entire function
     * is needed to do this because littlesvr.ca has a self-signed certificate.
     *
     * The caller of the function would do something like:
     * HttpsURLConnection urlConnection = setUpHttpsConnection("https://littlesvr.ca");
     * InputStream in = urlConnection.getInputStream();
     * And read from that "in" as usual in Java
     *
     * Based on code from:
     * https://developer.android.com/training/articles/security-ssl.html#SelfSigned
     */
    public static Context context;
    @SuppressLint("SdCardPath")

    public static HttpsURLConnection setUpHttpsConnection(String urlString)
    {
        try
        {
            //added Feb 29 2016
            HostnameVerifier hostnameVerifier = new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    HostnameVerifier hv =
                            HttpsURLConnection.getDefaultHostnameVerifier();
                    return true;

                }
            };

            // Load CAs from an InputStream
            // (could be from a resource or ByteArrayInputStream or ...)
            CertificateFactory cf = CertificateFactory.getInstance("X.509");

            // My CRT file that I put in the assets folder
            // I got this file by following these steps:
            // * Go to https://littlesvr.ca using Firefox
            // * Click the padlock/More/Security/View Certificate/Details/Export
            // * Saved the file as littlesvr.crt (type X.509 Certificate (PEM))
            // The MainActivity.context is declared as:
            // public static Context context;
            // And initialized in MainActivity.onCreate() as:
            // MainActivity.context = getApplicationContext();

            InputStream caInput = new BufferedInputStream(Splash_Screen.context.getAssets().open(Global.cert_name));
            //InputStream caInput = new BufferedInputStream(Splash_Screen.context.getAssets().open("gdsdev2.crt"));
            Certificate ca = cf.generateCertificate(caInput);
            System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());

            // Create a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            //KeyStore keyStore = KeyStore.getInstance("BKS");
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

            // Create a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);


            // Create an SSLContext that uses our TrustManager
            SSLContext context = SSLContext.getInstance("TLSv1.2");
            //SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, tmf.getTrustManagers(), null);

            // Tell the URLConnection to use a SocketFactory from our SSLContext
            URL url = new URL(urlString);
            HttpsURLConnection urlConnection = (HttpsURLConnection)url.openConnection();
           /* if(Global.cert_name.contentEquals("mpi_uat.crt")||Global.cert_name.contentEquals("mpi_prod.crt")){
                urlConnection.setSSLSocketFactory(context.getSocketFactory());
            }else{
                urlConnection.setSSLSocketFactory(new TLSSocketFactory());
            }*/
            urlConnection.setSSLSocketFactory(new TLS12SocketFactory(context.getSocketFactory()));

            //added Feb 29 2016
            urlConnection.setHostnameVerifier(hostnameVerifier);

            return urlConnection;
        }
        catch (Exception ex)
        {
            Log.e("Opps", "Failed to establish TLS connection to server: " + ex.toString());
            return null;
        }

    }


}