package com.gds.appraisalmaybank.townhouse;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Townhouse_API;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database2.Townhouse_API_Imp_Details;
import com.gds.appraisalmaybank.database2.Townhouse_API_Imp_Details_Features;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Townhouse_Desc_Of_Imp_Features extends ListActivity implements OnClickListener {
	DatabaseHandler2 db = new DatabaseHandler2(this);
	GDS_methods gds = new GDS_methods();
	Button btn_add_desc_of_imp_features, btn_save_desc_of_imp_features;
	Button btn_create, btn_cancel, btn_save, btn_delete;

	Session_Timer st = new Session_Timer(this);
	String record_id="";
	private static final String tag_imp_details_features_id = "imp_details_features_id";
	private static final String TAG_RECORD_ID = "record_id";
	private static final String tag_imp_details_id = "imp_details_id";
	private static final String tag_building_desc = "report_building_desc";
	private static final String tag_area = "area";
	private static final String tag_description = "description";
	String imp_details_features_id="", imp_details_id="", building_desc="";
	String area="", total_area="";
	DecimalFormat df = new DecimalFormat("#.00");
	Double area_d=0.00, total_area_d=0.00;
	ArrayList<String> field = new ArrayList<>();
	ArrayList<String> value = new ArrayList<>();
	Dialog myDialog;
	//create dialog
	EditText report_features, report_area, report_description;
	
	//forListViewDeclarations
	private ProgressDialog pDialog;
	ArrayList<HashMap<String, String>> itemList;
	String description="";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reports_land_desc_of_imp_features);

		st.resetDisconnectTimer();
		btn_add_desc_of_imp_features = (Button) findViewById(R.id.btn_add_desc_of_imp_features);
		btn_add_desc_of_imp_features.setOnClickListener(this);
		
		btn_save_desc_of_imp_features = (Button) findViewById(R.id.btn_save_desc_of_imp_features);
		btn_save_desc_of_imp_features.setOnClickListener(this);
		
		// getting record_id from intent / previous class
		Intent i = getIntent();
		record_id = i.getStringExtra(TAG_RECORD_ID);
		imp_details_id = i.getStringExtra(tag_imp_details_id);
		building_desc = i.getStringExtra(tag_building_desc);
		//list
		itemList = new ArrayList<HashMap<String, String>>();
		new LoadAll().execute();
		// Get listview
		ListView lv = getListView();

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// getting values from selected ListItem
				imp_details_features_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
				view_details();
			}
		});
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
                // TODO Auto-generated method stub

                Log.v("long clicked","pos: " + position);
                imp_details_features_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
                delete_item();
                return true;
            }
        }); 
		
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		save_total_area();
		Intent in = new Intent(getApplicationContext(),
				Townhouse_Desc_Of_Imp_View.class);
		in.putExtra(TAG_RECORD_ID, record_id);
		in.putExtra(tag_imp_details_id, imp_details_id);
		//starting new activity and expecting some response back
		startActivityForResult(in, 100);
		finish();
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.btn_add_desc_of_imp_features:
			add_new();
			break;
		case R.id.btn_save_desc_of_imp_features:
			save_total_area();
			Intent in = new Intent(getApplicationContext(),
					Townhouse_Desc_Of_Imp_View.class);
			in.putExtra(TAG_RECORD_ID, record_id);
			in.putExtra(tag_imp_details_id, imp_details_id);
			//starting new activity and expecting some response back
			startActivityForResult(in, 100);
			finish();
			break;
		default:
			break;
		}
	}
	
	public void add_new(){
		Toast.makeText(getApplicationContext(), "Add New",Toast.LENGTH_SHORT).show();
		myDialog = new Dialog(Townhouse_Desc_Of_Imp_Features.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.reports_townhouse_desc_of_imp_features_form);
		myDialog.setCancelable(false);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		
		btn_create = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_create.setText("Create");
		btn_cancel.setText("Cancel");
		
		report_features = (EditText) myDialog.findViewById(R.id.report_features);
		report_area = (EditText) myDialog.findViewById(R.id.report_area);
		report_description = (EditText) myDialog.findViewById(R.id.report_description);

		report_area.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_area, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});

		btn_create.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//list of EditTexts put in array


					//add data in SQLite
					db.addTownhouse_Imp_Details_Features(new Townhouse_API_Imp_Details_Features(
							record_id,
							imp_details_id,
							report_features.getText().toString(),
							gds.replaceFormat(report_area.getText().toString()),
							report_description.getText().toString()));
						Toast.makeText(getApplicationContext(),
								"data created", Toast.LENGTH_SHORT).show();
						myDialog.dismiss();
						reloadClass();

				
			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		gds.fill_in_error(new EditText[]{report_area});
		myDialog.show();
	}
	
	public void view_details(){
		myDialog = new Dialog(Townhouse_Desc_Of_Imp_Features.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.reports_townhouse_desc_of_imp_features_form);
		myDialog.setCancelable(false);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		
		btn_save = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_save.setText("Save");
		btn_cancel.setText("Cancel");
		
		report_features = (EditText) myDialog.findViewById(R.id.report_features);
		report_area = (EditText) myDialog.findViewById(R.id.report_area);
		report_description = (EditText) myDialog.findViewById(R.id.report_description);
		report_area.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_area, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		List<Townhouse_API_Imp_Details_Features> lotDetailsFeaturesList = db
				.getTownhouse_Imp_Details_Features_Single(String.valueOf(record_id), String.valueOf(imp_details_features_id));
		if (!lotDetailsFeaturesList.isEmpty()) {
			for (Townhouse_API_Imp_Details_Features li : lotDetailsFeaturesList) {
				report_features.setText(li.getreport_desc_features());
				report_area.setText(gds.numberFormat(li.getreport_desc_features_area()));
				report_description.setText(li.getreport_desc_features_area_desc());
			}
		}
		
		btn_save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				//list of EditTexts put in array

					//update data in SQLite
					Townhouse_API_Imp_Details_Features li = new Townhouse_API_Imp_Details_Features();
					li.setreport_desc_features(report_features.getText().toString());
					li.setreport_desc_features_area(gds.replaceFormat(report_area.getText().toString()));
					li.setreport_desc_features_area_desc(report_description.getText().toString());
					
					db.updateTownhouse_Imp_Details_Features(li, record_id, imp_details_features_id);
					db.close();
					myDialog.dismiss();
					Toast.makeText(getApplicationContext(), "Saved",Toast.LENGTH_SHORT).show();
					reloadClass();

			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		gds.fill_in_error(new EditText[]{report_area});
		myDialog.show();
	}
	
	public void save_total_area(){
		Log.e("Start","save_total_area");
		area_d = 0.00;
		total_area_d = 0.00;
		
		List<Townhouse_API_Imp_Details_Features> liidf = db.getTownhouse_Imp_Details_Features(record_id, imp_details_id);
		if (!liidf.isEmpty()) {
			for (Townhouse_API_Imp_Details_Features imidf : liidf) {
				area_d = Double.parseDouble(gds.nullCheck2(imidf.getreport_desc_features_area()));
				total_area_d = area_d + total_area_d;
			}
		}
		
		//save to imp details
		Townhouse_API_Imp_Details li = new Townhouse_API_Imp_Details();
		li.setreport_desc_imp_floor_area(gds.addition(String.valueOf(total_area_d),"0"));
		db.updateTownhouse_Imp_Details_Total(li, record_id, String.valueOf(imp_details_id));

		String where ="WHERE record_id = args and id = args";
		String[] args = new String[2];
		args[0] = record_id;
		args[1] = imp_details_id;
		String unit_val = db.getRecord("report_value_unit_value",where,args, db.tbl_townhouse_lot_valuation_details).get(0);
		String total_land_val = gds.round(gds.multiplication(String.valueOf(total_area_d),unit_val));

		Log.e("unit_val",unit_val);
		Log.e("total_land_val",total_land_val);

		field.clear();
		field.add("report_value_floor_area");
		field.add("report_value_land_value");
		value.clear();
		value.add(String.valueOf(total_area_d));
		value.add(String.valueOf(total_land_val));
		db.updateRecord(value,field,"record_id = ? and id = ?", new String[]{record_id,imp_details_id},db.tbl_townhouse_lot_valuation_details);

		String grand_total_land_val = gds.nullCheck(db.getTownhouse_API_Total_Land_Value_Lot_Valuation_Details(record_id));
		String grand_total_land_val_s = gds.stringToDecimal(grand_total_land_val);

		//save to summary
		DatabaseHandler dbb = new DatabaseHandler(this);
		Townhouse_API liTotal = new Townhouse_API();
		liTotal.setreport_final_value_total_appraised_value_land_imp(grand_total_land_val_s);
		dbb.updateTownhouse_Summary_Total(liTotal, record_id);

		dbb.close();
		db.close();
	}
	
	//EditText checker
	private boolean validate(EditText[] fields){
        for(int i=0; i<fields.length; i++){
            EditText currentField=fields[i];
            if(currentField.getText().toString().length()<=0){
                return false;
            }
        }
        return true;
	}
	public void reloadClass(){
		Intent intent = getIntent();
		finish();
		startActivity(intent);
	}
	public void delete_item(){
		myDialog = new Dialog(Townhouse_Desc_Of_Imp_Features.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.delete_dialog);
		myDialog.setCancelable(false);
		final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
		final Button btn_yes = (Button) myDialog.findViewById(R.id.btn_yes);
		final Button btn_no = (Button) myDialog.findViewById(R.id.btn_no);
		tv_question.setText("Are you sure you want to delete the selected item?");
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				db.deleteTownhouse_API_Imp_Details_Features_Single(record_id, imp_details_features_id);
				db.close();
				myDialog.dismiss();
				reloadClass();
				Toast.makeText(getApplicationContext(), "Data Deleted",
						Toast.LENGTH_SHORT).show();
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		
		myDialog.show();
	}
	// Response from LI_Desc_Of_Imp_View Activity
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// if result code 100
		if (resultCode == 100) {
			// if result code 100 is received 
			// means user edited/deleted product
			// reload this screen again
			Intent intent = getIntent();
			finish();
			startActivity(intent);
		}

	}
	
	/**
	 * Background Async Task to Load all 
	 * */
	class LoadAll extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Townhouse_Desc_Of_Imp_Features.this);
			pDialog.setMessage("Loading List. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All 
		 * */
		protected String doInBackground(String... args) {

			List<Townhouse_API_Imp_Details_Features> liidf = db.getTownhouse_Imp_Details_Features(record_id, imp_details_id);
			if (!liidf.isEmpty()) {
				for (Townhouse_API_Imp_Details_Features imidf : liidf) {
					imp_details_features_id = String.valueOf(imidf.getID());
					record_id = imidf.getrecord_id();
					imidf.getreport_desc_features();
					area = gds.numberFormat(imidf.getreport_desc_features_area());
					description = imidf.getreport_desc_features_area_desc();
					
					HashMap<String, String> map = new HashMap<String, String>();
					map.put(tag_imp_details_features_id, imp_details_features_id);
					map.put(tag_area, area);
					map.put(tag_description, description);
					//Toast.makeText(getApplicationContext(), ""+itemList,Toast.LENGTH_SHORT).show();

					// adding HashList to ArrayList
					itemList.add(map);
				}
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					ListAdapter adapter = new SimpleAdapter(
							Townhouse_Desc_Of_Imp_Features.this, itemList,
							R.layout.th_desc_of_imp_features_list, new String[] { tag_imp_details_features_id, tag_area,
									tag_description},
									new int[] { R.id.primary_key, R.id.item_area, R.id.item_description });
					// updating listview
					setListAdapter(adapter);
				}
			});
			if(itemList.isEmpty()){
				add_new();
			}
		}
	}
	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}


}