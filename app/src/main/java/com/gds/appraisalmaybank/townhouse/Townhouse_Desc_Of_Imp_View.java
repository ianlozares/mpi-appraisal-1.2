package com.gds.appraisalmaybank.townhouse;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.Toast;

import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database2.Townhouse_API_Imp_Details;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Townhouse_Desc_Of_Imp_View  extends Activity implements OnClickListener {
	DatabaseHandler2 db = new DatabaseHandler2(this);
	GDS_methods gds = new GDS_methods();
	Button btn_compute, btn_view_features;
	Button btn_cancel, btn_save;
	AlertDialog.Builder builder;
	AlertDialog dialog_box;
	String record_id = "";
	Session_Timer st = new Session_Timer(this);
	private static final String TAG_RECORD_ID = "record_id";
	private static final String tag_imp_details_id = "imp_details_id";
	private static final String tag_building_desc = "building_desc";
	String imp_details_id = "", building_desc="";

	Dialog myDialog;
	// create dialog
	EditText report_num_of_floors, report_desc_of_bldg, report_erected_on_lot,
			report_fa, report_fa_per_td, report_actual_utilization,
			report_declaration_as_to_usage, report_declared_owner, report_num_of_bedrooms,valrep_townhouse_impsummary_no_of_tb;
	Spinner spinner_type_of_property, spinner_ownership_of_property, spinner_socialized_housing;
	MultiAutoCompleteTextView report_foundation, report_columns, report_beams,
			report_exterior_walls, report_interior_walls, report_flooring,
			report_doors, report_windows, report_ceiling, report_roofing,
			report_trusses;
	EditText report_economic_life, report_effective_age,
			report_remaining_eco_life, report_occupants,
			report_owned_or_leased, report_floor_area;
	Spinner spinner_confirmed_thru, spinner_observed_condition;
	Intent in;
	// forListViewDeclarations
	ArrayList<HashMap<String, String>> itemList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.th_desc_of_imp_form);
		builder = new AlertDialog.Builder(Townhouse_Desc_Of_Imp_View.this);

		st.resetDisconnectTimer();
		btn_view_features = (Button) findViewById(R.id.btn_view_features);
		btn_view_features.setOnClickListener(this);
		btn_save = (Button) findViewById(R.id.btn_right);
		btn_save.setText("Save");
		btn_save.setOnClickListener(this);
		btn_cancel = (Button) findViewById(R.id.btn_left);
		btn_cancel.setText("Cancel");
		btn_cancel.setOnClickListener(this);
		
		report_num_of_floors = (EditText) findViewById(R.id.report_num_of_floors);
		report_desc_of_bldg = (EditText) findViewById(R.id.report_desc_of_bldg);
		report_erected_on_lot = (EditText) findViewById(R.id.report_erected_on_lot);
		report_fa = (EditText) findViewById(R.id.report_fa);
		report_fa_per_td = (EditText) findViewById(R.id.report_fa_per_td);
		report_actual_utilization = (EditText) findViewById(R.id.report_actual_utilization);
		report_declaration_as_to_usage = (EditText)findViewById(R.id.report_declaration_as_to_usage);
		report_declared_owner = (EditText) findViewById(R.id.report_declared_owner);

		spinner_type_of_property = (Spinner) findViewById(R.id.spinner_type_of_property);
		spinner_ownership_of_property = (Spinner) findViewById(R.id.spinner_ownership_of_property);
		spinner_socialized_housing = (Spinner) findViewById(R.id.spinner_socialized_housing);
		spinner_socialized_housing.setVisibility(View.GONE);

		report_foundation = (MultiAutoCompleteTextView) findViewById(R.id.report_foundation);
		report_columns = (MultiAutoCompleteTextView) findViewById(R.id.report_columns);
		report_beams = (MultiAutoCompleteTextView) findViewById(R.id.report_beams);
		report_exterior_walls = (MultiAutoCompleteTextView) findViewById(R.id.report_exterior_walls);
		report_interior_walls = (MultiAutoCompleteTextView) findViewById(R.id.report_interior_walls);
		report_flooring = (MultiAutoCompleteTextView) findViewById(R.id.report_flooring);
		report_doors = (MultiAutoCompleteTextView) findViewById(R.id.report_doors);
		report_windows = (MultiAutoCompleteTextView) findViewById(R.id.report_windows);
		report_ceiling = (MultiAutoCompleteTextView) findViewById(R.id.report_ceiling);
		report_roofing = (MultiAutoCompleteTextView) findViewById(R.id.report_roofing);
		report_trusses = (MultiAutoCompleteTextView) findViewById(R.id.report_trusses);

		report_economic_life = (EditText) findViewById(R.id.report_economic_life);
		report_effective_age = (EditText) findViewById(R.id.report_effective_age);
		report_remaining_eco_life = (EditText) findViewById(R.id.report_remaining_eco_life);
		report_remaining_eco_life.setEnabled(false);
		report_occupants = (EditText) findViewById(R.id.report_occupants);
		report_owned_or_leased = (EditText) findViewById(R.id.report_owned_or_leased);
		report_floor_area = (EditText) findViewById(R.id.report_floor_area);
		//report_floor_area.setEnabled(false);
		spinner_confirmed_thru = (Spinner) findViewById(R.id.spinner_confirmed_thru);
		spinner_observed_condition = (Spinner) findViewById(R.id.spinner_observed_condition);

		report_num_of_bedrooms = (EditText) findViewById(R.id.report_num_of_bedrooms);
		valrep_townhouse_impsummary_no_of_tb = (EditText) findViewById(R.id.valrep_townhouse_impsummary_no_of_tb);

		// getting record_id from intent / previous class
		Intent i = getIntent();
		record_id = i.getStringExtra(TAG_RECORD_ID);
		imp_details_id = i.getStringExtra(tag_imp_details_id);



		fillData();

		gds.multiautocomplete(report_foundation, getResources().getStringArray(R.array.report_foundation), this);
		gds.multiautocomplete(report_columns, getResources().getStringArray(R.array.report_columns), this);
		gds.multiautocomplete(report_beams, getResources().getStringArray(R.array.report_beams), this);
		gds.multiautocomplete(report_exterior_walls, getResources().getStringArray(R.array.report_exterior_walls), this);
		gds.multiautocomplete(report_interior_walls, getResources().getStringArray(R.array.report_interior_walls), this);
		gds.multiautocomplete(report_flooring, getResources().getStringArray(R.array.report_flooring), this);
		gds.multiautocomplete(report_doors, getResources().getStringArray(R.array.report_doors), this);
		gds.multiautocomplete(report_windows, getResources().getStringArray(R.array.report_windows), this);
		gds.multiautocomplete(report_ceiling, getResources().getStringArray(R.array.report_ceiling), this);
		gds.multiautocomplete(report_roofing, getResources().getStringArray(R.array.report_roofing), this);
		gds.multiautocomplete(report_trusses, getResources().getStringArray(R.array.report_trusses), this);

		report_fa.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_fa, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_fa_per_td.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_fa_per_td, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});

		report_economic_life.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				String economic_life_val = gds.nullCheck2(report_economic_life.getText().toString());
				String effective_age_val = gds.nullCheck2(report_effective_age.getText().toString());
				String remaining_eco_life = gds.remainEcoLI(economic_life_val, effective_age_val);
				report_remaining_eco_life.setText(remaining_eco_life);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_effective_age.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				String economic_life_val = gds.nullCheck2(report_economic_life.getText().toString());
				String effective_age_val = gds.nullCheck2(report_effective_age.getText().toString());
				String remaining_eco_life = gds.remainEcoLI(economic_life_val, effective_age_val);
				report_remaining_eco_life.setText(remaining_eco_life);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
	}
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
		Intent intent = new Intent(getApplicationContext(),
				Townhouse_Desc_Of_Imp.class);
		intent.putExtra(TAG_RECORD_ID, record_id);
		//intent.putExtra(tag_imp_details_id, imp_details_id);
		startActivityForResult(intent, 100);
	}
	// Response from TH_Desc_of_imp_form
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// if result code 100
		if (resultCode == 100) {
			// if result code 100 is received 
			// means user edited/deleted product
			// reload this screen again
			Intent intent = getIntent();
			finish();
			startActivity(intent);
		}
	}
	
	public void fillData(){
		List<Townhouse_API_Imp_Details> impDetailsList = db
				.getTownhouse_Imp_Details_with_bldg_desc(String.valueOf(record_id), imp_details_id);
		if (!impDetailsList.isEmpty()) {
			for (Townhouse_API_Imp_Details li : impDetailsList) {
				report_num_of_floors.setText(li.getreport_impsummary1_no_of_floors());
				report_desc_of_bldg.setText(li.getreport_impsummary1_building_desc());
				report_erected_on_lot.setText(li.getreport_impsummary1_erected_on_lot());
				report_fa.setText(gds.numberFormat(li.getreport_impsummary1_fa()));
				report_fa_per_td.setText(gds.numberFormat(li.getreport_impsummary1_fa_per_td()));
				report_actual_utilization.setText(li.getreport_impsummary1_actual_utilization());
				report_declaration_as_to_usage.setText(li.getreport_impsummary1_usage_declaration());
				report_declared_owner.setText(li.getreport_impsummary1_owner());
				spinner_type_of_property.setSelection(gds.spinnervalue(spinner_type_of_property,li.getreport_desc_property_type()));
				spinner_ownership_of_property.setSelection(gds.spinnervalue(spinner_ownership_of_property,li.getreport_ownership_of_property()));

				
				if (li.getreport_impsummary1_socialized_housing().equals("Yes")){
					spinner_socialized_housing.setSelection(0);
				} else if (li.getreport_impsummary1_socialized_housing().equals("No")){
					spinner_socialized_housing.setSelection(1);
				}
				
				
				
				report_foundation.setText(li.getreport_desc_foundation());
				report_columns.setText(li.getreport_desc_columns_posts());
				report_beams.setText(li.getreport_desc_beams());
				report_exterior_walls.setText(li.getreport_desc_exterior_walls());
				report_interior_walls.setText(li.getreport_desc_interior_walls());
				report_flooring.setText(li.getreport_desc_imp_flooring());
				report_doors.setText(li.getreport_desc_doors());
				report_windows.setText(li.getreport_desc_imp_windows());
				report_ceiling.setText(li.getreport_desc_ceiling());
				report_roofing.setText(li.getreport_desc_imp_roofing());
				report_trusses.setText(li.getreport_desc_trusses());
				
				report_economic_life.setText(li.getreport_desc_economic_life());
				report_effective_age.setText(li.getreport_desc_effective_age());
				report_remaining_eco_life.setText(li.getreport_desc_imp_remain_life());
				report_occupants.setText(li.getreport_desc_occupants());
				report_owned_or_leased.setText(li.getreport_desc_owned_or_leased());
				report_floor_area.setText(li.getreport_desc_imp_floor_area());

				spinner_confirmed_thru.setSelection(gds.spinnervalue(spinner_confirmed_thru,li.getreport_desc_confirmed_thru()));
				spinner_observed_condition.setSelection(gds.spinnervalue(spinner_observed_condition,li.getreport_desc_observed_condition()));

				report_num_of_bedrooms.setText(li.getreport_impsummary_no_of_bedroom());
				valrep_townhouse_impsummary_no_of_tb.setText(li.getvalrep_townhouse_impsummary_no_of_tb());
			}
		}
		gds.fill_in_error(new EditText[] {report_foundation,report_columns,report_beams,report_exterior_walls,
				report_interior_walls,report_flooring,report_doors,report_windows,report_ceiling,report_roofing,report_trusses,
				report_num_of_floors,report_desc_of_bldg,report_fa,report_economic_life,
				report_effective_age,report_occupants,valrep_townhouse_impsummary_no_of_tb,report_floor_area,report_num_of_bedrooms});
		gds.fill_in_error_spinner(new Spinner[]{spinner_confirmed_thru,spinner_ownership_of_property});

	}
	public void save_db(){



			//update data in SQLite
			Townhouse_API_Imp_Details li = new Townhouse_API_Imp_Details();
			li.setreport_impsummary1_no_of_floors(report_num_of_floors.getText().toString());
			li.setreport_impsummary1_building_desc(report_desc_of_bldg.getText().toString());
			li.setreport_impsummary1_erected_on_lot(report_erected_on_lot.getText().toString());
			li.setreport_impsummary1_fa(gds.replaceFormat(report_fa.getText().toString()));
			li.setreport_impsummary1_fa_per_td(gds.replaceFormat(report_fa_per_td.getText().toString()));
			li.setreport_impsummary1_actual_utilization(report_actual_utilization.getText().toString());
			li.setreport_impsummary1_usage_declaration(report_declaration_as_to_usage.getText().toString());
			li.setreport_impsummary1_owner(report_declared_owner.getText().toString());
			//li.report_impsummary1_owner = report_declared_owner.getText().toString();
			
			li.setreport_impsummary1_socialized_housing(spinner_socialized_housing.getSelectedItem().toString());
			li.setreport_desc_property_type(spinner_type_of_property.getSelectedItem().toString());
			
			li.setreport_desc_foundation(report_foundation.getText().toString());
			li.setreport_desc_columns_posts(report_columns.getText().toString());
			li.setreport_desc_beams(report_beams.getText().toString());
			li.setreport_desc_exterior_walls(report_exterior_walls.getText().toString());
			li.setreport_desc_interior_walls(report_interior_walls.getText().toString());
			li.setreport_desc_imp_flooring(report_flooring.getText().toString());
			li.setreport_desc_doors(report_doors.getText().toString());
			li.setreport_desc_imp_windows(report_windows.getText().toString());
			li.setreport_desc_ceiling(report_ceiling.getText().toString());
			li.setreport_desc_imp_roofing(report_roofing.getText().toString());
			li.setreport_desc_trusses(report_trusses.getText().toString());
			
			li.setreport_desc_economic_life(report_economic_life.getText().toString());
			li.setreport_desc_effective_age(report_effective_age.getText().toString());
			li.setreport_desc_imp_remain_life(report_remaining_eco_life.getText().toString());
			li.setreport_desc_occupants(report_occupants.getText().toString());
			li.setreport_desc_owned_or_leased(report_owned_or_leased.getText().toString());
			li.setreport_desc_imp_floor_area(report_floor_area.getText().toString());

			li.setreport_desc_confirmed_thru(spinner_confirmed_thru.getSelectedItem().toString());
			li.setreport_desc_observed_condition(spinner_observed_condition.getSelectedItem().toString());
			
			li.setreport_ownership_of_property(spinner_ownership_of_property.getSelectedItem().toString());
			li.setreport_impsummary_no_of_bedroom(report_num_of_bedrooms.getText().toString());
			li.setvalrep_townhouse_impsummary_no_of_tb(valrep_townhouse_impsummary_no_of_tb.getText().toString());

			db.updateTownhouse_Imp_Details(li, record_id, String.valueOf(imp_details_id));


		//recompute valuation
		db.close();
			Toast.makeText(getApplicationContext(), "Saved",Toast.LENGTH_SHORT).show();
			finish();
			Intent intent = new Intent(getApplicationContext(),
					Townhouse_Desc_Of_Imp.class);
			intent.putExtra(TAG_RECORD_ID, record_id);
			//intent.putExtra(tag_imp_details_id, imp_details_id);
			startActivityForResult(intent, 100);

	}
	public void save_db_feautes(){

			//update data in SQLite
			Townhouse_API_Imp_Details li = new Townhouse_API_Imp_Details();
			li.setreport_impsummary1_no_of_floors(report_num_of_floors.getText().toString());
			li.setreport_impsummary1_building_desc(report_desc_of_bldg.getText().toString());
			li.setreport_impsummary1_erected_on_lot(report_erected_on_lot.getText().toString());
			li.setreport_impsummary1_fa(gds.replaceFormat(report_fa.getText().toString()));
			li.setreport_impsummary1_fa_per_td(gds.replaceFormat(report_fa_per_td.getText().toString()));
			li.setreport_impsummary1_actual_utilization(report_actual_utilization.getText().toString());
			li.setreport_impsummary1_usage_declaration(report_declaration_as_to_usage.getText().toString());
			li.setreport_impsummary1_owner(report_declared_owner.getText().toString());
			//li.report_impsummary1_owner = report_declared_owner.getText().toString();

			li.setreport_impsummary1_socialized_housing(spinner_socialized_housing.getSelectedItem().toString());
			li.setreport_desc_property_type(spinner_type_of_property.getSelectedItem().toString());

			li.setreport_desc_foundation(report_foundation.getText().toString());
			li.setreport_desc_columns_posts(report_columns.getText().toString());
			li.setreport_desc_beams(report_beams.getText().toString());
			li.setreport_desc_exterior_walls(report_exterior_walls.getText().toString());
			li.setreport_desc_interior_walls(report_interior_walls.getText().toString());
			li.setreport_desc_imp_flooring(report_flooring.getText().toString());
			li.setreport_desc_doors(report_doors.getText().toString());
			li.setreport_desc_imp_windows(report_windows.getText().toString());
			li.setreport_desc_ceiling(report_ceiling.getText().toString());
			li.setreport_desc_imp_roofing(report_roofing.getText().toString());
			li.setreport_desc_trusses(report_trusses.getText().toString());

			li.setreport_desc_economic_life(report_economic_life.getText().toString());
			li.setreport_desc_effective_age(report_effective_age.getText().toString());
			li.setreport_desc_imp_remain_life(report_remaining_eco_life.getText().toString());
			li.setreport_desc_occupants(report_occupants.getText().toString());
			li.setreport_desc_owned_or_leased(report_owned_or_leased.getText().toString());
			li.setreport_desc_imp_floor_area(report_floor_area.getText().toString());

			li.setreport_desc_confirmed_thru(spinner_confirmed_thru.getSelectedItem().toString());
			li.setreport_desc_observed_condition(spinner_observed_condition.getSelectedItem().toString());

			li.setreport_ownership_of_property(spinner_ownership_of_property.getSelectedItem().toString());

			li.setreport_impsummary_no_of_bedroom(report_num_of_bedrooms.getText().toString());
			li.setvalrep_townhouse_impsummary_no_of_tb(valrep_townhouse_impsummary_no_of_tb.getText().toString());

			db.updateTownhouse_Imp_Details(li, record_id, String.valueOf(imp_details_id));
			db.close();
			Toast.makeText(getApplicationContext(), "Saved",Toast.LENGTH_SHORT).show();
			finish();
			Intent in = new Intent(getApplicationContext(),
					Townhouse_Desc_Of_Imp_Features.class);
			in.putExtra(TAG_RECORD_ID, record_id);
			in.putExtra(tag_building_desc, building_desc);
			in.putExtra(tag_imp_details_id, imp_details_id);

			// starting new activity and expecting some response back
			startActivityForResult(in, 100);

	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub

		switch (arg0.getId()) {
		
		/*case R.id.btn_compute://cancel
			compute();
			break;*/
		case R.id.btn_view_features:

			save_db_feautes();
			//btn_save.performClick();
			//Toast.makeText(getApplicationContext(), ""+record_id+"\n"+building_desc,Toast.LENGTH_SHORT).show();
			// Starting new intent


			break;
		case R.id.btn_right://save
			if(checkHasFeatures()){
				save_db();
			}else{
				builder.setMessage("Features/Division is Empty, Do you want to add Features/Division?")
						.setTitle("Features/Division Empty");

				builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog_box.dismiss();
						save_db();
					}
				});

				builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						save_db_feautes();
					}
				});

				dialog_box = builder.create();
				dialog_box.show();

			}
			break;
		case R.id.btn_left://cancel
			if(checkHasFeatures()){
				finish();
				in = new Intent(getApplicationContext(),
						Townhouse_Desc_Of_Imp.class);
				in.putExtra(TAG_RECORD_ID, record_id);
				startActivityForResult(in, 100);
			}else{
				builder.setMessage("Features/Division is Empty, Do you want to add Features/Division?")
						.setTitle("Features/Division Empty");

				builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog_box.dismiss();
						finish();
						in = new Intent(getApplicationContext(),
								Townhouse_Desc_Of_Imp.class);
						in.putExtra(TAG_RECORD_ID, record_id);
						startActivityForResult(in, 100);
					}
				});

				builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						save_db_feautes();
					}
				});

				dialog_box = builder.create();
				dialog_box.show();

			}

			break;
		default:
			break;
		}
	}
	// EditText checker
	private boolean validate(EditText[] fields) {
		for (int i = 0; i < fields.length; i++) {
			EditText currentField = fields[i];
			if (currentField.getText().toString().length() <= 0) {
				return false;
			}
		}
		return true;
	}
	public boolean checkHasFeatures(){
		String where ="WHERE record_id = args and imp_details_id = args";
		String[] args = new String[2];
		args[0] = record_id;
		args[1] = imp_details_id;
		String x = gds.nullCheck(db.getRecord("imp_details_features_id", where, args, "townhouse_imp_details_features").get(0));
		Log.e("Has Feature",""+x);
		if(x.contentEquals("0")){
			return false;
		}else{
			return true;
		}
	}
    private boolean validateSpinner(Spinner[] fields) {
        for (int i = 0; i < fields.length; i++) {
            Spinner currentField = fields[i];
            if (currentField.getSelectedItem().toString().length() <= 0) {
                return false;
            }
        }
        return true;
    }
	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}


}