package com.gds.appraisalmaybank.townhouse;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database2.Townhouse_API_Imp_Details;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Townhouse_Desc_Of_Imp extends ListActivity implements OnClickListener {
	DatabaseHandler2 db = new DatabaseHandler2(this);
	GDS_methods gds = new GDS_methods();
	Button btn_add_desc_of_imp, btn_save_desc_of_imp;
	Button btn_create, btn_cancel, btn_save, btn_delete, btn_compute;

	Session_Timer st = new Session_Timer(this);
	String record_id="";
	private static final String TAG_RECORD_ID = "record_id";
	private static final String tag_imp_details_id = "imp_details_id";
	
	Dialog myDialog;
	//create dialog
	EditText report_td_no,report_num_of_floors, report_desc_of_bldg, report_erected_on_lot, report_fa, report_fa_per_td,
		report_actual_utilization, report_declaration_as_to_usage, report_declared_owner, report_num_of_beds,valrep_townhouse_impsummary_no_of_tb;
	Spinner spinner_type_of_property, spinner_ownership_of_property, spinner_socialized_housing;
	EditText report_foundation, report_columns, report_beams, report_exterior_walls, report_interior_walls,
		report_flooring, report_doors, report_windows, report_ceiling, report_roofing, report_trusses;
	EditText report_economic_life, report_effective_age,
		report_remaining_eco_life, report_occupants, report_owned_or_leased, report_floor_area;
	Spinner spinner_confirmed_thru, spinner_observed_condition;
	//forListViewDeclarations
	private ProgressDialog pDialog;
	ArrayList<HashMap<String, String>> itemList;
	private static final String tag_building_desc = "building_desc";
	private static final String tag_type_of_property = "type_of_property";
	String imp_details_id="";
	String where = "WHERE record_id = args";
	final String[] args = new String[1];
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reports_land_desc_of_imp);

		st.resetDisconnectTimer();
		btn_add_desc_of_imp = (Button) findViewById(R.id.btn_add_desc_of_imp);
		btn_add_desc_of_imp.setOnClickListener(this);
		btn_add_desc_of_imp.setVisibility(View.GONE);
		
		btn_save_desc_of_imp = (Button) findViewById(R.id.btn_save_desc_of_imp);
		btn_save_desc_of_imp.setOnClickListener(this);
		report_td_no = (EditText)findViewById(R.id.report_td_no);
		// getting record_id from intent / previous class
		Intent i = getIntent();
		record_id = i.getStringExtra(TAG_RECORD_ID);
		//list
		itemList = new ArrayList<HashMap<String, String>>();
		new LoadAll().execute();
		// Get listview
		ListView lv = getListView();

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				savedb();
				// getting values from selected ListItem
				imp_details_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
				// Starting new intent
				finish();
				Intent in = new Intent(getApplicationContext(),
						Townhouse_Desc_Of_Imp_View.class);
				in.putExtra(TAG_RECORD_ID, record_id);
				in.putExtra(tag_imp_details_id, imp_details_id);
				// starting new activity and expecting some response back
				startActivityForResult(in, 0);

			}
		});
		/*lv.setOnItemLongClickListener(new OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
                // TODO Auto-generated method stub

                Log.v("long clicked","pos: " + position);
                imp_details_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
                delete_item();
                return true;
            }
        }); */
		args[0] = record_id;
		report_td_no.setText(db.getRecord("report_townhouse_td_no", where, args, "townhouse").get(0));
	}
	
	/*// Response from TH_Desc_of_imp_form
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// if result code 100
		if (resultCode == 100) {
			// if result code 100 is received 
			// means user edited/deleted product
			// reload this screen again
			Intent intent = getIntent();
			//finish();
			startActivity(intent);
		}
	}*/
	public void savedb() {
		ArrayList<String> field = new ArrayList<String>();
		field.clear();
		field.add("report_townhouse_td_no");

		ArrayList<String> value = new ArrayList<String>();
		value.clear();
		value.add(report_td_no.getText().toString());

		db.updateRecord(value, field, "record_id = ? ", new String[]{record_id}, "townhouse");
	}
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.btn_add_desc_of_imp:
			add_new();
			break;
		case R.id.btn_save_desc_of_imp:

			savedb();
			finish();
			break;
		default:
			break;
		}
	}
	
	public void add_new(){
		Toast.makeText(getApplicationContext(), "Add New",Toast.LENGTH_SHORT).show();
		myDialog = new Dialog(Townhouse_Desc_Of_Imp.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.th_desc_of_imp_form);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.FILL_PARENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		
		btn_compute = (Button) myDialog.findViewById(R.id.btn_compute);
		btn_create = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_create.setText("Create");
		btn_cancel.setText("Cancel");
		
		report_num_of_floors = (EditText) myDialog.findViewById(R.id.report_num_of_floors);
		report_desc_of_bldg = (EditText) myDialog.findViewById(R.id.report_desc_of_bldg);
		report_erected_on_lot = (EditText) myDialog.findViewById(R.id.report_erected_on_lot);
		report_fa = (EditText) myDialog.findViewById(R.id.report_fa);
		report_fa_per_td = (EditText) myDialog.findViewById(R.id.report_fa_per_td);
		report_actual_utilization = (EditText) myDialog.findViewById(R.id.report_actual_utilization);
		report_declaration_as_to_usage = (EditText) myDialog.findViewById(R.id.report_declaration_as_to_usage);
		report_declared_owner = (EditText) myDialog.findViewById(R.id.report_declared_owner);
		
		spinner_type_of_property = (Spinner) myDialog.findViewById(R.id.spinner_type_of_property);
		spinner_ownership_of_property = (Spinner) myDialog.findViewById(R.id.spinner_ownership_of_property);
		spinner_socialized_housing = (Spinner) myDialog.findViewById(R.id.spinner_socialized_housing);
		spinner_socialized_housing.setVisibility(View.GONE);
		report_foundation = (EditText) myDialog.findViewById(R.id.report_foundation);
		report_columns = (EditText) myDialog.findViewById(R.id.report_columns);
		report_beams = (EditText) myDialog.findViewById(R.id.report_beams);
		report_exterior_walls = (EditText) myDialog.findViewById(R.id.report_exterior_walls);
		report_interior_walls = (EditText) myDialog.findViewById(R.id.report_interior_walls);
		report_flooring = (EditText) myDialog.findViewById(R.id.report_flooring);
		report_doors = (EditText) myDialog.findViewById(R.id.report_doors);
		report_windows = (EditText) myDialog.findViewById(R.id.report_windows);
		report_ceiling = (EditText) myDialog.findViewById(R.id.report_ceiling);
		report_roofing = (EditText) myDialog.findViewById(R.id.report_roofing);
		report_trusses = (EditText) myDialog.findViewById(R.id.report_trusses);
		
		report_economic_life = (EditText) myDialog.findViewById(R.id.report_economic_life);
		report_effective_age = (EditText) myDialog.findViewById(R.id.report_effective_age);
		report_remaining_eco_life = (EditText) myDialog.findViewById(R.id.report_remaining_eco_life);
		report_occupants = (EditText) myDialog.findViewById(R.id.report_occupants);
		report_owned_or_leased = (EditText) myDialog.findViewById(R.id.report_owned_or_leased);
		report_floor_area = (EditText) myDialog.findViewById(R.id.report_floor_area);
		spinner_confirmed_thru = (Spinner) myDialog.findViewById(R.id.spinner_confirmed_thru);
		spinner_observed_condition = (Spinner) myDialog.findViewById(R.id.spinner_observed_condition);
		report_num_of_beds = (EditText) myDialog.findViewById(R.id.report_num_of_bedrooms);
		valrep_townhouse_impsummary_no_of_tb = (EditText) myDialog.findViewById(R.id.valrep_townhouse_impsummary_no_of_tb);

		report_fa.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_fa, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_fa_per_td.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_fa_per_td, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});

		btn_compute.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				gds.fill_in_error(new EditText[] { report_num_of_floors,report_desc_of_bldg,report_fa,report_economic_life,
						report_effective_age,report_occupants,valrep_townhouse_impsummary_no_of_tb });
				boolean fieldsOK = validate(new EditText[] { report_num_of_floors,report_desc_of_bldg,report_fa,report_economic_life,
						report_effective_age,report_occupants,valrep_townhouse_impsummary_no_of_tb });
				if (fieldsOK == true) { // check if all EditTexts in array are filled up
					int economic_life_val = Integer.parseInt(report_economic_life
							.getText().toString());
					int effective_age_val = Integer.parseInt(report_effective_age
							.getText().toString());
					report_remaining_eco_life.setText(String
							.valueOf((economic_life_val - effective_age_val)));
				}else{
					Toast.makeText(getApplicationContext(),
						"Please fill up required fields", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		btn_create.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				gds.fill_in_error(new EditText[] {report_num_of_floors,report_desc_of_bldg,report_fa,report_economic_life,
						report_effective_age,report_occupants,valrep_townhouse_impsummary_no_of_tb});
				//list of EditTexts put in array
				boolean fieldsOK = validate(new EditText[] {report_num_of_floors,report_desc_of_bldg,report_fa,report_economic_life,
						report_effective_age,report_occupants,valrep_townhouse_impsummary_no_of_tb});
				if (fieldsOK){ //check if all EditTexts in array are filled up
					//add data in SQLite
					db.addTownhouse_Imp_Details(new Townhouse_API_Imp_Details(
							record_id,
							report_num_of_floors.getText().toString(),
							report_desc_of_bldg.getText().toString(),
							report_erected_on_lot.getText().toString(),
							gds.replaceFormat(report_fa.getText().toString()),
							gds.replaceFormat(report_fa_per_td.getText().toString()),
							report_actual_utilization.getText().toString(),
							report_declaration_as_to_usage.getText().toString(),
							report_declared_owner.getText().toString(),
							
							spinner_socialized_housing.getSelectedItem().toString(),
							spinner_type_of_property.getSelectedItem().toString(),
							
							report_foundation.getText().toString(),
							report_columns.getText().toString(),
							report_beams.getText().toString(),
							report_exterior_walls.getText().toString(),
							report_interior_walls.getText().toString(),
							report_flooring.getText().toString(),
							report_doors.getText().toString(),
							report_windows.getText().toString(),
							report_ceiling.getText().toString(),
							report_roofing.getText().toString(),
							report_trusses.getText().toString(),
							report_economic_life.getText().toString(),
							report_effective_age.getText().toString(),
							report_remaining_eco_life.getText().toString(),
							report_occupants.getText().toString(),
							report_owned_or_leased.getText().toString(),
							report_floor_area.getText().toString(),
							spinner_confirmed_thru.getSelectedItem().toString(),
							spinner_observed_condition.getSelectedItem().toString(),
							spinner_ownership_of_property.getSelectedItem().toString(),
							report_num_of_beds.getText().toString(),
							valrep_townhouse_impsummary_no_of_tb.getText().toString()));
					
					//advance computations for valutaion cost
					//auto_compute_less_dep_percent();

						Toast.makeText(getApplicationContext(),
								"data created", Toast.LENGTH_SHORT).show();
						myDialog.dismiss();
						reloadClass();
				}else{
					Toast.makeText(getApplicationContext(),
						"Please fill up required fields", Toast.LENGTH_SHORT).show();
				}
				
			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		myDialog.show();
	}
	
	/*public void auto_compute_less_dep_percent(){
		economic_life_val = Double.parseDouble(report_economic_life.getText().toString());
		effective_age_val = Double.parseDouble(report_effective_age.getText().toString());
		less_dep_percent_val = ((effective_age_val/economic_life_val)*100);
	}*/
	
	//EditText checker
	private boolean validate(EditText[] fields){
        for(int i=0; i<fields.length; i++){
            EditText currentField=fields[i];
            if(currentField.getText().toString().length()<=0){
                return false;
            }
        }
        return true;
	}
	public void reloadClass(){
		Intent intent = getIntent();
		finish();
		startActivity(intent);
	}
	public void delete_item(){
		myDialog = new Dialog(Townhouse_Desc_Of_Imp.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.delete_dialog);
		myDialog.setCancelable(true);
		final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
		final Button btn_yes = (Button) myDialog.findViewById(R.id.btn_yes);
		final Button btn_no = (Button) myDialog.findViewById(R.id.btn_no);
		tv_question.setText("Are you sure you want to delete the selected item?");
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//sub table details must be deleted first
				db.deleteTownhouse_API_Imp_Details_Features(record_id, imp_details_id);//delete desc of imp features (all)
				db.deleteTownhouse_API_Imp_Details_Single(record_id, imp_details_id);//delete desc of imp
				db.close();
				myDialog.dismiss();
				reloadClass();
				Toast.makeText(getApplicationContext(), "Data Deleted",
						Toast.LENGTH_SHORT).show();
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		
		myDialog.show();
	}

	
	/**
	 * Background Async Task to Load all 
	 * */
	class LoadAll extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Townhouse_Desc_Of_Imp.this);
			pDialog.setMessage("Loading List. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All 
		 * */
		protected String doInBackground(String... args) {
			String building_desc="", type_of_property="";

			List<Townhouse_API_Imp_Details> liid = db.getTownhouse_Imp_Details(record_id);
			if (!liid.isEmpty()) {
				for (Townhouse_API_Imp_Details imid : liid) {
					imp_details_id = String.valueOf(imid.getID());
					imid.getreport_impsummary1_no_of_floors();
					building_desc = imid.getreport_impsummary1_building_desc();
					imid.getreport_impsummary1_erected_on_lot();
					imid.getreport_impsummary1_fa();
					imid.getreport_impsummary1_fa_per_td();
					imid.getreport_impsummary1_actual_utilization();
					imid.getreport_impsummary1_usage_declaration();
					imid.getreport_impsummary1_owner();
					type_of_property = imid.getreport_desc_property_type();
					imid.getreport_impsummary1_socialized_housing();
					
					HashMap<String, String> map = new HashMap<String, String>();
					map.put(tag_imp_details_id, imp_details_id);
					map.put(tag_building_desc, building_desc);
					map.put(tag_type_of_property, type_of_property);
					//Toast.makeText(getApplicationContext(), ""+itemList,Toast.LENGTH_SHORT).show();

					// adding HashList to ArrayList
					itemList.add(map);
				}
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					ListAdapter adapter = new SimpleAdapter(
							Townhouse_Desc_Of_Imp.this, itemList,
							R.layout.reports_land_desc_of_imp_list, new String[] { tag_imp_details_id,
									tag_building_desc,
									tag_type_of_property},
									new int[] { R.id.primary_key, R.id.item_desc_of_bldg, R.id.item_type_of_property });
					// updating listview
					setListAdapter(adapter);
				}
			});

		}
	}
	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}

}