package com.gds.appraisalmaybank.methods;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.CountDownTimer;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;

import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Motor_Vehicle_API;
import com.gds.appraisalmaybank.database.Report_Accepted_Jobs;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database_new.DatabaseHandler_New;
import com.gds.appraisalmaybank.database_new.Tables;
import com.gds.appraisalmaybank.json.MyHttpClient;
import com.gds.appraisalmaybank.json.TLSConnection;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by GDS: Ian Lozares on 11/2/2015.
 */
public class GDS_methods {
    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";
    ArrayList<String> add_common_details = new ArrayList<String>();
    public void menuItems(Menu menu, String appraisal_type){

        if(appraisal_type.contentEquals("Property Attachments")){

            menu.findItem(R.id.action_camera).setVisible(true);
            menu.findItem(R.id.action_done).setVisible(true);

        }
    }
    //EditText checker
    public boolean validate(EditText[] fields) {
        for (int i = 0; i < fields.length; i++) {
            EditText currentField = fields[i];
            if (currentField.getText().toString().trim().contentEquals("")) {
                return false;
            }
        }
        return true;
    }
    //EditText checker
    public boolean validate(String[] fields) {
        for (int i = 0; i < fields.length; i++) {
            String currentField = fields[i];
            if (currentField.contentEquals("")) {
                return false;
            }
        }
        return true;
    }
    public boolean nullCheck(EditText fields){

        if(fields.getText().toString().length()<=0){
            //fields.requestFocus();
            return false;
        }

        return true;
    }
    public String cbChecker(CheckBox cbName) {
        String cbValue = "";
        if (cbName.isChecked()) {
            cbValue = "true";
        } else {
            cbValue = "false";
        }
        return cbValue;
    }
    public void cbDisplay(CheckBox cbName, String value) {
        if (value.contentEquals("true")) {
            cbName.setChecked(true);
        } else {
            cbName.setChecked(false);
        }
    }
    public boolean cbReqChecker(CheckBox[] cbAry) {
        for (int x = 0; x < cbAry.length; x++){
            if (cbAry[x].isChecked()){
                return true;
            }
        }
        return false;
    }
    public boolean spReqChecker(Spinner[] spAry) {
        for (int x = 0; x < spAry.length; x++){
            if (spAry[x].getSelectedItem().toString().contentEquals("")){
                return false;
            }
        }
        return true;
    }
    public String nullChecker(String name){
        if(name.contentEquals("null")){
            name = "";
        }
        return name;
    }
    public String nullCheck(String name){
        if(name.equals("null") || name.equals("") || name.equals("0")||name.equals("0.00")|| name.equals(null)||name.contentEquals("null")){
            name="0";
        }
        return name;
    }
    public String nullCheck2(String name){
        if(name.equals("null") || name.equals("") || name.equals("0")||name.equals("0.00")|| name.equals(null)||name.contentEquals("No record found")||name.contentEquals("no record found")){
            name="0";
        }
        return name;
    }
    public String nullCheck3(String name){
        if(name.equals("null") || name.equals("") || name.equals("0")||name.equals("0.00")|| name.equals(null)||name.contentEquals("null")){
            name="";
        }
        return name;
    }
    public String nullCheck4(String name){
        if(name.equals("null") || name.equals("")|| name.equals(null)){
            name="";
        }
        return name;
    }
    //field checker
    public boolean checker(
            AutoCompleteTextView[] actfields,
            EditText[] etfields,
            Spinner[] spfields){


        //prompt all autocomplete
        if (actfields != null) {
            for(int i=0; i<actfields.length; i++){
                AutoCompleteTextView currentField=actfields[i];
                if(currentField.getText().toString().length()<=0){
                    currentField.requestFocus();
                    currentField.setError("Fill up this field");
//					currentField.setBackgroundColor(Color.YELLOW);
                    currentField.requestFocus();
                } else {
                    currentField.setBackgroundResource(R.drawable.item_frame);
                }
            }
        }

        if (actfields != null) {
            for(int i=0; i<actfields.length; i++){
                AutoCompleteTextView currentField=actfields[i];
                if(currentField.getText().toString().length()<=0){
                    currentField.requestFocus();
                    currentField.setError("Fill up this field");
//					currentField.setBackgroundColor(Color.YELLOW);
                    currentField.requestFocus();
                    return false;
                } else {
                    currentField.setBackgroundResource(R.drawable.item_frame);
                }
            }
        }

        //prompt all edittext
        if (etfields != null) {
            for(int i=0; i<etfields.length; i++){
                EditText currentField=etfields[i];
                if(currentField.getText().toString().length()<=0){
//					currentField.setBackgroundColor(Color.YELLOW);
                    currentField.setFocusable(true);
                    currentField.setFocusableInTouchMode(true);
                    currentField.requestFocus();
                } else {
                    currentField.setBackgroundResource(R.drawable.item_frame);
                }
            }
        }


        if (etfields != null) {
            for(int i=0; i<etfields.length; i++){
                EditText currentField=etfields[i];
                if(currentField.getText().toString().length()<=0){
//					currentField.setBackgroundColor(Color.YELLOW);
                    currentField.setFocusable(true);
                    currentField.setFocusableInTouchMode(true);
                    currentField.requestFocus();
                    return false;
                } else {
                    currentField.setBackgroundResource(R.drawable.item_frame);
                }
            }
        }

        //prompt all spinner
        if (spfields != null) {
            for(int i=0; i<spfields.length; i++){
                Spinner currentField=spfields[i];
                if(currentField.getSelectedItem().toString().length()<=0){
                    currentField.setBackgroundColor(Color.LTGRAY);
                    currentField.setFocusable(true);
                    currentField.setFocusableInTouchMode(true);
                    currentField.requestFocus();
                } else {
                    currentField.setBackgroundResource(R.drawable.item_frame);
                }
            }
        }

        if (spfields != null) {
            for(int i=0; i<spfields.length; i++){
                Spinner currentField=spfields[i];
                if(currentField.getSelectedItem().toString().length()<=0){
                    currentField.setBackgroundColor(Color.LTGRAY);
                    currentField.setFocusable(true);
                    currentField.setFocusableInTouchMode(true);
                    currentField.requestFocus();
                    return false;
                } else {
                    currentField.setBackgroundResource(R.drawable.item_frame);
                }
            }
        }

        return true;
    }


    public Double StringtoDouble(String s){

        Double d = 0.00;


        if(!s.contentEquals("null") && !s.contentEquals("")&& !s.contentEquals("0") && !s.contentEquals("No record found")) {

            if (s.contains("(")) {
                s = s.replaceAll("[()]", "");
                s = "-" + s;
            }

            d = Double.parseDouble(s.replaceAll("[₱,?]", ""));
        }
        return d;

    }
    public Integer StringtoInt(String s){

        int d = 0;

        if(!s.contentEquals("null") && !s.contentEquals("") && !s.contentEquals("No record found")) {
            d = Integer.parseInt(s);
        }
        return d;

    }
    public ArrayList<String> emptyFields(int numOfFields,String record_id){
        add_common_details.clear();
        add_common_details.add(record_id);
        for(int i = 0; i <= numOfFields; i++){
            add_common_details.add("");
        }

        return add_common_details;
    }
    public ArrayList<String> expandDate(DatePicker dp) {

        ArrayList<String> dateArrayList = new ArrayList<String>();
        int month = dp.getMonth() + 1;
        String s_month = "" + month;
        if (month <= 9) {
            s_month = "0" + month;
        }
        int day = dp.getDayOfMonth();
        int year = dp.getYear();

        dateArrayList.add(s_month);
        dateArrayList.add("" + day);
        dateArrayList.add("" + year);

        return dateArrayList;
    }
    public void reworkMethod(String xml, String array, DatabaseHandler2 db, ArrayList<String> db_common_details, String[] valrep, String record_id, String table) {

        try {

            JSONObject json = new JSONObject(xml);

            JSONArray jsonArray = json.getJSONArray(array);

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonObject = jsonArray.getJSONObject(i);
                ArrayList<String> createArrayList = new ArrayList<String>();
                createArrayList.clear();
                createArrayList.add(record_id);

                for (int j = 0; j < valrep.length; j++) {
                    if (jsonObject.has(valrep[j])) {
                        createArrayList.add(jsonObject.getString(valrep[j]));
                    } else {
                        createArrayList.add("");
                    }
                }

                db.addRecord(createArrayList, db_common_details, table);

            }


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            Log.e("", e.toString());
        }

    }

    public void reworkMethod(String xml, DatabaseHandler2 db, ArrayList<String> db_common_details, String[] valrep, String record_id, String table){

        String[] sqlite = new String [db_common_details.size()];
        ArrayList<String> returnArrayList = new ArrayList<String>();

        try {

            JSONObject json = new JSONObject(xml);

            for (int i = 0; i < db_common_details.size(); i++) {
                if (json.has(valrep[i])) {

                    sqlite[i] = json.getString(valrep[i]);

                }else{

                    sqlite[i] = "";

                }

                returnArrayList.add(sqlite[i]);
                Log.e("rework response: ", "" + sqlite[i]);
            }

            db.updateRecord(returnArrayList, db_common_details, "record_id = ?", new String[]{record_id}, table);


        }catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            Log.e("", e.toString());
        }

    }
    public void reworkMethodNestedArray(String xml, DatabaseHandler db, String array, ArrayList<String> arrayList, String array2, ArrayList<String> db_common_details, String[] valrep, String table) {
        try {

            JSONObject json = new JSONObject(xml);
            JSONArray jsonArray = json.getJSONArray(array);

            JSONObject jsonObject = jsonArray.getJSONObject(0);
            JSONArray jsonArray2 = jsonObject.getJSONArray(array2);

            Log.e("jsonObject2 size", "" + jsonArray2.length());
            ArrayList<String> createArrayList = new ArrayList<String>();

            for (int i = 0; i < jsonArray2.length(); i++) {
                Log.e("jsonObject2", "" + i);
                JSONObject jsonObject2 = jsonArray2.getJSONObject(i);

                createArrayList.clear();

                for (int j = 0; j < arrayList.size(); j++) {
                    createArrayList.add(arrayList.get(j));
                }

                for (int j = 0; j < valrep.length; j++) {
                    createArrayList.add(jsonObject2.getString(valrep[j]));
                }

                Log.e("array", createArrayList.toString());
                db.addRecord(createArrayList, db_common_details, table);

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            Log.e("", e.toString());
        }
    }

    public void reworkMethodNestedArray2(String xml, DatabaseHandler2 db, String record_id, String array, String array2, ArrayList<String> db_common_details, ArrayList<String> db_common_details2, String[] valrep,String[] valrep2, String table, String table2) {
        try {

            JSONObject json = new JSONObject(xml);
            JSONArray jsonArray = json.getJSONArray(array);
            for (int x = 0; x < jsonArray.length(); x++) {
                JSONObject jsonObject = jsonArray.getJSONObject(x);
                JSONArray jsonArray2 = jsonObject.getJSONArray(array2);

                Log.e("jsonObject2 size", "" + jsonArray2.length());
                ArrayList<String> createArrayList = new ArrayList<String>();
                createArrayList.add(record_id);

                for (int j = 0; j < valrep.length; j++) {
                    createArrayList.add(jsonObject.getString(valrep[j]));
                }
                Log.e("array1", createArrayList.toString());
                db.addRecord(createArrayList, db_common_details, table);

                String where ="WHERE record_id = args";
                String[] args = new String[1];
                args[0] = record_id;

                String imp_valuation_id = db.getRecord("id",where,args,table).get(x);

                for (int i = 0; i < jsonArray2.length(); i++) {
                    Log.e("jsonObject2", "" + i);
                    JSONObject jsonObject2 = jsonArray2.getJSONObject(i);

                    createArrayList.clear();
                    createArrayList.add(imp_valuation_id);
                    createArrayList.add(record_id);

                    for (int j = 0; j < valrep.length; j++) {
                        createArrayList.add(jsonObject2.getString(valrep2[j]));
                    }

                    Log.e("array2", createArrayList.toString());
                    db.addRecord(createArrayList, db_common_details2, table2);

                }
            }







        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            Log.e("", e.toString());
        }
    }

    public String stringToDecimal(String s1){


        double r = Double.parseDouble(s1.replaceAll("[₱,?]", ""));

        return String.format("%.2f", r);
    }

    public String replaceFormat(String s){
        String clean_s = s.replaceAll("[,]", "");

        return clean_s;
    }
    public String round(String s1){

        double d1,r;

        d1 = StringtoDouble(s1);
        r=(double) Math.round(d1*100)/100;

        return String.format("%.2f", r);
    }
    public String multiplication(String s1, String s2){
        String result = "0";

        double d1, d2,r;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);

        result = ""+ (d1*d2);
        r = Double.parseDouble(result);
        return String.format("%.2f", r);
    }
    public String multiplicationDecimal(String s1, String s2){
        String result = "0";

        double d1, d2,r;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);

        result = ""+ (d1*d2);
        r = Double.parseDouble(result);
        return ""+r;
    }
    public String division(String s1, String s2){
        String result = "0";

        Double d1, d2, r;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);

        result = ""+ (d1 / d2);

        r = Double.parseDouble(result);
        if(r.isNaN()||r.isInfinite()){
            r=0.00;
        }

        return String.format("%.2f", r);
    }
    public String divisionDecimal(String s1, String s2){
        String result = "0";

        Double d1, d2, r;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);

        result = ""+ (d1 / d2);

        r = Double.parseDouble(result);
        if(r.isNaN()||r.isInfinite()){
            r=0.00;
        }

        return ""+r;
    }
    public String addition(String s1, String s2){
        String result = "0";
        double r;
        double d1;
        double d2;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);

        result = "" + (d1+d2);

        r = Double.parseDouble(result);

        return String.format("%.2f", r);
    }

    public String subtraction(String s1, String s2){
        String result = "0";
        double r;
        double d1;
        double d2;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);

        result = "" + (d1-d2);

        r = Double.parseDouble(result);

        return String.format("%.2f", r);
    }

    public String simpleAverage(String s1, String s2){
        String result = "0";
        result=division(multiplication(s1,s2),"100");

        // result = "" + (d1*d2)/100;

        return result;
    }




    public String lessAmount(String s1, String s2){

        String result = "0";
        double r;
        Double d1, d2;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);

        result = "" + (d1 - d2);

        r = Double.parseDouble(result);

        return String.format("%.2f", r);
    }


    public String timeAdjAmount(String s1, String s2){

        String result = "0";
        double r;
        double d1, d2;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);

        result = "" + (d1 *(d2*0.01));

        r = Double.parseDouble(result);

        return String.format("%.2f", r);
    }
    public String computeIndexPrice(String s1, String s2, String s3, String s4){

        String result = "0";
        double r;
        double d1, d2, d3, d4;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);
        d3 = StringtoDouble(s3);
        d4 = StringtoDouble(s4);

        result = "" + (d1 + (d2+d3+d4));

        r = Double.parseDouble(result);

        return String.format("%.2f", r);
    }


    public String computeAverage(String s1, String s2, String s3){

        String result = "0";
        double r;
        Double d1, d2, d3,count=3.0;
        if(s1.contentEquals("0")||s1.contentEquals("")){
            count=count-1;
        }
        if(s2.contentEquals("0")||s2.contentEquals("")){
            count=count-1;
        }
        if(s3.contentEquals("0")||s3.contentEquals("")){
            count=count-1;
        }
        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);
        d3 = StringtoDouble(s3);

        result = "" + (d1+d2+d3)/count;

        r = Double.parseDouble(result);

        return String.format("%.2f", r);
    }



    public String computeCount(String s1, String s2, String s3){

        String result = "0";
        double r;
        int count=3;
        if(s1.contentEquals("0")||s1.contentEquals("")){
            count=count-1;
        }
        if(s2.contentEquals("0")||s2.contentEquals("")){
            count=count-1;
        }
        if(s3.contentEquals("0")||s3.contentEquals("")){
            count=count-1;
        }

        result = "" +count;

        return result;
    }

    public String rectif1(String s1, String s2, String s3, String s4, String s5, String s6, String s7, String s8, String s9){

        String result = "0";
        double r;
        Double d1, d2, d3,d4,d5,d6,d7,d8,d9;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);
        d3 = StringtoDouble(s3);
        d4 = StringtoDouble(s4);
        d5 = StringtoDouble(s5);
        d6 = StringtoDouble(s6);
        d7 = StringtoDouble(s7);
        d8 = StringtoDouble(s8);
        d9 = StringtoDouble(s9);

        result = "" + (d1+d2+d3+d4+d5+d6+d7+d8+d9);

        r = Double.parseDouble(result);

        return String.format("%.2f", r);
    }

    public String rectif2(String s1, String s2, String s3, String s4, String s5, String s6, String s7, String s8,
                          String s9, String s10, String s11, String s12, String s13){

        String result = "0";
        double r;
        Double d1, d2, d3,d4,d5,d6,d7, d8, d9,d10,d11,d12,d13;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);
        d3 = StringtoDouble(s3);
        d4 = StringtoDouble(s4);
        d5 = StringtoDouble(s5);
        d6 = StringtoDouble(s6);
        d7 = StringtoDouble(s7);
        d8 = StringtoDouble(s8);
        d9 = StringtoDouble(s9);
        d10 = StringtoDouble(s10);
        d11 = StringtoDouble(s11);
        d12 = StringtoDouble(s12);
        d13 = StringtoDouble(s13);

        result = "" + (d1+d2+d3+d4+d5+d6+d7+d8+d9+d10+d11+d12+d13);

        r = Double.parseDouble(result);

        return String.format("%.2f", r);
    }

    public String computeAppraisedVal(String s1, String s2){

        String result = "0";
        double r;
        Double d1, d2;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);

        result = "" + (d1-d2);

        r = Double.parseDouble(result);

        return String.format("%.2f", r);
    }

    public String getMin(String s1, String s2, String s3){
        String result = "0";
        double r;
        Double d1, d2, d3;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);
        d3 = StringtoDouble(s2);
        double smallest = Math.min(d1, Math.min(d2, d3));
        result = "" + smallest;

        r = Double.parseDouble(result);

        return String.format("%.2f", r);
    }

    public String getMax(String s1, String s2, String s3){
        String result = "0";
        double r;
        Double d1, d2, d3;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);
        d3 = StringtoDouble(s2);
        double largest = Math.max(d1, Math.max(d2, d3));
        result = "" + largest;

        r = Double.parseDouble(result);

        return String.format("%.2f", r);
    }
    public String roundToThousand(String s1){
        String result = "0";
        double r;
        double d1;
        double d2;

        d1 = StringtoDouble(s1);
        d2 = ((int) ((d1 / 1000.0) + 0.5)) * 1000;
        result = "" +d2;

        r = Double.parseDouble(result);

        return String.format("%.2f", r);
    }
    public String roundToHundred(String s1){
        double d1;
        d1 = StringtoDouble(s1);
        double rounded = (double) (Math.round(d1 / 100) * 100);

        return String.format("%.2f", rounded);
    }
    public String getPriceSQM(String s1, String s2){
        String result = "0";
        result = division(s1,s2);

        return result;
    }

    public int spinnervalue(Spinner spinner, String value){
        int v = 0;

        for(int i = 0; i <spinner.getCount(); i++){
            if(spinner.getItemAtPosition(i).toString().contentEquals(value)){
                return i;
            }
        }

        return v;
    }
    public String format(double d){

        return String.format("%.2f", d);
    }
    public String getDiscount(String s1, String s2){
        String result = "0";
        result = division(multiplication(s1,s2),"100");
        return result;
    }
    public String computesellingPrice(String s1, String s2){
        String result = "0";
        result = subtraction(s1,s2);
        return result;
    }

    public String rectifTotalVL(String s1, String s2, String s3, String s4, String s5, String s6, String s7, String s8, String s9){

        String result = "0";
        double r;
        Double d1, d2, d3,d4,d5,d6,d7,d8,d9;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);
        d3 = StringtoDouble(s3);
        d4 = StringtoDouble(s4);
        d5 = StringtoDouble(s5);
        d6 = StringtoDouble(s6);
        d7 = StringtoDouble(s7);
        d8 = StringtoDouble(s8);
        d9 = StringtoDouble(s9);;

        result = "" + (d1+d2+d3+d4+d5+d6+d7+d8+d9);

        r = Double.parseDouble(result);

        return String.format("%.2f", r);
    }
    public String rectifVL(String s1, String s2, String s3, String s4, String s5, String s6, String s7, String s8, String s9, String s10){

        String result = "0";
        double r;
        Double d1, d2, d3,d4,d5,d6,d7,d8,d9,d10;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);
        d3 = StringtoDouble(s3);
        d4 = StringtoDouble(s4);
        d5 = StringtoDouble(s5);
        d6 = StringtoDouble(s6);
        d7 = StringtoDouble(s7);
        d8 = StringtoDouble(s8);
        d9 = StringtoDouble(s9);
        d10 = StringtoDouble(s10);

        result = "" + (d1+d2+d3+d4+d5+d6+d7+d8+d9)*d10/100;

        r = Double.parseDouble(result);

        return String.format("%.2f", r);
    }
    public String rectifLI(String s1, String s2, String s3, String s4, String s5, String s6, String s7, String s8, String s9){

        String result = "0";
        double r;
        Double d1, d2, d3,d4,d5,d6,d7,d8,d9;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);
        d3 = StringtoDouble(s3);
        d4 = StringtoDouble(s4);
        d5 = StringtoDouble(s5);
        d6 = StringtoDouble(s6);
        d7 = StringtoDouble(s7);
        d8 = StringtoDouble(s8);
        d9 = StringtoDouble(s9);

        result = "" + (d1+d2+d3+d4+d5+d6+d7+d8)*d9/100;

        r = Double.parseDouble(result);

        return String.format("%.2f", r);
    }

    public String adjustedvalueVL(String s1, String s2){
        String result = "0";
        double r;
        Double d1;
        double d2;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);

        result = "" + (d1+d2);

        r = Double.parseDouble(result);

        return String.format("%.2f", r);
    }

    public String netAreaVL(String s1, String s2){
        String result = "0";
        double r;
        Double d1;
        double d2;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);

        result = "" + (d1-d2);

        r = Double.parseDouble(result);

        return String.format("%.2f", r);
    }

    public String totalLandVL(String s1, String s2){
        String result = "0";
        double r;
        Double d1;
        double d2;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);

        result = "" + (d1*d2);

        r = Double.parseDouble(result);

        return String.format("%.2f", r);
    }

    public String addTotalLI(String s1, String s2){
        String result = "0";
        double r;
        Double d1;
        double d2;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);

        result = "" + (d1+d2);

        r = Double.parseDouble(result);

        return String.format("%.2f", r);
    }

    public String remainEcoLI(String s1, String s2){
        String result = "0";
        int r;
        int d1;
        int d2;

        d1 = StringtoInt(s1);
        d2 = StringtoInt(s2);

        result = "" + (d1-d2);

        //r = Integer.parseInt(result);

        return result;
    }


    public String rectifTownhouse(String s1, String s2, String s3, String s4, String s5, String s6, String s7, String s8){

        String result = "0";
        double r;
        double d1, d2, d3,d4,d5,d6,d7,d8;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);
        d3 = StringtoDouble(s3);
        d4 = StringtoDouble(s4);
        d5 = StringtoDouble(s5);
        d6 = StringtoDouble(s6);
        d7 = StringtoDouble(s7);
        d8 = StringtoDouble(s8);


        result = "" + (d1+d2+d3+d4+d5+d6+d7)*d8/100;

        r = Double.parseDouble(result);

        return String.format("%.2f", r);
    }

    public String deducAmtVL(String s1, String s2){
        String result = "0";
        double r;
        double d1;
        double d2;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);

        result = "" + ((d1*d2)/100)*-1;

        r = Double.parseDouble(result);

        return String.format("%.2f", r);
    }
    public String netAppValVL(String s1, String s2){
        String result = "0";
        double r;
        double d1;
        double d2;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);

        result = "" + (d1+d2);

        r = Double.parseDouble(result);

        return String.format("%.2f", r);
    }

    public String rectifCondo(String s1, String s2, String s3, String s4, String s5, String s6, String s7, String s8, String s9){

        String result = "0";
        double r;
        double d1, d2, d3,d4,d5,d6,d7,d8,d9;

        d1 = StringtoDouble(s1);
        d2 = StringtoDouble(s2);
        d3 = StringtoDouble(s3);
        d4 = StringtoDouble(s4);
        d5 = StringtoDouble(s5);
        d6 = StringtoDouble(s6);
        d7 = StringtoDouble(s7);
        d8 = StringtoDouble(s8);
        d9 = StringtoDouble(s9);

        result = "" + (d1+d2+d3+d4+d5+d6+d7+d8)*d9/100;

        r = Double.parseDouble(result);

        return String.format("%.2f", r);
    }

    public String numberFormat(String s){
        String result;
        if(s.contentEquals("")||s.contentEquals("0")) {
            result="0.00";
        }else{
            NumberFormat numberFormat = new DecimalFormat("#,##0.00");

            result = numberFormat.format(StringtoDouble(s));
        }
        return result;
    }
    public String numberFormat(EditText et, TextWatcher tw, CharSequence s, String current){


        if(!s.toString().equals(current)) {

            et.removeTextChangedListener(tw);
            String cleanString = s.toString().replaceAll("[₱,.]", "");

            double parsed = StringtoDouble(cleanString);

            NumberFormat numberFormat = new DecimalFormat("#,##0.00");

            String formatted = numberFormat.format((parsed / 100));

            current = formatted;
            et.setText(formatted);
            et.setSelection(formatted.length());
            et.addTextChangedListener(tw);


        }

        return current;

    }

    public double summation(ArrayList<String> list) {
        double result = 0;

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).contentEquals("")) {

            } else if (list.get(i).contentEquals("No record found")) {

            } else {
                result = result + StringtoDouble(list.get(i));
            }
        }

        result = Double.parseDouble(String.format("%.2f", result));

        return result;
    }

    //EditText checker
    public void fill_in_error(EditText[] fields){
        for(int i=0; i<fields.length; i++){
            EditText currentField=fields[i];
            if(currentField.getText().toString().trim().contentEquals("")){
                currentField.setError("Fill in data");
            }

        }

    }
    //EditText checker
    public void fill_in_error_disabled(EditText[] fields){
        for(int i=0; i<fields.length; i++){
            EditText currentField=fields[i];
            if(currentField.getText().toString().trim().contentEquals("")
                    ||currentField.getText().toString().trim().contentEquals("0")
            ||currentField.getText().toString().trim().contentEquals("0.00")){
                currentField.setError("Fill in data");
            }

        }

    }
    public void fill_in_error_price(EditText[] fields){
        for(int i=0; i<fields.length; i++){
            EditText currentField=fields[i];
            if(currentField.getText().length() == 0){
                currentField.setError("Fill in data");
            }else if(currentField.getText().toString().contentEquals("0.00")){
                currentField.setError("Fill in data");
            }else if(currentField.getText().toString().contentEquals("0")){
                currentField.setError("Fill in data");
            }

        }

    }
    public void fill_in_error_spinner(Spinner[] fields){
        for(int i=0; i<fields.length; i++){
            Spinner currentField=fields[i];
            if(currentField.getSelectedItem().toString().length() == 0){
                //currentField.setBackgroundColor(Color.parseColor("#FF0000"));
                try{
                    ((TextView)currentField.getSelectedView()).setError("Fill in Data");
                }catch(NullPointerException e){
                    currentField.setBackgroundColor(Color.parseColor("#FF0000"));
                }

            }

        }

    }
    public void autocomplete(final AutoCompleteTextView ac, String [] options, Context context){

        final ArrayAdapter<String> optionsAdapter;

        optionsAdapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_list_item_1,
                options);

        ac.setAdapter(optionsAdapter);

        ac.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ac.showDropDown();
                return false;
            }
        });

    }

    public void multiautocomplete(final MultiAutoCompleteTextView ac, String [] options, Context context){

        final ArrayAdapter<String> optionsAdapter;

        optionsAdapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_list_item_1,
                options);

        ac.setAdapter(optionsAdapter);

        ac.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        /*ac.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent e) {
                // TODO Auto-generated method stub
                ac.showDropDown();
                return false;
            }
        });*/

        ac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ac.showDropDown();
            }
        });

    }


    public void reloadClass(Intent intent, Context context){
        Activity mContext = (Activity) context;
        Intent mIntent = intent;
        mContext.finish();
        mContext.startActivity(mIntent);
    }

    // function get json from url
    // by making HTTP POST or GET mehtod
    TLSConnection tlscon = new TLSConnection();
    public JSONObject makeHttpsRequest(String urlstr, String method, ArrayList<String> field, ArrayList<String> value) {


        InputStream is = null;
        JSONObject jObj = new JSONObject();
        String json = "";
        // Making HTTP request
        HttpsURLConnection urlConnection = tlscon.setUpHttpsConnection(urlstr);
        // check for request method
        if (method == "POST") {
            try {
                // URL url = new URL(urlstr);

                if (urlstr.matches("https(.*)")) {
                    Log.v("https", "https");
                } else if (urlstr.matches("http(.*)")) {
                    Log.v("http", "http");
                }

                urlConnection.setReadTimeout(100000);
                urlConnection.setConnectTimeout(0);
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(false);
                Uri.Builder builder = new Uri.Builder();
                Log.v("field and value", "" + field + " " + value);
                for (int x = 0; x < field.size(); x++) {
                    builder.appendQueryParameter(field.get(x), value.get(x));
                }


                String query = builder.build().getEncodedQuery();
                Log.v("builder", query);
                OutputStream os = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();

                urlConnection.connect();

                Log.e("response", ""+urlConnection.getResponseCode());
                if (urlConnection.getResponseCode() / 100 == 2) { // 2xx code means success
                    is = urlConnection.getInputStream();
                    Log.e("is", "inputstream");
                } else {
                    is = urlConnection.getErrorStream();
                    Log.e("is", "errorstream");

                }

                //is = urlConnection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuffer sb = new StringBuffer();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                Log.v("query", query);
                Log.v("writer", "" + writer);
                Log.v("jsonWhole", "" + sb.toString());
                json = sb.toString();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Log.e("", e.toString());
            } catch (Exception e) {
                e.printStackTrace();
            } catch (OutOfMemoryError e) {
                Log.e("Memory Error:", "Too Large.");
            }
        } else if (method == "GET") {
            // request method is GET
            try {
                urlConnection.setRequestMethod("GET");
                urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0");

                int responseCode = urlConnection.getResponseCode();
                Log.e("response", "" + responseCode);
                if (responseCode == HttpsURLConnection.HTTP_OK) { // success
                    BufferedReader in = new BufferedReader(new InputStreamReader(
                            urlConnection.getInputStream()));
                    String inputLine;
                    StringBuffer response = new StringBuffer();

                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();

                    // print result
                    json=response.toString();
                } else {
                    System.out.println("GET request not worked");
                }




                /*urlConnection.setReadTimeout(10000);
                urlConnection.setConnectTimeout(15000);
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(false);

                Uri.Builder builder = new Uri.Builder();
                Log.v("field and value", "" + field + " " + value);
                for (int x = 0; x < field.size(); x++) {
                    builder.appendQueryParameter(field.get(x), value.get(x));
                }

                String query = builder.build().getEncodedQuery();
                Log.v("builder", query);
                OutputStream os = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();

                urlConnection.connect();*/



                /*if (urlConnection.getResponseCode() / 100 == 2) { // 2xx code means success
                    is = urlConnection.getInputStream();
                } else {
                    is = urlConnection.getErrorStream();
                }

                //is = urlConnection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuffer sb = new StringBuffer();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

                Log.v("query", query);
                Log.v("writer", "" + writer);
                Log.v("jsonWhole", "" + sb.toString());*/

                /*json = sb.toString();*/

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Log.e("", e.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        // return JSON String
        return jObj;

    }
    public boolean pingAttachment(){
        int response = 0;
        boolean connection = false;
        try {

            Log.e("ping:", "start");
            //
            //wonderful_tls
            if(Global.type.contentEquals("tls")){
                HttpsURLConnection conn = tlscon.setUpHttpsConnection(Global.pdf_loc_url);
                conn.setRequestProperty("response_value", "default_response");
                response = conn.getResponseCode();
            }else{
                URL obj = new URL(Global.server_url);
                HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
                conn.setRequestProperty("response_value", "default_response");
                response = conn.getResponseCode();
            }

            Log.e("ping response:", ""+response);
            if(response == 200){
                //good connection
                connection = true;
                Log.e("ping", "good");
            }else{
                //bad connection
                connection = false;
                Log.e("ping", "bad");
            }

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return connection;

    }
    public boolean ping(){
        int response = 0;
        boolean connection = false;
        try {

            Log.e("ping:", "start");
            //
            //wonderful_tls
            if(Global.type.contentEquals("tls")){
                HttpsURLConnection conn = tlscon.setUpHttpsConnection(Global.server_url);
                conn.setRequestProperty("response_value", "default_response");
                response = conn.getResponseCode();
            }else{
                URL obj = new URL(Global.server_url);
                HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
                conn.setRequestProperty("response_value", "default_response");
                response = conn.getResponseCode();
            }

            Log.e("ping response:", ""+response);
            if(response == 200){
                //good connection
                connection = true;
                Log.e("ping", "good");
            }else{
                //bad connection
                connection = false;
                Log.e("ping", "bad");
            }

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return connection;

    }
    /* public String google_ping() {
         String response = "";
         boolean connection = false;
         try {

             Log.e("ping:", "start");
             //
             //wonderful_tls
             if (Global.type.contentEquals("tls")) {
                 HttpsURLConnection conn = tlscon.setUpHttpsConnection(Global.sign_in_url);
                 conn.setRequestProperty("response_value", "default_response");
                 response = String.valueOf(conn.getResponseCode());
             } else {
                 URL obj = new URL(Global.sign_in_url);
                 HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
                 conn.setRequestProperty("response_value", "default_response");
                 response = String.valueOf(conn.getResponseCode());
             }

             Log.e("ping response:", "" + response);
             *//*if (response == 200 || response == 301) {
                //good connection
                connection = true;
                Log.e("ping", "good");
            } else {
                //bad connection
                connection = false;
                Log.e("ping", "bad");
            }*//*

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return response;

    }*/
    /*public String google_ping() {
        String google_response="";
        try {
//			URL obj = new URL("https://www.google.com.ph/?gws_rd=cr&ei=td9kUrP0H8mjigenuoHAAg");
            URL obj = new URL(Global.sign_in_url);
            HttpsURLConnection con;
            HttpURLConnection con2;

            if(Global.type.contentEquals("tls")){
                con = tlscon.setUpHttpsConnection(""+obj);
                con.setRequestProperty("response_value", "default_response");
                google_response = String.valueOf(con.getResponseCode());
            }else{
                con2 = (HttpURLConnection) obj.openConnection();
                con2.setRequestMethod("GET");
                con2.setRequestProperty("User-Agent", "Mozilla/5.0");
                google_response = String.valueOf(con2.getResponseCode());

            }

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return google_response;
    }*/
   /* public String google_ping() {

        return "200";
    }*/

      public String google_ping(){
          int response = 0;
          String connection = "";
          try {

              Log.e("ping:", "start");
              //
              //wonderful_tls
              if(Global.type.contentEquals("tls")){
                  HttpsURLConnection conn = tlscon.setUpHttpsConnection(Global.sign_in_url);
                  conn.setRequestProperty("response_value", "default_response");
                  response = conn.getResponseCode();
              }else{
                URL obj = new URL(Global.sign_in_url);
                HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
                conn.setRequestProperty("response_value", "default_response");
                response = conn.getResponseCode();
            }

            Log.e("ping response:", ""+response);
            if(response == 200||response == 301){
                //good connection
                connection = "200";
                Log.e("ping", "good");
            }else{
                //bad connection
                connection = "404";
                Log.e("ping", "bad");
            }

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return connection;

    }
    public JSONObject makeHttpRequest(String url, String method, List<NameValuePair> params) {
        Log.e("makeHttpRequest", "started");
        // Making HTTP request
        try {

            // check for request method
            if(method == "POST"){
                Log.e("makeHttpRequest", "POST");
                // request method is POST
                // defaultHttpClient
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);
                httpPost.setEntity(new UrlEncodedFormEntity(params));

                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
            }else if(method == "GET"){
                Log.e("makeHttpRequest", "GET");
                // request method is GET
                DefaultHttpClient httpClient = new DefaultHttpClient();
                String paramString = URLEncodedUtils.format(params, "utf-8");
                url += "?" + paramString;
                HttpGet httpGet = new HttpGet(url);

                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
            }
            Log.e("makeHttpRequest", "ended");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
            Log.e("json", json);
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        // return JSON String
        return jObj;

    }

    public String http_posting(ArrayList<String> field,ArrayList<String> value, String network_url,Context context){
        DefaultHttpClient httpClient = new MyHttpClient(context);
        String xml="";


        try{
            HttpPost httpPost = new HttpPost(network_url);
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            Log.e("size",""+field.size());
            for(int x = 0;x<field.size();x++){
                Log.e("field",""+field.get(x));
                Log.e("value",""+value.get(x));
                params.add(new BasicNameValuePair(field.get(x), value.get(x)));
            }
            httpPost.setEntity(new UrlEncodedFormEntity(params));
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity httpEntity = response.getEntity();
            xml = EntityUtils.toString(httpEntity);
        }catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.e("", e.toString());
        } catch (Exception e) {
            Log.e("", e.toString());
        }

        return xml;
    }
    public void sessionTimeout(final Context context,final boolean inBackground){
        Log.e("Session Start","true");
        final Activity mContext = (Activity) context;

        final SharedPreferences appPrefs;
        final SharedPreferences.Editor appPrefsEditor;

        appPrefs = context.getSharedPreferences("preference", 0);
        appPrefsEditor = appPrefs.edit();

        new CountDownTimer( 0 *10 * 1000 , 1000 )
        {
            public void onTick(long millisUntilFinished) {
                appPrefsEditor.putString("error", "");
                appPrefsEditor.commit();
            }

            public void onFinish()
            {
                if (inBackground) {
                    // code to logout
                    appPrefsEditor.putString("password", "");
                    appPrefsEditor.putString("keep", "false");
                    appPrefsEditor.putString("error", "Session timed out.");
                    appPrefsEditor.commit();

                    mContext.finish();

                    /*builder.setMessage("Session timed out.")
                            .setTitle("Session");

                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User clicked OK button
                            context.startActivity(new Intent(context, LoginScreen.class));
                        }
                    });*/


                   /* AlertDialog dialog_box = builder.create();
                    dialog_box.show();
                    dialog_box.setCancelable(false);*/

                }
            }
        }.start();
    }


    /*
        01/12/2017
    */

    /**
     * converts 'null' values to blank ''
     */
    public String nullToBlank(String s){
        if(s.contentEquals("null") || s.isEmpty() || s.length() == 0 ){
            s = "";
        }
        return s;
    }

    /**
     * REWORK
     */

    //has checker
    public String getJsonString(JSONObject jsonObject, String fieldname){
        try {
            if(jsonObject.has(fieldname))
                return nullToBlank(jsonObject.getString(fieldname));
            else {
                Log.e("getJsonString", fieldname +" does not exist");
                return "";
            }
        } catch (JSONException e) {
            e.printStackTrace();
//            return e.toString();
            return "";
        }
    }

    public void reworkFreeFields(String xml, String[] fields, String record_id, String table, Context context){

        String[] sqlite = new String[fields.length - 3];
        ArrayList<String> returnArrayList = new ArrayList<String>();
        ArrayList<String> fieldsArrayList = new ArrayList<String>();
        DatabaseHandler_New db = new DatabaseHandler_New(context);

        Log.e("reworkFreeFields", table);

        try {


            JSONObject json = new JSONObject(xml);

            fieldsArrayList.clear();

            // i:0 = "id"
            // i:1 = "record_id"
            // i:2 = "appraisal_type"
            for (int i = 3, j = 0; i < fields.length; i++, j++) {

                if (json.has(fields[i])) {

                    Log.e("json has", fields[i]);

//                    sqlite[j] = json.getString(fields[i]);
                    sqlite[j] = getJsonString(json, fields[i]);

                    Log.e(fields[i], sqlite[j]);

                } else {

                    Log.e("json no", fields[i]);

                    sqlite[j] = "";

                }

                returnArrayList.add(sqlite[j]);

                fieldsArrayList.add(fields[i]);

            }


            db.updateRecord(returnArrayList, fieldsArrayList, "record_id = ?", new String[]{record_id}, table);


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.e("JSONException", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

    }

    public void reworkMultipleOccurence(String xml, String array, String[] fields, String record_id, String table, Context context){

        Log.e("reworkArray", array);

        DatabaseHandler_New db = new DatabaseHandler_New(context);
        Log.e("sizeOfFields", fields.length + "");
        String[] fields2 = new String[fields.length - 1];

        Log.e("reworkMultipleOccurence", table);

        //create array withoud id
        for(int i = 0; i < fields2.length; i++){

            fields2[i] = fields[i + 1];
            Log.e(fields2[i], fields[i + 1]);

        }

        try {

            JSONObject json = new JSONObject(xml);

            JSONArray jsonArray = json.getJSONArray(array);

            Log.e("sizeOfJsonArray", jsonArray.length() + "");

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonObject = jsonArray.getJSONObject(i);
                ArrayList<String> createArrayList = new ArrayList<String>();
                createArrayList.clear();
                createArrayList.add(record_id);

                for (int j = 1; j < fields2.length; j++) {

                    Log.e(""+j, fields2[j]);

                    if (jsonObject.has(fields2[j])) {

                        Log.e(fields2[j], getJsonString(jsonObject, fields2[j]));
                        createArrayList.add(getJsonString(jsonObject, fields2[j]));

                    } else {

                        createArrayList.add("");

                    }

                }

                db.addRecord(createArrayList, fields, table);

            }


        } catch (JSONException e) {
            Log.e("", e.toString());
        } catch (Exception e) {
            Log.e("", e.toString());
        }

    }

    public void reworkNestedMultipleOccurence(String xml, String motherArray, String motherTable, String array, String[] fields, String record_id, String table, Context context){

        Log.e("reworkArray", array);

        DatabaseHandler_New db = new DatabaseHandler_New(context);
        Log.e("sizeOfFields", fields.length + "");
        String[] fields2 = new String[fields.length - 1];

        Log.e("reworkNested", table);

        //create array withoud id
        for(int i = 0; i < fields2.length; i++){

            fields2[i] = fields[i + 1];
            Log.e(fields2[i], fields[i + 1]);

        }

        String where = "WHERE record_id = args";
        final String[] args = new String[1];
        args[0] = record_id;

        try {

            JSONObject json = new JSONObject(xml);
            JSONArray jsonArray = json.getJSONArray(motherArray);


            for (int i = 0; i < jsonArray.length(); i++) {

                Log.e("JsonArray", i + "");

                String id = db.getRecord("id", where, args, motherTable).get(i);

                JSONObject jsonObject = jsonArray.getJSONObject(i);
                JSONArray jsonArray2 = jsonObject.getJSONArray(array);
                Log.e("sizeOfJson2Array", jsonArray2.length() + "");

                for (int k = 0; k < jsonArray2.length(); k++) {

                    JSONObject jsonObject2 = jsonArray2.getJSONObject(k);
                    ArrayList<String> createArrayList = new ArrayList<String>();
                    createArrayList.clear();
                    createArrayList.add(record_id);
                    createArrayList.add(id);

                    for (int l = 2; l < fields2.length; l++) {

                        Log.e(""+l, fields2[l]);

                        if (jsonObject2.has(fields2[l])) {

                            Log.e(fields2[l], getJsonString(jsonObject2, fields2[l]));
                            createArrayList.add(getJsonString(jsonObject2, fields2[l]));

                        } else {

                            Log.e(fields2[l], "no record found");
                            createArrayList.add("");

                        }

                    }

                    db.addRecord(createArrayList, fields, table);

                }

            }


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            Log.e("", e.toString());
        } catch (Exception e) {
            Log.e("", e.toString());
        }

    }

    public void reworkComparatives(String xml, String array, String prefix, String required_suffix, String record_id, String motherTable, String[] fields, String table, Context context){

        Log.e("reworkArray", array);

        DatabaseHandler_New db = new DatabaseHandler_New(context);

        String where = "WHERE record_id = args";
        final String[] args = new String[1];
        args[0] = record_id;

        try {

            JSONObject json = new JSONObject(xml);
            JSONArray jsonArray = json.getJSONArray(array);

            Log.e("jsonArray.length()", "" + jsonArray.length());

            for (int i = 0; i < jsonArray.length(); i++) {

                Log.e("Option", i + "");

                String id = db.getRecord("id", where, args, motherTable).get(i);
                JSONObject jsonObject = jsonArray.getJSONObject(i);



                int comparatives_size = 0;
                for (int k = 1; k < 6; k++) {

                    String required_field = prefix + k + required_suffix;
                    if (!jsonObject.getString(required_field).contentEquals("")) {
                        comparatives_size++;
                    }

                }

                for(int k = 1; k <= comparatives_size; k++){

                    //create array withoud id
                    String[] fields2 = new String[fields.length - 1];
                    for(int j = 0; j < fields2.length; j++){

                        if(fields[j + 1].startsWith("_"))
                            fields2[j] = prefix + k + fields[j + 1];
                        else
                            fields2[j] = fields[j + 1];

                        Log.e(fields2[j], fields[j + 1]);

                    }

                    ArrayList<String> createArrayList = new ArrayList<>();
                    createArrayList.clear();
                    createArrayList.add(record_id);
                    createArrayList.add(id);

                    for (int l = 2; l < fields2.length; l++) {

                        Log.e(""+l, fields2[l]);

                        if (jsonObject.has(fields2[l])) {

                            Log.e(fields2[l], getJsonString(jsonObject, fields2[l]));
                            createArrayList.add(getJsonString(jsonObject, fields2[l]));

                        } else {

                            Log.e(fields2[l], "no record found");
                            createArrayList.add("");

                        }

                    }

                    db.addRecord(createArrayList, fields, table);

                }

            }


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            Log.e("", e.toString());
        } catch (Exception e) {
            Log.e("", e.toString());
        }

    }

    public void reworkNestedArrayMultipleOccurence(String xml, String motherArray, String array, String[] fields, String record_id, String table, Context context){

        Log.e("reworkNestedArray", array);

        DatabaseHandler_New db = new DatabaseHandler_New(context);
        Log.e("sizeOfFields", fields.length + "");
        String[] fields2 = new String[fields.length - 1];

        //create array withoud id
        for(int i = 0; i < fields2.length; i++){

            fields2[i] = fields[i + 1];
            Log.e(fields2[i], fields[i + 1]);

        }

        try {

            JSONObject json = new JSONObject(xml);
            JSONArray jsonArray = json.getJSONArray(motherArray);

            JSONObject jsonObject = jsonArray.getJSONObject(0);
            JSONArray jsonArray2 = jsonObject.getJSONArray(array);

            Log.e("sizeOfJsonArray2", "" + jsonArray2.length());

            for (int i = 0; i < jsonArray2.length(); i++) {

                JSONObject jsonObject2 = jsonArray2.getJSONObject(i);
                ArrayList<String> createArrayList = new ArrayList<String>();
                createArrayList.clear();
                createArrayList.add(record_id);

                for (int j = 1; j < fields2.length; j++) {

                    Log.e(""+j, fields2[j]);

                    if (jsonObject2.has(fields2[j])) {

                        Log.e(fields2[j], getJsonString(jsonObject2, fields2[j]));
                        createArrayList.add(getJsonString(jsonObject2, fields2[j]));

                    } else {

                        createArrayList.add("");

                    }

                }

                db.addRecord(createArrayList, fields, table);

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            Log.e("", e.toString());
        } catch (Exception e) {
            Log.e("", e.toString());
        }

    }
    public boolean checkMVRequest1(Context context,String record_id) {
        boolean isMandaOK=false;
        DatabaseHandler db = new DatabaseHandler(context);

        List<Report_Accepted_Jobs> report_accepted_jobs = db.getReport_Accepted_Jobs(record_id);
        if (!report_accepted_jobs.isEmpty()) {
            for (Report_Accepted_Jobs im : report_accepted_jobs) {

                isMandaOK= validate(new String []{im.getvehicle_type(),im.getcar_model(),im.getcar_mileage(),im.getcar_series(),im.getcar_color(),
                        im.getcar_plate_no(),im.getcar_body_type(),im.getcar_displacement(),im.getcar_motor_no(),im.getcar_chassis_no(),
                        im.getcar_no_cylinders()});
            }
        }
        return isMandaOK;
    }

    public boolean checkMVPage1(Context context,String record_id){
        boolean isMandaOK = false;
        DatabaseHandler db = new DatabaseHandler(context);
        DatabaseHandler2 db2 = new DatabaseHandler2(context);
        String where = "WHERE record_id = args";
        String[] args = new String[1];
        args[0] = record_id;
        List<Motor_Vehicle_API> mv = db.getMotor_Vehicle(record_id);
        if (!mv.isEmpty()) {
            for (Motor_Vehicle_API im : mv) {
                isMandaOK= validate(new String []{im.getreport_interior_dashboard(),im.getreport_interior_sidings(),im.getreport_interior_seats(),
                        im.getreport_interior_windows(),im.getreport_interior_ceiling(),im.getreport_interior_instrument(),im.getreport_bodytype_grille(),
                        im.getreport_bodytype_headlight(),im.getreport_bodytype_fbumper(),im.getreport_bodytype_hood(),im.getreport_bodytype_rf_fender(),
                        im.getreport_bodytype_rf_door(),im.getreport_bodytype_rr_door(),im.getreport_bodytype_rr_fender(),im.getreport_bodytype_backdoor(),
                        im.getreport_bodytype_taillight(),im.getreport_bodytype_r_bumper(),im.getreport_bodytype_lr_fender(),im.getreport_bodytype_lr_door(),
                        im.getreport_bodytype_lf_door(),im.getreport_bodytype_lf_fender(),im.getreport_bodytype_top(),im.getreport_bodytype_paint(),
                        im.getreport_bodytype_flooring(),db2.getRecord("valrep_mv_bodytype_trunk", where, args, "motor_vehicle").get(0),im.getreport_enginearea_fuel(),im.getreport_enginearea_chassis(),
                        im.getreport_enginearea_battery(),im.getreport_enginearea_electrical(),im.getreport_enginearea_engine(),
                        im.getreport_time_inspected(),im.getreport_date_inspected_day(),im.getreport_date_inspected_month(),im.getreport_date_inspected_year(),
                        im.getreport_misc_stereo(),im.getreport_misc_tools(),im.getreport_misc_speakers(),im.getreport_misc_airbag(),
                        im.getreport_misc_tires(),im.getreport_misc_mag_wheels(),im.getreport_misc_carpet(),im.getreport_misc_others(),
                        im.getreport_enginearea_transmission1()});
            }
        }
        return isMandaOK;
    }
    public boolean checkMVPage3TA(Context context,String record_id){
        boolean isMandaOK = false;
        DatabaseHandler db = new DatabaseHandler(context);

        List<Motor_Vehicle_API> mv = db.getMotor_Vehicle(record_id);
        if (!mv.isEmpty()) {
            for (Motor_Vehicle_API im : mv) {
                isMandaOK= validate(new String []{im.getreport_place_inspected(),im.getreport_market_valuation_remarks(),im.getvalrep_mv_market_valuation_previous_remarks()});
            }
        }
        return isMandaOK;
    }
    public boolean checkMVPage3(Context context,String record_id){
        boolean isMandaOK = false;
        DatabaseHandler db = new DatabaseHandler(context);

        List<Motor_Vehicle_API> mv = db.getMotor_Vehicle(record_id);
        if (!mv.isEmpty()) {
            for (Motor_Vehicle_API im : mv) {
                isMandaOK= validate(new String []{im.getreport_place_inspected(),im.getreport_market_valuation_remarks()});
            }
        }
        return isMandaOK;
    }

    public void deleteAllRecord(String record_id,DatabaseHandler2 db2){

        //Land Improvments
        db2.deleteRecord(Tables.land_improvements.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.land_improvements_imp_details.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.land_improvements_imp_details_features.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.land_improvements_imp_valuation.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.land_improvements_imp_valuation_details.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.land_improvements_lot_details.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.land_improvements_lot_valuation_details.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.land_improvements_prev_appraisal.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.land_improvements_main_prev_appraisal.table_name, "record_id = ?", new String[] { record_id });


        //Vacant Lot
        db2.deleteRecord(Tables.vacant_lot.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.vacant_lot_lot_details.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.vacant_lot_lot_valuation_details.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.vacant_lot_main_prev_appraisal.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.vacant_lot_prev_appraisal.table_name, "record_id = ?", new String[] { record_id });

        //Townhouse
        db2.deleteRecord(Tables.townhouse.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.townhouse_condo.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.townhouse_imp_details.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.townhouse_imp_details_features.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.townhouse_lot_details.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.townhouse_lot_valuation_details.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.townhouse_main_prev_appraisal.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.townhouse_prev_appraisal.table_name, "record_id = ?", new String[] { record_id });

        //WAF
        db2.deleteRecord(Tables.waf.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.waf_listing.table_name, "record_id = ?", new String[] { record_id });

        //SPV
        db2.deleteRecord(Tables.spv.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.valrep_landimp_imp_details.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.valrep_spv_land_imp_comps_table.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.valrep_landimp_imp_valuation.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.valrep_landimp_imp_valuation_details.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.valrep_spv_landimp_summary_of_value_table.table_name, "record_id = ?", new String[] { record_id });


        //GENERAL
        db2.deleteRecord(Tables.tbl_report_accepted_jobs.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord(Tables.tbl_report_accepted_jobs_contacts.table_name, "record_id = ?", new String[] { record_id });
        db2.deleteRecord("tbl_attachments", "record_id = ?", new String[] { record_id });
        db2.deleteRecord("report_filename", "record_id = ?", new String[] { record_id });
        db2.deleteRecord("submission_logs", "record_id = ?", new String[] { record_id });
    }
        public boolean uploadFile(String sourceFileUri){
            boolean response=false;
            int serverResponseCode;
            TLSConnection tlscon = new TLSConnection();
            String upLoadServerUri = Global.pdf_upload_url_new;
            String fileName = sourceFileUri;

            HttpsURLConnection conn = null;
            HttpURLConnection conn2=null;
            DataOutputStream dos = null;
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";
            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;
            File sourceFile = new File(sourceFileUri);
            if (!sourceFile.isFile()) {
                Log.e("uploadFile", "Source File Does not exist");
               // return 0;
            }
            try { // open a URL connection to the Servlet
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(upLoadServerUri);


                if(Global.type.contentEquals("tls")){

                    conn = tlscon.setUpHttpsConnection(""+url);

                    conn.setDoInput(true); // Allow Inputs
                    conn.setDoOutput(true); // Allow Outputs
                    conn.setUseCaches(false); // Don't use a Cached Copy
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Connection", "Keep-Alive");
                    conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                    conn.setRequestProperty("Content-Type",
                            "multipart/form-data;boundary=" + boundary);
                    conn.setRequestProperty("uploaded_file", fileName);
                    dos = new DataOutputStream(conn.getOutputStream());
                }else{

                    conn2 = (HttpURLConnection) url.openConnection();
                    conn2.setDoInput(true); // Allow Inputs
                    conn2.setDoOutput(true); // Allow Outputs
                    conn2.setUseCaches(false); // Don't use a Cached Copy
                    conn2.setRequestMethod("POST");
                    conn2.setRequestProperty("Connection", "Keep-Alive");
                    conn2.setRequestProperty("ENCTYPE", "multipart/form-data");
                    conn2.setRequestProperty("Content-Type",
                            "multipart/form-data;boundary=" + boundary);
                    conn2.setRequestProperty("uploaded_file", fileName);
                    dos = new DataOutputStream(conn2.getOutputStream());
                }



                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
                        + fileName + "\"" + lineEnd);
                dos.writeBytes(lineEnd);

                bytesAvailable = fileInputStream.available(); // create a buffer of
                // maximum size

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {
                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                String serverResponseMessage="";
                if(Global.type.contentEquals("tls")){
                    serverResponseCode = conn.getResponseCode();
                    serverResponseMessage = conn.getResponseMessage();
                }else{
                    serverResponseCode = conn2.getResponseCode();
                    serverResponseMessage = conn2.getResponseMessage();
                }

			/*serverResponseCode = conn.getResponseCode();
			String serverResponseMessage = conn.getResponseMessage();*/

                Log.i("uploadFile", "HTTP Response is : " + serverResponseMessage
                        + ": " + serverResponseCode);
              /*  if (serverResponseCode == 200) {
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                        }
                    });
                }*/

                // close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();

                //Added By Mark
                //get response of upload_php
                int file_success = -1;
                StringBuilder sb = new StringBuilder();
                InputStream in = new BufferedInputStream(conn.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                String upload_php_string_response = sb.toString();
                Log.e("upload php response",sb.toString());
                JSONObject upload_php_json = new JSONObject(upload_php_string_response);
                file_success = upload_php_json.getInt("upload_success");
                //get response of upload_php


                if(serverResponseCode == 200) {
                    if (file_success == 1) {
                        /*activity.runOnUiThread(new Runnable() {
                            public void run() {
                                Toast toast = Toast.makeText(activity.getBaseContext(), "File Upload Success", Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                                View view = toast.getView();
                                view.setBackgroundResource(R.color.toast_blue);
                                toast.show();
                            }
                        });*/
                        response=true;
                    } else {
                       /* runOnUiThread(new Runnable() {
                            public void run() {
                                Toast toast = Toast.makeText(getBaseContext(), "File Upload Failed", Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                                View view = toast.getView();
                                view.setBackgroundResource(R.color.toast_red);
                                toast.show();
                            }
                        });*/
                        response=false;

                    }
                }


            } catch (MalformedURLException ex) {
                ex.printStackTrace();
                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Upload file to server Exception",
                        "Exception : " + e.getMessage(), e);
                response=false;
            }
            return response;
        }

    public void get_json_data(JSONObject json_data, String valrep, JSONArray json_array, Context context) {

        try {
            json_data.put(valrep, json_array);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }
    public void get_json_data2(JSONObject json_data, String[] valrep, ArrayList<String> createArrayList, String where, String[] args, int position, String table, Context context) {

        DatabaseHandler2 db = new DatabaseHandler2(context);

        for (int i = 0; i < valrep.length; i++) {
            try {

                String currentData = db.getRecord(createArrayList.get(i), where, args, table).get(position);
                Log.e("currentData", currentData);

                if (!currentData.contentEquals("No record found"))
                    json_data.put(valrep[i], currentData);
                else
                    json_data.put(valrep[i], "");


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

}
