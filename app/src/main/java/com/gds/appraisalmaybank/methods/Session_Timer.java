package com.gds.appraisalmaybank.methods;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.gds.appraisalmaybank.requestjobs.Index_request_jobs_auth;

/**
 * Created by ianlo on 03/08/2016.
 */
public class Session_Timer extends Activity {

    //    public static final long DISCONNECT_TIMEOUT = 5000; // 5 min = 5 * 60 * 1000 ms
    public static final long DISCONNECT_TIMEOUT = 30 * 60 * 1000; // 5 min = 5 * 60 * 1000 ms

    /**
     * how to use

     //add this line inside onCreate
     st.resetDisconnectTimer();

     //then on main activity

     Session_Timer st = new Session_Timer(this);
     @Override
     public void onUserInteraction(){
     st.resetDisconnectTimer();
     }
     @Override
     public void onStop() {
     super.onStop();
     st.stopDisconnectTimer();
     }
     @Override
     public void onResume() {
     super.onResume();
     st.resetDisconnectTimer();
     }

     */

    Activity activity;

    Context context;

    public Session_Timer(Context context) {
        super.onUserInteraction();
        this.context = context;
        this.activity = (Activity) context;
        Log.i("Session_Timer", "constructed (called / opened)");
    }


    private Handler disconnectHandler = new Handler(){
        public void handleMessage(Message msg) {
        }
    };

    private Runnable disconnectCallback = new Runnable() {
        @Override
        public void run() {
            // Perform any required operation on disconnect

            Toast.makeText(context, "Session Expired", Toast.LENGTH_LONG).show();

            final SharedPreferences appPrefs;
            final SharedPreferences.Editor appPrefsEditor;

            appPrefs = context.getSharedPreferences("preference", 0);
            appPrefsEditor = appPrefs.edit();
            appPrefsEditor.putString("password", "");
            appPrefsEditor.putString("keep", "false");
            appPrefsEditor.putString("error", "Session timed out.");
            appPrefsEditor.commit();


            Intent i = new Intent(context, Index_request_jobs_auth.class);
            // set the new task and clear flags
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);//all class
            activity.startActivity(i);

            finish();
        }
    };

    public void resetDisconnectTimer(){
        Log.e("Session timeout","resetDisconnectTimer");
        disconnectHandler.removeCallbacks(disconnectCallback);
        disconnectHandler.postDelayed(disconnectCallback, DISCONNECT_TIMEOUT);
    }

    public void stopDisconnectTimer(){
        Log.e("Session timeout","stopDisconnectTimer");
        disconnectHandler.removeCallbacks(disconnectCallback);
    }

    @Override
    public void onUserInteraction(){
        resetDisconnectTimer();
    }

    @Override
    public void onResume() {
        super.onResume();
        resetDisconnectTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        stopDisconnectTimer();
    }
}