package com.gds.appraisalmaybank.waf;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.attachments.Attachments_Inflater;
import com.gds.appraisalmaybank.attachments.Collage;
import com.gds.appraisalmaybank.check_network.NetworkUtil;
import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Images;
import com.gds.appraisalmaybank.database.Report_Accepted_Jobs;
import com.gds.appraisalmaybank.database.Report_Accepted_Jobs_Contacts;
import com.gds.appraisalmaybank.database.Report_filename;
import com.gds.appraisalmaybank.database.Required_Attachments;
import com.gds.appraisalmaybank.database.TableObjects;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database_new.Tables;
import com.gds.appraisalmaybank.json.TLSConnection;
import com.gds.appraisalmaybank.json.UserFunctions;
import com.gds.appraisalmaybank.main.Connectivity;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;
import com.gds.appraisalmaybank.report.View_Report;
import com.gds.appraisalmaybank.required_fields.Fields_Checker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by ianlozares on 16/10/2017.
 */

public class Waf extends Activity  implements View.OnClickListener {

    GDS_methods gds = new GDS_methods();
    TableObjects to = new TableObjects();
    Session_Timer st = new Session_Timer(this);
    ActionBar actionBar;
    private static final String TAG_UID = "uid";
    private static final String TAG_RECORD_ID = "record_id";
    private static final String TAG_PASS_STAT = "pass_stat";
    String pass_stat = "offline";

    DatabaseHandler db = new DatabaseHandler(this);
    DatabaseHandler2 db2 = new DatabaseHandler2(this);
    Global gs;
    TLSConnection tlscon = new TLSConnection();
    ArrayList<String> field = new ArrayList<String>();
    ArrayList<String> value = new ArrayList<String>();
    // for attachments
    int responseCode;
    //for fileUpload checking
    boolean fileUploaded = false;
    String xml;
    String google_response = "";
    private ListView dlg_priority_lvw = null;
    Context context = this;
    String item, filename;
    String dir = "gds_appraisal";
    File myDirectory = new File(Environment.getExternalStorageDirectory() + "/"
            + dir + "/");

    //detect internet connection
    TextView tv_connection;
    boolean wifi = false;
    boolean data = false;
    boolean data_speed = false;

    private NetworkChangeReceiver receiver;
    private boolean isConnected = false;
    boolean globalSave = false;
    UserFunctions userFunction = new UserFunctions();
    String where = "WHERE record_id = args";
    String[] args = new String[1];


    String attachment_selected, uid;
    String db_app_uid, db_app_file, db_app_filename, db_app_appraisal_type,
            db_app_candidate_done;


    TextView tv_requested_month, tv_requested_day, tv_requested_year,
            tv_classification, tv_account_name,tv_company_name, tv_requesting_party, tv_requestor,
            tv_control_no, tv_appraisal_type, tv_nature_appraisal,
            tv_ins_month1, tv_ins_day1, tv_ins_year1, tv_ins_month2,
            tv_ins_day2, tv_ins_year2, tv_com_month, tv_com_day, tv_com_year,
            tv_tct_no, tv_unit_no, tv_bldg_name, tv_street_no, tv_street_name,
            tv_village, tv_district, tv_zip_code, tv_city, tv_province,
            tv_region, tv_country, tv_address,
            tv_attachment, tv_app_request_remarks, tv_app_kind_of_appraisal, tv_app_branch_code;
    LinearLayout ll_contact;
    EditText et_first_name, et_middle_name, et_last_name, et_requestor,app_account_company_name;
    CheckBox app_account_is_company;
    Spinner spinner_purpose_appraisal;
    EditText et_app_kind_of_appraisal;
    AutoCompleteTextView et_requesting_party;
    EditText report_date_requested;
    Button btn_update_app_details, btn_save_app_details, btn_cancel_app_details;

    String requested_month, requested_day, requested_year, classification,
            account_fname, account_mname, account_lname, requesting_party, requestor,
            control_no, appraisal_type, nature_appraisal, ins_month1, ins_day1,
            ins_year1, ins_month2, ins_day2, ins_year2, com_month, com_day,
            com_year, tct_no, unit_no, bldg_name, street_no, street_name,
            village, district, zip_code, city, province, region, country,
            counter, output_appraisal_type, output_street_name, output_village,
            output_city, output_province, app_request_remarks, app_branch_code, app_kind_of_appraisal,account_is_company,account_company_name;
    String lot_no, block_no;
    String record_id, pdf_name,rework_reason = "";

    ImageView btn_inspection_dealer,btn_listing,btn_summary;

    Button btn_report_update_cc, btn_report_view_report,btn_manual_delete,btn_update_address, btn_save_address;
    ProgressDialog pDialog;
    Dialog myDialog;
    ImageView btn_select_pdf, btn_report_pdf,btn_mysql_attachments;

    TextView tv_address_hide;
    // uploaded attachments
    LinearLayout container_attachments;
    String uploaded_attachment;
    String url_webby;
    public static final int progress_bar_type = 0;

    boolean network_status,appStatReviewer = false;
    ;
    File uploaded_file;
    String[] commandArray = new String[]{"View Attachment in Browser",
            "Download Attachment"};

    boolean withAttachment = false, freshData = true;
    TextView tv_rework_reason_header, tv_rework_reason;

    //address
    EditText et_unit_no, et_building_name, et_lot_no, et_block_no, et_street_no, et_street_name,
            et_village, et_district, et_zip_code, et_city, et_province,
            et_region, et_country;

//Inspection adn Dealer page
    DatePicker valrep_waf_date_inspected;
    EditText valrep_waf_time_inspected,valrep_waf_dealer;
    Button btn_save_inspection_dealer;
    String date_month,date_day,date_year;

    //Summary
    EditText  valrep_waf_no_of_units_subject_for_inspection, valrep_waf_units_sighted, valrep_waf_units_unsighted,
            valrep_waf_units_unsold, valrep_waf_remarks;
    Button btn_save_summary;


    public class NetworkChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            isNetworkAvailable(context);
        }

        private boolean isNetworkAvailable(final Context context) {
            ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null) {
                    for (int i = 0; i < info.length; i++) {
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            if (!isConnected) {
                                tv_connection.setText("Connected");
                                tv_connection.setBackgroundColor(Color.parseColor("#3399ff"));
                                isConnected = true;
                                // do your processing here ---
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        // Do something after 1s = 1000ms
                                        wifi = Connectivity.isConnectedWifi(context);
                                        data = Connectivity.isConnectedMobile(context);
                                        data_speed = Connectivity.isConnected(context);
                                        if (wifi) {
                                            tv_connection.setText(R.string.wifi_connected);
                                            tv_connection.setBackgroundColor(Color.parseColor("#39b476"));
                                        } else if (data) {
                                            if (data_speed) {
                                                tv_connection.setText(R.string.data_good);
                                                tv_connection.setBackgroundColor(Color.parseColor("#39b476"));
                                            } else if (data_speed == false) {
                                                tv_connection.setText(R.string.data_weak);
                                                tv_connection.setBackgroundColor(Color.parseColor("#FF9900"));
                                            }
                                        }
                                    }
                                }, 1000);

                            }
                            return true;
                        }
                    }
                }
            }
            tv_connection.setText("No Internet connection");
            tv_connection.setBackgroundColor(Color.parseColor("#CC0000"));
            isConnected = false;
            return false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.waf);
        st.resetDisconnectTimer();
        Authenticator.setDefault(new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("gdsuser", "gdsuser01".toCharArray());
            }
        });
        actionBar = getActionBar();
        actionBar.setIcon(R.drawable.ic_waf);
        actionBar.setSubtitle("WAF");
        String open = "0";
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            open = bundle.getString("keyopen");
        }
        tv_connection = (TextView) findViewById(R.id.tv_connection);
        tv_connection.setVisibility(View.GONE);
		/*IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
		receiver = new NetworkChangeReceiver();
		registerReceiver(receiver, filter);*/

        gs = ((Global) getApplicationContext());
        container_attachments = (LinearLayout) findViewById(R.id.container_attachments);
        ll_contact = (LinearLayout) findViewById(R.id.container);
        tv_requested_month = (TextView) findViewById(R.id.tv_requested_month);
        tv_requested_day = (TextView) findViewById(R.id.tv_requested_day);
        tv_requested_year = (TextView) findViewById(R.id.tv_requested_year);
        tv_classification = (TextView) findViewById(R.id.tv_classification);
        tv_account_name = (TextView) findViewById(R.id.tv_account_name);
        tv_company_name = (TextView) findViewById(R.id.tv_company_name);
        tv_requesting_party = (TextView) findViewById(R.id.tv_requesting_party);
        tv_requestor = (TextView) findViewById(R.id.tv_requestor);
        tv_control_no = (TextView) findViewById(R.id.tv_control_no);
        tv_appraisal_type = (TextView) findViewById(R.id.tv_appraisal_type);
        tv_nature_appraisal = (TextView) findViewById(R.id.tv_nature_appraisal);
        tv_ins_month1 = (TextView) findViewById(R.id.tv_ins_month1);
        tv_ins_day1 = (TextView) findViewById(R.id.tv_ins_day1);
        tv_ins_year1 = (TextView) findViewById(R.id.tv_ins_year1);
        tv_ins_month2 = (TextView) findViewById(R.id.tv_ins_month2);
        tv_ins_day2 = (TextView) findViewById(R.id.tv_ins_day2);
        tv_ins_year2 = (TextView) findViewById(R.id.tv_ins_year2);
        tv_com_month = (TextView) findViewById(R.id.tv_com_month);
        tv_com_day = (TextView) findViewById(R.id.tv_com_day);
        tv_com_year = (TextView) findViewById(R.id.tv_com_year);
        tv_tct_no = (TextView) findViewById(R.id.tv_tct_no);
        tv_unit_no = (TextView) findViewById(R.id.tv_unit_no);
        tv_bldg_name = (TextView) findViewById(R.id.tv_bldg_name);
        tv_street_no = (TextView) findViewById(R.id.tv_street_no);
        tv_street_name = (TextView) findViewById(R.id.tv_street_name);
        tv_village = (TextView) findViewById(R.id.tv_village);
        tv_district = (TextView) findViewById(R.id.tv_district);
        tv_zip_code = (TextView) findViewById(R.id.tv_zip_code);
        tv_city = (TextView) findViewById(R.id.tv_city);
        tv_province = (TextView) findViewById(R.id.tv_province);
        tv_region = (TextView) findViewById(R.id.tv_region);
        tv_country = (TextView) findViewById(R.id.tv_country);
        tv_address = (TextView) findViewById(R.id.tv_address);
        tv_attachment = (TextView) findViewById(R.id.tv_attachment);

        btn_inspection_dealer = (ImageView) findViewById(R.id.btn_inspection_dealer);
        btn_listing = (ImageView) findViewById(R.id.btn_listing);
        btn_summary= (ImageView) findViewById(R.id.btn_summary);

        btn_report_update_cc = (Button) findViewById(R.id.btn_report_update_cc);
        btn_report_view_report = (Button) findViewById(R.id.btn_report_view_report);
        btn_report_pdf = (ImageView) findViewById(R.id.btn_report_pdf);
        btn_select_pdf = (ImageView) findViewById(R.id.btn_select_pdf);
//app details
        btn_update_app_details = (Button) findViewById(R.id.btn_update_app_details);
        btn_update_app_details.setOnClickListener(this);
        tv_address_hide = (TextView) findViewById(R.id.tv_address_hide);
        tv_app_request_remarks = (TextView) findViewById(R.id.tv_app_request_remarks);
        tv_app_kind_of_appraisal = (TextView) findViewById(R.id.tv_app_kind_of_appraisal);
        tv_app_branch_code = (TextView) findViewById(R.id.tv_app_branch_code);


        btn_inspection_dealer.setOnClickListener(this);
        btn_listing.setOnClickListener(this);
        btn_summary.setOnClickListener(this);

        btn_report_update_cc.setOnClickListener(this);
        btn_report_view_report.setOnClickListener(this);
        btn_report_pdf.setOnClickListener(this);
        btn_select_pdf.setOnClickListener(this);
        tv_attachment.setOnClickListener(this);


        //case center attachments
        btn_mysql_attachments = (ImageView) findViewById(R.id.btn_mysql_attachments);
        btn_mysql_attachments.setOnClickListener(this);

        //address
        btn_update_address = (Button) findViewById(R.id.btn_update_address);
        btn_update_address.setOnClickListener(this);



        tv_rework_reason_header = (TextView) findViewById(R.id.tv_rework_reason_header);
        tv_rework_reason = (TextView) findViewById(R.id.tv_rework_reason);

        btn_manual_delete = (Button) findViewById(R.id.btn_manual_delete);
        btn_manual_delete.setOnClickListener(this);

        record_id = gs.record_id;
        args[0] = record_id;
        uid = db2.getRecord("app_main_id", where, args, "tbl_report_accepted_jobs").get(0);
        fill_details();
        uploaded_attachments();



       }

    public void fill_details() {

        List<Report_Accepted_Jobs> report_accepted_jobs = db
                .getReport_Accepted_Jobs(record_id);
        if (!report_accepted_jobs.isEmpty()) {
            for (Report_Accepted_Jobs im : report_accepted_jobs) {
                requested_month = im.getdr_month();
                requested_day = im.getdr_day();
                requested_year = im.getdr_year();
                classification = im.getclassification();
                account_fname = im.getfname();
                account_mname = im.getmname();
                account_lname = im.getlname();
                requesting_party = im.getrequesting_party();
                requestor = im.getrequestor();
                control_no = im.getcontrol_no();
                appraisal_type = im.getappraisal_type();
                nature_appraisal = im.getnature_appraisal();
                ins_month1 = im.getins_date1_month();
                ins_day1 = im.getins_date1_day();
                ins_year1 = im.getins_date1_year();
                ins_month2 = im.getins_date2_month();
                ins_day2 = im.getins_date2_day();
                ins_year2 = im.getins_date2_year();
                com_month = im.getcom_month();
                com_day = im.getcom_day();
                com_year = im.getcom_year();
                tct_no = im.gettct_no();
                unit_no = im.getunit_no();
                bldg_name = im.getbuilding_name();
                lot_no = im.getlot_no();
                block_no = im.getblock_no();
                street_no = im.getstreet_no();
                street_name = im.getstreet_name();
                village = im.getvillage();
                district = im.getdistrict();
                zip_code = im.getzip_code();
                city = im.getcity();
                province = im.getprovince();
                region = im.getregion();
                country = im.getcountry();
                counter = im.getcounter();
                app_request_remarks = im.getapp_request_remarks();
                app_kind_of_appraisal = im.getkind_of_appraisal();
                app_branch_code = im.getapp_branch_code();
                rework_reason = im.getrework_reason();
                args[0]=record_id;
                account_is_company=db2.getRecord("app_account_is_company",where,args,"tbl_report_accepted_jobs").get(0);
                account_company_name=db2.getRecord("app_account_company_name",where,args,"tbl_report_accepted_jobs").get(0);
            }

            args[0]=record_id;
            if(db2.getRecord("application_status",where,args,"tbl_report_accepted_jobs").get(0).contentEquals("for_rework")){
                tv_rework_reason_header.setVisibility(View.VISIBLE);
                tv_rework_reason.setVisibility(View.VISIBLE);
                tv_rework_reason.setText(rework_reason);
            }
            List<Report_Accepted_Jobs_Contacts> report_Accepted_Jobs_Contacts = db
                    .getReport_Accepted_Jobs_Contacts(record_id);
            if (!report_Accepted_Jobs_Contacts.isEmpty()) {
                for (Report_Accepted_Jobs_Contacts im : report_Accepted_Jobs_Contacts) {
                    LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
                            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View addView = layoutInflater.inflate(
                            R.layout.request_jobs_info_contact_layout, null);
                    final TextView tv_contact_person = (TextView) addView
                            .findViewById(R.id.tv_contact_person);
                    final TextView tv_contact_prefix = (TextView) addView
                            .findViewById(R.id.tv_contact_prefix);
                    final TextView tv_contact_mobile = (TextView) addView
                            .findViewById(R.id.tv_contact_mobile);
                    final TextView tv_contact_landline = (TextView) addView
                            .findViewById(R.id.tv_contact_landline);

                    tv_contact_person.setText(im.getcontact_person());
                    tv_contact_prefix.setText(im.getcontact_mobile_no_prefix());
                    tv_contact_mobile.setText(im.getcontact_mobile_no());
                    tv_contact_landline.setText(im.getcontact_landline());
                    ll_contact.addView(addView);
                }
            }
            List<Report_filename> rf = db.getReport_filename(record_id);
            if (!rf.isEmpty()) {
                for (Report_filename im : rf) {
                    tv_attachment.setText(im.getfilename());
                }
            }
            tv_requested_month.setText(requested_month + "/");
            tv_requested_day.setText(requested_day + "/");
            tv_requested_year.setText(requested_year);
            tv_classification.setText(classification);
            tv_account_name.setText(account_fname + " " + account_mname + " "
                    + account_lname);
            tv_company_name.setText(account_company_name);
            if(account_is_company.contentEquals("true")){
                actionBar.setTitle(account_company_name);
            }else{
                actionBar.setTitle(account_fname + " " + account_mname + " " + account_lname);
            }
            tv_requesting_party.setText(requesting_party);
            tv_requestor.setText(requestor);
            tv_control_no.setText(control_no);
            for (int x = 0; x < gs.appraisal_type_value.length; x++) {
                if (appraisal_type.equals(gs.appraisal_type_value[x])) {
                    output_appraisal_type = gs.appraisal_output[x];
                }
            }
            tv_appraisal_type.setText(output_appraisal_type);
            tv_nature_appraisal.setText(nature_appraisal);
            tv_ins_month1.setText(ins_month1 + "/");
            tv_ins_day1.setText(ins_day1 + "/");
            tv_ins_year1.setText(ins_year1);
            tv_ins_month2.setText(ins_month2 + "/");
            tv_ins_day2.setText(ins_day2 + "/");
            tv_ins_year2.setText(ins_year2);
            tv_com_month.setText(com_month + "/");
            tv_com_day.setText(com_day + "/");
            tv_com_year.setText(com_year);

            tv_tct_no.setText(tct_no);
            tv_unit_no.setText(unit_no);
            tv_bldg_name.setText(bldg_name);
            tv_street_no.setText(street_no);
            tv_street_name.setText(street_name);
            tv_village.setText(village);
            tv_district.setText(district);
            tv_zip_code.setText(zip_code);
            tv_city.setText(city);
            tv_province.setText(province);
            tv_region.setText(region);
            tv_country.setText(country);
            tv_app_request_remarks.setText(app_request_remarks);
            tv_app_kind_of_appraisal.setText(app_kind_of_appraisal);
            tv_app_branch_code.setText(app_branch_code);
            tv_address_hide.setText(street_name + " " + city + " " + region + " " + country);
            if (!street_name.equals("")) {
                output_street_name = street_name + ", ";
            } else {
                output_street_name = street_name;
            }
            if (!village.equals("")) {
                output_village = village + ", ";
            } else {
                output_village = village;
            }
            if (!city.equals("")) {
                output_city = city + ", ";
            } else {
                output_city = city;
            }
            if (!province.equals("")) {
                output_province = province + ", ";
            } else {
                output_province = province;
            }
            tv_address.setText(unit_no + " " + bldg_name + " " + lot_no + " " + block_no
                    + " " + output_street_name + output_village + district
                    + " " + output_city + region + " " + output_province
                    + country + " " + zip_code);
        }
    }
    public void uploaded_attachments() {
        // set value
        List<Required_Attachments> required_attachments = db
                .getAllRequired_Attachments_byid(String.valueOf(appraisal_type));
        if (!required_attachments.isEmpty()) {
            for (final Required_Attachments ra : required_attachments) {
                LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                final View addView = layoutInflater.inflate(
                        R.layout.report_uploaded_attachments_dynamic, null);
                final TextView tv_attachment = (TextView) addView
                        .findViewById(R.id.tv_attachment);
                final ImageView btn_view = (ImageView) addView
                        .findViewById(R.id.btn_view);
                tv_attachment.setText(ra.getattachment());
                btn_view.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        uploaded_attachment = uid + "_"
                                + ra.getattachment() + "_" + counter + ".pdf";
                        // directory
                        uploaded_file = new File(myDirectory,
                                uploaded_attachment);
                        uploaded_attachment = uploaded_attachment.replaceAll(
                                " ", "%20");

                        url_webby = gs.pdf_loc_url + uploaded_attachment;
                        view_pdf(uploaded_file);
                    }
                });
                container_attachments.addView(addView);

            }

        }

    }
    public void view_pdf(File file) {
        if (file.exists()) {
            Uri path = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(getApplicationContext(),
                        "No Application Available to View PDF",
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            NetworkUtil.getConnectivityStatusString(this);
            if (!NetworkUtil.status.equals("Network not available")) {
                new Attachment_validation().execute();
            } else {
                Toast.makeText(getApplicationContext(),
                        "Network not available", Toast.LENGTH_SHORT).show();
            }

        }
    }
    private class Attachment_validation extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pDialog = new ProgressDialog(Waf.this);
            pDialog.setMessage("Checking attachment..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            NetworkUtil.getConnectivityStatusString(Waf.this);
            if (!NetworkUtil.status.equals("Network not available")) {
                google_response = gds.google_ping();
                if (google_response.equals("200")) {
                    try {
                        URL obj = new URL(url_webby);
                        HttpsURLConnection con;
                        HttpURLConnection con2;
                        if (Global.type.contentEquals("tls")) {
                            con = tlscon.setUpHttpsConnection("" + obj);
                            con.setRequestMethod("GET");
                            con.setRequestProperty("User-Agent", "Mozilla/5.0");
                            responseCode = con.getResponseCode();
                        } else {
                            con2 = (HttpURLConnection) obj.openConnection();
                            con2.setRequestMethod("GET");
                            con2.setRequestProperty("User-Agent", "Mozilla/5.0");
                            responseCode = con2.getResponseCode();
                        }
                        network_status = true;
                        Log.e("", String.valueOf(responseCode));
                    } catch (MalformedURLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (ProtocolException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            } else {
                network_status = false;

            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            Log.e("", url_webby);
            if (network_status == false) {
                Toast.makeText(getApplicationContext(), "No network Available",
                        Toast.LENGTH_SHORT).show();
            } else if (network_status == true) {
                if (responseCode == 200) {
                    view_download();
                } else if (responseCode == 404) {
                    Toast.makeText(getApplicationContext(),
                            "Attachment doesn't exist", Toast.LENGTH_SHORT)
                            .show();
                }
            }
            pDialog.dismiss();

        }
    }
    private void view_download() {
        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Action");
        builder.setItems(commandArray, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int index) {
                if (index == 0) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
                            .parse(url_webby));

                    startActivity(browserIntent);
                    dialog.dismiss();
                } else if (index == 1) {
                    new DownloadFileFromURL().execute(url_webby);
                    dialog.dismiss();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
    /**
     * Background Async Task to download file
     */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                // Output stream to write file
                OutputStream output = new FileOutputStream(
                        uploaded_file.toString());

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            removeDialog(progress_bar_type);
            Toast.makeText(getApplicationContext(), "Attachment Downloaded",
                    Toast.LENGTH_LONG).show();
            view_pdf(uploaded_file);

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // TODO Auto-generated method stub
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        menu.findItem(R.id.map).setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {

            case R.id.map:
                view_map();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void view_map() {
        String add = tv_address_hide.getText().toString();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Waf.this);
        alertDialog.setTitle("Address");
        alertDialog.setMessage("Edit Address");

        final EditText input = new EditText(Waf.this);
        input.setText(add);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);
        alertDialog.setIcon(R.drawable.map_icon);

        alertDialog.setPositiveButton("Search",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String address = input.getText().toString();
                        Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + address);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        startActivity(mapIntent);

                    }
                });

        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }
    public void warning_dialog() {
        final Dialog dialog2 = new Dialog(context);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.setContentView(R.layout.warning_dialog);

        // set the custom dialog components - text, image and button
        TextView text2 = (TextView) dialog2.findViewById(R.id.tv_question);
        text2.setText("Please attach a file/pdf before you can submit the report");
        Button btn_ok = (Button) dialog2.findViewById(R.id.btn_ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });

        dialog2.show();
    }
    public void no_attachment() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.yes_no_dialog);

        Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btn_no = (Button) dialog.findViewById(R.id.btn_no);
        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_question);
        dialog.show();

        text.setText("Are you sure you want to submit ?");

        // if button is clicked, close the custom dialog
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                withAttachment = false;
              //new SendData().execute();
                //todo_new
                if (new Fields_Checker().isModuleComplete(record_id, context)) {
                    new SendData().execute();
//                    Toast.makeText(getApplicationContext(), "Module fields complete", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Module fields incomplete", Toast.LENGTH_LONG).show();
                }
                dialog.dismiss();
            }
        });
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
    public void check_attachment() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.yes_no_dialog);

        Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btn_no = (Button) dialog.findViewById(R.id.btn_no);
        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_question);
        text.setText("Are you sure you want to submit ?");
        //aug 17
        args[0] = record_id;
        if(db2.getRecord("application_status",where,args,"tbl_report_accepted_jobs").get(0).contentEquals("for_rework")){
            freshData = false;
        }else{
            freshData = true;
        }

        //check if with or without attachment
        if (((!tv_attachment.getText().toString().equals("")) && (freshData == false)) ||
                ((!tv_attachment.getText().toString().equals("")) && (freshData == true))) {
            dialog.show();
            withAttachment = true;
        } else if ((tv_attachment.getText().toString().equals("")) && (freshData == true)) {
            text.setText("Do you want to submit report without file/pdf attachment?");
            withAttachment = false;
            fileUploaded=true;
            dialog.dismiss();
            warning_dialog();
        } else if ((tv_attachment.getText().toString().equals("")) && (freshData == false)) {
            text.setText("Do you want to submit the report?");
            withAttachment = false;
            fileUploaded=true;
            dialog.show();
        }

        // if button is clicked, close the custom dialog
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((withAttachment == true) && (freshData == true)) ||
                        ((withAttachment == true) && (freshData == false)) ||
                        ((withAttachment == false) && (freshData == false))) {
                 //new SendData().execute();
                    //todo_new
                    if (new Fields_Checker().isModuleComplete(record_id, context)) {
                        new SendData().execute();
//                        Toast.makeText(getApplicationContext(), "Module fields complete", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Module fields incomplete", Toast.LENGTH_LONG).show();
                    }
                    dialog.dismiss();
                } else if ((withAttachment == false) && (freshData == true)) {
                    no_attachment();
                    dialog.dismiss();
                }
            }
        });
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
    public void custom_dialog_address() {
        myDialog = new Dialog(Waf.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.address_dialog);
        myDialog.setCancelable(true);

        android.view.WindowManager.LayoutParams params = myDialog.getWindow()
                .getAttributes();
        params.height = LayoutParams.WRAP_CONTENT;
        params.width = LayoutParams.FILL_PARENT;
        myDialog.getWindow().setAttributes(
                (android.view.WindowManager.LayoutParams) params);
        et_unit_no = (EditText) myDialog.findViewById(R.id.et_unit_no);
        et_building_name = (EditText) myDialog.findViewById(R.id.et_building_name);
        et_lot_no = (EditText) myDialog.findViewById(R.id.et_lot_no);
        et_block_no = (EditText) myDialog.findViewById(R.id.et_block_no);
        et_street_no = (EditText) myDialog.findViewById(R.id.et_street_no);
        et_street_name = (EditText) myDialog.findViewById(R.id.et_street_name);
        et_village = (EditText) myDialog.findViewById(R.id.et_village);
        et_district = (EditText) myDialog.findViewById(R.id.et_district);
        et_zip_code = (EditText) myDialog.findViewById(R.id.et_zip_code);
        et_city = (EditText) myDialog.findViewById(R.id.et_city);
        et_province = (EditText) myDialog.findViewById(R.id.et_province);
        et_region = (EditText) myDialog.findViewById(R.id.et_region);
        et_country = (EditText) myDialog.findViewById(R.id.et_country);

        btn_save_address = (Button) myDialog.findViewById(R.id.btn_save_address);
        btn_save_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method
                Report_Accepted_Jobs li = new Report_Accepted_Jobs();
                li.setunit_no(et_unit_no.getText().toString());
                li.setbuilding_name(et_building_name.getText().toString());
                li.setlot_no(et_lot_no.getText().toString());
                li.setblock_no(et_block_no.getText().toString());
                li.setstreet_no(et_street_no.getText().toString());
                li.setstreet_name(et_street_name.getText().toString());
                li.setvillage(et_village.getText().toString());
                li.setdistrict(et_district.getText().toString());
                li.setzip_code(et_zip_code.getText().toString());
                li.setcity(et_city.getText().toString());
                li.setprovince(et_province.getText().toString());
                li.setregion(et_region.getText().toString());
                li.setcountry(et_country.getText().toString());

                db.updateCollateral_Address(li, record_id);
                db.close();
                Toast.makeText(getApplicationContext(), "Saved",
                        Toast.LENGTH_SHORT).show();
                myDialog.dismiss();
                Intent intent = getIntent();
                intent.putExtra("keyopen", "0");
                finish();
                startActivity(intent);
            }
        });

        List<Report_Accepted_Jobs> li = db.getReport_Accepted_Jobs(String.valueOf(record_id));
        if (!li.isEmpty()) {
            for (Report_Accepted_Jobs im : li) {
                et_unit_no.setText(im.getunit_no());
                et_building_name.setText(im.getbuilding_name());
                et_lot_no.setText(im.getlot_no());
                et_block_no.setText(im.getblock_no());
                et_street_no.setText(im.getstreet_no());
                et_street_name.setText(im.getstreet_name());
                et_village.setText(im.getvillage());
                et_district.setText(im.getdistrict());
                et_zip_code.setText(im.getzip_code());
                et_city.setText(im.getcity());
                et_province.setText(im.getprovince());
                et_region.setText(im.getregion());
                et_country.setText(im.getcountry());
            }
        }

        myDialog.show();
    }
    public void custom_dialog_app_details() {
        myDialog = new Dialog(Waf.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.app_details_dialog);
        myDialog.setCancelable(true);

        android.view.WindowManager.LayoutParams params = myDialog.getWindow()
                .getAttributes();
        params.height = LayoutParams.WRAP_CONTENT;
        params.width = LayoutParams.FILL_PARENT;
        myDialog.getWindow().setAttributes(
                (android.view.WindowManager.LayoutParams) params);

        report_date_requested = (EditText) myDialog.findViewById(R.id.report_date_requested);
        et_first_name = (EditText) myDialog.findViewById(R.id.et_first_name);
        et_middle_name = (EditText) myDialog.findViewById(R.id.et_middle_name);
        et_last_name = (EditText) myDialog.findViewById(R.id.et_last_name);
        et_requesting_party = (AutoCompleteTextView) myDialog.findViewById(R.id.et_requesting_party);
        et_requestor = (EditText) myDialog.findViewById(R.id.et_requestor);
        app_account_company_name = (EditText) myDialog.findViewById(R.id.app_account_company_name);
        app_account_is_company = (CheckBox) myDialog.findViewById(R.id.app_account_is_company);
        spinner_purpose_appraisal = (Spinner) myDialog.findViewById(R.id.app_purpose_appraisal);
        et_app_kind_of_appraisal = (EditText) myDialog.findViewById(R.id.app_kind_of_appraisal);

        gds.autocomplete(et_requesting_party, getResources().getStringArray(R.array.requesting_party), this);
        if(app_account_is_company.isChecked()){
            app_account_company_name.setEnabled(true);
            et_first_name.setText("-");
            et_middle_name.setText("-");
            et_last_name.setText("-");

            et_first_name.setEnabled(false);
            et_middle_name.setEnabled(false);
            et_last_name.setEnabled(false);
        }else{
            app_account_company_name.setEnabled(false);
            app_account_company_name.setText("-");
            et_first_name.setEnabled(true);
            et_middle_name.setEnabled(true);
            et_last_name.setEnabled(true);
        }
        app_account_is_company.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    app_account_company_name.setEnabled(true);
                    et_first_name.setText("-");
                    et_middle_name.setText("-");
                    et_last_name.setText("-");
                    et_first_name.setEnabled(false);
                    et_middle_name.setEnabled(false);
                    et_last_name.setEnabled(false);
                } else {
                    app_account_company_name.setEnabled(false);
                    app_account_company_name.setText("-");
                    et_first_name.setEnabled(true);
                    et_middle_name.setEnabled(true);
                    et_last_name.setEnabled(true);
                }
            }
        });


        btn_save_app_details = (Button) myDialog.findViewById(R.id.btn_right);
        btn_cancel_app_details = (Button) myDialog.findViewById(R.id.btn_left);
        btn_save_app_details.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method

                boolean fieldsOK,fieldsOK2;
                gds.fill_in_error(new EditText[]{
                        et_first_name,et_last_name});
                fieldsOK = gds.validate(new EditText[]{
                        et_first_name,et_last_name});

                if(app_account_is_company.isChecked()){
                    gds.fill_in_error(new EditText[]{
                            app_account_company_name});
                    fieldsOK2 = gds.validate(new EditText[]{
                            app_account_company_name});
                }else{
                    fieldsOK2=true;
                }

                if(fieldsOK&&fieldsOK2) {
                    Report_Accepted_Jobs mv = new Report_Accepted_Jobs();


                    mv.setfname(et_first_name.getText().toString());
                    mv.setmname(et_middle_name.getText().toString());
                    mv.setlname(et_last_name.getText().toString());
                    mv.setrequesting_party(et_requesting_party.getText().toString());
                    mv.setrequestor(et_requestor.getText().toString());

                    db.updateApp_Details(mv, record_id);
                    field.clear();
                    field.add("app_account_is_company");
                    field.add("app_account_company_name");
                    field.add("nature_appraisal");
                    field.add("kind_of_appraisal");
                    value.clear();
                    value.add(gds.cbChecker(app_account_is_company));
                    value.add(app_account_company_name.getText().toString());
                    value.add(spinner_purpose_appraisal.getSelectedItem().toString());
                    value.add(et_app_kind_of_appraisal.getText().toString());
                    db2.updateRecord(value,field,"record_id = ?",new String[] { record_id },"tbl_report_accepted_jobs");
                    db.close();
                    Toast.makeText(getApplicationContext(), "Saved",
                            Toast.LENGTH_SHORT).show();
                    myDialog.dismiss();
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(),
                            "Please fill up required fields", Toast.LENGTH_SHORT).show();

                }

            }
        });

        btn_cancel_app_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method
                myDialog.dismiss();
            }
        });

        List<Report_Accepted_Jobs> report_accepted_jobs = db
                .getReport_Accepted_Jobs(record_id);
        if (!report_accepted_jobs.isEmpty()) {
            for (Report_Accepted_Jobs im : report_accepted_jobs) {
                et_first_name.setText(im.getfname());
                et_middle_name.setText(im.getmname());
                et_last_name.setText(im.getlname());
                et_requesting_party.setText(im.getrequesting_party());
                et_requestor.setText(im.getrequestor());
                gds.cbDisplay(app_account_is_company, db2.getRecord("app_account_is_company", where, args, "tbl_report_accepted_jobs").get(0));
                app_account_company_name.setText(db2.getRecord("app_account_company_name", where, args, "tbl_report_accepted_jobs").get(0));
                //valor2
                spinner_purpose_appraisal.setSelection(gds.spinnervalue(spinner_purpose_appraisal, db2.getRecord("nature_appraisal", where, args, "tbl_report_accepted_jobs").get(0)));
                et_app_kind_of_appraisal.setText(db2.getRecord("kind_of_appraisal", where, args, "tbl_report_accepted_jobs").get(0));

                // set current date into datepicker
                if ((!im.getdr_year().equals("")) && (!im.getdr_month().equals("")) && (!im.getdr_day().equals(""))) {
                    report_date_requested.setText(im.getdr_month() + "/" + im.getdr_day() + "/" + im.getdr_year());
                } else {
                    report_date_requested.setText("");
                }
            }
        }

        myDialog.show();
    }

    private List<HashMap<String, Object>> getPriorityList() {
        DatabaseHandler db = new DatabaseHandler(
                context.getApplicationContext());
        List<HashMap<String, Object>> priorityList = new ArrayList<HashMap<String, Object>>();

        List<Images> images = db.getAllImages(uid);//added account lname for specific search result like
        if (!images.isEmpty()) {
            for (Images im : images) {
                filename = im.getfilename();
                String filenameArray[] = filename.split("\\.");
                String extension = filenameArray[filenameArray.length - 1];
                if (extension.equals("pdf")) {
                    HashMap<String, Object> map1 = new HashMap<String, Object>();
                    map1.put("txt_pdf_list", im.getfilename());
                    map1.put("mono", R.drawable.attach_item);
                    priorityList.add(map1);
                }

            }

        }
        return priorityList;
    }

    public void checkFileSize() {
        myDialog = new Dialog(Waf.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.warning_dialog);
        myDialog.setCancelable(true);
        final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
        final Button btn_ok = (Button) myDialog.findViewById(R.id.btn_ok);
        tv_question.setText(R.string.file_size_too_large);
        //format double to 2 decimal place
        DecimalFormat df = new DecimalFormat("#.00");
        //get fileSize and convert to KB and MB
        File filenew = new File(myDirectory + "/" + item);//get path concat with slash and file name
        int file_size_kb = Integer.parseInt(String.valueOf(filenew.length() / 1024));
        double file_size_mb = file_size_kb / 1024.00;
        //custom toast
        Toast toast = Toast.makeText(context, item +
                "\nfile size in KB: " + file_size_kb + "KB" +
                "\nfile size in MB: " + df.format(file_size_mb) + "MB", Toast.LENGTH_LONG);
        View view = toast.getView();
        view.setBackgroundResource(R.color.toast_color);
        toast.show();


        if (file_size_kb > 6144) {//restrict the file size up to 6.0MB only
            //clear textView to restict uploading
            Toast.makeText(getApplicationContext(), "file size is too large",
                    Toast.LENGTH_SHORT).show();
            tv_attachment.setText("");
            myDialog.show();
        }

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }
    public void open_customdialog() {
        // dialog
        myDialog = new Dialog(context);
        myDialog.setTitle("Select Attachment");
        myDialog.setContentView(R.layout.request_pdf_list);
        // myDialog.setTitle("My Dialog");

        dlg_priority_lvw = (ListView) myDialog
                .findViewById(R.id.dlg_priority_lvw);
        // ListView
        SimpleAdapter adapter = new SimpleAdapter(context, getPriorityList(),
                R.layout.request_pdf_list_layout, new String[]{
                "txt_pdf_list", "mono"}, new int[]{
                R.id.txt_pdf_list, R.id.mono});
        dlg_priority_lvw.setAdapter(adapter);

        // ListView
        dlg_priority_lvw
                .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1,
                                            int arg2, long arg3) {

                        item = ((TextView) arg1.findViewById(R.id.txt_pdf_list))
                                .getText().toString();
                        attachment_selected = item;
                        tv_attachment.setText(attachment_selected);
                        // update

                        Report_filename rf = new Report_filename();
                        rf.setuid(uid);
                        rf.setfile("Property Pictures");
                        rf.setfilename(attachment_selected);
                        rf.setappraisal_type(appraisal_type + "_" + counter);
                        rf.setcandidate_done("true");
                        db.updateReport_filename(rf, record_id);
                        db.close();
                        myDialog.dismiss();
                        checkFileSize();
                    }
                });
        dlg_priority_lvw.setLongClickable(true);
        dlg_priority_lvw
                .setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> arg0,
                                                   View arg1, final int arg2, long arg3) {
                        item = ((TextView) arg1.findViewById(R.id.txt_pdf_list))
                                .getText().toString();
                        Toast.makeText(getApplicationContext(), item,
                                Toast.LENGTH_SHORT).show();

                        File file = new File(myDirectory, item);

                        if (file.exists()) {
                            Uri path = Uri.fromFile(file);
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(path, "application/pdf");
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                            try {
                                startActivity(intent);
                            } catch (ActivityNotFoundException e) {
                                Toast.makeText(getApplicationContext(),
                                        "No Application Available to View PDF",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }

                        return true;

                    }
                });
        myDialog.show();
    }

    public void open_pdf() {
        File file = new File(myDirectory, tv_attachment.getText().toString());

        if (file.exists()) {
            Uri path = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(getApplicationContext(),
                        "No Application Available to View PDF",
                        Toast.LENGTH_SHORT).show();
            }
        }

    }
    public void manual_delete_all() {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.delete_dialog);

        Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btn_no = (Button) dialog.findViewById(R.id.btn_no);
        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_question);
        dialog.show();

        text.setText("Do you want to delete this record?");

        // if button is clicked, close the custom dialog
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gds.deleteAllRecord(record_id,db2);

                dialog.dismiss();
                finish();
            }
        });
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    //PAGE BY PAGE
public void dialog_inspection_dealer() {

    myDialog = new Dialog(Waf.this);
    myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    myDialog.setContentView(R.layout.waf_dialog_inspection_dealer);
    myDialog.setCancelable(true);

    android.view.WindowManager.LayoutParams params = myDialog.getWindow()
            .getAttributes();
    params.height = LayoutParams.FILL_PARENT;
    params.width = LayoutParams.FILL_PARENT;
    myDialog.getWindow().setAttributes(
            (android.view.WindowManager.LayoutParams) params);

    valrep_waf_date_inspected = (DatePicker) myDialog
            .findViewById(R.id.valrep_waf_date_inspected);
    valrep_waf_time_inspected = (EditText) myDialog
            .findViewById(R.id.valrep_waf_time_inspected);

    valrep_waf_dealer = (EditText) myDialog.findViewById(R.id.valrep_waf_dealer);

    btn_save_inspection_dealer = (Button) myDialog
            .findViewById(R.id.btn_save_inspection_dealer);

    btn_save_inspection_dealer.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub

            // update

            field.clear();
            field.add("valrep_waf_date_inspected_month");
            field.add("valrep_waf_date_inspected_day");
            field.add("valrep_waf_date_inspected_year");
            field.add("valrep_waf_time_inspected");
            field.add("valrep_waf_dealer");
            value.clear();
            ArrayList<String> dateArrayList = gds.expandDate(valrep_waf_date_inspected);
            value.add(dateArrayList.get(0));
            value.add(dateArrayList.get(1));
            value.add(dateArrayList.get(2));
            value.add(valrep_waf_time_inspected.getText().toString());
            value.add(valrep_waf_dealer.getText().toString());
            db2.updateRecord(value,field,"record_id = ? ", new String[] { record_id }, Tables.waf.table_name);
            db.close();
            Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
            myDialog.dismiss();

        }
    });

    if(!db2.getRecord("valrep_waf_date_inspected_month", where, args, Tables.waf.table_name).get(0).contentEquals("")||
            !db2.getRecord("valrep_waf_date_inspected_day", where, args, Tables.waf.table_name).get(0).contentEquals("")||
            !db2.getRecord("valrep_waf_date_inspected_year", where, args, Tables.waf.table_name).get(0).contentEquals("")){
        int month = Integer.parseInt(db2.getRecord("valrep_waf_date_inspected_month", where, args, Tables.waf.table_name).get(0)) - 1;
        int day = Integer.parseInt(db2.getRecord("valrep_waf_date_inspected_day", where, args, Tables.waf.table_name).get(0));
        int year = Integer.parseInt(db2.getRecord("valrep_waf_date_inspected_year", where, args, Tables.waf.table_name).get(0));

        valrep_waf_date_inspected.updateDate(year, month, day);
    }

    valrep_waf_time_inspected.setText(gds.nullCheck3(db2.getRecord("valrep_waf_date_inspected_year", where, args, Tables.waf.table_name).get(0)));
    valrep_waf_dealer.setText(gds.nullCheck3(db2.getRecord("valrep_waf_dealer", where, args, Tables.waf.table_name).get(0)));

    myDialog.show();
}

public void dialog_summary(){
    myDialog = new Dialog(Waf.this);
    myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    myDialog.setContentView(R.layout.waf_dialog_summary);
    myDialog.setCancelable(true);

    android.view.WindowManager.LayoutParams params = myDialog.getWindow()
            .getAttributes();
    params.height = LayoutParams.FILL_PARENT;
    params.width = LayoutParams.FILL_PARENT;
    myDialog.getWindow().setAttributes(
            (android.view.WindowManager.LayoutParams) params);

    valrep_waf_no_of_units_subject_for_inspection = (EditText) myDialog.findViewById(R.id.valrep_waf_no_of_units_subject_for_inspection);
    valrep_waf_units_sighted = (EditText) myDialog.findViewById(R.id.valrep_waf_units_sighted);
    valrep_waf_units_unsighted = (EditText) myDialog.findViewById(R.id.valrep_waf_units_unsighted);
    valrep_waf_units_unsold = (EditText) myDialog.findViewById(R.id.valrep_waf_units_unsold);
    valrep_waf_remarks = (EditText) myDialog.findViewById(R.id.valrep_waf_remarks);
    btn_save_summary = (Button) myDialog.findViewById(R.id.btn_save_summary);

    btn_save_summary.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub

            // update

            field.clear();
            field.add("valrep_waf_no_of_units_subject_for_inspection");
            field.add("valrep_waf_units_sighted");
            field.add("valrep_waf_units_unsighted");
            field.add("valrep_waf_units_unsold");
            field.add("valrep_waf_remarks");
            value.clear();

            value.add(valrep_waf_no_of_units_subject_for_inspection.getText().toString());
            value.add(valrep_waf_units_sighted.getText().toString());
            value.add(valrep_waf_units_unsighted.getText().toString());
            value.add(valrep_waf_units_unsold.getText().toString());
            value.add(valrep_waf_remarks.getText().toString());
            db2.updateRecord(value,field,"record_id = ? ", new String[] { record_id }, Tables.waf.table_name);
            db.close();
            Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
            myDialog.dismiss();

        }
    });

    valrep_waf_no_of_units_subject_for_inspection.setText(gds.nullCheck3(db2.getRecord("valrep_waf_no_of_units_subject_for_inspection", where, args, Tables.waf.table_name).get(0)));
    valrep_waf_units_sighted.setText(gds.nullCheck3(db2.getRecord("valrep_waf_units_sighted", where, args, Tables.waf.table_name).get(0)));
    valrep_waf_units_unsighted.setText(gds.nullCheck3(db2.getRecord("valrep_waf_units_unsighted", where, args, Tables.waf.table_name).get(0)));
    valrep_waf_units_unsold.setText(gds.nullCheck3(db2.getRecord("valrep_waf_units_unsold", where, args, Tables.waf.table_name).get(0)));
    valrep_waf_remarks.setText(gds.nullCheck3(db2.getRecord("valrep_waf_remarks", where, args, Tables.waf.table_name).get(0)));

    myDialog.show();
}

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        Intent in;
        switch (v.getId()) {
            case R.id.btn_inspection_dealer://lot details
                dialog_inspection_dealer();
                break;
            case R.id.btn_listing://valuation
                in = new Intent(getApplicationContext(), Waf_Listing.class);
                in.putExtra(TAG_RECORD_ID, record_id);
                startActivityForResult(in, 100);
                break;
            case R.id.btn_summary:
                dialog_summary();
                break;
            case R.id.btn_update_address: //address
                custom_dialog_address();
                break;

            case R.id.btn_mysql_attachments: //attachments
                //in = new Intent(getApplicationContext(), Uploaded_Attachments_All.class);
                in = new Intent(getApplicationContext(), Attachments_Inflater.class);
                in.putExtra(TAG_RECORD_ID, record_id);
                in.putExtra(TAG_PASS_STAT, pass_stat);
                in.putExtra(TAG_UID, uid);
                startActivityForResult(in, 100);
                break;
            case R.id.btn_report_update_cc:
                check_attachment();
                break;
            case R.id.btn_report_view_report:
                globalSave = true;
                new GlobalSave().execute();
                break;
            case R.id.btn_report_pdf:
                //create_pdf();
                Intent createPDF = new Intent(getApplicationContext(), Collage.class);
                createPDF.putExtra(TAG_RECORD_ID, record_id);
                startActivity(createPDF);
                break;
            case R.id.btn_select_pdf:
                open_customdialog();
                break;
            case R.id.tv_attachment:
                open_pdf();
                break;
            case R.id.btn_update_app_details:
                custom_dialog_app_details();
                break;
            case R.id.btn_manual_delete:
                manual_delete_all();
                break;

            default:
                break;
        }
    }

    public void serverError() {

        myDialog = new Dialog(Waf.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.warning_dialog);
        myDialog.setCancelable(true);
        final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
        final Button btn_ok = (Button) myDialog.findViewById(R.id.btn_ok);

        tv_question.setText(R.string.server_error);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.show();
    }
    public void upload_status(boolean response) {
        fileUploaded = response;
        if (response) {
            globalSave=false;
            create_json();
        }
    }
    public void upload_attachment() {
        // upload pdf
        upload_status(gds.uploadFile(Environment.getExternalStorageDirectory() + "/" + dir + "/"
                + tv_attachment.getText().toString()));
    }
    private class SendData extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pDialog = new ProgressDialog(Waf.this);
            pDialog.setMessage("Updating data..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            NetworkUtil.getConnectivityStatusString(Waf.this);
            if (!NetworkUtil.status.equals("Network not available")) {
                gs.online = true;
                google_response = gds.google_ping();
                if (google_response.equals("200")) {

                    gs.connectedToServer = true;

                    boolean internetSpeedFast = Connectivity.isConnectedFast(getBaseContext());
                    if (internetSpeedFast == true) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                gs.fastInternet = true;
                                Toast toast = Toast.makeText(getBaseContext(), "Connected", Toast.LENGTH_LONG);
                                View view = toast.getView();
                                view.setBackgroundResource(R.color.toast_color);
                                toast.show();
                            }
                        });


                        if (((withAttachment == true) && (freshData == true)) ||
                                ((withAttachment == true) && (freshData == false))) {
                            upload_attachment();
                        } else if ((withAttachment == false) && (freshData == false)) {
                            globalSave = false;
                            create_json();
//							db_delete_all();//temp disabled
                            db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
                            finish();
                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                Toast toast = Toast.makeText(getBaseContext(), "Cannot send the data because your internet connection is slow. \nPlease make sure that you are connected to a stable connection", Toast.LENGTH_LONG);
                                View view = toast.getView();
                                view.setBackgroundResource(R.color.toast_red);
                                toast.show();
                            }
                        });
                    }
                    network_status = true;
                } else {
                    runOnUiThread(new Runnable() {
                        public void run() {

                            Toast toast = Toast.makeText(getBaseContext(), "Unable to connect to the server", Toast.LENGTH_LONG);
                            View view = toast.getView();
                            view.setBackgroundResource(R.color.toast_red);
                            toast.show();
                            serverError();
                        }
                    });
                }
            } else {
                network_status = false;
                runOnUiThread(new Runnable() {
                    public void run() {

                    }
                });
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            pDialog.dismiss();
            if (network_status == false) {

                Toast.makeText(getApplicationContext(),
                        "Network not available", Toast.LENGTH_SHORT).show();
            } else if (network_status == true) {
                if (google_response.equals("200")) {
                    if (google_response.equals("200")) {
                        if (fileUploaded) {

                            if (appStatReviewer) {
                                gs.fieldsComplete = true;
                                gs.sent = true;
                                db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
                                gs.online = false;
                                gs.connectedToServer = false;
                                gs.fastInternet = false;
                                gs.fieldsComplete = false;
                                gs.sent = false;
                                add_submitted_Delete();
                                Toast toast = Toast.makeText(getBaseContext(), "Data Updated", Toast.LENGTH_LONG);
                                View view = toast.getView();
                                view.setBackgroundResource(R.color.toast_blue);
                                toast.show();
                                finish();
                            }

                        } else{
                            Toast.makeText(getApplicationContext(), "Data Retained", Toast.LENGTH_LONG).show();
                            error_uploading();//popup dialog error
                        }
                        //finish();
                    }
                }
            }

        }
    }

    public void error_uploading() {
        myDialog = new Dialog(Waf.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.warning_dialog);
        myDialog.setCancelable(true);
        final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
        final Button btn_ok = (Button) myDialog.findViewById(R.id.btn_ok);
        tv_question.setText(R.string.error_uploading);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.show();
    }

    public void add_submitted_Delete() {
        String temp_is_company = db2.getRecord("app_account_is_company",where,args,"tbl_report_accepted_jobs").get(0);
        String temp_company_name = db2.getRecord("app_account_company_name",where,args,"tbl_report_accepted_jobs").get(0);

        if(temp_is_company.contentEquals("true")){
            account_fname = temp_company_name;
            account_mname="";
            account_lname="";
        }
        ArrayList<String> field = new ArrayList<String>();
        field.clear();
        field.add("record_id");
        field.add("first_name");
        field.add("middle_name");
        field.add("last_name");
        field.add("appraisal_type");
        field.add("status");
        ArrayList<String> value = new ArrayList<String>();
        value.clear();
        value.add(record_id);
        value.add(account_fname);
        value.add(account_mname);
        value.add(account_lname);
        value.add("WAF");
        value.add("submitted");
        String where = "WHERE record_id = args";
        String[] args = new String[1];
        args[0] = record_id;

        String r_id = db2.getRecord("record_id", where, args, "tbl_report_submitted").get(0);
        if (r_id.contentEquals(record_id)) {
            db2.deleteRecord("tbl_report_submitted", "record_id = ?", new String[]{record_id});
            db2.addRecord(value, field, "tbl_report_submitted");
        } else {
            db2.addRecord(value, field, "tbl_report_submitted");
        }
        //delete

        gds.deleteAllRecord(record_id,db2);

    }
    public void create_json() {
        JSONObject report = new JSONObject();
        JSONObject listing_objects;
        JSONArray listing_array = new JSONArray();
        try{
            ArrayList<String> mv_free = to.waf_db();
            mv_free.remove(0);
            gds.get_json_data2(report, to.waf_cc, mv_free, where, args, 0, Tables.waf.table_name, context);
            Log.e("waf_freefields", "done");

            //waf_listing
            for (int i = 0; i < db2.getRecord("id", where, args, Tables.waf_listing.table_name).size(); i++) {
                listing_objects = new JSONObject();
                ArrayList<String> listing = to.Waf_listing();
                listing.remove(0);
                gds.get_json_data2(listing_objects, to.Waf_Listing, listing, where, args, i,  Tables.waf_listing.table_name, context);
                if (!listing_objects.getString(to.Waf_Listing[1]).contentEquals(""))
                    listing_array.put(listing_objects);
            }
            gds.get_json_data(report, "valrep_waf_listings_table", listing_array, context);
            if (!globalSave) {
                if (requesting_party.contentEquals("AUTO FINANCE GROUP")) {
                    report.put("application_status", "afg_valuation");
                } else {
                    report.put("application_status", "report_review");
                }
            }
            TimeZone tz = TimeZone.getTimeZone(gs.gmt);
            Calendar c = Calendar.getInstance(tz);

            int cMonth = c.get(Calendar.MONTH);//starts with 0 for january
            int cDay = c.get(Calendar.DAY_OF_MONTH);
            int cYear = c.get(Calendar.YEAR);
            int cAmPm = c.get(Calendar.AM_PM);

            String sMonth = gs.monthInWord[cMonth];
            String sAmPm = "";
            if (cAmPm == 0) {
                sAmPm = "AM";
            } else if (cAmPm == 1) {
                sAmPm = "PM";
            }

            String cTime = String.format("%02d", c.get(Calendar.HOUR_OF_DAY)) + ":" +
                    String.format("%02d", c.get(Calendar.MINUTE)) + " " + sAmPm;

            Log.e("DATE: ", String.valueOf(cMonth + 1) + " " + String.valueOf(cDay) + " " + String.valueOf(cYear));
            Log.e("TIME: ", cTime);
//get current date and time and submit to CC

            JSONObject app_req_obj = new JSONObject();
            JSONObject address_obj = new JSONObject();//address
            JSONArray address_ary = new JSONArray();
            JSONObject record_temp = new JSONObject();

            List<Report_Accepted_Jobs> addList = db.getReport_Accepted_Jobs(String.valueOf(record_id));
            if (!addList.isEmpty()) {
                for (Report_Accepted_Jobs ad : addList) {
                    address_obj = new JSONObject();
                    address_obj.put("app_unit_no", ad.getunit_no());
                    address_obj.put("app_bldg", ad.getbuilding_name());
                    address_obj.put("app_lot_no", ad.getlot_no());
                    address_obj.put("app_block_no", ad.getblock_no());
                    address_obj.put("app_street_no", ad.getstreet_no());
                    address_obj.put("app_street_name", ad.getstreet_name());
                    address_obj.put("app_village", ad.getvillage());
                    address_obj.put("app_district", ad.getdistrict());
                    address_obj.put("app_zip", ad.getzip_code());
                    address_obj.put("app_city", ad.getcity());
                    address_obj.put("app_province", ad.getprovince());
                    address_obj.put("app_region", ad.getregion());
                    address_obj.put("app_country", ad.getcountry());
                    address_ary.put(address_obj);
                    //ADDED BY IAN
                    report.put("app_daterequested_day", ad.getdr_day());
                    report.put("app_daterequested_year", ad.getdr_year());
                    report.put("app_account_first_name", ad.getfname());
                    report.put("app_account_middle_name", ad.getmname());
                    report.put("app_account_last_name", ad.getlname());
                    report.put("app_requesting_party", ad.getrequesting_party());
                    report.put("app_requestor", ad.getrequestor());
                    args[0]=record_id;
                    report.put("app_account_is_company",db2.getRecord("app_account_is_company",where,args,"tbl_report_accepted_jobs").get(0));
                    report.put("app_account_company_name", db2.getRecord("app_account_company_name",where,args,"tbl_report_accepted_jobs").get(0));
                    report.put("app_unacceptable_collateral", db2.getRecord("app_unacceptable_collateral",where,args,"tbl_report_accepted_jobs").get(0));

                }
            }
            app_req_obj.put("app_collateral_address", address_ary);
            app_req_obj.put("app_request_appraisal", "waf");
            args[0]=record_id;
            app_req_obj.put("app_purpose_appraisal", db2.getRecord("nature_appraisal",where,args,"tbl_report_accepted_jobs").get(0));
            app_req_obj.put("app_kind_of_appraisal", db2.getRecord("kind_of_appraisal",where,args,"tbl_report_accepted_jobs").get(0));

            report.put("appraisal_request", app_req_obj);


            record_temp.put("record", report);


            // for attachment
            JSONObject appraisal_attachments;
            JSONObject appraisal_attachments_data = new JSONObject();
            JSONObject query_temp = new JSONObject();
            JSONArray app_attachments = new JSONArray();

            query_temp.put("system.record_id", record_id);
            gs.fieldsComplete = true;

                List<Report_filename> rf = db.getReport_filename(record_id);

                if (!rf.isEmpty()) {
                    for (Report_filename report_file : rf) {
                        db_app_uid = report_file.getuid();
                        db_app_file = report_file.getfile();
                        db_app_filename = report_file.getfilename();
                        db_app_appraisal_type = report_file
                                .getappraisal_type();
                        db_app_candidate_done = report_file
                                .getcandidate_done();

                        appraisal_attachments = new JSONObject();
                        appraisal_attachments.put("uid", db_app_uid);
                        appraisal_attachments.put("file", db_app_file);
                        appraisal_attachments.put("filename",
                                db_app_filename);
                        appraisal_attachments.put("appraisal_type",
                                db_app_appraisal_type);
                        appraisal_attachments.put("candidate_done",
                                db_app_candidate_done);
                        app_attachments.put(appraisal_attachments);
                        appraisal_attachments_data.put("attachments",
                                app_attachments);
                        String query = query_temp.toString();
                        String record = record_temp.toString();
                        // attachments
                        String attachment = appraisal_attachments_data
                                .toString();
                        userFunction.SendCaseCenter_Attachments(attachment);

                        boolean internetSpeedFast = Connectivity.isConnectedFast(getBaseContext());
                        if (internetSpeedFast == true) {

                            runOnUiThread(new Runnable() {
                                public void run() {
                                    gs.fastInternet = true;

                                    Toast toast = Toast.makeText(getBaseContext(), "Connected", Toast.LENGTH_LONG);
                                    View view = toast.getView();
                                    view.setBackgroundResource(R.color.toast_color);
                                    toast.show();

                                }
                            });

                            //TLS

                            // API
                            //for tls
                            Log.e("query",""+query);
                            JSONObject jsonResponse = new JSONObject();
                            ArrayList<String> field = new ArrayList<>();
                            ArrayList<String> value = new ArrayList<>();
                            field.clear();
                            field.add("auth_token");
                            field.add("query");
                            field.add("data");

                            value.clear();
                            value.add(gs.auth_token);
                            value.add(query);
                            value.add(record);


                            if(Global.type.contentEquals("tls")){
                                if (globalSave){
                                    jsonResponse = gds.makeHttpsRequest(gs.update_url, "POST", field, value);
                                } else {
                                    jsonResponse = gds.makeHttpsRequest(gs.matrix_url, "POST", field, value);
                                }
                                xml = jsonResponse.toString();
                            }else{
//									xml=gds.http_posting(field,value,gs.update_url,getApplicationContext());
                                if (globalSave){
                                    xml=gds.http_posting(field,value,gs.update_url,getApplicationContext());
                                } else {
                                    xml=gds.http_posting(field,value,gs.matrix_url,getApplicationContext());
                                }
                            }


                            Log.e("response", xml);

                            //check CC field status
                            // if application_status == report review then data is submitted
                            JSONObject mainxml = new JSONObject(xml);
                            String recordxml, application_status;
                            if (mainxml.has("record")) {
                                Log.e("record", mainxml.getString("record"));
                                if (mainxml.has("record")) {
                                    recordxml = mainxml.getString("record");
                                    Log.e("recordxml", recordxml);
                                    JSONObject json_application_status = new JSONObject(recordxml);
                                    if (json_application_status.has("application_status")) {
                                        application_status = json_application_status.getString("application_status");
                                        Log.e("application_status", application_status);

                                        if (application_status.contentEquals("report_review")) {
                                            appStatReviewer = true;
                                        } else if (application_status.contentEquals("afg_valuation")) {
                                            appStatReviewer = true;
                                        } else {
                                            appStatReviewer = false;
                                        }

                                    }
                                }
                            }//end of check CC field

                        } else {
                            runOnUiThread(new Runnable() {
                                public void run() {

                                    Toast toast = Toast.makeText(getBaseContext(), "Cannot send the data because your internet connection is slow/disconnected/unstable. \nPlease make sure that you are connected to a stable connection", Toast.LENGTH_LONG);
                                    View view = toast.getView();
                                    view.setBackgroundResource(R.color.toast_red);
                                    toast.show();
                                }
                            });
                        }
                    }
                }


        } catch (JSONException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    } catch (Exception e) {
        e.printStackTrace();
        Log.e("Update CC Failed",
                "Exception : " + e.getMessage(), e);
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(getApplicationContext(), "Failed to send data. Please try again.",
                        Toast.LENGTH_LONG).show();
                fileUploaded=false;
                upload_status(fileUploaded);
                error_uploading();
            }
        });

    }
    }
    private class GlobalSave extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pDialog = new ProgressDialog(Waf.this);
            pDialog.setMessage("Generating report...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            NetworkUtil.getConnectivityStatusString(Waf.this);
            if (!NetworkUtil.status.equals("Network not available")) {
                if (gds.google_ping().equals("200")) {
                    if (Connectivity.isConnectedFast(getBaseContext())) {
                        globalSave = true;
                        create_json();
                        Log.e("start creat json","true");
                        return true;
                    } else {
                        runOnUiThread(new Runnable() {
                            public void run() {

                                Toast toast = Toast.makeText(getBaseContext(), "Slow Internet connection.", Toast.LENGTH_LONG);
                                View view = toast.getView();
                                view.setBackgroundResource(R.color.toast_red);
                                toast.show();
                                serverError();
                            }
                        });
                        return false;
                    }

                } else {
                    runOnUiThread(new Runnable() {
                        public void run() {

                            Toast toast = Toast.makeText(getBaseContext(), "Unable to connect to the server", Toast.LENGTH_LONG);
                            View view = toast.getView();
                            view.setBackgroundResource(R.color.toast_red);
                            toast.show();
                            serverError();
                        }
                    });
                    return false;
                }

            } else {
                runOnUiThread(new Runnable() {
                    public void run() {

                        Toast toast = Toast.makeText(getBaseContext(), "Network not available.", Toast.LENGTH_LONG);
                        View view = toast.getView();
                        view.setBackgroundResource(R.color.toast_red);
                        toast.show();
                        serverError();
                    }
                });
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            if (result) {
                Intent in = new Intent(getApplicationContext(), View_Report.class);
                in.putExtra(TAG_RECORD_ID, record_id);
                in.putExtra("appraisal_type", "waf");
                startActivityForResult(in, 100);
            }
            pDialog.dismiss();


        }

    }

    @Override
    public void onUserInteraction() {
        st.resetDisconnectTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        st.stopDisconnectTimer();
    }

    @Override
    public void onResume() {
        super.onResume();
        st.resetDisconnectTimer();
    }
}
