package com.gds.appraisalmaybank.waf;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database.TableObjects;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database_new.Tables;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ianlozares on 17/10/2017.
 */

public class Waf_Listing extends ListActivity implements View.OnClickListener {
    DatabaseHandler2 db = new DatabaseHandler2(this);

    GDS_methods gds = new GDS_methods();
    Button btn_add_listing, btn_save_listing;
    Button btn_create, btn_cancel, btn_save;
    String where = "WHERE record_id = args";
    final String[] args = new String[1];
    String whereId = "WHERE record_id = args and id = args";
    final String[] argsId = new String[2];
    Session_Timer st = new Session_Timer(this);
    String record_id="";
    private static final String TAG_RECORD_ID = "record_id";
    private static final String tag_listing_id = "listing_id";
    private static final String tag_vin_serial = "vin_serial";
    private static final String tag_sticker_no = "sticker_no";
    private static final String tag_vpo_number = "vpo_number";
    Dialog myDialog;
    private ProgressDialog pDialog;

    ArrayList<HashMap<String, String>> itemList;
    String listing_id="";

    EditText valrep_waf_vin_serial, valrep_waf_model_description, valrep_waf_color, valrep_waf_conduction_sticker_no,
            valrep_waf_pntr_date, valrep_waf_vpo_number, valrep_waf_sighted, valrep_waf_odometer_reading,
            valrep_waf_unsold_in_transit, valrep_waf_unsold_others, valrep_waf_date_sold, valrep_waf_date_remit,
            valrep_waf_item_remarks;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.waf_listing);
        btn_add_listing = (Button) findViewById(R.id.btn_add_listing);
        btn_add_listing.setOnClickListener(this);

        btn_save_listing = (Button) findViewById(R.id.btn_save_listing);
        btn_save_listing.setOnClickListener(this);
        st.resetDisconnectTimer();

// getting record_id from intent / previous class
        Intent i = getIntent();
        record_id = i.getStringExtra(TAG_RECORD_ID);
        args[0] = record_id;
        argsId[0] = record_id;
        //list
        itemList = new ArrayList<HashMap<String, String>>();
        // Loading in Background Thread
        new LoadAll().execute();
        // Get listview
        ListView lv = getListView();

        // on seleting single item
        // launching Edit item Screen
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // getting values from selected ListItem
                listing_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
                        .toString();
                argsId[1] = listing_id;

                view_details();
            }
        });
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {
                // TODO Auto-generated method stub

                Log.v("long clicked","pos: " + position);
                listing_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
                        .toString();
                delete_item();
                return true;
            }
        });
    }

    @Override
    public void onClick(View arg0) {
        // TODO Auto-generated method stub
        switch (arg0.getId()) {
            case R.id.btn_add_listing:
                add_new();
                break;
            case R.id.btn_save_listing:
                finish();
                break;
            default:
                break;
        }
    }

    public void add_new() {
        Toast.makeText(getApplicationContext(), "Add New",
                Toast.LENGTH_SHORT).show();
        myDialog = new Dialog(Waf_Listing.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.waf_listing_form);
        myDialog.setCancelable(true);
        android.view.WindowManager.LayoutParams params = myDialog.getWindow()
                .getAttributes();
        params.height = LinearLayout.LayoutParams.FILL_PARENT;
        params.width = LinearLayout.LayoutParams.FILL_PARENT;
        myDialog.getWindow().setAttributes(
                (android.view.WindowManager.LayoutParams) params);

        btn_create = (Button) myDialog.findViewById(R.id.btn_right);
        btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
        btn_create.setText("Create");
        btn_cancel.setText("Cancel");

        valrep_waf_vin_serial = (EditText) myDialog.findViewById(R.id.valrep_waf_vin_serial);
        valrep_waf_model_description = (EditText) myDialog.findViewById(R.id.valrep_waf_model_description);
        valrep_waf_color = (EditText) myDialog.findViewById(R.id.valrep_waf_color);
        valrep_waf_conduction_sticker_no = (EditText) myDialog.findViewById(R.id.valrep_waf_conduction_sticker_no);
        valrep_waf_pntr_date = (EditText) myDialog.findViewById(R.id.valrep_waf_pntr_date);
        valrep_waf_vpo_number = (EditText) myDialog.findViewById(R.id.valrep_waf_vpo_number);
        valrep_waf_sighted = (EditText) myDialog.findViewById(R.id.valrep_waf_sighted);
        valrep_waf_odometer_reading = (EditText) myDialog.findViewById(R.id.valrep_waf_odometer_reading);
        valrep_waf_unsold_in_transit = (EditText) myDialog.findViewById(R.id.valrep_waf_unsold_in_transit);
        valrep_waf_unsold_others = (EditText) myDialog.findViewById(R.id.valrep_waf_unsold_others);
        valrep_waf_date_sold = (EditText) myDialog.findViewById(R.id.valrep_waf_date_sold);
        valrep_waf_date_remit = (EditText) myDialog.findViewById(R.id.valrep_waf_date_remit);
        valrep_waf_item_remarks = (EditText) myDialog.findViewById(R.id.valrep_waf_item_remarks);


        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> values = new ArrayList<String>();
                values.clear();
                values.add(record_id);
                values.add(valrep_waf_vin_serial.getText().toString());
                values.add(valrep_waf_model_description.getText().toString());
                values.add(valrep_waf_color.getText().toString());
                values.add(valrep_waf_conduction_sticker_no.getText().toString());
                values.add(valrep_waf_pntr_date.getText().toString());
                values.add(valrep_waf_vpo_number.getText().toString());
                values.add(valrep_waf_sighted.getText().toString());
                values.add(valrep_waf_odometer_reading.getText().toString());
                values.add(valrep_waf_unsold_in_transit.getText().toString());
                values.add(valrep_waf_unsold_others.getText().toString());
                values.add(valrep_waf_date_sold.getText().toString());
                values.add(valrep_waf_date_remit.getText().toString());
                values.add(valrep_waf_item_remarks.getText().toString());

                db.addRecord(values, TableObjects.Waf_listing(), Tables.waf_listing.table_name);
                Toast.makeText(getApplicationContext(), "Data created", Toast.LENGTH_SHORT).show();
                myDialog.dismiss();
                reloadClass();

            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.show();
    }
    public void view_details() {
        myDialog = new Dialog(Waf_Listing.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.waf_listing_form);
        myDialog.setCancelable(true);

        android.view.WindowManager.LayoutParams params = myDialog.getWindow()
                .getAttributes();
        params.height = LinearLayout.LayoutParams.FILL_PARENT;
        params.width = LinearLayout.LayoutParams.FILL_PARENT;
        myDialog.getWindow().setAttributes(
                (android.view.WindowManager.LayoutParams) params);

        btn_save = (Button) myDialog.findViewById(R.id.btn_right);
        btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
        btn_save.setText("Save");
        btn_cancel.setText("Cancel");

        valrep_waf_vin_serial = (EditText) myDialog.findViewById(R.id.valrep_waf_vin_serial);
        valrep_waf_model_description = (EditText) myDialog.findViewById(R.id.valrep_waf_model_description);
        valrep_waf_color = (EditText) myDialog.findViewById(R.id.valrep_waf_color);
        valrep_waf_conduction_sticker_no = (EditText) myDialog.findViewById(R.id.valrep_waf_conduction_sticker_no);
        valrep_waf_pntr_date = (EditText) myDialog.findViewById(R.id.valrep_waf_pntr_date);
        valrep_waf_vpo_number = (EditText) myDialog.findViewById(R.id.valrep_waf_vpo_number);
        valrep_waf_sighted = (EditText) myDialog.findViewById(R.id.valrep_waf_sighted);
        valrep_waf_odometer_reading = (EditText) myDialog.findViewById(R.id.valrep_waf_odometer_reading);
        valrep_waf_unsold_in_transit = (EditText) myDialog.findViewById(R.id.valrep_waf_unsold_in_transit);
        valrep_waf_unsold_others = (EditText) myDialog.findViewById(R.id.valrep_waf_unsold_others);
        valrep_waf_date_sold = (EditText) myDialog.findViewById(R.id.valrep_waf_date_sold);
        valrep_waf_date_remit = (EditText) myDialog.findViewById(R.id.valrep_waf_date_remit);
        valrep_waf_item_remarks = (EditText) myDialog.findViewById(R.id.valrep_waf_item_remarks);

        valrep_waf_vin_serial.setText(db.getRecord("valrep_waf_vin_serial", whereId, argsId, Tables.waf_listing.table_name).get(0));
        valrep_waf_model_description.setText(db.getRecord("valrep_waf_model_description", whereId, argsId, Tables.waf_listing.table_name).get(0));
        valrep_waf_color.setText(db.getRecord("valrep_waf_color", whereId, argsId, Tables.waf_listing.table_name).get(0));
        valrep_waf_conduction_sticker_no.setText(db.getRecord("valrep_waf_conduction_sticker_no", whereId, argsId, Tables.waf_listing.table_name).get(0));
        valrep_waf_vpo_number.setText(db.getRecord("valrep_waf_vpo_number", whereId, argsId, Tables.waf_listing.table_name).get(0));
        valrep_waf_sighted.setText(db.getRecord("valrep_waf_sighted", whereId, argsId, Tables.waf_listing.table_name).get(0));
        valrep_waf_odometer_reading.setText(db.getRecord("valrep_waf_odometer_reading", whereId, argsId, Tables.waf_listing.table_name).get(0));
        valrep_waf_unsold_in_transit.setText(db.getRecord("valrep_waf_unsold_in_transit", whereId, argsId, Tables.waf_listing.table_name).get(0));
        valrep_waf_unsold_others.setText(db.getRecord("valrep_waf_unsold_others", whereId, argsId, Tables.waf_listing.table_name).get(0));
        valrep_waf_date_sold.setText(db.getRecord("valrep_waf_date_sold", whereId, argsId, Tables.waf_listing.table_name).get(0));
        valrep_waf_date_remit.setText(db.getRecord("valrep_waf_date_remit", whereId, argsId, Tables.waf_listing.table_name).get(0));
        valrep_waf_item_remarks.setText(db.getRecord("valrep_waf_item_remarks", whereId, argsId, Tables.waf_listing.table_name).get(0));

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ArrayList<String> values = new ArrayList<String>();
                values.clear();
                values.add(record_id);
                values.add(valrep_waf_vin_serial.getText().toString());
                values.add(valrep_waf_model_description.getText().toString());
                values.add(valrep_waf_color.getText().toString());
                values.add(valrep_waf_conduction_sticker_no.getText().toString());
                values.add(valrep_waf_pntr_date.getText().toString());
                values.add(valrep_waf_vpo_number.getText().toString());
                values.add(valrep_waf_sighted.getText().toString());
                values.add(valrep_waf_odometer_reading.getText().toString());
                values.add(valrep_waf_unsold_in_transit.getText().toString());
                values.add(valrep_waf_unsold_others.getText().toString());
                values.add(valrep_waf_date_sold.getText().toString());
                values.add(valrep_waf_date_remit.getText().toString());
                values.add(valrep_waf_item_remarks.getText().toString());

                db.updateRecord(values, TableObjects.Waf_listing(), "record_id = ? AND id = ?", new String[] {record_id, listing_id},Tables.waf_listing.table_name);

                myDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Saved",Toast.LENGTH_SHORT).show();
                reloadClass();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });


        myDialog.show();
    }

    public void delete_item(){
        myDialog = new Dialog(Waf_Listing.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.delete_dialog);
        myDialog.setCancelable(true);
        final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
        final Button btn_yes = (Button) myDialog.findViewById(R.id.btn_yes);
        final Button btn_no = (Button) myDialog.findViewById(R.id.btn_no);
        tv_question.setText("Are you sure you want to delete the selected item?");
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.deleteRecord(Tables.waf_listing.table_name,"record_id = ? and id = ?", new String[]{record_id,listing_id});

                myDialog.dismiss();
                reloadClass();
                Toast.makeText(getApplicationContext(), "Data Deleted",Toast.LENGTH_SHORT).show();
            }
        });
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        myDialog.show();
    }

    public void reloadClass(){
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }
    /**
     * Background Async Task to Load all
     * */
    class LoadAll extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Waf_Listing.this);
            pDialog.setMessage("Loading Lists. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting All
         * */
        protected String doInBackground(String... arg) {
            String vin_serial="", sticker_no="", vpo_number="";

            for(int i = 0; i < db.getRecord("id", where, args, Tables.waf_listing.table_name).size(); i++){
                listing_id = db.getRecord("id", where, args, Tables.waf_listing.table_name).get(i);
                if(!listing_id.contentEquals("")){
                    vin_serial= db.getRecord("valrep_waf_vin_serial", where, args, Tables.waf_listing.table_name).get(i);
                    sticker_no= db.getRecord("valrep_waf_conduction_sticker_no", where, args, Tables.waf_listing.table_name).get(i);
                    vpo_number= db.getRecord("valrep_waf_vpo_number", where, args, Tables.waf_listing.table_name).get(i);
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(tag_listing_id, listing_id);
                    map.put(tag_vin_serial, vin_serial);
                    map.put(tag_sticker_no, sticker_no);
                    map.put(tag_vpo_number, vpo_number);
                    // adding HashList to ArrayList
                    itemList.add(map);
                }

            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all
            //if(pDialog != null)
                pDialog.dismiss();
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    ListAdapter adapter = new SimpleAdapter(
                            Waf_Listing.this, itemList,
                            R.layout.waf_listing_list, new String[] { tag_listing_id, tag_vin_serial,
                            tag_sticker_no, tag_vpo_number},
                            new int[] { R.id.primary_key, R.id.item_vin_serial, R.id.item_sticker_no, R.id.item_vpo_number });
                    // updating listview
                    setListAdapter(adapter);
                }
            });
            if(itemList.isEmpty()){
                add_new();
            }
        }
    }
    @Override
    public void onUserInteraction(){
        st.resetDisconnectTimer();
    }
    @Override
    public void onStop() {
        super.onStop();
        st.stopDisconnectTimer();
    }
    @Override
    public void onResume() {
        super.onResume();
        st.resetDisconnectTimer();
    }

}
