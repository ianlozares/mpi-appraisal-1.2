package com.gds.appraisalmaybank.main;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gds.appraisalmaybank.attachments.Index_Attachments;
import com.gds.appraisalmaybank.methods.Session_Timer;
import com.gds.appraisalmaybank.request.Index_Request;
import com.gds.appraisalmaybank.requestjobs.Index_request_jobs_auth;

public class Index extends Activity implements OnClickListener {
	Button index_btn_request_jobs, index_btn_request, index_btn_attachments;
	// for prompts
	Context context = this;
	Button button;
	EditText result;
	String image_name;
	String responseCode = "";
	
	TextView tv_download;
	
	WifiManager wifiManager;
	WifiLock lock;
	Session_Timer st = new Session_Timer(this);
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.index);
		st.resetDisconnectTimer();
		index_btn_request_jobs = (Button) findViewById(R.id.index_btn_request_jobs);
		index_btn_request = (Button) findViewById(R.id.index_btn_request);
		index_btn_attachments = (Button) findViewById(R.id.index_btn_attachments);
		index_btn_request_jobs.setOnClickListener(this);
		index_btn_request.setOnClickListener(this);
		index_btn_attachments.setOnClickListener(this);
		
		tv_download = (TextView) findViewById(R.id.tv_download);
		tv_download.setOnClickListener(this);
		
		/*wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
//		lock = wifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL, "LockTag");
		lock = wifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF, "HIGH_WIFI");
		lock.acquire();*/
	}

	@Override
	public void onBackPressed() {
		exit_dialog();
	}

	public void exit_dialog() {
		// custom dialog
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.yes_no_dialog);

		// set the custom dialog components - text, image and button
		TextView text = (TextView) dialog.findViewById(R.id.tv_question);
		text.setText("Are you sure you want to exit the application?");
		Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
		Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

		// if button is clicked, close the custom dialog
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				lock.release();
				
				finish();
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.index_btn_request:
			startActivity(new Intent(Index.this, Index_Request.class));
			break;

		case R.id.index_btn_attachments:
			startActivity(new Intent(Index.this, Index_Attachments.class));
			break;

		case R.id.index_btn_request_jobs:
			startActivity(new Intent(Index.this, Index_request_jobs_auth.class));
			break;
			
		case R.id.tv_download:
//			final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
			final String appPackageName = "com.gds.projects.lotplottingapp"; // getPackageName() from Context or Activity object
			try {
			    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
			} catch (android.content.ActivityNotFoundException anfe) {
			    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
			}
			break;
			
		default:
			break;
		}
	}

	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}
}
