package com.gds.appraisalmaybank.main;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.gds.appraisalmaybank.database_new.DatabaseHandler_New;
import com.gds.appraisalmaybank.requestjobs.Index_request_jobs_auth;

import java.io.File;
import java.util.List;

public class Splash_Screen extends Activity {
	public static Context context;
	String dir = "gds_appraisal";
	private static String DB_PATH = "/data/data/com.gds.appraisalmaybank.main/databases/";
	private static String DB_NAME = "GDSAppraisalDb";
	boolean check;
	private ProgressDialog pDialog;
	Global gs;
	DatabaseHandler_New db;
	ImageView splash_logo;
	Animation animate;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);
		Splash_Screen.context = getApplicationContext();
		splash_logo = (ImageView) findViewById(R.id.splash_logo);
		animate = AnimationUtils.loadAnimation(this, R.anim.translate);
		splash_logo.startAnimation(animate);
		create_folder();
		db = new DatabaseHandler_New(Splash_Screen.this);
		// create / open tables
		db.getWritableDatabase();
		new DB_checking().execute();

		
		//kill background processes
		List<ApplicationInfo> packages;
	    PackageManager pm;
	    pm = getPackageManager();
	    // get a list of installed apps.
	    packages = pm.getInstalledApplications(0);
	    
	    ActivityManager mActivityManager = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);

	    for (ApplicationInfo packageInfo : packages) {
	    	Log.v("pi",packageInfo.toString());
	        if ((packageInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 1)
	            continue;
	        if (packageInfo.packageName.equals("com.gds.appraisalmaybank.main"))
	            continue;
	        mActivityManager.killBackgroundProcesses(packageInfo.packageName);
	    }
	    //kill background processes //android:name="android.permission.KILL_BACKGROUND_PROCESSES

	}

	public void create_folder() {
		// create dir
		File myDirectory = new File(Environment.getExternalStorageDirectory()
				+ "/" + dir + "/");
		if (!myDirectory.exists()) {
			myDirectory.mkdirs();
		}
	}

	private class DB_checking extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			/*pDialog = new ProgressDialog(Splash_Screen.this);
			pDialog.setMessage("Parsing Data..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();*/
		}

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			check = true;
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			if (check == true) {

				/*try {
					// Open your local db as the input stream
					InputStream myInput = Splash_Screen.this.getAssets().open(
							DB_NAME);

					// Path to the just created empty db
					String outFileName = DB_PATH + DB_NAME;

					// Open the empty db as the output stream
					OutputStream myOutput = new FileOutputStream(outFileName);

					// transfer bytes from the inputfile to the outputfile
					byte[] buffer = new byte[1024];
					int length;
					while ((length = myInput.read(buffer)) > 0) {
						myOutput.write(buffer, 0, length);
					}

					// Close the streams
					myOutput.flush();
					myOutput.close();
					myInput.close();
				} catch (IOException e) {
					e.printStackTrace();
				}*/
				//pDialog.dismiss();
				startActivity(new Intent(Splash_Screen.this, Index_request_jobs_auth.class));
				finish();
			} else {
				//pDialog.dismiss();
				Thread timer = new Thread() {
					public void run() {
						try {
							sleep(4000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						} finally {
							startActivity(new Intent(Splash_Screen.this,
									Index_request_jobs_auth.class));

						}
					}
				};
				timer.start();

			}
		}
	}

	/*public void check_db() {
		List<Regions> regions = db.getAllRegions();
		if (regions.isEmpty()) {
			check = true;
		} else {
			check = false;
		}
	}*/

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}
}
