package com.gds.appraisalmaybank.main;

import android.app.Application;

public class Global extends Application {

	public String account_name, account_email, account_password;
	public Double latitude, longitude;
	public String image_single_filename, image_single_status;
	public String[] arrAuto = new String[10];

	public String attachment_file_name;
	public String request_form_record;

	public String rf_id, cntrol_no, vehicle_type, car_model, nature_appraisal,
			appraisal_type, inspection_date1_month, inspection_date1_day,
			inspection_date1_year, inspection_date2_month,
			inspection_date2_day, inspection_date2_year;
	public String mv_year, mv_make, mv_model, mv_odometer, mv_vin, me_quantity,
			me_type, me_manufacturer, me_model, me_serial, me_age,
			me_condition;
	
//	public String[] monthInWord = { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
//			"Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
	public String[] monthInWord = { "01", "02", "03", "04", "05", "06",
			"07", "08", "09", "10", "11", "12" };
	
	public String[] appraisal_type_value = { "machinery_equipment",
			"vacant_lot", "land_improvements", "townhouse_condo", "ppcr",
			"construction_ebm", "motor_vehicle", "townhouse", "waf","spv_land_improvements" };
	public String[] appraisal_output = { "Machinery and Equipment",
			"Vacant Lot", "Land with Improvements", "Condominium",
			"PPCR", "Construction with EBM", "Motor Vehicle", "Townhouse", "WAF","SPV - Land and Improvements" };
	public String[] appraisal_form_classification = { "Endorsed", "Unendorsed" };
	public String[] appraisal_type_nature_appraisal = { "", "Initial",
			"Re-appraisal" };

	public String request_form_fname, request_form_lname;
	public int request_type_index;

	// request jobs
	public String record_id;


	public String gmt = "GMT+08:00";
	
	public String date_submitted="", status="Failed", status_reason="";
	public String[] status_reason_ary = {
			"No internet connection",//0
			"Server connection error",//1
			"Slow internet connection",//2
			"Incomplete fields",//3
			"Sent"//4
			};
	public boolean online = false,
			connectedToServer = false,
			fastInternet = false,
			fieldsComplete = false,
			sent = false;

	public static String type="tls";//tls or http
	public static String cert_name="gdsdev2.crt";//mpi_uat or gdsdev2 or mpi_prod
	//DEV2
	//dev2 tls

	public String server_type = "Dev2";
	public static String server_url = "https://casecenter-dev.gdslinkasia.com";
	//dev2 http
	//public static String server_url = "http://dev2.gdslinkasia.com";

	public String create_url = server_url+"/maybank_philippines_incorporated/appraisal/records/create.json";
	public String records_url = server_url+"/maybank_philippines_incorporated/appraisal/records.json";
	public String show_url = server_url+"/maybank_philippines_incorporated/appraisal/records/show.json";
	public String update_url = server_url+"/maybank_philippines_incorporated/appraisal/records/update.json";
	public String matrix_url = server_url+"/maybank_philippines_incorporated/appraisal/update/mobile_submission.json";
	public static String  sign_in_url = server_url+"/users/sign_in.json";
	//public String pdf_upload_url = "http://dev2.gdslinkasia.com:8080/upload_pdf.php";
	public static String auth_token = "9qUpbma2sx1oz8PCLQpq";//maybank dev2
	public String user_role_url = server_url+"/maybank_philippines_incorporated/appraisal/users/";
	public String user_role_end_url = ".json?auth_token="+auth_token;

	//attachements DEV 2 AND UAT MAYBANK
	public static String registerURL_attachments = server_url+":8080/maybank_php/index2.php";//
	public String get_files_url = server_url+":8080/maybank_php/get_all_uploaded_files.php";//mySQL get_all_uploaded_files_imagick.php
	public String pdf_upload_url = server_url+":8080/maybank_php/upload_pdf_maybank.php";//webView
	public static String pdf_upload_url_new = server_url+":8080/maybank_php/upload_pdf_response_maybank.php";//webView
	public static String pdf_loc_url = server_url+":8080/pdf_attachments/pdf_maybank/";//webView
	public String pdf_to_jpg_converter_url = server_url+":8080/maybank_php/pdf_to_jpg_converter.php";




	//MPI UAT
	/*public String server_type = "UAT";
	public static String server_url = "https://www.mpilos-uat.maybank.com.ph";

	public String create_url = server_url+":8082/maybank_philippines_incorporated/appraisal/records/create.json";
	public String records_url = server_url+":8082/maybank_philippines_incorporated/appraisal/records.json";
	public String show_url = server_url+":8082/maybank_philippines_incorporated/appraisal/records/show.json";
	public String update_url = server_url+":8082/maybank_philippines_incorporated/appraisal/records/update.json";
	public String matrix_url = server_url+":8082/maybank_philippines_incorporated/appraisal/update/mobile_submission.json";
	public static String sign_in_url = server_url+":8082/users/sign_in.json";
	public static String auth_token = "dyyixUoeXsMLmbaRs9ox";//maybank uat
	public String user_role_url = server_url+":8082/maybank_philippines_incorporated/appraisal/users/";
	public String user_role_end_url = ".json?auth_token="+auth_token;

	//UAT MAYBANK SERVER
	public static String registerURL_attachments = server_url+":8080/appraisal/maybank_php/index2.php";//
	public String get_files_url = server_url+":8080/appraisal/maybank_php/get_all_uploaded_files.php";//mySQL get_all_uploaded_files_imagick.php
	public String pdf_upload_url = server_url+":8080/appraisal/maybank_php/upload_pdf_maybank.php";//webView
	public static String pdf_upload_url_new = server_url+":8080/appraisal/maybank_php/upload_pdf_response_maybank.php";//webView
	public static String pdf_loc_url = server_url+":8080/appraisal/pdf_attachments/appraisal/";//webView
	public String pdf_to_jpg_converter_url = server_url+":8080/appraisal/maybank_php/pdf_to_jpg_converter.php";*/



	//MPI PROD Server
	// string PROD MAYBANK SERVER
	/*public String server_type = "";
	public static String server_url = "https://mpi-prod.gdslinkasia.com:8082";
	public String create_url = "https://mpi-prod.gdslinkasia.com:8082/maybank_philippines_incorporated/appraisal/records/create.json";
	public String records_url = "https://mpi-prod.gdslinkasia.com:8082/maybank_philippines_incorporated/appraisal/records.json";
	public String show_url = "https://mpi-prod.gdslinkasia.com:8082/maybank_philippines_incorporated/appraisal/records/show.json";
	public String update_url = "https://mpi-prod.gdslinkasia.com:8082/maybank_philippines_incorporated/appraisal/records/update.json";
	public String matrix_url = "https://mpi-prod.gdslinkasia.com:8082/maybank_philippines_incorporated/appraisal/update/mobile_submission.json";
	public static String sign_in_url = "https://mpi-prod.gdslinkasia.com:8082/users/sign_in.json";
	public static String auth_token = "dyyixUoeXsMLmbaRs9ox";//maybank uat
	public String user_role_url = "https://mpi-prod.gdslinkasia.com:8082/maybank_philippines_incorporated/appraisal/users/";
	public String user_role_end_url = ".json?auth_token="+auth_token;

	public static String registerURL_attachments = "https://mpi-prod.gdslinkasia.com:8080/appraisal/maybank_php/index2.php";//
	public String get_files_url = "https://mpi-prod.gdslinkasia.com:8080/appraisal/maybank_php/get_all_uploaded_files.php";//mySQL get_all_uploaded_files_imagick.php
	public String pdf_upload_url = "https://mpi-prod.gdslinkasia.com:8080/appraisal/maybank_php/upload_pdf_maybank3.php";//webView
	public static String pdf_upload_url_new = "https://mpi-prod.gdslinkasia.com:8080/appraisal/maybank_php/upload_pdf_response_maybank.php";//webView
	public static String pdf_loc_url = "https://mpi-prod.gdslinkasia.com:8080/appraisal/pdf_attachments/appraisal/";//webView
	public String pdf_to_jpg_converter_url = "https://mpi-prod.gdslinkasia.com:8080/appraisal/maybank_php/pdf_to_jpg_converter.php";

*/
}
