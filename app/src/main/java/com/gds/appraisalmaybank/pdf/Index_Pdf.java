package com.gds.appraisalmaybank.pdf;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.gds.appraisalmaybank.attachments.Index_Attachments;
import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Images;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.Index;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.Session_Timer;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;

public class Index_Pdf extends Activity implements OnClickListener {
	DatabaseHandler db = new DatabaseHandler(this);
	Button buttonAdd, pdf_create;
	LinearLayout container;
	RelativeLayout container2;
	ArrayList<EditText> myEditTextList = new ArrayList<EditText>();
	int counter = 0;
	Global gs;
	List<String> ImagesArray = new ArrayList<String>();
	List<String> ImagesArraySize = new ArrayList<String>();
	// for prompts
	Context context = this;
	Button button;
	EditText result;
	String image_name, pdf_name, filename, item;
	String dir = "gds_appraisal";
	Dialog myDialog;
	ListView dlg_priority_lvw = null;
	String[] commandArray = new String[] { "Whole Page", "Half Page" };
	int height, width;
	String size;
	String month, day, year;
	String month_temp, day_temp;
	Session_Timer st = new Session_Timer(this);
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.index_pdf);
		st.resetDisconnectTimer();

		buttonAdd = (Button) findViewById(R.id.add);
		pdf_create = (Button) findViewById(R.id.pdf_create);
		container = (LinearLayout) findViewById(R.id.container);
		container2 = (RelativeLayout) findViewById(R.id.container2);
		gs = ((Global) getApplicationContext());
		pdf_name_prompts();

		buttonAdd.setOnClickListener(this);
		pdf_create.setOnClickListener(this);

		pdf_create.setEnabled(false);
		setCurrentDateOnView();
	}

	public void add_image() {
		String myDirectory = Environment.getExternalStorageDirectory() + "/"
				+ dir + "/";
		LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View addView = layoutInflater.inflate(R.layout.row, null);
		final ImageView textOut = (ImageView) addView
				.findViewById(R.id.textout);
		Bitmap bmp = BitmapFactory.decodeFile(myDirectory + "/" + item);
		textOut.setImageBitmap(bmp);

		final TextView tv_pdf = (TextView) addView.findViewById(R.id.tv_pdf);
		tv_pdf.setText(String.valueOf(counter));
		counter++;

		/*
		 * gs.arrAuto[counter] =
		 * spinner_image_list.getSelectedItem().toString();
		 */
		ImagesArray.add(myDirectory + "/" + item);
		// button remove
		Button buttonRemove = (Button) addView.findViewById(R.id.remove);
		buttonRemove.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((LinearLayout) addView.getParent()).removeView(addView);
				ImagesArray.set(Integer.parseInt(tv_pdf.getText().toString()),
						"");

			}
		});

		container.addView(addView);
	}

	public void open_customdialog() {
		// dialog
		myDialog = new Dialog(context);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.pdf_customdialog);
		// myDialog.setTitle("My Dialog");

		dlg_priority_lvw = (ListView) myDialog
				.findViewById(R.id.dlg_priority_lvw);
		// ListView
		SimpleAdapter adapter = new SimpleAdapter(context, getPriorityList(),
				R.layout.pdf_customdialog_layout, new String[] {
						"list_priority_img", "list_priority_value" },
				new int[] { R.id.list_priority_img, R.id.list_priority_value });
		dlg_priority_lvw.setAdapter(adapter);

		// ListView
		dlg_priority_lvw
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {

						item = ((TextView) arg1
								.findViewById(R.id.list_priority_value))
								.getText().toString();
						myDialog.dismiss();
						// dialog
						Resize_dialog();
						add_image();
					}
				});
		myDialog.show();
	}

	private void Resize_dialog() {
		android.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Size of Image:");
		builder.setItems(commandArray, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int index) {
				if (index == 0) {
					ImagesArraySize.add("500" + "-" + "730");
					dialog.dismiss();
					pdf_create.setEnabled(true);
				} else if (index == 1) {
					ImagesArraySize.add("500" + "-" + "365");
					dialog.dismiss();
					pdf_create.setEnabled(true);
				}
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public void create_pdf() {
		// step 1: creation of a document-object
		Document document = new Document();

		try {
			// step 2:
			// we create a writer that listens to the document
			// and directs a PDF-stream to a file
			PdfWriter.getInstance(document, new FileOutputStream(
					android.os.Environment.getExternalStorageDirectory()
							+ java.io.File.separator + "gds_appraisal"
							+ java.io.File.separator + pdf_name + ".pdf"));

			// step 3: we open the document
			document.open();

			for (int x = 0; x < ImagesArray.size(); x++) {
				if (!ImagesArray.get(x).equals("")) {
					/*
					 * // step 4: document.add(new Paragraph("Text Here :"));
					 */

					// Can't use filename => use byte[] instead
					// Image jpg = Image.getInstance("otsoe.jpg");
					ByteArrayOutputStream stream = new ByteArrayOutputStream();
					Bitmap bitmap = BitmapFactory
							.decodeFile(ImagesArray.get(x));
					size = ImagesArraySize.get(x);
					String filenameArray[] = size.split("\\-");
					width = Integer.parseInt(filenameArray[0]);
					height = Integer.parseInt(filenameArray[1]);
					Bitmap out = Bitmap.createScaledBitmap(bitmap, width,
							height, false);
					out.compress(Bitmap.CompressFormat.JPEG /* FileType */,
							100 /* Ratio */, stream);

					Image jpg = Image.getInstance(stream.toByteArray());
					jpg.setAlignment(Image.MIDDLE);
					document.add(new Paragraph("Jeven"));
					document.add(jpg);
				}
			}
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}

		// step 5: we close the document
		document.close();
	}

	public void pdf_name_prompts() {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView = li.inflate(R.layout.pdf_name_input, null);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);

		final EditText userInput = (EditText) promptsView
				.findViewById(R.id.et_image_name);

		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						pdf_name = userInput.getText().toString();
					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								dialog.cancel();
								startActivity(new Intent(Index_Pdf.this,
										Index.class));
								finish();
							}
						});
		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	public void setCurrentDateOnView() {
		final Calendar c = Calendar.getInstance();
		year = String.valueOf(c.get(Calendar.YEAR));
		month_temp = String.valueOf(c.get(Calendar.MONTH) + 1);
		day_temp = String.valueOf(c.get(Calendar.DAY_OF_MONTH));

		if (month_temp.length() == 1) {
			month = "0" + month_temp;
		} else {
			month = month_temp;
		}
		if (day_temp.length() == 1) {
			day = "0" + day_temp;
		} else {
			day = day_temp;
		}
	}

	public void db_pdf() {
		File myDirectory = new File(Environment.getExternalStorageDirectory()
				+ "/" + dir + "/");
		db.addImages(new Images(myDirectory.toString(), pdf_name + ".pdf"));
		Toast.makeText(getApplicationContext(), pdf_name + " created",
				Toast.LENGTH_SHORT).show();
		startActivity(new Intent(this, Index_Attachments.class));
		finish();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.add:
			open_customdialog();
			break;

		case R.id.pdf_create:
			create_pdf();
			db_pdf();
			break;

		default:
			break;
		}
	}

	private List<HashMap<String, Object>> getPriorityList() {
		DatabaseHandler db = new DatabaseHandler(
				context.getApplicationContext());
		List<HashMap<String, Object>> priorityList = new ArrayList<HashMap<String, Object>>();
		List<Images> images = db.getAllImages();
		if (!images.isEmpty()) {
			for (Images im : images) {
				filename = im.getfilename();
				String filenameArray[] = filename.split("\\.");
				String extension = filenameArray[filenameArray.length - 1];
				if (extension.equals("jpg")) {
					HashMap<String, Object> map1 = new HashMap<String, Object>();
					map1.put("list_priority_img",
							im.getpath() + "/" + im.getfilename());
					map1.put("list_priority_value", im.getfilename());
					priorityList.add(map1);
				}

			}

		}
		return priorityList;
	}
	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}


}
