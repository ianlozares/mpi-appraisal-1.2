package com.gds.appraisalmaybank.land_improvements;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database2.Land_Improvements_API_Cost_Valuation;
import com.gds.appraisalmaybank.database2.Land_Improvements_API_Cost_Valuation_Details;
import com.gds.appraisalmaybank.database_new.Tables;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LI_Valuation_Cost_View extends Activity implements OnClickListener {
	DatabaseHandler2 db = new DatabaseHandler2(this);
	GDS_methods gds = new GDS_methods();
	Button btn_view_cost_details;
	Button btn_cancel, btn_save;

	Intent in;
	String record_id = "";
	private static final String TAG_RECORD_ID = "record_id";
	private static final String tag_cost_valuation_id = "cost_valuation_id";
	String cost_valuation_id = "", building_desc="";

	Session_Timer st = new Session_Timer(this);
	Dialog myDialog;

	EditText report_description, report_total_area,
			report_total_reproduction_cost, report_depreciation,
			report_less_reproduction, report_depreciated_value;

	// forListViewDeclarations
	ArrayList<HashMap<String, String>> itemList;

	DecimalFormat df = new DecimalFormat("0.00");// instead of #.00 to round off the decimal place

	EditText valrep_landimp_imp_value_phys_cur_unit_value, valrep_landimp_imp_value_phys_cur_amt,
			valrep_landimp_imp_value_func_cur_unit_value, valrep_landimp_imp_value_func_cur_amt,
			valrep_landimp_imp_value_less_cur;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reports_land_valuation_cost_view);

		st.resetDisconnectTimer();

		btn_view_cost_details = (Button) findViewById(R.id.btn_view_cost_details);
		btn_view_cost_details.setOnClickListener(this);
		btn_save = (Button) findViewById(R.id.btn_right);
		btn_save.setText("Save");
		btn_save.setOnClickListener(this);
		btn_cancel = (Button) findViewById(R.id.btn_left);
		btn_cancel.setText("Cancel");
		btn_cancel.setOnClickListener(this);

		report_description = (EditText) findViewById(R.id.report_description);
		report_total_area = (EditText) findViewById(R.id.report_total_area);
		report_total_reproduction_cost = (EditText) findViewById(R.id.report_total_reproduction_cost);
		report_depreciation = (EditText) findViewById(R.id.report_depreciation);
		report_less_reproduction = (EditText) findViewById(R.id.report_less_reproduction);
		report_depreciated_value = (EditText) findViewById(R.id.report_depreciated_value);

		valrep_landimp_imp_value_phys_cur_unit_value = (EditText) findViewById(R.id.valrep_landimp_imp_value_phys_cur_unit_value);
		valrep_landimp_imp_value_phys_cur_amt = (EditText) findViewById(R.id.valrep_landimp_imp_value_phys_cur_amt);
		valrep_landimp_imp_value_func_cur_unit_value = (EditText) findViewById(R.id.valrep_landimp_imp_value_func_cur_unit_value);
		valrep_landimp_imp_value_func_cur_amt = (EditText) findViewById(R.id.valrep_landimp_imp_value_func_cur_amt);
		valrep_landimp_imp_value_less_cur = (EditText) findViewById(R.id.valrep_landimp_imp_value_less_cur);

		// getting record_id from intent / previous class
		Intent i = getIntent();
		record_id = i.getStringExtra(TAG_RECORD_ID);
		cost_valuation_id = i.getStringExtra(tag_cost_valuation_id);

		fillData();

		String sub_total_cost_new;
		String grand_total_cost_new="0";
		List<Land_Improvements_API_Cost_Valuation_Details> lilvd = db.getLand_Improvements_Cost_Valuation_Details(record_id, cost_valuation_id);
		if (!lilvd.isEmpty()) {
			for (Land_Improvements_API_Cost_Valuation_Details imlvd : lilvd) {
				sub_total_cost_new = imlvd.getreport_imp_value_reproduction_cost();
				grand_total_cost_new = gds.addition(sub_total_cost_new, grand_total_cost_new);
			}
		}
		/*String report_depreciation_d = gds.nullCheck2(report_depreciation.getText().toString());

		String less_reproduction_val = gds.simpleAverage(grand_total_cost_new, report_depreciation_d);

		report_less_reproduction.setText(gds.numberFormat(less_reproduction_val));

		String depreciated_val_val = gds.subtraction(grand_total_cost_new, less_reproduction_val);

		report_depreciated_value.setText(gds.numberFormat(depreciated_val_val));*/



		valrep_landimp_imp_value_phys_cur_unit_value.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(valrep_landimp_imp_value_phys_cur_unit_value, this, s, current);
				comp_textwatch();
			}
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
			}
		});

		valrep_landimp_imp_value_phys_cur_amt.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(valrep_landimp_imp_value_phys_cur_amt, this, s, current);
//				comp_textwatch();
			}
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
			}
		});

		valrep_landimp_imp_value_func_cur_unit_value.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(valrep_landimp_imp_value_func_cur_unit_value, this, s, current);
				comp_textwatch();
			}
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
			}
		});

		valrep_landimp_imp_value_func_cur_amt.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(valrep_landimp_imp_value_func_cur_amt, this, s, current);
//				comp_textwatch();
			}
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
			}
		});

		valrep_landimp_imp_value_less_cur.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(valrep_landimp_imp_value_less_cur, this, s, current);
//				comp_textwatch();
			}
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
			}
		});

		report_depreciated_value.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_depreciated_value, this, s, current);
			}
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
			}
		});

		report_less_reproduction.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_less_reproduction, this, s, current);
			}
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
			}
		});

		//show required fields
		gds.fill_in_error(new EditText[]{report_description, report_total_area,report_total_reproduction_cost});
		//list of EditTexts put in array
		comp_textwatch();
	}

	void comp_textwatch(){
		//initial values
		String total_area = gds.nullCheck2(report_total_area.getText().toString());
		String reproduction_cost = gds.nullCheck2(report_total_reproduction_cost.getText().toString());
		String percent = gds.nullCheck2(report_depreciation.getText().toString());

		String value_phys_cur_unit_value = gds.nullCheck2(valrep_landimp_imp_value_phys_cur_unit_value.getText().toString());

		String value_func_cur_unit_value = gds.nullCheck2(valrep_landimp_imp_value_func_cur_unit_value.getText().toString());

		//comp
		String value_phys_cur_amt = gds.multiplication(total_area, value_phys_cur_unit_value);
		String value_func_cur_amt = gds.multiplication(total_area, value_func_cur_unit_value);

		String total_cur_amt = gds.addition(value_phys_cur_amt, value_func_cur_amt);

		String value_less_cur = gds.subtraction(reproduction_cost, total_cur_amt);

		//final
		valrep_landimp_imp_value_phys_cur_amt.setText(value_phys_cur_amt);
		valrep_landimp_imp_value_func_cur_amt.setText(value_func_cur_amt);
		valrep_landimp_imp_value_less_cur.setText(value_less_cur);

		String less_reproduction = gds.division( gds.multiplication(value_less_cur,percent) , "100" );

		report_less_reproduction.setText(less_reproduction);



		String depreciated_val_val = gds.subtraction(reproduction_cost, less_reproduction);

		report_depreciated_value.setText(gds.numberFormat(depreciated_val_val));

	}

	public void fillData(){
		List<Land_Improvements_API_Cost_Valuation> impDetailsList = db
				.getLand_Improvements_Cost_Valuation(String.valueOf(record_id), cost_valuation_id);
		if (!impDetailsList.isEmpty()) {
			for (Land_Improvements_API_Cost_Valuation li : impDetailsList) {
				report_description.setText(li.getreport_imp_value_description());
				report_total_area.setText(gds.numberFormat(li.getreport_imp_value_total_area()));
				report_total_reproduction_cost.setText(gds.numberFormat(li.getreport_imp_value_total_reproduction_cost()));
				report_depreciation.setText(gds.numberFormat(li.getreport_imp_value_depreciation()));
				report_less_reproduction.setText(gds.numberFormat(li.getreport_imp_value_less_reproduction()));
				report_depreciated_value.setText(gds.numberFormat(li.getreport_imp_value_depreciated_value()));
				valrep_landimp_imp_value_phys_cur_unit_value.setText(gds.numberFormat(li.getvalrep_landimp_imp_value_phys_cur_unit_value()));
				valrep_landimp_imp_value_phys_cur_amt.setText(gds.numberFormat(li.getvalrep_landimp_imp_value_phys_cur_amt()));
				valrep_landimp_imp_value_func_cur_unit_value.setText(gds.numberFormat(li.getvalrep_landimp_imp_value_func_cur_unit_value()));
				valrep_landimp_imp_value_func_cur_amt.setText(gds.numberFormat(li.getvalrep_landimp_imp_value_func_cur_amt()));
				valrep_landimp_imp_value_less_cur.setText(gds.numberFormat(li.getvalrep_landimp_imp_value_less_cur()));
			}
		}
	}
	public void save_db(){
		comp_textwatch();
		//update data in SQLite
		Land_Improvements_API_Cost_Valuation li = new Land_Improvements_API_Cost_Valuation();
		li.setreport_imp_value_description(report_description.getText().toString());
		li.setreport_imp_value_total_area(gds.replaceFormat(report_total_area.getText().toString()));
		li.setreport_imp_value_total_reproduction_cost(gds.replaceFormat(report_total_reproduction_cost.getText().toString()));
		li.setreport_imp_value_depreciation(gds.replaceFormat(report_depreciation.getText().toString()));
		li.setreport_imp_value_less_reproduction(gds.replaceFormat(report_less_reproduction.getText().toString()));
		li.setreport_imp_value_depreciated_value(gds.replaceFormat(report_depreciated_value.getText().toString()));

		li.setvalrep_landimp_imp_value_phys_cur_unit_value(gds.replaceFormat(valrep_landimp_imp_value_phys_cur_unit_value.getText().toString()));
		li.setvalrep_landimp_imp_value_phys_cur_amt(gds.replaceFormat(valrep_landimp_imp_value_phys_cur_amt.getText().toString()));
		li.setvalrep_landimp_imp_value_func_cur_unit_value(gds.replaceFormat(valrep_landimp_imp_value_func_cur_unit_value.getText().toString()));
		li.setvalrep_landimp_imp_value_func_cur_amt(gds.replaceFormat(valrep_landimp_imp_value_func_cur_amt.getText().toString()));
		li.setvalrep_landimp_imp_value_less_cur(gds.replaceFormat(valrep_landimp_imp_value_less_cur.getText().toString()));

		db.updateLand_Improvements_Cost_Valuation(li, record_id, String.valueOf(cost_valuation_id));


		ArrayList<String> values = new ArrayList();
		ArrayList<String> fields = new ArrayList();
		values.clear();
		fields.clear();
		values.add(gds.replaceFormat(report_total_area.getText().toString()));
		fields.add("report_impsummary1_fa");
		db.updateRecord(values, fields, "record_id = ? and imp_details_id = ?", new String[]{record_id,cost_valuation_id}, Tables.land_improvements_imp_details.table_name);

		db.close();
		Toast.makeText(getApplicationContext(), "Saved",Toast.LENGTH_SHORT).show();
		reloadPrevClass();
	}

	public void save_db_id(){

		if (report_total_reproduction_cost.getText().length() == 0) {
			report_total_reproduction_cost.setText("0");
		}
		if (report_total_area.getText().length() == 0) {
			report_total_area.setText("0");
		}

			//update data in SQLite
			Land_Improvements_API_Cost_Valuation li = new Land_Improvements_API_Cost_Valuation();
			li.setreport_imp_value_description(report_description.getText().toString());
			li.setreport_imp_value_total_area(gds.replaceFormat(report_total_area.getText().toString()));
			li.setreport_imp_value_total_reproduction_cost(gds.replaceFormat(report_total_reproduction_cost.getText().toString()));
			li.setreport_imp_value_depreciation(gds.replaceFormat(report_depreciation.getText().toString()));
			li.setreport_imp_value_less_reproduction(gds.replaceFormat(report_less_reproduction.getText().toString()));
			li.setreport_imp_value_depreciated_value(gds.replaceFormat(report_depreciated_value.getText().toString()));

			li.setvalrep_landimp_imp_value_phys_cur_unit_value(gds.replaceFormat(valrep_landimp_imp_value_phys_cur_unit_value.getText().toString()));
			li.setvalrep_landimp_imp_value_phys_cur_amt(gds.replaceFormat(valrep_landimp_imp_value_phys_cur_amt.getText().toString()));
			li.setvalrep_landimp_imp_value_func_cur_unit_value(gds.replaceFormat(valrep_landimp_imp_value_func_cur_unit_value.getText().toString()));
			li.setvalrep_landimp_imp_value_func_cur_amt(gds.replaceFormat(valrep_landimp_imp_value_func_cur_amt.getText().toString()));
			li.setvalrep_landimp_imp_value_less_cur(gds.replaceFormat(valrep_landimp_imp_value_less_cur.getText().toString()));

			db.updateLand_Improvements_Cost_Valuation(li, record_id, String.valueOf(cost_valuation_id));
		ArrayList<String> values = new ArrayList();
		ArrayList<String> fields = new ArrayList();
		values.clear();
		fields.clear();
		values.add(gds.replaceFormat(report_total_area.getText().toString()));
		fields.add("report_impsummary1_fa");
		db.updateRecord(values, fields, "record_id = ? and imp_details_id = ?", new String[]{record_id,cost_valuation_id}, Tables.land_improvements_imp_details.table_name);

		db.close();
			//Toast.makeText(getApplicationContext(), "Saved",Toast.LENGTH_SHORT).show();


	}
	public void reloadPrevClass(){
		// successfully updated
		finish();
		Intent in = new Intent(getApplicationContext(),
				LI_Valuation_Cost.class);
		in.putExtra(TAG_RECORD_ID, record_id);
		startActivityForResult(in, 100);

	}
	//from Land_Imp_Valuation_Cost_Details class (deeper class)
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// if result code 100
		if (resultCode == 100) {
			// if result code 100 is received 
			// means user edited/deleted product
			// reload this screen again
			Intent intent = getIntent();
			finish();
			startActivity(intent);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		save_db();
		/*Intent in = new Intent(getApplicationContext(),
				LI_Valuation_Cost.class);
		in.putExtra(TAG_RECORD_ID, record_id);
		startActivityForResult(in, 100);*/
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
			case R.id.btn_view_cost_details:
				save_db_id();

					// Starting new intent
					in = new Intent(getApplicationContext(),
							LI_Valuation_Cost_Details.class);
					in.putExtra(TAG_RECORD_ID, record_id);
					in.putExtra(tag_cost_valuation_id, cost_valuation_id);
					//Toast.makeText(getApplicationContext(), ""+record_id+"\n"+cost_valuation_id,Toast.LENGTH_SHORT).show();
					// starting new activity and expecting some response back
					startActivityForResult(in, 100);
					finish();

				break;
			case R.id.btn_right://save
				save_db();
				break;
			case R.id.btn_left://cancel
				finish();
				in = new Intent(getApplicationContext(),
						LI_Valuation_Cost.class);
				in.putExtra(TAG_RECORD_ID, record_id);
				startActivityForResult(in, 100);
				break;

			default:
				break;
		}
	}
	// EditText checker
	private boolean validate(EditText[] fields) {
		for (int i = 0; i < fields.length; i++) {
			EditText currentField = fields[i];
			if (currentField.getText().toString().length() <= 0) {
				return false;
			}
		}
		return true;
	}
	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}
}