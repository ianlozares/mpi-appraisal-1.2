package com.gds.appraisalmaybank.land_improvements;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Land_Improvements_API;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database2.Land_Improvements_API_Cost_Valuation;
import com.gds.appraisalmaybank.database_new.Tables;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SuppressLint("InflateParams")
public class LI_Valuation_Cost extends ListActivity implements OnClickListener {
	DatabaseHandler2 db = new DatabaseHandler2(this);
	DatabaseHandler dbMain = new DatabaseHandler(this);
	GDS_methods gds = new GDS_methods();
	Button btn_add_valuation, btn_save_valuation;
	Button btn_create, btn_cancel, btn_save;
	Button btn_compute;
	EditText report_total_imp_value;
	String record_id="";
	private static final String TAG_RECORD_ID = "record_id";

	Session_Timer st = new Session_Timer(this);
	Dialog myDialog;
	//create dialog
	EditText report_description, report_total_area,
			report_total_reproduction_cost, report_depreciation,
			report_less_reproduction, report_depreciated_value;

	//forListViewDeclarations
	private ProgressDialog pDialog;
	ArrayList<HashMap<String, String>> itemList;
	private static final String tag_cost_valuation_id = "cost_valuation_id";
	private static final String tag_description = "description";
	private static final String tag_total_area = "total_area";
	private static final String tag_total_reproduction_cost = "total_reproduction_cost";
	private static final String tag_depreciation = "depreciation";
	private static final String tag_less_reproduction = "less_reproduction";
	private static final String tag_depreciated_value = "depreciated_value";

	String cost_valuation_id="";
	String description="", total_area="", total_reproduction_cost="", depreciation="", less_reproduction="",
			depreciated_value="";
	//for computation variables
	DecimalFormat df = new DecimalFormat("#.00");
	String sub_depreciated_value="";
	String grand_total_depreciated_value="0";

	String data_grand_total_d="0", cost_grand_total_d="0", final_grand_total_d="0";

	EditText valrep_landimp_imp_value_phys_cur_unit_value, valrep_landimp_imp_value_phys_cur_amt,
			valrep_landimp_imp_value_func_cur_unit_value, valrep_landimp_imp_value_func_cur_amt,
			valrep_landimp_imp_value_less_cur;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reports_land_valuation_cost);

		st.resetDisconnectTimer();
		report_total_imp_value = (EditText) findViewById(R.id.report_total_imp_value);

		btn_compute = (Button) findViewById(R.id.btn_compute);
		btn_compute.setOnClickListener(this);

		btn_add_valuation = (Button) findViewById(R.id.btn_add_valuation);
		btn_add_valuation.setOnClickListener(this);

		btn_add_valuation.setVisibility(View.GONE);

		btn_save_valuation = (Button) findViewById(R.id.btn_save_valuation);
		btn_save_valuation.setOnClickListener(this);

		Intent i = getIntent();
		record_id = i.getStringExtra(TAG_RECORD_ID);
		fillTotal();
		//list
		itemList = new ArrayList<HashMap<String, String>>();
		// Loading in Background Thread
		new LoadAll().execute();
		// Get listview
		ListView lv = getListView();

		// on seleting single item
		// launching Edit item Screen
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {
				// getting values from selected ListItem
				cost_valuation_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
				finish();
				Intent in = new Intent(getApplicationContext(),
						LI_Valuation_Cost_View.class);
				in.putExtra(TAG_RECORD_ID, record_id);
				in.putExtra(tag_cost_valuation_id, cost_valuation_id);
				startActivityForResult(in, 100);
			}
		});
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

			public boolean onItemLongClick(AdapterView<?> parent, View view,
										   int position, long id) {
				// TODO Auto-generated method stub

				Log.v("long clicked", "pos: " + position);
				cost_valuation_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
				delete_item();
				return true;
			}
		});
		compute_total();



	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
			case R.id.btn_add_valuation:
				add_new();
				break;
			case R.id.btn_save_valuation:
				cost_grand_total_d = gds.nullCheck2(report_total_imp_value.getText().toString());
				final_grand_total_d = gds.addition(data_grand_total_d,cost_grand_total_d);
				//ADDED BY IAN
				//save to valuation cost
				Land_Improvements_API valTotal = new Land_Improvements_API();
				valTotal.setreport_imp_value_total_imp_value(gds.replaceFormat(report_total_imp_value.getText().toString()));
				dbMain.updateLand_Improvements_Total_Valuation_Main(valTotal ,record_id);

				//save to summary
				Land_Improvements_API liTotal = new Land_Improvements_API();
				liTotal.setreport_final_value_total_appraised_value_land_imp(gds.replaceFormat(final_grand_total_d));
				dbMain.updateLand_Improvements_Summary_Total(liTotal ,record_id);

				//END OF ADDED BY IAN
				finish();
				break;
			default:
				break;
		}
	}

	public void fillTotal(){
		List<Land_Improvements_API> propDescList = dbMain.getLand_Improvements(String.valueOf(record_id));
		if (!propDescList.isEmpty()) {
			for (Land_Improvements_API li : propDescList) {
				report_total_imp_value.setText(gds.numberFormat(li.getreport_imp_value_total_imp_value()));
				data_grand_total_d = li.getreport_value_total_landimp_value();
			}
		}
	}

	public void compute_total(){

		List<Land_Improvements_API_Cost_Valuation> lild = db.getLand_Improvements_Cost_Valuation(record_id);
		if (!lild.isEmpty()) {
			for (Land_Improvements_API_Cost_Valuation imld : lild) {
				if (imld.getreport_imp_value_depreciated_value().equals("")){
					//Toast.makeText(getApplicationContext(), "Please fill up all required data to get the right total Valuation Cost",Toast.LENGTH_SHORT).show();
					sub_depreciated_value = "0.00";
				} else {
					sub_depreciated_value = imld.getreport_imp_value_depreciated_value();
				}
				grand_total_depreciated_value = gds.addition(sub_depreciated_value,grand_total_depreciated_value);
			}

		}
		report_total_imp_value.setText(gds.numberFormat(grand_total_depreciated_value));
	}

	public void add_new() {
		Toast.makeText(getApplicationContext(), "Add New",
				Toast.LENGTH_SHORT).show();
		myDialog = new Dialog(LI_Valuation_Cost.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.reports_land_valuation_cost_form);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);

		btn_create = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_create.setText("Create");
		btn_cancel.setText("Cancel");
		report_description = (EditText) myDialog.findViewById(R.id.report_description);
		report_total_area = (EditText) myDialog.findViewById(R.id.report_total_area);
		report_total_reproduction_cost = (EditText) myDialog.findViewById(R.id.report_total_reproduction_cost);
		report_depreciation = (EditText) myDialog.findViewById(R.id.report_depreciation);
		report_less_reproduction = (EditText) myDialog.findViewById(R.id.report_less_reproduction);
		report_depreciated_value = (EditText) myDialog.findViewById(R.id.report_depreciated_value);

		valrep_landimp_imp_value_phys_cur_unit_value = (EditText) findViewById(R.id.valrep_landimp_imp_value_phys_cur_unit_value);
		valrep_landimp_imp_value_phys_cur_amt = (EditText) findViewById(R.id.valrep_landimp_imp_value_phys_cur_amt);
		valrep_landimp_imp_value_func_cur_unit_value = (EditText) findViewById(R.id.valrep_landimp_imp_value_func_cur_unit_value);
		valrep_landimp_imp_value_func_cur_amt = (EditText) findViewById(R.id.valrep_landimp_imp_value_func_cur_amt);
		valrep_landimp_imp_value_less_cur = (EditText) findViewById(R.id.valrep_landimp_imp_value_less_cur);




		btn_create.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				//add data in SQLite
				db.addLand_Improvements_Cost_Valuation(new Land_Improvements_API_Cost_Valuation(
						record_id,
						report_description.getText().toString(),
						report_total_area.getText().toString(),
						report_total_reproduction_cost.getText().toString(),
						report_depreciation.getText().toString(),
						report_less_reproduction.getText().toString(),
						report_depreciated_value.getText().toString(),//
						valrep_landimp_imp_value_phys_cur_unit_value.getText().toString(),
						valrep_landimp_imp_value_phys_cur_amt.getText().toString(),
						valrep_landimp_imp_value_func_cur_unit_value.getText().toString(),
						valrep_landimp_imp_value_func_cur_amt.getText().toString(),
						valrep_landimp_imp_value_less_cur.getText().toString()
				));
				Toast.makeText(getApplicationContext(),
						"data created", Toast.LENGTH_SHORT).show();
				myDialog.dismiss();
				reloadClass();

			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		gds.fill_in_error(new EditText[]{report_description, report_total_area,
				report_total_reproduction_cost});

		myDialog.show();
	}

	public void view_details(){
		myDialog = new Dialog(LI_Valuation_Cost.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.reports_land_valuation_cost_view);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);

		btn_save = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_save.setText("Save");
		btn_cancel.setText("Cancel");
		report_description = (EditText) myDialog.findViewById(R.id.report_description);
		report_total_area = (EditText) myDialog.findViewById(R.id.report_total_area);
		report_total_reproduction_cost = (EditText) myDialog.findViewById(R.id.report_total_reproduction_cost);
		report_depreciation = (EditText) myDialog.findViewById(R.id.report_depreciation);
		report_less_reproduction = (EditText) myDialog.findViewById(R.id.report_less_reproduction);
		report_depreciated_value = (EditText) myDialog.findViewById(R.id.report_depreciated_value);

		List<Land_Improvements_API_Cost_Valuation> propDescList = db
				.getLand_Improvements_Cost_Valuation(String.valueOf(record_id), String.valueOf(cost_valuation_id));
		if (!propDescList.isEmpty()) {
			for (Land_Improvements_API_Cost_Valuation li : propDescList) {
				report_description.setText(li.getreport_imp_value_description());
				report_total_area.setText(li.getreport_imp_value_total_area());
				report_total_reproduction_cost.setText(li.getreport_imp_value_total_reproduction_cost());
				report_depreciation.setText(li.getreport_imp_value_depreciation());
				report_less_reproduction.setText(li.getreport_imp_value_less_reproduction());
				report_depreciated_value.setText(li.getreport_imp_value_depreciated_value());

			}
		}



		btn_save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				//update data in SQLite
				Land_Improvements_API_Cost_Valuation li = new Land_Improvements_API_Cost_Valuation();
				li.setreport_imp_value_description(report_description.getText().toString());
				li.setreport_imp_value_total_area(report_total_area.getText().toString());
				li.setreport_imp_value_total_reproduction_cost(report_total_reproduction_cost.getText().toString());
				li.setreport_imp_value_depreciation(report_depreciation.getText().toString());
				li.setreport_imp_value_less_reproduction(report_less_reproduction.getText().toString());
				li.setreport_imp_value_depreciated_value(report_depreciated_value.getText().toString());

				db.updateLand_Improvements_Cost_Valuation(li, record_id, cost_valuation_id);
				ArrayList<String> values = new ArrayList();
				ArrayList<String> fields = new ArrayList();
				values.clear();
				fields.clear();
				values.add(gds.replaceFormat(report_total_area.getText().toString()));
				fields.add("report_impsummary1_fa");
				db.updateRecord(values, fields, "record_id = ? and imp_details_id = ?", new String[]{record_id,cost_valuation_id}, Tables.land_improvements_imp_details.table_name);

				db.close();
				myDialog.dismiss();
				Toast.makeText(getApplicationContext(), "Saved",Toast.LENGTH_SHORT).show();
				reloadClass();

			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		gds.fill_in_error(new EditText[]{report_description, report_total_area,
				report_total_reproduction_cost});
		myDialog.show();
	}

	//EditText checker
	private boolean validate(EditText[] fields){
		for(int i=0; i<fields.length; i++){
			EditText currentField=fields[i];
			if(currentField.getText().toString().length()<=0){
				return false;
			}
		}
		return true;
	}
	public void reloadClass(){
		Intent intent = getIntent();
		finish();
		startActivity(intent);
	}
	public void delete_item(){
		myDialog = new Dialog(LI_Valuation_Cost.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.delete_dialog);
		myDialog.setCancelable(true);
		final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
		final Button btn_yes = (Button) myDialog.findViewById(R.id.btn_yes);
		final Button btn_no = (Button) myDialog.findViewById(R.id.btn_no);
		tv_question.setText("Are you sure you want to delete the selected item?");
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				db.deleteRecord(Tables.land_improvements_imp_details.table_name, "record_id = ? AND imp_details_id = ?", new String[] { record_id, cost_valuation_id});

				db.deleteLand_Improvements_Cost_Valuation_Details(record_id, cost_valuation_id);
				db.deleteLand_Improvements_Cost_Valuation(record_id, cost_valuation_id);
				db.close();

				//ADDED BY IAN
				//total land value
				String grand_total_land_val =gds.nullCheck2(db.getLand_Improvements_API_Total_Land_Value_Lot_Valuation_Details(record_id));
				String grand_total_land_val_s = gds.stringToDecimal(grand_total_land_val);
				//total imp value
				String total_imp_value =gds.nullCheck2(dbMain.getLand_Improvements_API_Total_imp_value(record_id));
				String total_imp_value_s = gds.stringToDecimal(total_imp_value);
				String final_grand_val = gds.addition(grand_total_land_val_s,total_imp_value_s);
				//update Summary
				Land_Improvements_API vl4 = new Land_Improvements_API();
				vl4.setreport_final_value_total_appraised_value_land_imp(final_grand_val);
				dbMain.updateLand_Improvements_Summary_Total(vl4, record_id);
				dbMain.close();
				//END ADDED BY IAN


				myDialog.dismiss();
				reloadClass();
				Toast.makeText(getApplicationContext(), "Data Deleted",Toast.LENGTH_SHORT).show();
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});

		myDialog.show();
	}

	// Response from LI_Valuation_Cost_View Activity
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// if result code 100
		if (resultCode == 100) {
			// if result code 100 is received 
			// means user edited/deleted product
			// reload this screen again
			Intent intent = getIntent();
			finish();
			startActivity(intent);
		}
	}

	/**
	 * Background Async Task to Load all
	 * */
	class LoadAll extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(LI_Valuation_Cost.this);
			pDialog.setMessage("Loading Lists. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All
		 * */
		protected String doInBackground(String... args) {
			List<Land_Improvements_API_Cost_Valuation> lild = db.getLand_Improvements_Cost_Valuation(record_id);
			if (!lild.isEmpty()) {
				for (Land_Improvements_API_Cost_Valuation imld : lild) {
					cost_valuation_id = String.valueOf(imld.getID());
					description = imld.getreport_imp_value_description();
					total_area = gds.numberFormat(imld.getreport_imp_value_total_area());
					total_reproduction_cost = gds.numberFormat(imld.getreport_imp_value_total_reproduction_cost());
					depreciation = gds.numberFormat(imld.getreport_imp_value_depreciation());
					less_reproduction = gds.numberFormat(imld.getreport_imp_value_less_reproduction());
					depreciated_value = gds.numberFormat(imld.getreport_imp_value_depreciated_value());

					HashMap<String, String> map = new HashMap<String, String>();
					map.put(tag_cost_valuation_id, cost_valuation_id);
					map.put(tag_description, description);
					map.put(tag_total_area, total_area);
					map.put(tag_total_reproduction_cost, total_reproduction_cost);
					map.put(tag_depreciation, depreciation);
					map.put(tag_less_reproduction, less_reproduction);
					map.put(tag_depreciated_value, depreciated_value);

					// adding HashList to ArrayList
					itemList.add(map);
				}
			} else {//return to prev class
				Intent in = new Intent(getApplicationContext(),Land_Improvements.class);
				startActivityForResult(in, 100);
				finish();
			}
			return null;
		}


		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					ListAdapter adapter = new SimpleAdapter(
							LI_Valuation_Cost.this, itemList,
							R.layout.reports_land_valuation_cost_list, new String[] { tag_cost_valuation_id, tag_description,
							tag_total_area, tag_total_reproduction_cost, tag_depreciation,
							tag_less_reproduction, tag_depreciated_value},
							new int[] { R.id.primary_key, R.id.item_desc, R.id.item_total_area,
									R.id.item_total_reproduction_cost, R.id.item_total_depreciation,
									R.id.item_less_reproduction, R.id.item_depreciated_value});
					// updating listview
					setListAdapter(adapter);
				}
			});

		}
	}

	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}


}