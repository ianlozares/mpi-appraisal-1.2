package com.gds.appraisalmaybank.land_improvements;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database2.Land_Improvements_API_Cost_Valuation;
import com.gds.appraisalmaybank.database2.Land_Improvements_API_Cost_Valuation_Details;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LI_Valuation_Cost_Details extends ListActivity implements OnClickListener {
	DatabaseHandler2 db = new DatabaseHandler2(this);
	GDS_methods gds = new GDS_methods();
	Button btn_add_imp_details, btn_save_imp_details;
	Button btn_create, btn_cancel, btn_save, btn_delete;
	Button btn_compute;

	Session_Timer st = new Session_Timer(this);
	String record_id="";
	private static final String tag_cost_valuation_details_id = "cost_valuation_details_id";
	private static final String TAG_RECORD_ID = "record_id";
	private static final String tag_cost_valuation_id = "cost_valuation_id";
	private static final String tag_kind = "kind";
	private static final String tag_area = "area";
	private static final String tag_cost_per_sqm = "cost_per_sqm";
	private static final String tag_reproduction_cost = "reproduction_cost";
	String cost_valuation_id = "", cost_valuation_details_id = "", kind = "",
			area = "", cost_per_sqm = "", reproduction_cost = "";
	//for computation variables
	DecimalFormat df = new DecimalFormat("#.00");
	String sub_total_area="", sub_total_cost="", sub_total_cost_new="";
	String grand_total_area="0", grand_total_cost="0", grand_total_cost_new="0";

	Dialog myDialog;
	//create dialog
	EditText report_imp_value_kind, report_imp_value_area,
			report_imp_value_cost_per_sqm, report_imp_value_reproduction_cost;

	//forListViewDeclarations
	private ProgressDialog pDialog;
	ArrayList<HashMap<String, String>> itemList;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reports_land_valuation_cost_details);

		st.resetDisconnectTimer();
		btn_add_imp_details = (Button) findViewById(R.id.btn_add_imp_details);
		btn_add_imp_details.setOnClickListener(this);

		btn_save_imp_details = (Button) findViewById(R.id.btn_save_imp_details);
		btn_save_imp_details.setOnClickListener(this);

		// getting record_id from intent / previous class
		Intent i = getIntent();
		record_id = i.getStringExtra(TAG_RECORD_ID);
		cost_valuation_id = i.getStringExtra(tag_cost_valuation_id);
		//list
		itemList = new ArrayList<HashMap<String, String>>();
		new LoadAll().execute();
		// Get listview
		ListView lv = getListView();

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {
				// getting values from selected ListItem
				cost_valuation_details_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
				view_details();
			}
		});
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

			public boolean onItemLongClick(AdapterView<?> parent, View view,
										   int position, long id) {
				// TODO Auto-generated method stub

				Log.v("long clicked","pos: " + position);
				cost_valuation_details_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
				delete_item();
				return true;
			}
		});

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		save_cost_approach();
		Intent in = new Intent(getApplicationContext(),
				LI_Valuation_Cost_View.class);
		in.putExtra(TAG_RECORD_ID, record_id);
		in.putExtra(tag_cost_valuation_id, cost_valuation_id);
		startActivityForResult(in, 100);
		finish();
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
			case R.id.btn_add_imp_details:
				add_new();
				break;
			case R.id.btn_save_imp_details:
				save_cost_approach();
				Intent in = new Intent(getApplicationContext(),
						LI_Valuation_Cost_View.class);
				in.putExtra(TAG_RECORD_ID, record_id);
				in.putExtra(tag_cost_valuation_id, cost_valuation_id);
				startActivityForResult(in, 100);
				finish();
				break;
			default:
				break;
		}
	}

	public void add_new(){
		Toast.makeText(getApplicationContext(), "Add New",Toast.LENGTH_SHORT).show();
		myDialog = new Dialog(LI_Valuation_Cost_Details.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.reports_land_valuation_cost_details_form);
		myDialog.setCancelable(false);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);

		btn_compute = (Button) myDialog.findViewById(R.id.btn_compute);
		btn_create = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_create.setText("Create");
		btn_cancel.setText("Cancel");

		report_imp_value_kind = (EditText) myDialog.findViewById(R.id.report_imp_value_kind);
		report_imp_value_area = (EditText) myDialog.findViewById(R.id.report_imp_value_area);
		report_imp_value_cost_per_sqm = (EditText) myDialog.findViewById(R.id.report_imp_value_cost_per_sqm);
		report_imp_value_reproduction_cost = (EditText) myDialog.findViewById(R.id.report_imp_value_reproduction_cost);


		report_imp_value_area.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_imp_value_area, this, s, current);

				String area_val_s = gds.nullCheck2(report_imp_value_area.getText().toString());
				String cost_val_s = gds.nullCheck2(report_imp_value_cost_per_sqm.getText().toString());
				String cost_new_val_s = gds.round(gds.multiplication(area_val_s,cost_val_s));
				report_imp_value_reproduction_cost.setText(cost_new_val_s);


			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		//innnitialize 0 to avoid being mandatory - mark 082217
		if (report_imp_value_cost_per_sqm.getText().toString().length()==0) {
			report_imp_value_cost_per_sqm.setText("0.00");
		}
		report_imp_value_cost_per_sqm.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_imp_value_cost_per_sqm, this, s, current);

				String area_val_s = gds.nullCheck2(report_imp_value_area.getText().toString());
				String cost_val_s = gds.nullCheck2(report_imp_value_cost_per_sqm.getText().toString());
				String cost_new_val_s = gds.round(gds.multiplication(area_val_s,cost_val_s));
				report_imp_value_reproduction_cost.setText(cost_new_val_s);


			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_imp_value_reproduction_cost.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_imp_value_reproduction_cost, this, s, current);
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});

		btn_create.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {


					//add data in SQLite
					db.addLand_Improvements_Cost_Valuation_Details(new Land_Improvements_API_Cost_Valuation_Details(
							record_id,
							cost_valuation_id,
							report_imp_value_kind.getText().toString(),
							gds.replaceFormat(report_imp_value_area.getText().toString()),
							gds.replaceFormat(report_imp_value_cost_per_sqm.getText().toString()),
							gds.replaceFormat(report_imp_value_reproduction_cost.getText().toString())));
					Toast.makeText(getApplicationContext(),
							"data created", Toast.LENGTH_SHORT).show();
					myDialog.dismiss();
					reloadClass();


			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		gds.fill_in_error(new EditText[] {
				report_imp_value_kind, report_imp_value_area,
				report_imp_value_cost_per_sqm});
		myDialog.show();
	}

	public void view_details(){
		myDialog = new Dialog(LI_Valuation_Cost_Details.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.reports_land_valuation_cost_details_form);
		myDialog.setCancelable(false);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);


		btn_save = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_save.setText("Save");
		btn_cancel.setText("Cancel");
		report_imp_value_kind = (EditText) myDialog.findViewById(R.id.report_imp_value_kind);
		report_imp_value_area = (EditText) myDialog.findViewById(R.id.report_imp_value_area);
		report_imp_value_cost_per_sqm = (EditText) myDialog.findViewById(R.id.report_imp_value_cost_per_sqm);
		report_imp_value_reproduction_cost = (EditText) myDialog.findViewById(R.id.report_imp_value_reproduction_cost);

		List<Land_Improvements_API_Cost_Valuation_Details> lotDetailsFeaturesList = db
				.getLand_Improvements_Cost_Valuation_Details_Single(String.valueOf(record_id), String.valueOf(cost_valuation_details_id));
		if (!lotDetailsFeaturesList.isEmpty()) {
			for (Land_Improvements_API_Cost_Valuation_Details li : lotDetailsFeaturesList) {
				report_imp_value_kind.setText(li.getreport_imp_value_kind());
				report_imp_value_area.setText(gds.numberFormat(li.getreport_imp_value_area()));
				report_imp_value_cost_per_sqm.setText(gds.numberFormat(li.getreport_imp_value_cost_per_sqm()));
				report_imp_value_reproduction_cost.setText(gds.numberFormat(li.getreport_imp_value_reproduction_cost()));
			}
		}

		report_imp_value_area.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_imp_value_area, this, s, current);

				String area_val = gds.nullCheck2(report_imp_value_area.getText().toString());
				String cost_val = gds.nullCheck2(report_imp_value_cost_per_sqm.getText().toString());
				String cost_new_val = gds.round(gds.multiplication(area_val,cost_val));
				report_imp_value_reproduction_cost.setText(cost_new_val);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_imp_value_cost_per_sqm.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_imp_value_cost_per_sqm, this, s, current);

				String area_val = gds.nullCheck2(report_imp_value_area.getText().toString());
				String cost_val = gds.nullCheck2(report_imp_value_cost_per_sqm.getText().toString());
				String cost_new_val = gds.round(gds.multiplication(area_val,cost_val));
				report_imp_value_reproduction_cost.setText(cost_new_val);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_imp_value_reproduction_cost.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub


				current = gds.numberFormat(report_imp_value_reproduction_cost, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});


		btn_save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

					//update data in SQLite
					Land_Improvements_API_Cost_Valuation_Details li = new Land_Improvements_API_Cost_Valuation_Details();
					li.setreport_imp_value_kind(report_imp_value_kind.getText().toString());
					li.setreport_imp_value_area(gds.replaceFormat(report_imp_value_area.getText().toString()));
					li.setreport_imp_value_cost_per_sqm(gds.replaceFormat(report_imp_value_cost_per_sqm.getText().toString()));
					li.setreport_imp_value_reproduction_cost(gds.replaceFormat(report_imp_value_reproduction_cost.getText().toString()));

					db.updateLand_Improvements_Cost_Valuation_Details(li, record_id, cost_valuation_details_id);
					db.close();
					myDialog.dismiss();
					Toast.makeText(getApplicationContext(), "Saved",Toast.LENGTH_SHORT).show();
					reloadClass();

			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});

		gds.fill_in_error(new EditText[] {
				report_imp_value_kind, report_imp_value_area,
				report_imp_value_cost_per_sqm});
		myDialog.show();
	}

	//EditText checker
	private boolean validate(EditText[] fields){
		for(int i=0; i<fields.length; i++){
			EditText currentField=fields[i];
			if(currentField.getText().toString().length()<=0){
				return false;
			}
		}
		return true;
	}
	public void reloadClass(){
		Intent intent = getIntent();
		finish();
		startActivity(intent);
	}
	public void delete_item(){
		myDialog = new Dialog(LI_Valuation_Cost_Details.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.delete_dialog);
		myDialog.setCancelable(false);
		final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
		final Button btn_yes = (Button) myDialog.findViewById(R.id.btn_yes);
		final Button btn_no = (Button) myDialog.findViewById(R.id.btn_no);
		tv_question.setText("Are you sure you want to delete the selected item?");
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				db.deleteLand_Improvements_Cost_Valuation_Details_Single(record_id, cost_valuation_details_id);
				db.close();
				myDialog.dismiss();
				reloadClass();
				Toast.makeText(getApplicationContext(), "Data Deleted",
						Toast.LENGTH_SHORT).show();
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});

		myDialog.show();
	}

	public void save_cost_approach(){

		sub_total_area="0"; sub_total_cost="0"; sub_total_cost_new="0";//to prevent x2 total
		//grand_total_area=0; grand_total_cost=0; grand_total_cost_new=0;//to prevent x2 total

		//autocompute
		List<Land_Improvements_API_Cost_Valuation_Details> lilvd = db.getLand_Improvements_Cost_Valuation_Details(record_id, cost_valuation_id);
		if (!lilvd.isEmpty()) {
			for (Land_Improvements_API_Cost_Valuation_Details imlvd : lilvd) {
				sub_total_area = imlvd.getreport_imp_value_area();
				//sub_total_cost = imlvd.getreport_imp_value_cost_per_sqm();
				sub_total_cost_new = imlvd.getreport_imp_value_reproduction_cost();
				//auto-add
				grand_total_area = gds.addition(sub_total_area, grand_total_area);
				//grand_total_cost = gds.StringtoDouble(sub_total_cost) + grand_total_cost;
				grand_total_cost_new = gds.addition(sub_total_cost_new,grand_total_cost_new);
			}
		}


		Land_Improvements_API_Cost_Valuation li = new Land_Improvements_API_Cost_Valuation();
		li.setreport_imp_value_total_area(gds.replaceFormat(grand_total_area));
		li.setreport_imp_value_total_reproduction_cost(gds.replaceFormat(grand_total_cost_new));

		db.updateLand_Improvements_Cost_Valuation_2(li, record_id, String.valueOf(cost_valuation_id));

		ArrayList<String>field = new ArrayList<String>();
		field.clear();
		field.add("report_impsummary1_fa");

		ArrayList<String> value = new ArrayList<String>();
		value.clear();
		value.add(gds.replaceFormat(gds.nullCheck(grand_total_area)));
		db.updateRecord(value, field, "record_id = ? and imp_details_id = ?", new String[] { record_id,cost_valuation_id }, "land_improvements_imp_details");
		/*db.close();*/

	}

	// Response from LI_Desc_Of_Imp_View Activity
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// if result code 100
		if (resultCode == 100) {
			// if result code 100 is received 
			// means user edited/deleted product
			// reload this screen again
			Intent intent = getIntent();
			finish();
			startActivity(intent);
		}

	}

	/**
	 * Background Async Task to Load all 
	 * */
	class LoadAll extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(LI_Valuation_Cost_Details.this);
			pDialog.setMessage("Loading List. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All 
		 * */
		protected String doInBackground(String... args) {
			List<Land_Improvements_API_Cost_Valuation_Details> liidf = db.getLand_Improvements_Cost_Valuation_Details(record_id, cost_valuation_id);
			if (!liidf.isEmpty()) {
				for (Land_Improvements_API_Cost_Valuation_Details imidf : liidf) {
					cost_valuation_details_id = String.valueOf(imidf.getID());
					record_id = imidf.getrecord_id();
					cost_valuation_id = imidf.getimp_valuation_id();
					kind = imidf.getreport_imp_value_kind();
					area = gds.numberFormat(imidf.getreport_imp_value_area());
					cost_per_sqm = gds.numberFormat(imidf.getreport_imp_value_cost_per_sqm());
					reproduction_cost = gds.numberFormat(imidf.getreport_imp_value_reproduction_cost());

					HashMap<String, String> map = new HashMap<String, String>();
					map.put(tag_cost_valuation_details_id, cost_valuation_details_id);
					map.put(tag_kind, kind);
					map.put(tag_area, area);
					map.put(tag_cost_per_sqm, cost_per_sqm);
					map.put(tag_reproduction_cost, reproduction_cost);

					// adding HashList to ArrayList
					itemList.add(map);
				}
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					ListAdapter adapter = new SimpleAdapter(
							LI_Valuation_Cost_Details.this, itemList,
							R.layout.reports_land_valuation_cost_details_list, new String[] { tag_cost_valuation_details_id,
							tag_kind, tag_area, tag_cost_per_sqm, tag_reproduction_cost},
							new int[] { R.id.primary_key, R.id.item_kind, R.id.item_area,
									R.id.item_cost, R.id.item_reproduction_cost });
					// updating listview
					setListAdapter(adapter);
				}
			});
			if(itemList.isEmpty()){
				add_new();
			}
		}
	}
	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}


}