package com.gds.appraisalmaybank.land_improvements;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database2.Land_Improvements_API_Prev_Appraisal;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LI_Prev_Appraisal extends ListActivity implements OnClickListener {
    DatabaseHandler2 db = new DatabaseHandler2(this);
    GDS_methods gds = new GDS_methods();
    Button btn_add_prev_appraisal, btn_save_prev_appraisal;
    Button btn_create, btn_cancel, btn_save;
    //EditText report_area, report_value,report_appraised_value;
    String record_id="";
    private static final String TAG_RECORD_ID = "record_id";

    Session_Timer st = new Session_Timer(this);
    String nature_appraisal;
    Dialog myDialog;
    //create dialog
    EditText  report_main_prev_desc, report_main_prev_area, report_main_prev_unit_value, report_main_prev_appraised_value,report_prev_total_appraised_value,
            valrep_land_prev_land_value, valrep_land_prev_imp_value;
    TextView tv_desc,tv_area,tv_value;
    //forListViewDeclarations
    private ProgressDialog pDialog;
    ArrayList<HashMap<String, String>> itemList;
    private static final String tag_prev_appraisal_id = "prev_appraisal_id";
    private static final String tag_desc = "prev_desc";
    private static final String tag_area= "prev_area";
    private static final String tag_value= "prev_value";
    private static final String tag_appraised_value= "prev_appraised_value";
    private static final String tag_land_value= "prev_land_value";
    private static final String tag_imp_value= "prev_imp_value";
    String prev_appraisal_id="";
    String where = "WHERE record_id = args";
    String[] args = new String[1];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reports_prev_appraisal);

        st.resetDisconnectTimer();

        report_prev_total_appraised_value = (EditText)findViewById(R.id.report_prev_total_appraised_value);

        btn_add_prev_appraisal = (Button) findViewById(R.id.btn_add_prev_appraisal);
        btn_add_prev_appraisal.setOnClickListener(this);

        btn_save_prev_appraisal = (Button) findViewById(R.id.btn_save_prev_appraisal);
        btn_save_prev_appraisal.setOnClickListener(this);

        // getting record_id from intent / previous class
        Intent i = getIntent();
        record_id = i.getStringExtra(TAG_RECORD_ID);
        //list
        itemList = new ArrayList<HashMap<String, String>>();
        // Loading in Background Thread
        new LoadAll().execute();
        // Get listview
        ListView lv = getListView();

        // on seleting single item
        // launching Edit item Screen
        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // getting values from selected ListItem
                prev_appraisal_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
                        .toString();
                view_details();
            }
        });
        lv.setOnItemLongClickListener(new OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {
                // TODO Auto-generated method stub

                Log.v("long clicked", "pos: " + position);
                prev_appraisal_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
                        .toString();
                delete_item();
                return true;
            }
        });


        //String where = "WHERE record_id = args";
        //String[] args = new String[1];
        args[0] = record_id;
         nature_appraisal = db.getRecord("nature_appraisal", where, args, "tbl_report_accepted_jobs").get(0);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        Intent intent = new Intent(LI_Prev_Appraisal.this, Land_Improvements.class);
        intent.putExtra("keyopen","1");
        startActivity(intent);
    }


    @Override
    public void onClick(View arg0) {
        // TODO Auto-generated method stub
        switch (arg0.getId()) {
            case R.id.btn_add_prev_appraisal:
                add_new();
                break;
            case R.id.btn_save_prev_appraisal:
                this.finish();
                Intent intent = new Intent(LI_Prev_Appraisal.this, Land_Improvements.class);
                intent.putExtra("keyopen","1");
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    public void add_new(){
        Toast.makeText(getApplicationContext(), "Add New",
                Toast.LENGTH_SHORT).show();
        myDialog = new Dialog(LI_Prev_Appraisal.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.report_prev_appraisal_form);
        myDialog.setCancelable(true);

        android.view.WindowManager.LayoutParams params = myDialog.getWindow()
                .getAttributes();
        params.height = LayoutParams.WRAP_CONTENT;
        params.width = LayoutParams.FILL_PARENT;
        myDialog.getWindow().setAttributes(
                (android.view.WindowManager.LayoutParams) params);

        btn_create = (Button) myDialog.findViewById(R.id.btn_right);
        btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
        btn_create.setText("Create");
        btn_cancel.setText("Cancel");
        report_main_prev_desc = (EditText) myDialog.findViewById(R.id.report_desc);
        report_main_prev_area = (EditText) myDialog.findViewById(R.id.report_area);
        report_main_prev_unit_value = (EditText) myDialog.findViewById(R.id.report_value);
        report_main_prev_appraised_value = (EditText) myDialog.findViewById(R.id.report_appraised_value);
        valrep_land_prev_land_value = (EditText) myDialog.findViewById(R.id.valrep_land_prev_land_value);
        valrep_land_prev_imp_value = (EditText) myDialog.findViewById(R.id.valrep_land_prev_imp_value);
        tv_desc = (TextView) myDialog.findViewById(R.id.tv_desc);
        tv_area = (TextView) myDialog.findViewById(R.id.tv_area);
        tv_value = (TextView) myDialog.findViewById(R.id.tv_value);


        report_main_prev_area.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(report_main_prev_area, this, s, current);
                prev_textwatch();

                /*//report_appraised_value.setText("" + val_appraised_value());
                report_main_prev_appraised_value.setText(gds.round(gds.multiplicationDecimal(report_main_prev_area.getText().toString(), report_main_prev_unit_value.getText().toString())));
                report_main_prev_area.setSelection(report_main_prev_area.length());*/


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        report_main_prev_unit_value.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                current = gds.numberFormat(report_main_prev_unit_value, this, s, current);
                prev_textwatch();

                /*//report_appraised_value.setText("" + val_appraised_value());
                report_main_prev_appraised_value.setText(gds.round(gds.multiplication(report_main_prev_area.getText().toString(), report_main_prev_unit_value.getText().toString())));
                report_main_prev_area.setSelection(report_main_prev_area.length());*/


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        valrep_land_prev_imp_value.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                current = gds.numberFormat(valrep_land_prev_imp_value, this, s, current);
                prev_textwatch();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        valrep_land_prev_land_value.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(valrep_land_prev_land_value, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });
        report_main_prev_appraised_value.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(report_main_prev_appraised_value, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });

        btn_create.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                    add_db();

            }
        });
        btn_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        if (nature_appraisal.contentEquals("Re-appraisal")) {
            gds.fill_in_error(new EditText[]{report_main_prev_desc, report_main_prev_area, report_main_prev_unit_value,
                    report_main_prev_appraised_value,valrep_land_prev_land_value,valrep_land_prev_imp_value});
        }
        myDialog.show();
    }
public void add_db(){
    db.addLand_Improvements_Prev_Appraisal(new Land_Improvements_API_Prev_Appraisal(
            record_id,
            report_main_prev_desc.getText().toString(),
            gds.replaceFormat(report_main_prev_area.getText().toString()),
            gds.replaceFormat(report_main_prev_unit_value.getText().toString()),
            gds.replaceFormat(report_main_prev_appraised_value.getText().toString()),
            gds.replaceFormat(valrep_land_prev_land_value.getText().toString()),
            gds.replaceFormat(valrep_land_prev_imp_value.getText().toString())));


    Toast.makeText(getApplicationContext(), "Data created", Toast.LENGTH_SHORT).show();
    myDialog.dismiss();
    reloadClass();
}
    public void view_details(){
        myDialog = new Dialog(LI_Prev_Appraisal.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.report_prev_appraisal_form);
        myDialog.setCancelable(true);

        android.view.WindowManager.LayoutParams params = myDialog.getWindow()
                .getAttributes();
        params.height = LayoutParams.WRAP_CONTENT;
        params.width = LayoutParams.FILL_PARENT;
        myDialog.getWindow().setAttributes(
                (android.view.WindowManager.LayoutParams) params);

        btn_save = (Button) myDialog.findViewById(R.id.btn_right);
        btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
        btn_save.setText("Save");
        btn_cancel.setText("Cancel");

        args[0]=record_id;
        report_main_prev_desc = (EditText) myDialog.findViewById(R.id.report_desc);
        report_main_prev_area = (EditText) myDialog.findViewById(R.id.report_area);
        report_main_prev_unit_value = (EditText) myDialog.findViewById(R.id.report_value);
        report_main_prev_appraised_value = (EditText) myDialog.findViewById(R.id.report_appraised_value);
        valrep_land_prev_land_value = (EditText) myDialog.findViewById(R.id.valrep_land_prev_land_value);
        valrep_land_prev_imp_value = (EditText) myDialog.findViewById(R.id.valrep_land_prev_imp_value);

        tv_desc = (TextView) myDialog.findViewById(R.id.tv_desc);
        tv_area = (TextView) myDialog.findViewById(R.id.tv_area);
        tv_value = (TextView) myDialog.findViewById(R.id.tv_value);


        List<Land_Improvements_API_Prev_Appraisal> prev_app_List = db
                .getLand_Improvements_API_Prev_Appraisal_Single(String.valueOf(record_id), String.valueOf(prev_appraisal_id));
        if (!prev_app_List.isEmpty()) {
            for (Land_Improvements_API_Prev_Appraisal li : prev_app_List) {
                report_main_prev_desc.setText(li.getreport_main_prev_desc());
                report_main_prev_area.setText(gds.numberFormat(li.getreport_main_prev_area()));
                report_main_prev_unit_value.setText(gds.numberFormat(li.getreport_main_prev_unit_value()));
                report_main_prev_appraised_value.setText(gds.numberFormat(li.getreport_main_prev_appraised_value()));
                valrep_land_prev_land_value.setText(gds.numberFormat(li.getvalrep_land_prev_land_value()));
                valrep_land_prev_imp_value.setText(gds.numberFormat(li.getvalrep_land_prev_imp_value()));

            }
        }
        report_main_prev_area.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                current = gds.numberFormat(report_main_prev_area, this, s, current);
                prev_textwatch();
                /*//report_appraised_value.setText("" + val_appraised_value());
                report_main_prev_appraised_value.setText(gds.round(gds.multiplication(report_main_prev_area.getText().toString(), report_main_prev_unit_value.getText().toString())));
                report_main_prev_area.setSelection(report_main_prev_area.length());*/


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        report_main_prev_unit_value.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                current = gds.numberFormat(report_main_prev_unit_value, this, s, current);
                prev_textwatch();
                /*//report_appraised_value.setText("" + val_appraised_value());
                report_main_prev_appraised_value.setText(gds.round(gds.multiplication(report_main_prev_area.getText().toString(), report_main_prev_unit_value.getText().toString())));
                report_main_prev_area.setSelection(report_main_prev_area.length());*/


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        valrep_land_prev_imp_value.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                current = gds.numberFormat(valrep_land_prev_imp_value, this, s, current);
                prev_textwatch();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        valrep_land_prev_land_value.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(valrep_land_prev_land_value, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });
        report_main_prev_appraised_value.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(report_main_prev_appraised_value, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });
        btn_save.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                    Land_Improvements_API_Prev_Appraisal li = new Land_Improvements_API_Prev_Appraisal();
                    li.setreport_main_prev_desc(report_main_prev_desc.getText().toString());
                    li.setreport_main_prev_area(gds.replaceFormat(report_main_prev_area.getText().toString()));
                    li.setreport_main_prev_unit_value(gds.replaceFormat(report_main_prev_unit_value.getText().toString()));
                    li.setreport_main_prev_appraised_value(gds.replaceFormat(report_main_prev_appraised_value.getText().toString()));
                    li.setvalrep_land_prev_land_value(gds.replaceFormat(valrep_land_prev_land_value.getText().toString()));
                    li.setvalrep_land_prev_imp_value(gds.replaceFormat(valrep_land_prev_imp_value.getText().toString()));

                    db.updateLand_Improvements_Main_Prev_Appraisal(li, record_id, String.valueOf(prev_appraisal_id));
                    db.close();
                    myDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Saved",Toast.LENGTH_SHORT).show();
                    reloadClass();


            }
        });
        btn_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        if (nature_appraisal.contentEquals("Re-appraisal")) {
            gds.fill_in_error(new EditText[]{report_main_prev_desc, report_main_prev_area, report_main_prev_unit_value,
                    report_main_prev_appraised_value,valrep_land_prev_land_value,valrep_land_prev_imp_value});
        }
        myDialog.show();
    }

    //EditText checker
    private boolean validate(EditText[] fields){
        for(int i=0; i<fields.length; i++){
            EditText currentField=fields[i];
            if(currentField.getText().toString().length()<=0){
                return false;
            }
        }
        return true;
    }

    public void prev_textwatch(){
        String prev_area=gds.replaceFormat(report_main_prev_area.getText().toString());
        String prev_unit=gds.replaceFormat(report_main_prev_unit_value.getText().toString());
        String land_val=gds.multiplication(prev_area,prev_unit);
        valrep_land_prev_land_value.setText(land_val);
        String prev_imp_val=gds.replaceFormat(valrep_land_prev_imp_value.getText().toString());
        report_main_prev_appraised_value.setText(gds.addition(prev_imp_val,land_val));
    }

    public void reloadClass(){
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }
    public void delete_item(){
        myDialog = new Dialog(LI_Prev_Appraisal.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.delete_dialog);
        myDialog.setCancelable(true);
        final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
        final Button btn_yes = (Button) myDialog.findViewById(R.id.btn_yes);
        final Button btn_no = (Button) myDialog.findViewById(R.id.btn_no);
        tv_question.setText("Are you sure you want to delete the selected item?");
        btn_yes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                db.deleteLand_Improvements_API_Prev_Appraisal_Single(record_id, prev_appraisal_id);
                db.close();
                //myDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Data Deleted",Toast.LENGTH_SHORT).show();
                reloadClass();

            }
        });
        btn_no.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        myDialog.show();
    }


    /**
     * Background Async Task to Load all
     * */
    class LoadAll extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Added By IAN
            String total_appraised = gds.nullCheck(db.getLand_Improvements_API_Total_Appraised(record_id).toString());
            String total_appraised_s = gds.stringToDecimal(total_appraised);
            report_prev_total_appraised_value.setText(gds.numberFormat(total_appraised_s));
            db.updateLand_Improvements_Total_Appraised_Value(record_id, gds.replaceFormat(total_appraised_s));

            pDialog = new ProgressDialog(LI_Prev_Appraisal.this);
            pDialog.setMessage("Loading Lists. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting All
         * */
        protected String doInBackground(String... args) {
            String desc="", area="", value="", appraised_value="", land_value="", imp_value="";


            List<Land_Improvements_API_Prev_Appraisal> liprev_app = db.getLand_Improvements_API_Prev_Appraisal(record_id);
            if (!liprev_app.isEmpty()) {
                for (Land_Improvements_API_Prev_Appraisal imprev_app : liprev_app) {
                    prev_appraisal_id = String.valueOf(imprev_app.getID());
                    record_id = imprev_app.getrecord_id();
                    desc = imprev_app.getreport_main_prev_desc();
                    area = gds.numberFormat(imprev_app.getreport_main_prev_area());
                    value = gds.numberFormat(imprev_app.getreport_main_prev_unit_value());
                    appraised_value = gds.numberFormat(imprev_app.getreport_main_prev_appraised_value());
                    land_value = gds.numberFormat(imprev_app.getvalrep_land_prev_land_value());
                    imp_value = gds.numberFormat(imprev_app.getvalrep_land_prev_imp_value());

                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(TAG_RECORD_ID, record_id);
                    map.put(tag_prev_appraisal_id, prev_appraisal_id);
                    map.put(tag_desc, desc);
                    map.put(tag_area, area);
                    map.put(tag_value, value);
                    map.put(tag_appraised_value, appraised_value);
                    map.put(tag_land_value, land_value);
                    map.put(tag_imp_value, imp_value);
                    // adding HashList to ArrayList
                    itemList.add(map);
                }
            }
            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all
            pDialog.dismiss();
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    ListAdapter adapter = new SimpleAdapter(
                            LI_Prev_Appraisal.this, itemList,
                            R.layout.report_prev_appraisal_list, new String[] { TAG_RECORD_ID, tag_prev_appraisal_id, tag_desc,
                            tag_area, tag_value, tag_appraised_value},
                            new int[] { R.id.report_record_id, R.id.primary_key, R.id.item_desc, R.id.item_area, R.id.item_value, R.id.item_appraised_value});
                    // updating listview
                    setListAdapter(adapter);
                }
            });
            if(itemList.isEmpty()){
                add_new();
            }
        }
    }

    @Override
    public void onUserInteraction(){
        st.resetDisconnectTimer();
    }
    @Override
    public void onStop() {
        super.onStop();
        st.stopDisconnectTimer();
    }
    @Override
    public void onResume() {
        super.onResume();
        st.resetDisconnectTimer();
    }


}