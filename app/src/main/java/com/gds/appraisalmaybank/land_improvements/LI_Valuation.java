package com.gds.appraisalmaybank.land_improvements;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Land_Improvements_API;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database2.Land_Improvements_API_Lot_Valuation_Details;
import com.gds.appraisalmaybank.database_new.Tables;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LI_Valuation extends ListActivity implements OnClickListener {
	DatabaseHandler2 db = new DatabaseHandler2(this);
	DatabaseHandler dbMain = new DatabaseHandler(this);

	Session_Timer st = new Session_Timer(this);
	GDS_methods gds = new GDS_methods();
	Button btn_add_valuation, btn_view_total_valuation, btn_update_valuation;
	
	Button btn_create, btn_cancel, btn_save, btn_compute;
	
	String record_id="";
	
	private static final String TAG_RECORD_ID = "record_id";
	
	Dialog myDialog;
	
	EditText report_grand_total_area, report_grand_total_deduc, report_grand_total_net_area, report_grand_total_land_value;
	//create dialog
	EditText report_tct_no, report_lot_no, report_block, report_total_area, report_deduc, report_net_area, report_unit_value, report_total_land_val;
	
	//forListViewDeclarations
	private ProgressDialog pDialog;
	ArrayList<HashMap<String, String>> itemList;
	private static final String tag_lot_valuation_details_id = "lot_valuation_details_id";
	private static final String tag_tct_no = "tct_no";
	private static final String tag_area = "area";
	private static final String tag_deduc = "deduc";
	private static final String tag_net_area = "net_area";
	private static final String tag_land_value = "land_value";
	String lot_valuation_details_id="", tct_no="", area="", deduc="", net_area="", land_value="";
	//total
	String grand_total_area, grand_total_deduc, grand_total_net_area, grand_total_land_value;
	//computation variables


	//net_area from comparative
	String first_unit_value="";
	int item_position=-1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reports_land_valuation);

		st.resetDisconnectTimer();
		btn_add_valuation = (Button) findViewById(R.id.btn_add_valuation);
		btn_add_valuation.setOnClickListener(this);
		btn_view_total_valuation = (Button) findViewById(R.id.btn_view_total_valuation);
		btn_view_total_valuation.setOnClickListener(this);
		//hide buttons because of auto sync with lot details
		btn_add_valuation.setVisibility(View.GONE);
		
		btn_update_valuation = (Button) findViewById(R.id.btn_update_valuation);
		btn_update_valuation.setOnClickListener(this);
		
		
		// getting record_id from intent / previous class
		Intent i = getIntent();
		record_id = i.getStringExtra(TAG_RECORD_ID);
		//list
		itemList = new ArrayList<HashMap<String, String>>();
		//view_total();
		// Loading in Background Thread
		new LoadAll().execute();
		/*if(net_area.contentEquals("")||net_area.contentEquals("0")) {
		}*/


		//auto-save of total
		//auto_save_total();
		// Get listview
		ListView lv = getListView();



		// on seleting single item
		// launching Edit item Screen
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// getting values from selected ListItem
				lot_valuation_details_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
				if (position==0){
					List<Land_Improvements_API> vl = dbMain.getLand_Improvements(String.valueOf(record_id));
					if (!vl.isEmpty()) {
						for (Land_Improvements_API im : vl) {
							first_unit_value = im.getreport_comp_rounded_to();
						}
					}
					item_position = position;
				}
				view_details();
			}
		});
		/*lv.setOnItemLongClickListener(new OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
                // TODO Auto-generated method stub

                Log.v("long clicked","pos: " + position);
                lot_valuation_details_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
                delete_item();
                return true;
            }
        }); */
	}
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.btn_add_valuation:
			add_new();
			break;
		case R.id.btn_view_total_valuation:
			view_total();
			break;
		case R.id.btn_update_valuation:
			save_summary_total();
			finish();
			break;
		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		auto_save_total();
		save_summary_total();
		finish();
	}

	public void auto_save_total(){//saving total to main table
		String where = "WHERE record_id = args";
		String[] args = new String[1];
		args[0]=record_id;
		String area = "" + gds.summation(db.getRecord("report_value_area", where, args, Tables.land_improvements_lot_valuation_details.table_name));
		String deduc = "" + gds.summation(db.getRecord("report_value_deduction", where, args, Tables.land_improvements_lot_valuation_details.table_name));
		String net_area = "" + gds.summation(db.getRecord("report_value_net_area", where, args, Tables.land_improvements_lot_valuation_details.table_name));
		String land_value = "" + gds.summation(db.getRecord("report_value_land_value", where, args, Tables.land_improvements_lot_valuation_details.table_name));

		 grand_total_area = gds.addition(area,"0");
		 grand_total_deduc = gds.addition(deduc,"0");
		 grand_total_net_area = gds.addition(net_area,"0");
		 grand_total_land_value =  gds.addition(land_value,"0");

		Land_Improvements_API li = new Land_Improvements_API();
		li.setreport_value_total_area(String.valueOf(grand_total_area));
		li.setreport_value_total_deduction(String.valueOf(grand_total_deduc));
		li.setreport_value_total_net_area(String.valueOf(grand_total_net_area));
		li.setreport_value_total_landimp_value(String.valueOf(grand_total_land_value));
		dbMain.updateLand_Improvements_Total_Valuation(li, record_id);
		db.close();
	}
	
	public void add_new(){
		Toast.makeText(getApplicationContext(), "Add New",
				Toast.LENGTH_SHORT).show();
		myDialog = new Dialog(LI_Valuation.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.reports_land_valuation_form);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.FILL_PARENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		
		btn_create = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_create.setText("Create");
		btn_cancel.setText("Cancel");
		
		report_tct_no = (EditText) myDialog.findViewById(R.id.report_tct_no);
		report_lot_no = (EditText) myDialog.findViewById(R.id.report_lot_no);
		report_block = (EditText) myDialog.findViewById(R.id.report_block);
		report_total_area = (EditText) myDialog.findViewById(R.id.report_total_area);
		report_deduc = (EditText) myDialog.findViewById(R.id.report_deduc);
		report_net_area = (EditText) myDialog.findViewById(R.id.report_net_area);
		report_unit_value = (EditText) myDialog.findViewById(R.id.report_unit_value);
		report_total_land_val = (EditText) myDialog.findViewById(R.id.report_total_land_val);
		
		btn_create.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				//list of EditTexts put in array

					//add data in SQLite
					db.addLand_Improvements_Lot_Valuation_Details(new Land_Improvements_API_Lot_Valuation_Details(
							record_id,
							report_tct_no.getText().toString(),
							report_lot_no.getText().toString(),
							report_block.getText().toString(),
							gds.replaceFormat(report_total_area.getText().toString()),
							gds.replaceFormat(report_deduc.getText().toString()),
							gds.replaceFormat(report_net_area.getText().toString()),
							gds.replaceFormat(report_unit_value.getText().toString()),
							gds.replaceFormat(report_total_land_val.getText().toString())));
						Toast.makeText(getApplicationContext(),"data created", Toast.LENGTH_SHORT).show();
						//auto-save of total

						auto_save_total();
						myDialog.dismiss();
						reloadClass();

				
			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		gds.fill_in_error(new EditText[]{report_deduc, report_unit_value});

		myDialog.show();
	}
	
	public void view_details(){
		myDialog = new Dialog(LI_Valuation.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.reports_land_valuation_form);
		myDialog.setCancelable(false);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.FILL_PARENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		
		btn_compute = (Button) myDialog.findViewById(R.id.btn_compute);
		btn_save = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_save.setText("Save");
		btn_cancel.setText("Cancel");
		report_tct_no = (EditText) myDialog.findViewById(R.id.report_tct_no);
		report_lot_no = (EditText) myDialog.findViewById(R.id.report_lot_no);
		report_block = (EditText) myDialog.findViewById(R.id.report_block);
		report_total_area = (EditText) myDialog.findViewById(R.id.report_total_area);
		report_deduc = (EditText) myDialog.findViewById(R.id.report_deduc);
		report_net_area = (EditText) myDialog.findViewById(R.id.report_net_area);
		report_unit_value = (EditText) myDialog.findViewById(R.id.report_unit_value);
		report_total_land_val = (EditText) myDialog.findViewById(R.id.report_total_land_val);
		//report_tct_no.setEnabled(false);
		//report_lot_no.setEnabled(false);
		//report_block.setEnabled(false);
		//report_total_area.setEnabled(false);
		//report_net_area.setEnabled(false);
		//report_total_land_val.setEnabled(false);


		if (item_position==0){
			/*report_unit_value.setEnabled(false);
			report_unit_value.setBackgroundResource(R.drawable.rounded_edittext_gray);
			report_unit_value.setTextColor(Color.DKGRAY);*/
			List<Land_Improvements_API> vl = dbMain.getLand_Improvements(record_id);
			if (!vl.isEmpty()) {
				for (Land_Improvements_API im : vl) {
					report_unit_value.setText(gds.numberFormat(gds.nullCheck2(im.getreport_comp_rounded_to())));

				}
			}
		}else{
			List<Land_Improvements_API_Lot_Valuation_Details> lotValuationDetailsList = db
					.getLand_Improvements_Lot_Valuation_Details_with_lot_valuation_details_id(String.valueOf(record_id), String.valueOf(lot_valuation_details_id));
			if (!lotValuationDetailsList.isEmpty()) {
				for (Land_Improvements_API_Lot_Valuation_Details vl : lotValuationDetailsList) {
					report_unit_value.setText(gds.numberFormat(vl.getreport_value_unit_value()));
				}
			}
		}

		List<Land_Improvements_API_Lot_Valuation_Details> lotValuationDetailsList = db
				.getLand_Improvements_Lot_Valuation_Details_with_lot_valuation_details_id(String.valueOf(record_id), String.valueOf(lot_valuation_details_id));
		if (!lotValuationDetailsList.isEmpty()) {
			for (Land_Improvements_API_Lot_Valuation_Details li : lotValuationDetailsList) {
				report_tct_no.setText(li.getreport_value_tct_no());
				report_lot_no.setText(li.getreport_value_lot_no());
				report_block.setText(li.getreport_value_block_no());
				report_total_area.setText(gds.numberFormat(li.getreport_value_area()));
				report_deduc.setText(gds.numberFormat(li.getreport_value_deduction()));
				report_net_area.setText(gds.numberFormat(li.getreport_value_net_area()));
				//report_unit_value.setText(li.getreport_value_unit_value());
				report_total_land_val.setText(gds.numberFormat(li.getreport_value_land_value()));
			}
		}
		String total_area_val = gds.nullCheck2(report_total_area.getText().toString());
		String deduc_val = gds.nullCheck2(report_deduc.getText().toString());
		String net_area_val = gds.netAreaVL(total_area_val,deduc_val);
		report_net_area.setText(gds.numberFormat(net_area_val));

		String unit_value_val = gds.nullCheck2(report_unit_value.getText().toString());
		String total_land_val_val = gds.totalLandVL(net_area_val,unit_value_val);
		report_total_land_val.setText(gds.numberFormat(total_land_val_val));

		report_total_area.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub


				current = gds.numberFormat(report_total_area, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});

		report_deduc.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_deduc, this, s, current);

				String total_area_val = gds.nullCheck2(report_total_area.getText().toString());
				String deduc_val = gds.nullCheck2(report_deduc.getText().toString());
				String net_area_val = gds.netAreaVL(total_area_val,deduc_val);
				report_net_area.setText(net_area_val);

				String unit_value_val = gds.nullCheck2(report_unit_value.getText().toString());
				String total_land_val_val = gds.totalLandVL(net_area_val,unit_value_val);
				report_total_land_val.setText(total_land_val_val);

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_net_area.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub


				current = gds.numberFormat(report_net_area, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});

		report_unit_value.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_unit_value, this, s, current);

				String total_area_val = gds.nullCheck2(report_total_area.getText().toString());
				String deduc_val = gds.nullCheck2(report_deduc.getText().toString());
				String net_area_val = gds.netAreaVL(total_area_val,deduc_val);
				report_net_area.setText(net_area_val);

				String unit_value_val = gds.nullCheck2(report_unit_value.getText().toString());
				String total_land_val_val = gds.totalLandVL(net_area_val,unit_value_val);
				report_total_land_val.setText(total_land_val_val);

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_total_land_val.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub


				current = gds.numberFormat(report_total_land_val, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});

		btn_save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

					//update data in SQLite
					Land_Improvements_API_Lot_Valuation_Details li = new Land_Improvements_API_Lot_Valuation_Details();
					li.setreport_value_tct_no(report_tct_no.getText().toString());
					li.setreport_value_lot_no(report_lot_no.getText().toString());
					li.setreport_value_block_no(report_block.getText().toString());
					li.setreport_value_area(gds.replaceFormat(report_total_area.getText().toString()));
					li.setreport_value_deduction(gds.replaceFormat(report_deduc.getText().toString()));
					li.setreport_value_net_area(gds.replaceFormat(report_net_area.getText().toString()));
					//li.setreport_value_net_area(total_area_val);
					li.setreport_value_unit_value(gds.replaceFormat(report_unit_value.getText().toString()));
					li.setreport_value_land_value(gds.replaceFormat(report_total_land_val.getText().toString()));
					//li.setreport_value_land_value(String.valueOf(df.format(total_land_val_val)));
					
					db.updateLand_Improvements_Lot_Valuation_Details(li, record_id, lot_valuation_details_id);

					db.close();
					myDialog.dismiss();
					Toast.makeText(getApplicationContext(), "Saved",Toast.LENGTH_SHORT).show();
					//reloadClass();
					view_total();




			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
				reloadClass();
			}
		});
		gds.fill_in_error(new EditText[]{report_deduc, report_unit_value});
		myDialog.show();
	}
	
	public void view_total(){
		myDialog = new Dialog(LI_Valuation.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.reports_land_valuation_view_total);
		myDialog.setCancelable(false);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		
		btn_save = (Button) myDialog.findViewById(R.id.btn_right_total);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left_total);
		btn_save.setText("Save");
		btn_cancel.setText("Cancel");
		btn_cancel.setVisibility(View.GONE);
		report_grand_total_area = (EditText) myDialog.findViewById(R.id.report_grand_total_area);
		report_grand_total_deduc = (EditText) myDialog.findViewById(R.id.report_grand_total_deduc);
		report_grand_total_net_area = (EditText) myDialog.findViewById(R.id.report_grand_total_net_area);
		report_grand_total_land_value = (EditText) myDialog.findViewById(R.id.report_grand_total_land_value);
		
		//to prevent x2 total
		
		//autocompute

		auto_save_total();
		List<Land_Improvements_API> li = dbMain.getLand_Improvements(String.valueOf(record_id));
		if (!li.isEmpty()) {
			for (Land_Improvements_API im : li) {
				report_grand_total_area.setText(gds.numberFormat(im.getreport_value_total_area()));
				report_grand_total_deduc.setText(gds.numberFormat(im.getreport_value_total_deduction()));
				report_grand_total_net_area.setText(gds.numberFormat(im.getreport_value_total_net_area()));
				report_grand_total_land_value.setText(gds.numberFormat(im.getreport_value_total_landimp_value()));
			}
		}
		//format 2 decimal place
		report_grand_total_area.setText(gds.numberFormat(String.valueOf(grand_total_area)));
		report_grand_total_deduc.setText(gds.numberFormat(String.valueOf(grand_total_deduc)));
		report_grand_total_net_area.setText(gds.numberFormat(String.valueOf(grand_total_net_area)));
		report_grand_total_land_value.setText(gds.numberFormat(String.valueOf(grand_total_land_value)));
		
		btn_save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

					//update data in SQLite
					Land_Improvements_API li = new Land_Improvements_API();
					li.setreport_value_total_area(gds.replaceFormat(report_grand_total_area.getText().toString()));
					li.setreport_value_total_deduction(gds.replaceFormat(report_grand_total_deduc.getText().toString()));
					li.setreport_value_total_net_area(gds.replaceFormat(report_grand_total_net_area.getText().toString()));
					li.setreport_value_total_landimp_value(gds.replaceFormat(report_grand_total_land_value.getText().toString()));
					/*//li.setreport_value_total_area(String.valueOf(grand_total_area));
					li.setreport_value_total_deduction(String.valueOf(grand_total_deduc));
					li.setreport_value_total_net_area(String.valueOf(grand_total_net_area));
					li.setreport_value_total_landimp_value(String.valueOf(grand_total_land_value));*/
					
					dbMain.updateLand_Improvements_Total_Valuation(li, record_id);
					db.close();
					myDialog.dismiss();
					//Toast.makeText(getApplicationContext(), "Saved",Toast.LENGTH_SHORT).show();
					reloadClass();

			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		gds.fill_in_error(new EditText[]{report_grand_total_deduc, report_grand_total_land_value});

		myDialog.show();
	}
	
	//EditText checker
	private boolean validate(EditText[] fields){
        for(int i=0; i<fields.length; i++){
            EditText currentField=fields[i];
            if((currentField.getText().toString().length()<=0)||(currentField.getText().toString().contains("_"))){
                return false;
            }
        }
        return true;
	}
	
	public void reloadClass(){
		Intent intent = getIntent();
		finish();
		startActivity(intent);
	}
	public void save_summary_total(){
//grand_total_land_value
		String grand_total_land_val =gds.nullCheck(db.getLand_Improvements_API_grand_total_land_value(record_id));
		String grand_total_land_val_s = gds.stringToDecimal(grand_total_land_val);
		//total imp value
		String total_imp_value =gds.nullCheck(dbMain.getLand_Improvements_API_Total_imp_value(record_id));
		String total_imp_value_s = gds.stringToDecimal(total_imp_value);
		String final_grand_val = gds.addition(grand_total_land_val_s,total_imp_value_s);
		//update Summary
		Land_Improvements_API vl4 = new Land_Improvements_API();
		vl4.setreport_final_value_total_appraised_value_land_imp(final_grand_val);
		dbMain.updateLand_Improvements_Summary_Total(vl4, record_id);


		dbMain.close();
	}
	public void delete_item(){
		myDialog = new Dialog(LI_Valuation.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.delete_dialog);
		myDialog.setCancelable(true);
		final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
		final Button btn_yes = (Button) myDialog.findViewById(R.id.btn_yes);
		final Button btn_no = (Button) myDialog.findViewById(R.id.btn_no);
		tv_question.setText("Are you sure you want to delete the selected item?");
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				db.deleteLand_Improvements_API_Lot_Valuation_Details_Single(record_id, lot_valuation_details_id);
				db.close();
				myDialog.dismiss();
				reloadClass();
				Toast.makeText(getApplicationContext(), "Data Deleted",Toast.LENGTH_SHORT).show();
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		
		myDialog.show();
	}
	
	
	/**
	 * Background Async Task to Load all
	 * */
	class LoadAll extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(LI_Valuation.this);
			pDialog.setMessage("Loading List. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All
		 * */
		protected String doInBackground(String... args) {
			
			List<Land_Improvements_API_Lot_Valuation_Details> lilvd = db.getLand_Improvements_Lot_Valuation_Details(record_id);
			if (!lilvd.isEmpty()) {
				for (Land_Improvements_API_Lot_Valuation_Details imlvd : lilvd) {
					lot_valuation_details_id = String.valueOf(imlvd.getID());
					tct_no = imlvd.getreport_value_tct_no();
					area = imlvd.getreport_value_area();
					deduc = imlvd.getreport_value_deduction();
					net_area = imlvd.getreport_value_net_area();
					land_value = imlvd.getreport_value_land_value();
					
					HashMap<String, String> map = new HashMap<String, String>();
					map.put(tag_lot_valuation_details_id, lot_valuation_details_id);
					map.put(tag_tct_no, tct_no);
					map.put(tag_area, area);
					map.put(tag_deduc, deduc);
					map.put(tag_net_area, net_area);
					map.put(tag_land_value, land_value);
					

					// adding HashList to ArrayList
					itemList.add(map);
				}

			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					ListAdapter adapter = new SimpleAdapter(
							LI_Valuation.this, itemList,
							R.layout.reports_land_valuation_list, new String[] {
									tag_lot_valuation_details_id, tag_tct_no,
									tag_area, tag_deduc, tag_net_area,
									tag_land_value }, new int[] {
									R.id.primary_key, R.id.item_tct_no,
									R.id.item_total_area, R.id.item_deduc,
									R.id.item_net_area,
									R.id.item_total_land_value });
					// updating listview
					setListAdapter(adapter);
				}
			});

		}
	}

	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}


}