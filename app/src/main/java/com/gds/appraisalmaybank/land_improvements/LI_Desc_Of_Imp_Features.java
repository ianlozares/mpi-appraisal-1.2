package com.gds.appraisalmaybank.land_improvements;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database2.Land_Improvements_API_Imp_Details_Features;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LI_Desc_Of_Imp_Features extends ListActivity implements OnClickListener {
	DatabaseHandler2 db = new DatabaseHandler2(this);
	GDS_methods gds = new GDS_methods();
	Button btn_add_desc_of_imp_features, btn_save_desc_of_imp_features;
	Button btn_create, btn_cancel, btn_save, btn_delete;
	private static final String tag_rownum = "rownum";//for cost valuation
	String rownum;//for cost valuation
	String record_id="";
	private static final String tag_imp_details_features_id = "imp_details_features_id";
	private static final String TAG_RECORD_ID = "record_id";
	private static final String tag_imp_details_id = "imp_details_id";
	private static final String tag_building_desc = "report_building_desc";
	private static final String tag_area = "area";
	private static final String tag_description = "description";
	String imp_details_features_id="", imp_details_id="", building_desc="";
	ArrayList<String> field = new ArrayList<String>();
	ArrayList<String> value = new ArrayList<String>();
	Session_Timer st = new Session_Timer(this);
	Dialog myDialog;
	//create dialog
	EditText report_area, report_description;

	//forListViewDeclarations
	private ProgressDialog pDialog;
	ArrayList<HashMap<String, String>> itemList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reports_land_desc_of_imp_features);

		st.resetDisconnectTimer();
		btn_add_desc_of_imp_features = (Button) findViewById(R.id.btn_add_desc_of_imp_features);
		btn_add_desc_of_imp_features.setOnClickListener(this);

		btn_save_desc_of_imp_features = (Button) findViewById(R.id.btn_save_desc_of_imp_features);
		btn_save_desc_of_imp_features.setOnClickListener(this);

		// getting record_id from intent / previous class
		Intent i = getIntent();
		record_id = i.getStringExtra(TAG_RECORD_ID);
		imp_details_id = i.getStringExtra(tag_imp_details_id);
		building_desc = i.getStringExtra(tag_building_desc);
		rownum = i.getStringExtra(tag_rownum);
		//Toast.makeText(getApplicationContext(), ""+record_id+"\n"+imp_details_id,Toast.LENGTH_SHORT).show();
		//list
		itemList = new ArrayList<HashMap<String, String>>();
		new LoadAll().execute();
		// Get listview
		ListView lv = getListView();

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {
				// getting values from selected ListItem
				imp_details_features_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
				view_details();
			}
		});
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

			public boolean onItemLongClick(AdapterView<?> parent, View view,
										   int position, long id) {
				// TODO Auto-generated method stub

				Log.v("long clicked","pos: " + position);
				imp_details_features_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
				delete_item();
				return true;
			}
		});

	}
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
			case R.id.btn_add_desc_of_imp_features:
				add_new();
				break;
			case R.id.btn_save_desc_of_imp_features:
				this.finish();
				//rownum = String.valueOf(position);//for cost valuation
				Intent in = new Intent(getApplicationContext(),
						LI_Desc_Of_Imp_View.class);
				in.putExtra(TAG_RECORD_ID, record_id);
				in.putExtra(tag_imp_details_id, imp_details_id);
				in.putExtra(tag_rownum, rownum);
				// starting new activity and expecting some response back
				startActivityForResult(in, 100);
				break;
			default:
				break;
		}
	}

	public void add_new(){
		Toast.makeText(getApplicationContext(), "Add New",Toast.LENGTH_SHORT).show();
		myDialog = new Dialog(LI_Desc_Of_Imp_Features.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.reports_land_desc_of_imp_features_form);
		myDialog.setCancelable(false);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);

		btn_create = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_create.setText("Create");
		btn_cancel.setText("Cancel");

		report_area = (EditText) myDialog.findViewById(R.id.report_area);
		report_description = (EditText) myDialog.findViewById(R.id.report_description);
		//changed to alphanumeric - mark 082217
		/*report_area.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub


				current = gds.numberFormat(report_area, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});*/


		btn_create.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {


					//add data in SQLite
					db.addLand_Improvements_Imp_Details_Features(new Land_Improvements_API_Imp_Details_Features(
							record_id,
							imp_details_id,
							building_desc,
							report_area.getText().toString(),
							report_description.getText().toString()));
					Toast.makeText(getApplicationContext(),
							"data created", Toast.LENGTH_SHORT).show();
					myDialog.dismiss();
					reloadClass();


			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});

		//gds.fill_in_error(new EditText[] {report_area, report_description});
		myDialog.show();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		this.finish();
		//rownum = String.valueOf(position);//for cost valuation
		Intent in = new Intent(getApplicationContext(),
				LI_Desc_Of_Imp_View.class);
		in.putExtra(TAG_RECORD_ID, record_id);
		in.putExtra(tag_imp_details_id, imp_details_id);
		in.putExtra(tag_rownum, rownum);
		// starting new activity and expecting some response back
		startActivityForResult(in, 100);
	}

	public void view_details(){
		myDialog = new Dialog(LI_Desc_Of_Imp_Features.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.reports_land_desc_of_imp_features_form);
		myDialog.setCancelable(false);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);

		btn_save = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_save.setText("Save");
		btn_cancel.setText("Cancel");
		report_area = (EditText) myDialog.findViewById(R.id.report_area);
		report_description = (EditText) myDialog.findViewById(R.id.report_description);

		//changed to alphanumeric - mark 082217
		/*report_area.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub


				current = gds.numberFormat(report_area, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});*/

		List<Land_Improvements_API_Imp_Details_Features> lotDetailsFeaturesList = db
				.getLand_Improvements_Imp_Details_Features_Single(String.valueOf(record_id), String.valueOf(imp_details_features_id));
		if (!lotDetailsFeaturesList.isEmpty()) {
			for (Land_Improvements_API_Imp_Details_Features li : lotDetailsFeaturesList) {
				report_area.setText(li.getreport_desc_features_area());
				report_description.setText(li.getreport_desc_features_area_desc());
			}
		}

		btn_save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

					//update data in SQLite
					Land_Improvements_API_Imp_Details_Features li = new Land_Improvements_API_Imp_Details_Features();
					li.setreport_desc_features_area(report_area.getText().toString());
					li.setreport_desc_features_area_desc(report_description.getText().toString());
					field.clear();
					db.updateLand_Improvements_Imp_Details_Features(li, record_id, imp_details_features_id);




					db.close();
					myDialog.dismiss();
					Toast.makeText(getApplicationContext(), "Saved",Toast.LENGTH_SHORT).show();
					reloadClass();

			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});

		//gds.fill_in_error(new EditText[]{report_area, report_description});

		myDialog.show();
	}

	//EditText checker
	private boolean validate(EditText[] fields){
		for(int i=0; i<fields.length; i++){
			EditText currentField=fields[i];
			if(currentField.getText().toString().length()<=0){
				return false;
			}
		}
		return true;
	}
	public void reloadClass(){
		Intent intent = getIntent();
		finish();
		startActivity(intent);
	}
	public void delete_item(){
		myDialog = new Dialog(LI_Desc_Of_Imp_Features.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.delete_dialog);
		myDialog.setCancelable(false);
		final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
		final Button btn_yes = (Button) myDialog.findViewById(R.id.btn_yes);
		final Button btn_no = (Button) myDialog.findViewById(R.id.btn_no);
		tv_question.setText("Are you sure you want to delete the selected item?");
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				db.deleteLand_Improvements_API_Imp_Details_Features_Single(record_id, imp_details_features_id);
				db.close();
				myDialog.dismiss();
				reloadClass();
				Toast.makeText(getApplicationContext(), "Data Deleted",
						Toast.LENGTH_SHORT).show();
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});

		myDialog.show();
	}
	// Response from LI_Desc_Of_Imp_View Activity
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// if result code 100
		if (resultCode == 100) {
			// if result code 100 is received 
			// means user edited/deleted product
			// reload this screen again
			Intent intent = getIntent();
			finish();
			startActivity(intent);
		}

	}

	/**
	 * Background Async Task to Load all 
	 * */
	class LoadAll extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(LI_Desc_Of_Imp_Features.this);
			pDialog.setMessage("Loading List. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All 
		 * */
		protected String doInBackground(String... args) {
			String imp_details_features_id="", area="", description="";

			List<Land_Improvements_API_Imp_Details_Features> liidf = db.getLand_Improvements_Imp_Details_Features(record_id, imp_details_id);
			if (!liidf.isEmpty()) {
				for (Land_Improvements_API_Imp_Details_Features imidf : liidf) {
					imp_details_features_id = String.valueOf(imidf.getID());
					record_id = imidf.getrecord_id();
					imidf.getreport_impsummary1_building_desc();
					area = imidf.getreport_desc_features_area();
					description = imidf.getreport_desc_features_area_desc();

					HashMap<String, String> map = new HashMap<String, String>();
					map.put(tag_imp_details_features_id, imp_details_features_id);
					map.put(tag_area, area);
					map.put(tag_description, description);
					//Toast.makeText(getApplicationContext(), ""+itemList,Toast.LENGTH_SHORT).show();

					// adding HashList to ArrayList
					itemList.add(map);
				}
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					ListAdapter adapter = new SimpleAdapter(
							LI_Desc_Of_Imp_Features.this, itemList,
							R.layout.reports_land_desc_of_imp_features_list, new String[] { tag_imp_details_features_id, tag_area,
							tag_description},
							new int[] { R.id.primary_key, R.id.item_area, R.id.item_description });
					// updating listview
					setListAdapter(adapter);
				}
			});
			if(itemList.isEmpty()){
				add_new();
			}
		}
	}

	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}

}