package com.gds.appraisalmaybank.land_improvements;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database2.Land_Improvements_API_Cost_Valuation;
import com.gds.appraisalmaybank.database2.Land_Improvements_API_Imp_Details;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SuppressLint("InflateParams")
public class LI_Desc_Of_Imp extends ListActivity implements OnClickListener {
	DatabaseHandler2 db = new DatabaseHandler2(this);
	GDS_methods gds = new GDS_methods();
	Button btn_add_desc_of_imp, btn_save_desc_of_imp;
	Button btn_create, btn_cancel, btn_save, btn_delete;
	Session_Timer st = new Session_Timer(this);
	String record_id="";
	private static final String TAG_RECORD_ID = "record_id";
	private static final String tag_imp_details_id = "imp_details_id";

	Dialog myDialog;
	//create dialog
	EditText report_td_no,report_num_of_floors, report_desc_of_bldg, report_erected_on_lot, report_fa, report_fa_per_td,
			report_actual_utilization, report_declaration_as_to_usage, report_declared_owner, report_num_of_bedrooms,valrep_landimp_impsummary1_no_of_tb;
	Spinner spinner_type_of_property, spinner_ownership_of_property, spinner_socialized_housing;
	MultiAutoCompleteTextView report_foundation, report_columns, report_beams, report_exterior_walls, report_interior_walls,
			report_flooring, report_doors, report_windows, report_ceiling, report_roofing, report_trusses, valrep_landimp_desc_stairs;
	EditText report_economic_life, report_effective_age,
			report_remaining_eco_life, report_occupants, report_owned_or_leased;
	Spinner spinner_confirmed_thru, spinner_observed_condition;
	//forListViewDeclarations
	private ProgressDialog pDialog;
	ArrayList<HashMap<String, String>> itemList;
	private static final String tag_building_desc = "building_desc";
	private static final String tag_type_of_property = "type_of_property";
	String imp_details_id="";
	private static final String tag_rownum = "rownum";//for cost valuation
	String rownum;//for cost valuation
	//for computaion variables
	double economic_life_val, effective_age_val, less_dep_percent_val;
	DecimalFormat df = new DecimalFormat("#.00");

	String where = "WHERE record_id = args";
	final String[] args = new String[1];
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reports_land_desc_of_imp);

		st.resetDisconnectTimer();
		btn_add_desc_of_imp = (Button) findViewById(R.id.btn_add_desc_of_imp);
		btn_add_desc_of_imp.setOnClickListener(this);

		btn_save_desc_of_imp = (Button) findViewById(R.id.btn_save_desc_of_imp);
		btn_save_desc_of_imp.setOnClickListener(this);
		report_td_no = (EditText)findViewById(R.id.report_td_no);
		// getting record_id from intent / previous class
		Intent i = getIntent();
		record_id = i.getStringExtra(TAG_RECORD_ID);
		//list
		itemList = new ArrayList<HashMap<String, String>>();
		new LoadAll().execute();
		// Get listview
		ListView lv = getListView();

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {
				savedb();
				// getting values from selected ListItem
				imp_details_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();

				rownum = String.valueOf(position);//for cost valuation
				// Starting new intent
				finish();
				Intent in = new Intent(getApplicationContext(),
						LI_Desc_Of_Imp_View.class);
				in.putExtra(TAG_RECORD_ID, record_id);
				in.putExtra(tag_imp_details_id, imp_details_id);
				in.putExtra(tag_rownum, rownum);
				// starting new activity and expecting some response back
				startActivityForResult(in, 100);
			}
		});
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

			public boolean onItemLongClick(AdapterView<?> parent, View view,
										   int position, long id) {
				// TODO Auto-generated method stub

				Log.v("long clicked", "pos: " + position);
				imp_details_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
				delete_item();
				return true;
			}
		});
		args[0] = record_id;
		report_td_no.setText(db.getRecord("report_landimp_td_no", where, args, "land_improvements").get(0));
	}
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
			case R.id.btn_add_desc_of_imp:
				savedb();
				add_new();
				break;
			case R.id.btn_save_desc_of_imp:
				savedb();
				finish();
				break;
			default:
				break;
		}
	}
	public void savedb(){
		ArrayList<String>field = new ArrayList<String>();
		field.clear();
		field.add("report_landimp_td_no");

		ArrayList<String> value = new ArrayList<String>();
		value.clear();
		value.add(gds.nullCheck3(report_td_no.getText().toString()));

		db.updateRecord(value, field, "record_id = ? ", new String[] { record_id }, "land_improvements");
	}

	public void add_new(){
		Toast.makeText(getApplicationContext(), "Add New",Toast.LENGTH_SHORT).show();
		myDialog = new Dialog(LI_Desc_Of_Imp.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.reports_land_desc_of_imp_form);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.FILL_PARENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);


		btn_create = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_create.setText("Create");
		btn_cancel.setText("Cancel");


		report_num_of_floors = (EditText) myDialog.findViewById(R.id.report_num_of_floors);
		report_desc_of_bldg = (EditText) myDialog.findViewById(R.id.report_desc_of_bldg);
		report_erected_on_lot = (EditText) myDialog.findViewById(R.id.report_erected_on_lot);
		report_fa = (EditText) myDialog.findViewById(R.id.report_fa);
		report_fa_per_td = (EditText) myDialog.findViewById(R.id.report_fa_per_td);
		report_actual_utilization = (EditText) myDialog.findViewById(R.id.report_actual_utilization);
		report_declaration_as_to_usage = (EditText) myDialog.findViewById(R.id.report_declaration_as_to_usage);
		report_declared_owner = (EditText) myDialog.findViewById(R.id.report_declared_owner);

		spinner_type_of_property = (Spinner) myDialog.findViewById(R.id.spinner_type_of_property);
		spinner_ownership_of_property = (Spinner) myDialog.findViewById(R.id.spinner_ownership_of_property);
		spinner_socialized_housing = (Spinner) myDialog.findViewById(R.id.spinner_socialized_housing);
		spinner_socialized_housing.setVisibility(View.GONE);
		report_foundation = (MultiAutoCompleteTextView) myDialog.findViewById(R.id.report_foundation);
		report_columns = (MultiAutoCompleteTextView) myDialog.findViewById(R.id.report_columns);
		report_beams = (MultiAutoCompleteTextView) myDialog.findViewById(R.id.report_beams);
		report_exterior_walls = (MultiAutoCompleteTextView) myDialog.findViewById(R.id.report_exterior_walls);
		report_interior_walls = (MultiAutoCompleteTextView) myDialog.findViewById(R.id.report_interior_walls);
		report_flooring = (MultiAutoCompleteTextView) myDialog.findViewById(R.id.report_flooring);
		report_doors = (MultiAutoCompleteTextView) myDialog.findViewById(R.id.report_doors);
		report_windows = (MultiAutoCompleteTextView) myDialog.findViewById(R.id.report_windows);
		report_ceiling = (MultiAutoCompleteTextView) myDialog.findViewById(R.id.report_ceiling);
		report_roofing = (MultiAutoCompleteTextView) myDialog.findViewById(R.id.report_roofing);
		report_trusses = (MultiAutoCompleteTextView) myDialog.findViewById(R.id.report_trusses);

		valrep_landimp_desc_stairs = (MultiAutoCompleteTextView) myDialog.findViewById(R.id.valrep_landimp_desc_stairs);

		report_economic_life = (EditText) myDialog.findViewById(R.id.report_economic_life);
		report_effective_age = (EditText) myDialog.findViewById(R.id.report_effective_age);
		report_remaining_eco_life = (EditText) myDialog.findViewById(R.id.report_remaining_eco_life);
		report_occupants = (EditText) myDialog.findViewById(R.id.report_occupants);
		report_owned_or_leased = (EditText) myDialog.findViewById(R.id.report_owned_or_leased);
		spinner_confirmed_thru = (Spinner) myDialog.findViewById(R.id.spinner_confirmed_thru);
		spinner_observed_condition = (Spinner) myDialog.findViewById(R.id.spinner_observed_condition);

		report_num_of_bedrooms = (EditText) myDialog.findViewById(R.id.report_num_of_bedrooms);
		valrep_landimp_impsummary1_no_of_tb = (EditText) myDialog.findViewById(R.id.valrep_townhouse_impsummary_no_of_tb);


		gds.multiautocomplete(report_foundation, getResources().getStringArray(R.array.report_foundation), this);
		gds.multiautocomplete(report_columns, getResources().getStringArray(R.array.report_columns), this);
		gds.multiautocomplete(report_beams, getResources().getStringArray(R.array.report_beams), this);
		gds.multiautocomplete(report_exterior_walls, getResources().getStringArray(R.array.report_exterior_walls), this);
		gds.multiautocomplete(report_interior_walls, getResources().getStringArray(R.array.report_interior_walls), this);
		gds.multiautocomplete(report_flooring, getResources().getStringArray(R.array.report_flooring), this);
		gds.multiautocomplete(report_doors, getResources().getStringArray(R.array.report_doors), this);
		gds.multiautocomplete(report_windows, getResources().getStringArray(R.array.report_windows), this);
		gds.multiautocomplete(report_ceiling, getResources().getStringArray(R.array.report_ceiling), this);
		gds.multiautocomplete(report_roofing, getResources().getStringArray(R.array.report_roofing), this);
		gds.multiautocomplete(report_trusses, getResources().getStringArray(R.array.report_trusses), this);
		gds.multiautocomplete(valrep_landimp_desc_stairs, getResources().getStringArray(R.array.valrep_landimp_desc_stairs), this);

		report_fa.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_fa, this, s, current);
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_fa_per_td.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_fa_per_td, this, s, current);
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});



		report_economic_life.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				String economic_life_val = gds.nullCheck2(report_economic_life.getText().toString());
				String effective_age_val = gds.nullCheck2(report_effective_age.getText().toString());
				String remaining_eco_life = gds.remainEcoLI(economic_life_val,effective_age_val);
				report_remaining_eco_life.setText(remaining_eco_life);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_effective_age.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				String economic_life_val = gds.nullCheck2(report_economic_life.getText().toString());
				String effective_age_val = gds.nullCheck2(report_effective_age.getText().toString());
				String remaining_eco_life = gds.remainEcoLI(economic_life_val,effective_age_val);
				report_remaining_eco_life.setText(remaining_eco_life);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});






		btn_create.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				//add data in SQLite
				db.addLand_Improvements_Imp_Details(new Land_Improvements_API_Imp_Details(
						record_id,
						report_num_of_floors.getText().toString(),
						report_desc_of_bldg.getText().toString(),
						report_erected_on_lot.getText().toString(),
						gds.replaceFormat(report_fa.getText().toString()),
						gds.replaceFormat(report_fa_per_td.getText().toString()),
						report_actual_utilization.getText().toString(),
						report_declaration_as_to_usage.getText().toString(),
						report_declared_owner.getText().toString(),

						spinner_socialized_housing.getSelectedItem().toString(),
						spinner_type_of_property.getSelectedItem().toString(),

						report_foundation.getText().toString(),
						report_columns.getText().toString(),
						report_beams.getText().toString(),
						report_exterior_walls.getText().toString(),
						report_interior_walls.getText().toString(),
						report_flooring.getText().toString(),
						report_doors.getText().toString(),
						report_windows.getText().toString(),
						report_ceiling.getText().toString(),
						report_roofing.getText().toString(),
						report_trusses.getText().toString(),
						report_economic_life.getText().toString(),
						report_effective_age.getText().toString(),
						report_remaining_eco_life.getText().toString(),
						report_occupants.getText().toString(),
						report_owned_or_leased.getText().toString(),
						gds.replaceFormat(report_fa.getText().toString()),
						spinner_confirmed_thru.getSelectedItem().toString(),
						spinner_observed_condition.getSelectedItem().toString(),
						spinner_ownership_of_property.getSelectedItem().toString(),
						report_num_of_bedrooms.getText().toString(),
						valrep_landimp_impsummary1_no_of_tb.getText().toString(),
						valrep_landimp_desc_stairs.getText().toString()
				));

				//advance computations for valutaion cost
				auto_compute_less_dep_percent();

				//sync with valuation cost
				db.addLand_Improvements_Cost_Valuation(new Land_Improvements_API_Cost_Valuation(
						record_id,
						report_desc_of_bldg.getText().toString(),//report_desc_of_bldg.getText().toString(),
						"",//report_total_area.getText().toString(),
						"",//report_total_reproduction_cost.getText().toString(),
						String.valueOf(df.format(less_dep_percent_val)),//report_depreciation.getText().toString(),
						"",//report_less_reproduction.getText().toString(),
						"",
						"",//valrep_landimp_imp_value_phys_cur_unit_value
						"",//valrep_landimp_imp_value_phys_cur_amt
						"",//valrep_landimp_imp_value_func_cur_unit_value
						"",//valrep_landimp_imp_value_func_cur_amt
						""//valrep_landimp_imp_value_less_cur
				));//report_depreciated_value.getText().toString()));
				Toast.makeText(getApplicationContext(),
						"data created", Toast.LENGTH_SHORT).show();
				myDialog.dismiss();
				reloadClass();

			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});

		gds.fill_in_error(new EditText[] {
				report_desc_of_bldg,
				report_foundation,
				report_columns,
				report_beams,
				report_exterior_walls,
				report_interior_walls,
				report_flooring,
				report_doors,
				report_windows,
				report_ceiling,
				report_roofing,
				report_trusses,
				valrep_landimp_desc_stairs,
				report_economic_life,
				report_effective_age,
				report_num_of_floors,
				report_num_of_bedrooms,
				report_occupants,
				valrep_landimp_impsummary1_no_of_tb});
		gds.fill_in_error_disabled(new EditText[]{report_fa});
		gds.fill_in_error_spinner(new Spinner[]{spinner_ownership_of_property,spinner_confirmed_thru});
		myDialog.show();
	}

	public void auto_compute_less_dep_percent(){
		economic_life_val = gds.StringtoDouble(report_economic_life.getText().toString());
		effective_age_val = gds.StringtoDouble(report_effective_age.getText().toString());
		less_dep_percent_val = ((effective_age_val/economic_life_val)*100);
	}

	//EditText checker
	private boolean validate(EditText[] fields){
		for(int i=0; i<fields.length; i++){
			EditText currentField=fields[i];
			if(currentField.getText().toString().length()<=0){
				return false;
			}
		}
		return true;
	}
	public void reloadClass(){
		Intent intent = getIntent();
		finish();
		startActivity(intent);
	}
	public void delete_item(){
		myDialog = new Dialog(LI_Desc_Of_Imp.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.delete_dialog);
		myDialog.setCancelable(true);
		final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
		final Button btn_yes = (Button) myDialog.findViewById(R.id.btn_yes);
		final Button btn_no = (Button) myDialog.findViewById(R.id.btn_no);
		tv_question.setText("Are you sure you want to delete the selected item?");
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//sub table details must be deleted first
				db.deleteLand_Improvements_API_Imp_Details_Features(record_id, imp_details_id);//delete desc of imp features (all)
				db.deleteLand_Improvements_API_Imp_Details_Single(record_id, imp_details_id);//delete desc of imp
				db.deleteLand_Improvements_Cost_Valuation_Details(record_id, imp_details_id);
				db.deleteLand_Improvements_Cost_Valuation(record_id, imp_details_id);

				//db.close();


				myDialog.dismiss();
				reloadClass();
				Toast.makeText(getApplicationContext(), "Data Deleted",
						Toast.LENGTH_SHORT).show();
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});

		myDialog.show();
	}
	// Response from LI_Desc_Of_Imp_View Activity
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// if result code 100
		if (resultCode == 100) {
			// if result code 100 is received
			// means user edited/deleted product
			// reload this screen again
			Intent intent = getIntent();
			finish();
			startActivity(intent);
		}
	}

	/**
	 * Background Async Task to Load all
	 * */
	class LoadAll extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(LI_Desc_Of_Imp.this);
			pDialog.setMessage("Loading List. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All
		 * */
		protected String doInBackground(String... args) {
			String building_desc="", type_of_property="";

			List<Land_Improvements_API_Imp_Details> liid = db.getLand_Improvements_Imp_Details(record_id);
			if (!liid.isEmpty()) {
				for (Land_Improvements_API_Imp_Details imid : liid) {
					imp_details_id = String.valueOf(imid.getID());
					imid.getreport_impsummary1_no_of_floors();
					building_desc = imid.getreport_impsummary1_building_desc();
					imid.getreport_impsummary1_erected_on_lot();
					imid.getreport_impsummary1_fa();
					imid.getreport_impsummary1_fa_per_td();
					imid.getreport_impsummary1_actual_utilization();
					imid.getreport_impsummary1_usage_declaration();
					imid.getreport_impsummary1_owner();
					type_of_property = imid.getreport_desc_property_type();
					imid.getreport_impsummary1_socialized_housing();

					HashMap<String, String> map = new HashMap<String, String>();
					map.put(tag_imp_details_id, imp_details_id);
					map.put(tag_building_desc, building_desc);
					map.put(tag_type_of_property, type_of_property);
					//Toast.makeText(getApplicationContext(), ""+itemList,Toast.LENGTH_SHORT).show();

					// adding HashList to ArrayList
					itemList.add(map);
				}
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					ListAdapter adapter = new SimpleAdapter(
							LI_Desc_Of_Imp.this, itemList,
							R.layout.reports_land_desc_of_imp_list, new String[] { tag_imp_details_id,
							tag_building_desc,
							tag_type_of_property},
							new int[] { R.id.primary_key, R.id.item_desc_of_bldg, R.id.item_type_of_property });
					// updating listview
					setListAdapter(adapter);
				}
			});

		}
	}
	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}


}