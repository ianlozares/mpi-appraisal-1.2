package com.gds.appraisalmaybank.construction_ebm;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.attachments.Attachments_Inflater;
import com.gds.appraisalmaybank.attachments.Collage;
import com.gds.appraisalmaybank.check_network.NetworkUtil;
import com.gds.appraisalmaybank.database.Construction_Ebm_API;
import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Images;
import com.gds.appraisalmaybank.database.Logs;
import com.gds.appraisalmaybank.database.Report_Accepted_Jobs;
import com.gds.appraisalmaybank.database.Report_Accepted_Jobs_Contacts;
import com.gds.appraisalmaybank.database.Report_filename;
import com.gds.appraisalmaybank.database.Required_Attachments;
import com.gds.appraisalmaybank.database2.Construction_Ebm_API_Room_List;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.json.MyHttpClient;
import com.gds.appraisalmaybank.json.TLSConnection;
import com.gds.appraisalmaybank.json.UserFunctions;
import com.gds.appraisalmaybank.main.Connectivity;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;
import com.gds.appraisalmaybank.report.View_Report;
import com.gds.appraisalmaybank.required_fields.Fields_Checker;
import com.gds.appraisalmaybank.townhouse_condo.Pdf_Townhouse_condo;

import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import javax.net.ssl.HttpsURLConnection;

@SuppressLint("InflateParams")
public class Construction_ebm extends Activity implements OnClickListener, OnCheckedChangeListener{
	ActionBar actionBar;
	private static final String TAG_UID = "uid";
	private static final String TAG_RECORD_ID = "record_id";
	private static final String TAG_PASS_STAT = "pass_stat";
	String pass_stat="offline";
	Double matrix_value=0.00;
	GDS_methods gds = new GDS_methods();
	TLSConnection tlscon = new TLSConnection();

	Session_Timer st = new Session_Timer(this);
	TextView tv_requested_month, tv_requested_day, tv_requested_year,
			tv_classification, tv_account_name,tv_company_name, tv_requesting_party,tv_requestor,
			tv_control_no, tv_appraisal_type, tv_nature_appraisal,
			tv_ins_month1, tv_ins_day1, tv_ins_year1, tv_ins_month2,
			tv_ins_day2, tv_ins_year2, tv_com_month, tv_com_day, tv_com_year,
			tv_tct_no, tv_unit_no, tv_bldg_name, tv_street_no, tv_street_name,
			tv_village, tv_district, tv_zip_code, tv_city, tv_province,
			tv_region, tv_country, tv_address, tv_attachment,
	tv_address_hide,tv_app_request_remarks,tv_app_kind_of_appraisal,tv_app_branch_code;
	LinearLayout ll_contact;
	DatabaseHandler db = new DatabaseHandler(this);
	DatabaseHandler2 db2 = new DatabaseHandler2(this);

	ArrayList<String> field = new ArrayList<String>();
	ArrayList<String> value = new ArrayList<String>();
	Global gs;
	String requested_month, requested_day, requested_year, classification,
			account_fname, account_mname, account_lname, requesting_party,requestor,
			control_no, appraisal_type, nature_appraisal, ins_month1, ins_day1,
			ins_year1, ins_month2, ins_day2, ins_year2, com_month, com_day,
			com_year, tct_no, unit_no, bldg_name, street_no, street_name,
			village, district, zip_code, city, province, region, country,
			counter, output_appraisal_type, output_street_name, output_village,
			output_city, output_province,app_request_remarks,app_kind_of_appraisal,app_branch_code,account_is_company,account_company_name;
	String lot_no, block_no;
	String record_id, pdf_name;
	ImageView btn_report_page1, btn_report_page2, btn_report_page3, btn_report_page4;
	Button btn_report_update_cc,btn_report_view_report;
	ProgressDialog pDialog;
	ImageView btn_select_pdf, btn_report_pdf;
	EditText et_first_name,et_middle_name,et_last_name,et_requestor,app_account_company_name;
	CheckBox app_account_is_company;
	Spinner spinner_purpose_appraisal;
	EditText et_app_kind_of_appraisal;
	AutoCompleteTextView et_requesting_party;
	EditText report_date_requested;
	Button btn_update_app_details, btn_save_app_details, btn_cancel_app_details;

	// for attachments
	int responseCode;
	//for fileUpload checking
	boolean fileUploaded = false;
	
	String google_response = "";
	ListView dlg_priority_lvw = null;
	Context context = this;
	String item, filename;
	String dir = "gds_appraisal";
	File myDirectory = new File(Environment.getExternalStorageDirectory() + "/"
			+ dir + "/");
	String attachment_selected, uid, file, candidate_done;
	int serverResponseCode = 0;
	String db_app_uid, db_app_file, db_app_filename, db_app_appraisal_type,
			db_app_candidate_done;
	// page1
	Dialog myDialog;
	DatePicker report_date_inspected;
	EditText report_time_inspected, report_proposed_project, report_floor_area, report_no_of_storey, report_economic_life;
	Spinner spinner_type_of_housing_unit;
	TextView type_construction,foundation,post,beams,floor,walls,partitions,windows,doors,ceiling,roof;
	CheckBox const_type_reinforced_concrete, const_type_semi_concrete, const_type_mixed_materials;
	Button btn_update_page1;
	int classification_position;
	// page2
	CheckBox foundation_concrete, foundation_other;
	EditText foundation_other_value;
	CheckBox post_concrete, post_concrete_timber, post_steel, post_other;
	EditText post_other_value;
	CheckBox beams_concrete, beams_timber, beams_steel, beams_other;
	EditText beams_other_value;
	CheckBox floors_concrete, floors_tiles,
			floors_tiles_cement, floors_laminated_wood,
			floors_ceramic_tiles, floors_wood_planks,
			floors_marble_washout, floors_concrete_boards,
			floors_granite_tiles, floors_marble_wood,
			floors_carpet, floors_other;
	EditText floors_other_value;
	CheckBox walls_chb, walls_chb_cement, walls_anay,
			walls_chb_wood, walls_precast,
			walls_decorative_stone, walls_adobe,
			walls_ceramic_tiles, walls_cast_in_place,
			walls_sandblast, walls_mactan_stone,
			walls_painted, walls_other;
	EditText walls_other_value;
	CheckBox partitions_chb, partitions_painted_cement,
			partitions_anay, partitions_wood_boards,
			partitions_precast, partitions_decorative_stone,
			partitions_adobe, partitions_granite,
			partitions_cast_in_place, partitions_sandblast,
			partitions_mactan_stone, partitions_ceramic_tiles,
			partitions_chb_plywood, partitions_hardiflex,
			partitions_wallpaper, partitions_painted,
			partitions_other;
	EditText partitions_other_value;
	CheckBox windows_steel_casement, windows_fixed_view,
			windows_analok_sliding, windows_alum_glass,
			windows_aluminum_sliding, windows_awning_type,
			windows_powder_coated, windows_wooden_frame,
			windows_other;
	EditText windows_other_value;
	CheckBox doors_wood_panel, doors_pvc,
			doors_analok_sliding, doors_screen_door,
			doors_flush, doors_molded_door,
			doors_aluminum_sliding, doors_flush_french,
			doors_other;
	EditText doors_other_value;
	CheckBox ceiling_plywood, ceiling_painted_gypsum,
			ceiling_soffit_slab, ceiling_metal_deck,
			ceiling_hardiflex, ceiling_plywood_tg,
			ceiling_plywood_pvc, ceiling_painted,
			ceiling_with_cornice, ceiling_with_moulding,
			ceiling_drop_ceiling, ceiling_other;
	EditText ceiling_other_value;
	CheckBox roof_pre_painted, roof_rib_type,
			roof_tilespan, roof_tegula_asphalt,
			roof_tegula_longspan, roof_tegula_gi,
			roof_steel_concrete, roof_polycarbonate,
			roof_on_steel_trusses, roof_on_wooden_trusses,
			roof_other;
	EditText roof_other_value;
	
	Button btn_update_page2;
	// page3
	Button btn_update_page3;
	
	//page4
	EditText et_remarks;
	Button btn_clear, btn_default;
	Button btn_update_page4;
	
	//address
	Button btn_update_address, btn_save_address;
	EditText et_unit_no, et_building_name, et_lot_no, et_block_no, et_street_no, et_street_name,
			et_village, et_district, et_zip_code, et_city, et_province,
			et_region, et_country;
	
	//case center attachments
	ImageView btn_mysql_attachments;
	
	Button btn_manual_delete;
	
	// uploaded attachments
	LinearLayout container_attachments;
	String uploaded_attachment;
	String url_webby;
	UserFunctions userFunction = new UserFunctions();
	File uploaded_file;
	String[] commandArray = new String[] { "View Attachment in Browser",
			"Download Attachment" };
	// for API
	String xml;
	DefaultHttpClient httpClient = new MyHttpClient(this);
	boolean network_status;
	// Progress dialog type (0 - for Horizontal progress bar)
	public static final int progress_bar_type = 0;

	TextView tv_rework_reason_header, tv_rework_reason;
	String rework_reason;
	boolean withAttachment = false, freshData = true;
	boolean appStatReviewer = false;
	
	//detect internet connection
	TextView tv_connection;
	boolean wifi = false;
	boolean data = false;
	boolean data_speed = false;
	boolean freeFields;
	private NetworkChangeReceiver receiver;
	private boolean isConnected = false;
	boolean globalSave=false;
	String where = "WHERE record_id = args";
	String[] args = new String[1];

	public class NetworkChangeReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(final Context context, final Intent intent) {
			isNetworkAvailable(context);
		}

		private boolean isNetworkAvailable(final Context context) {
			ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivity != null) {
				NetworkInfo[] info = connectivity.getAllNetworkInfo();
				if (info != null) {
					for (int i = 0; i < info.length; i++) {
						if (info[i].getState() == NetworkInfo.State.CONNECTED) {
							if (!isConnected) {
								tv_connection.setText("Connected");
								tv_connection.setBackgroundColor(Color.parseColor("#3399ff"));
								isConnected = true;
								// do your processing here ---
								final Handler handler = new Handler();
								handler.postDelayed(new Runnable() {
									@Override
									public void run() {
										// Do something after 1s = 1000ms
										wifi = Connectivity.isConnectedWifi(context);
										data = Connectivity.isConnectedMobile(context);
										data_speed = Connectivity.isConnected(context);
										if (wifi) {
											tv_connection.setText(R.string.wifi_connected);
											tv_connection.setBackgroundColor(Color.parseColor("#39b476"));
										} else if (data) {
											if (data_speed) {
												tv_connection.setText(R.string.data_good);
												tv_connection.setBackgroundColor(Color.parseColor("#39b476"));
											} else if (data_speed==false){
												tv_connection.setText(R.string.data_weak);
												tv_connection.setBackgroundColor(Color.parseColor("#FF9900"));
											}
										}
									}
								}, 1000);

							}
							return true;
						}
					}
				}
			}
			tv_connection.setText("No Internet connection");
			tv_connection.setBackgroundColor(Color.parseColor("#CC0000"));
			isConnected = false;
			return false;
		}
	}
	//detect internet connection
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.construction_ebm);

		st.resetDisconnectTimer();
		Authenticator.setDefault(new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("gdsuser", "gdsuser01".toCharArray());
			}
		});
		actionBar = getActionBar();
		actionBar.setIcon(R.drawable.ic_cebm);
		actionBar.setSubtitle("Construction EBM");
		
		tv_connection = (TextView) findViewById(R.id.tv_connection);
		tv_connection.setVisibility(View.GONE);
		/*IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
		receiver = new NetworkChangeReceiver();
		registerReceiver(receiver, filter);*/
		
		gs = ((Global) getApplicationContext());
		container_attachments = (LinearLayout) findViewById(R.id.container_attachments);
		ll_contact = (LinearLayout) findViewById(R.id.container);
		tv_requested_month = (TextView) findViewById(R.id.tv_requested_month);
		tv_requested_day = (TextView) findViewById(R.id.tv_requested_day);
		tv_requested_year = (TextView) findViewById(R.id.tv_requested_year);
		tv_classification = (TextView) findViewById(R.id.tv_classification);
		tv_account_name = (TextView) findViewById(R.id.tv_account_name);
		tv_company_name = (TextView) findViewById(R.id.tv_company_name);
		tv_requesting_party = (TextView) findViewById(R.id.tv_requesting_party);
		tv_requestor = (TextView) findViewById(R.id.tv_requestor);
		tv_control_no = (TextView) findViewById(R.id.tv_control_no);
		tv_appraisal_type = (TextView) findViewById(R.id.tv_appraisal_type);
		tv_nature_appraisal = (TextView) findViewById(R.id.tv_nature_appraisal);
		tv_ins_month1 = (TextView) findViewById(R.id.tv_ins_month1);
		tv_ins_day1 = (TextView) findViewById(R.id.tv_ins_day1);
		tv_ins_year1 = (TextView) findViewById(R.id.tv_ins_year1);
		tv_ins_month2 = (TextView) findViewById(R.id.tv_ins_month2);
		tv_ins_day2 = (TextView) findViewById(R.id.tv_ins_day2);
		tv_ins_year2 = (TextView) findViewById(R.id.tv_ins_year2);
		tv_com_month = (TextView) findViewById(R.id.tv_com_month);
		tv_com_day = (TextView) findViewById(R.id.tv_com_day);
		tv_com_year = (TextView) findViewById(R.id.tv_com_year);
		tv_tct_no = (TextView) findViewById(R.id.tv_tct_no);
		tv_unit_no = (TextView) findViewById(R.id.tv_unit_no);
		tv_bldg_name = (TextView) findViewById(R.id.tv_bldg_name);
		tv_street_no = (TextView) findViewById(R.id.tv_street_no);
		tv_street_name = (TextView) findViewById(R.id.tv_street_name);
		tv_village = (TextView) findViewById(R.id.tv_village);
		tv_district = (TextView) findViewById(R.id.tv_district);
		tv_zip_code = (TextView) findViewById(R.id.tv_zip_code);
		tv_city = (TextView) findViewById(R.id.tv_city);
		tv_province = (TextView) findViewById(R.id.tv_province);
		tv_region = (TextView) findViewById(R.id.tv_region);
		tv_country = (TextView) findViewById(R.id.tv_country);
		tv_address = (TextView) findViewById(R.id.tv_address);
		tv_attachment = (TextView) findViewById(R.id.tv_attachment);
		btn_report_page1 = (ImageView) findViewById(R.id.btn_report_page1);
		btn_report_page2 = (ImageView) findViewById(R.id.btn_report_page2);
		btn_report_page3 = (ImageView) findViewById(R.id.btn_report_page3);
		btn_report_page3 = (ImageView) findViewById(R.id.btn_report_page3);
		btn_report_page4 = (ImageView) findViewById(R.id.btn_report_page4);
		btn_report_update_cc = (Button) findViewById(R.id.btn_report_update_cc);
		btn_report_view_report = (Button) findViewById(R.id.btn_report_view_report);
		btn_report_pdf = (ImageView) findViewById(R.id.btn_report_pdf);
		btn_select_pdf = (ImageView) findViewById(R.id.btn_select_pdf);
		tv_app_request_remarks = (TextView) findViewById(R.id.tv_app_request_remarks);
		tv_app_kind_of_appraisal = (TextView) findViewById(R.id.tv_app_kind_of_appraisal);
		tv_app_branch_code = (TextView) findViewById(R.id.tv_app_branch_code);
		tv_address_hide= (TextView) findViewById(R.id.tv_address_hide);
//app details
		btn_update_app_details = (Button) findViewById(R.id.btn_update_app_details);
		btn_update_app_details.setOnClickListener(this);

		btn_report_page1.setOnClickListener(this);
		btn_report_page2.setOnClickListener(this);
		btn_report_page3.setOnClickListener(this);
		btn_report_page4.setOnClickListener(this);
		btn_report_update_cc.setOnClickListener(this);
		btn_report_view_report.setOnClickListener(this);
		btn_report_pdf.setOnClickListener(this);
		btn_select_pdf.setOnClickListener(this);
		tv_attachment.setOnClickListener(this);
		
		//case center attachments
		btn_mysql_attachments = (ImageView) findViewById(R.id.btn_mysql_attachments);
		btn_mysql_attachments.setOnClickListener(this);
		//address
		btn_update_address = (Button) findViewById(R.id.btn_update_address);
		btn_update_address.setOnClickListener(this);
		
		tv_rework_reason_header = (TextView) findViewById(R.id.tv_rework_reason_header);
		tv_rework_reason = (TextView) findViewById(R.id.tv_rework_reason);
		
		btn_manual_delete = (Button) findViewById(R.id.btn_manual_delete);
		btn_manual_delete.setOnClickListener(this);
		
		record_id = gs.record_id;
		args[0] = record_id;
		uid = db2.getRecord("app_main_id", where, args, "tbl_report_accepted_jobs").get(0);
		fill_details();
		uploaded_attachments();
		statusDisplay();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater menuInflater = getMenuInflater();
		menuInflater.inflate(R.menu.menu, menu);
		menu.findItem(R.id.map).setVisible(true);
		menu.findItem(R.id.logs).setVisible(true);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.logs:
			logsDisplay();
			return true;
			case R.id.map:
				view_map();
				return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void fill_details() {
		List<Report_Accepted_Jobs> report_accepted_jobs = db
				.getReport_Accepted_Jobs(record_id);
		if (!report_accepted_jobs.isEmpty()) {
			for (Report_Accepted_Jobs im : report_accepted_jobs) {
				requested_month = im.getdr_month();
				requested_day = im.getdr_day();
				requested_year = im.getdr_year();
				classification = im.getclassification();
				account_fname = im.getfname();
				account_mname = im.getmname();
				account_lname = im.getlname();
				requesting_party = im.getrequesting_party();
				requestor = im.getrequestor();
				control_no = im.getcontrol_no();
				appraisal_type = im.getappraisal_type();
				nature_appraisal = im.getnature_appraisal();
				ins_month1 = im.getins_date1_month();
				ins_day1 = im.getins_date1_day();
				ins_year1 = im.getins_date1_year();
				ins_month2 = im.getins_date2_month();
				ins_day2 = im.getins_date2_day();
				ins_year2 = im.getins_date2_year();
				com_month = im.getcom_month();
				com_day = im.getcom_day();
				com_year = im.getcom_year();
				tct_no = im.gettct_no();
				unit_no = im.getunit_no();
				bldg_name = im.getbuilding_name();
				lot_no = im.getlot_no();
				block_no = im.getblock_no();
				street_no = im.getstreet_no();
				street_name = im.getstreet_name();
				village = im.getvillage();
				district = im.getdistrict();
				zip_code = im.getzip_code();
				city = im.getcity();
				province = im.getprovince();
				region = im.getregion();
				country = im.getcountry();
				counter = im.getcounter();
				app_request_remarks = im.getapp_request_remarks();
				app_kind_of_appraisal = im.getkind_of_appraisal();
				app_branch_code = im.getapp_branch_code();
				rework_reason  = im.getrework_reason();
				args[0]=record_id;
				account_is_company=db2.getRecord("app_account_is_company",where,args,"tbl_report_accepted_jobs").get(0);
				account_company_name=db2.getRecord("app_account_company_name",where,args,"tbl_report_accepted_jobs").get(0);
			}

			args[0]=record_id;
			if(db2.getRecord("application_status",where,args,"tbl_report_accepted_jobs").get(0).contentEquals("for_rework")){
				tv_rework_reason_header.setVisibility(View.VISIBLE);
				tv_rework_reason.setVisibility(View.VISIBLE);
				tv_rework_reason.setText(rework_reason);
			}
			List<Report_Accepted_Jobs_Contacts> report_Accepted_Jobs_Contacts = db
					.getReport_Accepted_Jobs_Contacts(record_id);
			if (!report_Accepted_Jobs_Contacts.isEmpty()) {
				for (Report_Accepted_Jobs_Contacts im : report_Accepted_Jobs_Contacts) {
					LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
							.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					final View addView = layoutInflater.inflate(
							R.layout.request_jobs_info_contact_layout, null);

					final TextView tv_contact_person = (TextView) addView
							.findViewById(R.id.tv_contact_person);
					final TextView tv_contact_prefix = (TextView) addView
							.findViewById(R.id.tv_contact_prefix);
					final TextView tv_contact_mobile = (TextView) addView
							.findViewById(R.id.tv_contact_mobile);
					final TextView tv_contact_landline = (TextView) addView
							.findViewById(R.id.tv_contact_landline);

					tv_contact_person.setText(im.getcontact_person());
					tv_contact_prefix.setText(im.getcontact_mobile_no_prefix());
					tv_contact_mobile.setText(im.getcontact_mobile_no());
					tv_contact_landline.setText(im.getcontact_landline());
					ll_contact.addView(addView);
				}
			}
			List<Report_filename> rf = db.getReport_filename(record_id);
			if (!rf.isEmpty()) {
				for (Report_filename im : rf) {
					tv_attachment.setText(im.getfilename());
				}
			}
			tv_requested_month.setText(requested_month + "/");
			tv_requested_day.setText(requested_day + "/");
			tv_requested_year.setText(requested_year);
			tv_classification.setText(classification);
			tv_account_name.setText(account_fname + " " + account_mname + " "
					+ account_lname);
			tv_company_name.setText(account_company_name);
			if(account_is_company.contentEquals("true")){
				actionBar.setTitle(account_company_name);
			}else{
				actionBar.setTitle(account_fname + " " + account_mname + " " + account_lname);
			}
			tv_requesting_party.setText(requesting_party);
			tv_requestor.setText(requestor);
			tv_control_no.setText(control_no);
			for (int x = 0; x < gs.appraisal_type_value.length; x++) {
				if (appraisal_type.equals(gs.appraisal_type_value[x])) {
					output_appraisal_type = gs.appraisal_output[x];
				}
			}
			tv_appraisal_type.setText(output_appraisal_type);
			tv_nature_appraisal.setText(nature_appraisal);
			tv_ins_month1.setText(ins_month1 + "/");
			tv_ins_day1.setText(ins_day1 + "/");
			tv_ins_year1.setText(ins_year1);
			tv_ins_month2.setText(ins_month2 + "/");
			tv_ins_day2.setText(ins_day2 + "/");
			tv_ins_year2.setText(ins_year2);
			tv_com_month.setText(com_month + "/");
			tv_com_day.setText(com_day + "/");
			tv_com_year.setText(com_year);

			tv_tct_no.setText(tct_no);
			tv_unit_no.setText(unit_no);
			tv_bldg_name.setText(bldg_name);
			tv_street_no.setText(street_no);
			tv_street_name.setText(street_name);
			tv_village.setText(village);
			tv_district.setText(district);
			tv_zip_code.setText(zip_code);
			tv_city.setText(city);
			tv_province.setText(province);
			tv_region.setText(region);
			tv_country.setText(country);
			tv_app_request_remarks.setText(app_request_remarks);
			tv_app_kind_of_appraisal.setText(app_kind_of_appraisal);
			tv_app_branch_code.setText(app_branch_code);
			tv_address_hide.setText(street_name+" "+city+" "+region+" "+country);
			if (!street_name.equals("")) {
				output_street_name = street_name + ", ";
			} else {
				output_street_name = street_name;
			}
			if (!village.equals("")) {
				output_village = village + ", ";
			} else {
				output_village = village;
			}
			if (!city.equals("")) {
				output_city = city + ", ";
			} else {
				output_city = city;
			}
			if (!province.equals("")) {
				output_province = province + ", ";
			} else {
				output_province = province;
			}
			tv_address.setText(unit_no + " " + bldg_name + " " + lot_no + " " + block_no
					+ " " + output_street_name + output_village + district
					+ " " + output_city + region + " " + output_province
					+ country + " " + zip_code);
		}
	}
	
	public void custom_dialog_page1() {
		myDialog = new Dialog(Construction_ebm.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.report_construction_ebm_page1);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.FILL_PARENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		
		report_date_inspected = (DatePicker) myDialog
				.findViewById(R.id.report_date_inspected);
		report_time_inspected = (EditText) myDialog
				.findViewById(R.id.report_time_inspected);
		report_proposed_project = (EditText) myDialog
				.findViewById(R.id.report_proposed_project);
		report_floor_area = (EditText) myDialog
				.findViewById(R.id.report_floor_area);
		report_no_of_storey = (EditText) myDialog
				.findViewById(R.id.report_no_of_storey);
		report_economic_life = (EditText) myDialog
				.findViewById(R.id.report_economic_life);
		spinner_type_of_housing_unit = (Spinner) myDialog
				.findViewById(R.id.spinner_type_of_housing_unit);
		const_type_reinforced_concrete = (CheckBox) myDialog
				.findViewById(R.id.const_type_reinforced_concrete);
		const_type_semi_concrete = (CheckBox) myDialog
				.findViewById(R.id.const_type_semi_concrete);
		const_type_mixed_materials = (CheckBox) myDialog
				.findViewById(R.id.const_type_mixed_materials);
		type_construction= (TextView) myDialog.findViewById(R.id.type_construction);

		/*report_floor_area.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_floor_area, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});*/

		btn_update_page1 = (Button) myDialog
				.findViewById(R.id.btn_update_page1);
		btn_update_page1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub


					Construction_Ebm_API ce = new Construction_Ebm_API();
					//date
					if (String.valueOf(report_date_inspected.getMonth()).length() == 1) {//if month is 1 digit add 0 to initial +1 to value
						if (String.valueOf(report_date_inspected.getMonth())
								.equals("9")) {//if october==9 just add 1
							ce.setreport_date_inspected_month(String
									.valueOf(report_date_inspected.getMonth() + 1));
						} else {
							ce.setreport_date_inspected_month("0"
									+ String.valueOf(report_date_inspected
									.getMonth() + 1));
						}
					} else {
						ce.setreport_date_inspected_month(String
								.valueOf(report_date_inspected.getMonth() + 1));
					}
					ce.setreport_date_inspected_day(String
							.valueOf(report_date_inspected.getDayOfMonth()));
					ce.setreport_date_inspected_year(String
							.valueOf(report_date_inspected.getYear()));

					ce.setreport_time_inspected(report_time_inspected.getText().toString());
					ce.setreport_project_type(report_proposed_project.getText().toString());
					ce.setreport_floor_area(report_floor_area.getText().toString());
					ce.setreport_storeys(report_no_of_storey.getText().toString());
					ce.setreport_expected_economic_life(report_economic_life.getText().toString());

					ce.setreport_type_of_housing_unit(spinner_type_of_housing_unit.getSelectedItem().toString());

					if (const_type_reinforced_concrete.isChecked()) {
						ce.setreport_const_type_reinforced_concrete("true");
					} else {
						ce.setreport_const_type_reinforced_concrete("");
					}
					if (const_type_semi_concrete.isChecked()) {
						ce.setreport_const_type_semi_concrete("true");
					} else {
						ce.setreport_const_type_semi_concrete("");
					}
					if (const_type_mixed_materials.isChecked()) {
						ce.setreport_const_type_mixed_materials("true");
					} else {
						ce.setreport_const_type_mixed_materials("");
					}
					db.updateConstruction_Ebm_page1(ce, record_id);
					db.close();
					Toast.makeText(getApplicationContext(), "Saved",
							Toast.LENGTH_SHORT).show();
					myDialog.dismiss();

			}
		});
		List<Construction_Ebm_API> ce = db.getConstruction_Ebm(record_id);
		if (!ce.isEmpty()) {
			for (Construction_Ebm_API im : ce) {
				//date of inspection
				// set current date into datepicker
				if ((!im.getreport_date_inspected_year().equals(""))&&(!im.getreport_date_inspected_month().equals(""))) {
					report_date_inspected.init(
							Integer.parseInt(im.getreport_date_inspected_year()),
							Integer.parseInt(im.getreport_date_inspected_month())-1,
							Integer.parseInt(im.getreport_date_inspected_day()),
							null);
				} else {
					Calendar c = Calendar.getInstance();
					int mYear = c.get(Calendar.YEAR);
					int mMonth = c.get(Calendar.MONTH);
					int mDay = c.get(Calendar.DAY_OF_MONTH);
					report_date_inspected.init(mYear,mMonth,mDay,null);
				}
				
				report_time_inspected.setText(im.getreport_time_inspected());
				report_proposed_project.setText(im.getreport_project_type());
				report_floor_area.setText(im.getreport_floor_area());
				report_no_of_storey.setText(im.getreport_storeys());
				report_economic_life.setText(im.getreport_expected_economic_life());
				spinner_type_of_housing_unit.setSelection(gds.spinnervalue(spinner_type_of_housing_unit,im.getreport_type_of_housing_unit()));

				
				if (im.getreport_const_type_reinforced_concrete().equals("true")) {
					const_type_reinforced_concrete.setChecked(true);
				}
				if (im.getreport_const_type_semi_concrete().equals("true")) {
					const_type_semi_concrete.setChecked(true);
				}
				if (im.getreport_const_type_mixed_materials().equals("true")) {
					const_type_mixed_materials.setChecked(true);
				}

			}
		}
		gds.fill_in_error(new EditText[]{
				report_proposed_project,
				report_floor_area,report_no_of_storey,report_economic_life});

		if (const_type_reinforced_concrete.isChecked() || const_type_semi_concrete.isChecked() ||
				const_type_mixed_materials.isChecked()) {

			type_construction.setText("Type of Construction");
			type_construction.setTextColor(Color.BLACK);
		} else {

			type_construction.setText("Type of Construction: Please select at least one");
			type_construction.setTextColor(Color.RED);

		}
		myDialog.show();
	}

	public void custom_dialog_page2() {
		myDialog = new Dialog(Construction_ebm.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.report_construction_ebm_page2);
		// myDialog.setTitle("My Dialog");
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.FILL_PARENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);

		foundation_concrete = (CheckBox) myDialog.findViewById(R.id.foundation_concrete);
		foundation_other = (CheckBox) myDialog.findViewById(R.id.foundation_other);
		foundation_other.setOnCheckedChangeListener(this);
		foundation_other_value = (EditText) myDialog.findViewById(R.id.foundation_other_value);
		foundation_other_value.setVisibility(View.GONE);
		
		post_concrete = (CheckBox) myDialog.findViewById(R.id.post_concrete);
		post_concrete_timber = (CheckBox) myDialog.findViewById(R.id.post_concrete_timber);
		post_steel = (CheckBox) myDialog.findViewById(R.id.post_steel);
		post_other = (CheckBox) myDialog.findViewById(R.id.post_other);
		post_other.setOnCheckedChangeListener(this);
		post_other_value = (EditText) myDialog.findViewById(R.id.post_other_value);
		post_other_value.setVisibility(View.GONE);

		beams_concrete = (CheckBox) myDialog.findViewById(R.id.beams_concrete);
		beams_timber = (CheckBox) myDialog.findViewById(R.id.beams_timber);
		beams_steel = (CheckBox) myDialog.findViewById(R.id.beams_steel);
		beams_other = (CheckBox) myDialog.findViewById(R.id.beams_other);
		beams_other.setOnCheckedChangeListener(this);
		beams_other_value = (EditText) myDialog.findViewById(R.id.beams_other_value);
		beams_other_value.setVisibility(View.GONE);

		floors_concrete = (CheckBox) myDialog.findViewById(R.id.floors_concrete);
		floors_tiles = (CheckBox) myDialog.findViewById(R.id.floors_tiles);
		floors_tiles_cement = (CheckBox) myDialog.findViewById(R.id.floors_tiles_cement);
		floors_laminated_wood = (CheckBox) myDialog.findViewById(R.id.floors_laminated_wood);
		floors_ceramic_tiles = (CheckBox) myDialog.findViewById(R.id.floors_ceramic_tiles);
		floors_wood_planks = (CheckBox) myDialog.findViewById(R.id.floors_wood_planks);
		floors_marble_washout = (CheckBox) myDialog.findViewById(R.id.floors_marble_washout);
		floors_concrete_boards = (CheckBox) myDialog.findViewById(R.id.floors_concrete_boards);
		floors_granite_tiles = (CheckBox) myDialog.findViewById(R.id.floors_granite_tiles);
		floors_marble_wood = (CheckBox) myDialog.findViewById(R.id.floors_marble_wood);
		floors_carpet = (CheckBox) myDialog.findViewById(R.id.floors_carpet);
		floors_other = (CheckBox) myDialog.findViewById(R.id.floors_other);
		floors_other.setOnCheckedChangeListener(this);
		floors_other_value = (EditText) myDialog.findViewById(R.id.floors_other_value);
		floors_other_value.setVisibility(View.GONE);

		walls_chb = (CheckBox) myDialog.findViewById(R.id.walls_chb);
		walls_chb_cement = (CheckBox) myDialog.findViewById(R.id.walls_chb_cement);
		walls_anay = (CheckBox) myDialog.findViewById(R.id.walls_anay);
		walls_chb_wood = (CheckBox) myDialog.findViewById(R.id.walls_chb_wood);
		walls_precast = (CheckBox) myDialog.findViewById(R.id.walls_precast);
		walls_decorative_stone = (CheckBox) myDialog.findViewById(R.id.walls_decorative_stone);
		walls_adobe = (CheckBox) myDialog.findViewById(R.id.walls_adobe);
		walls_ceramic_tiles = (CheckBox) myDialog.findViewById(R.id.walls_ceramic_tiles);
		walls_cast_in_place = (CheckBox) myDialog.findViewById(R.id.walls_cast_in_place);
		walls_sandblast = (CheckBox) myDialog.findViewById(R.id.walls_sandblast);
		walls_mactan_stone = (CheckBox) myDialog.findViewById(R.id.walls_mactan_stone);
		walls_painted = (CheckBox) myDialog.findViewById(R.id.walls_painted);
		walls_other = (CheckBox) myDialog.findViewById(R.id.walls_other);
		walls_other.setOnCheckedChangeListener(this);
		walls_other_value = (EditText) myDialog.findViewById(R.id.walls_other_value);
		walls_other_value.setVisibility(View.GONE);

		partitions_chb = (CheckBox) myDialog.findViewById(R.id.partitions_chb);
		partitions_painted_cement = (CheckBox) myDialog.findViewById(R.id.partitions_painted_cement);
		partitions_anay = (CheckBox) myDialog.findViewById(R.id.partitions_anay);
		partitions_wood_boards = (CheckBox) myDialog.findViewById(R.id.partitions_wood_boards);
		partitions_precast = (CheckBox) myDialog.findViewById(R.id.partitions_precast);
		partitions_decorative_stone = (CheckBox) myDialog.findViewById(R.id.partitions_decorative_stone);
		partitions_adobe = (CheckBox) myDialog.findViewById(R.id.partitions_adobe);
		partitions_granite = (CheckBox) myDialog.findViewById(R.id.partitions_granite);
		partitions_cast_in_place = (CheckBox) myDialog.findViewById(R.id.partitions_cast_in_place);
		partitions_sandblast = (CheckBox) myDialog.findViewById(R.id.partitions_sandblast);
		partitions_mactan_stone = (CheckBox) myDialog.findViewById(R.id.partitions_mactan_stone);
		partitions_ceramic_tiles = (CheckBox) myDialog.findViewById(R.id.partitions_ceramic_tiles);
		partitions_chb_plywood = (CheckBox) myDialog.findViewById(R.id.partitions_chb_plywood);
		partitions_hardiflex = (CheckBox) myDialog.findViewById(R.id.partitions_hardiflex);
		partitions_wallpaper = (CheckBox) myDialog.findViewById(R.id.partitions_wallpaper);
		partitions_painted = (CheckBox) myDialog.findViewById(R.id.partitions_painted);
		partitions_other = (CheckBox) myDialog.findViewById(R.id.partitions_other);
		partitions_other.setOnCheckedChangeListener(this);
		partitions_other_value = (EditText) myDialog.findViewById(R.id.partitions_other_value);
		partitions_other_value.setVisibility(View.GONE);

		windows_steel_casement = (CheckBox) myDialog.findViewById(R.id.windows_steel_casement);
		windows_fixed_view = (CheckBox) myDialog.findViewById(R.id.windows_fixed_view);
		windows_analok_sliding = (CheckBox) myDialog.findViewById(R.id.windows_analok_sliding);
		windows_alum_glass = (CheckBox) myDialog.findViewById(R.id.windows_alum_glass);
		windows_aluminum_sliding = (CheckBox) myDialog.findViewById(R.id.windows_aluminum_sliding);
		windows_awning_type = (CheckBox) myDialog.findViewById(R.id.windows_awning_type);
		windows_powder_coated = (CheckBox) myDialog.findViewById(R.id.windows_powder_coated);
		windows_wooden_frame = (CheckBox) myDialog.findViewById(R.id.windows_wooden_frame);
		windows_other = (CheckBox) myDialog.findViewById(R.id.windows_other);
		windows_other.setOnCheckedChangeListener(this);
		windows_other_value = (EditText) myDialog.findViewById(R.id.windows_other_value);
		windows_other_value.setVisibility(View.GONE);

		doors_wood_panel = (CheckBox) myDialog.findViewById(R.id.doors_wood_panel);
		doors_pvc = (CheckBox) myDialog.findViewById(R.id.doors_pvc);
		doors_analok_sliding = (CheckBox) myDialog.findViewById(R.id.doors_analok_sliding);
		doors_screen_door = (CheckBox) myDialog.findViewById(R.id.doors_screen_door);
		doors_flush = (CheckBox) myDialog.findViewById(R.id.doors_flush);
		doors_molded_door = (CheckBox) myDialog.findViewById(R.id.doors_molded_door);
		doors_aluminum_sliding = (CheckBox) myDialog.findViewById(R.id.doors_aluminum_sliding);
		doors_flush_french = (CheckBox) myDialog.findViewById(R.id.doors_flush_french);
		doors_other = (CheckBox) myDialog.findViewById(R.id.doors_other);
		doors_other.setOnCheckedChangeListener(this);
		doors_other_value = (EditText) myDialog.findViewById(R.id.doors_other_value);
		doors_other_value.setVisibility(View.GONE);

		ceiling_plywood = (CheckBox) myDialog.findViewById(R.id.ceiling_plywood);
		ceiling_painted_gypsum = (CheckBox) myDialog.findViewById(R.id.ceiling_painted_gypsum);
		ceiling_soffit_slab = (CheckBox) myDialog.findViewById(R.id.ceiling_soffit_slab);
		ceiling_metal_deck = (CheckBox) myDialog.findViewById(R.id.ceiling_metal_deck);
		ceiling_hardiflex = (CheckBox) myDialog.findViewById(R.id.ceiling_hardiflex);
		ceiling_plywood_tg = (CheckBox) myDialog.findViewById(R.id.ceiling_plywood_tg);
		ceiling_plywood_pvc = (CheckBox) myDialog.findViewById(R.id.ceiling_plywood_pvc);
		ceiling_painted = (CheckBox) myDialog.findViewById(R.id.ceiling_painted);
		ceiling_with_cornice = (CheckBox) myDialog.findViewById(R.id.ceiling_with_cornice);
		ceiling_with_moulding = (CheckBox) myDialog.findViewById(R.id.ceiling_with_moulding);
		ceiling_drop_ceiling = (CheckBox) myDialog.findViewById(R.id.ceiling_drop_ceiling);
		ceiling_other = (CheckBox) myDialog.findViewById(R.id.ceiling_other);
		ceiling_other.setOnCheckedChangeListener(this);
		ceiling_other_value = (EditText) myDialog.findViewById(R.id.ceiling_other_value);
		ceiling_other_value.setVisibility(View.GONE);

		roof_pre_painted = (CheckBox) myDialog.findViewById(R.id.roof_pre_painted);
		roof_rib_type = (CheckBox) myDialog.findViewById(R.id.roof_rib_type);
		roof_tilespan = (CheckBox) myDialog.findViewById(R.id.roof_tilespan);
		roof_tegula_asphalt = (CheckBox) myDialog.findViewById(R.id.roof_tegula_asphalt);
		roof_tegula_longspan = (CheckBox) myDialog.findViewById(R.id.roof_tegula_longspan);
		roof_tegula_gi = (CheckBox) myDialog.findViewById(R.id.roof_tegula_gi);
		roof_steel_concrete = (CheckBox) myDialog.findViewById(R.id.roof_steel_concrete);
		roof_polycarbonate = (CheckBox) myDialog.findViewById(R.id.roof_polycarbonate);
		roof_on_steel_trusses = (CheckBox) myDialog.findViewById(R.id.roof_on_steel_trusses);
		roof_on_wooden_trusses = (CheckBox) myDialog.findViewById(R.id.roof_on_wooden_trusses);
		roof_other = (CheckBox) myDialog.findViewById(R.id.roof_other);
		roof_other.setOnCheckedChangeListener(this);
		roof_other_value = (EditText) myDialog.findViewById(R.id.roof_other_value);
		roof_other_value.setVisibility(View.GONE);

		foundation = (TextView) myDialog.findViewById(R.id.foundation);
		post = (TextView) myDialog.findViewById(R.id.post);
		beams = (TextView) myDialog.findViewById(R.id.beams);
		floor = (TextView) myDialog.findViewById(R.id.floor);
		walls = (TextView) myDialog.findViewById(R.id.walls);
		partitions = (TextView) myDialog.findViewById(R.id.partitions);
		windows = (TextView) myDialog.findViewById(R.id.windows);
		doors = (TextView) myDialog.findViewById(R.id.doors);
		ceiling = (TextView) myDialog.findViewById(R.id.ceiling);
		roof = (TextView) myDialog.findViewById(R.id.roof);

		btn_update_page2 = (Button) myDialog
				.findViewById(R.id.btn_update_page2);
		btn_update_page2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Construction_Ebm_API ce = new Construction_Ebm_API();
				// checkbox
				
				if (foundation_concrete.isChecked()) {
					ce.setreport_foundation_concrete("true");
				} else {
					ce.setreport_foundation_concrete("");
				}
				if (foundation_other.isChecked()) {
					ce.setreport_foundation_other("true");
				} else {
					ce.setreport_foundation_other("");
				}
				ce.setreport_foundation_other_value(foundation_other_value
						.getText().toString());

				if (post_concrete.isChecked()) {
					ce.setreport_post_concrete("true");
				} else {
					ce.setreport_post_concrete("");
				}
				if (post_concrete_timber.isChecked()) {
					ce.setreport_post_concrete_timber("true");
				} else {
					ce.setreport_post_concrete_timber("");
				}
				if (post_steel.isChecked()) {
					ce.setreport_post_steel("true");
				} else {
					ce.setreport_post_steel("");
				}
				if (post_other.isChecked()) {
					ce.setreport_post_other("true");
				} else {
					ce.setreport_post_other("");
				}
				ce.setreport_post_other_value(post_other_value.getText()
						.toString());

				if (beams_concrete.isChecked()) {
					ce.setreport_beams_concrete("true");
				} else {
					ce.setreport_beams_concrete("");
				}
				if (beams_timber.isChecked()) {
					ce.setreport_beams_timber("true");
				} else {
					ce.setreport_beams_timber("");
				}
				if (beams_steel.isChecked()) {
					ce.setreport_beams_steel("true");
				} else {
					ce.setreport_beams_steel("");
				}
				if (beams_other.isChecked()) {
					ce.setreport_beams_other("true");
				} else {
					ce.setreport_beams_other("");
				}
				ce.setreport_beams_other_value(beams_other_value.getText()
						.toString());

				if (floors_concrete.isChecked()) {
					ce.setreport_floors_concrete("true");
				} else {
					ce.setreport_floors_concrete("");
				}
				if (floors_tiles.isChecked()) {
					ce.setreport_floors_tiles("true");
				} else {
					ce.setreport_floors_tiles("");
				}
				if (floors_tiles_cement.isChecked()) {
					ce.setreport_floors_tiles_cement("true");
				} else {
					ce.setreport_floors_tiles_cement("");
				}
				if (floors_laminated_wood.isChecked()) {
					ce.setreport_floors_laminated_wood("true");
				} else {
					ce.setreport_floors_laminated_wood("");
				}
				if (floors_ceramic_tiles.isChecked()) {
					ce.setreport_floors_ceramic_tiles("true");
				} else {
					ce.setreport_floors_ceramic_tiles("");
				}
				if (floors_wood_planks.isChecked()) {
					ce.setreport_floors_wood_planks("true");
				} else {
					ce.setreport_floors_wood_planks("");
				}
				if (floors_marble_washout.isChecked()) {
					ce.setreport_floors_marble_washout("true");
				} else {
					ce.setreport_floors_marble_washout("");
				}
				if (floors_concrete_boards.isChecked()) {
					ce.setreport_floors_concrete_boards("true");
				} else {
					ce.setreport_floors_concrete_boards("");
				}
				if (floors_granite_tiles.isChecked()) {
					ce.setreport_floors_granite_tiles("true");
				} else {
					ce.setreport_floors_granite_tiles("");
				}
				if (floors_marble_wood.isChecked()) {
					ce.setreport_floors_marble_wood("true");
				} else {
					ce.setreport_floors_marble_wood("");
				}
				if (floors_carpet.isChecked()) {
					ce.setreport_floors_carpet("true");
				} else {
					ce.setreport_floors_carpet("");
				}
				if (floors_other.isChecked()) {
					ce.setreport_floors_other("true");
				} else {
					ce.setreport_floors_other("");
				}
				ce.setreport_floors_other_value(floors_other_value.getText()
						.toString());

				if (walls_chb.isChecked()) {
					ce.setreport_walls_chb("true");
				} else {
					ce.setreport_walls_chb("");
				}
				if (walls_chb_cement.isChecked()) {
					ce.setreport_walls_chb_cement("true");
				} else {
					ce.setreport_walls_chb_cement("");
				}
				if (walls_anay.isChecked()) {
					ce.setreport_walls_anay("true");
				} else {
					ce.setreport_walls_anay("");
				}
				if (walls_chb_wood.isChecked()) {
					ce.setreport_walls_chb_wood("true");
				} else {
					ce.setreport_walls_chb_wood("");
				}
				if (walls_precast.isChecked()) {
					ce.setreport_walls_precast("true");
				} else {
					ce.setreport_walls_precast("");
				}
				if (walls_decorative_stone.isChecked()) {
					ce.setreport_walls_decorative_stone("true");
				} else {
					ce.setreport_walls_decorative_stone("");
				}
				if (walls_adobe.isChecked()) {
					ce.setreport_walls_adobe("true");
				} else {
					ce.setreport_walls_adobe("");
				}
				if (walls_ceramic_tiles.isChecked()) {
					ce.setreport_walls_ceramic_tiles("true");
				} else {
					ce.setreport_walls_ceramic_tiles("");
				}
				if (walls_cast_in_place.isChecked()) {
					ce.setreport_walls_cast_in_place("true");
				} else {
					ce.setreport_walls_cast_in_place("");
				}
				if (walls_sandblast.isChecked()) {
					ce.setreport_walls_sandblast("true");
				} else {
					ce.setreport_walls_sandblast("");
				}
				if (walls_mactan_stone.isChecked()) {
					ce.setreport_walls_mactan_stone("true");
				} else {
					ce.setreport_walls_mactan_stone("");
				}
				if (walls_painted.isChecked()) {
					ce.setreport_walls_painted("true");
				} else {
					ce.setreport_walls_painted("");
				}
				if (walls_other.isChecked()) {
					ce.setreport_walls_other("true");
				} else {
					ce.setreport_walls_other("");
				}
				ce.setreport_walls_other_value(walls_other_value.getText()
						.toString());

				if (partitions_chb.isChecked()) {
					ce.setreport_partitions_chb("true");
				} else {
					ce.setreport_partitions_chb("");
				}
				if (partitions_painted_cement.isChecked()) {
					ce.setreport_partitions_painted_cement("true");
				} else {
					ce.setreport_partitions_painted_cement("");
				}
				if (partitions_anay.isChecked()) {
					ce.setreport_partitions_anay("true");
				} else {
					ce.setreport_partitions_anay("");
				}
				if (partitions_wood_boards.isChecked()) {
					ce.setreport_partitions_wood_boards("true");
				} else {
					ce.setreport_partitions_wood_boards("");
				}
				if (partitions_precast.isChecked()) {
					ce.setreport_partitions_precast("true");
				} else {
					ce.setreport_partitions_precast("");
				}
				if (partitions_decorative_stone.isChecked()) {
					ce.setreport_partitions_decorative_stone("true");
				} else {
					ce.setreport_partitions_decorative_stone("");
				}
				if (partitions_adobe.isChecked()) {
					ce.setreport_partitions_adobe("true");
				} else {
					ce.setreport_partitions_adobe("");
				}
				if (partitions_granite.isChecked()) {
					ce.setreport_partitions_granite("true");
				} else {
					ce.setreport_partitions_granite("");
				}
				if (partitions_cast_in_place.isChecked()) {
					ce.setreport_partitions_cast_in_place("true");
				} else {
					ce.setreport_partitions_cast_in_place("");
				}
				if (partitions_sandblast.isChecked()) {
					ce.setreport_partitions_sandblast("true");
				} else {
					ce.setreport_partitions_sandblast("");
				}
				if (partitions_mactan_stone.isChecked()) {
					ce.setreport_partitions_mactan_stone("true");
				} else {
					ce.setreport_partitions_mactan_stone("");
				}
				if (partitions_ceramic_tiles.isChecked()) {
					ce.setreport_partitions_ceramic_tiles("true");
				} else {
					ce.setreport_partitions_ceramic_tiles("");
				}
				if (partitions_chb_plywood.isChecked()) {
					ce.setreport_partitions_chb_plywood("true");
				} else {
					ce.setreport_partitions_chb_plywood("");
				}
				if (partitions_hardiflex.isChecked()) {
					ce.setreport_partitions_hardiflex("true");
				} else {
					ce.setreport_partitions_hardiflex("");
				}
				if (partitions_wallpaper.isChecked()) {
					ce.setreport_partitions_wallpaper("true");
				} else {
					ce.setreport_partitions_wallpaper("");
				}
				if (partitions_painted.isChecked()) {
					ce.setreport_partitions_painted("true");
				} else {
					ce.setreport_partitions_painted("");
				}
				if (partitions_other.isChecked()) {
					ce.setreport_partitions_other("true");
				} else {
					ce.setreport_partitions_other("");
				}
				ce.setreport_partitions_other_value(partitions_other_value
						.getText().toString());

				if (windows_steel_casement.isChecked()) {
					ce.setreport_windows_steel_casement("true");
				} else {
					ce.setreport_windows_steel_casement("");
				}
				if (windows_fixed_view.isChecked()) {
					ce.setreport_windows_fixed_view("true");
				} else {
					ce.setreport_windows_fixed_view("");
				}
				if (windows_analok_sliding.isChecked()) {
					ce.setreport_windows_analok_sliding("true");
				} else {
					ce.setreport_windows_analok_sliding("");
				}
				if (windows_alum_glass.isChecked()) {
					ce.setreport_windows_alum_glass("true");
				} else {
					ce.setreport_windows_alum_glass("");
				}
				if (windows_aluminum_sliding.isChecked()) {
					ce.setreport_windows_aluminum_sliding("true");
				} else {
					ce.setreport_windows_aluminum_sliding("");
				}
				if (windows_awning_type.isChecked()) {
					ce.setreport_windows_awning_type("true");
				} else {
					ce.setreport_windows_awning_type("");
				}
				if (windows_powder_coated.isChecked()) {
					ce.setreport_windows_powder_coated("true");
				} else {
					ce.setreport_windows_powder_coated("");
				}
				if (windows_wooden_frame.isChecked()) {
					ce.setreport_windows_wooden_frame("true");
				} else {
					ce.setreport_windows_wooden_frame("");
				}
				if (windows_other.isChecked()) {
					ce.setreport_windows_other("true");
				} else {
					ce.setreport_windows_other("");
				}
				ce.setreport_windows_other_value(windows_other_value.getText()
						.toString());

				if (doors_wood_panel.isChecked()) {
					ce.setreport_doors_wood_panel("true");
				} else {
					ce.setreport_doors_wood_panel("");
				}
				if (doors_pvc.isChecked()) {
					ce.setreport_doors_pvc("true");
				} else {
					ce.setreport_doors_pvc("");
				}
				if (doors_analok_sliding.isChecked()) {
					ce.setreport_doors_analok_sliding("true");
				} else {
					ce.setreport_doors_analok_sliding("");
				}
				if (doors_screen_door.isChecked()) {
					ce.setreport_doors_screen_door("true");
				} else {
					ce.setreport_doors_screen_door("");
				}
				if (doors_flush.isChecked()) {
					ce.setreport_doors_flush("true");
				} else {
					ce.setreport_doors_flush("");
				}
				if (doors_molded_door.isChecked()) {
					ce.setreport_doors_molded_door("true");
				} else {
					ce.setreport_doors_molded_door("");
				}
				if (doors_aluminum_sliding.isChecked()) {
					ce.setreport_doors_aluminum_sliding("true");
				} else {
					ce.setreport_doors_aluminum_sliding("");
				}
				if (doors_flush_french.isChecked()) {
					ce.setreport_doors_flush_french("true");
				} else {
					ce.setreport_doors_flush_french("");
				}
				if (doors_other.isChecked()) {
					ce.setreport_doors_other("true");
				} else {
					ce.setreport_doors_other("");
				}
				ce.setreport_doors_other_value(doors_other_value.getText()
						.toString());

				if (ceiling_plywood.isChecked()) {
					ce.setreport_ceiling_plywood("true");
				} else {
					ce.setreport_ceiling_plywood("");
				}
				if (ceiling_painted_gypsum.isChecked()) {
					ce.setreport_ceiling_painted_gypsum("true");
				} else {
					ce.setreport_ceiling_painted_gypsum("");
				}
				if (ceiling_soffit_slab.isChecked()) {
					ce.setreport_ceiling_soffit_slab("true");
				} else {
					ce.setreport_ceiling_soffit_slab("");
				}
				if (ceiling_metal_deck.isChecked()) {
					ce.setreport_ceiling_metal_deck("true");
				} else {
					ce.setreport_ceiling_metal_deck("");
				}
				if (ceiling_hardiflex.isChecked()) {
					ce.setreport_ceiling_hardiflex("true");
				} else {
					ce.setreport_ceiling_hardiflex("");
				}
				if (ceiling_plywood_tg.isChecked()) {
					ce.setreport_ceiling_plywood_tg("true");
				} else {
					ce.setreport_ceiling_plywood_tg("");
				}
				if (ceiling_plywood_pvc.isChecked()) {
					ce.setreport_ceiling_plywood_pvc("true");
				} else {
					ce.setreport_ceiling_plywood_pvc("");
				}
				if (ceiling_painted.isChecked()) {
					ce.setreport_ceiling_painted("true");
				} else {
					ce.setreport_ceiling_painted("");
				}
				if (ceiling_with_cornice.isChecked()) {
					ce.setreport_ceiling_with_cornice("true");
				} else {
					ce.setreport_ceiling_with_cornice("");
				}
				if (ceiling_with_moulding.isChecked()) {
					ce.setreport_ceiling_with_moulding("true");
				} else {
					ce.setreport_ceiling_with_moulding("");
				}
				if (ceiling_drop_ceiling.isChecked()) {
					ce.setreport_ceiling_drop_ceiling("true");
				} else {
					ce.setreport_ceiling_drop_ceiling("");
				}
				if (ceiling_other.isChecked()) {
					ce.setreport_ceiling_other("true");
				} else {
					ce.setreport_ceiling_other("");
				}
				ce.setreport_ceiling_other_value(ceiling_other_value.getText()
						.toString());

				if (roof_pre_painted.isChecked()) {
					ce.setreport_roof_pre_painted("true");
				} else {
					ce.setreport_roof_pre_painted("");
				}
				if (roof_rib_type.isChecked()) {
					ce.setreport_roof_rib_type("true");
				} else {
					ce.setreport_roof_rib_type("");
				}
				if (roof_tilespan.isChecked()) {
					ce.setreport_roof_tilespan("true");
				} else {
					ce.setreport_roof_tilespan("");
				}
				if (roof_tegula_asphalt.isChecked()) {
					ce.setreport_roof_tegula_asphalt("true");
				} else {
					ce.setreport_roof_tegula_asphalt("");
				}
				if (roof_tegula_longspan.isChecked()) {
					ce.setreport_roof_tegula_longspan("true");
				} else {
					ce.setreport_roof_tegula_longspan("");
				}
				if (roof_tegula_gi.isChecked()) {
					ce.setreport_roof_tegula_gi("true");
				} else {
					ce.setreport_roof_tegula_gi("");
				}
				if (roof_steel_concrete.isChecked()) {
					ce.setreport_roof_steel_concrete("true");
				} else {
					ce.setreport_roof_steel_concrete("");
				}
				if (roof_polycarbonate.isChecked()) {
					ce.setreport_roof_polycarbonate("true");
				} else {
					ce.setreport_roof_polycarbonate("");
				}
				if (roof_on_steel_trusses.isChecked()) {
					ce.setreport_roof_on_steel_trusses("true");
				} else {
					ce.setreport_roof_on_steel_trusses("");
				}
				if (roof_on_wooden_trusses.isChecked()) {
					ce.setreport_roof_on_wooden_trusses("true");
				} else {
					ce.setreport_roof_on_wooden_trusses("");
				}
				if (roof_other.isChecked()) {
					ce.setreport_roof_other("true");
				} else {
					ce.setreport_roof_other("");
				}
				ce.setreport_roof_other_value(roof_other_value.getText()
						.toString());
				//aug 15


					db.updateConstruction_Ebm_page2(ce, record_id);
					db.close();
					Toast.makeText(getApplicationContext(), "Saved",
							Toast.LENGTH_SHORT).show();
					myDialog.dismiss();

			}
		});
		List<Construction_Ebm_API> ce = db.getConstruction_Ebm(record_id);
		if (!ce.isEmpty()) {
			for (Construction_Ebm_API im : ce) {

				if (im.getreport_foundation_concrete().equals("true")) {
					foundation_concrete.setChecked(true);
				}
				if (im.getreport_foundation_other().equals("true")) {
					foundation_other.setChecked(true);
				}
				foundation_other_value.setText(im.getreport_foundation_other_value());

				if (im.getreport_post_concrete().equals("true")) {
					post_concrete.setChecked(true);
				}
				if (im.getreport_post_concrete_timber().equals("true")) {
					post_concrete_timber.setChecked(true);
				}
				if (im.getreport_post_steel().equals("true")) {
					post_steel.setChecked(true);
				}
				if (im.getreport_post_other().equals("true")) {
					post_other.setChecked(true);
				}
				post_other_value.setText(im.getreport_post_other_value());

				if (im.getreport_beams_concrete().equals("true")) {
					beams_concrete.setChecked(true);
				}
				if (im.getreport_beams_timber().equals("true")) {
					beams_timber.setChecked(true);
				}
				if (im.getreport_beams_steel().equals("true")) {
					beams_steel.setChecked(true);
				}
				if (im.getreport_beams_other().equals("true")) {
					beams_other.setChecked(true);
				}
				beams_other_value.setText(im.getreport_beams_other_value());

				if (im.getreport_floors_concrete().equals("true")) {
					floors_concrete.setChecked(true);
				}
				if (im.getreport_floors_tiles().equals("true")) {
					floors_tiles.setChecked(true);
				}
				if (im.getreport_floors_tiles_cement().equals("true")) {
					floors_tiles_cement.setChecked(true);
				}
				if (im.getreport_floors_laminated_wood().equals("true")) {
					floors_laminated_wood.setChecked(true);
				}
				if (im.getreport_floors_ceramic_tiles().equals("true")) {
					floors_ceramic_tiles.setChecked(true);
				}
				if (im.getreport_floors_wood_planks().equals("true")) {
					floors_wood_planks.setChecked(true);
				}
				if (im.getreport_floors_marble_washout().equals("true")) {
					floors_marble_washout.setChecked(true);
				}
				if (im.getreport_floors_concrete_boards().equals("true")) {
					floors_concrete_boards.setChecked(true);
				}
				if (im.getreport_floors_granite_tiles().equals("true")) {
					floors_granite_tiles.setChecked(true);
				}
				if (im.getreport_floors_marble_wood().equals("true")) {
					floors_marble_wood.setChecked(true);
				}
				if (im.getreport_floors_carpet().equals("true")) {
					floors_carpet.setChecked(true);
				}
				if (im.getreport_floors_other().equals("true")) {
					floors_other.setChecked(true);
				}
				floors_other_value.setText(im.getreport_floors_other_value());

				if (im.getreport_walls_chb().equals("true")) {
					walls_chb.setChecked(true);
				}
				if (im.getreport_walls_chb_cement().equals("true")) {
					walls_chb_cement.setChecked(true);
				}
				if (im.getreport_walls_anay().equals("true")) {
					walls_anay.setChecked(true);
				}
				if (im.getreport_walls_chb_wood().equals("true")) {
					walls_chb_wood.setChecked(true);
				}
				if (im.getreport_walls_precast().equals("true")) {
					walls_precast.setChecked(true);
				}
				if (im.getreport_walls_decorative_stone().equals("true")) {
					walls_decorative_stone.setChecked(true);
				}
				if (im.getreport_walls_adobe().equals("true")) {
					walls_adobe.setChecked(true);
				}
				if (im.getreport_walls_ceramic_tiles().equals("true")) {
					walls_ceramic_tiles.setChecked(true);
				}
				if (im.getreport_walls_cast_in_place().equals("true")) {
					walls_cast_in_place.setChecked(true);
				}
				if (im.getreport_walls_sandblast().equals("true")) {
					walls_sandblast.setChecked(true);
				}
				if (im.getreport_walls_mactan_stone().equals("true")) {
					walls_mactan_stone.setChecked(true);
				}
				if (im.getreport_walls_painted().equals("true")) {
					walls_painted.setChecked(true);
				}
				if (im.getreport_walls_other().equals("true")) {
					walls_other.setChecked(true);
				}
				walls_other_value.setText(im.getreport_walls_other_value());

				if (im.getreport_partitions_chb().equals("true")) {
					partitions_chb.setChecked(true);
				}
				if (im.getreport_partitions_painted_cement().equals("true")) {
					partitions_painted_cement.setChecked(true);
				}
				if (im.getreport_partitions_anay().equals("true")) {
					partitions_anay.setChecked(true);
				}
				if (im.getreport_partitions_wood_boards().equals("true")) {
					partitions_wood_boards.setChecked(true);
				}
				if (im.getreport_partitions_precast().equals("true")) {
					partitions_precast.setChecked(true);
				}
				if (im.getreport_partitions_decorative_stone().equals("true")) {
					partitions_decorative_stone.setChecked(true);
				}
				if (im.getreport_partitions_adobe().equals("true")) {
					partitions_adobe.setChecked(true);
				}
				if (im.getreport_partitions_granite().equals("true")) {
					partitions_granite.setChecked(true);
				}
				if (im.getreport_partitions_cast_in_place().equals("true")) {
					partitions_cast_in_place.setChecked(true);
				}
				if (im.getreport_partitions_sandblast().equals("true")) {
					partitions_sandblast.setChecked(true);
				}
				if (im.getreport_partitions_mactan_stone().equals("true")) {
					partitions_mactan_stone.setChecked(true);
				}
				if (im.getreport_partitions_ceramic_tiles().equals("true")) {
					partitions_ceramic_tiles.setChecked(true);
				}
				if (im.getreport_partitions_chb_plywood().equals("true")) {
					partitions_chb_plywood.setChecked(true);
				}
				if (im.getreport_partitions_hardiflex().equals("true")) {
					partitions_hardiflex.setChecked(true);
				}
				if (im.getreport_partitions_wallpaper().equals("true")) {
					partitions_wallpaper.setChecked(true);
				}
				if (im.getreport_partitions_painted().equals("true")) {
					partitions_painted.setChecked(true);
				}
				if (im.getreport_partitions_other().equals("true")) {
					partitions_other.setChecked(true);
				}
				partitions_other_value.setText(im.getreport_partitions_other_value());

				if (im.getreport_windows_steel_casement().equals("true")) {
					windows_steel_casement.setChecked(true);
				}
				if (im.getreport_windows_fixed_view().equals("true")) {
					windows_fixed_view.setChecked(true);
				}
				if (im.getreport_windows_analok_sliding().equals("true")) {
					windows_analok_sliding.setChecked(true);
				}
				if (im.getreport_windows_alum_glass().equals("true")) {
					windows_alum_glass.setChecked(true);
				}
				if (im.getreport_windows_aluminum_sliding().equals("true")) {
					windows_aluminum_sliding.setChecked(true);
				}
				if (im.getreport_windows_awning_type().equals("true")) {
					windows_awning_type.setChecked(true);
				}
				if (im.getreport_windows_powder_coated().equals("true")) {
					windows_powder_coated.setChecked(true);
				}
				if (im.getreport_windows_wooden_frame().equals("true")) {
					windows_wooden_frame.setChecked(true);
				}
				if (im.getreport_windows_other().equals("true")) {
					windows_other.setChecked(true);
				}
				windows_other_value.setText(im.getreport_windows_other_value());

				if (im.getreport_doors_wood_panel().equals("true")) {
					doors_wood_panel.setChecked(true);
				}
				if (im.getreport_doors_pvc().equals("true")) {
					doors_pvc.setChecked(true);
				}
				if (im.getreport_doors_analok_sliding().equals("true")) {
					doors_analok_sliding.setChecked(true);
				}
				if (im.getreport_doors_screen_door().equals("true")) {
					doors_screen_door.setChecked(true);
				}
				if (im.getreport_doors_flush().equals("true")) {
					doors_flush.setChecked(true);
				}
				if (im.getreport_doors_molded_door().equals("true")) {
					doors_molded_door.setChecked(true);
				}
				if (im.getreport_doors_aluminum_sliding().equals("true")) {
					doors_aluminum_sliding.setChecked(true);
				}
				if (im.getreport_doors_flush_french().equals("true")) {
					doors_flush_french.setChecked(true);
				}
				if (im.getreport_doors_other().equals("true")) {
					doors_other.setChecked(true);
				}
				doors_other_value.setText(im.getreport_doors_other_value());

				if (im.getreport_ceiling_plywood().equals("true")) {
					ceiling_plywood.setChecked(true);
				}
				if (im.getreport_ceiling_painted_gypsum().equals("true")) {
					ceiling_painted_gypsum.setChecked(true);
				}
				if (im.getreport_ceiling_soffit_slab().equals("true")) {
					ceiling_soffit_slab.setChecked(true);
				}
				if (im.getreport_ceiling_metal_deck().equals("true")) {
					ceiling_metal_deck.setChecked(true);
				}
				if (im.getreport_ceiling_hardiflex().equals("true")) {
					ceiling_hardiflex.setChecked(true);
				}
				if (im.getreport_ceiling_plywood_tg().equals("true")) {
					ceiling_plywood_tg.setChecked(true);
				}
				if (im.getreport_ceiling_plywood_pvc().equals("true")) {
					ceiling_plywood_pvc.setChecked(true);
				}
				if (im.getreport_ceiling_painted().equals("true")) {
					ceiling_painted.setChecked(true);
				}
				if (im.getreport_ceiling_with_cornice().equals("true")) {
					ceiling_with_cornice.setChecked(true);
				}
				if (im.getreport_ceiling_with_moulding().equals("true")) {
					ceiling_with_moulding.setChecked(true);
				}
				if (im.getreport_ceiling_drop_ceiling().equals("true")) {
					ceiling_drop_ceiling.setChecked(true);
				}
				if (im.getreport_ceiling_other().equals("true")) {
					ceiling_other.setChecked(true);
				}
				ceiling_other_value.setText(im.getreport_ceiling_other_value());

				if (im.getreport_roof_pre_painted().equals("true")) {
					roof_pre_painted.setChecked(true);
				}
				if (im.getreport_roof_rib_type().equals("true")) {
					roof_rib_type.setChecked(true);
				}
				if (im.getreport_roof_tilespan().equals("true")) {
					roof_tilespan.setChecked(true);
				}
				if (im.getreport_roof_tegula_asphalt().equals("true")) {
					roof_tegula_asphalt.setChecked(true);
				}
				if (im.getreport_roof_tegula_longspan().equals("true")) {
					roof_tegula_longspan.setChecked(true);
				}
				if (im.getreport_roof_tegula_gi().equals("true")) {
					roof_tegula_gi.setChecked(true);
				}
				if (im.getreport_roof_steel_concrete().equals("true")) {
					roof_steel_concrete.setChecked(true);
				}
				if (im.getreport_roof_polycarbonate().equals("true")) {
					roof_polycarbonate.setChecked(true);
				}
				if (im.getreport_roof_on_steel_trusses().equals("true")) {
					roof_on_steel_trusses.setChecked(true);
				}
				if (im.getreport_roof_on_wooden_trusses().equals("true")) {
					roof_on_wooden_trusses.setChecked(true);
				}
				if (im.getreport_roof_other().equals("true")) {
					roof_other.setChecked(true);
				}
				roof_other_value.setText(im.getreport_roof_other_value());

			}
		}
		//sep 5
		if (foundation_concrete.isChecked()||foundation_other.isChecked()) {

			foundation.setText("Foundation");
			foundation.setTextColor(Color.BLACK);
		} else {

			foundation.setText("Foundation: Please select at least one");
			foundation.setTextColor(Color.RED);

		}
		if (foundation_other.isChecked()) {
			gds.fill_in_error(new EditText[]{foundation_other_value});
		}
		if (post_concrete.isChecked()||post_concrete_timber.isChecked()||post_steel.isChecked()||post_other.isChecked()) {

			post.setText("Columns/Posts");
			post.setTextColor(Color.BLACK);
		} else {

			post.setText("Columns/Posts: Please select at least one");
			post.setTextColor(Color.RED);

		}
		if (post_other.isChecked()) {
			gds.fill_in_error(new EditText[]{post_other_value});
		}
		if (beams_concrete.isChecked()||beams_timber.isChecked()||beams_steel.isChecked()||beams_other.isChecked()) {

			beams.setText("Beams");
			beams.setTextColor(Color.BLACK);
		} else {

			beams.setText("Beams: Please select at least one");
			beams.setTextColor(Color.RED);

		}
		if (beams_other.isChecked()) {
			gds.fill_in_error(new EditText[]{beams_other_value});
		}
		if (floors_concrete.isChecked()||floors_tiles.isChecked()||floors_tiles_cement.isChecked()||
				floors_laminated_wood.isChecked()||floors_ceramic_tiles.isChecked()||floors_wood_planks.isChecked()||
				floors_marble_washout.isChecked()||floors_concrete_boards.isChecked()||floors_granite_tiles.isChecked()||
				floors_marble_wood.isChecked()||floors_carpet.isChecked()||floors_other.isChecked()) {

			floor.setText("Floor");
			floor.setTextColor(Color.BLACK);
		} else {

			floor.setText("Floor: Please select at least one");
			floor.setTextColor(Color.RED);

		}
		if (floors_other.isChecked()) {
			gds.fill_in_error(new EditText[]{floors_other_value});
		}
		if (walls_chb.isChecked()||walls_chb_cement.isChecked()||walls_anay.isChecked()||
				walls_chb_wood.isChecked()||walls_precast.isChecked()||walls_decorative_stone.isChecked()||
				walls_adobe.isChecked()||walls_ceramic_tiles.isChecked()||walls_cast_in_place.isChecked()||
				walls_sandblast.isChecked()||walls_mactan_stone.isChecked()||walls_painted.isChecked()||walls_other.isChecked()) {

			walls.setText("Walls");
			walls.setTextColor(Color.BLACK);
		} else {

			walls.setText("Walls: Please select at least one");
			walls.setTextColor(Color.RED);
		}
		if (walls_other.isChecked()) {
			gds.fill_in_error(new EditText[]{walls_other_value});
		}
		if (partitions_chb.isChecked()||partitions_painted_cement.isChecked()||partitions_anay.isChecked()||
				partitions_wood_boards.isChecked()||partitions_precast.isChecked()||partitions_decorative_stone.isChecked()||
				partitions_adobe.isChecked()||partitions_granite.isChecked()||partitions_cast_in_place.isChecked()||
				partitions_sandblast.isChecked()||partitions_mactan_stone.isChecked()||partitions_ceramic_tiles.isChecked()||
				partitions_chb_plywood.isChecked()|| partitions_hardiflex.isChecked()||partitions_wallpaper.isChecked()
				||partitions_painted.isChecked()||partitions_other.isChecked()) {

			partitions.setText("Partitions");
			partitions.setTextColor(Color.BLACK);
		} else {

			partitions.setText("Partitions: Please select at least one");
			partitions.setTextColor(Color.RED);
		}
		if (partitions_other.isChecked()) {
			gds.fill_in_error(new EditText[]{partitions_other_value});
		}
		if (windows_steel_casement.isChecked()||windows_fixed_view.isChecked()||windows_analok_sliding.isChecked()||
				windows_alum_glass.isChecked()||windows_aluminum_sliding.isChecked()||windows_awning_type.isChecked()||
				windows_powder_coated.isChecked()||windows_wooden_frame.isChecked()||windows_other.isChecked()) {

			windows.setText("Windows");
			windows.setTextColor(Color.BLACK);
		} else {

			windows.setText("Windows: Please select at least one");
			windows.setTextColor(Color.RED);
		}
		if (windows_other.isChecked()) {
			gds.fill_in_error(new EditText[]{windows_other_value});
		}
		if (doors_wood_panel.isChecked()||doors_pvc.isChecked()||doors_analok_sliding.isChecked()||
				doors_screen_door.isChecked()||doors_flush.isChecked()||doors_molded_door.isChecked()||
				doors_aluminum_sliding.isChecked()||doors_flush_french.isChecked()||doors_other.isChecked()) {

			doors.setText("Doors");
			doors.setTextColor(Color.BLACK);
		} else {

			doors.setText("Doors: Please select at least one");
			doors.setTextColor(Color.RED);
		}
		if (doors_other.isChecked()) {
			gds.fill_in_error(new EditText[]{doors_other_value});
		}
		if (ceiling_plywood.isChecked()||ceiling_painted_gypsum.isChecked()||ceiling_soffit_slab.isChecked()||
				ceiling_metal_deck.isChecked()||ceiling_hardiflex.isChecked()||ceiling_plywood_tg.isChecked()||
				ceiling_plywood_pvc.isChecked()||ceiling_painted.isChecked()||
				ceiling_with_cornice.isChecked()||ceiling_with_moulding.isChecked()||ceiling_drop_ceiling.isChecked()||ceiling_other.isChecked()) {

			ceiling.setText("Ceiling");
			ceiling.setTextColor(Color.BLACK);
		} else {

			ceiling.setText("Ceiling: Please select at least one");
			ceiling.setTextColor(Color.RED);
		}
		if (ceiling_other.isChecked()) {
			gds.fill_in_error(new EditText[]{ceiling_other_value});
		}
		if (roof_pre_painted.isChecked()||roof_rib_type.isChecked()||roof_tilespan.isChecked()||
				roof_tegula_asphalt.isChecked()||roof_tegula_longspan.isChecked()||roof_tegula_gi.isChecked()||
				roof_steel_concrete.isChecked()||roof_polycarbonate.isChecked()||
				roof_on_steel_trusses.isChecked()||roof_on_wooden_trusses.isChecked()||roof_other.isChecked()) {

			roof.setText("Roof");
			roof.setTextColor(Color.BLACK);
		} else {

			roof.setText("Roof: Please select at least one");
			roof.setTextColor(Color.RED);
		}
		if (roof_other.isChecked()) {
			gds.fill_in_error(new EditText[]{roof_other_value});
		}
		myDialog.show();
	}
	
	public void custom_dialog_page4() {
		myDialog = new Dialog(Construction_ebm.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.report_construction_ebm_page4);
		// myDialog.setTitle("My Dialog");
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.FILL_PARENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		et_remarks = (EditText) myDialog.findViewById(R.id.et_remarks);
		btn_clear = (Button) myDialog.findViewById(R.id.btn_clear);
		btn_default = (Button) myDialog.findViewById(R.id.btn_default);
		btn_update_page4 = (Button) myDialog.findViewById(R.id.btn_update_page4);
		
		btn_default.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				et_remarks.setText("1. For presentation purposes, building valued as if 100% completed based on building plans & building specifications provided, subject to progress completion & validation upon completion. Any deviation on the given specifications would warrant changes on the suggested RCN."
						+"\n"+
						"2. Valuation given was based on Bank Schedule of RCN, considering the materials specifications given.");
			}
		});
		
		btn_clear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				et_remarks.setText("");
			}
		});
		
		btn_update_page4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub


					Construction_Ebm_API ce = new Construction_Ebm_API();
					ce.setreport_valuation_remarks(et_remarks.getText().toString());
					db.updateConstruction_Ebm_page4(ce, record_id);
					db.close();
					Toast.makeText(getApplicationContext(), "Saved",
							Toast.LENGTH_SHORT).show();
					myDialog.dismiss();

			}
		});
		
		List<Construction_Ebm_API> ce = db.getConstruction_Ebm(record_id);
		if (!ce.isEmpty()) {
			for (Construction_Ebm_API im : ce) {
				et_remarks.setText(im.getreport_valuation_remarks());
			}
		}
		gds.fill_in_error(new EditText[]{et_remarks});
		myDialog.show();
	}

	public void custom_dialog_address() {
		myDialog = new Dialog(Construction_ebm.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.address_dialog);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		et_unit_no = (EditText) myDialog.findViewById(R.id.et_unit_no);
		et_building_name = (EditText) myDialog.findViewById(R.id.et_building_name);
		et_lot_no = (EditText) myDialog.findViewById(R.id.et_lot_no);
		et_block_no = (EditText) myDialog.findViewById(R.id.et_block_no);
		et_street_no = (EditText) myDialog.findViewById(R.id.et_street_no);
		et_street_name = (EditText) myDialog.findViewById(R.id.et_street_name);
		et_village = (EditText) myDialog.findViewById(R.id.et_village);
		et_district = (EditText) myDialog.findViewById(R.id.et_district);
		et_zip_code = (EditText) myDialog.findViewById(R.id.et_zip_code);
		et_city = (EditText) myDialog.findViewById(R.id.et_city);
		et_province = (EditText) myDialog.findViewById(R.id.et_province);
		et_region = (EditText) myDialog.findViewById(R.id.et_region);
		et_country = (EditText) myDialog.findViewById(R.id.et_country);
		
		btn_save_address = (Button) myDialog.findViewById(R.id.btn_save_address);
		btn_save_address.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method
				Report_Accepted_Jobs li = new Report_Accepted_Jobs();
				li.setunit_no(et_unit_no.getText().toString());
				li.setbuilding_name(et_building_name.getText().toString());
				li.setlot_no(et_lot_no.getText().toString());
				li.setblock_no(et_block_no.getText().toString());
				li.setstreet_no(et_street_no.getText().toString());
				li.setstreet_name(et_street_name.getText().toString());
				li.setvillage(et_village.getText().toString());
				li.setdistrict(et_district.getText().toString());
				li.setzip_code(et_zip_code.getText().toString());
				li.setcity(et_city.getText().toString());
				li.setprovince(et_province.getText().toString());
				li.setregion(et_region.getText().toString());
				li.setcountry(et_country.getText().toString());
				
				db.updateCollateral_Address(li, record_id);
				db.close();
				Toast.makeText(getApplicationContext(), "Saved",
						Toast.LENGTH_SHORT).show();
				myDialog.dismiss();
				Intent intent = getIntent();
				finish();
				startActivity(intent);
			}
		});
		
		List<Report_Accepted_Jobs> li = db.getReport_Accepted_Jobs(String.valueOf(record_id));
		if (!li.isEmpty()) {
			for (Report_Accepted_Jobs im : li) {
				et_unit_no.setText(im.getunit_no());
				et_building_name.setText(im.getbuilding_name());
				et_lot_no.setText(im.getlot_no());
				et_block_no.setText(im.getblock_no());
				et_street_no.setText(im.getstreet_no());
				et_street_name.setText(im.getstreet_name());
				et_village.setText(im.getvillage());
				et_district.setText(im.getdistrict());
				et_zip_code.setText(im.getzip_code());
				et_city.setText(im.getcity());
				et_province.setText(im.getprovince());
				et_region.setText(im.getregion());
				et_country.setText(im.getcountry());
			}
		}

		myDialog.show();
	}
	
	
	public void create_json() {
		List<Construction_Ebm_API> ce = db.getConstruction_Ebm(record_id);
		if (!ce.isEmpty()) {
			for (Construction_Ebm_API im : ce) {
				JSONObject query_temp = new JSONObject();
				JSONObject report = new JSONObject();
				JSONObject record_temp = new JSONObject();
				JSONObject appraisal_attachments = new JSONObject();
				JSONObject appraisal_attachments_data = new JSONObject();
				JSONArray app_attachments = new JSONArray();
				
				JSONObject cerl_obj = new JSONObject();//prev appraisal
				JSONArray cerl_ary = new JSONArray();//prev appraisal
				
				JSONObject address_obj = new JSONObject();//address
				JSONArray address_ary = new JSONArray();//address
				JSONObject app_req_obj = new JSONObject();//address
				
				try {
					//check first for required fields
					 freeFields = true;

						/*fieldsComplete = true;
						gs.fieldsComplete  = true;*/
					//submit
					Log.e("Complete","All required fields are filled up");
					
					query_temp.put("system.record_id", record_id);
					// report.put("application_status", "report_review");
					report.put("from_mobile","true");
					report.put("valrep_constebm_date_inspected_month", im.getreport_date_inspected_month());
					report.put("valrep_constebm_date_inspected_day", im.getreport_date_inspected_day());
					report.put("valrep_constebm_date_inspected_year", im.getreport_date_inspected_year());
					report.put("valrep_constebm_time_inspected", im.getreport_time_inspected());

					report.put("valrep_constebm_project_type", im.getreport_project_type());
					report.put("valrep_constebm_floor_area", im.getreport_floor_area());
					report.put("valrep_constebm_storeys", im.getreport_storeys());
					report.put("valrep_constebm_expected_economic_life", im.getreport_expected_economic_life());
					report.put("valrep_constebm_type_of_housing_unit", im.getreport_type_of_housing_unit());

					report.put("valrep_constebm_const_type_reinforced_concrete", im.getreport_const_type_reinforced_concrete()); 
					report.put("valrep_constebm_const_type_semi_concrete", im.getreport_const_type_semi_concrete()); 
					report.put("valrep_constebm_const_type_mixed_materials", im.getreport_const_type_mixed_materials()); 

					report.put("valrep_constebm_foundation_concrete", im.getreport_foundation_concrete()); 
					report.put("valrep_constebm_foundation_other", im.getreport_foundation_other()); 
					report.put("valrep_constebm_foundation_other_value", im.getreport_foundation_other_value());

					report.put("valrep_constebm_post_concrete", im.getreport_post_concrete()); 
					report.put("valrep_constebm_post_concrete_timber", im.getreport_post_concrete_timber()); 
					report.put("valrep_constebm_post_steel", im.getreport_post_steel()); 
					report.put("valrep_constebm_post_other", im.getreport_post_other()); 
					report.put("valrep_constebm_post_other_value", im.getreport_post_other_value());

					report.put("valrep_constebm_beams_concrete", im.getreport_beams_concrete()); 
					report.put("valrep_constebm_beams_timber", im.getreport_beams_timber());
					report.put("valrep_constebm_beams_steel", im.getreport_beams_steel()); 
					report.put("valrep_constebm_beams_other", im.getreport_beams_other()); 
					report.put("valrep_constebm_beams_other_value", im.getreport_beams_other_value());

					report.put("valrep_constebm_floors_concrete", im.getreport_floors_concrete()); 
					report.put("valrep_constebm_floors_tiles", im.getreport_floors_tiles()); 
					report.put("valrep_constebm_floors_tiles_cement", im.getreport_floors_tiles_cement()); 
					report.put("valrep_constebm_floors_laminated_wood", im.getreport_floors_laminated_wood()); 
					report.put("valrep_constebm_floors_ceramic_tiles", im.getreport_floors_ceramic_tiles()); 
					report.put("valrep_constebm_floors_wood_planks", im.getreport_floors_wood_planks()); 
					report.put("valrep_constebm_floors_marble_washout", im.getreport_floors_marble_washout()); 
					report.put("valrep_constebm_floors_concrete_boards", im.getreport_floors_concrete_boards()); 
					report.put("valrep_constebm_floors_granite_tiles", im.getreport_floors_granite_tiles()); 
					report.put("valrep_constebm_floors_marble_wood", im.getreport_floors_marble_wood()); 
					report.put("valrep_constebm_floors_carpet", im.getreport_floors_carpet()); 
					report.put("valrep_constebm_floors_other", im.getreport_floors_other()); 
					report.put("valrep_constebm_floors_other_value", im.getreport_floors_other_value());

					report.put("valrep_constebm_walls_chb", im.getreport_walls_chb()); 
					report.put("valrep_constebm_walls_chb_cement", im.getreport_walls_chb_cement()); 
					report.put("valrep_constebm_walls_anay", im.getreport_walls_anay()); 
					report.put("valrep_constebm_walls_chb_wood", im.getreport_walls_chb_wood()); 
					report.put("valrep_constebm_walls_precast", im.getreport_walls_precast()); 
					report.put("valrep_constebm_walls_decorative_stone", im.getreport_walls_decorative_stone()); 
					report.put("valrep_constebm_walls_adobe", im.getreport_walls_adobe()); 
					report.put("valrep_constebm_walls_ceramic_tiles", im.getreport_walls_ceramic_tiles()); 
					report.put("valrep_constebm_walls_cast_in_place", im.getreport_walls_cast_in_place()); 
					report.put("valrep_constebm_walls_sandblast", im.getreport_walls_sandblast());
					report.put("valrep_constebm_walls_mactan_stone", im.getreport_walls_mactan_stone()); 
					report.put("valrep_constebm_walls_painted", im.getreport_walls_painted()); 
					report.put("valrep_constebm_walls_other", im.getreport_walls_other()); 
					report.put("valrep_constebm_walls_other_value", im.getreport_walls_other_value());

					report.put("valrep_constebm_partitions_chb", im.getreport_partitions_chb()); 
					report.put("valrep_constebm_partitions_painted_cement", im.getreport_partitions_painted_cement()); 
					report.put("valrep_constebm_partitions_anay", im.getreport_partitions_anay()); 
					report.put("valrep_constebm_partitions_wood_boards", im.getreport_partitions_wood_boards());
					report.put("valrep_constebm_partitions_precast", im.getreport_partitions_precast()); 
					report.put("valrep_constebm_partitions_decorative_stone", im.getreport_partitions_decorative_stone()); 
					report.put("valrep_constebm_partitions_adobe", im.getreport_partitions_adobe()); 
					report.put("valrep_constebm_partitions_granite", im.getreport_partitions_granite()); 
					report.put("valrep_constebm_partitions_cast_in_place", im.getreport_partitions_cast_in_place()); 
					report.put("valrep_constebm_partitions_sandblast", im.getreport_partitions_sandblast()); 
					report.put("valrep_constebm_partitions_mactan_stone", im.getreport_partitions_mactan_stone()); 
					report.put("valrep_constebm_partitions_ceramic_tiles", im.getreport_partitions_ceramic_tiles()); 
					report.put("valrep_constebm_partitions_chb_plywood", im.getreport_partitions_chb_plywood()); 
					report.put("valrep_constebm_partitions_hardiflex", im.getreport_partitions_hardiflex()); 
					report.put("valrep_constebm_partitions_wallpaper", im.getreport_partitions_wallpaper()); 
					report.put("valrep_constebm_partitions_painted", im.getreport_partitions_painted()); 
					report.put("valrep_constebm_partitions_other", im.getreport_partitions_other()); 
					report.put("valrep_constebm_partitions_other_value", im.getreport_partitions_other_value());

					report.put("valrep_constebm_windows_steel_casement", im.getreport_windows_steel_casement()); 
					report.put("valrep_constebm_windows_fixed_view", im.getreport_windows_fixed_view()); 
					report.put("valrep_constebm_windows_analok_sliding", im.getreport_windows_analok_sliding()); 
					report.put("valrep_constebm_windows_alum_glass", im.getreport_windows_alum_glass()); 
					report.put("valrep_constebm_windows_aluminum_sliding", im.getreport_windows_aluminum_sliding()); 
					report.put("valrep_constebm_windows_awning_type", im.getreport_windows_awning_type()); 
					report.put("valrep_constebm_windows_powder_coated", im.getreport_windows_powder_coated()); 
					report.put("valrep_constebm_windows_wooden_frame", im.getreport_windows_wooden_frame()); 
					report.put("valrep_constebm_windows_other", im.getreport_windows_other()); 
					report.put("valrep_constebm_windows_other_value", im.getreport_windows_other_value());

					report.put("valrep_constebm_doors_wood_panel", im.getreport_doors_wood_panel());
					report.put("valrep_constebm_doors_pvc", im.getreport_doors_pvc()); 
					report.put("valrep_constebm_doors_analok_sliding", im.getreport_doors_analok_sliding()); 
					report.put("valrep_constebm_doors_screen_door", im.getreport_doors_screen_door()); 
					report.put("valrep_constebm_doors_flush", im.getreport_doors_flush()); 
					report.put("valrep_constebm_doors_molded_door", im.getreport_doors_molded_door()); 
					report.put("valrep_constebm_doors_aluminum_sliding", im.getreport_doors_aluminum_sliding()); 
					report.put("valrep_constebm_doors_flush_french", im.getreport_doors_flush_french()); 
					report.put("valrep_constebm_doors_other", im.getreport_doors_other()); 
					report.put("valrep_constebm_doors_other_value", im.getreport_doors_other_value());

					report.put("valrep_constebm_ceiling_plywood", im.getreport_ceiling_plywood()); 
					report.put("valrep_constebm_ceiling_painted_gypsum", im.getreport_ceiling_painted_gypsum()); 
					report.put("valrep_constebm_ceiling_soffit_slab", im.getreport_ceiling_soffit_slab()); 
					report.put("valrep_constebm_ceiling_metal_deck", im.getreport_ceiling_metal_deck()); 
					report.put("valrep_constebm_ceiling_hardiflex", im.getreport_ceiling_hardiflex()); 
					report.put("valrep_constebm_ceiling_plywood_tg", im.getreport_ceiling_plywood_tg()); 
					report.put("valrep_constebm_ceiling_plywood_pvc", im.getreport_ceiling_plywood_pvc()); 
					report.put("valrep_constebm_ceiling_painted", im.getreport_ceiling_painted()); 
					report.put("valrep_constebm_ceiling_with_cornice", im.getreport_ceiling_with_cornice()); 
					report.put("valrep_constebm_ceiling_with_moulding", im.getreport_ceiling_with_moulding()); 
					report.put("valrep_constebm_ceiling_drop_ceiling", im.getreport_ceiling_drop_ceiling()); 
					report.put("valrep_constebm_ceiling_other", im.getreport_ceiling_other()); 
					report.put("valrep_constebm_ceiling_other_value", im.getreport_ceiling_other_value());

					report.put("valrep_constebm_roof_pre_painted", im.getreport_roof_pre_painted()); 
					report.put("valrep_constebm_roof_rib_type", im.getreport_roof_rib_type()); 
					report.put("valrep_constebm_roof_tilespan", im.getreport_roof_tilespan()); 
					report.put("valrep_constebm_roof_tegula_asphalt", im.getreport_roof_tegula_asphalt()); 
					report.put("valrep_constebm_roof_tegula_longspan", im.getreport_roof_tegula_longspan()); 
					report.put("valrep_constebm_roof_tegula_gi", im.getreport_roof_tegula_gi()); 
					report.put("valrep_constebm_roof_steel_concrete", im.getreport_roof_steel_concrete()); 
					report.put("valrep_constebm_roof_polycarbonate", im.getreport_roof_polycarbonate()); 
					report.put("valrep_constebm_roof_on_steel_trusses", im.getreport_roof_on_steel_trusses()); 
					report.put("valrep_constebm_roof_on_wooden_trusses", im.getreport_roof_on_wooden_trusses()); 
					report.put("valrep_constebm_roof_other", im.getreport_roof_other()); 
					report.put("valrep_constebm_roof_other_value", im.getreport_roof_other_value());

					//get room list
					List<Construction_Ebm_API_Room_List> cerl = db2.getConstruction_Ebm_Room_List(record_id);
					if (!cerl.isEmpty()) {
						for (Construction_Ebm_API_Room_List imcerl : cerl) {
							cerl_obj = new JSONObject();
							cerl_obj.put("valrep_constebm_room_list_area", imcerl.getreport_room_list_area());
							cerl_obj.put("valrep_constebm_room_list_description", imcerl.getreport_room_list_description());
							cerl_obj.put("valrep_constebm_room_list_est_proj_cost", imcerl.getreport_room_list_est_proj_cost());
							cerl_obj.put("valrep_constebm_room_list_floor", imcerl.getreport_room_list_floor());
							cerl_obj.put("valrep_constebm_room_list_rcn", imcerl.getreport_room_list_rcn());
							cerl_ary.put(cerl_obj);
						}
						report.put("valrep_constebm_room_list",cerl_ary);
					} else {
						Log.e("incomplete","incomplete");
					}
					
					
					report.put("valrep_constebm_valuation_total_area", im.getreport_valuation_total_area()); 
//matrix condition
					report.put("valrep_constebm_valuation_total_proj_cost", im.getreport_valuation_total_proj_cost());
					if(!globalSave) {
						if (im.getreport_valuation_total_proj_cost().equals("")) {
							matrix_value = 0.00;
						} else {
							matrix_value = Double.parseDouble(im.getreport_valuation_total_proj_cost());
						}
						/*//Matrix MV1
						if ((matrix_value >= 0) && (matrix_value <= 1000000)) {
							report.put("approval_signor_reviewer", "false");
							report.put("approval_signor_reviewer_done", "true");
							report.put("approval_supervisor_reviewer", "false");
							report.put("approval_supervisor_reviewer_done", "true");

							report.put("approval_signor", "true");
							report.put("approval_supervisor", "false");
							report.put("approval_manager", "false");
							report.put("approval_head", "false");

							report.put("approval_signor_done", "false");
							report.put("approval_supervisor_done", "true");
							report.put("approval_manager_done", "true");
							report.put("approval_head_done", "true");

							report.put("approval_matrix_type", "MV1");
						}

						//Matrix MV2
						else if ((matrix_value > 1000000) && (matrix_value <= 3000000)) {
							report.put("approval_signor_reviewer", "false");
							report.put("approval_signor_reviewer_done", "true");
							report.put("approval_supervisor_reviewer", "false");
							report.put("approval_supervisor_reviewer_done", "true");

							report.put("approval_signor", "false");
							report.put("approval_supervisor", "true");
							report.put("approval_manager", "false");
							report.put("approval_head", "false");

							report.put("approval_signor_done", "true");
							report.put("approval_supervisor_done", "false");
							report.put("approval_manager_done", "true");
							report.put("approval_head_done", "true");

							report.put("approval_matrix_type", "MV2");
						}

						//Matrix MV3
						else if ((matrix_value > 3000000) && (matrix_value <= 10000000)) {
							report.put("approval_signor_reviewer", "false");
							report.put("approval_signor_reviewer_done", "true");
							report.put("approval_supervisor_reviewer", "false");
							report.put("approval_supervisor_reviewer_done", "true");

							report.put("approval_signor", "true");
							report.put("approval_supervisor", "true");
							report.put("approval_manager", "true");
							report.put("approval_head", "alt");

							report.put("approval_signor_done", "false");
							report.put("approval_supervisor_done", "false");
							report.put("approval_manager_done", "false");
							report.put("approval_head_done", "false");

							report.put("approval_matrix_type", "MV3");
						}

						//Matrix MV4
						else if (matrix_value > 10000000) {
							report.put("approval_signor_reviewer", "false");
							report.put("approval_signor_reviewer_done", "true");
							report.put("approval_supervisor_reviewer", "false");
							report.put("approval_supervisor_reviewer_done", "true");

							report.put("approval_signor", "true");
							report.put("approval_supervisor", "true");
							report.put("approval_manager", "true");
							report.put("approval_head", "true");

							report.put("approval_signor_done", "false");
							report.put("approval_supervisor_done", "false");
							report.put("approval_manager_done", "false");
							report.put("approval_head_done", "false");

							report.put("approval_matrix_type", "MV4");
						}
						if (requesting_party.contentEquals("AUTO FINANCE GROUP")) {
							report.put("application_status", "afg_valuation");
						} else {
							report.put("application_status", "report_review");
						}*/
						if (requesting_party.contentEquals("AUTO FINANCE GROUP")) {
							report.put("application_status", "afg_valuation");
						} else {
							report.put("application_status", "report_review");
						}

					}
						//end of matrix
//get current date and time and submit to CC
					TimeZone tz = TimeZone.getTimeZone(gs.gmt);
					Calendar c = Calendar.getInstance(tz);
					
					int cMonth = c.get(Calendar.MONTH);//starts with 0 for january
					int cDay = c.get(Calendar.DAY_OF_MONTH);
					int cYear = c.get(Calendar.YEAR);
					int cAmPm = c.get(Calendar.AM_PM);
					
					String sMonth = gs.monthInWord[cMonth];
					String sAmPm="";
					if (cAmPm==0){
						sAmPm = "AM";
					} else if (cAmPm==1){
						sAmPm = "PM";
					}
					
					String cTime = String.format("%02d" , c.get(Calendar.HOUR_OF_DAY))+":"+
							String.format("%02d" , c.get(Calendar.MINUTE))+" "+sAmPm;
					
					report.put("valrep_constebm_report_date_month",sMonth);
					report.put("valrep_constebm_report_date_day",String.valueOf(cDay));
					report.put("valrep_constebm_report_date_year", String.valueOf(cYear));
					report.put("valrep_constebm_time_of_report", cTime);
					
					Log.e("DATE: ",String.valueOf(cMonth+1)+" "+String.valueOf(cDay)+" "+String.valueOf(cYear));
					Log.e("TIME: ",cTime);
//get current date and time and submit to CC
					
					report.put("valrep_constebm_valuation_remarks", im.getreport_valuation_remarks());
					
					
					//address
					List<Report_Accepted_Jobs> addList = db.getReport_Accepted_Jobs(String.valueOf(record_id));
					if (!addList.isEmpty()) {
						for (Report_Accepted_Jobs ad : addList) {
							address_obj = new JSONObject();
							address_obj.put("app_unit_no", ad.getunit_no());
							address_obj.put("app_bldg", ad.getbuilding_name());
							address_obj.put("app_lot_no", ad.getlot_no());
							address_obj.put("app_block_no", ad.getblock_no());
							address_obj.put("app_street_no", ad.getstreet_no());
							address_obj.put("app_street_name", ad.getstreet_name());
							address_obj.put("app_village", ad.getvillage());
							address_obj.put("app_district", ad.getdistrict());
							address_obj.put("app_zip", ad.getzip_code());
							address_obj.put("app_city", ad.getcity());
							address_obj.put("app_province", ad.getprovince());
							address_obj.put("app_region", ad.getregion());
							address_obj.put("app_country", ad.getcountry());
							address_ary.put(address_obj);

							//ADDED BY IAN
							report.put("app_daterequested_day", ad.getdr_day());
							report.put("app_daterequested_year", ad.getdr_year());
							report.put("app_account_first_name", ad.getfname());
							report.put("app_account_middle_name", ad.getmname());
							report.put("app_account_last_name", ad.getlname());
							report.put("app_requesting_party", ad.getrequesting_party());
							report.put("app_requestor", ad.getrequestor());
							args[0]=record_id;
							report.put("app_account_is_company",db2.getRecord("app_account_is_company",where,args,"tbl_report_accepted_jobs").get(0));
							report.put("app_account_company_name", db2.getRecord("app_account_company_name",where,args,"tbl_report_accepted_jobs").get(0));
							report.put("app_unacceptable_collateral", db2.getRecord("app_unacceptable_collateral",where,args,"tbl_report_accepted_jobs").get(0));
						}		
					}
					app_req_obj.put("app_collateral_address", address_ary);
					args[0]=record_id;
					app_req_obj.put("app_purpose_appraisal", db2.getRecord("nature_appraisal",where,args,"tbl_report_accepted_jobs").get(0));
					app_req_obj.put("app_kind_of_appraisal", db2.getRecord("kind_of_appraisal",where,args,"tbl_report_accepted_jobs").get(0));

					report.put("appraisal_request",app_req_obj);
					
					record_temp.put("record", report);

					// for attachment
					// attachments
					List<Report_filename> rf = db.getReport_filename(record_id);
						if (freeFields) {
					if (!rf.isEmpty()) {
						for (Report_filename report_file : rf) {
							db_app_uid = report_file.getuid();
							db_app_file = report_file.getfile();
							db_app_filename = report_file.getfilename();
							db_app_appraisal_type = report_file
									.getappraisal_type();
							db_app_candidate_done = report_file
									.getcandidate_done();

							appraisal_attachments = new JSONObject();
							appraisal_attachments.put("uid", db_app_uid);
							appraisal_attachments.put("file", db_app_file);
							appraisal_attachments.put("filename",
									db_app_filename);
							appraisal_attachments.put("appraisal_type",
									db_app_appraisal_type);
							appraisal_attachments.put("candidate_done",
									db_app_candidate_done);
							app_attachments.put(appraisal_attachments);
							appraisal_attachments_data.put("attachments",
									app_attachments);

							String query = query_temp.toString();
							String record = record_temp.toString();
							UserFunctions userFunction = new UserFunctions();
							// attachments
							String attachment = appraisal_attachments_data
									.toString();
							userFunction.SendCaseCenter_Attachments(attachment);

							boolean internetSpeedFast = Connectivity.isConnectedFast(getBaseContext());
							if (internetSpeedFast==true){
								
								runOnUiThread(new Runnable() {
									public void run() {
										gs.fastInternet = true;
										db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
										statusDisplay();
										Toast toast = Toast.makeText(getBaseContext(), "Connected", Toast.LENGTH_LONG);
										View view = toast.getView();
										view.setBackgroundResource(R.color.toast_color);
										toast.show();
										
									}
								});
								//CODE TO BE CHANGE IN TLS IAN
									// API
								//for tls
								JSONObject jsonResponse = new JSONObject();
								ArrayList<String> field = new ArrayList<>();
								ArrayList<String> value = new ArrayList<>();
								field.clear();
								field.add("auth_token");
								field.add("query");
								field.add("data");

								value.clear();
								value.add(gs.auth_token);
								value.add(query);
								value.add(record);


							/*	if(Global.type.contentEquals("tls")){
									jsonResponse = gds.makeHttpsRequest(gs.update_url, "POST", field, value);
									xml = jsonResponse.toString();
								}else{
									xml=gds.http_posting(field,value,gs.update_url,Construction_ebm.this);
								}*/
								if(Global.type.contentEquals("tls")){
									if (globalSave){
										jsonResponse = gds.makeHttpsRequest(gs.update_url, "POST", field, value);
									} else {
										jsonResponse = gds.makeHttpsRequest(gs.matrix_url, "POST", field, value);
									}
									xml = jsonResponse.toString();
								}else{
//									xml=gds.http_posting(field,value,gs.update_url,getApplicationContext());
									if (globalSave){
										xml=gds.http_posting(field,value,gs.update_url,getApplicationContext());
									} else {
										xml=gds.http_posting(field,value,gs.matrix_url,getApplicationContext());
									}
								}
									Log.e("response", xml);
									//END OF NEED TO CHANGE IN TLS IAN
									//check CC field status
									// if application_status == report review then data is submitted
									JSONObject mainxml = new JSONObject(xml);
									String recordxml, application_status;
									if (mainxml.has("record")) {
										Log.e("record", mainxml.getString("record"));
										if (mainxml.has("record")) {
											recordxml = mainxml.getString("record");
											Log.e("recordxml", recordxml);
											JSONObject json_application_status = new JSONObject(recordxml);
											if (json_application_status.has("application_status")){
												application_status = json_application_status.getString("application_status");
												Log.e("application_status", application_status);
												
												if (application_status.contentEquals("report_review")){
													appStatReviewer = true;
												}else if(application_status.contentEquals("afg_valuation")) {
													appStatReviewer = true;
												}else{
													appStatReviewer = false;
												}
												
											}
										}
									}//end of check CC field
									
								} else {
									runOnUiThread(new Runnable() {
										public void run() {
											db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
											statusDisplay();
											Toast toast = Toast.makeText(getBaseContext(), "Cannot send the data because your internet connection is slow/disconnected/unstable. \nPlease make sure that you are connected to a stable connection", Toast.LENGTH_LONG);
											View view = toast.getView();
											view.setBackgroundResource(R.color.toast_red);
											toast.show();
										}
									});
								}
							}
					}

							if(!globalSave) {
								runOnUiThread(new Runnable() {
									public void run() {
										if (freeFields && appStatReviewer) {
											gs.fieldsComplete = freeFields;
											gs.sent = true;
											db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
											gs.online = false;
											gs.connectedToServer = false;
											gs.fastInternet = false;
											gs.fieldsComplete = false;
											gs.sent = false;
											add_submitted_Delete();
											Toast toast = Toast.makeText(getBaseContext(), "Data Updated", Toast.LENGTH_LONG);
											View view = toast.getView();
											view.setBackgroundResource(R.color.toast_blue);
											toast.show();
										} else {

											Toast toast = Toast.makeText(getBaseContext(), "Submmission failed due to incomplete data.", Toast.LENGTH_LONG);
											View view = toast.getView();
											view.setBackgroundResource(R.color.toast_red);
											toast.show();
											finish();

										}
									}
								});
							}
					}//end of if freeFields == true
					else if (!freeFields){
						runOnUiThread(new Runnable() {
			                public void run() {
			                	db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
			                	statusDisplay();
			                	Toast toast = Toast.makeText(getBaseContext(), "You need to fill up all required fields before you can submit the report.", Toast.LENGTH_LONG);
								View view = toast.getView();
								view.setBackgroundResource(R.color.toast_red);
								toast.show();
			                }
			            });
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  catch (Exception e) {
					e.printStackTrace();
					Log.e("Update CC Failed",
							"Exception : " + e.getMessage(), e);
		            runOnUiThread(new Runnable() {
		                public void run() {
		                	Toast.makeText(getApplicationContext(), "Failed to send data. Please try again.",
									Toast.LENGTH_LONG).show();
							fileUploaded=false;
							upload_status(fileUploaded);
							error_uploading();
		                }
		            });
		            
				}

			}
		}
	}

	public void add_submitted_Delete(){

		String temp_is_company = db2.getRecord("app_account_is_company",where,args,"tbl_report_accepted_jobs").get(0);
		String temp_company_name = db2.getRecord("app_account_company_name",where,args,"tbl_report_accepted_jobs").get(0);

		if(temp_is_company.contentEquals("true")){
			account_fname = temp_company_name;
			account_mname="";
			account_lname="";
		}
		ArrayList<String> field = new ArrayList<String>();
		field.clear();
		field.add("record_id");
		field.add("first_name");
		field.add("middle_name");
		field.add("last_name");
		field.add("appraisal_type");
		field.add("status");
		ArrayList<String> value = new ArrayList<String>();
		value.clear();
		value.add(record_id);
		value.add(account_fname);
		value.add(account_mname);
		value.add(account_lname);
		value.add("Construction with EBM");
		value.add("submitted");
		String where ="WHERE record_id = args";
		String[] args = new String[1];
		args[0] = record_id;

		String r_id = db2.getRecord("record_id",where,args,"tbl_report_submitted").get(0);
		if(r_id.contentEquals(record_id)) {
			db2.deleteRecord("tbl_report_submitted", "record_id = ?", new String[] { record_id });
			db2.addRecord(value, field, "tbl_report_submitted");
		}else{
			db2.addRecord(value, field, "tbl_report_submitted");
		}
		//delete

		db2.deleteRecord("construction_ebm", "record_id = ?", new String[] { record_id });
		db2.deleteRecord("construction_ebm_room_list", "record_id = ?", new String[] { record_id });
		db2.deleteRecord("tbl_attachments", "record_id = ?", new String[] { record_id });
		db2.deleteRecord("tbl_report_accepted_jobs", "record_id = ?", new String[] { record_id });
		db2.deleteRecord("tbl_report_accepted_jobs_contacts", "record_id = ?", new String[] { record_id });
		db2.deleteRecord("submission_logs", "record_id = ?", new String[] { record_id });
		db2.deleteRecord("report_filename", "record_id = ?", new String[] { record_id });
	}
	private class SendData extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(Construction_ebm.this);
			pDialog.setMessage("Updating data..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.setCanceledOnTouchOutside(false);
			pDialog.show();
		}

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			NetworkUtil.getConnectivityStatusString(Construction_ebm.this);
			if (!NetworkUtil.status.equals("Network not available")) {
				gs.online = true;
				google_response = gds.google_ping();
				if (google_response.equals("200")) {
					
					gs.connectedToServer = true;
					boolean internetSpeedFast = Connectivity.isConnectedFast(getBaseContext());
					if (internetSpeedFast){
						runOnUiThread(new Runnable() {
							public void run() {
								gs.fastInternet = true;
								Toast toast = Toast.makeText(getBaseContext(), "Connected", Toast.LENGTH_LONG);
								View view = toast.getView();
								view.setBackgroundResource(R.color.toast_color);
								toast.show();
							}
						});
						
						
						//create_json();
						//db_delete_all();
						if (withAttachment){
							upload_attachment();
						} else if (!withAttachment){
							globalSave=false;
							create_json();
//							db_delete_all();//temp disabled
							db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
							finish();
						}
					} else {
						runOnUiThread(new Runnable() {
							public void run() {
								Toast toast = Toast.makeText(getBaseContext(), "Cannot send the data because your internet connection is slow. \nPlease make sure that you are connected to a stable connection", Toast.LENGTH_LONG);
								View view = toast.getView();
								view.setBackgroundResource(R.color.toast_red);
								toast.show();
							}
						});
					}
					
					network_status = true;
				} else {
					runOnUiThread(new Runnable() {
						public void run() {
							db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
							statusDisplay();
							Toast toast = Toast.makeText(getBaseContext(), "Unable to connect to the server", Toast.LENGTH_LONG);
							View view = toast.getView();
							view.setBackgroundResource(R.color.toast_red);
							toast.show();
							serverError();
						}
					});
					
				}
			} else {
				network_status = false;
				runOnUiThread(new Runnable() {
					public void run() {
						db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
						statusDisplay();
					}
				});
			}

			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			pDialog.dismiss();
			if (network_status == false) {
				db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
				statusDisplay();
				Toast.makeText(getApplicationContext(),
						"Network not available", Toast.LENGTH_SHORT).show();
			} else if (network_status == true) {
				if (google_response.equals("200")) {
					if (fileUploaded){

						/*if (freeFields&&appStatReviewer){
							gs.fieldsComplete = freeFields;
							gs.sent = true;
							db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
							gs.online = false;
							gs.connectedToServer = false;
							gs.fastInternet = false;
							gs.fieldsComplete = false;
							gs.sent = false;
		                	Toast toast = Toast.makeText(getBaseContext(), "Data Updated", Toast.LENGTH_LONG);
							View view = toast.getView();
							view.setBackgroundResource(R.color.toast_blue);
							toast.show();
							finish();
						}else{
							Toast toast = Toast.makeText(getBaseContext(), "Submmission failed due to incomplete data.", Toast.LENGTH_LONG);
							View view = toast.getView();
							view.setBackgroundResource(R.color.toast_red);
							toast.show();
							finish();
						}*/
						
					} else{
						Toast.makeText(getApplicationContext(), "Data Retained",Toast.LENGTH_LONG).show();
						error_uploading();//popup dialog error
					}
				}
				finish();
			}

		}

	}
	private class GlobalSave extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(Construction_ebm.this);
			pDialog.setMessage("Generating report...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.setCanceledOnTouchOutside(false);
			pDialog.show();
		}

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			NetworkUtil.getConnectivityStatusString(Construction_ebm.this);
			if (!NetworkUtil.status.equals("Network not available")) {
				if(gds.google_ping().equals("200")){
					if(Connectivity.isConnectedFast(getBaseContext())){
						globalSave=true;
						create_json();
						return true;
					}else{
						runOnUiThread(new Runnable() {
							public void run() {
								db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
								statusDisplay();
								Toast toast = Toast.makeText(getBaseContext(), "Slow Internet connection.", Toast.LENGTH_LONG);
								View view = toast.getView();
								view.setBackgroundResource(R.color.toast_red);
								toast.show();
								serverError();
							}
						});
						return false;
					}

				}else{
					runOnUiThread(new Runnable() {
						public void run() {
							db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
							statusDisplay();
							Toast toast = Toast.makeText(getBaseContext(), "Unable to connect to the server", Toast.LENGTH_LONG);
							View view = toast.getView();
							view.setBackgroundResource(R.color.toast_red);
							toast.show();
							serverError();
						}
					});
					return false;
				}

			}else{
				runOnUiThread(new Runnable() {
					public void run() {
						db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
						statusDisplay();
						Toast toast = Toast.makeText(getBaseContext(), "Network not available.", Toast.LENGTH_LONG);
						View view = toast.getView();
						view.setBackgroundResource(R.color.toast_red);
						toast.show();
						serverError();
					}
				});
				return false;
			}

		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			if(result){
				Intent in = new Intent(getApplicationContext(), View_Report.class);
				in.putExtra(TAG_RECORD_ID, record_id);
				in.putExtra("appraisal_type", "construction_ebm");
				startActivityForResult(in, 100);
			}
			pDialog.dismiss();


		}

	}
	public void serverError(){
		
		myDialog = new Dialog(Construction_ebm.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.warning_dialog);
		myDialog.setCancelable(true);
		final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
		final Button btn_ok = (Button) myDialog.findViewById(R.id.btn_ok);
		
		tv_question.setText(R.string.server_error);
		btn_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		myDialog.show();
	}
	
	public void error_uploading(){
		myDialog = new Dialog(Construction_ebm.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.warning_dialog);
		myDialog.setCancelable(true);
		final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
		final Button btn_ok = (Button) myDialog.findViewById(R.id.btn_ok);
		tv_question.setText(R.string.error_uploading);
		btn_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		myDialog.show();
	}

	public void create_pdf() {
		pdf_name = uid + "_"
				+ "Property Pictures" + "_" + counter;
		SharedPreferences appPrefs = getSharedPreferences("preference",
				MODE_PRIVATE);
		SharedPreferences.Editor prefsEditor = appPrefs.edit();
		prefsEditor.putString("pdf", pdf_name);
		prefsEditor.putString("fname", account_fname);
		prefsEditor.putString("lname", account_lname);
		prefsEditor.commit();
		startActivity(new Intent(Construction_ebm.this,
				Pdf_Townhouse_condo.class));

	}

	public void open_customdialog() {
		// dialog
		myDialog = new Dialog(context);
		myDialog.setTitle("Select Attachment");
		myDialog.setContentView(R.layout.request_pdf_list);
		// myDialog.setTitle("My Dialog");

		dlg_priority_lvw = (ListView) myDialog
				.findViewById(R.id.dlg_priority_lvw);
		// ListView
		SimpleAdapter adapter = new SimpleAdapter(context, getPriorityList(),
				R.layout.request_pdf_list_layout, new String[] {
						"txt_pdf_list", "mono" }, new int[] {
						R.id.txt_pdf_list, R.id.mono });
		dlg_priority_lvw.setAdapter(adapter);

		// ListView
		dlg_priority_lvw
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {

						item = ((TextView) arg1.findViewById(R.id.txt_pdf_list))
								.getText().toString();
						attachment_selected = item;
						tv_attachment.setText(attachment_selected);
						// update


						Report_filename rf = new Report_filename();
						rf.setuid(uid);
						rf.setfile("Property Pictures");
						rf.setfilename(attachment_selected);
						rf.setappraisal_type(appraisal_type + "_" + counter);
						rf.setcandidate_done("true");
						db.updateReport_filename(rf, record_id);
						db.close();
						myDialog.dismiss();
						checkFileSize();
					}
				});
		dlg_priority_lvw.setLongClickable(true);
		dlg_priority_lvw
				.setOnItemLongClickListener(new OnItemLongClickListener() {
					@Override
					public boolean onItemLongClick(AdapterView<?> arg0,
							View arg1, final int arg2, long arg3) {
						item = ((TextView) arg1.findViewById(R.id.txt_pdf_list))
								.getText().toString();
						Toast.makeText(getApplicationContext(), item,
								Toast.LENGTH_SHORT).show();

						File file = new File(myDirectory, item);

						if (file.exists()) {
							Uri path = Uri.fromFile(file);
							Intent intent = new Intent(Intent.ACTION_VIEW);
							intent.setDataAndType(path, "application/pdf");
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

							try {
								startActivity(intent);
							} catch (ActivityNotFoundException e) {
								Toast.makeText(getApplicationContext(),
										"No Application Available to View PDF",
										Toast.LENGTH_SHORT).show();
							}
						}

						return true;

					}
				});
		myDialog.show();
	}

	private List<HashMap<String, Object>> getPriorityList() {
		DatabaseHandler db = new DatabaseHandler(
				context.getApplicationContext());
		List<HashMap<String, Object>> priorityList = new ArrayList<HashMap<String, Object>>();
		List<Images> images = db.getAllImages(uid);//added account lname for specific search result like
		if (!images.isEmpty()) {
			for (Images im : images) {
				filename = im.getfilename();
				String filenameArray[] = filename.split("\\.");
				String extension = filenameArray[filenameArray.length - 1];
				if (extension.equals("pdf")) {
					HashMap<String, Object> map1 = new HashMap<String, Object>();
					map1.put("txt_pdf_list", im.getfilename());
					map1.put("mono", R.drawable.attach_item);
					priorityList.add(map1);
				}

			}

		}
		return priorityList;
	}
	
	public void checkFileSize(){
		myDialog = new Dialog(Construction_ebm.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.warning_dialog);
		myDialog.setCancelable(true);
		final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
		final Button btn_ok = (Button) myDialog.findViewById(R.id.btn_ok);
		tv_question.setText(R.string.file_size_too_large);
		//format double to 2 decimal place
		DecimalFormat df = new DecimalFormat("#.00");
		//get fileSize and convert to KB and MB
		File filenew = new File(myDirectory+"/"+item);//get path concat with slash and file name
		int file_size_kb = Integer.parseInt(String.valueOf(filenew.length()/1024));
		double file_size_mb = file_size_kb/1024.00;
		//custom toast
		Toast toast = Toast.makeText(context, item +
				"\nfile size in KB: "+file_size_kb+"KB" +
				"\nfile size in MB: "+df.format(file_size_mb)+"MB", Toast.LENGTH_LONG);
		View view = toast.getView();
		view.setBackgroundResource(R.color.toast_color);
		toast.show();
		
		if (file_size_kb>6144){//restrict the file size up to 6.0MB only
			//clear textView to restict uploading
			Toast.makeText(getApplicationContext(), "file size is too large",
					Toast.LENGTH_SHORT).show();
			tv_attachment.setText("");
			myDialog.show();
		}
		
		btn_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		
	}

	public void upload_attachment() {
		// upload pdf
		upload_status(gds.uploadFile(Environment.getExternalStorageDirectory() + "/" + dir + "/"
				+ tv_attachment.getText().toString()));
	}

	/*public int uploadFile(String sourceFileUri) {
		TLSConnection tlscon = new TLSConnection();
		String upLoadServerUri = gs.pdf_upload_url;
		String fileName = sourceFileUri;

		HttpsURLConnection conn = null;
		HttpURLConnection conn2=null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		File sourceFile = new File(sourceFileUri);
		if (!sourceFile.isFile()) {
			Log.e("uploadFile", "Source File Does not exist");
			return 0;
		}
		try { // open a URL connection to the Servlet
			FileInputStream fileInputStream = new FileInputStream(sourceFile);
			URL url = new URL(upLoadServerUri);


			if(Global.type.contentEquals("tls")){

				conn = tlscon.setUpHttpsConnection(""+url);

				conn.setDoInput(true); // Allow Inputs
				conn.setDoOutput(true); // Allow Outputs
				conn.setUseCaches(false); // Don't use a Cached Copy
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Connection", "Keep-Alive");
				conn.setRequestProperty("ENCTYPE", "multipart/form-data");
				conn.setRequestProperty("Content-Type",
						"multipart/form-data;boundary=" + boundary);
				conn.setRequestProperty("uploaded_file", fileName);
				dos = new DataOutputStream(conn.getOutputStream());
			}else{

				conn2 = (HttpURLConnection) url.openConnection();
				conn2.setDoInput(true); // Allow Inputs
				conn2.setDoOutput(true); // Allow Outputs
				conn2.setUseCaches(false); // Don't use a Cached Copy
				conn2.setRequestMethod("POST");
				conn2.setRequestProperty("Connection", "Keep-Alive");
				conn2.setRequestProperty("ENCTYPE", "multipart/form-data");
				conn2.setRequestProperty("Content-Type",
						"multipart/form-data;boundary=" + boundary);
				conn2.setRequestProperty("uploaded_file", fileName);
				dos = new DataOutputStream(conn2.getOutputStream());
			}

			*//*conn = tlscon.setUpHttpsConnection(""+url); // Open a HTTP
			// the URL
			conn.setDoInput(true); // Allow Inputs
			conn.setDoOutput(true); // Allow Outputs
			conn.setUseCaches(false); // Don't use a Cached Copy
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("ENCTYPE", "multipart/form-data");
			conn.setRequestProperty("Content-Type",
					"multipart/form-data;boundary=" + boundary);
			conn.setRequestProperty("uploaded_file", fileName);
			dos = new DataOutputStream(conn.getOutputStream());*//*

			dos.writeBytes(twoHyphens + boundary + lineEnd);
			dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
					+ fileName + "\"" + lineEnd);
			dos.writeBytes(lineEnd);

			bytesAvailable = fileInputStream.available(); // create a buffer of
			// maximum size

			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];

			// read file and write it into form...
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);

			while (bytesRead > 0) {
				dos.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}

			// send multipart form data necesssary after file data...
			dos.writeBytes(lineEnd);
			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			// Responses from the server (code and message)
			String serverResponseMessage="";
			if(Global.type.contentEquals("tls")){
				serverResponseCode = conn.getResponseCode();
				serverResponseMessage = conn.getResponseMessage();
			}else{
				serverResponseCode = conn2.getResponseCode();
				serverResponseMessage = conn2.getResponseMessage();
			}

			*//*serverResponseCode = conn.getResponseCode();
			String serverResponseMessage = conn.getResponseMessage();*//*

			Log.i("uploadFile", "HTTP Response is : " + serverResponseMessage
					+ ": " + serverResponseCode);
			if (serverResponseCode == 200) {
				runOnUiThread(new Runnable() {
					public void run() {
					}
				});
			}

			// close the streams //
			fileInputStream.close();
			dos.flush();
			dos.close();

			//if upload succeeded
			fileUploadFailed = "false";
			upload_status();

		} catch (MalformedURLException ex) {
			ex.printStackTrace();
			Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("Upload file to server Exception",
					"Exception : " + e.getMessage(), e);
			runOnUiThread(new Runnable() {
				public void run() {
					Toast.makeText(getApplicationContext(), "Failed to upload File / Images",
							Toast.LENGTH_LONG).show();
					fileUploadFailed = "true";
					upload_status();
				}
			});
		}
		return serverResponseCode;
	}*/

	public void upload_status(boolean response) {
		fileUploaded = response;
		if (response){
			globalSave=false;
			create_json();

		}
	}

	public void open_pdf() {
		File file = new File(myDirectory, tv_attachment.getText().toString());

		if (file.exists()) {
			Uri path = Uri.fromFile(file);
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setDataAndType(path, "application/pdf");
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			try {
				startActivity(intent);
			} catch (ActivityNotFoundException e) {
				Toast.makeText(getApplicationContext(),
						"No Application Available to View PDF",
						Toast.LENGTH_SHORT).show();
			}
		}

	}

	public void uploaded_attachments() {
		// set value
		List<Required_Attachments> required_attachments = db
				.getAllRequired_Attachments_byid(String.valueOf(appraisal_type));
		if (!required_attachments.isEmpty()) {
			for (final Required_Attachments ra : required_attachments) {
				LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

				final View addView = layoutInflater.inflate(
						R.layout.report_uploaded_attachments_dynamic, null);
				final TextView tv_attachment = (TextView) addView
						.findViewById(R.id.tv_attachment);
				final ImageView btn_view = (ImageView) addView
						.findViewById(R.id.btn_view);
				tv_attachment.setText(ra.getattachment());
				btn_view.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						uploaded_attachment = pdf_name =  uid + "_"
								+ ra.getattachment() + "_" + counter + ".pdf";
						// directory
						uploaded_file = new File(myDirectory,
								uploaded_attachment);
						uploaded_attachment = uploaded_attachment.replaceAll(
								" ", "%20");
						//url_webby = "http://dv-dev.gdslinkasia.com/attachments/"
						//		+ uploaded_attachment;
						url_webby = "http://dev.gdslinkasia.com:8080/pdf_attachments/"
								+ uploaded_attachment;
						view_pdf(uploaded_file);
					}
				});
				container_attachments.addView(addView);

			}

		}

	}

	private void view_download() {
		android.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Action");
		builder.setItems(commandArray, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int index) {
				if (index == 0) {
					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
							.parse(url_webby));
					startActivity(browserIntent);
					dialog.dismiss();
				} else if (index == 1) {
					new DownloadFileFromURL().execute(url_webby);
					dialog.dismiss();
				}
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public void view_pdf(File file) {
		if (file.exists()) {
			Uri path = Uri.fromFile(file);
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setDataAndType(path, "application/pdf");
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			try {
				startActivity(intent);
			} catch (ActivityNotFoundException e) {
				Toast.makeText(getApplicationContext(),
						"No Application Available to View PDF",
						Toast.LENGTH_SHORT).show();
			}
		} else {
			NetworkUtil.getConnectivityStatusString(this);
			if (!NetworkUtil.status.equals("Network not available")) {
				new Attachment_validation().execute();
			} else {
				Toast.makeText(getApplicationContext(),
						"Network not available", Toast.LENGTH_SHORT).show();
			}
		}
	}

	public void db_delete_all() {
		String[] involvedTables = {"construction_ebm_room_list", "submission_logs"};
		for (int x=0; x<involvedTables.length; x++){
			db2.deleteAllDB2(record_id, involvedTables[x]);
			db2.close();
		}
		
		Report_Accepted_Jobs raj = new Report_Accepted_Jobs();
		raj.setrecord_id(record_id);
		db.deleteReport_Accepted_Jobs(raj);
		db.close();

		Report_Accepted_Jobs_Contacts rajc = new Report_Accepted_Jobs_Contacts();
		rajc.setrecord_id(record_id);
		db.deleteReport_Accepted_Jobs_Contacts(rajc);
		db.close();

		Report_filename rf = new Report_filename();
		rf.setrecord_id(record_id);
		db.deleteReport_filename(rf);
		db.close();

		Construction_Ebm_API ce = new Construction_Ebm_API();
		ce.setrecord_id(record_id);
		db.deleteConstruction_Ebm_API(ce);
		db.close();
	}
	
	public void manual_delete_all() {
		
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.delete_dialog);

		Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
		Button btn_no = (Button) dialog.findViewById(R.id.btn_no);
		// set the custom dialog components - text, image and button
		TextView text = (TextView) dialog.findViewById(R.id.tv_question);
		dialog.show();
		
		text.setText("Do you want to delete this record?");
		
		// if button is clicked, close the custom dialog
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String[] involvedTables = {"construction_ebm_room_list"};
				for (int x=0; x<involvedTables.length; x++){
					db2.deleteAllDB2(record_id, involvedTables[x]);
					db2.close();
				}
				
				Report_Accepted_Jobs raj = new Report_Accepted_Jobs();
				raj.setrecord_id(record_id);
				db.deleteReport_Accepted_Jobs(raj);
				db.close();

				Report_Accepted_Jobs_Contacts rajc = new Report_Accepted_Jobs_Contacts();
				rajc.setrecord_id(record_id);
				db.deleteReport_Accepted_Jobs_Contacts(rajc);
				db.close();

				Report_filename rf = new Report_filename();
				rf.setrecord_id(record_id);
				db.deleteReport_filename(rf);
				db.close();

				Construction_Ebm_API ce = new Construction_Ebm_API();
				ce.setrecord_id(record_id);
				db.deleteConstruction_Ebm_API(ce);
				db.close();
				
				dialog.dismiss();
				finish();
			}
		});
		
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

	}

	public void check_attachment(){
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.yes_no_dialog);

		Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
		Button btn_no = (Button) dialog.findViewById(R.id.btn_no);
		// set the custom dialog components - text, image and button
		TextView text = (TextView) dialog.findViewById(R.id.tv_question);
		dialog.show();
		
		//check if with or without attachment
		if (!tv_attachment.getText().toString().equals("")){
			text.setText("Are you sure you want to submit ?");
			withAttachment = true;
		} else if (tv_attachment.getText().toString().equals("")){
			text.setText("Do you want to submit report without file/pdf attachment?");
			withAttachment = false;
			fileUploaded = true;
		}
		//check if fresh data or rework
		//aug 17
		args[0] = record_id;
		if(db2.getRecord("application_status",where,args,"tbl_report_accepted_jobs").get(0).contentEquals("")){
			freshData = true;
		}else{
			freshData = false;
		}
		
		// if button is clicked, close the custom dialog
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (((withAttachment==true)&&(freshData==true))||
						((withAttachment==true)&&(freshData==false))){
//					new SendData().execute();
					if (new Fields_Checker().isModuleComplete(record_id, context)) {
						new SendData().execute();
//                        Toast.makeText(getApplicationContext(), "Module fields complete", Toast.LENGTH_LONG).show();
					} else {
						Toast.makeText(getApplicationContext(), "Module fields incomplete", Toast.LENGTH_LONG).show();
					}
					dialog.dismiss();
				} else if (((withAttachment==false)&&(freshData==true))||
						((withAttachment==false)&&(freshData==false))){
					no_attachment();
					dialog.dismiss();
				}
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		
	}
	
	public void no_attachment(){
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.yes_no_dialog);

		Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
		Button btn_no = (Button) dialog.findViewById(R.id.btn_no);
		// set the custom dialog components - text, image and button
		TextView text = (TextView) dialog.findViewById(R.id.tv_question);
		dialog.show();
		
		text.setText("Are you sure you want to submit ?");
		
		// if button is clicked, close the custom dialog
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				withAttachment = false;
//				new SendData().execute();
				if (new Fields_Checker().isModuleComplete(record_id, context)) {
					new SendData().execute();
//                        Toast.makeText(getApplicationContext(), "Module fields complete", Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(getApplicationContext(), "Module fields incomplete", Toast.LENGTH_LONG).show();
				}
				dialog.dismiss();
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
	}
	
	/*public void update_dialog() {
		// custom dialog
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.yes_no_dialog);

		// set the custom dialog components - text, image and button
		TextView text = (TextView) dialog.findViewById(R.id.tv_question);
		text.setText("Are you sure you want to submit ?");
		Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
		Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

		// if button is clicked, close the custom dialog
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new SendData().execute();
				dialog.dismiss();
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}*/

	private class Attachment_validation extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(Construction_ebm.this);
			pDialog.setMessage("Checking attachment..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			try {
				URL obj = new URL(url_webby);
				HttpsURLConnection con;
				HttpURLConnection con2;
				if(Global.type.contentEquals("tls")){
					con = tlscon.setUpHttpsConnection("" +obj);
					con.setRequestMethod("GET");
					con.setRequestProperty("User-Agent", "Mozilla/5.0");
					responseCode = con.getResponseCode();
				}else{
					con2 = (HttpURLConnection) obj.openConnection();
					con2.setRequestMethod("GET");
					con2.setRequestProperty("User-Agent", "Mozilla/5.0");
					responseCode = con2.getResponseCode();
				}
				Log.e("", String.valueOf(responseCode));
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			Log.e("", url_webby);
			if (responseCode == 200) {
				view_download();
			} else if (responseCode == 404) {
				Toast.makeText(getApplicationContext(),
						"Attachment doesn't exist", Toast.LENGTH_SHORT).show();
			}
			pDialog.dismiss();

		}
	}

	/*public void google_ping() {
		try {
//			URL obj = new URL("https://www.google.com.ph/?gws_rd=cr&ei=td9kUrP0H8mjigenuoHAAg");
			URL obj = new URL(gs.server_url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");

			// add request header
			con.setRequestProperty("User-Agent", "Mozilla/5.0");

			google_response = String.valueOf(con.getResponseCode());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/

	/**
	 * Showing Dialog
	 * */
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case progress_bar_type:
			pDialog = new ProgressDialog(this);
			pDialog.setMessage("Downloading file. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setMax(100);
			pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pDialog.setCancelable(true);
			pDialog.setCanceledOnTouchOutside(false);
			pDialog.show();
			return pDialog;
		default:
			return null;
		}
	}

	/**
	 * Background Async Task to download file
	 * */
	class DownloadFileFromURL extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Bar Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showDialog(progress_bar_type);
		}

		/**
		 * Downloading file in background thread
		 * */
		@Override
		protected String doInBackground(String... f_url) {
			int count;
			try {
				URL url = new URL(f_url[0]);
				URLConnection conection = url.openConnection();
				conection.connect();
				// getting file length
				int lenghtOfFile = conection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(),
						8192);

				// Output stream to write file
				OutputStream output = new FileOutputStream(
						uploaded_file.toString());

				byte data[] = new byte[1024];

				long total = 0;

				while ((count = input.read(data)) != -1) {
					total += count;
					// publishing the progress....
					// After this onProgressUpdate will be called
					publishProgress("" + (int) ((total * 100) / lenghtOfFile));

					// writing data to file
					output.write(data, 0, count);
				}

				// flushing output
				output.flush();

				// closing streams
				output.close();
				input.close();

			} catch (Exception e) {
				Log.e("Error: ", e.getMessage());
			}

			return null;
		}

		/**
		 * Updating progress bar
		 * */
		protected void onProgressUpdate(String... progress) {
			// setting progress percentage
			pDialog.setProgress(Integer.parseInt(progress[0]));
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		@Override
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after the file was downloaded
			removeDialog(progress_bar_type);
			Toast.makeText(getApplicationContext(), "Attachment Downloaded",
					Toast.LENGTH_LONG).show();
			view_pdf(uploaded_file);

		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent in;
		switch (v.getId()) {
		case R.id.btn_report_page1:
			custom_dialog_page1();
			break;
		case R.id.btn_report_page2:
			custom_dialog_page2();
			break;
		case R.id.btn_report_page3:
			in = new Intent(getApplicationContext(),
					Construction_ebm_room_list.class);
			in.putExtra(TAG_RECORD_ID, record_id);
			startActivityForResult(in, 100);
			break;
		case R.id.btn_report_page4:
			custom_dialog_page4();
			break;
		case R.id.btn_update_address: //address
			custom_dialog_address();
			break;
		case R.id.btn_mysql_attachments: //attachments
			//in = new Intent(getApplicationContext(), Uploaded_Attachments_All.class);
			in = new Intent(getApplicationContext(), Attachments_Inflater.class);
			in.putExtra(TAG_RECORD_ID, record_id);
			in.putExtra(TAG_PASS_STAT, pass_stat);
			in.putExtra(TAG_UID, uid);
			startActivityForResult(in, 100);
			break;
		case R.id.btn_report_update_cc:
			//update_dialog();
			check_attachment();
			break;
			case R.id.btn_report_view_report:
				globalSave=true;
				new GlobalSave().execute();
				break;
		case R.id.btn_report_pdf:
			//create_pdf();
			Intent createPDF = new Intent(getApplicationContext(), Collage.class);//.putExtra("position", position);
			createPDF.putExtra(TAG_RECORD_ID, record_id);
			startActivity(createPDF);
			break;
		case R.id.btn_select_pdf:
			open_customdialog();
			break;
		case R.id.tv_attachment:
			open_pdf();
			break;
		case R.id.btn_manual_delete:
			manual_delete_all();
			break;
			case R.id.btn_update_app_details: //zonal values
				custom_dialog_app_details();
				break;
		default:
			break;
		}
	}

	public void custom_dialog_app_details() {
		myDialog = new Dialog(Construction_ebm.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.app_details_dialog);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);

		report_date_requested = (EditText) myDialog.findViewById(R.id.report_date_requested);
		et_first_name = (EditText) myDialog.findViewById(R.id.et_first_name);
		et_middle_name = (EditText) myDialog.findViewById(R.id.et_middle_name);
		et_last_name = (EditText) myDialog.findViewById(R.id.et_last_name);
		et_requesting_party = (AutoCompleteTextView) myDialog.findViewById(R.id.et_requesting_party);
		et_requestor = (EditText) myDialog.findViewById(R.id.et_requestor);
		app_account_company_name = (EditText) myDialog.findViewById(R.id.app_account_company_name);
		app_account_is_company = (CheckBox) myDialog.findViewById(R.id.app_account_is_company);
		spinner_purpose_appraisal = (Spinner) myDialog.findViewById(R.id.app_purpose_appraisal);
		et_app_kind_of_appraisal = (EditText) myDialog.findViewById(R.id.app_kind_of_appraisal);

		gds.autocomplete(et_requesting_party,getResources().getStringArray(R.array.requesting_party),this);
		if(app_account_is_company.isChecked()){
			app_account_company_name.setEnabled(true);
			et_first_name.setText("-");
			et_middle_name.setText("-");
			et_last_name.setText("-");

			et_first_name.setEnabled(false);
			et_middle_name.setEnabled(false);
			et_last_name.setEnabled(false);
		}else{
			app_account_company_name.setEnabled(false);
			app_account_company_name.setText("-");
			et_first_name.setEnabled(true);
			et_middle_name.setEnabled(true);
			et_last_name.setEnabled(true);
		}
		app_account_is_company.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					app_account_company_name.setEnabled(true);
					et_first_name.setText("-");
					et_middle_name.setText("-");
					et_last_name.setText("-");
					et_first_name.setEnabled(false);
					et_middle_name.setEnabled(false);
					et_last_name.setEnabled(false);
				} else {
					app_account_company_name.setEnabled(false);
					app_account_company_name.setText("-");
					et_first_name.setEnabled(true);
					et_middle_name.setEnabled(true);
					et_last_name.setEnabled(true);
				}
			}
		});
		btn_save_app_details = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel_app_details = (Button) myDialog.findViewById(R.id.btn_left);
		btn_save_app_details.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method

				boolean fieldsOK,fieldsOK2;
				gds.fill_in_error(new EditText[]{
						et_first_name,et_last_name});
				fieldsOK = gds.validate(new EditText[]{
						et_first_name,et_last_name});

				if(app_account_is_company.isChecked()){
					gds.fill_in_error(new EditText[]{
							app_account_company_name});
					fieldsOK2 = gds.validate(new EditText[]{
							app_account_company_name});
				}else{
					fieldsOK2=true;
				}

				if(fieldsOK&&fieldsOK2) {
				Report_Accepted_Jobs mv = new Report_Accepted_Jobs();


				mv.setfname(et_first_name.getText().toString());
				mv.setmname(et_middle_name.getText().toString());
				mv.setlname(et_last_name.getText().toString());
				mv.setrequesting_party(et_requesting_party.getText().toString());
				mv.setrequestor(et_requestor.getText().toString());


				db.updateApp_Details(mv, record_id);
					field.clear();
					field.add("app_account_is_company");
					field.add("app_account_company_name");
					field.add("nature_appraisal");
					field.add("kind_of_appraisal");
					value.clear();
					value.add(gds.cbChecker(app_account_is_company));
					value.add(app_account_company_name.getText().toString());
					value.add(spinner_purpose_appraisal.getSelectedItem().toString());
					value.add(et_app_kind_of_appraisal.getText().toString());
					db2.updateRecord(value,field,"record_id = ?",new String[] { record_id },"tbl_report_accepted_jobs");
					db.close();
				Toast.makeText(getApplicationContext(), "Saved",
						Toast.LENGTH_SHORT).show();
				myDialog.dismiss();
				Intent intent = getIntent();
				finish();
				startActivity(intent);
				}else{
					Toast.makeText(getApplicationContext(),
							"Please fill up required fields", Toast.LENGTH_SHORT).show();

				}
			}
		});

		btn_cancel_app_details.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method
				myDialog.dismiss();
			}
		});

		List<Report_Accepted_Jobs> report_accepted_jobs = db
				.getReport_Accepted_Jobs(record_id);
		if (!report_accepted_jobs.isEmpty()) {
			for (Report_Accepted_Jobs im : report_accepted_jobs) {
				et_first_name.setText(im.getfname());
				et_middle_name.setText(im.getmname());
				et_last_name.setText(im.getlname());
				et_requesting_party.setText(im.getrequesting_party());
				et_requestor.setText(im.getrequestor());
				gds.cbDisplay(app_account_is_company, db2.getRecord("app_account_is_company", where, args, "tbl_report_accepted_jobs").get(0));
				app_account_company_name.setText(db2.getRecord("app_account_company_name", where, args, "tbl_report_accepted_jobs").get(0));
				//valor2
				spinner_purpose_appraisal.setSelection(gds.spinnervalue(spinner_purpose_appraisal, db2.getRecord("nature_appraisal", where, args, "tbl_report_accepted_jobs").get(0)));

					et_app_kind_of_appraisal.setText(db2.getRecord("kind_of_appraisal", where, args, "tbl_report_accepted_jobs").get(0));

				// set current date into datepicker
				if ((!im.getdr_year().equals(""))&&(!im.getdr_month().equals(""))&&(!im.getdr_day().equals(""))) {
					report_date_requested.setText(im.getdr_month()+"/"+im.getdr_day()+"/"+im.getdr_year());
				} else{
					report_date_requested.setText("");
				}
			}
		}

		myDialog.show();
	}
	@Override
	public void onCheckedChanged(CompoundButton bv, boolean isChecked) {
		// TODO Auto-generated method stub
		switch (bv.getId()) {
		case R.id.foundation_other:
			if (isChecked) {
				foundation_other_value.setVisibility(View.VISIBLE);
			} else {
				foundation_other_value.setVisibility(View.GONE);
				foundation_other_value.setText("");
			}
			break;
		case R.id.post_other:
			if (isChecked) {
				post_other_value.setVisibility(View.VISIBLE);
			} else {
				post_other_value.setVisibility(View.GONE);
				post_other_value.setText("");
			}
			break;
		case R.id.beams_other:
			if (isChecked) {
				beams_other_value.setVisibility(View.VISIBLE);
			} else {
				beams_other_value.setVisibility(View.GONE);
				beams_other_value.setText("");
			}
			break;
		case R.id.floors_other:
			if (isChecked) {
				floors_other_value.setVisibility(View.VISIBLE);
			} else {
				floors_other_value.setVisibility(View.GONE);
				floors_other_value.setText("");
			}
			break;
		case R.id.walls_other:
			if (isChecked) {
				walls_other_value.setVisibility(View.VISIBLE);
			} else {
				walls_other_value.setVisibility(View.GONE);
				walls_other_value.setText("");
			}
			break;
		case R.id.partitions_other:
			if (isChecked) {
				partitions_other_value.setVisibility(View.VISIBLE);
			} else {
				partitions_other_value.setVisibility(View.GONE);
				partitions_other_value.setText("");
			}
			break;
		case R.id.windows_other:
			if (isChecked) {
				windows_other_value.setVisibility(View.VISIBLE);
			} else {
				windows_other_value.setVisibility(View.GONE);
				windows_other_value.setText("");
			}
			break;
		case R.id.doors_other:
			if (isChecked) {
				doors_other_value.setVisibility(View.VISIBLE);
			} else {
				doors_other_value.setVisibility(View.GONE);
				doors_other_value.setText("");
			}
			break;
		case R.id.ceiling_other:
			if (isChecked) {
				ceiling_other_value.setVisibility(View.VISIBLE);
			} else {
				ceiling_other_value.setVisibility(View.GONE);
				ceiling_other_value.setText("");
			}
			break;
		case R.id.roof_other:
			if (isChecked) {
				roof_other_value.setVisibility(View.VISIBLE);
			} else {
				roof_other_value.setVisibility(View.GONE);
				roof_other_value.setText("");
			}
			break;
		default:
			break;
		}
	}
	
	//fields checker
	private boolean validateFields(String[] fields){
        for(int i=0; i<fields.length; i++){
            String currentField=fields[i];
            if(currentField.equals("")){
                return false;
            }
        }
        return true;
	}
	
	public void statusDisplay(){
		List<Logs> sd = db.getLogsLatest(record_id);
		if (!sd.isEmpty()) {
			for (Logs si : sd) {
				actionBar.setSubtitle(si.getstatus_reason() + " ("+si.getdate_submitted()+")");
			}
		} else {
			actionBar.setSubtitle("Not yet submitted");
		}
	}

	public void logsDisplay(){
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.submission_logs);
		android.view.WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.MATCH_PARENT;
		dialog.getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
		TextView tvLog = (TextView) dialog.findViewById(R.id.tvLog);
		String logs = "";
		List<Logs> sd = db.getLogs(record_id);
		if (!sd.isEmpty()) {
			for (Logs si : sd) {
				logs = logs + si.getdate_submitted() +"\t" + si.getstatus() + " - " + si.getstatus_reason() + "\n";
			}
		} 
		tvLog.setText(logs);
		dialog.show();
	}
	public void view_map() {
		String add=tv_address_hide.getText().toString();
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(Construction_ebm.this);
		alertDialog.setTitle("Address");
		alertDialog.setMessage("Edit Address");

		final EditText input = new EditText(Construction_ebm.this);
		input.setText(add);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.MATCH_PARENT);
		input.setLayoutParams(lp);
		alertDialog.setView(input);
		alertDialog.setIcon(R.drawable.map_icon);

		alertDialog.setPositiveButton("Search",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						String address = input.getText().toString();
						Uri gmmIntentUri = Uri.parse("geo:0,0?q="+address);
						Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
						mapIntent.setPackage("com.google.android.apps.maps");
						startActivity(mapIntent);

					}
				});

		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		alertDialog.show();
	}
	private boolean validate(EditText[] fields){
		for(int i=0; i<fields.length; i++){
			EditText currentField=fields[i];
			if(currentField.getText().toString().length()<=0){
				return false;
			}
		}
		return true;
	}
	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}


}
