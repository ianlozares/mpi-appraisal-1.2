package com.gds.appraisalmaybank.construction_ebm;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database.Construction_Ebm_API;
import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database2.Construction_Ebm_API_Room_List;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

public class Construction_ebm_room_list  extends ListActivity implements OnClickListener {
	DatabaseHandler2 db = new DatabaseHandler2(this);
	DatabaseHandler dbMain = new DatabaseHandler(this);
	GDS_methods gds = new GDS_methods();
	EditText report_valuation_total_area, report_valuation_total_proj_cost;
	Button btn_add_room, btn_save_room;
	Button btn_create, btn_cancel, btn_save;

	Session_Timer st = new Session_Timer(this);
	String record_id="";
	private static final String TAG_RECORD_ID = "record_id";
	
	Dialog myDialog;
	//create dialog
	EditText  report_room_list_area, report_room_list_description,	report_room_list_est_proj_cost, report_room_list_floor, report_room_list_rcn;

	//forListViewDeclarations
	private ProgressDialog pDialog;
	ArrayList<HashMap<String, String>> itemList;
	private static final String tag_cerl_id = "cerl_id";
	private static final String tag_floor = "floor";
	private static final String tag_area = "area";
	private static final String tag_est_proj_cost = "est_proj_cost";
	String cerl_id="", floor="", area="", est_proj_cost="";
	DecimalFormat df = new DecimalFormat("#.00");
	Double total_area_d=0.00, total_est_proj_cost_d=0.00;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.report_construction_ebm_rl);

		st.resetDisconnectTimer();
		report_valuation_total_area = (EditText) findViewById(R.id.report_valuation_total_area);
		report_valuation_total_proj_cost = (EditText) findViewById(R.id.report_valuation_total_proj_cost);
		report_valuation_total_area.setEnabled(false);
		report_valuation_total_proj_cost.setEnabled(false);
		
		btn_add_room = (Button) findViewById(R.id.btn_add_room);
		btn_add_room.setOnClickListener(this);
		
		btn_save_room = (Button) findViewById(R.id.btn_save_room);
		btn_save_room.setOnClickListener(this);
		
		// getting record_id from intent / previous class
		Intent i = getIntent();
		record_id = i.getStringExtra(TAG_RECORD_ID);
		//list
		itemList = new ArrayList<HashMap<String, String>>();
		// Loading in Background Thread
		new LoadAll().execute();
		fill_total();
		// Get listview
		ListView lv = getListView();

		// on seleting single item
		// launching Edit item Screen
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// getting values from selected ListItem
				cerl_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
				view_details();
			}
		});
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
                // TODO Auto-generated method stub

                Log.v("long clicked","pos: " + position);
                cerl_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
                delete_item();
                return true;
            }
        }); 
		
	}
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.btn_add_room:
			add_new();
			break;
		case R.id.btn_save_room:
			save_total();
			finish();
			break;
		default:
			break;
		}
	}
	
	public void fill_total(){
		List<Construction_Ebm_API> ce = dbMain.getConstruction_Ebm(record_id);
		if (!ce.isEmpty()) {
			for (Construction_Ebm_API im : ce) {
				report_valuation_total_area.setText(gds.numberFormat(im.getreport_valuation_total_area()));
				report_valuation_total_proj_cost.setText(gds.numberFormat(im.getreport_valuation_total_proj_cost()));
			}
		}
	}
	
	public void save_total(){
		
		List<Construction_Ebm_API_Room_List> cerl = db.getConstruction_Ebm_Room_List(record_id);
		if (!cerl.isEmpty()) {
			for (Construction_Ebm_API_Room_List imrl : cerl) {
				area = imrl.getreport_room_list_area();
				est_proj_cost = imrl.getreport_room_list_est_proj_cost();
				
				total_area_d = gds.StringtoDouble(area) + total_area_d;
				total_est_proj_cost_d = gds.StringtoDouble(est_proj_cost) + total_est_proj_cost_d;
			}
			
		} else {
			area = "0.00";
			est_proj_cost = "0.00";
		}
		
		Construction_Ebm_API ce = new Construction_Ebm_API();
		ce.setreport_valuation_total_area(gds.replaceFormat(String.valueOf(df.format(total_area_d))));
		ce.setreport_valuation_total_proj_cost(gds.replaceFormat(String.valueOf(df.format(total_est_proj_cost_d))));
		dbMain.updateConstruction_Ebm_Total(ce, record_id);
		dbMain.close();
	}
	
	public void add_new(){
		Toast.makeText(getApplicationContext(), "Add New",
				Toast.LENGTH_SHORT).show();
		myDialog = new Dialog(Construction_ebm_room_list.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.report_construction_ebm_rl_form);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		
		btn_create = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_create.setText("Create");
		btn_cancel.setText("Cancel");
		report_room_list_area = (EditText) myDialog.findViewById(R.id.report_room_list_area);
		report_room_list_description = (EditText) myDialog.findViewById(R.id.report_room_list_description);
		report_room_list_est_proj_cost = (EditText) myDialog.findViewById(R.id.report_room_list_est_proj_cost);
		report_room_list_floor = (EditText) myDialog.findViewById(R.id.report_room_list_floor);
		report_room_list_rcn = (EditText) myDialog.findViewById(R.id.report_room_list_rcn);
		report_room_list_est_proj_cost.setEnabled(false);
		
		report_room_list_area.addTextChangedListener(new TextWatcher() {
			String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
				current = gds.numberFormat(report_room_list_area, this, s, current);

				autoCompute();
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub\

            }
        });
		report_room_list_rcn.addTextChangedListener(new TextWatcher() {
			String current="";

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_room_list_rcn, this, s, current);
				autoCompute();
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub\

			}
		});
		report_room_list_est_proj_cost.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_room_list_est_proj_cost, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});

		btn_create.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				gds.fill_in_error(new EditText[]{report_room_list_area, report_room_list_description, report_room_list_floor, report_room_list_rcn});
				//list of EditTexts put in array

					//add data in SQLite
					db.addConstruction_Ebm_Room_List(new Construction_Ebm_API_Room_List(
							record_id,
							gds.replaceFormat(report_room_list_area.getText().toString()),
							report_room_list_description.getText().toString(),
							gds.replaceFormat(report_room_list_est_proj_cost.getText().toString()),
							report_room_list_floor.getText().toString(),
							gds.replaceFormat(report_room_list_rcn.getText().toString())));

						Toast.makeText(getApplicationContext(),"data created", Toast.LENGTH_SHORT).show();
						myDialog.dismiss();
						reloadClass();

				
			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		myDialog.show();
	}
	
	public void view_details(){
		myDialog = new Dialog(Construction_ebm_room_list.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.report_construction_ebm_rl_form);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		
		btn_save = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_save.setText("Save");
		btn_cancel.setText("Cancel");
		
		report_room_list_area = (EditText) myDialog.findViewById(R.id.report_room_list_area);
		report_room_list_description = (EditText) myDialog.findViewById(R.id.report_room_list_description);
		report_room_list_est_proj_cost = (EditText) myDialog.findViewById(R.id.report_room_list_est_proj_cost);
		report_room_list_floor = (EditText) myDialog.findViewById(R.id.report_room_list_floor);
		report_room_list_rcn = (EditText) myDialog.findViewById(R.id.report_room_list_rcn);
		report_room_list_est_proj_cost.setEnabled(false);
		
		List<Construction_Ebm_API_Room_List> cerlList = db
				.getConstruction_Ebm_Room_List_Single(String.valueOf(record_id), String.valueOf(cerl_id));
		if (!cerlList.isEmpty()) {
			for (Construction_Ebm_API_Room_List li : cerlList) {
				report_room_list_area.setText(gds.numberFormat(li.getreport_room_list_area()));
				report_room_list_description.setText(li.getreport_room_list_description());
				report_room_list_est_proj_cost.setText(gds.numberFormat(li.getreport_room_list_est_proj_cost()));
				report_room_list_floor.setText(li.getreport_room_list_floor());
				report_room_list_rcn.setText(gds.numberFormat(li.getreport_room_list_rcn()));
			}
		}

		report_room_list_area.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_room_list_area, this, s, current);

				autoCompute();
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub\

			}
		});
		report_room_list_rcn.addTextChangedListener(new TextWatcher() {
			String current="";

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_room_list_rcn, this, s, current);
				autoCompute();
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub\

			}
		});
		report_room_list_est_proj_cost.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_room_list_est_proj_cost, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});

		btn_save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//list of EditTexts put in array

					//update data in SQLite
					Construction_Ebm_API_Room_List ce = new Construction_Ebm_API_Room_List();
					ce.setreport_room_list_area(gds.replaceFormat(report_room_list_area.getText().toString()));
					ce.setreport_room_list_description(report_room_list_description.getText().toString());
					ce.setreport_room_list_est_proj_cost(gds.replaceFormat(report_room_list_est_proj_cost.getText().toString()));
					ce.setreport_room_list_floor(report_room_list_floor.getText().toString());
					ce.setreport_room_list_rcn(gds.replaceFormat(report_room_list_rcn.getText().toString()));
					db.updateConstruction_Ebm_Room_List(ce, record_id, cerl_id);
					db.close();
					myDialog.dismiss();
					Toast.makeText(getApplicationContext(), "Saved",Toast.LENGTH_SHORT).show();
					reloadClass();

			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});

		gds.fill_in_error(new EditText[]{report_room_list_area, report_room_list_description, report_room_list_floor, report_room_list_rcn});

		myDialog.show();
	}
	
	public void autoCompute(){
		if ((report_room_list_area.getText().length()!=0)&&(report_room_list_rcn.getText().length()!=0)){
			report_room_list_est_proj_cost.setText(
					gds.round(gds.multiplicationDecimal(report_room_list_area.getText().toString(), report_room_list_rcn.getText().toString())));
		} else if ((report_room_list_area.getText().length()==0)||(report_room_list_rcn.getText().length()==0)){
			report_room_list_est_proj_cost.setText("");
		}
		
	}
	
	//EditText checker
	private boolean validate(EditText[] fields){
        for(int i=0; i<fields.length; i++){
            EditText currentField=fields[i];
            if(currentField.getText().toString().length()<=0){
                return false;
            }
        }
        return true;
	}
	public void reloadClass(){
		save_total();
		Intent intent = getIntent();
		finish();
		startActivity(intent);
	}
	public void delete_item(){
		myDialog = new Dialog(Construction_ebm_room_list.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.delete_dialog);
		myDialog.setCancelable(true);
		final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
		final Button btn_yes = (Button) myDialog.findViewById(R.id.btn_yes);
		final Button btn_no = (Button) myDialog.findViewById(R.id.btn_no);
		tv_question.setText("Are you sure you want to delete the selected item?");
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				db.deleteConstruction_Ebm_Room_List(record_id, cerl_id);
				db.close();
				myDialog.dismiss();
				reloadClass();
				Toast.makeText(getApplicationContext(), "Data Deleted",Toast.LENGTH_SHORT).show();
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		
		myDialog.show();
	}
	
	
	/**
	 * Background Async Task to Load all
	 * */
	class LoadAll extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Construction_ebm_room_list.this);
			pDialog.setMessage("Loading Lists. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All
		 * */
		protected String doInBackground(String... args) {

			List<Construction_Ebm_API_Room_List> lirdps = db.getConstruction_Ebm_Room_List(record_id);
			if (!lirdps.isEmpty()) {
				for (Construction_Ebm_API_Room_List imrdps : lirdps) {
					cerl_id = String.valueOf(imrdps.getID());
					record_id = imrdps.getrecord_id();					
					floor = imrdps.getreport_room_list_floor();
					area = gds.numberFormat(imrdps.getreport_room_list_area());
					est_proj_cost = gds.numberFormat(imrdps.getreport_room_list_est_proj_cost());
					
					HashMap<String, String> map = new HashMap<String, String>();
					map.put(TAG_RECORD_ID, record_id);
					map.put(tag_cerl_id, cerl_id);
					map.put(tag_floor, floor);
					map.put(tag_area, area);
					map.put(tag_est_proj_cost, est_proj_cost);

					// adding HashList to ArrayList
					itemList.add(map);
				}
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					ListAdapter adapter = new SimpleAdapter(
							Construction_ebm_room_list.this, itemList,
							R.layout.report_construction_ebm_rl_list, new String[] { TAG_RECORD_ID, tag_cerl_id, tag_floor,
									tag_area, tag_est_proj_cost},
									new int[] { R.id.report_record_id, R.id.primary_key, R.id.item_floor, R.id.item_area, R.id.item_est_proj_cost});
					// updating listview
					setListAdapter(adapter);
				}
			});
			if(itemList.isEmpty()){
				add_new();
			}
		}
	}

	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}


}