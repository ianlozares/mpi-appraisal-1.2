package com.gds.appraisalmaybank.vacant_lot;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Vacant_Lot_API;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database2.Vacant_Lot_API_Lot_Details;
import com.gds.appraisalmaybank.database2.Vacant_Lot_API_Lot_Valuation_Details;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class VL_Lot_Details extends ListActivity implements OnClickListener {
    DatabaseHandler2 db = new DatabaseHandler2(this);
    DatabaseHandler dbMain = new DatabaseHandler(this);
    GDS_methods gds = new GDS_methods();
    Button btn_add_prop_desc, btn_save_prop_desc;
    Button btn_create, btn_cancel, btn_save;

    Session_Timer st = new Session_Timer(this);
    String record_id = "";
    private static final String TAG_RECORD_ID = "record_id";

    Dialog myDialog;
    //create dialog
    EditText valrep_land_propdesc_registry_date;
    EditText report_tct_no, report_lot, report_block, report_survey_no, report_area, report_reg_owner;
    EditText report_registry_of_deeds;

    //forListViewDeclarations
    private ProgressDialog pDialog;
    ArrayList<HashMap<String, String>> itemList;
    private static final String tag_lot_details_id = "lot_details_id";
    private static final String tag_tct_no = "tct_no";
    private static final String tag_reg_owner = "reg_owner";
    private static final String tag_date_reg = "date_reg";
    String lot_details_id = "";
    String unaccept;
    String where = "WHERE record_id = args";
    String[] args = new String[1];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reports_land_prop_desc);

        st.resetDisconnectTimer();

        btn_add_prop_desc = (Button) findViewById(R.id.btn_add_prop_desc);
        btn_add_prop_desc.setOnClickListener(this);

        btn_save_prop_desc = (Button) findViewById(R.id.btn_save_prop_desc);
        btn_save_prop_desc.setOnClickListener(this);

        // getting record_id from intent / previous class
        Intent i = getIntent();
        record_id = i.getStringExtra(TAG_RECORD_ID);
        //list
        itemList = new ArrayList<HashMap<String, String>>();
        // Loading in Background Thread
        new LoadAll().execute();
        // Get listview
        ListView lv = getListView();

        // on seleting single item
        // launching Edit item Screen
        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // getting values from selected ListItem
                lot_details_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
                        .toString();
                view_details();
            }
        });
        lv.setOnItemLongClickListener(new OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {
                // TODO Auto-generated method stub

                Log.v("long clicked", "pos: " + position);
                lot_details_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
                        .toString();
                delete_item();
                return true;
            }
        });
        args[0]=record_id;
        unaccept=gds.nullCheck3(db.getRecord("app_unacceptable_collateral",where,args,"tbl_report_accepted_jobs").get(0));
    }

    @Override
    public void onClick(View arg0) {
        // TODO Auto-generated method stub
        switch (arg0.getId()) {
            case R.id.btn_add_prop_desc:
                add_new();
                break;
            case R.id.btn_save_prop_desc:
                finish();
                break;
            default:
                break;
        }
    }

    public void add_new() {
        Toast.makeText(getApplicationContext(), "Add New",
                Toast.LENGTH_SHORT).show();
        myDialog = new Dialog(VL_Lot_Details.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.reports_land_prop_desc_form);
        myDialog.setCancelable(true);

        android.view.WindowManager.LayoutParams params = myDialog.getWindow()
                .getAttributes();
        params.height = LayoutParams.FILL_PARENT;
        params.width = LayoutParams.FILL_PARENT;
        myDialog.getWindow().setAttributes(
                (android.view.WindowManager.LayoutParams) params);

        btn_create = (Button) myDialog.findViewById(R.id.btn_right);
        btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
        btn_create.setText("Create");
        btn_cancel.setText("Cancel");
        report_tct_no = (EditText) myDialog.findViewById(R.id.report_tct_no);
        report_area = (EditText) myDialog.findViewById(R.id.report_area);
        report_lot = (EditText) myDialog.findViewById(R.id.report_lot);
        report_block = (EditText) myDialog.findViewById(R.id.report_block);
        report_survey_no = (EditText) myDialog.findViewById(R.id.report_survey_no);
        report_reg_owner = (EditText) myDialog.findViewById(R.id.report_reg_owner);
        report_registry_of_deeds = (EditText) myDialog.findViewById(R.id.report_registry_of_deeds);

        valrep_land_propdesc_registry_date = (EditText) myDialog.findViewById(R.id.valrep_landimp_propdesc_registry_date);

        report_area.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(report_area, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });


        btn_create.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {




                    //adjust for month

                    //add data in SQLite
                    db.addVacant_Lot_Lot_Details(new Vacant_Lot_API_Lot_Details(
                            record_id,
                            report_tct_no.getText().toString(),
                            report_lot.getText().toString(),
                            report_block.getText().toString(),
                            report_survey_no.getText().toString(),
                            gds.replaceFormat(report_area.getText().toString()),
                            valrep_land_propdesc_registry_date.getText().toString(),
                           report_reg_owner.getText().toString(),
                            report_registry_of_deeds.getText().toString()));
                    //sync with valuation (if 3 records added, then 3 records should also be created to valuation
                    db.addVacant_Lot_Lot_Valuation_Details(new Vacant_Lot_API_Lot_Valuation_Details(
                            record_id,
                            report_tct_no.getText().toString(),
                            report_lot.getText().toString(),
                            report_block.getText().toString(),
                            gds.replaceFormat(report_area.getText().toString()),
                            "0",//report_deduc.getText().toString(),
                            "0",//report_net_area.getText().toString(),
                            "0",//report_unit_value.getText().toString(),
                            "0"));//report_total_land_val.getText().toString()));
                    Toast.makeText(getApplicationContext(),
                            "data created", Toast.LENGTH_SHORT).show();
                    myDialog.dismiss();
                    reloadClass();


            }
        });
        btn_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        gds.fill_in_error(new EditText[]{report_tct_no, report_area, report_lot,report_block,
                report_survey_no,valrep_land_propdesc_registry_date,report_reg_owner,report_registry_of_deeds});


        myDialog.show();
    }

    public void view_details() {
        myDialog = new Dialog(VL_Lot_Details.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.reports_land_prop_desc_form);
        myDialog.setCancelable(true);

        android.view.WindowManager.LayoutParams params = myDialog.getWindow()
                .getAttributes();
        params.height = LayoutParams.FILL_PARENT;
        params.width = LayoutParams.FILL_PARENT;
        myDialog.getWindow().setAttributes(
                (android.view.WindowManager.LayoutParams) params);

        btn_save = (Button) myDialog.findViewById(R.id.btn_right);
        btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
        btn_save.setText("Save");
        btn_cancel.setText("Cancel");

        report_tct_no = (EditText) myDialog.findViewById(R.id.report_tct_no);
        report_area = (EditText) myDialog.findViewById(R.id.report_area);
        report_lot = (EditText) myDialog.findViewById(R.id.report_lot);
        report_block = (EditText) myDialog.findViewById(R.id.report_block);
        report_survey_no = (EditText) myDialog.findViewById(R.id.report_survey_no);
        report_reg_owner = (EditText) myDialog.findViewById(R.id.report_reg_owner);
        report_registry_of_deeds = (EditText) myDialog.findViewById(R.id.report_registry_of_deeds);
        valrep_land_propdesc_registry_date = (EditText) myDialog.findViewById(R.id.valrep_landimp_propdesc_registry_date);

        report_area.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(report_area, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });


        List<Vacant_Lot_API_Lot_Details> propDescList = db
                .getVacant_Lot_Lot_Details_with_lot_details_id(String.valueOf(record_id), String.valueOf(lot_details_id));
        if (!propDescList.isEmpty()) {
            for (Vacant_Lot_API_Lot_Details vl : propDescList) {
                report_tct_no.setText(vl.getreport_propdesc_tct_no());
                report_area.setText(gds.numberFormat(vl.getreport_propdesc_area()));
                report_lot.setText(vl.getreport_propdesc_lot());
                report_block.setText(vl.getreport_propdesc_block());
                report_survey_no.setText(vl.getreport_propdesc_survey_nos());
                report_reg_owner.setText(vl.getreport_propdesc_registered_owner());
                valrep_land_propdesc_registry_date.setText(vl.getvalrep_land_propdesc_registry_date());
                report_registry_of_deeds.setText(vl.getreport_propdesc_deeds());
            }
        }

        btn_save.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                //check if all EditTexts in array are filled up
                    //update data in SQLite
                    Vacant_Lot_API_Lot_Details vl = new Vacant_Lot_API_Lot_Details();
                    vl.setreport_propdesc_tct_no(report_tct_no.getText().toString());
                    vl.setreport_propdesc_lot(report_lot.getText().toString());
                    vl.setreport_propdesc_block(report_block.getText().toString());
                    vl.setreport_propdesc_survey_nos(report_survey_no.getText().toString());
                    vl.setreport_propdesc_area( gds.replaceFormat(report_area.getText().toString()));
                    vl.setvalrep_land_propdesc_registry_date(valrep_land_propdesc_registry_date.getText().toString());

                    vl.setreport_propdesc_registered_owner(report_reg_owner.getText().toString());
                    vl.setreport_propdesc_deeds(report_registry_of_deeds.getText().toString());
                    db.updateVacant_Lot_Lot_Details(vl, record_id, lot_details_id);

                    //ADDED By IAN
                    Vacant_Lot_API_Lot_Valuation_Details vl2 = new Vacant_Lot_API_Lot_Valuation_Details();
                    vl2.setreport_value_tct_no(report_tct_no.getText().toString());
                    vl2.setreport_value_lot_no(report_lot.getText().toString());
                    vl2.setreport_value_block_no(report_block.getText().toString());
                    vl2.setreport_value_area( gds.replaceFormat(report_area.getText().toString()));

                    String deduc_s = "0";
                    String unit_value_s = "0";
                    List<Vacant_Lot_API_Lot_Valuation_Details> propDescList = db
                            .getVacant_Lot_Lot_Valuation_Details_with_lot_valuation_details_id(String.valueOf(record_id), String.valueOf(lot_details_id));
                    if (!propDescList.isEmpty()) {
                        for (Vacant_Lot_API_Lot_Valuation_Details vl3 : propDescList) {
                            deduc_s = gds.nullCheck2(vl3.getreport_value_deduction());
                            unit_value_s = gds.nullCheck2(vl3.getreport_value_unit_value());
                        }
                    }

                    //update valuation
                    String net_area_val = gds.netAreaVL(gds.nullCheck2(report_area.getText().toString()), deduc_s);
                    vl2.setreport_value_net_area(net_area_val);
                    String total_land_val_val = gds.totalLandVL(net_area_val, unit_value_s);
                    vl2.setreport_value_land_value(total_land_val_val);
                    db.updateVacant_Lot_Lot_Valuation_Details2(vl2, record_id, lot_details_id);
                    //total land value
                    String grand_total_land_val = gds.nullCheck(db.getVacant_Lot_API_Total_Land_Value_Lot_Valuation_Details(record_id));
                    String grand_total_land_val_s = gds.stringToDecimal(grand_total_land_val);


                    //update Summary
                    Vacant_Lot_API vl4 = new Vacant_Lot_API();
                    vl4.setreport_final_value_total_appraised_value_land_imp(grand_total_land_val_s);
                    dbMain.updateVacant_Lot_Summary_Total(vl4, record_id);
                    dbMain.close();

                    //END of ADDED by IAN
                    db.close();
                    myDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
                    reloadClass();

            }
        });
        btn_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });


        gds.fill_in_error(new EditText[]{report_tct_no, report_area, report_lot,report_block,
                report_survey_no,valrep_land_propdesc_registry_date,report_reg_owner,report_registry_of_deeds});


        myDialog.show();
    }


    public void reloadClass() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    public void delete_item() {
        myDialog = new Dialog(VL_Lot_Details.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.delete_dialog);
        myDialog.setCancelable(true);
        final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
        final Button btn_yes = (Button) myDialog.findViewById(R.id.btn_yes);
        final Button btn_no = (Button) myDialog.findViewById(R.id.btn_no);
        tv_question.setText("Are you sure you want to delete the selected item?");
        btn_yes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                db.deleteVacant_Lot_Lot_Details_Single(record_id, lot_details_id);
                db.deleteVacant_Lot_Lot_Valuation_Details_Single(record_id, lot_details_id);
                db.close();

                //ADDED BY IAN
                //total land value
                String grand_total_land_val = gds.nullCheck(db.getVacant_Lot_API_Total_Land_Value_Lot_Valuation_Details(record_id));
                String grand_total_land_val_s = gds.stringToDecimal(grand_total_land_val);

                //update Summary
                Vacant_Lot_API vl4 = new Vacant_Lot_API();
                vl4.setreport_final_value_total_appraised_value_land_imp(grand_total_land_val_s);
                dbMain.updateVacant_Lot_Summary_Total(vl4, record_id);
                dbMain.close();

                //END of ADDED by IAN


                myDialog.dismiss();
                reloadClass();
                Toast.makeText(getApplicationContext(), "Data Deleted", Toast.LENGTH_SHORT).show();
            }
        });
        btn_no.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        myDialog.show();
    }


    /**
     * Background Async Task to Load all
     */
    class LoadAll extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(VL_Lot_Details.this);
            pDialog.setMessage("Loading Lists. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting All
         */
        protected String doInBackground(String... args) {
            String tct_no = "", reg_owner = "", reg_date = "";
            int reg_month_int;
            String reg_day = "", reg_year = "";


            List<Vacant_Lot_API_Lot_Details> vlld = db.getVacant_Lot_Lot_Details(record_id);
            if (!vlld.isEmpty()) {
                for (Vacant_Lot_API_Lot_Details imld : vlld) {
                    lot_details_id = String.valueOf(imld.getID());
                    tct_no = imld.getreport_propdesc_tct_no();
                    imld.getreport_propdesc_lot();
                    imld.getreport_propdesc_block();
                    imld.getreport_propdesc_survey_nos();
                    imld.getreport_propdesc_area();
                    imld.getvalrep_land_propdesc_registry_date();
                    reg_owner = imld.getreport_propdesc_registered_owner();
                    reg_date =  imld.getvalrep_land_propdesc_registry_date();
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(tag_lot_details_id, lot_details_id);
                    map.put(tag_tct_no, tct_no);
                    map.put(tag_date_reg, reg_date);
                    map.put(tag_reg_owner, reg_owner);

                    // adding HashList to ArrayList
                    itemList.add(map);
                }
            }
            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all
            pDialog.dismiss();
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    ListAdapter adapter = new SimpleAdapter(
                            VL_Lot_Details.this, itemList,
                            R.layout.reports_land_prop_desc_list, new String[]{tag_lot_details_id, tag_tct_no,
                            tag_reg_owner, tag_date_reg},
                            new int[]{R.id.primary_key, R.id.item_tct_no, R.id.item_reg_owner, R.id.item_date_reg});
                    // updating listview
                    setListAdapter(adapter);
                }
            });

            if(itemList.isEmpty()){
                add_new();
            }
        }
    }


    @Override
    public void onUserInteraction(){
        st.resetDisconnectTimer();
    }
    @Override
    public void onStop() {
        super.onStop();
        st.stopDisconnectTimer();
    }
    @Override
    public void onResume() {
        super.onResume();
        st.resetDisconnectTimer();
    }



}