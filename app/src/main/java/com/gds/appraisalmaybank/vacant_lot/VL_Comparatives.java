package com.gds.appraisalmaybank.vacant_lot;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Vacant_Lot_API;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database2.Vacant_Lot_API_Lot_Valuation_Details;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class VL_Comparatives extends Activity implements OnClickListener {
	DatabaseHandler db = new DatabaseHandler(this);
	DatabaseHandler2 db2 = new DatabaseHandler2(this);
	GDS_methods gds = new GDS_methods();
	Session_Timer st = new Session_Timer(this);

	String record_id="";
	private static final String TAG_RECORD_ID = "record_id";
	
	Button btn_comp1, btn_comp2, btn_comp3, btn_comp4, btn_comp5, btn_save, btn_compute;
	EditText report_average, report_round_to;
	
	//myDialog Objects
	String dialog_title="";
	Dialog myDialog;
	DatePicker report_date;
	EditText report_source, report_contact_no, report_location, report_area,
			report_base_price, report_price_sqm, report_discount_rate,
			report_discounts, report_selling_price, report_rec_location,
			report_rec_size,report_rec_amenities,report_rec_accessibility,report_rec_terrain,report_rec_shape, report_rec_corner_influence, report_rec_tru_lot,
			report_rec_elevation, report_rec_time_element,report_rec_others, report_rec_degree_of_devt,
			report_concluded_adjustment, report_adjusted_value, report_remarks,valrep_land_comp_total_rectification;
	
	//computation variables
	DecimalFormat df = new DecimalFormat("#.00");
	DecimalFormat df2 = new DecimalFormat("###");
	Double adjusted_value_d;
	Double adj_val_1=0.00, adj_val_2=0.00, adj_val_3=0.00, adj_val_4=0.00, adj_val_5=0.00;
	Double average_d, round_to_d;
	String average_s,unaccept;
	String where = "WHERE record_id = args";
	String[] args = new String[1];
	ArrayList<String> field = new ArrayList<String>();
	ArrayList<String> value = new ArrayList<String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.comparatives);

		st.resetDisconnectTimer();
		btn_comp1 = (Button) findViewById(R.id.btn_comp1);
		btn_comp1.setOnClickListener(this);
		btn_comp2 = (Button) findViewById(R.id.btn_comp2);
		btn_comp2.setOnClickListener(this);
		btn_comp3 = (Button) findViewById(R.id.btn_comp3);
		btn_comp3.setOnClickListener(this);
		btn_comp4 = (Button) findViewById(R.id.btn_comp4);
		btn_comp4.setOnClickListener(this);
		btn_comp5 = (Button) findViewById(R.id.btn_comp5);
		btn_comp5.setOnClickListener(this);
		
		btn_compute = (Button) findViewById(R.id.btn_compute);
		btn_compute.setOnClickListener(this);
		btn_save = (Button) findViewById(R.id.btn_save);
		btn_save.setOnClickListener(this);
		
		report_average = (EditText) findViewById(R.id.report_average);
		report_round_to = (EditText) findViewById(R.id.report_round_to);
		
		// getting record_id from intent / previous class
		Intent i = getIntent();
		record_id = i.getStringExtra(TAG_RECORD_ID);
		args[0]=record_id;
		unaccept=gds.nullCheck3(db2.getRecord("app_unacceptable_collateral",where,args,"tbl_report_accepted_jobs").get(0));
		if(unaccept.contentEquals("true")){
			btn_comp1.setEnabled(false);
			btn_comp2.setEnabled(false);
			btn_comp3.setEnabled(false);
			btn_comp4.setEnabled(false);
			btn_comp5.setEnabled(false);
			btn_comp1.setBackgroundColor(Color.GRAY);
			btn_comp2.setBackgroundColor(Color.GRAY);
			btn_comp3.setBackgroundColor(Color.GRAY);
			btn_comp4.setBackgroundColor(Color.GRAY);
			btn_comp5.setBackgroundColor(Color.GRAY);
		}else{
			btn_comp1.setEnabled(true);
			btn_comp2.setEnabled(true);
			btn_comp3.setEnabled(true);
			btn_comp4.setEnabled(true);
			btn_comp5.setEnabled(true);
		}
		fill_data();

	}
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		save_comparatives();
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.btn_comp1:
			dialog_title="Data Comparative 1";
			view_comp1();
			break;
		case R.id.btn_comp2:
			dialog_title="Data Comparative 2";
			view_comp2();
			break;
		case R.id.btn_comp3:
			dialog_title="Data Comparative 3";
			view_comp3();
			break;
		case R.id.btn_comp4:
			dialog_title="Data Comparative 4";
			view_comp4();
			break;
		case R.id.btn_comp5:
			dialog_title="Data Comparative 5";
			view_comp5();
			break;
		case R.id.btn_compute:
			compute_average();
			break;
		case R.id.btn_save:
			save_comparatives();
			finish();
			break;
		default:
			break;
		}
	}
	
	public void fill_data(){
		List<Vacant_Lot_API> vl = db.getVacant_Lot(String.valueOf(record_id));
		if (!vl.isEmpty()) {
			for (Vacant_Lot_API im : vl) {
				report_average.setText(gds.numberFormat(im.getreport_comp_average()));
				report_round_to.setText(gds.numberFormat(im.getreport_comp_rounded_to()));
			}
		}
	}
	
	public void view_comp1(){
		myDialog_config();
		List<Vacant_Lot_API> vl = db.getVacant_Lot(String.valueOf(record_id));
		if (!vl.isEmpty()) {
			for (Vacant_Lot_API im : vl) {
				//date of inspection
				// set current date into datepicker
				if ((!im.getreport_comp1_date_year().equals(""))||(!im.getreport_comp1_date_month().equals(""))) {
					report_date.init(
							Integer.parseInt(im.getreport_comp1_date_year()),
							Integer.parseInt(im.getreport_comp1_date_month())-1,
							Integer.parseInt(im.getreport_comp1_date_day()),
							null);
				}
				report_source.setText(im.getreport_comp1_source());
				report_contact_no.setText(im.getreport_comp1_contact_no());
				report_location.setText(im.getreport_comp1_location());
				report_area.setText(gds.numberFormat(im.getreport_comp1_area()));
				report_base_price.setText(gds.numberFormat(im.getreport_comp1_base_price()));
				report_price_sqm.setText(gds.numberFormat(im.getreport_comp1_price_sqm()));
				report_discount_rate.setText(im.getreport_comp1_discount_rate());
				report_discounts.setText(gds.numberFormat(im.getreport_comp1_discounts()));
				report_selling_price.setText(gds.numberFormat(im.getreport_comp1_selling_price()));
				report_rec_location.setText(im.getreport_comp1_rec_location());
				report_rec_size.setText(im.getreport_comp1_rec_size());
				report_rec_corner_influence.setText(im.getreport_comp1_rec_corner_influence());
				report_rec_amenities.setText(im.getreport_comp1_rec_amenities());
				report_rec_terrain.setText(im.getreport_comp1_rec_terrain());
				report_rec_shape.setText(im.getreport_comp1_rec_shape());
				report_rec_accessibility.setText("0");
				//report_rec_tru_lot.setText(im.getreport_comp1_rec_tru_lot());
				report_rec_elevation.setText(im.getreport_comp1_rec_elevation());
				//report_rec_time_element.setText(im.getreport_comp1_rec_time_element());
				report_rec_others.setText(im.getreport_comp1_rec_others());
				report_rec_degree_of_devt.setText(im.getreport_comp1_rec_degree_of_devt());
				report_concluded_adjustment.setText(gds.numberFormat(im.getreport_comp1_concluded_adjustment()));
				report_adjusted_value.setText(gds.numberFormat(im.getreport_comp1_adjusted_value()));
				report_remarks.setText(im.getreport_comp1_remarks());
				args[0]=record_id;
				valrep_land_comp_total_rectification.setText(db2.getRecord("valrep_land_comp1_total_rectification",where,args,"vacant_lot").get(0));
			}
		}


		report_area.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_area, this, s, current);
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_base_price.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_base_price, this, s, current);

				comp_textwatch();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_discount_rate.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_location.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_size.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_corner_influence.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_amenities.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();


			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_terrain.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_shape.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_elevation.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_others.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_degree_of_devt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		report_price_sqm.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_price_sqm, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_discounts.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_discounts, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_selling_price.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_selling_price, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_concluded_adjustment.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_concluded_adjustment, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_adjusted_value.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_adjusted_value, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});


		btn_save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//list of EditTexts put in array

				null_to_zero();

				gds.fill_in_error(new EditText[] { report_source,report_area,
						report_base_price });
				boolean fieldsOK = gds.validate(new EditText[] { report_source,report_area,
						report_base_price});
				if (fieldsOK==true){ //check if all EditTexts in array are filled up
					//update data in SQLite
					Vacant_Lot_API vl = new Vacant_Lot_API();
					//date
					if (String.valueOf(report_date.getMonth()).length() == 1) {//if month is 1 digit add 0 to initial +1 to value
						if (String.valueOf(report_date.getMonth())
								.equals("9")) {//if october==9 just add 1
							vl.setreport_comp1_date_month(String
									.valueOf(report_date.getMonth() + 1));
						} else {
							vl.setreport_comp1_date_month("0"
									+ String.valueOf(report_date
									.getMonth() + 1));
						}
					} else {
						vl.setreport_comp1_date_month(String
								.valueOf(report_date.getMonth() + 1));
					}
					vl.setreport_comp1_date_day(String.valueOf(report_date.getDayOfMonth()));
					vl.setreport_comp1_date_year(String.valueOf(report_date.getYear()));
					
					vl.setreport_comp1_source(report_source.getText().toString());
					vl.setreport_comp1_contact_no(report_contact_no.getText().toString());
					vl.setreport_comp1_location(report_location.getText().toString());
					vl.setreport_comp1_area(gds.replaceFormat(report_area.getText().toString()));
					vl.setreport_comp1_base_price(gds.replaceFormat(report_base_price.getText().toString()));
					vl.setreport_comp1_price_sqm(gds.replaceFormat(report_price_sqm.getText().toString()));
					vl.setreport_comp1_discount_rate(report_discount_rate.getText().toString());
					vl.setreport_comp1_discounts(gds.replaceFormat(report_discounts.getText().toString()));
					vl.setreport_comp1_selling_price(gds.replaceFormat(report_selling_price.getText().toString()));
					vl.setreport_comp1_rec_location(report_rec_location.getText().toString());
					vl.setreport_comp1_rec_size(report_rec_size.getText().toString());
					vl.setreport_comp1_rec_accessibility(report_rec_accessibility.getText().toString());
					vl.setreport_comp1_rec_amenities(report_rec_amenities.getText().toString());
					vl.setreport_comp1_rec_terrain(report_rec_terrain.getText().toString());
					vl.setreport_comp1_rec_shape(report_rec_shape.getText().toString());
					vl.setreport_comp1_rec_corner_influence(report_rec_corner_influence.getText().toString());
					vl.setreport_comp1_rec_tru_lot(report_rec_tru_lot.getText().toString());
					vl.setreport_comp1_rec_elevation(report_rec_elevation.getText().toString());
					vl.setreport_comp1_rec_time_element(report_rec_time_element.getText().toString());
					vl.setreport_comp1_rec_others(report_rec_others.getText().toString());
					vl.setreport_comp1_rec_degree_of_devt(report_rec_degree_of_devt.getText().toString());
					vl.setreport_comp1_concluded_adjustment(gds.replaceFormat(report_concluded_adjustment.getText().toString()));
					vl.setreport_comp1_adjusted_value(gds.replaceFormat(report_adjusted_value.getText().toString()));
					vl.setreport_comp1_remarks(report_remarks.getText().toString());
					
					db.updateVacant_Lot_comp1(vl, record_id);
					field.clear();
					field.add("valrep_land_comp1_total_rectification");
					value.clear();
					value.add(gds.nullCheck(valrep_land_comp_total_rectification.getText().toString()));
					db2.updateRecord(value,field,"record_id = ?", new String[]{record_id},"vacant_lot");
					db.close();
					myDialog.dismiss();

					compute_average();
					
				}else{
					Toast.makeText(getApplicationContext(),
						"Please fill up required fields", Toast.LENGTH_SHORT).show();
				}
			}
		});
		myDialog.show();
	}
	
	public void view_comp2(){
		myDialog_config();
		List<Vacant_Lot_API> vl = db.getVacant_Lot(String.valueOf(record_id));
		if (!vl.isEmpty()) {
			for (Vacant_Lot_API im : vl) {
				//date of inspection
				// set current date into datepicker
				if ((!im.getreport_comp2_date_year().equals(""))||(!im.getreport_comp2_date_month().equals(""))) {
					report_date.init(
							Integer.parseInt(im.getreport_comp2_date_year()),
							Integer.parseInt(im.getreport_comp2_date_month())-1,
							Integer.parseInt(im.getreport_comp2_date_day()),
							null);
				}
				report_source.setText(im.getreport_comp2_source());
				report_contact_no.setText(im.getreport_comp2_contact_no());
				report_location.setText(im.getreport_comp2_location());
				report_area.setText(gds.numberFormat(im.getreport_comp2_area()));
				report_base_price.setText(gds.numberFormat(im.getreport_comp2_base_price()));
				report_price_sqm.setText(gds.numberFormat(im.getreport_comp2_price_sqm()));
				report_discount_rate.setText(im.getreport_comp2_discount_rate());
				report_discounts.setText(gds.numberFormat(im.getreport_comp2_discounts()));
				report_selling_price.setText(gds.numberFormat(im.getreport_comp2_selling_price()));
				report_rec_location.setText(im.getreport_comp2_rec_location());
				report_rec_size.setText(im.getreport_comp2_rec_size());
				report_rec_accessibility.setText("0");
				report_rec_amenities.setText(im.getreport_comp2_rec_amenities());
				report_rec_terrain.setText(im.getreport_comp2_rec_terrain());
				report_rec_shape.setText(im.getreport_comp2_rec_shape());
				report_rec_corner_influence.setText(im.getreport_comp2_rec_corner_influence());
				//report_rec_tru_lot.setText(im.getreport_comp2_rec_tru_lot());
				report_rec_elevation.setText(im.getreport_comp2_rec_elevation());
				//report_rec_time_element.setText(im.getreport_comp2_rec_time_element());
				report_rec_others.setText(im.getreport_comp2_rec_others());
				report_rec_degree_of_devt.setText(im.getreport_comp2_rec_degree_of_devt());
				report_concluded_adjustment.setText(gds.numberFormat(im.getreport_comp2_concluded_adjustment()));
				report_adjusted_value.setText(gds.numberFormat(im.getreport_comp2_adjusted_value()));
				report_remarks.setText(im.getreport_comp2_remarks());
				args[0]=record_id;
				valrep_land_comp_total_rectification.setText(db2.getRecord("valrep_land_comp2_total_rectification",where,args,"vacant_lot").get(0));

			}
		}

		report_area.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_area, this, s, current);
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_base_price.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_base_price, this, s, current);

				comp_textwatch();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_discount_rate.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_location.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_size.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_corner_influence.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_amenities.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();


			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_terrain.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_shape.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_elevation.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_others.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_degree_of_devt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		report_price_sqm.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_price_sqm, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_discounts.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_discounts, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_selling_price.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_selling_price, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_concluded_adjustment.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_concluded_adjustment, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_adjusted_value.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_adjusted_value, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});


		btn_save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				null_to_zero();
				gds.fill_in_error(new EditText[] { report_source,report_area,
						report_base_price });
				boolean fieldsOK = gds.validate(new EditText[] { report_source,report_area,
						report_base_price});
				if (fieldsOK==true){ //check if all EditTexts in array are filled up
					//update data in SQLite
					Vacant_Lot_API vl = new Vacant_Lot_API();
					//date
					if (String.valueOf(report_date.getMonth()).length() == 1) {//if month is 2 digit add 0 to initial +2 to value
						if (String.valueOf(report_date.getMonth())
								.equals("9")) {//if october==9 just add 2
							vl.setreport_comp2_date_month(String
									.valueOf(report_date.getMonth() + 1));
						} else {
							vl.setreport_comp2_date_month("0"
									+ String.valueOf(report_date
									.getMonth() + 1));
						}
					} else {
						vl.setreport_comp2_date_month(String
								.valueOf(report_date.getMonth() + 1));
					}
					vl.setreport_comp2_date_day(String.valueOf(report_date.getDayOfMonth()));
					vl.setreport_comp2_date_year(String.valueOf(report_date.getYear()));
					
					vl.setreport_comp2_source(report_source.getText().toString());
					vl.setreport_comp2_contact_no(report_contact_no.getText().toString());
					vl.setreport_comp2_location(report_location.getText().toString());
					vl.setreport_comp2_area(gds.replaceFormat(report_area.getText().toString()));
					vl.setreport_comp2_base_price(gds.replaceFormat(report_base_price.getText().toString()));
					vl.setreport_comp2_price_sqm(gds.replaceFormat(report_price_sqm.getText().toString()));
					vl.setreport_comp2_discount_rate(report_discount_rate.getText().toString());
					vl.setreport_comp2_discounts(gds.replaceFormat(report_discounts.getText().toString()));
					vl.setreport_comp2_selling_price(gds.replaceFormat(report_selling_price.getText().toString()));
					vl.setreport_comp2_rec_location(report_rec_location.getText().toString());
					vl.setreport_comp2_rec_size(report_rec_size.getText().toString());
					vl.setreport_comp2_rec_accessibility(report_rec_accessibility.getText().toString());
					vl.setreport_comp2_rec_amenities(report_rec_amenities.getText().toString());
					vl.setreport_comp2_rec_terrain(report_rec_terrain.getText().toString());
					vl.setreport_comp2_rec_shape(report_rec_shape.getText().toString());
					vl.setreport_comp2_rec_corner_influence(report_rec_corner_influence.getText().toString());
					vl.setreport_comp2_rec_tru_lot(report_rec_tru_lot.getText().toString());
					vl.setreport_comp2_rec_elevation(report_rec_elevation.getText().toString());
					vl.setreport_comp2_rec_time_element(report_rec_time_element.getText().toString());
					vl.setreport_comp2_rec_others(report_rec_others.getText().toString());
					vl.setreport_comp2_rec_degree_of_devt(report_rec_degree_of_devt.getText().toString());
					vl.setreport_comp2_concluded_adjustment(gds.replaceFormat(report_concluded_adjustment.getText().toString()));
					vl.setreport_comp2_adjusted_value(gds.replaceFormat(report_adjusted_value.getText().toString()));
					vl.setreport_comp2_remarks(report_remarks.getText().toString());
					
					db.updateVacant_Lot_comp2(vl, record_id);
					field.clear();
					field.add("valrep_land_comp2_total_rectification");
					value.clear();
					value.add(gds.nullCheck(valrep_land_comp_total_rectification.getText().toString()));
					db2.updateRecord(value,field,"record_id = ?", new String[]{record_id},"vacant_lot");
					db.close();
					myDialog.dismiss();
					compute_average();
				}else{
					Toast.makeText(getApplicationContext(),
						"Please fill up required fields", Toast.LENGTH_SHORT).show();
				}
			}
		});
		myDialog.show();
	}

	public void view_comp3(){
		myDialog_config();
		List<Vacant_Lot_API> vl = db.getVacant_Lot(String.valueOf(record_id));
		if (!vl.isEmpty()) {
			for (Vacant_Lot_API im : vl) {
				//date of inspection
				// set current date into datepicker
				if ((!im.getreport_comp3_date_year().equals(""))||(!im.getreport_comp3_date_month().equals(""))) {
					report_date.init(
							Integer.parseInt(im.getreport_comp3_date_year()),
							Integer.parseInt(im.getreport_comp3_date_month())-1,
							Integer.parseInt(im.getreport_comp3_date_day()),
							null);
				}
				report_source.setText(im.getreport_comp3_source());
				report_contact_no.setText(im.getreport_comp3_contact_no());
				report_location.setText(im.getreport_comp3_location());
				report_area.setText(gds.numberFormat(im.getreport_comp3_area()));
				report_base_price.setText(gds.numberFormat(im.getreport_comp3_base_price()));
				report_price_sqm.setText(gds.numberFormat(im.getreport_comp3_price_sqm()));
				report_discount_rate.setText(im.getreport_comp3_discount_rate());
				report_discounts.setText(gds.numberFormat(im.getreport_comp3_discounts()));
				report_selling_price.setText(gds.numberFormat(im.getreport_comp3_selling_price()));
				report_rec_location.setText(im.getreport_comp3_rec_location());
				report_rec_size.setText(im.getreport_comp3_rec_size());
				report_rec_accessibility.setText(im.getreport_comp3_rec_accessibility());
				report_rec_amenities.setText(im.getreport_comp3_rec_amenities());
				report_rec_terrain.setText(im.getreport_comp3_rec_terrain());
				report_rec_shape.setText(im.getreport_comp3_rec_shape());
				report_rec_corner_influence.setText(im.getreport_comp3_rec_corner_influence());
				//report_rec_tru_lot.setText(im.getreport_comp3_rec_tru_lot());
				report_rec_elevation.setText(im.getreport_comp3_rec_elevation());
				//report_rec_time_element.setText(im.getreport_comp3_rec_time_element());
				report_rec_others.setText(im.getreport_comp3_rec_others());
				report_rec_degree_of_devt.setText(im.getreport_comp3_rec_degree_of_devt());
				report_concluded_adjustment.setText(gds.numberFormat(im.getreport_comp3_concluded_adjustment()));
				report_adjusted_value.setText(gds.numberFormat(im.getreport_comp3_adjusted_value()));
				report_remarks.setText(im.getreport_comp3_remarks());
				args[0]=record_id;
				valrep_land_comp_total_rectification.setText(db2.getRecord("valrep_land_comp3_total_rectification",where,args,"vacant_lot").get(0));

			}
		}

		report_area.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_area, this, s, current);
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_base_price.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_base_price, this, s, current);

				comp_textwatch();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_discount_rate.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_location.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_size.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_corner_influence.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_amenities.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();


			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_terrain.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_shape.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_elevation.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_others.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_degree_of_devt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		report_price_sqm.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_price_sqm, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_discounts.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_discounts, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_selling_price.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_selling_price, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_concluded_adjustment.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_concluded_adjustment, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_adjusted_value.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_adjusted_value, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});


		btn_save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//list of EditTexts put in array
				null_to_zero();
				gds.fill_in_error(new EditText[] { report_source,report_area,
						report_base_price });
				boolean fieldsOK = gds.validate(new EditText[] { report_source,report_area,
						report_base_price});
				if (fieldsOK==true){ //check if all EditTexts in array are filled up
					//update data in SQLite
					Vacant_Lot_API vl = new Vacant_Lot_API();
					//date
					if (String.valueOf(report_date.getMonth()).length() == 1) {//if month is 3 digit add 0 to initial +3 to value
						if (String.valueOf(report_date.getMonth())
								.equals("9")) {//if october==9 just add 3
							vl.setreport_comp3_date_month(String
									.valueOf(report_date.getMonth() + 1));
						} else {
							vl.setreport_comp3_date_month("0"
									+ String.valueOf(report_date
									.getMonth() + 1));
						}
					} else {
						vl.setreport_comp3_date_month(String
								.valueOf(report_date.getMonth() + 1));
					}
					vl.setreport_comp3_date_day(String.valueOf(report_date.getDayOfMonth()));
					vl.setreport_comp3_date_year(String.valueOf(report_date.getYear()));
					
					vl.setreport_comp3_source(report_source.getText().toString());
					vl.setreport_comp3_contact_no(report_contact_no.getText().toString());
					vl.setreport_comp3_location(report_location.getText().toString());
					vl.setreport_comp3_area(gds.replaceFormat(report_area.getText().toString()));
					vl.setreport_comp3_base_price(gds.replaceFormat(report_base_price.getText().toString()));
					vl.setreport_comp3_price_sqm(gds.replaceFormat(report_price_sqm.getText().toString()));
					vl.setreport_comp3_discount_rate(report_discount_rate.getText().toString());
					vl.setreport_comp3_discounts(gds.replaceFormat(report_discounts.getText().toString()));
					vl.setreport_comp3_selling_price(gds.replaceFormat(report_selling_price.getText().toString()));
					vl.setreport_comp3_rec_location(report_rec_location.getText().toString());
					vl.setreport_comp3_rec_size(report_rec_size.getText().toString());
					vl.setreport_comp3_rec_accessibility(report_rec_accessibility.getText().toString());
					vl.setreport_comp3_rec_amenities(report_rec_amenities.getText().toString());
					vl.setreport_comp3_rec_terrain(report_rec_terrain.getText().toString());
					vl.setreport_comp3_rec_shape(report_rec_shape.getText().toString());
					vl.setreport_comp3_rec_corner_influence(report_rec_corner_influence.getText().toString());
					vl.setreport_comp3_rec_tru_lot(report_rec_tru_lot.getText().toString());
					vl.setreport_comp3_rec_elevation(report_rec_elevation.getText().toString());
					vl.setreport_comp3_rec_time_element(report_rec_time_element.getText().toString());
					vl.setreport_comp3_rec_others(report_rec_others.getText().toString());
					vl.setreport_comp3_rec_degree_of_devt(report_rec_degree_of_devt.getText().toString());
					vl.setreport_comp3_concluded_adjustment(gds.replaceFormat(report_concluded_adjustment.getText().toString()));
					vl.setreport_comp3_adjusted_value(gds.replaceFormat(report_adjusted_value.getText().toString()));
					vl.setreport_comp3_remarks(report_remarks.getText().toString());
					
					db.updateVacant_Lot_comp3(vl, record_id);
					field.clear();
					field.add("valrep_land_comp3_total_rectification");
					value.clear();
					value.add(gds.nullCheck(valrep_land_comp_total_rectification.getText().toString()));
					db2.updateRecord(value,field,"record_id = ?", new String[]{record_id},"vacant_lot");
					db.close();
					myDialog.dismiss();
					compute_average();
				}else{
					Toast.makeText(getApplicationContext(),
						"Please fill up required fields", Toast.LENGTH_SHORT).show();
				}
			}
		});
		myDialog.show();
	}

	public void view_comp4(){
		myDialog_config();
		List<Vacant_Lot_API> vl = db.getVacant_Lot(String.valueOf(record_id));
		if (!vl.isEmpty()) {
			for (Vacant_Lot_API im : vl) {
				//date of inspection
				// set current date into datepicker
				if ((!im.getreport_comp4_date_year().equals(""))||(!im.getreport_comp4_date_month().equals(""))) {
					report_date.init(
							Integer.parseInt(im.getreport_comp4_date_year()),
							Integer.parseInt(im.getreport_comp4_date_month())-1,
							Integer.parseInt(im.getreport_comp4_date_day()),
							null);
				}
				report_source.setText(im.getreport_comp4_source());
				report_contact_no.setText(im.getreport_comp4_contact_no());
				report_location.setText(im.getreport_comp4_location());
				report_area.setText(gds.numberFormat(im.getreport_comp4_area()));
				report_base_price.setText(gds.numberFormat(im.getreport_comp4_base_price()));
				report_price_sqm.setText(gds.numberFormat(im.getreport_comp4_price_sqm()));
				report_discount_rate.setText(im.getreport_comp4_discount_rate());
				report_discounts.setText(gds.numberFormat(im.getreport_comp4_discounts()));
				report_selling_price.setText(gds.numberFormat(im.getreport_comp4_selling_price()));
				report_rec_location.setText(im.getreport_comp4_rec_location());
				report_rec_size.setText(im.getreport_comp4_rec_size());
				report_rec_accessibility.setText("0");
				report_rec_amenities.setText(im.getreport_comp4_rec_amenities());
				report_rec_terrain.setText(im.getreport_comp4_rec_terrain());
				report_rec_shape.setText(im.getreport_comp4_rec_shape());
				report_rec_corner_influence.setText(im.getreport_comp4_rec_corner_influence());
				//report_rec_tru_lot.setText(im.getreport_comp4_rec_tru_lot());
				report_rec_elevation.setText(im.getreport_comp4_rec_elevation());
				//report_rec_time_element.setText(im.getreport_comp4_rec_time_element());
				report_rec_others.setText(im.getreport_comp4_rec_others());
				report_rec_degree_of_devt.setText(im.getreport_comp4_rec_degree_of_devt());
				report_concluded_adjustment.setText(gds.numberFormat(im.getreport_comp4_concluded_adjustment()));
				report_adjusted_value.setText(gds.numberFormat(im.getreport_comp4_adjusted_value()));
				report_remarks.setText(im.getreport_comp4_remarks());
				args[0]=record_id;
				valrep_land_comp_total_rectification.setText(db2.getRecord("valrep_land_comp4_total_rectification",where,args,"vacant_lot").get(0));

			}
		}

		report_area.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_area, this, s, current);
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_base_price.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_base_price, this, s, current);

				comp_textwatch();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_discount_rate.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_location.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_size.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_corner_influence.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_amenities.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();


			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_terrain.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_shape.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_elevation.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_others.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_degree_of_devt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		report_price_sqm.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_price_sqm, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_discounts.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_discounts, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_selling_price.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_selling_price, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_concluded_adjustment.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_concluded_adjustment, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_adjusted_value.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_adjusted_value, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});


		btn_save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//list of EditTexts put in array

				null_to_zero();
				/*gds.fill_in_error(new EditText[] { report_source,report_area,
						report_base_price });
				boolean fieldsOK = gds.validate(new EditText[] { report_source,report_area,
						report_base_price});*/
				boolean fieldsOK=true;
				if (fieldsOK==true){ //check if all EditTexts in array are filled up
					//update data in SQLite
					Vacant_Lot_API vl = new Vacant_Lot_API();
					//date
					if (String.valueOf(report_date.getMonth()).length() == 1) {//if month is 4 digit add 0 to initial +4 to value
						if (String.valueOf(report_date.getMonth())
								.equals("9")) {//if october==9 just add 4
							vl.setreport_comp4_date_month(String
									.valueOf(report_date.getMonth() + 1));
						} else {
							vl.setreport_comp4_date_month("0"
									+ String.valueOf(report_date
									.getMonth() + 1));
						}
					} else {
						vl.setreport_comp4_date_month(String
								.valueOf(report_date.getMonth() + 1));
					}
					vl.setreport_comp4_date_day(String.valueOf(report_date.getDayOfMonth()));
					vl.setreport_comp4_date_year(String.valueOf(report_date.getYear()));
					
					vl.setreport_comp4_source(report_source.getText().toString());
					vl.setreport_comp4_contact_no(report_contact_no.getText().toString());
					vl.setreport_comp4_location(report_location.getText().toString());
					vl.setreport_comp4_area(gds.replaceFormat(report_area.getText().toString()));
					vl.setreport_comp4_base_price(gds.replaceFormat(report_base_price.getText().toString()));
					vl.setreport_comp4_price_sqm(gds.replaceFormat(report_price_sqm.getText().toString()));
					vl.setreport_comp4_discount_rate(report_discount_rate.getText().toString());
					vl.setreport_comp4_discounts(gds.replaceFormat(report_discounts.getText().toString()));
					vl.setreport_comp4_selling_price(gds.replaceFormat(report_selling_price.getText().toString()));
					vl.setreport_comp4_rec_location(report_rec_location.getText().toString());
					vl.setreport_comp4_rec_size(report_rec_size.getText().toString());
					vl.setreport_comp4_rec_accessibility(report_rec_accessibility.getText().toString());
					vl.setreport_comp4_rec_amenities(report_rec_amenities.getText().toString());
					vl.setreport_comp4_rec_terrain(report_rec_terrain.getText().toString());
					vl.setreport_comp4_rec_shape(report_rec_shape.getText().toString());
					vl.setreport_comp4_rec_corner_influence(report_rec_corner_influence.getText().toString());
					vl.setreport_comp4_rec_tru_lot(report_rec_tru_lot.getText().toString());
					vl.setreport_comp4_rec_elevation(report_rec_elevation.getText().toString());
					vl.setreport_comp4_rec_time_element(report_rec_time_element.getText().toString());
					vl.setreport_comp4_rec_others(report_rec_others.getText().toString());
					vl.setreport_comp4_rec_degree_of_devt(report_rec_degree_of_devt.getText().toString());
					vl.setreport_comp4_concluded_adjustment(gds.replaceFormat(report_concluded_adjustment.getText().toString()));
					vl.setreport_comp4_adjusted_value(gds.replaceFormat(report_adjusted_value.getText().toString()));
					vl.setreport_comp4_remarks(report_remarks.getText().toString());
					
					db.updateVacant_Lot_comp4(vl, record_id);
					field.clear();
					field.add("valrep_land_comp4_total_rectification");
					value.clear();
					value.add(gds.nullCheck(valrep_land_comp_total_rectification.getText().toString()));
					db2.updateRecord(value,field,"record_id = ?", new String[]{record_id},"vacant_lot");
					db.close();
					myDialog.dismiss();
					compute_average();
				}else{
					Toast.makeText(getApplicationContext(),
						"Please fill up required fields", Toast.LENGTH_SHORT).show();
				}
			}
		});
		myDialog.show();
	}

	public void view_comp5(){
		myDialog_config();
		List<Vacant_Lot_API> vl = db.getVacant_Lot(String.valueOf(record_id));
		if (!vl.isEmpty()) {
			for (Vacant_Lot_API im : vl) {
				//date of inspection
				// set current date into datepicker
				if ((!im.getreport_comp5_date_year().equals(""))||(!im.getreport_comp5_date_month().equals(""))) {
					report_date.init(
							Integer.parseInt(im.getreport_comp5_date_year()),
							Integer.parseInt(im.getreport_comp5_date_month())-1,
							Integer.parseInt(im.getreport_comp5_date_day()),
							null);
				}
				report_source.setText(im.getreport_comp5_source());
				report_contact_no.setText(im.getreport_comp5_contact_no());
				report_location.setText(im.getreport_comp5_location());
				report_area.setText(gds.numberFormat(im.getreport_comp5_area()));
				report_base_price.setText(gds.numberFormat(im.getreport_comp5_base_price()));
				report_price_sqm.setText(gds.numberFormat(im.getreport_comp5_price_sqm()));
				report_discount_rate.setText(im.getreport_comp5_discount_rate());
				report_discounts.setText(gds.numberFormat(im.getreport_comp5_discounts()));
				report_selling_price.setText(gds.numberFormat(im.getreport_comp5_selling_price()));
				report_rec_location.setText(im.getreport_comp5_rec_location());
				report_rec_size.setText(im.getreport_comp5_rec_size());
				report_rec_accessibility.setText("0");
				report_rec_amenities.setText(im.getreport_comp5_rec_amenities());
				report_rec_terrain.setText(im.getreport_comp5_rec_terrain());
				report_rec_shape.setText(im.getreport_comp5_rec_shape());
				report_rec_corner_influence.setText(im.getreport_comp5_rec_corner_influence());
				//report_rec_tru_lot.setText(im.getreport_comp5_rec_tru_lot());
				report_rec_elevation.setText(im.getreport_comp5_rec_elevation());
				//report_rec_time_element.setText(im.getreport_comp5_rec_time_element());
				report_rec_others.setText(im.getreport_comp5_rec_others());
				report_rec_degree_of_devt.setText(im.getreport_comp5_rec_degree_of_devt());
				report_concluded_adjustment.setText(gds.numberFormat(im.getreport_comp5_concluded_adjustment()));
				report_adjusted_value.setText(gds.numberFormat(im.getreport_comp5_adjusted_value()));
				report_remarks.setText(im.getreport_comp5_remarks());
				args[0]=record_id;
				valrep_land_comp_total_rectification.setText(db2.getRecord("valrep_land_comp5_total_rectification",where,args,"vacant_lot").get(0));

			}
		}

		report_area.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_area, this, s, current);
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_base_price.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_base_price, this, s, current);

				comp_textwatch();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_discount_rate.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_location.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_size.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_corner_influence.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_amenities.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();


			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_terrain.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_shape.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_elevation.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_others.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_rec_degree_of_devt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				comp_textwatch();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		report_price_sqm.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_price_sqm, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_discounts.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_discounts, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_selling_price.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_selling_price, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_concluded_adjustment.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_concluded_adjustment, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_adjusted_value.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_adjusted_value, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});


		btn_save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				null_to_zero();
				/*gds.fill_in_error(new EditText[] { report_source,report_area,
						report_base_price });
				boolean fieldsOK = validate(new EditText[] { report_source,report_area,
						report_base_price});*/
				boolean fieldsOK = true;
				if (fieldsOK==true){ //check if all EditTexts in array are filled up
					//update data in SQLite
					Vacant_Lot_API vl = new Vacant_Lot_API();
					//date
					if (String.valueOf(report_date.getMonth()).length() == 1) {//if month is 5 digit add 0 to initial +5 to value
						if (String.valueOf(report_date.getMonth())
								.equals("9")) {//if october==9 just add 5
							vl.setreport_comp5_date_month(String
									.valueOf(report_date.getMonth() + 1));
						} else {
							vl.setreport_comp5_date_month("0"
									+ String.valueOf(report_date
									.getMonth() + 1));
						}
					} else {
						vl.setreport_comp5_date_month(String
								.valueOf(report_date.getMonth() + 1));
					}
					vl.setreport_comp5_date_day(String.valueOf(report_date.getDayOfMonth()));
					vl.setreport_comp5_date_year(String.valueOf(report_date.getYear()));
					
					vl.setreport_comp5_source(report_source.getText().toString());
					vl.setreport_comp5_contact_no(report_contact_no.getText().toString());
					vl.setreport_comp5_location(report_location.getText().toString());
					vl.setreport_comp5_area(gds.replaceFormat(report_area.getText().toString()));
					vl.setreport_comp5_base_price(gds.replaceFormat(report_base_price.getText().toString()));
					vl.setreport_comp5_price_sqm(gds.replaceFormat(report_price_sqm.getText().toString()));
					vl.setreport_comp5_discount_rate(report_discount_rate.getText().toString());
					vl.setreport_comp5_discounts(gds.replaceFormat(report_discounts.getText().toString()));
					vl.setreport_comp5_selling_price(gds.replaceFormat(report_selling_price.getText().toString()));
					vl.setreport_comp5_rec_location(report_rec_location.getText().toString());
					vl.setreport_comp5_rec_size(report_rec_size.getText().toString());
					vl.setreport_comp5_rec_accessibility(report_rec_accessibility.getText().toString());
					vl.setreport_comp5_rec_amenities(report_rec_amenities.getText().toString());
					vl.setreport_comp5_rec_terrain(report_rec_terrain.getText().toString());
					vl.setreport_comp5_rec_shape(report_rec_shape.getText().toString());
					vl.setreport_comp5_rec_corner_influence(report_rec_corner_influence.getText().toString());
					vl.setreport_comp5_rec_tru_lot(report_rec_tru_lot.getText().toString());
					vl.setreport_comp5_rec_elevation(report_rec_elevation.getText().toString());
					vl.setreport_comp5_rec_time_element(report_rec_time_element.getText().toString());
					vl.setreport_comp5_rec_others(report_rec_others.getText().toString());
					vl.setreport_comp5_rec_degree_of_devt(report_rec_degree_of_devt.getText().toString());
					vl.setreport_comp5_concluded_adjustment(gds.replaceFormat(report_concluded_adjustment.getText().toString()));
					vl.setreport_comp5_adjusted_value(gds.replaceFormat(report_adjusted_value.getText().toString()));
					vl.setreport_comp5_remarks(report_remarks.getText().toString());
					
					db.updateVacant_Lot_comp5(vl, record_id);
					field.clear();
					field.add("valrep_land_comp5_total_rectification");
					value.clear();
					value.add(gds.nullCheck(valrep_land_comp_total_rectification.getText().toString()));
					db2.updateRecord(value,field,"record_id = ?", new String[]{record_id},"vacant_lot");
					db.close();
					myDialog.dismiss();
					compute_average();
				}else{
					Toast.makeText(getApplicationContext(),
						"Please fill up required fields", Toast.LENGTH_SHORT).show();
				}
			}
		});
		myDialog.show();
	}

	public void comp_textwatch(){
		String base_price_d = gds.nullCheck2(report_base_price.getText().toString());
		String area_d = gds.nullCheck2(report_area.getText().toString());
		String price_sqm_d = gds.getPriceSQM(base_price_d, area_d);
		report_price_sqm.setText(price_sqm_d);
		//discounts = price_sqm x discount_rate



		if(!report_discount_rate.getText().toString().contentEquals("-")&&!report_rec_location.getText().toString().contentEquals("-")&&!report_rec_size.getText().toString().contentEquals("-")
				&&!report_rec_corner_influence.getText().toString().contentEquals("-")&&!report_rec_amenities.getText().toString().contentEquals("-")&&!report_rec_terrain.getText().toString().contentEquals("-")&&!report_rec_shape.getText().toString().contentEquals("-")
				&&!report_rec_elevation.getText().toString().contentEquals("-")&&!report_rec_others.getText().toString().contentEquals("-")&&!report_rec_degree_of_devt.getText().toString().contentEquals("-")){
			if(!report_discount_rate.getText().toString().contentEquals(".")&&!report_rec_location.getText().toString().contentEquals(".")&&!report_rec_size.getText().toString().contentEquals(".")
					&&!report_rec_corner_influence.getText().toString().contentEquals(".")&&!report_rec_amenities.getText().toString().contentEquals(".")&&!report_rec_terrain.getText().toString().contentEquals(".")&&!report_rec_shape.getText().toString().contentEquals(".")
					&&!report_rec_elevation.getText().toString().contentEquals(".")&&!report_rec_others.getText().toString().contentEquals(".")&&!report_rec_degree_of_devt.getText().toString().contentEquals(".")){
				if(!report_discount_rate.getText().toString().contentEquals("-.")&&!report_rec_location.getText().toString().contentEquals("-.")&&!report_rec_size.getText().toString().contentEquals("-.")
						&&!report_rec_corner_influence.getText().toString().contentEquals("-.")&&!report_rec_amenities.getText().toString().contentEquals("-.")&&!report_rec_terrain.getText().toString().contentEquals("-.")&&!report_rec_shape.getText().toString().contentEquals("-.")
						&&!report_rec_elevation.getText().toString().contentEquals("-.")&&!report_rec_others.getText().toString().contentEquals("-.")&&!report_rec_degree_of_devt.getText().toString().contentEquals("-.")){
					String discount_rate_d = gds.nullCheck2(report_discount_rate.getText().toString());

					String discounts_d = gds.getDiscount(price_sqm_d, discount_rate_d);
					report_discounts.setText(discounts_d);

					//discount

					String discounts_s = gds.nullCheck2(report_discounts.getText().toString());
					//index price
					String selling_price_d = gds.computesellingPrice(price_sqm_d,discounts_s);
					report_selling_price.setText(selling_price_d);

					//total adjustments
					String rec_location = gds.nullCheck2(report_rec_location.getText().toString());
					String rec_size = gds.nullCheck2(report_rec_size.getText().toString());
					String rec_corner_influence = gds.nullCheck2(report_rec_corner_influence.getText().toString());
					String rec_amenities = gds.nullCheck2(report_rec_amenities.getText().toString());
					String rec_terrain = gds.nullCheck2(report_rec_terrain.getText().toString());
					String rec_shape = gds.nullCheck2(report_rec_shape.getText().toString());
					String rec_elevation = gds.nullCheck2(report_rec_elevation.getText().toString());
					String rec_others = gds.nullCheck2(report_rec_others.getText().toString());
					String rec_degree_of_devt = gds.nullCheck2(report_rec_degree_of_devt.getText().toString());
					String total_rectif = gds.rectifTotalVL(rec_location,rec_size,rec_corner_influence,rec_amenities,
							rec_terrain,rec_shape,rec_elevation,rec_others,rec_degree_of_devt);
					valrep_land_comp_total_rectification.setText(total_rectif);
					String concluded_adjustment_d = gds.rectifVL(rec_location,rec_size,rec_corner_influence,rec_amenities,
							rec_terrain,rec_shape,rec_elevation,rec_others,rec_degree_of_devt,selling_price_d);
					report_concluded_adjustment.setText(concluded_adjustment_d);

					//adjusted_value = concluded_adjustment + selling_price
					String adjusted_value_d = gds.adjustedvalueVL(concluded_adjustment_d,selling_price_d);
					//display output
					report_adjusted_value.setText(adjusted_value_d);
				}
			}
		}
	}

	
	public void myDialog_config(){
		myDialog = new Dialog(VL_Comparatives.this);
		//myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setTitle(dialog_title);
		myDialog.setContentView(R.layout.comparatives_vl_form);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.FILL_PARENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		
		btn_compute = (Button) myDialog.findViewById(R.id.btn_compute);
		btn_save = (Button) myDialog.findViewById(R.id.btn_save);
		
		report_date = (DatePicker) myDialog.findViewById(R.id.report_date);
		
		report_source = (EditText) myDialog.findViewById(R.id.report_source);
		report_contact_no = (EditText) myDialog.findViewById(R.id.report_contact_no);
		report_location = (EditText) myDialog.findViewById(R.id.report_location);
		report_area = (EditText) myDialog.findViewById(R.id.report_area);
		report_base_price = (EditText) myDialog.findViewById(R.id.report_base_price);
		report_price_sqm = (EditText) myDialog.findViewById(R.id.report_price_sqm);
		report_discount_rate = (EditText) myDialog.findViewById(R.id.report_discount_rate);
		report_discounts = (EditText) myDialog.findViewById(R.id.report_discounts);
		report_selling_price = (EditText) myDialog.findViewById(R.id.report_selling_price);
		report_rec_location = (EditText) myDialog.findViewById(R.id.report_rec_location);
		report_rec_size = (EditText) myDialog.findViewById(R.id.report_rec_size);
		report_rec_accessibility = (EditText) myDialog.findViewById(R.id.report_rec_accessibility);
		report_rec_amenities = (EditText) myDialog.findViewById(R.id.report_rec_amenities);
		report_rec_terrain = (EditText) myDialog.findViewById(R.id.report_rec_terrain);
		report_rec_shape = (EditText) myDialog.findViewById(R.id.report_rec_shape);
		report_rec_corner_influence = (EditText) myDialog.findViewById(R.id.report_rec_corner_influence);
		report_rec_tru_lot = (EditText) myDialog.findViewById(R.id.report_rec_tru_lot);
		report_rec_elevation = (EditText) myDialog.findViewById(R.id.report_rec_elevation);
		report_rec_time_element = (EditText) myDialog.findViewById(R.id.report_rec_time_element);
		report_rec_others = (EditText) myDialog.findViewById(R.id.report_rec_others);
		report_rec_degree_of_devt = (EditText) myDialog.findViewById(R.id.report_rec_degree_of_devt);
		report_concluded_adjustment = (EditText) myDialog.findViewById(R.id.report_concluded_adjustment);
		valrep_land_comp_total_rectification = (EditText) myDialog.findViewById(R.id.valrep_land_comp_total_rectification);
		report_adjusted_value = (EditText) myDialog.findViewById(R.id.report_adjusted_value);
		report_remarks = (EditText) myDialog.findViewById(R.id.report_remarks);
		report_price_sqm.setEnabled(false);
		report_discounts.setEnabled(false);
		report_selling_price.setEnabled(false);
		report_concluded_adjustment.setEnabled(false);
		report_adjusted_value.setEnabled(false);
		valrep_land_comp_total_rectification.setEnabled(false);
	}
	

	
	public void compute_average(){
		fill_data();
		int numOfAdjVal=0;

		List<Vacant_Lot_API> vl = db.getVacant_Lot(String.valueOf(record_id));
		if (!vl.isEmpty()) {
			for (Vacant_Lot_API im : vl) {
				if (im.getreport_comp1_adjusted_value().length()!=0){
					adj_val_1 = gds.StringtoDouble(im.getreport_comp1_adjusted_value());
				} else {
					adj_val_1 = 0.00;
				}
				if (im.getreport_comp2_adjusted_value().length()!=0){
					adj_val_2 = gds.StringtoDouble(im.getreport_comp2_adjusted_value());
				} else {
					adj_val_2 = 0.00;
				}
				if (im.getreport_comp3_adjusted_value().length()!=0){
					adj_val_3 = gds.StringtoDouble(im.getreport_comp3_adjusted_value());
				} else {
					adj_val_3 = 0.00;
				}
				if (im.getreport_comp4_adjusted_value().length()!=0){
					adj_val_4 = gds.StringtoDouble(im.getreport_comp4_adjusted_value());
				} else {
					adj_val_4 = 0.00;
				}
				if (im.getreport_comp5_adjusted_value().length()!=0){
					adj_val_5 = gds.StringtoDouble(im.getreport_comp5_adjusted_value());
				} else {
					adj_val_5 = 0.00;
				}
			}
		}
		if (adj_val_1!=0.00){
			numOfAdjVal++;
			if (adj_val_1.isNaN()){
				numOfAdjVal--;
				adj_val_1=0.00;
			}
		}
		if (adj_val_2!=0.00){
			numOfAdjVal++;
			if (adj_val_2.isNaN()){
				numOfAdjVal--;
				adj_val_2=0.00;
			}
		}
		if (adj_val_3!=0.00){
			numOfAdjVal++;
			if (adj_val_3.isNaN()){
				numOfAdjVal--;
				adj_val_3=0.00;
			}
		}
		if (adj_val_4!=0.00){
			numOfAdjVal++;
			if (adj_val_4.isNaN()){
				numOfAdjVal--;
				adj_val_4=0.00;
			}
		}
		if (adj_val_5!=0.00){
			numOfAdjVal++;
			if (adj_val_5.isNaN()){
				numOfAdjVal--;
				adj_val_5=0.00;
			}
		}
		
		adjusted_value_d = adj_val_1 + adj_val_2 + adj_val_3 + adj_val_4 + adj_val_5;
		average_s = gds.divisionDecimal(""+adjusted_value_d,""+numOfAdjVal);
		average_s=gds.round(average_s);
		report_average.setText(gds.numberFormat(average_s));

		String round_s = gds.roundToHundred(average_s);
		round_to_d = gds.StringtoDouble(round_s);
		//round_to_d = (Math.round(average2_d/1000.00)*1000.00);
		report_round_to.setText(gds.numberFormat(String.valueOf(df2.format(round_to_d))));
		

		myDialog = new Dialog(VL_Comparatives.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.comparative_summary);
		myDialog.setCancelable(true);

		myDialog.setCanceledOnTouchOutside(true);
		final TextView tv_val1 = (TextView) myDialog.findViewById(R.id.tv_val1);
		final TextView tv_val2 = (TextView) myDialog.findViewById(R.id.tv_val2);
		final TextView tv_val3 = (TextView) myDialog.findViewById(R.id.tv_val3);
		final TextView tv_val4 = (TextView) myDialog.findViewById(R.id.tv_val4);
		final TextView tv_val5 = (TextView) myDialog.findViewById(R.id.tv_val5);
		final TextView tv_total = (TextView) myDialog.findViewById(R.id.tv_total);
		final TextView tv_numOfAdj = (TextView) myDialog.findViewById(R.id.tv_numOfAdj);
		final TextView tv_average = (TextView) myDialog.findViewById(R.id.tv_average);
		
		tv_val1.setText(gds.numberFormat(String.valueOf(adj_val_1)));
		tv_val2.setText(gds.numberFormat(String.valueOf(adj_val_2)));
		tv_val3.setText(gds.numberFormat(String.valueOf(adj_val_3)));
		tv_val4.setText(gds.numberFormat(String.valueOf(adj_val_4)));
		tv_val5.setText(gds.numberFormat(String.valueOf(adj_val_5)));
		tv_total.setText(gds.numberFormat(String.valueOf(adj_val_1 + adj_val_2 + adj_val_3 + adj_val_4 + adj_val_5)));
		tv_numOfAdj.setText(String.valueOf(numOfAdjVal));
		tv_average.setText(gds.numberFormat(average_s));
		myDialog.show();
	}
	
	public void save_comparatives(){
		Vacant_Lot_API vl = new Vacant_Lot_API();
		vl.setreport_comp_average(gds.replaceFormat(report_average.getText().toString()));
		vl.setreport_comp_rounded_to(gds.replaceFormat(report_round_to.getText().toString()));

		db.updateVacant_Lot_comparatives(vl, record_id);
		//ADDED BY IAN
		String deduc_s="0";
		String area_s="0";
		List<Vacant_Lot_API_Lot_Valuation_Details> propDescList = db2
				.getVacant_Lot_Lot_Valuation_Details_with_lot_valuation_details_id(String.valueOf(record_id),"1");
		if (!propDescList.isEmpty()) {
			for (Vacant_Lot_API_Lot_Valuation_Details vl3 : propDescList) {
				deduc_s=gds.nullCheck2(vl3.getreport_value_deduction());
				//unit_value_s=gds.nullCheck2(vl3.getreport_value_unit_value());
				area_s=gds.nullCheck2(vl3.getreport_value_area());
			}
		}

		//update valuation
		Vacant_Lot_API_Lot_Valuation_Details vl2 = new Vacant_Lot_API_Lot_Valuation_Details();
		String net_area_val = gds.netAreaVL(area_s, deduc_s);
		vl2.setreport_value_net_area(net_area_val);
		String total_land_val_val = gds.totalLandVL(net_area_val,report_round_to.getText().toString());
		vl2.setreport_value_land_value(total_land_val_val);
		db2.updateVacant_Lot_Lot_Valuation_Details3(vl2, record_id, "1");

		String grand_total_land_val = gds.nullCheck(db2.getVacant_Lot_API_Total_Land_Value_Lot_Valuation_Details(record_id));
		String grand_total_land_val_s = gds.stringToDecimal(grand_total_land_val);
		//update Summary
		Vacant_Lot_API vl4 = new Vacant_Lot_API();
		vl4.setreport_final_value_total_appraised_value_land_imp(grand_total_land_val_s);
		db.updateVacant_Lot_Summary_Total(vl4, record_id);
		db2.close();
		//END OF ADDED BY IAN

		db.close();
		Toast.makeText(getApplicationContext(), "Saved",Toast.LENGTH_SHORT).show();
	}
	public void null_to_zero(){
		if (report_discount_rate.getText().length() == 0) {
			report_discount_rate.setText("");
		}
		if (report_rec_location.getText().length() == 0) {
			report_rec_location.setText("");
		}
		if (report_rec_size.getText().length() == 0) {
			report_rec_size.setText("");
		}
		if (report_rec_corner_influence.getText().length() == 0) {
			report_rec_corner_influence.setText("");
		}
		if (report_rec_amenities.getText().length() == 0) {
			report_rec_amenities.setText("");
		}
		if (report_rec_terrain.getText().length() == 0) {
			report_rec_terrain.setText("");
		}
		if (report_rec_shape.getText().length() == 0) {
			report_rec_shape.setText("");
		}
		if (report_rec_elevation.getText().length() == 0) {
			report_rec_elevation.setText("");
		}
		if (report_rec_degree_of_devt.getText().length() == 0) {
			report_rec_degree_of_devt.setText("");
		}
		if (report_rec_others.getText().length() == 0) {
			report_rec_others.setText("");
		}


	}

	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}


}