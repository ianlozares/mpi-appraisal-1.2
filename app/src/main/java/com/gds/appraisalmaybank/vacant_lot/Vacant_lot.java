package com.gds.appraisalmaybank.vacant_lot;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.attachments.Attachments_Inflater;
import com.gds.appraisalmaybank.attachments.Collage;
import com.gds.appraisalmaybank.check_network.NetworkUtil;
import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Images;
import com.gds.appraisalmaybank.database.Logs;
import com.gds.appraisalmaybank.database.Report_Accepted_Jobs;
import com.gds.appraisalmaybank.database.Report_Accepted_Jobs_Contacts;
import com.gds.appraisalmaybank.database.Report_filename;
import com.gds.appraisalmaybank.database.Required_Attachments;
import com.gds.appraisalmaybank.database.TableObjects;
import com.gds.appraisalmaybank.database.Vacant_Lot_API;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database2.Vacant_Lot_API_Lot_Details;
import com.gds.appraisalmaybank.database2.Vacant_Lot_API_Lot_Valuation_Details;
import com.gds.appraisalmaybank.database2.Vacant_Lot_API_Prev_Appraisal;
import com.gds.appraisalmaybank.database2.Vacant_Lot_API_RDPS;
import com.gds.appraisalmaybank.database_new.DatabaseHandler_New;
import com.gds.appraisalmaybank.database_new.Tables;
import com.gds.appraisalmaybank.json.TLSConnection;
import com.gds.appraisalmaybank.json.UserFunctions;
import com.gds.appraisalmaybank.main.Connectivity;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;
import com.gds.appraisalmaybank.report.View_Report;
import com.gds.appraisalmaybank.required_fields.Fields_Checker;
import com.gds.appraisalmaybank.townhouse_condo.Pdf_Townhouse_condo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import javax.net.ssl.HttpsURLConnection;


@SuppressLint("InflateParams")
public class Vacant_lot extends Activity implements OnClickListener, OnCheckedChangeListener {
    GDS_methods gds = new GDS_methods();
    Session_Timer st = new Session_Timer(this);
    ActionBar actionBar;
    private static final String TAG_UID = "uid";
    private static final String TAG_RECORD_ID = "record_id";
    private static final String TAG_PASS_STAT = "pass_stat";
    String pass_stat = "offline";
    Double matrix_value;

    TextView tv_requested_month, tv_requested_day, tv_requested_year,
            tv_classification, tv_account_name,tv_company_name, tv_requesting_party, tv_requestor,
            tv_control_no, tv_appraisal_type, tv_nature_appraisal,
            tv_ins_month1, tv_ins_day1, tv_ins_year1, tv_ins_month2,
            tv_ins_day2, tv_ins_year2, tv_com_month, tv_com_day, tv_com_year,
            tv_tct_no, tv_unit_no, tv_bldg_name, tv_street_no, tv_street_name,
            tv_village, tv_district, tv_zip_code, tv_city, tv_province,
            tv_region, tv_country, tv_address,
            tv_attachment, tv_app_request_remarks, tv_app_kind_of_appraisal, tv_app_branch_code;
    LinearLayout ll_contact;
    EditText et_first_name, et_middle_name, et_last_name, et_requestor,app_account_company_name;
    CheckBox app_account_is_company;
    Spinner spinner_purpose_appraisal;
    EditText et_app_kind_of_appraisal;
    AutoCompleteTextView et_requesting_party;
    EditText report_date_requested;
    Button btn_update_app_details, btn_save_app_details, btn_cancel_app_details;
    DatabaseHandler db = new DatabaseHandler(this);
    DatabaseHandler2 db2 = new DatabaseHandler2(this);
    Global gs;
    TLSConnection tlscon = new TLSConnection();
    ArrayList<String> field = new ArrayList<String>();
    ArrayList<String> value = new ArrayList<String>();
    String requested_month, requested_day, requested_year, classification,
            account_fname, account_mname, account_lname, requesting_party, requestor,
            control_no, appraisal_type, nature_appraisal, ins_month1, ins_day1,
            ins_year1, ins_month2, ins_day2, ins_year2, com_month, com_day,
            com_year, tct_no, unit_no, bldg_name, street_no, street_name,
            village, district, zip_code, city, province, region, country,
            counter, output_appraisal_type, output_street_name, output_village,
            output_city, output_province, app_request_remarks, app_branch_code, app_kind_of_appraisal,account_is_company,account_company_name;
    String lot_no, block_no;
    String record_id, pdf_name;
    ImageView btn_report_page1, btn_report_page2, btn_report_page3, btn_report_page4;
    ImageView btn_comparatives;
    Button btn_report_update_cc, btn_report_view_report;
    ProgressDialog pDialog;
    ImageView btn_select_pdf, btn_report_pdf;
    TextView tv_address_hide;
    // for attachments
    int responseCode;
    //for fileUpload checking
    boolean fileUploaded = false;

    String google_response = "";
    private ListView dlg_priority_lvw = null;
    Context context = this;
    String item, filename;
    String dir = "gds_appraisal";
    File myDirectory = new File(Environment.getExternalStorageDirectory() + "/"
            + dir + "/");
    String attachment_selected, uid;
    String db_app_uid, db_app_file, db_app_filename, db_app_appraisal_type,
            db_app_candidate_done,unaccept;

    // page2 land details
    Dialog myDialog, myDialog2;
    //Date Inspected
    DatePicker report_date_inspected;
    EditText report_time_inspected;

    //Basis of Iden
    TextView basis_identification, physical_characteristic,facilities;
    CheckBox report_homeowners_assoc, report_tax_mapping, report_lra_office,
            report_lot_config, report_subd_map, report_neighborhood_checking, valrep_land_id_others, valrep_land_physical_others;
    //Site Imp
    EditText report_street_name, valrep_land_id_others_desc, valrep_land_physical_others_desc;
    Spinner spinner_street, spinner_sidewalk, spinner_curb, spinner_drainage, spinner_street_lights,valrep_land_road_right_of_way;
    //Physical Char
    CheckBox report_corner_lot, report_non_corner_lot, report_perimeter_lot,
            report_intersected_lot, report_interior_with_row, report_landlocked;
    Spinner spinner_shape;
    EditText report_frontage, report_depth, report_road;
    Spinner spinner_elevation, spinner_terrain;
    //Lot class
    Spinner spinner_per_tax_desc, spinner_actual_usage, spinner_neighborhood,
            spinner_highest_and_best_use;
    //Notable Landmarks
    EditText report_landmark_1, report_landmark_2, report_landmark_3, report_landmark_4, report_landmark_5;
    EditText report_distance_1, report_distance_2, report_distance_3, report_distance_4, report_distance_5;
    //Utilities
    EditText report_electricity;
    Spinner spinner_water, spinner_telephone, spinner_garbage;
    //Transportation
    CheckBox report_jeepneys, report_bus, report_taxi, report_tricycle;
    //Facilities
    CheckBox report_recreation_facilities, report_security, report_clubhouse,
            report_swimming_pool;
    //rdps
    Button btn_view_rdps, btn_view_prev_appraisal;
    //boundaries
    EditText report_right, report_left, report_rear, report_front;
    //Prev Appraisal
    EditText report_prev_date, report_prev_appraiser;
    TextView tv_prev_date, tv_prev_appraiser;
    Button btn_update_page2;
    String nature_appraisal_land_d;
    // page4 summary
    EditText report_final_value_total_appraised_value_land_imp;
    CheckBox app_unacceptable_collateral;
    Spinner report_final_value_recommended_deductions_desc;
    EditText report_final_value_recommended_deductions_rate, report_final_value_recommended_deductions_other, report_final_value_recommended_deductions_amt,
            report_final_value_net_appraised_value, report_final_value_quick_sale_value_rate,
            report_final_value_quick_sale_value_amt, /*report_factors_of_concern,
            report_suggested_corrective_actions,*/
            report_remarks/*, report_requirements*/;
    TextView /*tv_factors_of_concern, tv_suggested_corrective_actions,*/ tv_remarks, tv_appraised_value;
    CheckBox concerns_minor_1, concerns_minor_2, concerns_minor_3, concerns_minor_others;
    EditText concerns_minor_others_desc, concerns_major_others_desc;
    CheckBox concerns_major_1, concerns_major_2, concerns_major_3, concerns_major_4, concerns_major_5, concerns_major_6, concerns_major_others;


    Button btn_update_summary;


    // zonal dialog
    Button btn_update_zonal, btn_save_zonal, btn_cancel_zonal;
    EditText report_zonal_location, report_zonal_lot_classification, report_zonal_value;
    TextView tv_location, tv_lot_classification, tv_value_per_sqm;

    //address
    Button btn_update_address, btn_save_address;
    EditText et_unit_no, et_building_name, et_lot_no, et_block_no, et_street_no, et_street_name,
            et_village, et_district, et_zip_code, et_city, et_province,
            et_region, et_country;

    //case center attachments
    ImageView btn_mysql_attachments;

    //switch appraisal type
    Button btn_switch_app_type;

    Button btn_manual_delete;

    // uploaded attachments
    LinearLayout container_attachments;
    String uploaded_attachment;
    String url_webby;
    UserFunctions userFunction = new UserFunctions();
    File uploaded_file;
    String[] commandArray = new String[]{"View Attachment in Browser",
            "Download Attachment"};
    // for API
    String xml;

    boolean network_status;
    // Progress Dialog

    // Progress dialog type (0 - for Horizontal progress bar)
    public static final int progress_bar_type = 0;

    TextView tv_rework_reason_header, tv_rework_reason;
    String rework_reason = "";
    boolean withAttachment = false, freshData = true;
    //boolean hasOwnership = false;
    boolean appStatReviewer = false;

    //detect internet connection
    TextView tv_connection;
    boolean wifi = false;
    boolean data = false;
    boolean data_speed = false;
    boolean freeFields;
    private NetworkChangeReceiver receiver;
    private boolean isConnected = false;
    boolean globalSave = false;
    String where = "WHERE record_id = args";
    String[] args = new String[1];


    DatabaseHandler_New db_new = new DatabaseHandler_New(this);

    public class NetworkChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            isNetworkAvailable(context);
        }

        private boolean isNetworkAvailable(final Context context) {
            ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null) {
                    for (int i = 0; i < info.length; i++) {
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            if (!isConnected) {
                                tv_connection.setText("Connected");
                                tv_connection.setBackgroundColor(Color.parseColor("#3399ff"));
                                isConnected = true;
                                // do your processing here ---
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        // Do something after 1s = 1000ms
                                        wifi = Connectivity.isConnectedWifi(context);
                                        data = Connectivity.isConnectedMobile(context);
                                        data_speed = Connectivity.isConnected(context);
                                        if (wifi) {
                                            tv_connection.setText(R.string.wifi_connected);
                                            tv_connection.setBackgroundColor(Color.parseColor("#39b476"));
                                        } else if (data) {
                                            if (data_speed) {
                                                tv_connection.setText(R.string.data_good);
                                                tv_connection.setBackgroundColor(Color.parseColor("#39b476"));
                                            } else if (data_speed == false) {
                                                tv_connection.setText(R.string.data_weak);
                                                tv_connection.setBackgroundColor(Color.parseColor("#FF9900"));
                                            }
                                        }
                                    }
                                }, 1000);

                            }
                            return true;
                        }
                    }
                }
            }
            tv_connection.setText("No Internet connection");
            tv_connection.setBackgroundColor(Color.parseColor("#CC0000"));
            isConnected = false;
            return false;
        }
    }

    //detect internet connection
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vacant_lot);
        st.resetDisconnectTimer();
        Authenticator.setDefault(new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("gdsuser", "gdsuser01".toCharArray());
            }
        });
        actionBar = getActionBar();
        actionBar.setIcon(R.drawable.ic_vl);
        actionBar.setSubtitle("Vacant Lot");
        String open = "0";
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            open = bundle.getString("keyopen");
        }
        tv_connection = (TextView) findViewById(R.id.tv_connection);
        tv_connection.setVisibility(View.GONE);
		/*IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
		receiver = new NetworkChangeReceiver();
		registerReceiver(receiver, filter);*/

        gs = ((Global) getApplicationContext());
        container_attachments = (LinearLayout) findViewById(R.id.container_attachments);
        ll_contact = (LinearLayout) findViewById(R.id.container);
        tv_requested_month = (TextView) findViewById(R.id.tv_requested_month);
        tv_requested_day = (TextView) findViewById(R.id.tv_requested_day);
        tv_requested_year = (TextView) findViewById(R.id.tv_requested_year);
        tv_classification = (TextView) findViewById(R.id.tv_classification);
        tv_account_name = (TextView) findViewById(R.id.tv_account_name);
        tv_company_name = (TextView) findViewById(R.id.tv_company_name);
        tv_requesting_party = (TextView) findViewById(R.id.tv_requesting_party);
        tv_requestor = (TextView) findViewById(R.id.tv_requestor);
        tv_control_no = (TextView) findViewById(R.id.tv_control_no);
        tv_appraisal_type = (TextView) findViewById(R.id.tv_appraisal_type);
        tv_nature_appraisal = (TextView) findViewById(R.id.tv_nature_appraisal);
        tv_ins_month1 = (TextView) findViewById(R.id.tv_ins_month1);
        tv_ins_day1 = (TextView) findViewById(R.id.tv_ins_day1);
        tv_ins_year1 = (TextView) findViewById(R.id.tv_ins_year1);
        tv_ins_month2 = (TextView) findViewById(R.id.tv_ins_month2);
        tv_ins_day2 = (TextView) findViewById(R.id.tv_ins_day2);
        tv_ins_year2 = (TextView) findViewById(R.id.tv_ins_year2);
        tv_com_month = (TextView) findViewById(R.id.tv_com_month);
        tv_com_day = (TextView) findViewById(R.id.tv_com_day);
        tv_com_year = (TextView) findViewById(R.id.tv_com_year);
        tv_tct_no = (TextView) findViewById(R.id.tv_tct_no);
        tv_unit_no = (TextView) findViewById(R.id.tv_unit_no);
        tv_bldg_name = (TextView) findViewById(R.id.tv_bldg_name);
        tv_street_no = (TextView) findViewById(R.id.tv_street_no);
        tv_street_name = (TextView) findViewById(R.id.tv_street_name);
        tv_village = (TextView) findViewById(R.id.tv_village);
        tv_district = (TextView) findViewById(R.id.tv_district);
        tv_zip_code = (TextView) findViewById(R.id.tv_zip_code);
        tv_city = (TextView) findViewById(R.id.tv_city);
        tv_province = (TextView) findViewById(R.id.tv_province);
        tv_region = (TextView) findViewById(R.id.tv_region);
        tv_country = (TextView) findViewById(R.id.tv_country);
        tv_address = (TextView) findViewById(R.id.tv_address);
        tv_attachment = (TextView) findViewById(R.id.tv_attachment);
        btn_report_page1 = (ImageView) findViewById(R.id.btn_report_page1);//prop desc
        btn_report_page2 = (ImageView) findViewById(R.id.btn_report_page2);//land details
        btn_report_page3 = (ImageView) findViewById(R.id.btn_report_page3);//valuation
        btn_report_page4 = (ImageView) findViewById(R.id.btn_report_page4);//report summary
        btn_comparatives = (ImageView) findViewById(R.id.btn_comparatives);//comparatives
        btn_report_update_cc = (Button) findViewById(R.id.btn_report_update_cc);
        btn_report_view_report = (Button) findViewById(R.id.btn_report_view_report);
        btn_report_pdf = (ImageView) findViewById(R.id.btn_report_pdf);
        btn_select_pdf = (ImageView) findViewById(R.id.btn_select_pdf);
//app details
        btn_update_app_details = (Button) findViewById(R.id.btn_update_app_details);
        btn_update_app_details.setOnClickListener(this);
        tv_address_hide = (TextView) findViewById(R.id.tv_address_hide);
        tv_app_request_remarks = (TextView) findViewById(R.id.tv_app_request_remarks);
        tv_app_kind_of_appraisal = (TextView) findViewById(R.id.tv_app_kind_of_appraisal);
        tv_app_branch_code = (TextView) findViewById(R.id.tv_app_branch_code);


        btn_report_page1.setOnClickListener(this);
        btn_report_page2.setOnClickListener(this);
        btn_report_page3.setOnClickListener(this);
        btn_report_page4.setOnClickListener(this);
        btn_comparatives.setOnClickListener(this);
        btn_report_update_cc.setOnClickListener(this);
        btn_report_view_report.setOnClickListener(this);
        btn_report_pdf.setOnClickListener(this);
        btn_select_pdf.setOnClickListener(this);
        tv_attachment.setOnClickListener(this);

        //zonal value
        tv_location = (TextView) findViewById(R.id.tv_location);
        tv_lot_classification = (TextView) findViewById(R.id.tv_lot_classification);
        tv_value_per_sqm = (TextView) findViewById(R.id.tv_value_per_sqm);
        btn_update_zonal = (Button) findViewById(R.id.btn_update_zonal);
        btn_update_zonal.setOnClickListener(this);

        //case center attachments
        btn_mysql_attachments = (ImageView) findViewById(R.id.btn_mysql_attachments);
        btn_mysql_attachments.setOnClickListener(this);

        //address
        btn_update_address = (Button) findViewById(R.id.btn_update_address);
        btn_update_address.setOnClickListener(this);

        //switch appraisal type
        btn_switch_app_type = (Button) findViewById(R.id.btn_switch_app_type);
        btn_switch_app_type.setOnClickListener(this);


        tv_rework_reason_header = (TextView) findViewById(R.id.tv_rework_reason_header);
        tv_rework_reason = (TextView) findViewById(R.id.tv_rework_reason);

        btn_manual_delete = (Button) findViewById(R.id.btn_manual_delete);
        btn_manual_delete.setOnClickListener(this);

        record_id = gs.record_id;
        args[0] = record_id;
        uid = db2.getRecord("app_main_id", where, args, "tbl_report_accepted_jobs").get(0);
        fill_details();
        fill_zonal();
        uploaded_attachments();
        statusDisplay();
        if (open.contentEquals("1")) {
            custom_dialog_page2();
        }
        unaccept=gds.nullCheck3(db2.getRecord("app_unacceptable_collateral",where,args,"tbl_report_accepted_jobs").get(0));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // TODO Auto-generated method stub
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        menu.findItem(R.id.map).setVisible(true);
        menu.findItem(R.id.logs).setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case R.id.logs:
                logsDisplay();
                return true;
            case R.id.map:
                view_map();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void fill_details() {
        List<Report_Accepted_Jobs> report_accepted_jobs = db
                .getReport_Accepted_Jobs(record_id);
        if (!report_accepted_jobs.isEmpty()) {
            for (Report_Accepted_Jobs im : report_accepted_jobs) {
                requested_month = im.getdr_month();
                requested_day = im.getdr_day();
                requested_year = im.getdr_year();
                classification = im.getclassification();
                account_fname = im.getfname();
                account_mname = im.getmname();
                account_lname = im.getlname();
                requesting_party = im.getrequesting_party();
                requestor = im.getrequestor();
                control_no = im.getcontrol_no();
                appraisal_type = im.getappraisal_type();
                nature_appraisal = im.getnature_appraisal();
                ins_month1 = im.getins_date1_month();
                ins_day1 = im.getins_date1_day();
                ins_year1 = im.getins_date1_year();
                ins_month2 = im.getins_date2_month();
                ins_day2 = im.getins_date2_day();
                ins_year2 = im.getins_date2_year();
                com_month = im.getcom_month();
                com_day = im.getcom_day();
                com_year = im.getcom_year();
                tct_no = im.gettct_no();
                unit_no = im.getunit_no();
                bldg_name = im.getbuilding_name();
                lot_no = im.getlot_no();
                block_no = im.getblock_no();
                street_no = im.getstreet_no();
                street_name = im.getstreet_name();
                village = im.getvillage();
                district = im.getdistrict();
                zip_code = im.getzip_code();
                city = im.getcity();
                province = im.getprovince();
                region = im.getregion();
                country = im.getcountry();
                counter = im.getcounter();
                app_request_remarks = im.getapp_request_remarks();
                app_kind_of_appraisal = im.getkind_of_appraisal();
                app_branch_code = im.getapp_branch_code();
                rework_reason = im.getrework_reason();
                args[0]=record_id;
                account_is_company=db2.getRecord("app_account_is_company",where,args,"tbl_report_accepted_jobs").get(0);
                account_company_name=db2.getRecord("app_account_company_name",where,args,"tbl_report_accepted_jobs").get(0);
            }

            args[0]=record_id;
            if(db2.getRecord("application_status",where,args,"tbl_report_accepted_jobs").get(0).contentEquals("for_rework")){
                tv_rework_reason_header.setVisibility(View.VISIBLE);
                tv_rework_reason.setVisibility(View.VISIBLE);
                tv_rework_reason.setText(rework_reason);
            }
            List<Report_Accepted_Jobs_Contacts> report_Accepted_Jobs_Contacts = db
                    .getReport_Accepted_Jobs_Contacts(record_id);
            if (!report_Accepted_Jobs_Contacts.isEmpty()) {
                for (Report_Accepted_Jobs_Contacts im : report_Accepted_Jobs_Contacts) {
                    LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
                            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View addView = layoutInflater.inflate(
                            R.layout.request_jobs_info_contact_layout, null);
                    final TextView tv_contact_person = (TextView) addView
                            .findViewById(R.id.tv_contact_person);
                    final TextView tv_contact_prefix = (TextView) addView
                            .findViewById(R.id.tv_contact_prefix);
                    final TextView tv_contact_mobile = (TextView) addView
                            .findViewById(R.id.tv_contact_mobile);
                    final TextView tv_contact_landline = (TextView) addView
                            .findViewById(R.id.tv_contact_landline);

                    tv_contact_person.setText(im.getcontact_person());
                    tv_contact_prefix.setText(im.getcontact_mobile_no_prefix());
                    tv_contact_mobile.setText(im.getcontact_mobile_no());
                    tv_contact_landline.setText(im.getcontact_landline());
                    ll_contact.addView(addView);
                }
            }
            List<Report_filename> rf = db.getReport_filename(record_id);
            if (!rf.isEmpty()) {
                for (Report_filename im : rf) {
                    tv_attachment.setText(im.getfilename());
                }
            }
            tv_requested_month.setText(requested_month + "/");
            tv_requested_day.setText(requested_day + "/");
            tv_requested_year.setText(requested_year);
            tv_classification.setText(classification);
            tv_account_name.setText(account_fname + " " + account_mname + " "
                    + account_lname);
            tv_company_name.setText(account_company_name);
            if(account_is_company.contentEquals("true")){
                actionBar.setTitle(account_company_name);
            }else{
                actionBar.setTitle(account_fname + " " + account_mname + " " + account_lname);
            }
            tv_requesting_party.setText(requesting_party);
            tv_requestor.setText(requestor);
            tv_control_no.setText(control_no);
            for (int x = 0; x < gs.appraisal_type_value.length; x++) {
                if (appraisal_type.equals(gs.appraisal_type_value[x])) {
                    output_appraisal_type = gs.appraisal_output[x];
                }
            }
            tv_appraisal_type.setText(output_appraisal_type);
            tv_nature_appraisal.setText(nature_appraisal);
            tv_ins_month1.setText(ins_month1 + "/");
            tv_ins_day1.setText(ins_day1 + "/");
            tv_ins_year1.setText(ins_year1);
            tv_ins_month2.setText(ins_month2 + "/");
            tv_ins_day2.setText(ins_day2 + "/");
            tv_ins_year2.setText(ins_year2);
            tv_com_month.setText(com_month + "/");
            tv_com_day.setText(com_day + "/");
            tv_com_year.setText(com_year);

            tv_tct_no.setText(tct_no);
            tv_unit_no.setText(unit_no);
            tv_bldg_name.setText(bldg_name);
            tv_street_no.setText(street_no);
            tv_street_name.setText(street_name);
            tv_village.setText(village);
            tv_district.setText(district);
            tv_zip_code.setText(zip_code);
            tv_city.setText(city);
            tv_province.setText(province);
            tv_region.setText(region);
            tv_country.setText(country);
            tv_app_request_remarks.setText(app_request_remarks);
            tv_app_kind_of_appraisal.setText(app_kind_of_appraisal);
            tv_app_branch_code.setText(app_branch_code);
            tv_address_hide.setText(street_name + " " + city + " " + region + " " + country);
            if (!street_name.equals("")) {
                output_street_name = street_name + ", ";
            } else {
                output_street_name = street_name;
            }
            if (!village.equals("")) {
                output_village = village + ", ";
            } else {
                output_village = village;
            }
            if (!city.equals("")) {
                output_city = city + ", ";
            } else {
                output_city = city;
            }
            if (!province.equals("")) {
                output_province = province + ", ";
            } else {
                output_province = province;
            }
            tv_address.setText(unit_no + " " + bldg_name + " " + lot_no + " " + block_no
                    + " " + output_street_name + output_village + district
                    + " " + output_city + region + " " + output_province
                    + country + " " + zip_code);
        }
    }

    public void fill_zonal() {
        List<Vacant_Lot_API> vl = db.getVacant_Lot(String.valueOf(record_id));
        if (!vl.isEmpty()) {
            for (Vacant_Lot_API im : vl) {
                tv_location.setText(im.getreport_zonal_location());
                tv_lot_classification.setText(im.getreport_zonal_lot_classification());
                tv_value_per_sqm.setText(gds.numberFormat(im.getreport_zonal_value()));
            }
        }
    }

    public void custom_dialog_page2() {
        //land details
        myDialog = new Dialog(Vacant_lot.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.report_vacant_lot_page1);
        myDialog.setCancelable(true);

        android.view.WindowManager.LayoutParams params = myDialog.getWindow()
                .getAttributes();
        params.height = LayoutParams.FILL_PARENT;
        params.width = LayoutParams.FILL_PARENT;
        myDialog.getWindow().setAttributes(
                (android.view.WindowManager.LayoutParams) params);

        btn_view_rdps = (Button) myDialog.findViewById(R.id.btn_view_rdps);
        btn_view_prev_appraisal = (Button) myDialog.findViewById(R.id.btn_view_prev_appraisal);
        report_date_inspected = (DatePicker) myDialog
                .findViewById(R.id.report_date_inspected);
        report_time_inspected = (EditText) myDialog
                .findViewById(R.id.report_time_inspected);
        report_homeowners_assoc = (CheckBox) myDialog.findViewById(R.id.report_homeowners_assoc);
        report_tax_mapping = (CheckBox) myDialog.findViewById(R.id.report_tax_mapping);
        report_lra_office = (CheckBox) myDialog.findViewById(R.id.report_lra_office);
        report_lot_config = (CheckBox) myDialog.findViewById(R.id.report_lot_config);
        report_subd_map = (CheckBox) myDialog.findViewById(R.id.report_subd_map);
        report_neighborhood_checking = (CheckBox) myDialog.findViewById(R.id.report_neighborhood_checking);
        report_street_name = (EditText) myDialog.findViewById(R.id.report_street_name);
        spinner_street = (Spinner) myDialog.findViewById(R.id.spinner_street);
        spinner_sidewalk = (Spinner) myDialog.findViewById(R.id.spinner_sidewalk);
        spinner_curb = (Spinner) myDialog.findViewById(R.id.spinner_curb);
        spinner_drainage = (Spinner) myDialog.findViewById(R.id.spinner_drainage);
        spinner_street_lights = (Spinner) myDialog.findViewById(R.id.spinner_street_lights);
        report_corner_lot = (CheckBox) myDialog.findViewById(R.id.report_corner_lot);
        report_non_corner_lot = (CheckBox) myDialog.findViewById(R.id.report_non_corner_lot);
        report_perimeter_lot = (CheckBox) myDialog.findViewById(R.id.report_perimeter_lot);
        report_intersected_lot = (CheckBox) myDialog.findViewById(R.id.report_intersected_lot);
        report_interior_with_row = (CheckBox) myDialog.findViewById(R.id.report_interior_with_row);
        report_landlocked = (CheckBox) myDialog.findViewById(R.id.report_landlocked);
        spinner_shape = (Spinner) myDialog.findViewById(R.id.spinner_shape);
        report_frontage = (EditText) myDialog.findViewById(R.id.report_frontage);
        report_depth = (EditText) myDialog.findViewById(R.id.report_depth);
        report_road = (EditText) myDialog.findViewById(R.id.report_road);
        spinner_elevation = (Spinner) myDialog.findViewById(R.id.spinner_elevation);
        spinner_terrain = (Spinner) myDialog.findViewById(R.id.spinner_terrain);
        spinner_per_tax_desc = (Spinner) myDialog.findViewById(R.id.spinner_per_tax_desc);
        spinner_actual_usage = (Spinner) myDialog.findViewById(R.id.spinner_actual_usage);
        spinner_neighborhood = (Spinner) myDialog.findViewById(R.id.spinner_neighborhood);
        spinner_highest_and_best_use = (Spinner) myDialog.findViewById(R.id.spinner_highest_and_best_use);
        report_landmark_1 = (EditText) myDialog.findViewById(R.id.report_landmark_1);
        report_landmark_2 = (EditText) myDialog.findViewById(R.id.report_landmark_2);
        report_landmark_3 = (EditText) myDialog.findViewById(R.id.report_landmark_3);
        report_landmark_4 = (EditText) myDialog.findViewById(R.id.report_landmark_4);
        report_landmark_5 = (EditText) myDialog.findViewById(R.id.report_landmark_5);
        report_distance_1 = (EditText) myDialog.findViewById(R.id.report_distance_1);
        report_distance_2 = (EditText) myDialog.findViewById(R.id.report_distance_2);
        report_distance_3 = (EditText) myDialog.findViewById(R.id.report_distance_3);
        report_distance_4 = (EditText) myDialog.findViewById(R.id.report_distance_4);
        report_distance_5 = (EditText) myDialog.findViewById(R.id.report_distance_5);
        report_electricity = (EditText) myDialog.findViewById(R.id.report_electricity);
        spinner_water = (Spinner) myDialog.findViewById(R.id.spinner_water);
        spinner_telephone = (Spinner) myDialog.findViewById(R.id.spinner_telephone);
        spinner_garbage = (Spinner) myDialog.findViewById(R.id.spinner_garbage);
        report_jeepneys = (CheckBox) myDialog.findViewById(R.id.report_jeepneys);
        report_bus = (CheckBox) myDialog.findViewById(R.id.report_bus);
        report_taxi = (CheckBox) myDialog.findViewById(R.id.report_taxi);
        report_tricycle = (CheckBox) myDialog.findViewById(R.id.report_tricycle);
        report_recreation_facilities = (CheckBox) myDialog.findViewById(R.id.report_recreation_facilities);
        report_security = (CheckBox) myDialog.findViewById(R.id.report_security);
        report_clubhouse = (CheckBox) myDialog.findViewById(R.id.report_clubhouse);
        report_swimming_pool = (CheckBox) myDialog.findViewById(R.id.report_swimming_pool);
        report_right = (EditText) myDialog.findViewById(R.id.report_right);
        report_left = (EditText) myDialog.findViewById(R.id.report_left);
        report_rear = (EditText) myDialog.findViewById(R.id.report_rear);
        report_front = (EditText) myDialog.findViewById(R.id.report_front);
        report_prev_date = (EditText) myDialog.findViewById(R.id.report_prev_date);
        report_prev_appraiser = (EditText) myDialog.findViewById(R.id.report_prev_appraiser);
        tv_prev_date = (TextView) myDialog.findViewById(R.id.tv_prev_date);
        tv_prev_appraiser = (TextView) myDialog.findViewById(R.id.tv_prev_appraiser);
        basis_identification = (TextView) myDialog.findViewById(R.id.basis_identification);
        facilities = (TextView) myDialog.findViewById(R.id.facilities);
        physical_characteristic = (TextView) myDialog.findViewById(R.id.physical_characteristic);

        valrep_land_id_others = (CheckBox) myDialog.findViewById(R.id.valrep_land_id_others);
        valrep_land_physical_others = (CheckBox) myDialog.findViewById(R.id.valrep_land_physical_others);
        valrep_land_id_others_desc = (EditText) myDialog.findViewById(R.id.valrep_land_id_others_desc);
        valrep_land_physical_others_desc = (EditText) myDialog.findViewById(R.id.valrep_land_physical_others_desc);

        valrep_land_road_right_of_way = (Spinner) myDialog.findViewById(R.id.valrep_land_road_right_of_way);



        args[0]=record_id;
        unaccept=gds.nullCheck3(db2.getRecord("app_unacceptable_collateral",where,args,"tbl_report_accepted_jobs").get(0));
        btn_update_page2 = (Button) myDialog
                .findViewById(R.id.btn_update_page2);
        btn_update_page2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                    // update
                    Vacant_Lot_API vl = new Vacant_Lot_API();
                    //date
                    if (String.valueOf(report_date_inspected.getMonth()).length() == 1) {//if month is 1 digit add 0 to initial +1 to value
                        if (String.valueOf(report_date_inspected.getMonth())
                                .equals("9")) {//if october==9 just add 1
                            vl.setreport_date_inspected_month(String
                                    .valueOf(report_date_inspected.getMonth() + 1));
                        } else {
                            vl.setreport_date_inspected_month("0"
                                    + String.valueOf(report_date_inspected
                                    .getMonth() + 1));
                        }
                    } else {
                        vl.setreport_date_inspected_month(String
                                .valueOf(report_date_inspected.getMonth() + 1));
                    }
                    vl.setreport_date_inspected_day(String.valueOf(report_date_inspected.getDayOfMonth()));
                    vl.setreport_date_inspected_year(String.valueOf(report_date_inspected.getYear()));
                    //valor
                    if (report_homeowners_assoc.isChecked()) {
                        vl.setreport_id_association("true");
                    } else {
                        vl.setreport_id_association("");
                    }
                    if (report_tax_mapping.isChecked()) {
                        vl.setreport_id_tax("true");
                    } else {
                        vl.setreport_id_tax("");
                    }
                    if (report_lra_office.isChecked()) {
                        vl.setreport_id_lra("true");
                    } else {
                        vl.setreport_id_lra("");
                    }
                    if (report_lot_config.isChecked()) {
                        vl.setreport_id_lot_config("true");
                    } else {
                        vl.setreport_id_lot_config("");
                    }
                    if (report_subd_map.isChecked()) {
                        vl.setreport_id_subd_map("true");
                    } else {
                        vl.setreport_id_subd_map("");
                    }
                    if (report_neighborhood_checking.isChecked()) {
                        vl.setreport_id_nbrhood_checking("true");
                    } else {
                        vl.setreport_id_nbrhood_checking("");
                    }

                    vl.setreport_imp_street_name(report_street_name.getText().toString());
                    vl.setreport_imp_street(spinner_street.getSelectedItem().toString());
                    vl.setreport_imp_sidewalk(spinner_sidewalk.getSelectedItem().toString());
                    vl.setreport_imp_curb(spinner_curb.getSelectedItem().toString());
                    vl.setreport_imp_drainage(spinner_drainage.getSelectedItem().toString());
                    vl.setreport_imp_street_lights(spinner_street_lights.getSelectedItem().toString());

                    if (report_corner_lot.isChecked()) {
                        vl.setreport_physical_corner_lot("true");
                    } else {
                        vl.setreport_physical_corner_lot("");
                    }
                    if (report_non_corner_lot.isChecked()) {
                        vl.setreport_physical_non_corner_lot("true");
                    } else {
                        vl.setreport_physical_non_corner_lot("");
                    }
                    if (report_perimeter_lot.isChecked()) {
                        vl.setreport_physical_perimeter_lot("true");
                    } else {
                        vl.setreport_physical_perimeter_lot("");
                    }
                    if (report_intersected_lot.isChecked()) {
                        vl.setreport_physical_intersected_lot("true");
                    } else {
                        vl.setreport_physical_intersected_lot("");
                    }
                    if (report_interior_with_row.isChecked()) {
                        vl.setreport_physical_interior_with_row("true");
                    } else {
                        vl.setreport_physical_interior_with_row("");
                    }
                    if (report_landlocked.isChecked()) {
                        vl.setreport_physical_landlocked("true");
                    } else {
                        vl.setreport_physical_landlocked("");
                    }


                    vl.setreport_lotclass_per_tax_dec(spinner_per_tax_desc.getSelectedItem().toString());
                    vl.setreport_lotclass_actual_usage(spinner_actual_usage.getSelectedItem().toString());
                    vl.setreport_lotclass_neighborhood(spinner_neighborhood.getSelectedItem().toString());
                    vl.setreport_lotclass_highest_best_use(spinner_highest_and_best_use.getSelectedItem().toString());
                    vl.setreport_physical_shape(spinner_shape.getSelectedItem().toString());
                    vl.setreport_physical_frontage(report_frontage.getText().toString());
                    vl.setreport_physical_depth(report_depth.getText().toString());
                    vl.setreport_physical_road(report_road.getText().toString());
                    vl.setreport_physical_elevation(spinner_elevation.getSelectedItem().toString());
                    vl.setreport_physical_terrain(spinner_terrain.getSelectedItem().toString());
                    vl.setreport_landmark_1(report_landmark_1.getText().toString());
                    vl.setreport_landmark_2(report_landmark_2.getText().toString());
                    vl.setreport_landmark_3(report_landmark_3.getText().toString());
                    vl.setreport_landmark_4(report_landmark_4.getText().toString());
                    vl.setreport_landmark_5(report_landmark_5.getText().toString());
                    vl.setreport_distance_1(report_distance_1.getText().toString());
                    vl.setreport_distance_2(report_distance_2.getText().toString());
                    vl.setreport_distance_3(report_distance_3.getText().toString());
                    vl.setreport_distance_4(report_distance_4.getText().toString());
                    vl.setreport_distance_5(report_distance_5.getText().toString());
                    vl.setreport_util_electricity(report_electricity.getText().toString());
                    vl.setreport_util_water(spinner_water.getSelectedItem().toString());
                    vl.setreport_util_telephone(spinner_telephone.getSelectedItem().toString());
                    vl.setreport_util_garbage(spinner_garbage.getSelectedItem().toString());

                    if (report_jeepneys.isChecked()) {
                        vl.setreport_trans_jeep("true");
                    } else {
                        vl.setreport_trans_jeep("");
                    }
                    if (report_bus.isChecked()) {
                        vl.setreport_trans_bus("true");
                    } else {
                        vl.setreport_trans_bus("");
                    }
                    if (report_taxi.isChecked()) {
                        vl.setreport_trans_taxi("true");
                    } else {
                        vl.setreport_trans_taxi("");
                    }
                    if (report_tricycle.isChecked()) {
                        vl.setreport_trans_tricycle("true");
                    } else {
                        vl.setreport_trans_tricycle("");
                    }
                    if (report_recreation_facilities.isChecked()) {
                        vl.setreport_faci_recreation("true");
                    } else {
                        vl.setreport_faci_recreation("");
                    }
                    if (report_security.isChecked()) {
                        vl.setreport_faci_security("true");
                    } else {
                        vl.setreport_faci_security("");
                    }
                    if (report_clubhouse.isChecked()) {
                        vl.setreport_faci_clubhouse("true");
                    } else {
                        vl.setreport_faci_clubhouse("");
                    }
                    if (report_swimming_pool.isChecked()) {
                        vl.setreport_faci_pool("true");
                    } else {
                        vl.setreport_faci_pool("");
                    }

                    vl.setreport_bound_right(report_right.getText().toString());
                    vl.setreport_bound_left(report_left.getText().toString());
                    vl.setreport_bound_rear(report_rear.getText().toString());
                    vl.setreport_bound_front(report_front.getText().toString());
                    vl.setreport_prev_date(report_prev_date.getText().toString());
                    vl.setreport_prev_appraiser(report_prev_appraiser.getText().toString());

                    vl.setreport_time_inspected(report_time_inspected.getText().toString());



                    db.updateVacant_Lot_page2(vl, record_id);

                    field.clear();
                    field.add("valrep_land_id_others");
                    field.add("valrep_land_physical_others");
                    field.add("valrep_land_id_others_desc");
                    field.add("valrep_land_physical_others_desc");
                    field.add("valrep_land_road_right_of_way");
                    value.clear();
                    value.add(gds.cbChecker(valrep_land_id_others));
                    value.add(gds.cbChecker(valrep_land_physical_others));
                    value.add(valrep_land_id_others_desc.getText().toString());
                    value.add(valrep_land_physical_others_desc.getText().toString());
                    value.add(valrep_land_road_right_of_way.getSelectedItem().toString());
                    db2.updateRecord(value,field,"record_id = ? ", new String[] { record_id },"vacant_lot");
                    db.close();
                    Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
                    myDialog.dismiss();

            }
        });
        List<Vacant_Lot_API> vl = db.getVacant_Lot(String.valueOf(record_id));
        if (!vl.isEmpty()) {
            for (Vacant_Lot_API im : vl) {
                //date of inspection
                // set current date into datepicker
                if ((!im.getreport_date_inspected_year().equals("")) && (!im.getreport_date_inspected_month().equals(""))) {
                    report_date_inspected.init(
                            Integer.parseInt(im.getreport_date_inspected_year()),
                            Integer.parseInt(im.getreport_date_inspected_month()) - 1,
                            Integer.parseInt(im.getreport_date_inspected_day()),
                            null);
                } else {
                    Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR);
                    int mMonth = c.get(Calendar.MONTH);
                    int mDay = c.get(Calendar.DAY_OF_MONTH);
                    report_date_inspected.init(mYear, mMonth, mDay, null);
                }
                //basis of identification
                if (im.getreport_id_association().equals("true")) {
                    report_homeowners_assoc.setChecked(true);
                }
                if (im.getreport_id_tax().equals("true")) {
                    report_tax_mapping.setChecked(true);
                }

                if (im.getreport_id_lra().equals("true")) {
                    report_lra_office.setChecked(true);
                }

                if (im.getreport_id_lot_config().equals("true")) {
                    report_lot_config.setChecked(true);
                }

                if (im.getreport_id_subd_map().equals("true")) {
                    report_subd_map.setChecked(true);
                }

                if (im.getreport_id_nbrhood_checking().equals("true")) {
                    report_neighborhood_checking.setChecked(true);
                }
                gds.cbDisplay(valrep_land_id_others, db2.getRecord("valrep_land_id_others", where, args, "vacant_lot").get(0));
                valrep_land_id_others_desc.setText(db2.getRecord("valrep_land_id_others_desc", where, args, "vacant_lot").get(0));
                valrep_land_road_right_of_way.setSelection(gds.spinnervalue(valrep_land_road_right_of_way, db2.getRecord("valrep_land_road_right_of_way", where, args, "vacant_lot").get(0)));

                //site imp
                //report_street_name.setText(im.getreport_imp_street_name());

                //for streetname ADDED BY IAN
                List<Report_Accepted_Jobs> li2 = db.getReport_Accepted_Jobs(String.valueOf(record_id));
                if (!li2.isEmpty()) {
                    for (Report_Accepted_Jobs im2 : li2) {

                        report_street_name.setText(im2.getstreet_name());

                    }
                }

                //END for streetname ADDED BY IAN
                spinner_street.setSelection(gds.spinnervalue(spinner_street, im.getreport_imp_street()));
                spinner_sidewalk.setSelection(gds.spinnervalue(spinner_sidewalk, im.getreport_imp_sidewalk()));
                spinner_curb.setSelection(gds.spinnervalue(spinner_curb, im.getreport_imp_curb()));
                spinner_drainage.setSelection(gds.spinnervalue(spinner_drainage, im.getreport_imp_drainage()));
                spinner_street_lights.setSelection(gds.spinnervalue(spinner_street_lights, im.getreport_imp_street_lights()));
//                spinner_street_lights.setSelection(gds.spinnervalue(spinner_street_lights, im.getreport_imp_street_lights()));


                //physical char
                if (im.getreport_physical_corner_lot().equals("true")) {
                    report_corner_lot.setChecked(true);
                }
                if (im.getreport_physical_non_corner_lot().equals("true")) {
                    report_non_corner_lot.setChecked(true);
                }
                if (im.getreport_physical_perimeter_lot().equals("true")) {
                    report_perimeter_lot.setChecked(true);
                }
                if (im.getreport_physical_intersected_lot().equals("true")) {
                    report_intersected_lot.setChecked(true);
                }
                if (im.getreport_physical_interior_with_row().equals("true")) {
                    report_interior_with_row.setChecked(true);
                }
                if (im.getreport_physical_landlocked().equals("true")) {
                    report_landlocked.setChecked(true);
                }
                gds.cbDisplay(valrep_land_physical_others, db2.getRecord("valrep_land_physical_others", where, args, "vacant_lot").get(0));
                valrep_land_physical_others_desc.setText(db2.getRecord("valrep_land_physical_others_desc", where, args, "vacant_lot").get(0));

                spinner_shape.setSelection(gds.spinnervalue(spinner_shape, im.getreport_physical_shape()));


                report_frontage.setText(im.getreport_physical_frontage());
                report_depth.setText(im.getreport_physical_depth());
                report_road.setText(im.getreport_physical_road());
                spinner_elevation.setSelection(gds.spinnervalue(spinner_elevation, im.getreport_physical_elevation()));
                spinner_terrain.setSelection(gds.spinnervalue(spinner_terrain, im.getreport_physical_terrain()));
                spinner_per_tax_desc.setSelection(gds.spinnervalue(spinner_per_tax_desc, im.getreport_lotclass_per_tax_dec()));
                spinner_actual_usage.setSelection(gds.spinnervalue(spinner_actual_usage, im.getreport_lotclass_actual_usage()));
                spinner_neighborhood.setSelection(gds.spinnervalue(spinner_neighborhood, im.getreport_lotclass_neighborhood()));
                spinner_highest_and_best_use.setSelection(gds.spinnervalue(spinner_highest_and_best_use, im.getreport_lotclass_highest_best_use()));

                //notable landmarks
                report_landmark_1.setText(im.getreport_landmark_1());
                report_distance_1.setText(im.getreport_distance_1());
                report_landmark_2.setText(im.getreport_landmark_2());
                report_distance_2.setText(im.getreport_distance_2());
                report_landmark_3.setText(im.getreport_landmark_3());
                report_distance_3.setText(im.getreport_distance_3());
                report_landmark_4.setText(im.getreport_landmark_4());
                report_distance_4.setText(im.getreport_distance_4());
                report_landmark_5.setText(im.getreport_landmark_5());
                report_distance_5.setText(im.getreport_distance_5());
                //utilities
                report_electricity.setText(im.getreport_util_electricity());
                spinner_water.setSelection(gds.spinnervalue(spinner_water, im.getreport_util_water()));
                spinner_telephone.setSelection(gds.spinnervalue(spinner_telephone, im.getreport_util_telephone()));
                spinner_garbage.setSelection(gds.spinnervalue(spinner_garbage, im.getreport_util_garbage()));


                //transportation
                if (im.getreport_trans_jeep().equals("true")) {
                    report_jeepneys.setChecked(true);
                }
                if (im.getreport_trans_bus().equals("true")) {
                    report_bus.setChecked(true);
                }
                if (im.getreport_trans_taxi().equals("true")) {
                    report_taxi.setChecked(true);
                }
                if (im.getreport_trans_tricycle().equals("true")) {
                    report_tricycle.setChecked(true);
                }
                //facilities
                if (im.getreport_faci_recreation().equals("true")) {
                    report_recreation_facilities.setChecked(true);
                }
                if (im.getreport_faci_security().equals("true")) {
                    report_security.setChecked(true);
                }
                if (im.getreport_faci_clubhouse().equals("true")) {
                    report_clubhouse.setChecked(true);
                }
                if (im.getreport_faci_pool().equals("true")) {
                    report_swimming_pool.setChecked(true);
                }
                //boundaries
                report_right.setText(im.getreport_bound_right());
                report_left.setText(im.getreport_bound_left());
                report_rear.setText(im.getreport_bound_rear());
                report_front.setText(im.getreport_bound_front());

                //prev Appraisal
                report_prev_date.setText(im.getreport_prev_date());
                report_prev_appraiser.setText(im.getreport_prev_appraiser());
                report_time_inspected.setText(im.getreport_time_inspected());
            }
        }

        // Button View RPDS
        btn_view_rdps.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                btn_update_page2.performClick();//auto-save

                    finish();
                    Intent in = new Intent(getApplicationContext(),
                            Vacant_Lot_RDPS.class);
                    // sending record_id to next activity
                    in.putExtra(TAG_RECORD_ID, record_id);
                    // starting new activity and expecting some response back
                    startActivityForResult(in, 100);
                    myDialog.dismiss();

            }
        });

// Button View Prev Appraisal
        btn_view_prev_appraisal.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                btn_update_page2.performClick();//auto-save

                    finish();
                    Intent in = new Intent(getApplicationContext(),
                            Vacant_Lot_Prev_Appraisal.class);
                    // sending record_id to next activity
                    in.putExtra(TAG_RECORD_ID, record_id);
                    // starting new activity and expecting some response back
                    startActivityForResult(in, 100);
                    myDialog.dismiss();

            }
        });

        //set required views
        nature_appraisal_land_d = db2.getRecord("nature_appraisal", where, args, "tbl_report_accepted_jobs").get(0);

        if (nature_appraisal_land_d.contentEquals("Re-appraisal")) {
            gds.fill_in_error(new EditText[]{report_prev_date,report_prev_appraiser});
        }
        gds.fill_in_error(new EditText[]{report_electricity,
                report_street_name,
                report_landmark_1,
                report_distance_1,
                report_right,
                report_left,
                report_rear,
                report_front,});
        gds.fill_in_error_spinner(new Spinner[]{
                valrep_land_road_right_of_way,
                spinner_street,
                spinner_curb,
                spinner_sidewalk,
                spinner_drainage,
                spinner_street_lights,
                spinner_per_tax_desc,
                spinner_actual_usage,
                spinner_neighborhood,
                spinner_highest_and_best_use,
                spinner_water,
                spinner_telephone,
                spinner_garbage,
        });
        if (report_homeowners_assoc.isChecked() || report_tax_mapping.isChecked() ||
                report_lra_office.isChecked() || report_lot_config.isChecked() ||
                report_subd_map.isChecked() || report_neighborhood_checking.isChecked() || valrep_land_id_others.isChecked()) {
//
            basis_identification.setText("Basis of Identification");
            basis_identification.setTextColor(Color.BLACK);
        } else {
//
            basis_identification.setText("Basis of Identification: Please select at least one");
            basis_identification.setTextColor(Color.RED);

        }
        if (valrep_land_id_others.isChecked()) {
            gds.fill_in_error(new EditText[]{valrep_land_id_others_desc});
//
        } else {
//
        }
        if (report_corner_lot.isChecked() || report_non_corner_lot.isChecked() ||
                report_perimeter_lot.isChecked() || report_intersected_lot.isChecked() ||
                report_interior_with_row.isChecked() || report_landlocked.isChecked() || valrep_land_physical_others.isChecked()) {
//
            physical_characteristic.setText("Physical Characteristics");
            physical_characteristic.setTextColor(Color.BLACK);
        } else {
//
            physical_characteristic.setText("Physical Characteristics: Please select at least one");
            physical_characteristic.setTextColor(Color.RED);

        }


        if (valrep_land_physical_others.isChecked()) {
            gds.fill_in_error(new EditText[]{valrep_land_physical_others_desc});
//
        } else {
//
        }

        myDialog.show();
    }

    public void custom_dialog_summary() {
        args[0]=record_id;
        myDialog = new Dialog(Vacant_lot.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.reports_land_summary);
        // myDialog.setTitle("My Dialog");
        myDialog.setCancelable(true);

        android.view.WindowManager.LayoutParams params = myDialog.getWindow()
                .getAttributes();
        params.height = LayoutParams.FILL_PARENT;
        params.width = LayoutParams.FILL_PARENT;
        myDialog.getWindow().setAttributes(
                (android.view.WindowManager.LayoutParams) params);


        report_final_value_total_appraised_value_land_imp = (EditText) myDialog.findViewById(R.id.report_final_value_total_appraised_value_land_imp);
        report_final_value_recommended_deductions_desc = (Spinner) myDialog.findViewById(R.id.report_final_value_recommended_deductions_desc);

        report_final_value_recommended_deductions_rate = (EditText) myDialog.findViewById(R.id.report_final_value_recommended_deductions_rate);
        report_final_value_recommended_deductions_other = (EditText) myDialog.findViewById(R.id.report_final_value_recommended_deductions_other);
        report_final_value_recommended_deductions_amt = (EditText) myDialog.findViewById(R.id.report_final_value_recommended_deductions_amt);
        report_final_value_net_appraised_value = (EditText) myDialog.findViewById(R.id.report_final_value_net_appraised_value);
        report_final_value_quick_sale_value_rate = (EditText) myDialog.findViewById(R.id.report_final_value_quick_sale_value_rate);
        report_final_value_quick_sale_value_amt = (EditText) myDialog.findViewById(R.id.report_final_value_quick_sale_value_amt);

        app_unacceptable_collateral = (CheckBox) myDialog.findViewById(R.id.app_unacceptable_collateral);


        tv_appraised_value = (TextView) myDialog.findViewById(R.id.tv_appraised_value);

        tv_appraised_value.setText("Total Appraised Value");


        report_remarks = (EditText) myDialog.findViewById(R.id.report_remarks);
        tv_remarks = (TextView) myDialog.findViewById(R.id.tv_remarks);
        tv_remarks.setOnClickListener(this);


        concerns_minor_1 = (CheckBox) myDialog.findViewById(R.id.concerns_minor_1);
        concerns_minor_2 = (CheckBox) myDialog.findViewById(R.id.concerns_minor_2);
        concerns_minor_3 = (CheckBox) myDialog.findViewById(R.id.concerns_minor_3);
        concerns_minor_others = (CheckBox) myDialog.findViewById(R.id.concerns_minor_others);
        concerns_minor_others_desc = (EditText) myDialog.findViewById(R.id.concerns_minor_others_desc);
        concerns_major_1 = (CheckBox) myDialog.findViewById(R.id.concerns_major_1);
        concerns_major_2 = (CheckBox) myDialog.findViewById(R.id.concerns_major_2);
        concerns_major_3 = (CheckBox) myDialog.findViewById(R.id.concerns_major_3);
        concerns_major_4 = (CheckBox) myDialog.findViewById(R.id.concerns_major_4);
        concerns_major_5 = (CheckBox) myDialog.findViewById(R.id.concerns_major_5);
        concerns_major_6 = (CheckBox) myDialog.findViewById(R.id.concerns_major_6);
        concerns_major_others = (CheckBox) myDialog.findViewById(R.id.concerns_major_others);
        concerns_major_others_desc = (EditText) myDialog.findViewById(R.id.concerns_major_others_desc);

        concerns_minor_others.setOnCheckedChangeListener(this);
        concerns_major_others.setOnCheckedChangeListener(this);
        concerns_minor_others_desc.setVisibility(View.GONE);
        concerns_major_others_desc.setVisibility(View.GONE);


        btn_update_summary = (Button) myDialog.findViewById(R.id.btn_update_summary);


        report_final_value_recommended_deductions_rate.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                String total_appraised_value_land_imp_s;
                if (report_final_value_total_appraised_value_land_imp.getText().toString().equals("")) {
                    total_appraised_value_land_imp_s = "0.00";
                } else {
                    total_appraised_value_land_imp_s = gds.nullCheck2(report_final_value_total_appraised_value_land_imp.getText().toString());
                }
                String recommended_deductions_rate_d = gds.nullCheck2(report_final_value_recommended_deductions_rate.getText().toString());

                //recommended_deductions_amt = total_appraised_value_land_imp x .recommended_deductions_rate
                String recommended_deductions_amt_d = gds.deducAmtVL(total_appraised_value_land_imp_s, recommended_deductions_rate_d);
                report_final_value_recommended_deductions_amt.setText(recommended_deductions_amt_d);

                //net_appraised_value = total_appraised_value_land_imp-recommended_deductions_amt;

                String net_appraised_value_d = gds.netAppValVL(total_appraised_value_land_imp_s, recommended_deductions_amt_d);
                report_final_value_net_appraised_value.setText(net_appraised_value_d);

                String quick_sale_value_rate_d = gds.nullCheck2(report_final_value_quick_sale_value_rate.getText().toString());

                //quick_sale_value_amt = net_appraised_value*.quick_sale_value_rate
                String quick_sale_value_amt_d = gds.deducAmtVL(net_appraised_value_d, quick_sale_value_rate_d);
                report_final_value_quick_sale_value_amt.setText(quick_sale_value_amt_d);


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_final_value_quick_sale_value_rate.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                summary_textwatch();


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        report_final_value_total_appraised_value_land_imp.addTextChangedListener(new TextWatcher() {

            String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(report_final_value_total_appraised_value_land_imp, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });
        report_final_value_recommended_deductions_amt.addTextChangedListener(new TextWatcher() {

            String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(report_final_value_recommended_deductions_amt, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });
        report_final_value_net_appraised_value.addTextChangedListener(new TextWatcher() {

            String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(report_final_value_net_appraised_value, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });

        if(app_unacceptable_collateral.isChecked()){
            report_final_value_total_appraised_value_land_imp.setText("0");
            report_final_value_net_appraised_value.setText("0");
        }else{
            report_final_value_total_appraised_value_land_imp.setText(
                    db2.getRecord("report_final_value_total_appraised_value_land_imp",where,args,"vacant_lot").get(0));
            report_final_value_net_appraised_value.setText(
                    db2.getRecord("report_final_value_net_appraised_value",where,args,"vacant_lot").get(0));
            summary_textwatch();
        }
        app_unacceptable_collateral.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    report_final_value_total_appraised_value_land_imp.setText("0");
                    report_final_value_net_appraised_value.setText("0");
                }else{
                    report_final_value_total_appraised_value_land_imp.setText(
                            db2.getRecord("report_final_value_total_appraised_value_land_imp",where,args,"vacant_lot").get(0));
                    report_final_value_net_appraised_value.setText(
                            db2.getRecord("report_final_value_net_appraised_value",where,args,"vacant_lot").get(0));
                    summary_textwatch();
                }
            }
        });

        btn_update_summary.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method

                if (report_final_value_recommended_deductions_other.getText().length() == 0) {
                    report_final_value_recommended_deductions_other.setText("None");
                }

                if (report_final_value_quick_sale_value_rate.getText().length() == 0) {
                    report_final_value_quick_sale_value_rate.setText("0");
                }
                if (report_final_value_quick_sale_value_amt.getText().length() == 0) {
                    report_final_value_quick_sale_value_amt.setText("0.00");
                }

                    Vacant_Lot_API vl = new Vacant_Lot_API();
                    vl.setreport_final_value_total_appraised_value_land_imp(gds.replaceFormat(report_final_value_total_appraised_value_land_imp
                            .getText().toString()));
                    vl.setreport_final_value_recommended_deductions_desc(report_final_value_recommended_deductions_desc
                            .getSelectedItem().toString());
                    vl.setreport_final_value_recommended_deductions_rate(gds.nullCheck2(report_final_value_recommended_deductions_rate
                            .getText().toString()));
                    vl.setreport_final_value_recommended_deductions_other(report_final_value_recommended_deductions_other
                            .getText().toString());
                    vl.setreport_final_value_recommended_deductions_amt(gds.replaceFormat(report_final_value_recommended_deductions_amt
                            .getText().toString()));
                    vl.setreport_final_value_net_appraised_value(gds.replaceFormat(report_final_value_net_appraised_value
                            .getText().toString()));
                    vl.setreport_final_value_quick_sale_value_rate(report_final_value_quick_sale_value_rate
                            .getText().toString());
                    vl.setreport_final_value_quick_sale_value_amt(report_final_value_quick_sale_value_amt
                            .getText().toString());
                    vl.setreport_factors_of_concern("0");
                    vl.setreport_suggested_corrective_actions("0");
                    vl.setreport_remarks(report_remarks.getText().toString());
                    vl.setreport_requirements("0");

                    if (concerns_minor_1.isChecked()) {
                        vl.setreport_concerns_minor_1("true");
                    } else {
                        vl.setreport_concerns_minor_1("");
                    }
                    if (concerns_minor_2.isChecked()) {
                        vl.setreport_concerns_minor_2("true");
                    } else {
                        vl.setreport_concerns_minor_2("");
                    }
                    if (concerns_minor_3.isChecked()) {
                        vl.setreport_concerns_minor_3("true");
                    } else {
                        vl.setreport_concerns_minor_3("");
                    }
                    if (concerns_minor_others.isChecked()) {
                        vl.setreport_concerns_minor_others("true");
                    } else {
                        vl.setreport_concerns_minor_others("");
                    }
                    vl.setreport_concerns_minor_others_desc(concerns_minor_others_desc.getText()
                            .toString());

                    if (concerns_major_1.isChecked()) {
                        vl.setreport_concerns_major_1("true");
                    } else {
                        vl.setreport_concerns_major_1("");
                    }
                    if (concerns_major_2.isChecked()) {
                        vl.setreport_concerns_major_2("true");
                    } else {
                        vl.setreport_concerns_major_2("");
                    }
                    if (concerns_major_3.isChecked()) {
                        vl.setreport_concerns_major_3("true");
                    } else {
                        vl.setreport_concerns_major_3("");
                    }
                    if (concerns_major_4.isChecked()) {
                        vl.setreport_concerns_major_4("true");
                    } else {
                        vl.setreport_concerns_major_4("");
                    }
                    if (concerns_major_5.isChecked()) {
                        vl.setreport_concerns_major_5("true");
                    } else {
                        vl.setreport_concerns_major_5("");
                    }
                    if (concerns_major_6.isChecked()) {
                        vl.setreport_concerns_major_6("true");
                    } else {
                        vl.setreport_concerns_major_6("");
                    }
                    if (concerns_major_others.isChecked()) {
                        vl.setreport_concerns_major_others("true");
                    } else {
                        vl.setreport_concerns_major_others("");
                    }
                    vl.setreport_concerns_major_others_desc(concerns_major_others_desc.getText()
                            .toString());

                    db.updateVacant_Lot_Summary(vl, record_id);
                    field.clear();
                    field.add("app_unacceptable_collateral");
                    value.clear();
                    value.add(gds.cbChecker(app_unacceptable_collateral));
                    db2.updateRecord(value,field,"record_id = ?",new String[] { record_id },"tbl_report_accepted_jobs");
                    db.close();
                    Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
                    myDialog.dismiss();


            }
        });

        List<Vacant_Lot_API> vl = db.getVacant_Lot(String.valueOf(record_id));
        if (!vl.isEmpty()) {
            for (Vacant_Lot_API im : vl) {
                report_final_value_total_appraised_value_land_imp.setText(gds.numberFormat(im.getreport_final_value_total_appraised_value_land_imp()));

                report_final_value_recommended_deductions_desc.setSelection(gds.spinnervalue(report_final_value_recommended_deductions_desc, im.getreport_final_value_recommended_deductions_desc()));


                report_final_value_recommended_deductions_rate.setText(gds.nullCheck2(im.getreport_final_value_recommended_deductions_rate()));
                report_final_value_recommended_deductions_other.setText(im.getreport_final_value_recommended_deductions_other());
                report_final_value_recommended_deductions_amt.setText(gds.numberFormat(im.getreport_final_value_recommended_deductions_amt()));
                report_final_value_net_appraised_value.setText(gds.numberFormat(im.getreport_final_value_net_appraised_value()));
                report_final_value_quick_sale_value_rate.setText(im.getreport_final_value_quick_sale_value_rate());
                report_final_value_quick_sale_value_amt.setText(im.getreport_final_value_quick_sale_value_amt());
                args[0]=record_id;
                gds.cbDisplay(app_unacceptable_collateral,db2.getRecord("app_unacceptable_collateral",where,args,"tbl_report_accepted_jobs").get(0));



                report_remarks.setText(im.getreport_remarks());

                if (im.getreport_concerns_minor_1().equals("true")) {
                    concerns_minor_1.setChecked(true);
                }
                if (im.getreport_concerns_minor_2().equals("true")) {
                    concerns_minor_2.setChecked(true);
                }
                if (im.getreport_concerns_minor_3().equals("true")) {
                    concerns_minor_3.setChecked(true);
                }
                if (im.getreport_concerns_minor_others().equals("true")) {
                    concerns_minor_others.setChecked(true);
                }
                concerns_minor_others_desc.setText(im.getreport_concerns_minor_others_desc());

                if (im.getreport_concerns_major_1().equals("true")) {
                    concerns_major_1.setChecked(true);
                }
                if (im.getreport_concerns_major_2().equals("true")) {
                    concerns_major_2.setChecked(true);
                }
                if (im.getreport_concerns_major_3().equals("true")) {
                    concerns_major_3.setChecked(true);
                }
                if (im.getreport_concerns_major_4().equals("true")) {
                    concerns_major_4.setChecked(true);
                }
                if (im.getreport_concerns_major_5().equals("true")) {
                    concerns_major_5.setChecked(true);
                }
                if (im.getreport_concerns_major_6().equals("true")) {
                    concerns_major_6.setChecked(true);
                }
                if (im.getreport_concerns_major_others().equals("true")) {
                    concerns_major_others.setChecked(true);
                }
                concerns_major_others_desc.setText(im.getreport_concerns_major_others_desc());

            }
        }

//set required views
        boolean fieldsOK = new Fields_Checker().checker(
                new EditText[]{
                        report_remarks,
                },
                new AutoCompleteTextView[]{},
                new Spinner[]{}
        );


        myDialog.show();

    }
    public void summary_textwatch(){
        String total_appraised_value_land_imp_s;
        if (report_final_value_total_appraised_value_land_imp.getText().toString().equals("")) {
            total_appraised_value_land_imp_s = "0.00";
        } else {
            total_appraised_value_land_imp_s = gds.nullCheck2(report_final_value_total_appraised_value_land_imp.getText().toString());
        }
        String recommended_deductions_rate_d = gds.nullCheck2(report_final_value_recommended_deductions_rate.getText().toString());

        //recommended_deductions_amt = total_appraised_value_land_imp x .recommended_deductions_rate
        String recommended_deductions_amt_d = gds.deducAmtVL(total_appraised_value_land_imp_s, recommended_deductions_rate_d);
        report_final_value_recommended_deductions_amt.setText(recommended_deductions_amt_d);

        //net_appraised_value = total_appraised_value_land_imp-recommended_deductions_amt;

        String net_appraised_value_d = gds.netAppValVL(total_appraised_value_land_imp_s, recommended_deductions_amt_d);
        report_final_value_net_appraised_value.setText(net_appraised_value_d);

        String quick_sale_value_rate_d = gds.nullCheck2(report_final_value_quick_sale_value_rate.getText().toString());

        //quick_sale_value_amt = net_appraised_value*.quick_sale_value_rate
        String quick_sale_value_amt_d = gds.deducAmtVL(net_appraised_value_d, quick_sale_value_rate_d);
        report_final_value_quick_sale_value_amt.setText(quick_sale_value_amt_d);
    }
    public void custom_dialog_address() {
        myDialog = new Dialog(Vacant_lot.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.address_dialog);
        myDialog.setCancelable(true);

        android.view.WindowManager.LayoutParams params = myDialog.getWindow()
                .getAttributes();
        params.height = LayoutParams.WRAP_CONTENT;
        params.width = LayoutParams.FILL_PARENT;
        myDialog.getWindow().setAttributes(
                (android.view.WindowManager.LayoutParams) params);
        et_unit_no = (EditText) myDialog.findViewById(R.id.et_unit_no);
        et_building_name = (EditText) myDialog.findViewById(R.id.et_building_name);
        et_lot_no = (EditText) myDialog.findViewById(R.id.et_lot_no);
        et_block_no = (EditText) myDialog.findViewById(R.id.et_block_no);
        et_street_no = (EditText) myDialog.findViewById(R.id.et_street_no);
        et_street_name = (EditText) myDialog.findViewById(R.id.et_street_name);
        et_village = (EditText) myDialog.findViewById(R.id.et_village);
        et_district = (EditText) myDialog.findViewById(R.id.et_district);
        et_zip_code = (EditText) myDialog.findViewById(R.id.et_zip_code);
        et_city = (EditText) myDialog.findViewById(R.id.et_city);
        et_province = (EditText) myDialog.findViewById(R.id.et_province);
        et_region = (EditText) myDialog.findViewById(R.id.et_region);
        et_country = (EditText) myDialog.findViewById(R.id.et_country);

        btn_save_address = (Button) myDialog.findViewById(R.id.btn_save_address);
        btn_save_address.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method
                Report_Accepted_Jobs li = new Report_Accepted_Jobs();
                li.setunit_no(et_unit_no.getText().toString());
                li.setbuilding_name(et_building_name.getText().toString());
                li.setlot_no(et_lot_no.getText().toString());
                li.setblock_no(et_block_no.getText().toString());
                li.setstreet_no(et_street_no.getText().toString());
                li.setstreet_name(et_street_name.getText().toString());
                li.setvillage(et_village.getText().toString());
                li.setdistrict(et_district.getText().toString());
                li.setzip_code(et_zip_code.getText().toString());
                li.setcity(et_city.getText().toString());
                li.setprovince(et_province.getText().toString());
                li.setregion(et_region.getText().toString());
                li.setcountry(et_country.getText().toString());

                db.updateCollateral_Address(li, record_id);
                //ADDED BY IAN
                Vacant_Lot_API vl2 = new Vacant_Lot_API();
                vl2.setreport_imp_street_name(et_street_name.getText().toString());
                db.updateVacant_Lot_page2_streetName(vl2, record_id);
                //END OF ADDED BY IAN
                db.close();
                Toast.makeText(getApplicationContext(), "Saved",
                        Toast.LENGTH_SHORT).show();
                myDialog.dismiss();
                Intent intent = getIntent();
                intent.putExtra("keyopen", "0");
                finish();
                startActivity(intent);
            }
        });

        List<Report_Accepted_Jobs> li = db.getReport_Accepted_Jobs(String.valueOf(record_id));
        if (!li.isEmpty()) {
            for (Report_Accepted_Jobs im : li) {
                et_unit_no.setText(im.getunit_no());
                et_building_name.setText(im.getbuilding_name());
                et_lot_no.setText(im.getlot_no());
                et_block_no.setText(im.getblock_no());
                et_street_no.setText(im.getstreet_no());
                et_street_name.setText(im.getstreet_name());
                et_village.setText(im.getvillage());
                et_district.setText(im.getdistrict());
                et_zip_code.setText(im.getzip_code());
                et_city.setText(im.getcity());
                et_province.setText(im.getprovince());
                et_region.setText(im.getregion());
                et_country.setText(im.getcountry());
            }
        }

        myDialog.show();
    }

    public void custom_dialog_zonal() {
        myDialog = new Dialog(Vacant_lot.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.zonal_dialog);
        myDialog.setCancelable(false);

        android.view.WindowManager.LayoutParams params = myDialog.getWindow()
                .getAttributes();
        params.height = LayoutParams.WRAP_CONTENT;
        params.width = LayoutParams.FILL_PARENT;
        myDialog.getWindow().setAttributes(
                (android.view.WindowManager.LayoutParams) params);
        report_zonal_location = (EditText) myDialog.findViewById(R.id.et_location);
        report_zonal_lot_classification = (EditText) myDialog.findViewById(R.id.et_lot_classification);
        report_zonal_value = (EditText) myDialog.findViewById(R.id.et_value_per_sqm);

        btn_save_zonal = (Button) myDialog.findViewById(R.id.btn_right);
        btn_cancel_zonal = (Button) myDialog.findViewById(R.id.btn_left);

        //valor3
        report_zonal_value.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(report_zonal_value, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });
        btn_save_zonal.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method
                Vacant_Lot_API vl = new Vacant_Lot_API();
                vl.setreport_zonal_location(report_zonal_location.getText().toString());
                vl.setreport_zonal_lot_classification(report_zonal_lot_classification.getText().toString());
                vl.setreport_zonal_value(gds.replaceFormat(report_zonal_value.getText().toString()));

                db.updateVacant_Lot_Zonal(vl, record_id);
                db.close();
                Toast.makeText(getApplicationContext(), "Saved",
                        Toast.LENGTH_SHORT).show();
                myDialog.dismiss();
                Intent intent = getIntent();
                intent.putExtra("keyopen", "0");
                finish();
                startActivity(intent);
            }
        });

        btn_cancel_zonal.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method
                myDialog.dismiss();
            }
        });

        List<Vacant_Lot_API> vl = db.getVacant_Lot(String.valueOf(record_id));
        if (!vl.isEmpty()) {
            for (Vacant_Lot_API im : vl) {
                report_zonal_location.setText(im.getreport_zonal_location());
                report_zonal_lot_classification.setText(im.getreport_zonal_lot_classification());
                report_zonal_value.setText(gds.numberFormat(im.getreport_zonal_value()));
            }
        }

        myDialog.show();
    }



    public void create_json() {
        List<Vacant_Lot_API> vl = db.getVacant_Lot(record_id);
        if (!vl.isEmpty()) {
            for (Vacant_Lot_API im : vl) {
                JSONObject query_temp = new JSONObject();
                JSONObject report = new JSONObject();
                JSONObject record_temp = new JSONObject();
                JSONObject appraisal_attachments = new JSONObject();
                JSONObject appraisal_attachments_data = new JSONObject();
                JSONArray app_attachments = new JSONArray();

                JSONObject lot_details_object = new JSONObject();//lot_details
                JSONArray lot_details_array = new JSONArray();//lot_details
                JSONObject val_obj = new JSONObject();//valuation market
                JSONArray val_ary = new JSONArray();//valuation market
                JSONObject val_details_obj = new JSONObject();//valuation market
                JSONObject rdps_obj = new JSONObject();//prev appraisal
                JSONArray rdps_ary = new JSONArray();//prev appraisal
                JSONObject prev_app_obj = new JSONObject();//main prev appraisal
                JSONArray prev_app_ary = new JSONArray();//main prev appraisal
                JSONObject address_obj = new JSONObject();//address
                JSONArray address_ary = new JSONArray();//address
                JSONObject app_req_obj = new JSONObject();//address

                try {
                    //check first for required fields
                    freeFields = true;
					/*if (freeFields) {*/
						/*fieldsComplete = true;
						gs.fieldsComplete  = true;*/
                    //submit
                    Log.e("Complete", "All required fields are filled up");

                    query_temp.put("system.record_id", record_id);
                    // report.put("application_status", "report_review");
                    report.put("from_mobile", "true");
                   report.put("valrep_land_time_inspected", im.getreport_time_inspected());
                    report.put("valrep_land_date_inspected_month", im.getreport_date_inspected_month());
                    report.put("valrep_land_date_inspected_day", im.getreport_date_inspected_day());
                    report.put("valrep_land_date_inspected_year", im.getreport_date_inspected_year());

                    report.put("valrep_land_id_association", im.getreport_id_association());
                    report.put("valrep_land_id_tax", im.getreport_id_tax());
                    report.put("valrep_land_id_lra", im.getreport_id_lra());
                    report.put("valrep_land_id_lot_config", im.getreport_id_lot_config());
                    report.put("valrep_land_id_subd_map", im.getreport_id_subd_map());
                    report.put("valrep_land_id_neighborhood_checking", im.getreport_id_nbrhood_checking());
                    report.put("valrep_land_imp_street_name", im.getreport_imp_street_name());
                    report.put("valrep_land_imp_street", im.getreport_imp_street());
                    report.put("valrep_land_imp_sidewalk", im.getreport_imp_sidewalk());
                    report.put("valrep_land_imp_curb", im.getreport_imp_curb());
                    report.put("valrep_land_imp_drainage", im.getreport_imp_drainage());
                    report.put("valrep_land_imp_street_lights", im.getreport_imp_street_lights());
                    report.put("valrep_land_physical_corner_lot", im.getreport_physical_corner_lot());
                    report.put("valrep_land_physical_non_corner_lot", im.getreport_physical_non_corner_lot());
                    report.put("valrep_land_physical_perimeter_lot", im.getreport_physical_perimeter_lot());
                    report.put("valrep_land_physical_intersected_lot", im.getreport_physical_intersected_lot());
                    report.put("valrep_land_physical_interior_with_row", im.getreport_physical_interior_with_row());
                    report.put("valrep_land_physical_landlocked", im.getreport_physical_landlocked());
                    report.put("valrep_land_physical_shape", im.getreport_physical_shape());
                    report.put("valrep_land_physical_frontage", im.getreport_physical_frontage());
                    report.put("valrep_land_physical_depth", im.getreport_physical_depth());
                    report.put("valrep_land_physical_road", im.getreport_physical_road());
                    report.put("valrep_land_physical_elevation", im.getreport_physical_elevation());
                    report.put("valrep_land_physical_terrain", im.getreport_physical_terrain());
                    report.put("valrep_land_lotclass_per_tax_dec", im.getreport_lotclass_per_tax_dec());
                    report.put("valrep_land_lotclass_actual_usage", im.getreport_lotclass_actual_usage());
                    report.put("valrep_land_lotclass_neighborhood", im.getreport_lotclass_neighborhood());
                    report.put("valrep_land_lotclass_highest_best_use", im.getreport_lotclass_highest_best_use());
                    report.put("valrep_land_landmark_1", im.getreport_landmark_1());
                    report.put("valrep_land_landmark_2", im.getreport_landmark_2());
                    report.put("valrep_land_landmark_3", im.getreport_landmark_3());
                    report.put("valrep_land_landmark_4", im.getreport_landmark_4());
                    report.put("valrep_land_landmark_5", im.getreport_landmark_5());
                    report.put("valrep_land_distance_1", im.getreport_distance_1());
                    report.put("valrep_land_distance_2", im.getreport_distance_2());
                    report.put("valrep_land_distance_3", im.getreport_distance_3());
                    report.put("valrep_land_distance_4", im.getreport_distance_4());
                    report.put("valrep_land_distance_5", im.getreport_distance_5());
                    report.put("valrep_land_util_electricity", im.getreport_util_electricity());
                    report.put("valrep_land_util_water", im.getreport_util_water());
                    report.put("valrep_land_util_telephone", im.getreport_util_telephone());
                    report.put("valrep_land_util_garbage", im.getreport_util_garbage());
                    report.put("valrep_land_trans_jeep", im.getreport_trans_jeep());
                    report.put("valrep_land_trans_bus", im.getreport_trans_bus());
                    report.put("valrep_land_trans_taxi", im.getreport_trans_taxi());
                    report.put("valrep_land_trans_tricycle", im.getreport_trans_tricycle());
                    report.put("valrep_land_faci_recreation", im.getreport_faci_recreation());
                    report.put("valrep_land_faci_security", im.getreport_faci_security());
                    report.put("valrep_land_faci_clubhouse", im.getreport_faci_clubhouse());
                    report.put("valrep_land_faci_pool", im.getreport_faci_pool());
                    report.put("valrep_land_bound_right", im.getreport_bound_right());
                    report.put("valrep_land_bound_left", im.getreport_bound_left());
                    report.put("valrep_land_bound_rear", im.getreport_bound_rear());
                    report.put("valrep_land_bound_front", im.getreport_bound_front());
                    report.put("valrep_land_prev_date", im.getreport_prev_date());
                    report.put("valrep_land_prev_appraiser", im.getreport_prev_appraiser());
                    report.put("valrep_land_prev_total_appraised_value", im.getreport_prev_total_appraised_value());
                    //comparatives
                    report.put("valrep_land_comp1_date_month", im.getreport_comp1_date_month());
                    report.put("valrep_land_comp1_date_day", im.getreport_comp1_date_day());
                    report.put("valrep_land_comp1_date_year", im.getreport_comp1_date_year());
                    report.put("valrep_land_comp1_source", im.getreport_comp1_source());
                    report.put("valrep_land_comp1_contact_no", im.getreport_comp1_contact_no());
                    report.put("valrep_land_comp1_location", im.getreport_comp1_location());
                    report.put("valrep_land_comp1_area", im.getreport_comp1_area());
                    report.put("valrep_land_comp1_base_price", im.getreport_comp1_base_price());
                    report.put("valrep_land_comp1_price_value", im.getreport_comp1_price_sqm());
                    report.put("valrep_land_comp1_discount_rate", im.getreport_comp1_discount_rate());
                    report.put("valrep_land_comp1_discounts", im.getreport_comp1_discounts());
                    report.put("valrep_land_comp1_selling_price", im.getreport_comp1_selling_price());
                    report.put("valrep_land_comp1_rec_location", im.getreport_comp1_rec_location());
                    report.put("valrep_land_comp1_rec_size", im.getreport_comp1_rec_size());
                    report.put("valrep_land_comp1_rec_accessibility", im.getreport_comp1_rec_accessibility());
                    report.put("valrep_land_comp1_rec_amenities", im.getreport_comp1_rec_amenities());
                    report.put("valrep_land_comp1_rec_terrain", im.getreport_comp1_rec_terrain());


                    report.put("valrep_land_comp1_rec_shape", im.getreport_comp1_rec_shape());
                    report.put("valrep_land_comp1_rec_corner_influence", im.getreport_comp1_rec_corner_influence());
                    report.put("valrep_land_comp1_rec_tru_lot", im.getreport_comp1_rec_tru_lot());
                    report.put("valrep_land_comp1_rec_elevation", im.getreport_comp1_rec_elevation());
                    report.put("valrep_land_comp1_rec_degree_of_devt", im.getreport_comp1_rec_degree_of_devt());
                    report.put("valrep_land_comp1_rec_time_element", im.getreport_comp1_rec_time_element());
                    report.put("valrep_land_comp1_concluded_adjustment", im.getreport_comp1_concluded_adjustment());
                    report.put("valrep_land_comp1_adjusted_value", im.getreport_comp1_adjusted_value());
                    report.put("valrep_land_comp1_remarks", im.getreport_comp1_remarks());

                    report.put("valrep_land_comp2_date_month", im.getreport_comp2_date_month());
                    report.put("valrep_land_comp2_date_day", im.getreport_comp2_date_day());
                    report.put("valrep_land_comp2_date_year", im.getreport_comp2_date_year());
                    report.put("valrep_land_comp2_source", im.getreport_comp2_source());
                    report.put("valrep_land_comp2_contact_no", im.getreport_comp2_contact_no());
                    report.put("valrep_land_comp2_location", im.getreport_comp2_location());
                    report.put("valrep_land_comp2_area", im.getreport_comp2_area());
                    report.put("valrep_land_comp2_base_price", im.getreport_comp2_base_price());
                    report.put("valrep_land_comp2_price_value", im.getreport_comp2_price_sqm());
                    report.put("valrep_land_comp2_discount_rate", im.getreport_comp2_discount_rate());
                    report.put("valrep_land_comp2_discounts", im.getreport_comp2_discounts());
                    report.put("valrep_land_comp2_selling_price", im.getreport_comp2_selling_price());
                    report.put("valrep_land_comp2_rec_location", im.getreport_comp2_rec_location());
                    report.put("valrep_land_comp2_rec_size", im.getreport_comp2_rec_size());

                    report.put("valrep_land_comp2_rec_accessibility", im.getreport_comp2_rec_accessibility());
                    report.put("valrep_land_comp2_rec_amenities", im.getreport_comp2_rec_amenities());
                    report.put("valrep_land_comp2_rec_terrain", im.getreport_comp2_rec_terrain());

                    report.put("valrep_land_comp2_rec_shape", im.getreport_comp2_rec_shape());
                    report.put("valrep_land_comp2_rec_corner_influence", im.getreport_comp2_rec_corner_influence());
                    report.put("valrep_land_comp2_rec_tru_lot", im.getreport_comp2_rec_tru_lot());
                    report.put("valrep_land_comp2_rec_elevation", im.getreport_comp2_rec_elevation());
                    report.put("valrep_land_comp2_rec_degree_of_devt", im.getreport_comp2_rec_degree_of_devt());
                    report.put("valrep_land_comp2_rec_time_element", im.getreport_comp2_rec_time_element());
                    report.put("valrep_land_comp2_concluded_adjustment", im.getreport_comp2_concluded_adjustment());
                    report.put("valrep_land_comp2_adjusted_value", im.getreport_comp2_adjusted_value());
                    report.put("valrep_land_comp2_remarks", im.getreport_comp2_remarks());

                    report.put("valrep_land_comp3_date_month", im.getreport_comp3_date_month());
                    report.put("valrep_land_comp3_date_day", im.getreport_comp3_date_day());
                    report.put("valrep_land_comp3_date_year", im.getreport_comp3_date_year());
                    report.put("valrep_land_comp3_source", im.getreport_comp3_source());
                    report.put("valrep_land_comp3_contact_no", im.getreport_comp3_contact_no());
                    report.put("valrep_land_comp3_location", im.getreport_comp3_location());
                    report.put("valrep_land_comp3_area", im.getreport_comp3_area());
                    report.put("valrep_land_comp3_base_price", im.getreport_comp3_base_price());
                    report.put("valrep_land_comp3_price_value", im.getreport_comp3_price_sqm());
                    report.put("valrep_land_comp3_discount_rate", im.getreport_comp3_discount_rate());
                    report.put("valrep_land_comp3_discounts", im.getreport_comp3_discounts());
                    report.put("valrep_land_comp3_selling_price", im.getreport_comp3_selling_price());
                    report.put("valrep_land_comp3_rec_location", im.getreport_comp3_rec_location());
                    report.put("valrep_land_comp3_rec_size", im.getreport_comp3_rec_size());

                    report.put("valrep_land_comp3_rec_accessibility", im.getreport_comp3_rec_accessibility());
                    report.put("valrep_land_comp3_rec_amenities", im.getreport_comp3_rec_amenities());
                    report.put("valrep_land_comp3_rec_terrain", im.getreport_comp3_rec_terrain());

                    report.put("valrep_land_comp3_rec_shape", im.getreport_comp3_rec_shape());
                    report.put("valrep_land_comp3_rec_corner_influence", im.getreport_comp3_rec_corner_influence());
                    report.put("valrep_land_comp3_rec_tru_lot", im.getreport_comp3_rec_tru_lot());
                    report.put("valrep_land_comp3_rec_elevation", im.getreport_comp3_rec_elevation());
                    report.put("valrep_land_comp3_rec_degree_of_devt", im.getreport_comp3_rec_degree_of_devt());
                    report.put("valrep_land_comp3_rec_time_element", im.getreport_comp3_rec_time_element());
                    report.put("valrep_land_comp3_concluded_adjustment", im.getreport_comp3_concluded_adjustment());
                    report.put("valrep_land_comp3_adjusted_value", im.getreport_comp3_adjusted_value());
                    report.put("valrep_land_comp3_remarks", im.getreport_comp3_remarks());

                    report.put("valrep_land_comp4_date_month", im.getreport_comp4_date_month());
                    report.put("valrep_land_comp4_date_day", im.getreport_comp4_date_day());
                    report.put("valrep_land_comp4_date_year", im.getreport_comp4_date_year());
                    report.put("valrep_land_comp4_source", im.getreport_comp4_source());
                    report.put("valrep_land_comp4_contact_no", im.getreport_comp4_contact_no());
                    report.put("valrep_land_comp4_location", im.getreport_comp4_location());
                    report.put("valrep_land_comp4_area", im.getreport_comp4_area());
                    report.put("valrep_land_comp4_base_price", im.getreport_comp4_base_price());
                    report.put("valrep_land_comp4_price_value", im.getreport_comp4_price_sqm());
                    report.put("valrep_land_comp4_discount_rate", im.getreport_comp4_discount_rate());
                    report.put("valrep_land_comp4_discounts", im.getreport_comp4_discounts());
                    report.put("valrep_land_comp4_selling_price", im.getreport_comp4_selling_price());
                    report.put("valrep_land_comp4_rec_location", im.getreport_comp4_rec_location());
                    report.put("valrep_land_comp4_rec_size", im.getreport_comp4_rec_size());

                    report.put("valrep_land_comp4_rec_accessibility", im.getreport_comp4_rec_accessibility());
                    report.put("valrep_land_comp4_rec_amenities", im.getreport_comp4_rec_amenities());
                    report.put("valrep_land_comp4_rec_terrain", im.getreport_comp4_rec_terrain());

                    report.put("valrep_land_comp4_rec_shape", im.getreport_comp4_rec_shape());
                    report.put("valrep_land_comp4_rec_corner_influence", im.getreport_comp4_rec_corner_influence());
                    report.put("valrep_land_comp4_rec_tru_lot", im.getreport_comp4_rec_tru_lot());
                    report.put("valrep_land_comp4_rec_elevation", im.getreport_comp4_rec_elevation());
                    report.put("valrep_land_comp4_rec_degree_of_devt", im.getreport_comp4_rec_degree_of_devt());
                    report.put("valrep_land_comp4_rec_time_element", im.getreport_comp4_rec_time_element());
                    report.put("valrep_land_comp4_concluded_adjustment", im.getreport_comp4_concluded_adjustment());
                    report.put("valrep_land_comp4_adjusted_value", im.getreport_comp4_adjusted_value());
                    report.put("valrep_land_comp4_remarks", im.getreport_comp4_remarks());

                    report.put("valrep_land_comp5_date_month", im.getreport_comp5_date_month());
                    report.put("valrep_land_comp5_date_day", im.getreport_comp5_date_day());
                    report.put("valrep_land_comp5_date_year", im.getreport_comp5_date_year());
                    report.put("valrep_land_comp5_source", im.getreport_comp5_source());
                    report.put("valrep_land_comp5_contact_no", im.getreport_comp5_contact_no());
                    report.put("valrep_land_comp5_location", im.getreport_comp5_location());
                    report.put("valrep_land_comp5_area", im.getreport_comp5_area());
                    report.put("valrep_land_comp5_base_price", im.getreport_comp5_base_price());
                    report.put("valrep_land_comp5_price_value", im.getreport_comp5_price_sqm());
                    report.put("valrep_land_comp5_discount_rate", im.getreport_comp5_discount_rate());
                    report.put("valrep_land_comp5_discounts", im.getreport_comp5_discounts());
                    report.put("valrep_land_comp5_selling_price", im.getreport_comp5_selling_price());
                    report.put("valrep_land_comp5_rec_location", im.getreport_comp5_rec_location());
                    report.put("valrep_land_comp5_rec_size", im.getreport_comp5_rec_size());

                    report.put("valrep_land_comp5_rec_accessibility", im.getreport_comp5_rec_accessibility());
                    report.put("valrep_land_comp5_rec_amenities", im.getreport_comp5_rec_amenities());
                    report.put("valrep_land_comp5_rec_terrain", im.getreport_comp5_rec_terrain());

                    report.put("valrep_land_comp5_rec_shape", im.getreport_comp5_rec_shape());
                    report.put("valrep_land_comp5_rec_corner_influence", im.getreport_comp5_rec_corner_influence());
                    report.put("valrep_land_comp5_rec_tru_lot", im.getreport_comp5_rec_tru_lot());
                    report.put("valrep_land_comp5_rec_elevation", im.getreport_comp5_rec_elevation());
                    report.put("valrep_land_comp5_rec_degree_of_devt", im.getreport_comp5_rec_degree_of_devt());
                    report.put("valrep_land_comp5_rec_time_element", im.getreport_comp5_rec_time_element());
                    report.put("valrep_land_comp5_concluded_adjustment", im.getreport_comp5_concluded_adjustment());
                    report.put("valrep_land_comp5_adjusted_value", im.getreport_comp5_adjusted_value());
                    report.put("valrep_land_comp5_remarks", im.getreport_comp5_remarks());

                    args[0]=record_id;
                    report.put("valrep_land_comp1_total_rectification", db2.getRecord("valrep_land_comp1_total_rectification",where,args,"vacant_lot").get(0));
                    report.put("valrep_land_comp2_total_rectification", db2.getRecord("valrep_land_comp2_total_rectification",where,args,"vacant_lot").get(0));
                    report.put("valrep_land_comp3_total_rectification", db2.getRecord("valrep_land_comp3_total_rectification",where,args,"vacant_lot").get(0));
                    report.put("valrep_land_comp4_total_rectification", db2.getRecord("valrep_land_comp4_total_rectification",where,args,"vacant_lot").get(0));
                    report.put("valrep_land_comp5_total_rectification", db2.getRecord("valrep_land_comp5_total_rectification",where,args,"vacant_lot").get(0));


                    report.put("valrep_land_comp_average", im.getreport_comp_average());
                    report.put("valrep_land_comp_rounded_to", im.getreport_comp_rounded_to());

                    //concerns
                    report.put("concerns_minor_1", im.getreport_concerns_minor_1());
                    report.put("concerns_minor_2", im.getreport_concerns_minor_2());
                    report.put("concerns_minor_3", im.getreport_concerns_minor_3());
                    report.put("concerns_minor_others", im.getreport_concerns_minor_others());
                    report.put("concerns_minor_others_desc", im.getreport_concerns_minor_others_desc());
                    report.put("concerns_major_1", im.getreport_concerns_major_1());
                    report.put("concerns_major_2", im.getreport_concerns_major_2());
                    report.put("concerns_major_3", im.getreport_concerns_major_3());
                    report.put("concerns_major_4", im.getreport_concerns_major_4());
                    report.put("concerns_major_5", im.getreport_concerns_major_5());
                    report.put("concerns_major_6", im.getreport_concerns_major_6());
                    report.put("concerns_major_others", im.getreport_concerns_major_others());
                    report.put("concerns_major_others_desc", im.getreport_concerns_major_others_desc());

                    report.put("valrep_land_comp1_rec_others", im.getreport_comp1_rec_others());
                    report.put("valrep_land_comp2_rec_others", im.getreport_comp2_rec_others());
                    report.put("valrep_land_comp3_rec_others", im.getreport_comp3_rec_others());
                    report.put("valrep_land_comp4_rec_others", im.getreport_comp4_rec_others());
                    report.put("valrep_land_comp5_rec_others", im.getreport_comp5_rec_others());

                    args[0]=record_id;
                    report.put("valrep_land_id_others", db2.getRecord("valrep_land_id_others",where,args,"vacant_lot").get(0));
                    report.put("valrep_land_physical_others", db2.getRecord("valrep_land_physical_others",where,args,"vacant_lot").get(0));
                    report.put("valrep_land_id_others_desc", db2.getRecord("valrep_land_id_others_desc",where,args,"vacant_lot").get(0));
                    report.put("valrep_land_physical_others_desc", db2.getRecord("valrep_land_physical_others_desc",where,args,"vacant_lot").get(0));
                    report.put("valrep_land_comp1_total_rectification", db2.getRecord("valrep_land_comp1_total_rectification",where,args,"vacant_lot").get(0));
                    report.put("valrep_land_comp2_total_rectification", db2.getRecord("valrep_land_comp2_total_rectification",where,args,"vacant_lot").get(0));
                    report.put("valrep_land_comp3_total_rectification", db2.getRecord("valrep_land_comp3_total_rectification",where,args,"vacant_lot").get(0));
                    report.put("valrep_land_comp4_total_rectification", db2.getRecord("valrep_land_comp4_total_rectification",where,args,"vacant_lot").get(0));
                    report.put("valrep_land_comp5_total_rectification", db2.getRecord("valrep_land_comp5_total_rectification",where,args,"vacant_lot").get(0));
                    report.put("app_unacceptable_collateral", db2.getRecord("app_unacceptable_collateral",where,args,"tbl_report_accepted_jobs").get(0));
                    report.put("valrep_land_road_right_of_way", db2.getRecord("valrep_land_road_right_of_way",where,args,"vacant_lot").get(0));


                    //get lot_details
                    List<Vacant_Lot_API_Lot_Details> vlld = db2.getVacant_Lot_Lot_Details(record_id);
                    if (!vlld.isEmpty()) {
                        for (Vacant_Lot_API_Lot_Details imld : vlld) {
                            lot_details_object = new JSONObject();
                            lot_details_object.put("valrep_land_propdesc_tct_no", imld.getreport_propdesc_tct_no());
                            lot_details_object.put("valrep_land_propdesc_lot", imld.getreport_propdesc_lot());
                            lot_details_object.put("valrep_land_propdesc_block", imld.getreport_propdesc_block());
                            lot_details_object.put("valrep_land_propdesc_survey_nos", imld.getreport_propdesc_survey_nos());
                            lot_details_object.put("valrep_land_propdesc_area", imld.getreport_propdesc_area());
                            lot_details_object.put("valrep_land_propdesc_registry_date", imld.getvalrep_land_propdesc_registry_date());
                            lot_details_object.put("valrep_land_propdesc_registered_owner", imld.getreport_propdesc_registered_owner());
                            lot_details_object.put("valrep_land_propdesc_deeds", imld.getreport_propdesc_deeds());
                            lot_details_array.put(lot_details_object);
                        }
                        //put to valrep_landimp_lot_details
                        report.put("valrep_land_lot_details", lot_details_array);

                    }


                    //get valuation (market data)
                    List<Vacant_Lot_API_Lot_Valuation_Details> vilvd = db2.getVacant_Lot_Lot_Valuation_Details(record_id);
                    if (!vilvd.isEmpty()) {
                        for (Vacant_Lot_API_Lot_Valuation_Details imlvd : vilvd) {
                            val_obj = new JSONObject();
                            val_obj.put("valrep_land_value_tct_no", imlvd.getreport_value_tct_no());
                            val_obj.put("valrep_land_value_lot_no", imlvd.getreport_value_lot_no());
                            val_obj.put("valrep_land_value_block_no", imlvd.getreport_value_block_no());
                            val_obj.put("valrep_land_value_total_area", imlvd.getreport_value_area());
                            val_obj.put("valrep_land_value_deduction", imlvd.getreport_value_deduction());
                            val_obj.put("valrep_land_value_net_area", imlvd.getreport_value_net_area());
                            val_obj.put("valrep_land_value_unit_value", imlvd.getreport_value_unit_value());
                            val_obj.put("valrep_land_value_total_land_value", imlvd.getreport_value_land_value());
                            val_ary.put(val_obj);
                        }
                        val_details_obj.put("valrep_land_lot_valuation_details", val_ary);
                        report.put("valrep_land_lot_valuation", val_details_obj);

                    } else {

                    }


                    //get rdps / prev app
                    List<Vacant_Lot_API_RDPS> vlrdps = db2.getVacant_Lot_RDPS(record_id);
                    if (!vlrdps.isEmpty()) {
                        for (Vacant_Lot_API_RDPS imrdps : vlrdps) {
                            rdps_obj = new JSONObject();
                            rdps_obj.put("valrep_land_prev_app_account", imrdps.getreport_prev_app_name());
                            rdps_obj.put("valrep_land_prev_app_location", imrdps.getreport_prev_app_location());
                            rdps_obj.put("valrep_land_prev_app_area", imrdps.getreport_prev_app_area());
                            rdps_obj.put("valrep_land_prev_app_value", imrdps.getreport_prev_app_value());
                            rdps_obj.put("valrep_land_prev_app_date", imrdps.getreport_prev_app_date());
                            rdps_ary.put(rdps_obj);
                        }
                        report.put("valrep_land_prev_appraisal", rdps_ary);
                    } else {
                        report.put("valrep_land_prev_appraisal", null);
                    }

                    //get main prev appraisal
                    List<Vacant_Lot_API_Prev_Appraisal> vlprevapp = db2.getVacant_Lot_API_Prev_Appraisal(record_id);
                    if (!vlprevapp.isEmpty()) {
                        for (Vacant_Lot_API_Prev_Appraisal imprevapp : vlprevapp) {
                            prev_app_obj = new JSONObject();
                            prev_app_obj.put("valrep_land_prev_desc", imprevapp.getreport_main_prev_desc());
                            prev_app_obj.put("valrep_land_prev_area", imprevapp.getreport_main_prev_area());
                            prev_app_obj.put("valrep_land_prev_unit_value", imprevapp.getreport_main_prev_unit_value());
                            prev_app_obj.put("valrep_land_prev_appraised_value", imprevapp.getreport_main_prev_appraised_value());
                            prev_app_obj.put("valrep_land_prev_land_value", imprevapp.getvalrep_land_prev_land_value());
                            prev_app_obj.put("valrep_land_prev_imp_value", imprevapp.getvalrep_land_prev_imp_value());
                            prev_app_ary.put(prev_app_obj);
                        }
                        report.put("valrep_land_main_prev_appraisal", prev_app_ary);
                    } else {
                        report.put("valrep_land_main_prev_appraisal", null);
                    }


                    //zonal value
                    report.put("valrep_land_zonal_location", im.getreport_zonal_location());
                    report.put("valrep_land_zonal_lot_classification", im.getreport_zonal_lot_classification());
                    report.put("valrep_land_zonal_value", im.getreport_zonal_value());

                    //totals
                    report.put("valrep_land_value_total_area", im.getreport_value_total_area());
                    report.put("valrep_land_value_total_deduction", im.getreport_value_total_deduction());
                    report.put("valrep_land_value_total_net_area", im.getreport_value_total_net_area());
                    report.put("valrep_land_value_total_land_value", im.getreport_value_total_landimp_value());
                    report.put("valrep_land_imp_value_total_imp_value", im.getreport_imp_value_total_imp_value());

                    //report summary
                    report.put("valrep_land_final_value_total_appraised_value", im.getreport_final_value_total_appraised_value_land_imp());
                    report.put("valrep_land_final_value_recommended_deductions_desc", im.getreport_final_value_recommended_deductions_desc());
                    report.put("valrep_land_final_value_recommended_deductions_rate", gds.nullCheck2(im.getreport_final_value_recommended_deductions_rate()));
                    report.put("valrep_land_final_value_recommended_deductions_other", im.getreport_final_value_recommended_deductions_other());

                    report.put("valrep_land_final_value_recommended_deductions_amt", im.getreport_final_value_recommended_deductions_amt());
//matrix condition


                    String recommended_deductions_rate_d = gds.nullCheck2(im.getreport_final_value_recommended_deductions_rate());
                    String total_appraised_value_land_imp_s = gds.nullCheck2(im.getreport_final_value_total_appraised_value_land_imp());

                    String recommended_deductions_amt_d = gds.deducAmtVL(total_appraised_value_land_imp_s, recommended_deductions_rate_d);

                    String net_appraised_value_d = gds.netAppValVL(total_appraised_value_land_imp_s, recommended_deductions_amt_d);


                    report.put("valrep_land_final_value_net_appraised_value", net_appraised_value_d);


                    if (!globalSave){
                        if (im.getreport_final_value_net_appraised_value().equals("")) {
                            matrix_value = 0.00;
                        } else {
                            matrix_value = gds.StringtoDouble(im.getreport_final_value_net_appraised_value());
                        }

                        /**
                         * removed matrix, date report and application status change due to matrix url
                         * 071917 mark
                         */
                        /*
                        //Matrix RE1
                        if ((matrix_value >= 0) && (matrix_value <= 5000000)) {
                            report.put("approval_signor_reviewer", "false");
                            report.put("approval_signor_reviewer_done", "true");
                            report.put("approval_supervisor_reviewer", "false");
                            report.put("approval_supervisor_reviewer_done", "true");

                            report.put("approval_signor", "true");
                            report.put("approval_supervisor", "alt");
                            report.put("approval_manager", "alt");
                            report.put("approval_head", "false");

                            report.put("approval_signor_done", "false");
                            report.put("approval_supervisor_done", "false");
                            report.put("approval_manager_done", "false");
                            report.put("approval_head_done", "true");

                            report.put("approval_matrix_type", "RE1");
                        }

                        //Matrix RE2
                        else if ((matrix_value > 5000000) && (matrix_value <= 10000000)) {
                            report.put("approval_signor_reviewer", "false");
                            report.put("approval_signor_reviewer_done", "true");
                            report.put("approval_supervisor_reviewer", "false");
                            report.put("approval_supervisor_reviewer_done", "true");

                            report.put("approval_signor", "false");
                            report.put("approval_supervisor", "true");
                            report.put("approval_manager", "alt");
                            report.put("approval_head", "alt");

                            report.put("approval_signor_done", "true");
                            report.put("approval_supervisor_done", "false");
                            report.put("approval_manager_done", "false");
                            report.put("approval_head_done", "false");

                            report.put("approval_matrix_type", "RE2");
                        }

                        //Matrix RE3
                        else if ((matrix_value > 10000000) && (matrix_value <= 20000000)) {
                            report.put("approval_signor_reviewer", "false");
                            report.put("approval_signor_reviewer_done", "true");
                            report.put("approval_supervisor_reviewer", "false");
                            report.put("approval_supervisor_reviewer_done", "true");

                            report.put("approval_signor", "true");
                            report.put("approval_supervisor", "true");
                            report.put("approval_manager", "true");
                            report.put("approval_head", "alt");

                            report.put("approval_signor_done", "false");
                            report.put("approval_supervisor_done", "false");
                            report.put("approval_manager_done", "false");
                            report.put("approval_head_done", "false");

                            report.put("approval_matrix_type", "RE3");
                        }

                        //Matrix RE4
                        else if (matrix_value > 20000000) {
                            report.put("approval_signor_reviewer", "false");
                            report.put("approval_signor_reviewer_done", "true");
                            report.put("approval_supervisor_reviewer", "false");
                            report.put("approval_supervisor_reviewer_done", "true");

                            report.put("approval_signor", "true");
                            report.put("approval_supervisor", "true");
                            report.put("approval_manager", "true");
                            report.put("approval_head", "true");

                            report.put("approval_signor_done", "false");
                            report.put("approval_supervisor_done", "false");
                            report.put("approval_manager_done", "false");
                            report.put("approval_head_done", "false");

                            report.put("approval_matrix_type", "RE4");
                        }
                        if (requesting_party.contentEquals("AUTO FINANCE GROUP")) {
                            report.put("application_status", "afg_valuation");
                        } else {
                            report.put("application_status", "report_review");
                        }*/
                        if (requesting_party.contentEquals("AUTO FINANCE GROUP")) {
                            report.put("application_status", "afg_valuation");
                        } else {
                            report.put("application_status", "report_review");
                        }
                    }
//matrix condition
//					}//end of matrix condition
//get current date and time and submit to CC
                    TimeZone tz = TimeZone.getTimeZone(gs.gmt);
                    Calendar c = Calendar.getInstance(tz);

                    int cMonth = c.get(Calendar.MONTH);//starts with 0 for january
                    int cDay = c.get(Calendar.DAY_OF_MONTH);
                    int cYear = c.get(Calendar.YEAR);
                    int cAmPm = c.get(Calendar.AM_PM);

                    String sMonth = gs.monthInWord[cMonth];
                    String sAmPm = "";
                    if (cAmPm == 0) {
                        sAmPm = "AM";
                    } else if (cAmPm == 1) {
                        sAmPm = "PM";
                    }

                    String cTime = String.format("%02d", c.get(Calendar.HOUR_OF_DAY)) + ":" +
                            String.format("%02d", c.get(Calendar.MINUTE)) + " " + sAmPm;


                    /**
                     * removed matrix, date report and application status change due to matrix url
                     * 071917 mark
                     */
                    /*report.put("valrep_land_report_date_month", sMonth);
                    report.put("valrep_land_report_date_day", String.valueOf(cDay));
                    report.put("valrep_land_report_date_year", String.valueOf(cYear));
                    report.put("valrep_land_time_of_report", cTime);*/

                    Log.e("DATE: ", String.valueOf(cMonth + 1) + " " + String.valueOf(cDay) + " " + String.valueOf(cYear));
                    Log.e("TIME: ", cTime);
//get current date and time and submit to CC

                    report.put("valrep_land_final_value_quick_sale_value_rate", im.getreport_final_value_quick_sale_value_rate());
                    report.put("valrep_land_final_value_quick_sale_value_amt", im.getreport_final_value_quick_sale_value_amt());

                    report.put("valrep_land_factors_of_concern", im.getreport_factors_of_concern());
                    report.put("valrep_land_suggested_corrective_actions", im.getreport_suggested_corrective_actions());
                    report.put("valrep_land_remarks", im.getreport_remarks());
                    report.put("valrep_land_requirements", im.getreport_requirements());

                    List<Report_Accepted_Jobs> addList = db.getReport_Accepted_Jobs(String.valueOf(record_id));
                    if (!addList.isEmpty()) {
                        for (Report_Accepted_Jobs ad : addList) {
                            address_obj = new JSONObject();
                            address_obj.put("app_unit_no", ad.getunit_no());
                            address_obj.put("app_bldg", ad.getbuilding_name());
                            address_obj.put("app_lot_no", ad.getlot_no());
                            address_obj.put("app_block_no", ad.getblock_no());
                            address_obj.put("app_street_no", ad.getstreet_no());
                            address_obj.put("app_street_name", ad.getstreet_name());
                            address_obj.put("app_village", ad.getvillage());
                            address_obj.put("app_district", ad.getdistrict());
                            address_obj.put("app_zip", ad.getzip_code());
                            address_obj.put("app_city", ad.getcity());
                            address_obj.put("app_province", ad.getprovince());
                            address_obj.put("app_region", ad.getregion());
                            address_obj.put("app_country", ad.getcountry());
                            address_ary.put(address_obj);
                            //ADDED BY IAN
                            report.put("app_daterequested_day", ad.getdr_day());
                            report.put("app_daterequested_year", ad.getdr_year());
                            report.put("app_account_first_name", ad.getfname());
                            report.put("app_account_middle_name", ad.getmname());
                            report.put("app_account_last_name", ad.getlname());
                            report.put("app_requesting_party", ad.getrequesting_party());
                            report.put("app_requestor", ad.getrequestor());
                            args[0]=record_id;
                            report.put("app_account_is_company",db2.getRecord("app_account_is_company",where,args,"tbl_report_accepted_jobs").get(0));
                            report.put("app_account_company_name", db2.getRecord("app_account_company_name",where,args,"tbl_report_accepted_jobs").get(0));
                            report.put("app_unacceptable_collateral", db2.getRecord("app_unacceptable_collateral",where,args,"tbl_report_accepted_jobs").get(0));

                        }
                    }
                    app_req_obj.put("app_collateral_address", address_ary);
                    app_req_obj.put("app_request_appraisal", "vacant_lot");
                    args[0]=record_id;
                    app_req_obj.put("app_purpose_appraisal", db2.getRecord("nature_appraisal",where,args,"tbl_report_accepted_jobs").get(0));
                    app_req_obj.put("app_kind_of_appraisal", db2.getRecord("kind_of_appraisal",where,args,"tbl_report_accepted_jobs").get(0));

                    report.put("appraisal_request", app_req_obj);


                    record_temp.put("record", report);

                    // for attachment

                    gs.fieldsComplete = freeFields;
                    if (freeFields) {
                        List<Report_filename> rf = db.getReport_filename(record_id);

                        if (!rf.isEmpty()) {
                            for (Report_filename report_file : rf) {
                                db_app_uid = report_file.getuid();
                                db_app_file = report_file.getfile();
                                db_app_filename = report_file.getfilename();
                                db_app_appraisal_type = report_file
                                        .getappraisal_type();
                                db_app_candidate_done = report_file
                                        .getcandidate_done();

                                appraisal_attachments = new JSONObject();
                                appraisal_attachments.put("uid", db_app_uid);
                                appraisal_attachments.put("file", db_app_file);
                                appraisal_attachments.put("filename",
                                        db_app_filename);
                                appraisal_attachments.put("appraisal_type",
                                        db_app_appraisal_type);
                                appraisal_attachments.put("candidate_done",
                                        db_app_candidate_done);
                                app_attachments.put(appraisal_attachments);
                                appraisal_attachments_data.put("attachments",
                                        app_attachments);
                                String query = query_temp.toString();
                                String record = record_temp.toString();
                                // attachments
                                String attachment = appraisal_attachments_data
                                        .toString();
                                userFunction.SendCaseCenter_Attachments(attachment);

                                boolean internetSpeedFast = Connectivity.isConnectedFast(getBaseContext());
                                if (internetSpeedFast == true) {

                                    runOnUiThread(new Runnable() {
                                        public void run() {
                                            gs.fastInternet = true;
                                            db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
                                            statusDisplay();
                                            Toast toast = Toast.makeText(getBaseContext(), "Connected", Toast.LENGTH_LONG);
                                            View view = toast.getView();
                                            view.setBackgroundResource(R.color.toast_color);
                                            toast.show();

                                        }
                                    });

                                    //TLS

                                    // API
                                    //for tls
                                    Log.e("query",""+query);
                                    JSONObject jsonResponse = new JSONObject();
                                    ArrayList<String> field = new ArrayList<>();
                                    ArrayList<String> value = new ArrayList<>();
                                    field.clear();
                                    field.add("auth_token");
                                    field.add("query");
                                    field.add("data");

                                    value.clear();
                                    value.add(gs.auth_token);
                                    value.add(query);
                                    value.add(record);

                                    /*if (Global.type.contentEquals("tls")) {
                                        jsonResponse = gds.makeHttpsRequest(gs.update_url, "POST", field, value);
                                        xml = jsonResponse.toString();
                                    } else {
                                        xml = gds.http_posting(field, value, gs.update_url, Vacant_lot.this);
                                    }*/

                                    if(Global.type.contentEquals("tls")){
                                        if (globalSave){
                                            jsonResponse = gds.makeHttpsRequest(gs.update_url, "POST", field, value);
                                        } else {
                                            jsonResponse = gds.makeHttpsRequest(gs.matrix_url, "POST", field, value);
                                        }
                                        xml = jsonResponse.toString();
                                    }else{
//									xml=gds.http_posting(field,value,gs.update_url,getApplicationContext());
                                        if (globalSave){
                                            xml=gds.http_posting(field,value,gs.update_url,getApplicationContext());
                                        } else {
                                            xml=gds.http_posting(field,value,gs.matrix_url,getApplicationContext());
                                        }
                                    }


                                    Log.e("response", xml);

                                    //check CC field status
                                    // if application_status == report review then data is submitted
                                    JSONObject mainxml = new JSONObject(xml);
                                    String recordxml, application_status;
                                    if (mainxml.has("record")) {
                                        Log.e("record", mainxml.getString("record"));
                                        if (mainxml.has("record")) {
                                            recordxml = mainxml.getString("record");
                                            Log.e("recordxml", recordxml);
                                            JSONObject json_application_status = new JSONObject(recordxml);
                                            if (json_application_status.has("application_status")) {
                                                application_status = json_application_status.getString("application_status");
                                                Log.e("application_status", application_status);

                                                if (application_status.contentEquals("report_review")) {
                                                    appStatReviewer = true;
                                                } else if (application_status.contentEquals("afg_valuation")) {
                                                    appStatReviewer = true;
                                                } else {
                                                    appStatReviewer = false;
                                                }

                                            }
                                        }
                                    }//end of check CC field

                                } else {
                                    runOnUiThread(new Runnable() {
                                        public void run() {
                                            db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
                                            statusDisplay();
                                            Toast toast = Toast.makeText(getBaseContext(), "Cannot send the data because your internet connection is slow/disconnected/unstable. \nPlease make sure that you are connected to a stable connection", Toast.LENGTH_LONG);
                                            View view = toast.getView();
                                            view.setBackgroundResource(R.color.toast_red);
                                            toast.show();
                                        }
                                    });
                                }
                            }
                        }

                    }//end of if freeFields == true
                    else if (!freeFields) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
                                statusDisplay();
                                Toast toast = Toast.makeText(getBaseContext(), "You need to fill up all required fields before you can submit the report.", Toast.LENGTH_LONG);
                                View view = toast.getView();
                                view.setBackgroundResource(R.color.toast_red);
                                toast.show();
                            }
                        });
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Update CC Failed",
                            "Exception : " + e.getMessage(), e);
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Failed to send data. Please try again.",
                                    Toast.LENGTH_LONG).show();
                            fileUploaded=false;
                            upload_status(fileUploaded);
                            error_uploading();
                        }
                    });

                }

            }
        }

    }

    private class SendData extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pDialog = new ProgressDialog(Vacant_lot.this);
            pDialog.setMessage("Updating data..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            NetworkUtil.getConnectivityStatusString(Vacant_lot.this);
            if (!NetworkUtil.status.equals("Network not available")) {
                gs.online = true;
                google_response = gds.google_ping();
                if (google_response.equals("200")) {

                    gs.connectedToServer = true;

                    boolean internetSpeedFast = Connectivity.isConnectedFast(getBaseContext());
                    if (internetSpeedFast == true) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                gs.fastInternet = true;
                                Toast toast = Toast.makeText(getBaseContext(), "Connected", Toast.LENGTH_LONG);
                                View view = toast.getView();
                                view.setBackgroundResource(R.color.toast_color);
                                toast.show();
                            }
                        });


                        if (((withAttachment == true) && (freshData == true)) ||
                                ((withAttachment == true) && (freshData == false))) {
                            upload_attachment();
                        } else if ((withAttachment == false) && (freshData == false)) {
                            globalSave = false;
                            create_json();
//							db_delete_all();//temp disabled
                            db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
                            finish();
                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                Toast toast = Toast.makeText(getBaseContext(), "Cannot send the data because your internet connection is slow. \nPlease make sure that you are connected to a stable connection", Toast.LENGTH_LONG);
                                View view = toast.getView();
                                view.setBackgroundResource(R.color.toast_red);
                                toast.show();
                            }
                        });
                    }
                    network_status = true;
                } else {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
                            statusDisplay();
                            Toast toast = Toast.makeText(getBaseContext(), "Unable to connect to the server", Toast.LENGTH_LONG);
                            View view = toast.getView();
                            view.setBackgroundResource(R.color.toast_red);
                            toast.show();
                            serverError();
                        }
                    });
                }
            } else {
                network_status = false;
                runOnUiThread(new Runnable() {
                    public void run() {
                        db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
                        statusDisplay();
                    }
                });
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            pDialog.dismiss();
            if (network_status == false) {
                db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
                statusDisplay();
                Toast.makeText(getApplicationContext(),
                        "Network not available", Toast.LENGTH_SHORT).show();
            } else if (network_status == true) {
                if (google_response.equals("200")) {
                    if (google_response.equals("200")) {
                        if (fileUploaded) {

                            if (appStatReviewer && freeFields) {
                                gs.fieldsComplete = freeFields;
                                gs.sent = true;
                                db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
                                gs.online = false;
                                gs.connectedToServer = false;
                                gs.fastInternet = false;
                                gs.fieldsComplete = false;
                                gs.sent = false;
                                add_submitted_Delete();
                                Toast toast = Toast.makeText(getBaseContext(), "Data Updated", Toast.LENGTH_LONG);
                                View view = toast.getView();
                                view.setBackgroundResource(R.color.toast_blue);
                                toast.show();
                                finish();
                            }

                        } else{
                            Toast.makeText(getApplicationContext(), "Data Retained", Toast.LENGTH_LONG).show();
                            error_uploading();//popup dialog error
                        }
                        //finish();
                    }
                }
            }

        }
    }

    public void add_submitted_Delete() {
        String temp_is_company = db2.getRecord("app_account_is_company",where,args,"tbl_report_accepted_jobs").get(0);
        String temp_company_name = db2.getRecord("app_account_company_name",where,args,"tbl_report_accepted_jobs").get(0);

        if(temp_is_company.contentEquals("true")){
            account_fname = temp_company_name;
            account_mname="";
            account_lname="";
        }
        ArrayList<String> field = new ArrayList<String>();
        field.clear();
        field.add("record_id");
        field.add("first_name");
        field.add("middle_name");
        field.add("last_name");
        field.add("appraisal_type");
        field.add("status");
        ArrayList<String> value = new ArrayList<String>();
        value.clear();
        value.add(record_id);
        value.add(account_fname);
        value.add(account_mname);
        value.add(account_lname);
        value.add("Vacant Lot");
        value.add("submitted");
        String where = "WHERE record_id = args";
        String[] args = new String[1];
        args[0] = record_id;

        String r_id = db2.getRecord("record_id", where, args, "tbl_report_submitted").get(0);
        if (r_id.contentEquals(record_id)) {
            db2.deleteRecord("tbl_report_submitted", "record_id = ?", new String[]{record_id});
            db2.addRecord(value, field, "tbl_report_submitted");
        } else {
            db2.addRecord(value, field, "tbl_report_submitted");
        }
        //delete

        gds.deleteAllRecord(record_id,db2);

    }

    private class GlobalSave extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pDialog = new ProgressDialog(Vacant_lot.this);
            pDialog.setMessage("Generating report...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            NetworkUtil.getConnectivityStatusString(Vacant_lot.this);
            if (!NetworkUtil.status.equals("Network not available")) {
                if (gds.google_ping().equals("200")) {
                    if (Connectivity.isConnectedFast(getBaseContext())) {
                        globalSave = true;
                        create_json();
                        Log.e("start creat json","true");
                        return true;
                    } else {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
                                statusDisplay();
                                Toast toast = Toast.makeText(getBaseContext(), "Slow Internet connection.", Toast.LENGTH_LONG);
                                View view = toast.getView();
                                view.setBackgroundResource(R.color.toast_red);
                                toast.show();
                                serverError();
                            }
                        });
                        return false;
                    }

                } else {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
                            statusDisplay();
                            Toast toast = Toast.makeText(getBaseContext(), "Unable to connect to the server", Toast.LENGTH_LONG);
                            View view = toast.getView();
                            view.setBackgroundResource(R.color.toast_red);
                            toast.show();
                            serverError();
                        }
                    });
                    return false;
                }

            } else {
                runOnUiThread(new Runnable() {
                    public void run() {
                        db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
                        statusDisplay();
                        Toast toast = Toast.makeText(getBaseContext(), "Network not available.", Toast.LENGTH_LONG);
                        View view = toast.getView();
                        view.setBackgroundResource(R.color.toast_red);
                        toast.show();
                        serverError();
                    }
                });
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            if (result) {
                Intent in = new Intent(getApplicationContext(), View_Report.class);
                in.putExtra(TAG_RECORD_ID, record_id);
                in.putExtra("appraisal_type", "vacant_lot");
                startActivityForResult(in, 100);
            }
            pDialog.dismiss();


        }

    }

    public void serverError() {

        myDialog = new Dialog(Vacant_lot.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.warning_dialog);
        myDialog.setCancelable(true);
        final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
        final Button btn_ok = (Button) myDialog.findViewById(R.id.btn_ok);

        tv_question.setText(R.string.server_error);
        btn_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.show();
    }


    public void error_uploading() {
        myDialog = new Dialog(Vacant_lot.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.warning_dialog);
        myDialog.setCancelable(true);
        final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
        final Button btn_ok = (Button) myDialog.findViewById(R.id.btn_ok);
        tv_question.setText(R.string.error_uploading);
        btn_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.show();
    }

    public void create_pdf() {
        pdf_name = uid + "_"
                + "Property Pictures" + "_" + counter;
        SharedPreferences appPrefs = getSharedPreferences("preference",
                MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = appPrefs.edit();
        prefsEditor.putString("pdf", pdf_name);
        prefsEditor.putString("fname", account_fname);
        prefsEditor.putString("lname", account_lname);
        prefsEditor.commit();
        startActivity(new Intent(Vacant_lot.this, Pdf_Townhouse_condo.class));

    }

    public void open_customdialog() {
        // dialog
        myDialog = new Dialog(context);
        myDialog.setTitle("Select Attachment");
        myDialog.setContentView(R.layout.request_pdf_list);
        // myDialog.setTitle("My Dialog");

        dlg_priority_lvw = (ListView) myDialog
                .findViewById(R.id.dlg_priority_lvw);
        // ListView
        SimpleAdapter adapter = new SimpleAdapter(context, getPriorityList(),
                R.layout.request_pdf_list_layout, new String[]{
                "txt_pdf_list", "mono"}, new int[]{
                R.id.txt_pdf_list, R.id.mono});
        dlg_priority_lvw.setAdapter(adapter);

        // ListView
        dlg_priority_lvw
                .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1,
                                            int arg2, long arg3) {

                        item = ((TextView) arg1.findViewById(R.id.txt_pdf_list))
                                .getText().toString();
                        attachment_selected = item;
                        tv_attachment.setText(attachment_selected);
                        // update

                        Report_filename rf = new Report_filename();
                        rf.setuid(uid);
                        rf.setfile("Property Pictures");
                        rf.setfilename(attachment_selected);
                        rf.setappraisal_type(appraisal_type + "_" + counter);
                        rf.setcandidate_done("true");
                        db.updateReport_filename(rf, record_id);
                        db.close();
                        myDialog.dismiss();
                        checkFileSize();
                    }
                });
        dlg_priority_lvw.setLongClickable(true);
        dlg_priority_lvw
                .setOnItemLongClickListener(new OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> arg0,
                                                   View arg1, final int arg2, long arg3) {
                        item = ((TextView) arg1.findViewById(R.id.txt_pdf_list))
                                .getText().toString();
                        Toast.makeText(getApplicationContext(), item,
                                Toast.LENGTH_SHORT).show();

                        File file = new File(myDirectory, item);

                        if (file.exists()) {
                            Uri path = Uri.fromFile(file);
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(path, "application/pdf");
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                            try {
                                startActivity(intent);
                            } catch (ActivityNotFoundException e) {
                                Toast.makeText(getApplicationContext(),
                                        "No Application Available to View PDF",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }

                        return true;

                    }
                });
        myDialog.show();
    }

    private List<HashMap<String, Object>> getPriorityList() {
        DatabaseHandler db = new DatabaseHandler(
                context.getApplicationContext());
        List<HashMap<String, Object>> priorityList = new ArrayList<HashMap<String, Object>>();

        List<Images> images = db.getAllImages(uid);//added account lname for specific search result like
        if (!images.isEmpty()) {
            for (Images im : images) {
                filename = im.getfilename();
                String filenameArray[] = filename.split("\\.");
                String extension = filenameArray[filenameArray.length - 1];
                if (extension.equals("pdf")) {
                    HashMap<String, Object> map1 = new HashMap<String, Object>();
                    map1.put("txt_pdf_list", im.getfilename());
                    map1.put("mono", R.drawable.attach_item);
                    priorityList.add(map1);
                }

            }

        }
        return priorityList;
    }

    public void checkFileSize() {
        myDialog = new Dialog(Vacant_lot.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.warning_dialog);
        myDialog.setCancelable(true);
        final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
        final Button btn_ok = (Button) myDialog.findViewById(R.id.btn_ok);
        tv_question.setText(R.string.file_size_too_large);
        //format double to 2 decimal place
        DecimalFormat df = new DecimalFormat("#.00");
        //get fileSize and convert to KB and MB
        File filenew = new File(myDirectory + "/" + item);//get path concat with slash and file name
        int file_size_kb = Integer.parseInt(String.valueOf(filenew.length() / 1024));
        double file_size_mb = file_size_kb / 1024.00;
        //custom toast
        Toast toast = Toast.makeText(context, item +
                "\nfile size in KB: " + file_size_kb + "KB" +
                "\nfile size in MB: " + df.format(file_size_mb) + "MB", Toast.LENGTH_LONG);
        View view = toast.getView();
        view.setBackgroundResource(R.color.toast_color);
        toast.show();


        if (file_size_kb > 6144) {//restrict the file size up to 6.0MB only
            //clear textView to restict uploading
            Toast.makeText(getApplicationContext(), "file size is too large",
                    Toast.LENGTH_SHORT).show();
            tv_attachment.setText("");
            myDialog.show();
        }

        btn_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void upload_attachment() {
        // upload pdf
        upload_status(gds.uploadFile(Environment.getExternalStorageDirectory() + "/" + dir + "/"
                + tv_attachment.getText().toString()));
    }

    /*public int uploadFile(String sourceFileUri) {
        TLSConnection tlscon = new TLSConnection();
        String upLoadServerUri = gs.pdf_upload_url;
        String fileName = sourceFileUri;

        HttpsURLConnection conn = null;
        HttpURLConnection conn2 = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(sourceFileUri);
        if (!sourceFile.isFile()) {
            Log.e("uploadFile", "Source File Does not exist");
            return 0;
        }
        try { // open a URL connection to the Servlet
            FileInputStream fileInputStream = new FileInputStream(sourceFile);
            URL url = new URL(upLoadServerUri);


            if (Global.type.contentEquals("tls")) {

                conn = tlscon.setUpHttpsConnection("" + url);

                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type",
                        "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("uploaded_file", fileName);
                dos = new DataOutputStream(conn.getOutputStream());
            } else {

                conn2 = (HttpURLConnection) url.openConnection();
                conn2.setDoInput(true); // Allow Inputs
                conn2.setDoOutput(true); // Allow Outputs
                conn2.setUseCaches(false); // Don't use a Cached Copy
                conn2.setRequestMethod("POST");
                conn2.setRequestProperty("Connection", "Keep-Alive");
                conn2.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn2.setRequestProperty("Content-Type",
                        "multipart/form-data;boundary=" + boundary);
                conn2.setRequestProperty("uploaded_file", fileName);
                dos = new DataOutputStream(conn2.getOutputStream());
            }

			*//*conn = tlscon.setUpHttpsConnection(""+url); // Open a HTTP
			// the URL
			conn.setDoInput(true); // Allow Inputs
			conn.setDoOutput(true); // Allow Outputs
			conn.setUseCaches(false); // Don't use a Cached Copy
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("ENCTYPE", "multipart/form-data");
			conn.setRequestProperty("Content-Type",
					"multipart/form-data;boundary=" + boundary);
			conn.setRequestProperty("uploaded_file", fileName);
			dos = new DataOutputStream(conn.getOutputStream());*//*

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
                    + fileName + "\"" + lineEnd);
            dos.writeBytes(lineEnd);

            bytesAvailable = fileInputStream.available(); // create a buffer of
            // maximum size

            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            // read file and write it into form...
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0) {
                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            // send multipart form data necesssary after file data...
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            // Responses from the server (code and message)
            String serverResponseMessage = "";
            if (Global.type.contentEquals("tls")) {
                serverResponseCode = conn.getResponseCode();
                serverResponseMessage = conn.getResponseMessage();
            } else {
                serverResponseCode = conn2.getResponseCode();
                serverResponseMessage = conn2.getResponseMessage();
            }

			*//*serverResponseCode = conn.getResponseCode();
			String serverResponseMessage = conn.getResponseMessage();*//*

            Log.i("uploadFile", "HTTP Response is : " + serverResponseMessage
                    + ": " + serverResponseCode);
            if (serverResponseCode == 200) {
                runOnUiThread(new Runnable() {
                    public void run() {
                    }
                });
            }

            // close the streams //
            fileInputStream.close();
            dos.flush();
            dos.close();

            //if upload succeeded
            fileUploadFailed = "false";
            upload_status();

        } catch (MalformedURLException ex) {
            ex.printStackTrace();
            Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Upload file to server Exception",
                    "Exception : " + e.getMessage(), e);
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getApplicationContext(), "Failed to upload File / Images",
                            Toast.LENGTH_LONG).show();
                    fileUploadFailed = "true";
                    upload_status();
                }
            });
        }
        return serverResponseCode;
    }*/

    public void upload_status(boolean response) {
        fileUploaded = response;
        if (response) {
            globalSave=false;
            create_json();

        }
    }

    public void open_pdf() {
        File file = new File(myDirectory, tv_attachment.getText().toString());

        if (file.exists()) {
            Uri path = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(getApplicationContext(),
                        "No Application Available to View PDF",
                        Toast.LENGTH_SHORT).show();
            }
        }

    }

    public void uploaded_attachments() {
        // set value
        List<Required_Attachments> required_attachments = db
                .getAllRequired_Attachments_byid(String.valueOf(appraisal_type));
        if (!required_attachments.isEmpty()) {
            for (final Required_Attachments ra : required_attachments) {
                LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                final View addView = layoutInflater.inflate(
                        R.layout.report_uploaded_attachments_dynamic, null);
                final TextView tv_attachment = (TextView) addView
                        .findViewById(R.id.tv_attachment);
                final ImageView btn_view = (ImageView) addView
                        .findViewById(R.id.btn_view);
                tv_attachment.setText(ra.getattachment());
                btn_view.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        uploaded_attachment = uid + "_"
                                + ra.getattachment() + "_" + counter + ".pdf";
                        // directory
                        uploaded_file = new File(myDirectory,
                                uploaded_attachment);
                        uploaded_attachment = uploaded_attachment.replaceAll(
                                " ", "%20");
                        //url_webby = "http://dv-dev.gdslinkasia.com/attachments/"
                        //		+ uploaded_attachment;//dev orig
                        //url_webby = "http:http://dev.gdslinkasia.com:8080/ia.com:8080/pdf_attachments/"
                        //		+ uploaded_attachment;
                        //url_webby = "https://gdsuser:gdsuser01@gds.securitybank.com:8080/pdf_attachments/"
                        //		+ uploaded_attachment;//sbs
                        //url_webby = "https://gds.securitybank.com:8080/pdf_attachments/"
                        //		+ uploaded_attachment;//sbs
                        url_webby = gs.pdf_loc_url + uploaded_attachment;
                        view_pdf(uploaded_file);
                    }
                });
                container_attachments.addView(addView);

            }

        }

    }

    private void view_download() {
        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Action");
        builder.setItems(commandArray, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int index) {
                if (index == 0) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
                            .parse(url_webby));
					/*Authenticator.setDefault (new Authenticator() {
					    protected PasswordAuthentication getPasswordAuthentication() {
					        return new PasswordAuthentication ("gdsuser", "gdsuser01".toCharArray());
					    }
					});*/
                    startActivity(browserIntent);
                    dialog.dismiss();
                } else if (index == 1) {
                    new DownloadFileFromURL().execute(url_webby);
                    dialog.dismiss();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void view_pdf(File file) {
        if (file.exists()) {
            Uri path = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(getApplicationContext(),
                        "No Application Available to View PDF",
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            NetworkUtil.getConnectivityStatusString(this);
            if (!NetworkUtil.status.equals("Network not available")) {
                new Attachment_validation().execute();
            } else {
                Toast.makeText(getApplicationContext(),
                        "Network not available", Toast.LENGTH_SHORT).show();
            }

        }
    }

    public void db_delete_all() {

        String[] involvedTables = {"vacant_lot_lot_details", "vacant_lot_lot_valuation_details",
                "vacant_lot_prev_appraisal", "tbl_attachments", "submission_logs"};
        for (int x = 0; x < involvedTables.length; x++) {
            db2.deleteAllDB2(record_id, involvedTables[x]);
            db2.close();
        }

        Report_Accepted_Jobs raj = new Report_Accepted_Jobs();
        raj.setrecord_id(record_id);
        db.deleteReport_Accepted_Jobs(raj);
        db.close();

        Report_Accepted_Jobs_Contacts rajc = new Report_Accepted_Jobs_Contacts();
        rajc.setrecord_id(record_id);
        db.deleteReport_Accepted_Jobs_Contacts(rajc);
        db.close();

        Report_filename rf = new Report_filename();
        rf.setrecord_id(record_id);
        db.deleteReport_filename(rf);
        db.close();

        Vacant_Lot_API va = new Vacant_Lot_API();
        va.setrecord_id(record_id);
        db.deleteVacant_Lot_API(va);
        db.close();
    }

    public void manual_delete_all() {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.delete_dialog);

        Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btn_no = (Button) dialog.findViewById(R.id.btn_no);
        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_question);
        dialog.show();

        text.setText("Do you want to delete this record?");

        // if button is clicked, close the custom dialog
        btn_yes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                gds.deleteAllRecord(record_id,db2);

                dialog.dismiss();
                finish();
            }
        });
        btn_no.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void check_attachment() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.yes_no_dialog);

        Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btn_no = (Button) dialog.findViewById(R.id.btn_no);
        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_question);
        text.setText("Are you sure you want to submit ?");
        //aug 17
        args[0] = record_id;
        if(db2.getRecord("application_status",where,args,"tbl_report_accepted_jobs").get(0).contentEquals("for_rework")){
            freshData = false;
        }else{
            freshData = true;
        }

        //check if with or without attachment
        if (((!tv_attachment.getText().toString().equals("")) && (freshData == false)) ||
                ((!tv_attachment.getText().toString().equals("")) && (freshData == true))) {
            dialog.show();
            withAttachment = true;
        } else if ((tv_attachment.getText().toString().equals("")) && (freshData == true)) {
            text.setText("Do you want to submit report without file/pdf attachment?");
            withAttachment = false;
            fileUploaded=true;
            dialog.dismiss();
            warning_dialog();
        } else if ((tv_attachment.getText().toString().equals("")) && (freshData == false)) {
            text.setText("Do you want to submit the report?");
            withAttachment = false;
            fileUploaded=true;
            dialog.show();
        }

        // if button is clicked, close the custom dialog
        btn_yes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((withAttachment == true) && (freshData == true)) ||
                        ((withAttachment == true) && (freshData == false)) ||
                        ((withAttachment == false) && (freshData == false))) {
//                    new SendData().execute();
                    if (new Fields_Checker().isModuleComplete(record_id, context)) {
                        new SendData().execute();
//                        Toast.makeText(getApplicationContext(), "Module fields complete", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Module fields incomplete", Toast.LENGTH_LONG).show();
                    }
                    dialog.dismiss();
                } else if ((withAttachment == false) && (freshData == true)) {
                    no_attachment();
                    dialog.dismiss();
                }
            }
        });
        btn_no.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void warning_dialog() {
        final Dialog dialog2 = new Dialog(context);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.setContentView(R.layout.warning_dialog);

        // set the custom dialog components - text, image and button
        TextView text2 = (TextView) dialog2.findViewById(R.id.tv_question);
        text2.setText("Please attach a file/pdf before you can submit the report");
        Button btn_ok = (Button) dialog2.findViewById(R.id.btn_ok);

        btn_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });

        dialog2.show();
    }

    public void no_attachment() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.yes_no_dialog);

        Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btn_no = (Button) dialog.findViewById(R.id.btn_no);
        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_question);
        dialog.show();

        text.setText("Are you sure you want to submit ?");

        // if button is clicked, close the custom dialog
        btn_yes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                withAttachment = false;
//                new SendData().execute();
                if (new Fields_Checker().isModuleComplete(record_id, context)) {
                    new SendData().execute();
//                    Toast.makeText(getApplicationContext(), "Module fields complete", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Module fields incomplete", Toast.LENGTH_LONG).show();
                }
                dialog.dismiss();
            }
        });
        btn_no.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void update_dialog() {
        // custom dialog
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.yes_no_dialog);

        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_question);
        text.setText("Are you sure you want to submit ?");
        Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

        // if button is clicked, close the custom dialog
        btn_yes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                new SendData().execute();
                if (new Fields_Checker().isModuleComplete(record_id, context)) {
                    new SendData().execute();
//                    Toast.makeText(getApplicationContext(), "Module fields complete", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Module fields incomplete", Toast.LENGTH_LONG).show();
                }
                dialog.dismiss();
            }
        });
        btn_no.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void switch_appraisal_type() {
        // custom dialog
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.yes_no_dialog);

        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_question);
        text.setText("Are you sure you want to CHANGE APPRAISAL TYPE?");
        Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

        // if button is clicked, close the custom dialog
        btn_yes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                transfer_ownership_property();


                String app_type = "land_improvements";
                //update appraisal type
                String[] involvedTablesToUpdate = {"report_filename", "tbl_attachments",
                        "tbl_report_accepted_jobs"};
                for (int x = 0; x < involvedTablesToUpdate.length; x++) {
                    db.updateAppraisal_Type(record_id, app_type, involvedTablesToUpdate[x]);
                    db.close();
                }

                //create/add new blank fields
                TableObjects to = new TableObjects();
                args[0]=record_id;
                String check_li_size = gds.nullCheck4(db2.getRecord("id", where, args, Tables.land_improvements.table_name).get(0));

                if (check_li_size.contentEquals("")) {
                    db2.addRecord(gds.emptyFields(to.landimp_db().size() - 2,record_id), to.landimp_db(),"land_improvements");
                }



                //get streetname and save to new table
                String street_name = db_new.getRecord("street_name", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id);
                ArrayList<String> values = new ArrayList<>();
                ArrayList<String> fields = new ArrayList<>();
                values.add(street_name);
                fields.add("report_imp_street");
                db_new.updateRecord(values, fields, "record_id = ?", new String[]{record_id}, Tables.vacant_lot.table_name);

                dialog.dismiss();
                finish();
            }
        });
        btn_no.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }



    void transfer_ownership_property() {
        args[0]=record_id;
        int ownership_size = db2.getRecord("report_propdesc_tct_no", where, args, Tables.vacant_lot_lot_details.table_name).size();

        String check_li_ownership_size = gds.nullCheck4(db2.getRecord("id", where, args, Tables.land_improvements_lot_details.table_name).get(0));

        if (check_li_ownership_size.contentEquals("")) {

        for (int x = 0; x < ownership_size; x++) {
            //transfer ownership and market data

            ArrayList<String> fields = new ArrayList<>();

            fields.clear();
            fields.add("record_id");
            fields.add("report_propdesc_tct_no");
            fields.add("report_propdesc_lot");
            fields.add("report_propdesc_block");
            fields.add("report_propdesc_survey_nos");
            fields.add("report_propdesc_area");
            fields.add("valrep_landimp_propdesc_registry_date");
            fields.add("report_propdesc_registered_owner");
            fields.add("report_propdesc_registry_of_deeds");

            ArrayList<String> values = new ArrayList<>();
            values.clear();
            values.add(record_id);
            values.add(db2.getRecord("report_propdesc_tct_no", where, args, Tables.vacant_lot_lot_details.table_name).get(x));
            values.add(db2.getRecord("report_propdesc_lot", where, args, Tables.vacant_lot_lot_details.table_name).get(x));
            values.add(db2.getRecord("report_propdesc_block", where, args, Tables.vacant_lot_lot_details.table_name).get(x));
            values.add(db2.getRecord("report_propdesc_survey_nos", where, args, Tables.vacant_lot_lot_details.table_name).get(x));
            values.add(db2.getRecord("report_propdesc_area", where, args, Tables.vacant_lot_lot_details.table_name).get(x));
            values.add(db2.getRecord("valrep_land_propdesc_registry_date", where, args, Tables.vacant_lot_lot_details.table_name).get(x));
            values.add(db2.getRecord("report_propdesc_registered_owner", where, args, Tables.vacant_lot_lot_details.table_name).get(x));
            values.add(db2.getRecord("report_propdesc_deeds", where, args, Tables.vacant_lot_lot_details.table_name).get(x));
            db_new.addRecord(values, fields, Tables.land_improvements_lot_details.table_name);

            //add market data
            ArrayList<String> market_fields = new ArrayList<>();

            market_fields.clear();
            market_fields.add("record_id");
            market_fields.add("report_value_tct_no");
            market_fields.add("report_value_lot_no");
            market_fields.add("report_value_block_no");
            market_fields.add("report_value_area");
            market_fields.add("report_value_deduction");
            market_fields.add("report_value_net_area");
            market_fields.add("report_value_unit_value");
            market_fields.add("report_value_land_value");

            ArrayList<String> market_values = new ArrayList<>();
            market_values.clear();
            market_values.add(record_id);
            market_values.add(db2.getRecord("report_propdesc_tct_no", where, args, Tables.land_improvements_lot_details.table_name).get(x));
            market_values.add(db2.getRecord("report_propdesc_lot", where, args, Tables.land_improvements_lot_details.table_name).get(x));
            market_values.add(db2.getRecord("report_propdesc_block", where, args, Tables.land_improvements_lot_details.table_name).get(x));
            market_values.add(db2.getRecord("report_propdesc_area", where, args, Tables.land_improvements_lot_details.table_name).get(x));
            market_values.add("0");
            market_values.add("0");
            market_values.add("0");
            market_values.add("0");
            db_new.addRecord(market_values, market_fields, Tables.land_improvements_lot_valuation_details.table_name);
        }
    }
    }

    private class Attachment_validation extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pDialog = new ProgressDialog(Vacant_lot.this);
            pDialog.setMessage("Checking attachment..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            NetworkUtil.getConnectivityStatusString(Vacant_lot.this);
            if (!NetworkUtil.status.equals("Network not available")) {
                google_response = gds.google_ping();
                if (google_response.equals("200")) {
                    try {
                        URL obj = new URL(url_webby);
                        HttpsURLConnection con;
                        HttpURLConnection con2;
                        if (Global.type.contentEquals("tls")) {
                            con = tlscon.setUpHttpsConnection("" + obj);
                            con.setRequestMethod("GET");
                            con.setRequestProperty("User-Agent", "Mozilla/5.0");
                            responseCode = con.getResponseCode();
                        } else {
                            con2 = (HttpURLConnection) obj.openConnection();
                            con2.setRequestMethod("GET");
                            con2.setRequestProperty("User-Agent", "Mozilla/5.0");
                            responseCode = con2.getResponseCode();
                        }
                        network_status = true;
                        Log.e("", String.valueOf(responseCode));
                    } catch (MalformedURLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (ProtocolException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            } else {
                network_status = false;

            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            Log.e("", url_webby);
            if (network_status == false) {
                Toast.makeText(getApplicationContext(), "No network Available",
                        Toast.LENGTH_SHORT).show();
            } else if (network_status == true) {
                if (responseCode == 200) {
                    view_download();
                } else if (responseCode == 404) {
                    Toast.makeText(getApplicationContext(),
                            "Attachment doesn't exist", Toast.LENGTH_SHORT)
                            .show();
                }
            }
            pDialog.dismiss();

        }
    }

	/*public void google_ping() {
		try {
//			URL obj = new URL("https://www.google.com.ph/?gws_rd=cr&ei=td9kUrP0H8mjigenuoHAAg");
			URL obj = new URL(gs.server_url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");

			// add request header
			con.setRequestProperty("User-Agent", "Mozilla/5.0");

			google_response = String.valueOf(con.getResponseCode());
			Log.e("google ping: ", google_response);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/

    /**
     * Showing Dialog
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type:
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.setCanceledOnTouchOutside(false);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    /**
     * Background Async Task to download file
     */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                // Output stream to write file
                OutputStream output = new FileOutputStream(
                        uploaded_file.toString());

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            removeDialog(progress_bar_type);
            Toast.makeText(getApplicationContext(), "Attachment Downloaded",
                    Toast.LENGTH_LONG).show();
            view_pdf(uploaded_file);

        }

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        Intent in;
        switch (v.getId()) {
            case R.id.btn_report_page1://lot details
                in = new Intent(getApplicationContext(), VL_Lot_Details.class);
                in.putExtra(TAG_RECORD_ID, record_id);
                startActivityForResult(in, 100);
                break;
            case R.id.btn_report_page2://land details
                custom_dialog_page2();
                break;
            case R.id.btn_report_page3://valuation
                in = new Intent(getApplicationContext(), VL_Market_Valuation.class);
                in.putExtra(TAG_RECORD_ID, record_id);
                startActivityForResult(in, 100);
                break;
            case R.id.btn_report_page4:
                custom_dialog_summary();
                break;
            case R.id.btn_update_address: //address
                custom_dialog_address();
                break;
            case R.id.btn_update_zonal: //zonal values
                custom_dialog_zonal();
                break;
            case R.id.btn_comparatives: //comparatives
                in = new Intent(getApplicationContext(), VL_Comparatives.class);
                in.putExtra(TAG_RECORD_ID, record_id);
                startActivityForResult(in, 100);
                break;
            case R.id.btn_mysql_attachments: //attachments
                //in = new Intent(getApplicationContext(), Uploaded_Attachments_All.class);
                in = new Intent(getApplicationContext(), Attachments_Inflater.class);
                in.putExtra(TAG_RECORD_ID, record_id);
                in.putExtra(TAG_PASS_STAT, pass_stat);
                in.putExtra(TAG_UID, uid);
                startActivityForResult(in, 100);
                break;
            case R.id.btn_report_update_cc:
                //update_dialog();
                check_attachment();
                break;
            case R.id.btn_report_view_report:
                globalSave = true;
                new GlobalSave().execute();
                break;
            case R.id.btn_report_pdf:
                //create_pdf();
                Intent createPDF = new Intent(getApplicationContext(), Collage.class);//.putExtra("position", position);
                createPDF.putExtra(TAG_RECORD_ID, record_id);
                startActivity(createPDF);
                break;
            case R.id.btn_select_pdf:
                open_customdialog();
                break;
            case R.id.tv_attachment:
                open_pdf();
                break;
            case R.id.btn_switch_app_type:
                switch_appraisal_type();
                break;
            case R.id.btn_update_app_details:
                custom_dialog_app_details();
                break;

            case R.id.btn_manual_delete:
                manual_delete_all();
                break;


            case R.id.tv_remarks:
                remarks_dialog();
                break;

            default:
                break;
        }
    }

    public void custom_dialog_app_details() {
        myDialog = new Dialog(Vacant_lot.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.app_details_dialog);
        myDialog.setCancelable(true);

        android.view.WindowManager.LayoutParams params = myDialog.getWindow()
                .getAttributes();
        params.height = LayoutParams.WRAP_CONTENT;
        params.width = LayoutParams.FILL_PARENT;
        myDialog.getWindow().setAttributes(
                (android.view.WindowManager.LayoutParams) params);

        report_date_requested = (EditText) myDialog.findViewById(R.id.report_date_requested);
        et_first_name = (EditText) myDialog.findViewById(R.id.et_first_name);
        et_middle_name = (EditText) myDialog.findViewById(R.id.et_middle_name);
        et_last_name = (EditText) myDialog.findViewById(R.id.et_last_name);
        et_requesting_party = (AutoCompleteTextView) myDialog.findViewById(R.id.et_requesting_party);
        et_requestor = (EditText) myDialog.findViewById(R.id.et_requestor);
        app_account_company_name = (EditText) myDialog.findViewById(R.id.app_account_company_name);
        app_account_is_company = (CheckBox) myDialog.findViewById(R.id.app_account_is_company);
        spinner_purpose_appraisal = (Spinner) myDialog.findViewById(R.id.app_purpose_appraisal);
        et_app_kind_of_appraisal = (EditText) myDialog.findViewById(R.id.app_kind_of_appraisal);

        gds.autocomplete(et_requesting_party, getResources().getStringArray(R.array.requesting_party), this);
        if(app_account_is_company.isChecked()){
            app_account_company_name.setEnabled(true);
            et_first_name.setText("-");
            et_middle_name.setText("-");
            et_last_name.setText("-");

            et_first_name.setEnabled(false);
            et_middle_name.setEnabled(false);
            et_last_name.setEnabled(false);
        }else{
            app_account_company_name.setEnabled(false);
            app_account_company_name.setText("-");
            et_first_name.setEnabled(true);
            et_middle_name.setEnabled(true);
            et_last_name.setEnabled(true);
        }
        app_account_is_company.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    app_account_company_name.setEnabled(true);
                    et_first_name.setText("-");
                    et_middle_name.setText("-");
                    et_last_name.setText("-");
                    et_first_name.setEnabled(false);
                    et_middle_name.setEnabled(false);
                    et_last_name.setEnabled(false);
                } else {
                    app_account_company_name.setEnabled(false);
                    app_account_company_name.setText("-");
                    et_first_name.setEnabled(true);
                    et_middle_name.setEnabled(true);
                    et_last_name.setEnabled(true);
                }
            }
        });


        btn_save_app_details = (Button) myDialog.findViewById(R.id.btn_right);
        btn_cancel_app_details = (Button) myDialog.findViewById(R.id.btn_left);
        btn_save_app_details.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method

                boolean fieldsOK,fieldsOK2;
                gds.fill_in_error(new EditText[]{
                        et_first_name,et_last_name});
                fieldsOK = gds.validate(new EditText[]{
                        et_first_name,et_last_name});

                if(app_account_is_company.isChecked()){
                    gds.fill_in_error(new EditText[]{
                            app_account_company_name});
                    fieldsOK2 = gds.validate(new EditText[]{
                            app_account_company_name});
                }else{
                    fieldsOK2=true;
                }

                if(fieldsOK&&fieldsOK2) {
                    Report_Accepted_Jobs mv = new Report_Accepted_Jobs();


                    mv.setfname(et_first_name.getText().toString());
                    mv.setmname(et_middle_name.getText().toString());
                    mv.setlname(et_last_name.getText().toString());
                    mv.setrequesting_party(et_requesting_party.getText().toString());
                    mv.setrequestor(et_requestor.getText().toString());

                    db.updateApp_Details(mv, record_id);
                    field.clear();
                    field.add("app_account_is_company");
                    field.add("app_account_company_name");
                    field.add("nature_appraisal");
                    field.add("kind_of_appraisal");
                    value.clear();
                    value.add(gds.cbChecker(app_account_is_company));
                    value.add(app_account_company_name.getText().toString());
                    value.add(spinner_purpose_appraisal.getSelectedItem().toString());
                    value.add(et_app_kind_of_appraisal.getText().toString());
                    db2.updateRecord(value,field,"record_id = ?",new String[] { record_id },"tbl_report_accepted_jobs");
                    db.close();
                    Toast.makeText(getApplicationContext(), "Saved",
                            Toast.LENGTH_SHORT).show();
                    myDialog.dismiss();
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(),
                            "Please fill up required fields", Toast.LENGTH_SHORT).show();

                }

            }
        });

        btn_cancel_app_details.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method
                myDialog.dismiss();
            }
        });

        List<Report_Accepted_Jobs> report_accepted_jobs = db
                .getReport_Accepted_Jobs(record_id);
        if (!report_accepted_jobs.isEmpty()) {
            for (Report_Accepted_Jobs im : report_accepted_jobs) {
                et_first_name.setText(im.getfname());
                et_middle_name.setText(im.getmname());
                et_last_name.setText(im.getlname());
                et_requesting_party.setText(im.getrequesting_party());
                et_requestor.setText(im.getrequestor());
                gds.cbDisplay(app_account_is_company, db2.getRecord("app_account_is_company", where, args, "tbl_report_accepted_jobs").get(0));
                app_account_company_name.setText(db2.getRecord("app_account_company_name", where, args, "tbl_report_accepted_jobs").get(0));
                //valor2
                spinner_purpose_appraisal.setSelection(gds.spinnervalue(spinner_purpose_appraisal, db2.getRecord("nature_appraisal", where, args, "tbl_report_accepted_jobs").get(0)));
                et_app_kind_of_appraisal.setText(db2.getRecord("kind_of_appraisal", where, args, "tbl_report_accepted_jobs").get(0));

                // set current date into datepicker
                if ((!im.getdr_year().equals("")) && (!im.getdr_month().equals("")) && (!im.getdr_day().equals(""))) {
                    report_date_requested.setText(im.getdr_month() + "/" + im.getdr_day() + "/" + im.getdr_year());
                } else {
                    report_date_requested.setText("");
                }
            }
        }

        myDialog.show();
    }

    @Override
    public void onCheckedChanged(CompoundButton bv, boolean isChecked) {
        // TODO Auto-generated method stub
        switch (bv.getId()) {
            case R.id.concerns_minor_others:
                if (isChecked) {
                    concerns_minor_others_desc.setVisibility(View.VISIBLE);
                } else {
                    concerns_minor_others_desc.setVisibility(View.GONE);
                    concerns_minor_others_desc.setText("");
                }
                break;
            case R.id.concerns_major_others:
                if (isChecked) {
                    concerns_major_others_desc.setVisibility(View.VISIBLE);
                } else {
                    concerns_major_others_desc.setVisibility(View.GONE);
                    concerns_major_others_desc.setText("");
                }
                break;
            default:
                break;
        }
    }



    /**
     * for FOC SCA REM REQ
     */
    CheckBox foc_cb0, foc_cb1, foc_cb2, foc_cb3, foc_cb4, foc_cb5, foc_cb6, foc_cb7, foc_cb8, foc_cb9, foc_cb10, foc_cb11;
    Button b_ok, b_cancel;
    View v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10;

    public void dialog_declaration() {
        myDialog2 = new Dialog(Vacant_lot.this);
        myDialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog2.setContentView(R.layout.summary_foc);
        myDialog2.setCancelable(true);

        android.view.WindowManager.LayoutParams params = myDialog2.getWindow()
                .getAttributes();
        params.height = LayoutParams.WRAP_CONTENT;
        params.width = LayoutParams.MATCH_PARENT;
        myDialog2.getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        foc_cb0 = (CheckBox) myDialog2.findViewById(R.id.foc_cb0);
        foc_cb1 = (CheckBox) myDialog2.findViewById(R.id.foc_cb1);
        foc_cb2 = (CheckBox) myDialog2.findViewById(R.id.foc_cb2);
        foc_cb3 = (CheckBox) myDialog2.findViewById(R.id.foc_cb3);
        foc_cb4 = (CheckBox) myDialog2.findViewById(R.id.foc_cb4);
        foc_cb5 = (CheckBox) myDialog2.findViewById(R.id.foc_cb5);
        foc_cb6 = (CheckBox) myDialog2.findViewById(R.id.foc_cb6);
        foc_cb7 = (CheckBox) myDialog2.findViewById(R.id.foc_cb7);
        foc_cb8 = (CheckBox) myDialog2.findViewById(R.id.foc_cb8);
        foc_cb9 = (CheckBox) myDialog2.findViewById(R.id.foc_cb9);
        foc_cb10 = (CheckBox) myDialog2.findViewById(R.id.foc_cb10);
        foc_cb11 = (CheckBox) myDialog2.findViewById(R.id.foc_cb11);

        v0 = (View) myDialog2.findViewById(R.id.v0);
        v1 = (View) myDialog2.findViewById(R.id.v1);
        v2 = (View) myDialog2.findViewById(R.id.v2);
        v3 = (View) myDialog2.findViewById(R.id.v3);
        v4 = (View) myDialog2.findViewById(R.id.v4);
        v5 = (View) myDialog2.findViewById(R.id.v5);
        v6 = (View) myDialog2.findViewById(R.id.v6);
        v7 = (View) myDialog2.findViewById(R.id.v7);
        v8 = (View) myDialog2.findViewById(R.id.v8);
        v9 = (View) myDialog2.findViewById(R.id.v9);
        v10 = (View) myDialog2.findViewById(R.id.v10);

        b_ok = (Button) myDialog2.findViewById(R.id.b_ok);
        b_cancel = (Button) myDialog2.findViewById(R.id.b_cancel);
    }

	/*private void foc_dialog(){
		dialog_declaration();

		foc_cb8.setVisibility(View.GONE);
		foc_cb9.setVisibility(View.GONE);
		foc_cb10.setVisibility(View.GONE);
		foc_cb11.setVisibility(View.GONE);

		v7.setVisibility(View.GONE);
		v8.setVisibility(View.GONE);
		v9.setVisibility(View.GONE);
		v10.setVisibility(View.GONE);

		Resources res = getResources();
		final String[] ary_foc = res.getStringArray(R.array.ary_foc);
		final String[] ary_sca = res.getStringArray(R.array.ary_sca);

		final CheckBox[] foc_cb = {
				foc_cb0,
				foc_cb1,
				foc_cb2,
				foc_cb3,
				foc_cb4,
				foc_cb5,
				foc_cb6,
				foc_cb7,
				foc_cb8,
				foc_cb9,
				foc_cb10,
				foc_cb11
				};

		for (int x = 0; x < ary_foc.length; x++){
			foc_cb[x].setText(ary_foc[x]);
		}
		b_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String foc = "";
				String sca = "";
				int number = 1;

				int numberfoc = 1;

				for (int x = 0; x < ary_foc.length; x++){
					if (foc_cb[x].isChecked()){
						foc = foc + String.valueOf(numberfoc) +". "+ ary_foc[x] + "\n";


						if ((x==0)&&(foc_cb[0].isChecked()&&(!foc_cb[1].isChecked()))){
							sca = sca + String.valueOf(number) +". "+ ary_sca[0] + "\n";
						} else if ((x==0)&&(!foc_cb[0].isChecked()&&(foc_cb[1].isChecked()))){
							sca = sca + String.valueOf(number) +". "+ ary_sca[0] + "\n";
						} else if ((x==0)&&(foc_cb[0].isChecked()&&(foc_cb[1].isChecked()))){
							number--;
						} else if ((x!=0)||(x!=1)){
							sca = sca + String.valueOf(number) +". "+ ary_sca[x-1] + "\n";
						}
						numberfoc++;
						number++;
					}
				}
				report_factors_of_concern.setText(foc);
				report_suggested_corrective_actions.setText(sca);

				myDialog2.dismiss();
			}
		});
		b_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog2.dismiss();
			}
		});
		myDialog2.show();
	}

	private void sca_dialog(){
	}*/

    private void remarks_dialog() {
        dialog_declaration();

        Resources res = getResources();
        final String[] ary_remarks = res.getStringArray(R.array.ary_remarks);

        final CheckBox[] foc_cb = {
                foc_cb0,
                foc_cb1,
                foc_cb2,
                foc_cb3,
                foc_cb4,
                foc_cb5,
                foc_cb6,
                foc_cb7,
                foc_cb8,
                foc_cb9,
                foc_cb10,
                foc_cb11
        };

        for (int x = 0; x < ary_remarks.length; x++) {
            foc_cb[x].setText(ary_remarks[x]);
        }
        b_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String foc = "";
                int number = 1;
                for (int x = 0; x < ary_remarks.length; x++) {
                    if (foc_cb[x].isChecked()) {
                        foc = foc + String.valueOf(number) + ". " + ary_remarks[x] + "\n";
                        number++;
                    }
                }
                report_remarks.setText(foc);
                myDialog2.dismiss();
            }
        });
        b_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog2.dismiss();
            }
        });
        myDialog2.show();
    }

	/*private void requirements_dialog(){
		dialog_declaration();

		foc_cb2.setVisibility(View.GONE);
		foc_cb3.setVisibility(View.GONE);
		foc_cb4.setVisibility(View.GONE);
		foc_cb5.setVisibility(View.GONE);
		foc_cb6.setVisibility(View.GONE);
		foc_cb7.setVisibility(View.GONE);
		foc_cb8.setVisibility(View.GONE);
		foc_cb9.setVisibility(View.GONE);
		foc_cb10.setVisibility(View.GONE);
		foc_cb11.setVisibility(View.GONE);

		v1.setVisibility(View.GONE);
		v2.setVisibility(View.GONE);
		v3.setVisibility(View.GONE);
		v4.setVisibility(View.GONE);
		v5.setVisibility(View.GONE);
		v6.setVisibility(View.GONE);
		v7.setVisibility(View.GONE);
		v8.setVisibility(View.GONE);
		v9.setVisibility(View.GONE);
		v10.setVisibility(View.GONE);

		Resources res = getResources();
		final String[] ary_requirements = res.getStringArray(R.array.ary_requirements);

		final CheckBox[] foc_cb = {
				foc_cb0,
				foc_cb1,
				foc_cb2,
				foc_cb3,
				foc_cb4,
				foc_cb5,
				foc_cb6,
				foc_cb7,
				foc_cb8,
				foc_cb9,
				foc_cb10,
				foc_cb11
				};

		for (int x = 0; x < ary_requirements.length; x++){
			foc_cb[x].setText(ary_requirements[x]);
		}
		b_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String foc = "";
				int number = 1;
				for (int x = 0; x < ary_requirements.length; x++){
					if (foc_cb[x].isChecked()){
						foc = foc + String.valueOf(number) +". "+ ary_requirements[x] + "\n";
						number++;
					}
				}
				report_requirements.setText(foc);
				myDialog2.dismiss();
			}
		});
		b_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog2.dismiss();
			}
		});
		myDialog2.show();
	}*/

    public void statusDisplay() {
        List<Logs> sd = db.getLogsLatest(record_id);
        if (!sd.isEmpty()) {
            for (Logs si : sd) {
                actionBar.setSubtitle(si.getstatus_reason() + " (" + si.getdate_submitted() + ")");
            }
        } else {
            actionBar.setSubtitle("Not yet submitted");
        }
    }

    public void logsDisplay() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.submission_logs);
        android.view.WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.height = LayoutParams.WRAP_CONTENT;
        params.width = LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        TextView tvLog = (TextView) dialog.findViewById(R.id.tvLog);
        String logs = "";
        List<Logs> sd = db.getLogs(record_id);
        if (!sd.isEmpty()) {
            for (Logs si : sd) {
                logs = logs + si.getdate_submitted() + "\t" + si.getstatus() + " - " + si.getstatus_reason() + "\n";
            }
        }
        tvLog.setText(logs);
        dialog.show();
    }

    public void view_map() {
        String add = tv_address_hide.getText().toString();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Vacant_lot.this);
        alertDialog.setTitle("Address");
        alertDialog.setMessage("Edit Address");

        final EditText input = new EditText(Vacant_lot.this);
        input.setText(add);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);
        alertDialog.setIcon(R.drawable.map_icon);

        alertDialog.setPositiveButton("Search",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String address = input.getText().toString();
                        Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + address);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        startActivity(mapIntent);

                    }
                });

        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }
    private boolean validateSpinner(Spinner[] fields) {
        for (int i = 0; i < fields.length; i++) {
            Spinner currentField = fields[i];
            if (currentField.getSelectedItem().toString().length() <= 0) {
                return false;
            }
        }
        return true;
    }
    @Override
    public void onUserInteraction() {
        st.resetDisconnectTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        st.stopDisconnectTimer();
    }

    @Override
    public void onResume() {
        super.onResume();
        st.resetDisconnectTimer();
    }
}