package com.gds.appraisalmaybank.vacant_lot;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database2.Vacant_Lot_API_RDPS;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class Vacant_Lot_RDPS extends ListActivity implements OnClickListener {
	DatabaseHandler2 db = new DatabaseHandler2(this);
	GDS_methods gds = new GDS_methods();
	Button btn_add_rdps, btn_save_rdps;
	Button btn_create, btn_cancel, btn_save;

	Session_Timer st = new Session_Timer(this);
	String record_id="";
	private static final String TAG_RECORD_ID = "record_id";
	
	Dialog myDialog;
	//create dialog
	EditText  report_account_name, report_location,	report_area, report_value, report_date_appraised;

	//forListViewDeclarations
	private ProgressDialog pDialog;
	ArrayList<HashMap<String, String>> itemList;
	private static final String tag_rdps_id = "rdps_id";
	private static final String tag_account_name = "account_name";
	private static final String tag_prev_app_date = "prev_app_date";
	String rdps_id="";
	Calendar myCalendar = Calendar.getInstance();
	DatePickerDialog.OnDateSetListener date;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reports_rdps);

		st.resetDisconnectTimer();
		
		btn_add_rdps = (Button) findViewById(R.id.btn_add_rdps);
		btn_add_rdps.setOnClickListener(this);
		
		btn_save_rdps = (Button) findViewById(R.id.btn_save_rdps);
		btn_save_rdps.setOnClickListener(this);
		
		// getting record_id from intent / previous class
		Intent i = getIntent();
		record_id = i.getStringExtra(TAG_RECORD_ID);
		//list
		itemList = new ArrayList<HashMap<String, String>>();
		// Loading in Background Thread
		new LoadAll().execute();
		// Get listview
		ListView lv = getListView();

		// on seleting single item
		// launching Edit item Screen
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// getting values from selected ListItem
				rdps_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
				view_details();
			}
		});
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
                // TODO Auto-generated method stub

                Log.v("long clicked","pos: " + position);
                rdps_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
                delete_item();
                return true;
            }
        }); 
		
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		this.finish();
		Intent intent = new Intent(Vacant_Lot_RDPS.this, Vacant_lot.class);
		intent.putExtra("keyopen","1");
		startActivity(intent);
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.btn_add_rdps:
			add_new();
			break;
		case R.id.btn_save_rdps:
			this.finish();
			Intent intent = new Intent(Vacant_Lot_RDPS.this, Vacant_lot.class);
			intent.putExtra("keyopen","1");
			startActivity(intent);
			break;
		default:
			break;
		}
	}
	
	public void add_new(){
		Toast.makeText(getApplicationContext(), "Add New",
				Toast.LENGTH_SHORT).show();
		myDialog = new Dialog(Vacant_Lot_RDPS.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.reports_rdps_form);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		
		btn_create = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_create.setText("Create");
		btn_cancel.setText("Cancel");
		report_account_name = (EditText) myDialog.findViewById(R.id.report_account_name);
		report_location = (EditText) myDialog.findViewById(R.id.report_location);
		report_area = (EditText) myDialog.findViewById(R.id.report_area);
		report_value = (EditText) myDialog.findViewById(R.id.report_value);
		report_date_appraised = (EditText) myDialog.findViewById(R.id.report_date_appraised);

		report_area.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_area, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_value.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_value, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});

			date = new DatePickerDialog.OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
								  int dayOfMonth) {
				// TODO Auto-generated method stub
				myCalendar.set(Calendar.YEAR, year);
				myCalendar.set(Calendar.MONTH, monthOfYear);
				myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				updateLabel();
			}

		};
		report_date_appraised.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new DatePickerDialog(Vacant_Lot_RDPS.this, date, myCalendar
						.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
						myCalendar.get(Calendar.DAY_OF_MONTH)).show();
			}
		});
		btn_create.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

					//add data in SQLite
					db.addVacant_Lot_RDPS(new Vacant_Lot_API_RDPS(
							record_id,
							report_account_name.getText().toString(),
							report_location.getText().toString(),
							gds.replaceFormat(report_area.getText().toString()),
							gds.replaceFormat(report_value.getText().toString()),
							report_date_appraised.getText().toString()));
					
						Toast.makeText(getApplicationContext(),"data created", Toast.LENGTH_SHORT).show();
						myDialog.dismiss();
						reloadClass();

				
			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		myDialog.show();
	}
	
	public void view_details(){
		myDialog = new Dialog(Vacant_Lot_RDPS.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.reports_rdps_form);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		
		btn_save = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_save.setText("Save");
		btn_cancel.setText("Cancel");

		
		report_account_name = (EditText) myDialog.findViewById(R.id.report_account_name);
		report_location = (EditText) myDialog.findViewById(R.id.report_location);
		report_area = (EditText) myDialog.findViewById(R.id.report_area);
		report_value = (EditText) myDialog.findViewById(R.id.report_value);
		report_date_appraised = (EditText) myDialog.findViewById(R.id.report_date_appraised);

		report_area.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_area, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_value.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_value, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});




		date = new DatePickerDialog.OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
								  int dayOfMonth) {
				// TODO Auto-generated method stub
				myCalendar.set(Calendar.YEAR, year);
				myCalendar.set(Calendar.MONTH, monthOfYear);
				myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				updateLabel();
			}

		};
		report_date_appraised.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new DatePickerDialog(Vacant_Lot_RDPS.this, date, myCalendar
						.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
						myCalendar.get(Calendar.DAY_OF_MONTH)).show();
			}
		});
		List<Vacant_Lot_API_RDPS> rdpsList = db
				.getVacant_Lot_RDPS_Single(String.valueOf(record_id), String.valueOf(rdps_id));
		if (!rdpsList.isEmpty()) {
			for (Vacant_Lot_API_RDPS li : rdpsList) {
				report_account_name.setText(li.getreport_prev_app_name());
				report_location.setText(li.getreport_prev_app_location());
				report_area.setText(gds.numberFormat(li.getreport_prev_app_area()));
				report_value.setText(gds.numberFormat(li.getreport_prev_app_value()));
				report_date_appraised.setText(li.getreport_prev_app_date());
			}
		}
		
		btn_save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//list of EditTexts put in array

					Vacant_Lot_API_RDPS li = new Vacant_Lot_API_RDPS();
					li.setreport_prev_app_name(report_account_name.getText().toString());
					li.setreport_prev_app_location(report_location.getText().toString());
					li.setreport_prev_app_area(gds.replaceFormat(report_area.getText().toString()));
					li.setreport_prev_app_value(gds.replaceFormat(report_value.getText().toString()));
					li.setreport_prev_app_date(report_date_appraised.getText().toString());

					db.updateVacant_Lot_RDPS(li, record_id, String.valueOf(rdps_id));
					db.close();
					myDialog.dismiss();
					Toast.makeText(getApplicationContext(), "Saved",Toast.LENGTH_SHORT).show();
					reloadClass();

			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		myDialog.show();
	}

	private void updateLabel() {

		String myFormat = "MM/dd/yyyy"; //In which you need put here
		SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

		report_date_appraised.setText(sdf.format(myCalendar.getTime()));
	}
	public void reloadClass(){
		Intent intent = getIntent();
		finish();
		startActivity(intent);
	}
	public void delete_item(){
		myDialog = new Dialog(Vacant_Lot_RDPS.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.delete_dialog);
		myDialog.setCancelable(true);
		final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
		final Button btn_yes = (Button) myDialog.findViewById(R.id.btn_yes);
		final Button btn_no = (Button) myDialog.findViewById(R.id.btn_no);
		tv_question.setText("Are you sure you want to delete the selected item?");
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				db.deleteVacant_Lot_RDPS_Single(record_id, rdps_id);
				db.close();
				myDialog.dismiss();
				reloadClass();
				Toast.makeText(getApplicationContext(), "Data Deleted",Toast.LENGTH_SHORT).show();
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		
		myDialog.show();
	}
	
	
	/**
	 * Background Async Task to Load all
	 * */
	class LoadAll extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Vacant_Lot_RDPS.this);
			pDialog.setMessage("Loading Lists. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All
		 * */
		protected String doInBackground(String... args) {
			String account_name="", prev_app_date="";
			

			List<Vacant_Lot_API_RDPS> lirdps = db.getVacant_Lot_RDPS(record_id);
			if (!lirdps.isEmpty()) {
				for (Vacant_Lot_API_RDPS imrdps : lirdps) {
					rdps_id = String.valueOf(imrdps.getID());
					record_id = imrdps.getrecord_id();					
					account_name = imrdps.getreport_prev_app_name();
					prev_app_date = imrdps.getreport_prev_app_date();
					
					HashMap<String, String> map = new HashMap<String, String>();
					map.put(TAG_RECORD_ID, record_id);
					map.put(tag_rdps_id, rdps_id);
					map.put(tag_account_name, account_name);
					map.put(tag_prev_app_date, prev_app_date);

					// adding HashList to ArrayList
					itemList.add(map);
				}
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					ListAdapter adapter = new SimpleAdapter(
							Vacant_Lot_RDPS.this, itemList,
							R.layout.reports_rdps_list, new String[] { TAG_RECORD_ID, tag_rdps_id, tag_account_name,
									tag_prev_app_date},
									new int[] { R.id.report_record_id, R.id.primary_key, R.id.item_account_name, R.id.item_date_appraised});
					// updating listview
					setListAdapter(adapter);
				}
			});
			if(itemList.isEmpty()){
				add_new();
			}
		}
	}

	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}




}