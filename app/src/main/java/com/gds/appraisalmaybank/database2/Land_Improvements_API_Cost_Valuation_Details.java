package com.gds.appraisalmaybank.database2;

public class Land_Improvements_API_Cost_Valuation_Details {
	//private variables
	int valuation_details_id;
	String record_id;
	String imp_valuation_id;
	String report_imp_value_kind;
	String report_imp_value_area;
	String report_imp_value_cost_per_sqm;
	String report_imp_value_reproduction_cost;
	
	// Empty constructor
	public Land_Improvements_API_Cost_Valuation_Details() {

	}

	public Land_Improvements_API_Cost_Valuation_Details(String record_id) {
		this.record_id = record_id;
	}
	
	public Land_Improvements_API_Cost_Valuation_Details(
			int valuation_details_id, String record_id,
			String imp_valuation_id, String report_imp_value_kind,
			String report_imp_value_area, String report_imp_value_cost_per_sqm,
			String report_imp_value_reproduction_cost) {
		this.valuation_details_id = valuation_details_id;
		this.record_id = record_id;
		this.imp_valuation_id = imp_valuation_id;
		this.report_imp_value_kind = report_imp_value_kind;
		this.report_imp_value_area = report_imp_value_area;
		this.report_imp_value_cost_per_sqm = report_imp_value_cost_per_sqm;
		this.report_imp_value_reproduction_cost = report_imp_value_reproduction_cost;
	}
	
	public Land_Improvements_API_Cost_Valuation_Details(String record_id,
			String imp_valuation_id, String report_imp_value_kind,
			String report_imp_value_area, String report_imp_value_cost_per_sqm,
			String report_imp_value_reproduction_cost) {
		this.record_id = record_id;
		this.imp_valuation_id = imp_valuation_id;
		this.report_imp_value_kind = report_imp_value_kind;
		this.report_imp_value_area = report_imp_value_area;
		this.report_imp_value_cost_per_sqm = report_imp_value_cost_per_sqm;
		this.report_imp_value_reproduction_cost = report_imp_value_reproduction_cost;
	}
	
	public Land_Improvements_API_Cost_Valuation_Details(
			String imp_valuation_id, String report_imp_value_kind,
			String report_imp_value_area, String report_imp_value_cost_per_sqm,
			String report_imp_value_reproduction_cost) {
		this.imp_valuation_id = imp_valuation_id;
		this.report_imp_value_kind = report_imp_value_kind;
		this.report_imp_value_area = report_imp_value_area;
		this.report_imp_value_cost_per_sqm = report_imp_value_cost_per_sqm;
		this.report_imp_value_reproduction_cost = report_imp_value_reproduction_cost;
	}
	
	public Land_Improvements_API_Cost_Valuation_Details(
			String report_imp_value_kind, String report_imp_value_area,
			String report_imp_value_cost_per_sqm,
			String report_imp_value_reproduction_cost) {
		this.report_imp_value_kind = report_imp_value_kind;
		this.report_imp_value_area = report_imp_value_area;
		this.report_imp_value_cost_per_sqm = report_imp_value_cost_per_sqm;
		this.report_imp_value_reproduction_cost = report_imp_value_reproduction_cost;
	}
	
	// getting ID
	public int getID() {
		return this.valuation_details_id;
	}

	// setting id
	public void setID(int valuation_details_id) {
		this.valuation_details_id = valuation_details_id;
	}

	public String getrecord_id() {
		return this.record_id;
	}

	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}
	
	//getters
	public String getimp_valuation_id() {
		return this.imp_valuation_id;
	}
	
	public String getreport_imp_value_kind() {
		return this.report_imp_value_kind;
	}

	public String getreport_imp_value_area() {
		return this.report_imp_value_area;
	}

	public String getreport_imp_value_cost_per_sqm() {
		return this.report_imp_value_cost_per_sqm;
	}

	public String getreport_imp_value_reproduction_cost() {
		return this.report_imp_value_reproduction_cost;
	}
	
	//setters
	public void setimp_valuation_id(String imp_valuation_id) {
		this.imp_valuation_id = imp_valuation_id;
	}

	public void setreport_imp_value_kind(String report_imp_value_kind) {
		this.report_imp_value_kind = report_imp_value_kind;
	}

	public void setreport_imp_value_area(String report_imp_value_area) {
		this.report_imp_value_area = report_imp_value_area;
	}

	public void setreport_imp_value_cost_per_sqm(
			String report_imp_value_cost_per_sqm) {
		this.report_imp_value_cost_per_sqm = report_imp_value_cost_per_sqm;
	}

	public void setreport_imp_value_reproduction_cost(
			String report_imp_value_reproduction_cost) {
		this.report_imp_value_reproduction_cost = report_imp_value_reproduction_cost;
	}
	
}
