package com.gds.appraisalmaybank.database2;

public class Condo_API_Title_Details {
	// private variables
	int id;
	String record_id;
	String report_propdesc_property_type;
	String report_propdesc_cct_no;
	String report_propdesc_unit_no;
	String report_propdesc_floor;
	String report_propdesc_bldg_name;
	String report_propdesc_area;
	String valrep_condo_propdesc_registry_date;
	String report_propdesc_reg_of_deeds;
	String report_propdesc_registered_owner;

	// Empty constructor
	public Condo_API_Title_Details() {

	}

	public Condo_API_Title_Details(String record_id) {
		this.record_id = record_id;
	}

	public Condo_API_Title_Details(int id, String record_id,
			String report_propdesc_property_type,
			String report_propdesc_cct_no, String report_propdesc_unit_no,
			String report_propdesc_floor, String report_propdesc_bldg_name,
			String report_propdesc_area,
			String valrep_condo_propdesc_registry_date,
			String report_propdesc_reg_of_deeds,
			String report_propdesc_registered_owner) {

		this.id = id;
		this.record_id = record_id;
		this.report_propdesc_property_type = report_propdesc_property_type;
		this.report_propdesc_cct_no = report_propdesc_cct_no;
		this.report_propdesc_unit_no = report_propdesc_unit_no;
		this.report_propdesc_floor = report_propdesc_floor;
		this.report_propdesc_bldg_name = report_propdesc_bldg_name;
		this.report_propdesc_area = report_propdesc_area;
		this.valrep_condo_propdesc_registry_date = valrep_condo_propdesc_registry_date;
		this.report_propdesc_reg_of_deeds = report_propdesc_reg_of_deeds;
		this.report_propdesc_registered_owner = report_propdesc_registered_owner;
	}

	// constructor without id
	public Condo_API_Title_Details(String record_id,
			String report_propdesc_property_type,
			String report_propdesc_cct_no, String report_propdesc_unit_no,
			String report_propdesc_floor, String report_propdesc_bldg_name,
			String report_propdesc_area,
			String valrep_condo_propdesc_registry_date,
			String report_propdesc_reg_of_deeds,
			String report_propdesc_registered_owner) {

		this.record_id = record_id;
		this.report_propdesc_property_type = report_propdesc_property_type;
		this.report_propdesc_cct_no = report_propdesc_cct_no;
		this.report_propdesc_unit_no = report_propdesc_unit_no;
		this.report_propdesc_floor = report_propdesc_floor;
		this.report_propdesc_bldg_name = report_propdesc_bldg_name;
		this.report_propdesc_area = report_propdesc_area;
		this.valrep_condo_propdesc_registry_date = valrep_condo_propdesc_registry_date;
		this.report_propdesc_reg_of_deeds = report_propdesc_reg_of_deeds;
		this.report_propdesc_registered_owner = report_propdesc_registered_owner;
	}

	// constructor without record_id
	public Condo_API_Title_Details(String report_propdesc_property_type,
			String report_propdesc_cct_no,
			String report_propdesc_unit_no, String report_propdesc_floor,
			String report_propdesc_bldg_name, String report_propdesc_area,
			String valrep_condo_propdesc_registry_date,
			String report_propdesc_reg_of_deeds,
			String report_propdesc_registered_owner) {
		this.report_propdesc_property_type = report_propdesc_property_type;
		this.report_propdesc_cct_no = report_propdesc_cct_no;
		this.report_propdesc_unit_no = report_propdesc_unit_no;
		this.report_propdesc_floor = report_propdesc_floor;
		this.report_propdesc_bldg_name = report_propdesc_bldg_name;
		this.report_propdesc_area = report_propdesc_area;
		this.valrep_condo_propdesc_registry_date = valrep_condo_propdesc_registry_date;
		this.report_propdesc_reg_of_deeds = report_propdesc_reg_of_deeds;
		this.report_propdesc_registered_owner = report_propdesc_registered_owner;
	}

	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	public String getrecord_id() {
		return this.record_id;
	}

	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}

	// getters
	public String getreport_propdesc_property_type() {
		return this.report_propdesc_property_type;
	}

	public String getreport_propdesc_cct_no() {
		return this.report_propdesc_cct_no;
	}

	public String getreport_propdesc_unit_no() {
		return this.report_propdesc_unit_no;
	}

	public String getreport_propdesc_floor() {
		return this.report_propdesc_floor;
	}

	public String getreport_propdesc_bldg_name() {
		return this.report_propdesc_bldg_name;
	}

	public String getreport_propdesc_area() {
		return this.report_propdesc_area;
	}

	public String getvalrep_condo_propdesc_registry_date() {
		return this.valrep_condo_propdesc_registry_date;
	}

	
	public String getreport_propdesc_reg_of_deeds(){
		return this.report_propdesc_reg_of_deeds;
	}
	
	public String getreport_propdesc_registered_owner() {
		return this.report_propdesc_registered_owner;
	}

	// setters
	public void setreport_propdesc_property_type(String report_propdesc_property_type) {
		this.report_propdesc_property_type = report_propdesc_property_type;
	}
	
	public void setreport_propdesc_cct_no(String report_propdesc_cct_no) {
		this.report_propdesc_cct_no = report_propdesc_cct_no;
	}

	public void setreport_propdesc_unit_no(String report_propdesc_unit_no) {
		this.report_propdesc_unit_no = report_propdesc_unit_no;
	}

	public void setreport_propdesc_floor(String report_propdesc_floor) {
		this.report_propdesc_floor = report_propdesc_floor;
	}

	public void setreport_propdesc_bldg_name(String report_propdesc_bldg_name) {
		this.report_propdesc_bldg_name = report_propdesc_bldg_name;
	}

	public void setreport_propdesc_area(String report_propdesc_area) {
		this.report_propdesc_area = report_propdesc_area;
	}

	public void setvalrep_condo_propdesc_registry_date(
			String valrep_condo_propdesc_registry_date) {
		this.valrep_condo_propdesc_registry_date = valrep_condo_propdesc_registry_date;
	}



	public void setreport_propdesc_reg_of_deeds(String report_propdesc_reg_of_deeds) {
		this.report_propdesc_reg_of_deeds = report_propdesc_reg_of_deeds;
	}
	
	public void setreport_propdesc_registered_owner(
			String report_propdesc_registered_owner) {
		this.report_propdesc_registered_owner = report_propdesc_registered_owner;
	}
}