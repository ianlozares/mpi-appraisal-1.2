package com.gds.appraisalmaybank.database2;

public class Townhouse_API_Lot_Details {
	// private variables
	int id;
	String record_id;
	String report_propdesc_tct_no;
	String report_propdesc_lot;
	String report_propdesc_block;
	String report_propdesc_survey_nos;
	String report_propdesc_area;
	String valrep_townhouse_propdesc_registry_date;
	String report_propdesc_registered_owner;
	String report_propdesc_registry_of_deeds;
	
	// Empty constructor
	public Townhouse_API_Lot_Details(){
		
	}
	
	public Townhouse_API_Lot_Details(String record_id) {
		this.record_id = record_id;
	}
	
	public Townhouse_API_Lot_Details(int id, String record_id,
			String report_propdesc_tct_no, String report_propdesc_lot,
			String report_propdesc_block, String report_propdesc_survey_nos,
			String report_propdesc_area,
			String valrep_townhouse_propdesc_registry_date,
			String report_propdesc_registered_owner,
			String report_propdesc_registry_of_deeds) {

		this.id = id;
		this.record_id = record_id;
		this.report_propdesc_tct_no = report_propdesc_tct_no;
		this.report_propdesc_lot = report_propdesc_lot;
		this.report_propdesc_block = report_propdesc_block;
		this.report_propdesc_survey_nos = report_propdesc_survey_nos;
		this.report_propdesc_area = report_propdesc_area;
		this.valrep_townhouse_propdesc_registry_date = valrep_townhouse_propdesc_registry_date;
		this.report_propdesc_registered_owner = report_propdesc_registered_owner;
		this.report_propdesc_registry_of_deeds = report_propdesc_registry_of_deeds;
	}
	
	//constructor without id
	public Townhouse_API_Lot_Details(String record_id,
			String report_propdesc_tct_no, String report_propdesc_lot,
			String report_propdesc_block, String report_propdesc_survey_nos,
			String report_propdesc_area,
			String valrep_townhouse_propdesc_registry_date,
			String report_propdesc_registered_owner,
			String report_propdesc_registry_of_deeds) {

		this.record_id = record_id;
		this.report_propdesc_tct_no = report_propdesc_tct_no;
		this.report_propdesc_lot = report_propdesc_lot;
		this.report_propdesc_block = report_propdesc_block;
		this.report_propdesc_survey_nos = report_propdesc_survey_nos;
		this.report_propdesc_area = report_propdesc_area;
		this.valrep_townhouse_propdesc_registry_date = valrep_townhouse_propdesc_registry_date;
		this.report_propdesc_registered_owner = report_propdesc_registered_owner;
		this.report_propdesc_registry_of_deeds = report_propdesc_registry_of_deeds;
	}
	
	// constructor without record_id
	public Townhouse_API_Lot_Details(String report_propdesc_tct_no,
			String report_propdesc_lot, String report_propdesc_block,
			String report_propdesc_survey_nos, String report_propdesc_area,
			String valrep_townhouse_propdesc_registry_date,
			String report_propdesc_registered_owner,
			String report_propdesc_registry_of_deeds) {

		this.report_propdesc_tct_no = report_propdesc_tct_no;
		this.report_propdesc_lot = report_propdesc_lot;
		this.report_propdesc_block = report_propdesc_block;
		this.report_propdesc_survey_nos = report_propdesc_survey_nos;
		this.report_propdesc_area = report_propdesc_area;
		this.valrep_townhouse_propdesc_registry_date = valrep_townhouse_propdesc_registry_date;
		this.report_propdesc_registered_owner = report_propdesc_registered_owner;
		this.report_propdesc_registry_of_deeds = report_propdesc_registry_of_deeds;
	}
	
	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	public String getrecord_id() {
		return this.record_id;
	}

	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}
	
	//getters
	public String getreport_propdesc_tct_no(){
		return this.report_propdesc_tct_no;
	}
	public String getreport_propdesc_lot(){
		return this.report_propdesc_lot;
	}
	public String getreport_propdesc_block(){
		return this.report_propdesc_block;
	}
	public String getreport_propdesc_survey_nos(){
		return this.report_propdesc_survey_nos;
	}
	public String getreport_propdesc_area(){
		return this.report_propdesc_area;
	}
	public String getvalrep_townhouse_propdesc_registry_date(){
		return this.valrep_townhouse_propdesc_registry_date;
	}

	public String getreport_propdesc_registered_owner(){
		return this.report_propdesc_registered_owner;
	}
	public String getreport_propdesc_registry_of_deeds(){
		return this.report_propdesc_registry_of_deeds;
	}
	
	//setters
	public void setreport_propdesc_tct_no(String report_propdesc_tct_no) {
		this.report_propdesc_tct_no = report_propdesc_tct_no;
	}

	public void setreport_propdesc_lot(String report_propdesc_lot) {
		this.report_propdesc_lot = report_propdesc_lot;
	}

	public void setreport_propdesc_block(String report_propdesc_block) {
		this.report_propdesc_block = report_propdesc_block;
	}

	public void setreport_propdesc_survey_nos(String report_propdesc_survey_nos) {
		this.report_propdesc_survey_nos = report_propdesc_survey_nos;
	}

	public void setreport_propdesc_area(String report_propdesc_area) {
		this.report_propdesc_area = report_propdesc_area;
	}

	public void setvalrep_townhouse_propdesc_registry_date(String valrep_townhouse_propdesc_registry_date) {
		this.valrep_townhouse_propdesc_registry_date = valrep_townhouse_propdesc_registry_date;
	}


	public void setreport_propdesc_registered_owner(String report_propdesc_registered_owner) {
		this.report_propdesc_registered_owner = report_propdesc_registered_owner;
	}
	
	public void setreport_propdesc_registry_of_deeds(String report_propdesc_registry_of_deeds) {
		this.report_propdesc_registry_of_deeds = report_propdesc_registry_of_deeds;
	}
}