package com.gds.appraisalmaybank.database2;

public class Townhouse_API_Lot_Valuation_Details {
	// private variables
	int id;
	String record_id;
	String report_value_tct_no;
	String report_value_lot_no;
	String report_value_block_no;
	String report_value_lot_area;
	String report_value_floor_area;
	String report_value_unit_value;
	String report_value_land_value;
	
	// Empty constructor
	public Townhouse_API_Lot_Valuation_Details(){
		
	}
	
	public Townhouse_API_Lot_Valuation_Details(String record_id) {
		this.record_id = record_id;
	}
	
	public Townhouse_API_Lot_Valuation_Details(int id,
			String record_id, String report_value_tct_no,
			String report_value_lot_no, String report_value_block_no,
			String report_value_lot_area,
			String report_value_floor_area, String report_value_unit_value,
			String report_value_land_value) {
		this.id = id;
		this.record_id = record_id;
		this.report_value_tct_no = report_value_tct_no;
		this.report_value_lot_no = report_value_lot_no;
		this.report_value_block_no = report_value_block_no;
		this.report_value_lot_area = report_value_lot_area;
		this.report_value_floor_area = report_value_floor_area;
		this.report_value_unit_value = report_value_unit_value;
		this.report_value_land_value = report_value_land_value;
	}
	
	public Townhouse_API_Lot_Valuation_Details(String record_id, String report_value_tct_no,
			String report_value_lot_no, String report_value_block_no,
			String report_value_lot_area,
			String report_value_floor_area, String report_value_unit_value,
			String report_value_land_value) {
		this.record_id = record_id;
		this.report_value_tct_no = report_value_tct_no;
		this.report_value_lot_no = report_value_lot_no;
		this.report_value_block_no = report_value_block_no;
		this.report_value_lot_area = report_value_lot_area;
		this.report_value_floor_area = report_value_floor_area;
		this.report_value_unit_value = report_value_unit_value;
		this.report_value_land_value = report_value_land_value;
	}
	
	public Townhouse_API_Lot_Valuation_Details(String report_value_tct_no,
			String report_value_lot_no, String report_value_block_no,
			String report_value_lot_area,
			String report_value_floor_area, String report_value_unit_value,
			String report_value_land_value) {
		this.report_value_tct_no = report_value_tct_no;
		this.report_value_lot_no = report_value_lot_no;
		this.report_value_block_no = report_value_block_no;
		this.report_value_lot_area = report_value_lot_area;
		this.report_value_floor_area = report_value_floor_area;
		this.report_value_unit_value = report_value_unit_value;
		this.report_value_land_value = report_value_land_value;
	}
	
	
	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	public String getrecord_id() {
		return this.record_id;
	}

	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}
	
	//getters
	public String getreport_value_tct_no() {
		return this.report_value_tct_no;
	}

	public String getreport_value_lot_no() {
		return this.report_value_lot_no;
	}

	public String getreport_value_block_no() {
		return this.report_value_block_no;
	}

	public String getreport_value_lot_area() {
		return this.report_value_lot_area;
	}

	public String getreport_value_floor_area() {
		return this.report_value_floor_area;
	}

	public String getreport_value_unit_value() {
		return this.report_value_unit_value;
	}

	public String getreport_value_land_value() {
		return this.report_value_land_value;
	}
	
	//setters
	public void setreport_value_tct_no(String report_value_tct_no) {
		this.report_value_tct_no = report_value_tct_no;
	}

	public void setreport_value_lot_no(String report_value_lot_no) {
		this.report_value_lot_no = report_value_lot_no;
	}

	public void setreport_value_block_no(String report_value_block_no) {
		this.report_value_block_no = report_value_block_no;
	}

	public void setreport_value_lot_area(String report_value_lot_area) {
		this.report_value_lot_area = report_value_lot_area;
	}

	public void setreport_value_floor_area(String report_value_floor_area) {
		this.report_value_floor_area = report_value_floor_area;
	}

	public void setreport_value_unit_value(String report_value_unit_value) {
		this.report_value_unit_value = report_value_unit_value;
	}

	public void setreport_value_land_value(String report_value_land_value) {
		this.report_value_land_value = report_value_land_value;
	}
}