package com.gds.appraisalmaybank.database2;

public class Motor_Vehicle_API_Prev_Appraisal {
    // private variables
    int id;
    String record_id;
    String report_prev_date;
    String report_prev_appraiser;
    String report_prev_requestor;
    String report_prev_appraised_value;

    // Empty constructor
    public Motor_Vehicle_API_Prev_Appraisal(){

    }

    public Motor_Vehicle_API_Prev_Appraisal(String record_id) {
        this.record_id = record_id;
    }

    public Motor_Vehicle_API_Prev_Appraisal(int id,
                                      String record_id, String report_prev_date,
                                      String report_prev_appraiser, String report_prev_requestor,
                                      String report_prev_appraised_value) {
        this.id = id;
        this.record_id = record_id;
        this.report_prev_date = report_prev_date;
        this.report_prev_appraiser = report_prev_appraiser;
        this.report_prev_requestor = report_prev_requestor;
        this.report_prev_appraised_value = report_prev_appraised_value;

    }

    public Motor_Vehicle_API_Prev_Appraisal(String record_id,
                                      String report_prev_date, String report_prev_appraiser,
                                      String report_prev_requestor, String report_prev_appraised_value) {
        this.record_id = record_id;
        this.report_prev_date = report_prev_date;
        this.report_prev_appraiser = report_prev_appraiser;
        this.report_prev_requestor =report_prev_requestor;
        this.report_prev_appraised_value = report_prev_appraised_value;

    }

    public Motor_Vehicle_API_Prev_Appraisal(String report_prev_date,
                                      String report_prev_appraiser, String report_prev_requestor,
                                      String report_prev_appraised_value) {
        this.report_prev_date = report_prev_date;
        this.report_prev_appraiser = report_prev_appraiser;
        this.report_prev_requestor = report_prev_requestor;
        this.report_prev_appraised_value = report_prev_appraised_value;

    }

    // getting ID
    public int getID() {
        return this.id;
    }

    // setting id
    public void setID(int id) {
        this.id = id;
    }

    public String getrecord_id() {
        return this.record_id;
    }

    public void setrecord_id(String record_id) {
        this.record_id = record_id;
    }

    // getters
    public String getreport_prev_date() {
        return this.report_prev_date;
    }

    public String getreport_prev_appraiser() {
        return this.report_prev_appraiser;
    }

    public String getreport_prev_requestor() {
        return this.report_prev_requestor;
    }

    public String getreport_prev_appraised_value() {
        return this.report_prev_appraised_value;
    }



    //setters
    public void setreport_prev_date(String report_prev_date) {
        this.report_prev_date = report_prev_date;
    }

    public void setreport_prev_appraiser(String report_prev_appraiser) {
        this.report_prev_appraiser = report_prev_appraiser;
    }

    public void setreport_prev_requestor(String report_prev_requestor) {
        this.report_prev_requestor = report_prev_requestor;
    }

    public void setreport_prev_appraised_value(String report_prev_appraised_value) {
        this.report_prev_appraised_value = report_prev_appraised_value;
    }


}
