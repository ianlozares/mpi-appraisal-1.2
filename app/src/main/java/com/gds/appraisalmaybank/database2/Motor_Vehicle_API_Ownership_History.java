package com.gds.appraisalmaybank.database2;

public class Motor_Vehicle_API_Ownership_History {
	//private variables
	int id;
	String record_id;
	String report_verification_ownerhistory_date_day;
	String report_verification_ownerhistory_date_month;
	String report_verification_ownerhistory_date_year;
	String report_verification_ownerhistory_name;
	
	public Motor_Vehicle_API_Ownership_History() {

	}
	
	public Motor_Vehicle_API_Ownership_History(	int id,
			String record_id,
			String report_verification_ownerhistory_date_day,
			String report_verification_ownerhistory_date_month,
			String report_verification_ownerhistory_date_year,
			String report_verification_ownerhistory_name) {
		this.id = id;
		this.record_id = record_id;
		this.report_verification_ownerhistory_date_day = report_verification_ownerhistory_date_day;
		this.report_verification_ownerhistory_date_month = report_verification_ownerhistory_date_month;
		this.report_verification_ownerhistory_date_year = report_verification_ownerhistory_date_year;
		this.report_verification_ownerhistory_name = report_verification_ownerhistory_name;
	}
	
	public Motor_Vehicle_API_Ownership_History(
			String record_id,
			String report_verification_ownerhistory_date_day,
			String report_verification_ownerhistory_date_month,
			String report_verification_ownerhistory_date_year,
			String report_verification_ownerhistory_name) {
		this.record_id = record_id;
		this.report_verification_ownerhistory_date_day = report_verification_ownerhistory_date_day;
		this.report_verification_ownerhistory_date_month = report_verification_ownerhistory_date_month;
		this.report_verification_ownerhistory_date_year = report_verification_ownerhistory_date_year;
		this.report_verification_ownerhistory_name = report_verification_ownerhistory_name;
	}
	
	public Motor_Vehicle_API_Ownership_History(
			String report_verification_ownerhistory_date_day,
			String report_verification_ownerhistory_date_month,
			String report_verification_ownerhistory_date_year,
			String report_verification_ownerhistory_name) {
		this.report_verification_ownerhistory_date_day = report_verification_ownerhistory_date_day;
		this.report_verification_ownerhistory_date_month = report_verification_ownerhistory_date_month;
		this.report_verification_ownerhistory_date_year = report_verification_ownerhistory_date_year;
		this.report_verification_ownerhistory_name = report_verification_ownerhistory_name;
	}
	
	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	// getting record_id
	public String getrecord_id() {
		return this.record_id;
	}

	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}

	//getters
	public String getreport_verification_ownerhistory_date_day() {
		return this.report_verification_ownerhistory_date_day;
	}

	public String getreport_verification_ownerhistory_date_month() {
		return this.report_verification_ownerhistory_date_month;
	}

	public String getreport_verification_ownerhistory_date_year() {
		return this.report_verification_ownerhistory_date_year;
	}

	public String getreport_verification_ownerhistory_name() {
		return this.report_verification_ownerhistory_name;
	}
	
	//setters
	public void setreport_verification_ownerhistory_date_day(
			String report_verification_ownerhistory_date_day) {
		this.report_verification_ownerhistory_date_day = report_verification_ownerhistory_date_day;
	}

	public void setreport_verification_ownerhistory_date_month(
			String report_verification_ownerhistory_date_month) {
		this.report_verification_ownerhistory_date_month = report_verification_ownerhistory_date_month;
	}

	public void setreport_verification_ownerhistory_date_year(
			String report_verification_ownerhistory_date_year) {
		this.report_verification_ownerhistory_date_year = report_verification_ownerhistory_date_year;
	}

	public void setreport_verification_ownerhistory_name(
			String report_verification_ownerhistory_name) {
		this.report_verification_ownerhistory_name = report_verification_ownerhistory_name;
	}
}
