package com.gds.appraisalmaybank.database2;

public class Construction_Ebm_API_Room_List {
	//private variables
	int id;
	String record_id;
	String report_room_list_area;
	String report_room_list_description;
	String report_room_list_est_proj_cost;
	String report_room_list_floor;
	String report_room_list_rcn;
	
	// Empty constructor
	public Construction_Ebm_API_Room_List(){
		
	}
	
	public Construction_Ebm_API_Room_List(String record_id) {
		this.record_id = record_id;
	}
	
	public Construction_Ebm_API_Room_List(int id, String record_id,
			String report_room_list_area, String report_room_list_description,
			String report_room_list_est_proj_cost,
			String report_room_list_floor, String report_room_list_rcn) {		
		this.id = id;
		this.record_id = record_id;
		this.report_room_list_area = report_room_list_area;
		this.report_room_list_description = report_room_list_description;
		this.report_room_list_est_proj_cost = report_room_list_est_proj_cost;
		this.report_room_list_floor = report_room_list_floor;
		this.report_room_list_rcn = report_room_list_rcn;
	}
	
	public Construction_Ebm_API_Room_List(String record_id,
			String report_room_list_area, String report_room_list_description,
			String report_room_list_est_proj_cost,
			String report_room_list_floor, String report_room_list_rcn) {		
		this.record_id = record_id;
		this.report_room_list_area = report_room_list_area;
		this.report_room_list_description = report_room_list_description;
		this.report_room_list_est_proj_cost = report_room_list_est_proj_cost;
		this.report_room_list_floor = report_room_list_floor;
		this.report_room_list_rcn = report_room_list_rcn;
	}
	
	public Construction_Ebm_API_Room_List(
			String report_room_list_area, String report_room_list_description,
			String report_room_list_est_proj_cost,
			String report_room_list_floor, String report_room_list_rcn) {		
		this.report_room_list_area = report_room_list_area;
		this.report_room_list_description = report_room_list_description;
		this.report_room_list_est_proj_cost = report_room_list_est_proj_cost;
		this.report_room_list_floor = report_room_list_floor;
		this.report_room_list_rcn = report_room_list_rcn;
	}
	
	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}
	
	public String getrecord_id() {
		return this.record_id;
	}

	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}
	
	// getters
	public String getreport_room_list_area() {
		return this.report_room_list_area;
	}
	

	public String getreport_room_list_description() {
		return this.report_room_list_description;
	}

	public String getreport_room_list_est_proj_cost() {
		return this.report_room_list_est_proj_cost;
	}

	public String getreport_room_list_floor() {
		return this.report_room_list_floor;
	}

	public String getreport_room_list_rcn() {
		return this.report_room_list_rcn;
	}
	
	//setters
	public void setreport_room_list_area(String report_room_list_area) {
		this.report_room_list_area = report_room_list_area;
	}

	public void setreport_room_list_description(String report_room_list_description) {
		this.report_room_list_description = report_room_list_description;
	}

	public void setreport_room_list_est_proj_cost(String report_room_list_est_proj_cost) {
		this.report_room_list_est_proj_cost = report_room_list_est_proj_cost;
	}

	public void setreport_room_list_floor(String report_room_list_floor) {
		this.report_room_list_floor = report_room_list_floor;
	}

	public void setreport_room_list_rcn(String report_room_list_rcn) {
		this.report_room_list_rcn = report_room_list_rcn;
	}
}
