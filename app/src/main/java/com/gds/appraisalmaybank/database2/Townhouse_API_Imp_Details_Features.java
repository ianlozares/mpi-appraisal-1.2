package com.gds.appraisalmaybank.database2;

public class Townhouse_API_Imp_Details_Features {
	// private variables
	int imp_details_features_id;
	String record_id;
	String imp_details_id;
	String report_desc_features;
	String report_desc_features_area;
	String report_desc_features_area_desc;
	
	// Empty constructor
	public Townhouse_API_Imp_Details_Features(){
		
	}
	
	public Townhouse_API_Imp_Details_Features(String record_id) {
		this.record_id = record_id;
	}
	
	public Townhouse_API_Imp_Details_Features(
			int imp_details_features_id, String record_id,
			String imp_details_id, String report_desc_features,
			String report_desc_features_area,
			String report_desc_features_area_desc) {
		this.imp_details_features_id = imp_details_features_id;
		this.record_id = record_id;
		this.imp_details_id = imp_details_id;
		this.report_desc_features = report_desc_features;
		this.report_desc_features_area = report_desc_features_area;
		this.report_desc_features_area_desc = report_desc_features_area_desc;
	}
	
	public Townhouse_API_Imp_Details_Features(String record_id,
			String imp_details_id, String report_desc_features,
			String report_desc_features_area,
			String report_desc_features_area_desc) {
		this.record_id = record_id;
		this.imp_details_id = imp_details_id;
		this.report_desc_features = report_desc_features;
		this.report_desc_features_area = report_desc_features_area;
		this.report_desc_features_area_desc = report_desc_features_area_desc;
	}
	
	public Townhouse_API_Imp_Details_Features(String imp_details_id,
			String report_desc_features,
			String report_desc_features_area,
			String report_desc_features_area_desc) {
		this.imp_details_id = imp_details_id;
		this.report_desc_features = report_desc_features;
		this.report_desc_features_area = report_desc_features_area;
		this.report_desc_features_area_desc = report_desc_features_area_desc;
	}
	
	public Townhouse_API_Imp_Details_Features(String report_desc_features,
			String report_desc_features_area,
			String report_desc_features_area_desc) {
		this.report_desc_features = report_desc_features;
		this.report_desc_features_area = report_desc_features_area;
		this.report_desc_features_area_desc = report_desc_features_area_desc;
	}
	
	// getting ID
	public int getID() {
		return this.imp_details_features_id;
	}

	// setting id
	public void setID(int imp_details_features_id) {
		this.imp_details_features_id = imp_details_features_id;
	}
	
	public String getrecord_id() {
		return this.record_id;
	}

	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}
	
	// getters
	public String getimp_details_id() {
		return this.imp_details_id;
	}
	public String getreport_desc_features() {
		return this.report_desc_features;
	}
	public String getreport_desc_features_area() {
		return this.report_desc_features_area;
	}
	public String getreport_desc_features_area_desc() {
		return this.report_desc_features_area_desc;
	}
	
	//setters
	public void setimp_details_id(String imp_details_id) {
		this.imp_details_id = imp_details_id;
	}
	public void setreport_desc_features(String report_desc_features) {
		this.report_desc_features = report_desc_features;
	}
	public void setreport_desc_features_area(String report_desc_features_area) {
		this.report_desc_features_area = report_desc_features_area;
	}
	public void setreport_desc_features_area_desc(String report_desc_features_area_desc) {
		this.report_desc_features_area_desc = report_desc_features_area_desc;
	}
	
}
