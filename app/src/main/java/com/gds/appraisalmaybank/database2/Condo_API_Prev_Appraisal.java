package com.gds.appraisalmaybank.database2;

public class Condo_API_Prev_Appraisal {
    // private variables
    int id;
    String record_id;
    String report_main_prev_desc;
    String report_main_prev_area;
    String report_main_prev_unit_value;
    String report_main_prev_appraised_value;

    // Empty constructor
    public Condo_API_Prev_Appraisal(){

    }

    public Condo_API_Prev_Appraisal(String record_id) {
        this.record_id = record_id;
    }

    public Condo_API_Prev_Appraisal(int id,
                                                String record_id, String report_main_prev_desc,
                                                String report_main_prev_area, String report_main_prev_unit_value,
                                                String report_main_prev_appraised_value) {
        this.id = id;
        this.record_id = record_id;
        this.report_main_prev_desc = report_main_prev_desc;
        this.report_main_prev_area = report_main_prev_area;
        this.report_main_prev_unit_value = report_main_prev_unit_value;
        this.report_main_prev_appraised_value = report_main_prev_appraised_value;

    }

    public Condo_API_Prev_Appraisal(String record_id,
                                                String report_main_prev_desc, String report_main_prev_area,
                                                String report_main_prev_unit_value, String report_main_prev_appraised_value) {
        this.record_id = record_id;
        this.report_main_prev_desc = report_main_prev_desc;
        this.report_main_prev_area = report_main_prev_area;
        this.report_main_prev_unit_value = report_main_prev_unit_value;
        this.report_main_prev_appraised_value = report_main_prev_appraised_value;
    }

    public Condo_API_Prev_Appraisal(String report_main_prev_desc,
                                                String report_main_prev_area, String report_main_prev_unit_value,
                                                String report_main_prev_appraised_value) {
        this.report_main_prev_desc = report_main_prev_desc;
        this.report_main_prev_area = report_main_prev_area;
        this.report_main_prev_unit_value = report_main_prev_unit_value;
        this.report_main_prev_appraised_value = report_main_prev_appraised_value;
    }

    // getting ID
    public int getID() {
        return this.id;
    }

    // setting id
    public void setID(int id) {
        this.id = id;
    }

    public String getrecord_id() {
        return this.record_id;
    }

    public void setrecord_id(String record_id) {
        this.record_id = record_id;
    }

    // getters
    public String getreport_main_prev_desc() {
        return this.report_main_prev_desc;
    }

    public String getreport_main_prev_area() {
        return this.report_main_prev_area;
    }

    public String getreport_main_prev_unit_value() {
        return this.report_main_prev_unit_value;
    }

    public String getreport_main_prev_appraised_value() {
        return this.report_main_prev_appraised_value;
    }


    //setters
    public void setreport_main_prev_desc(String report_main_prev_desc) {
        this.report_main_prev_desc = report_main_prev_desc;
    }

    public void setreport_main_prev_area(String report_main_prev_area) {
        this.report_main_prev_area = report_main_prev_area;
    }

    public void setreport_main_prev_unit_value(String report_main_prev_unit_value) {
        this.report_main_prev_unit_value = report_main_prev_unit_value;
    }

    public void setreport_main_prev_appraised_value(String report_main_prev_appraised_value) {
        this.report_main_prev_appraised_value = report_main_prev_appraised_value;
    }


}