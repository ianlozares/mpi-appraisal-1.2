package com.gds.appraisalmaybank.database2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.gds.appraisalmaybank.database_new.DatabaseTableObject;
import com.gds.appraisalmaybank.database_new.Tables;
import com.gds.appraisalmaybank.methods.GDS_methods;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler2 extends SQLiteOpenHelper {
    // Database Version
    private static final int DATABASE_VERSION = 2;
    GDS_methods gds = new GDS_methods();
    // Database Name
    private static final String DATABASE_NAME = "GDSAppraisalDb";

    // table names
    private static final String tbl_request_form = "tbl_request_form";
    private static final String tbl_regions = "tbl_regions";
    //land improvements
    private static final String tbl_land_improvements_lot_details = "land_improvements_lot_details";
    private static final String tbl_land_improvements_imp_details = "land_improvements_imp_details";
    private static final String tbl_land_improvements_imp_details_features = "land_improvements_imp_details_features";
    private static final String tbl_land_improvements_lot_valuation_details = "land_improvements_lot_valuation_details";
    private static final String tbl_land_improvements_imp_valuation = "land_improvements_imp_valuation";
    private static final String tbl_land_improvements_imp_valuation_details = "land_improvements_imp_valuation_details";
    private static final String tbl_land_improvements_prev_appraisal = "land_improvements_prev_appraisal";
    private static final String tbl_land_improvements_main_prev_appraisal = "land_improvements_main_prev_appraisal";

    private static final String tbl_vacant_lot_lot_details = "vacant_lot_lot_details";
    private static final String tbl_vacant_lot_lot_valuation_details = "vacant_lot_lot_valuation_details";
    private static final String tbl_vacant_lot_prev_appraisal = "vacant_lot_prev_appraisal";
    private static final String tbl_vacant_lot_main_prev_appraisal = "vacant_lot_main_prev_appraisal";
    private static final String tbl_vacant_lot = "vacant_lot";

    private static final String tbl_condo_title_details = "condo_title_details";
    private static final String tbl_condo_unit_details = "condo_unit_details";
    private static final String tbl_condo_unit_valuation = "condo_unit_valuation";
    private static final String tbl_condo_prev_appraisal = "condo_prev_appraisal";
    private static final String tbl_condo_main_prev_appraisal = "condo_main_prev_appraisal";

    private static final String tbl_townhouse_lot_details = "townhouse_lot_details";
    private static final String tbl_townhouse_imp_details = "townhouse_imp_details";
    private static final String tbl_townhouse_imp_details_features = "townhouse_imp_details_features";
    public static final String tbl_townhouse_lot_valuation_details = "townhouse_lot_valuation_details";
    private static final String tbl_townhouse_prev_appraisal = "townhouse_prev_appraisal";
    private static final String tbl_townhouse_main_prev_appraisal = "townhouse_main_prev_appraisal";

    private static final String tbl_construction_ebm_room_list = "construction_ebm_room_list";
    private static final String tbl_motor_vehicle_prev_appraisal = "motor_vehicle_previous_appraisal";
    private static final String tbl_motor_vehicle_ownership_history = "motor_vehicle_ownership_history";
    private static final String tbl_motor_vehicle_ownership_source = "motor_vehicle_ownership_source";

    private static final String tbl_ppcr_details = "ppcr_report_details";

    // universal field names (used in many tables)
    private static final String KEY_ID = "id";
    private static final String Report_record_id = "record_id";

    // tbl regions
    private static final String KEY_name = "name";

    //land_improvements_lot_details
    public static final String report_propdesc_tct_no = "report_propdesc_tct_no";
    public static final String report_propdesc_lot = "report_propdesc_lot";
    public static final String report_propdesc_block = "report_propdesc_block";
    public static final String report_propdesc_survey_nos = "report_propdesc_survey_nos";
    public static final String report_propdesc_area = "report_propdesc_area";
    public static final String valrep_landimp_propdesc_registry_date = "valrep_landimp_propdesc_registry_date";
    public static final String report_propdesc_registered_owner = "report_propdesc_registered_owner";
    public static final String report_propdesc_registry_of_deeds = "report_propdesc_registry_of_deeds";

    public static final String valrep_land_propdesc_registry_date = "valrep_land_propdesc_registry_date";
    public static final String valrep_townhouse_propdesc_registry_date = "valrep_townhouse_propdesc_registry_date";
    public static final String valrep_condo_propdesc_registry_date = "valrep_condo_propdesc_registry_date";

    //land_improvements_imp_details
    public static final String imp_details_id = "imp_details_id";
    public static final String report_impsummary1_no_of_floors = "report_impsummary1_no_of_floors";
    public static final String report_impsummary1_building_desc = "report_impsummary1_building_desc";
    public static final String report_impsummary1_erected_on_lot = "report_impsummary1_erected_on_lot";
    public static final String report_impsummary1_fa = "report_impsummary1_fa";
    public static final String report_impsummary1_fa_per_td = "report_impsummary1_fa_per_td";
    public static final String report_impsummary1_actual_utilization = "report_impsummary1_actual_utilization";
    public static final String report_impsummary1_usage_declaration = "report_impsummary1_usage_declaration";
    public static final String report_impsummary1_owner = "report_impsummary1_owner";
    public static final String report_impsummary1_socialized_housing = "report_impsummary1_socialized_housing";
    public static final String report_desc_property_type = "report_desc_property_type";
    public static final String report_desc_foundation = "report_desc_foundation";
    public static final String report_desc_columns_posts = "report_desc_columns_posts";
    public static final String report_desc_beams = "report_desc_beams";
    public static final String report_desc_exterior_walls = "report_desc_exterior_walls";
    public static final String report_desc_interior_walls = "report_desc_interior_walls";
    public static final String report_desc_imp_flooring = "report_desc_imp_flooring";
    public static final String report_desc_doors = "report_desc_doors";
    public static final String report_desc_imp_windows = "report_desc_imp_windows";
    public static final String report_desc_ceiling = "report_desc_ceiling";
    public static final String report_desc_imp_roofing = "report_desc_imp_roofing";
    public static final String report_desc_trusses = "report_desc_trusses";
    public static final String report_desc_economic_life = "report_desc_economic_life";
    public static final String report_desc_effective_age = "report_desc_effective_age";
    public static final String report_desc_imp_remain_life = "report_desc_imp_remain_life";
    public static final String report_desc_occupants = "report_desc_occupants";
    public static final String report_desc_owned_or_leased = "report_desc_owned_or_leased";
    public static final String report_desc_imp_floor_area = "report_desc_imp_floor_area";
    public static final String report_desc_confirmed_thru = "report_desc_confirmed_thru";
    public static final String report_desc_observed_condition = "report_desc_observed_condition";

    //land_improvements_imp_details_features
    public static final String imp_details_features_id = "imp_details_features_id";
    public static final String report_desc_features = "report_desc_features";
    public static final String report_desc_features_area = "report_desc_features_area";
    public static final String report_desc_features_area_desc = "report_desc_features_area_desc";

    //land_improvements_lot_valuation_details
    public static final String lot_valuation_details_id = "lot_valuation_details_id";
    public static final String report_value_tct_no = "report_value_tct_no";
    public static final String report_value_lot_no = "report_value_lot_no";
    public static final String report_value_block_no = "report_value_block_no";
    public static final String report_value_area = "report_value_area";
    public static final String report_value_deduction = "report_value_deduction";
    public static final String report_value_net_area = "report_value_net_area";
    public static final String report_value_unit_value = "report_value_unit_value";
    public static final String report_value_land_value = "report_value_land_value";

    //added in Vacant lot lot details
    public static final String report_propdesc_deeds = "report_propdesc_deeds";

    //land_improvements_cost_approach
    public static final String imp_valuation_id = "imp_valuation_id";
    public static final String report_description = "report_description";
    public static final String report_total_area = "report_total_area";
    public static final String report_total_reproduction_cost = "report_total_reproduction_cost";
    public static final String report_depreciation = "report_depreciation";
    public static final String report_less_reproduction = "report_less_reproduction";
    public static final String report_depreciated_value = "report_depreciated_value";

    //land_improvements_cost_approach_details imp_valuation_details_id
    public static final String imp_valuation_details_id = "imp_valuation_details_id";
    public static final String report_imp_value_kind = "report_imp_value_kind";
    public static final String report_imp_value_area = "report_imp_value_area";
    public static final String report_imp_value_cost_per_sqm = "report_imp_value_cost_per_sqm";
    public static final String report_imp_value_reproduction_cost = "report_imp_value_reproduction_cost";

    //land improvements_prev_appraisal
    public static final String report_prev_app_name = "report_prev_app_name";
    public static final String report_prev_app_location = "report_prev_app_location";
    public static final String report_prev_app_area = "report_prev_app_area";
    public static final String report_prev_app_value = "report_prev_app_value";
    public static final String report_prev_app_date = "report_prev_app_date";

    //condo_title_details
    public static final String report_propdesc_property_type = "report_propdesc_property_type";
    public static final String report_propdesc_cct_no = "report_propdesc_cct_no";
    public static final String report_prodesc_unit_no = "report_prodesc_unit_no";
    public static final String report_prodesc_floor = "report_prodesc_floor";
    public static final String report_prodesc_bldg_name = "report_prodesc_bldg_name";
    public static final String report_prodesc_reg_of_deeds = "report_prodesc_reg_of_deeds";

    //condo_prev_appraisal
    public static final String report_prev_app_account = "report_prev_app_account";

    //condo_unit_details
    public static final String report_unit_description = "report_unit_description";
    public static final String report_unit_no_of_storeys = "report_unit_no_of_storeys";
    public static final String report_unit_unit_no = "report_unit_unit_no";
    public static final String report_unit_floor_location = "report_unit_floor_location";
    public static final String report_unit_floor_area = "report_unit_floor_area";
    public static final String report_unit_interior_flooring = "report_unit_interior_flooring";
    public static final String report_unit_interior_partitions = "report_unit_interior_partitions";
    public static final String report_unit_interior_doors = "report_unit_interior_doors";
    public static final String report_unit_interior_windows = "report_unit_interior_windows";
    public static final String report_unit_interior_ceiling = "report_unit_interior_ceiling";
    public static final String report_unit_features = "report_unit_features";
    public static final String report_unit_occupants = "report_unit_occupants";
    public static final String report_unit_owned_or_leased = "report_unit_owned_or_leased";
    public static final String report_unit_observed_condition = "report_unit_observed_condition";
    public static final String report_unit_ownership_of_property = "report_unit_ownership_of_property";
    public static final String report_unit_socialized_housing = "report_unit_socialized_housing";
    public static final String report_unit_type_of_property = "report_unit_type_of_property";

    //condo_unit_valuation
    public static final String report_value_property_type = "report_value_property_type";
    public static final String report_value_cct_no = "report_value_cct_no";
    public static final String report_value_unit_no = "report_value_unit_no";
    public static final String report_value_floor_area = "report_value_floor_area";
    public static final String report_value_appraised_value = "report_value_appraised_value";

    //condo_title_details
    public static final String report_propdesc_unit_no = "report_propdesc_unit_no";
    public static final String report_propdesc_floor = "report_propdesc_floor";
    public static final String report_propdesc_bldg_name = "report_propdesc_bldg_name";
    public static final String report_propdesc_reg_of_deeds = "report_propdesc_reg_of_deeds";

    //condtruction_ebm_room_list
    public static final String report_room_list_area = "report_room_list_area";
    public static final String report_room_list_description = "report_room_list_description";
    public static final String report_room_list_est_proj_cost = "report_room_list_est_proj_cost";
    public static final String report_room_list_floor = "report_room_list_floor";
    public static final String report_room_list_rcn = "report_room_list_rcn";

    //nvoh
    public static final String report_verification_ownerhistory_date_day = "report_verification_ownerhistory_date_day";
    public static final String report_verification_ownerhistory_date_month = "report_verification_ownerhistory_date_month";
    public static final String report_verification_ownerhistory_date_year = "report_verification_ownerhistory_date_year";
    public static final String report_verification_ownerhistory_name = "report_verification_ownerhistory_name";

    //mvos
    public static final String report_source_mileage = "report_source_mileage";
    public static final String report_source_range_min = "report_source_range_min";
    public static final String report_source_range_max = "report_source_range_max";
    public static final String report_source_source = "report_source_source";
    public static final String report_source_vehicle_type = "report_source_vehicle_type";


    //ppcr details
    public static final String report_visit_month = "report_visit_month";
    public static final String report_visit_day = "report_visit_day";
    public static final String report_visit_year = "report_visit_year";
    public static final String report_nth = "report_nth";
    public static final String report_value_foundation = "report_value_foundation";
    public static final String report_value_columns = "report_value_columns";
    public static final String report_value_beams_girders = "report_value_beams_girders";
    public static final String report_value_surround_walls = "report_value_surround_walls";
    public static final String report_value_interior_partition = "report_value_interior_partition";
    public static final String report_value_roofing_works = "report_value_roofing_works";
    public static final String report_value_plumbing_rough_in = "report_value_plumbing_rough_in";
    public static final String report_value_electrical_rough_in = "report_value_electrical_rough_in";
    public static final String report_value_ceiling_works = "report_value_ceiling_works";
    public static final String report_value_doors_windows = "report_value_doors_windows";
    public static final String report_value_plastering_wall = "report_value_plastering_wall";
    public static final String report_value_slab_include_finish = "report_value_slab_include_finish";
    public static final String report_value_painting_finishing = "report_value_painting_finishing";
    public static final String report_value_plumbing_elect_fix = "report_value_plumbing_elect_fix";
    public static final String report_value_utilities_tapping = "report_value_utilities_tapping";
    public static final String report_completion_foundation = "report_completion_foundation";
    public static final String report_completion_columns = "report_completion_columns";
    public static final String report_completion_beams_girders = "report_completion_beams_girders";
    public static final String report_completion_surround_walls = "report_completion_surround_walls";
    public static final String report_completion_interior_partition = "report_completion_interior_partition";
    public static final String report_completion_roofing_works = "report_completion_roofing_works";
    public static final String report_completion_plumbing_rough_in = "report_completion_plumbing_rough_in";
    public static final String report_completion_electrical_rough_in = "report_completion_electrical_rough_in";
    public static final String report_completion_ceiling_works = "report_completion_ceiling_works";
    public static final String report_completion_doors_windows = "report_completion_doors_windows";
    public static final String report_completion_plastering_wall = "report_completion_plastering_wall";
    public static final String report_completion_slab_include_finish = "report_completion_slab_include_finish";
    public static final String report_completion_painting_finishing = "report_completion_painting_finishing";
    public static final String report_completion_plumbing_elect_fix = "report_completion_plumbing_elect_fix";
    public static final String report_completion_utilities_tapping = "report_completion_utilities_tapping";
    public static final String report_total_value = "report_total_value";

    public DatabaseHandler2(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        /*String CREATE_tbl_regions = "CREATE TABLE " + tbl_regions + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_name + " TEXT)";

        db.execSQL(CREATE_tbl_regions);*/
        Log.i("DatabaseHandler:", "Creating Tables ...");

        //add tables
        DatabaseTableObject condo = new DatabaseTableObject(Tables.condo.table_name, Tables.condo.fields);
        DatabaseTableObject condo_main_prev_appraisal = new DatabaseTableObject(Tables.condo_main_prev_appraisal.table_name, Tables.condo_main_prev_appraisal.fields);
        DatabaseTableObject condo_prev_appraisal = new DatabaseTableObject(Tables.condo_prev_appraisal.table_name, Tables.condo_prev_appraisal.fields);
        DatabaseTableObject condo_title_details = new DatabaseTableObject(Tables.condo_title_details.table_name, Tables.condo_title_details.fields);
        DatabaseTableObject condo_unit_details = new DatabaseTableObject(Tables.condo_unit_details.table_name, Tables.condo_unit_details.fields);
        DatabaseTableObject condo_unit_valuation = new DatabaseTableObject(Tables.condo_unit_valuation.table_name, Tables.condo_unit_valuation.fields);
        DatabaseTableObject construction_ebm = new DatabaseTableObject(Tables.construction_ebm.table_name, Tables.construction_ebm.fields);
        DatabaseTableObject construction_ebm_room_list = new DatabaseTableObject(Tables.construction_ebm_room_list.table_name, Tables.construction_ebm_room_list.fields);
        DatabaseTableObject db_check = new DatabaseTableObject(Tables.db_check.table_name, Tables.db_check.fields);
        DatabaseTableObject land_improvements = new DatabaseTableObject(Tables.land_improvements.table_name, Tables.land_improvements.fields);

        //tables with different id name
        /*DatabaseTableObject land_improvements_imp_details = new DatabaseTableObject(Tables.condo.table_name, Tables.condo.fields);
        DatabaseTableObject land_improvements_imp_details_features = new DatabaseTableObject(Tables.condo.table_name, Tables.condo.fields);
        DatabaseTableObject land_improvements_imp_details = new DatabaseTableObject(Tables.condo.table_name, Tables.condo.fields);
        DatabaseTableObject land_improvements_imp_valuation = new DatabaseTableObject(Tables.condo.table_name, Tables.condo.fields);
        DatabaseTableObject land_improvements_imp_valuation_details = new DatabaseTableObject(Tables.condo.table_name, Tables.condo.fields);*/

        DatabaseTableObject land_improvements_lot_details = new DatabaseTableObject(Tables.land_improvements_lot_details.table_name, Tables.land_improvements_lot_details.fields);
        DatabaseTableObject land_improvements_lot_valuation_details = new DatabaseTableObject(Tables.land_improvements_lot_valuation_details.table_name, Tables.land_improvements_lot_valuation_details.fields);
        DatabaseTableObject land_improvements_main_prev_appraisal = new DatabaseTableObject(Tables.land_improvements_main_prev_appraisal.table_name, Tables.land_improvements_main_prev_appraisal.fields);
        DatabaseTableObject land_improvements_prev_appraisal = new DatabaseTableObject(Tables.land_improvements_prev_appraisal.table_name, Tables.land_improvements_prev_appraisal.fields);
        DatabaseTableObject motor_vehicle = new DatabaseTableObject(Tables.motor_vehicle.table_name, Tables.motor_vehicle.fields);
        DatabaseTableObject motor_vehicle_ownership_history = new DatabaseTableObject(Tables.motor_vehicle_ownership_history.table_name, Tables.motor_vehicle_ownership_history.fields);
        DatabaseTableObject motor_vehicle_ownership_source = new DatabaseTableObject(Tables.motor_vehicle_ownership_source.table_name, Tables.motor_vehicle_ownership_source.fields);
        DatabaseTableObject motor_vehicle_previous_appraisal = new DatabaseTableObject(Tables.motor_vehicle_previous_appraisal.table_name, Tables.motor_vehicle_previous_appraisal.fields);
        DatabaseTableObject ppcr = new DatabaseTableObject(Tables.ppcr.table_name, Tables.ppcr.fields);
        DatabaseTableObject ppcr_report_details = new DatabaseTableObject(Tables.ppcr_report_details.table_name, Tables.ppcr_report_details.fields);
        DatabaseTableObject report_filename = new DatabaseTableObject(Tables.report_filename.table_name, Tables.report_filename.fields);
        DatabaseTableObject submission_logs = new DatabaseTableObject(Tables.submission_logs.table_name, Tables.submission_logs.fields);
        DatabaseTableObject tbl_attachments = new DatabaseTableObject(Tables.tbl_attachments.table_name, Tables.tbl_attachments.fields);
        DatabaseTableObject tbl_cities = new DatabaseTableObject(Tables.tbl_cities.table_name, Tables.tbl_cities.fields);
        DatabaseTableObject tbl_images = new DatabaseTableObject(Tables.tbl_images.table_name, Tables.tbl_images.fields);
        DatabaseTableObject tbl_provinces = new DatabaseTableObject(Tables.tbl_provinces.table_name, Tables.tbl_provinces.fields);
        DatabaseTableObject tbl_regions = new DatabaseTableObject(Tables.tbl_regions.table_name, Tables.tbl_regions.fields);
        DatabaseTableObject tbl_report_accepted_jobs = new DatabaseTableObject(Tables.tbl_report_accepted_jobs.table_name, Tables.tbl_report_accepted_jobs.fields);
        DatabaseTableObject tbl_report_accepted_jobs_contacts = new DatabaseTableObject(Tables.tbl_report_accepted_jobs_contacts.table_name, Tables.tbl_report_accepted_jobs_contacts.fields);
        DatabaseTableObject tbl_report_submitted = new DatabaseTableObject(Tables.tbl_report_submitted.table_name, Tables.tbl_report_submitted.fields);
        DatabaseTableObject tbl_request_attachments = new DatabaseTableObject(Tables.tbl_request_attachments.table_name, Tables.tbl_request_attachments.fields);
        DatabaseTableObject tbl_request_collateral_address = new DatabaseTableObject(Tables.tbl_request_collateral_address.table_name, Tables.tbl_request_collateral_address.fields);
        DatabaseTableObject tbl_request_contact_persons = new DatabaseTableObject(Tables.tbl_request_contact_persons.table_name, Tables.tbl_request_contact_persons.fields);
        DatabaseTableObject tbl_request_form = new DatabaseTableObject(Tables.tbl_request_form.table_name, Tables.tbl_request_form.fields);
        DatabaseTableObject tbl_request_type = new DatabaseTableObject(Tables.tbl_request_type.table_name, Tables.tbl_request_type.fields);
        DatabaseTableObject tbl_required_attachments = new DatabaseTableObject(Tables.tbl_required_attachments.table_name, Tables.tbl_required_attachments.fields);
        DatabaseTableObject townhouse = new DatabaseTableObject(Tables.townhouse.table_name, Tables.townhouse.fields);
        DatabaseTableObject townhouse_condo = new DatabaseTableObject(Tables.townhouse_condo.table_name, Tables.townhouse_condo.fields);

        //tables with different id name
        /*DatabaseTableObject townhouse_imp_details = new DatabaseTableObject(Tables.condo.table_name, Tables.condo.fields);
        DatabaseTableObject townhouse_imp_details_features = new DatabaseTableObject(Tables.condo.table_name, Tables.condo.fields);*/

        DatabaseTableObject townhouse_lot_details = new DatabaseTableObject(Tables.townhouse_lot_details.table_name, Tables.townhouse_lot_details.fields);
        DatabaseTableObject townhouse_lot_valuation_details = new DatabaseTableObject(Tables.townhouse_lot_valuation_details.table_name, Tables.townhouse_lot_valuation_details.fields);
        DatabaseTableObject townhouse_main_prev_appraisal = new DatabaseTableObject(Tables.townhouse_main_prev_appraisal.table_name, Tables.townhouse_main_prev_appraisal.fields);
        DatabaseTableObject townhouse_prev_appraisal = new DatabaseTableObject(Tables.townhouse_prev_appraisal.table_name, Tables.townhouse_prev_appraisal.fields);
        DatabaseTableObject vacant_lot = new DatabaseTableObject(Tables.vacant_lot.table_name, Tables.vacant_lot.fields);
        DatabaseTableObject vacant_lot_lot_details = new DatabaseTableObject(Tables.vacant_lot_lot_details.table_name, Tables.vacant_lot_lot_details.fields);
        DatabaseTableObject vacant_lot_lot_valuation_details = new DatabaseTableObject(Tables.vacant_lot_lot_valuation_details.table_name, Tables.vacant_lot_lot_valuation_details.fields);
        DatabaseTableObject vacant_lot_main_prev_appraisal = new DatabaseTableObject(Tables.vacant_lot_main_prev_appraisal.table_name, Tables.vacant_lot_main_prev_appraisal.fields);
        DatabaseTableObject vacant_lot_prev_appraisal = new DatabaseTableObject(Tables.vacant_lot_prev_appraisal.table_name, Tables.vacant_lot_prev_appraisal.fields);



        //add queries
        db.execSQL(condo.createQuery());

        db.execSQL(condo_main_prev_appraisal.createQuery());
        db.execSQL(condo_prev_appraisal.createQuery());
        db.execSQL(condo_title_details.createQuery());
        db.execSQL(condo_unit_details.createQuery());
        db.execSQL(condo_unit_valuation.createQuery());
        db.execSQL(construction_ebm.createQuery());
        db.execSQL(construction_ebm_room_list.createQuery());
        db.execSQL(db_check.createQuery());
        db.execSQL(land_improvements.createQuery());

        //tables with different id name
        db.execSQL(new DatabaseTableObject().create_land_improvements_imp_valuation_details());
        db.execSQL(new DatabaseTableObject().create_land_improvements_imp_details_features());
        db.execSQL(new DatabaseTableObject().create_land_improvements_imp_details());
        db.execSQL(new DatabaseTableObject().create_land_improvements_imp_valuation());

        db.execSQL(land_improvements_lot_details.createQuery());
        db.execSQL(land_improvements_lot_valuation_details.createQuery());
        db.execSQL(land_improvements_main_prev_appraisal.createQuery());
        db.execSQL(land_improvements_prev_appraisal.createQuery());
        db.execSQL(motor_vehicle.createQuery());
        db.execSQL(motor_vehicle_ownership_history.createQuery());
        db.execSQL(motor_vehicle_ownership_source.createQuery());
        db.execSQL(motor_vehicle_previous_appraisal.createQuery());
        db.execSQL(ppcr.createQuery());
        db.execSQL(ppcr_report_details.createQuery());
        db.execSQL(report_filename.createQuery());
        db.execSQL(submission_logs.createQuery());
        db.execSQL(tbl_attachments.createQuery());
        db.execSQL(tbl_cities.createQuery());
        db.execSQL(tbl_images.createQuery());
        db.execSQL(tbl_provinces.createQuery());
        db.execSQL(tbl_regions.createQuery());
        db.execSQL(tbl_report_accepted_jobs.createQuery());
        db.execSQL(tbl_report_accepted_jobs_contacts.createQuery());
        db.execSQL(tbl_report_submitted.createQuery());
        db.execSQL(tbl_request_attachments.createQuery());
        db.execSQL(tbl_request_collateral_address.createQuery());
        db.execSQL(tbl_request_contact_persons.createQuery());
        db.execSQL(tbl_request_form.createQuery());
        db.execSQL(tbl_request_type.createQuery());
        db.execSQL(tbl_required_attachments.createQuery());
        db.execSQL(townhouse.createQuery());
        db.execSQL(townhouse_condo.createQuery());

        //tables with different id name
        db.execSQL(new DatabaseTableObject().create_townhouse_imp_details());
        db.execSQL(new DatabaseTableObject().create_townhouse_imp_details_features());

        db.execSQL(townhouse_lot_details.createQuery());
        db.execSQL(townhouse_lot_valuation_details.createQuery());
        db.execSQL(townhouse_main_prev_appraisal.createQuery());
        db.execSQL(townhouse_prev_appraisal.createQuery());
        db.execSQL(vacant_lot.createQuery());
        db.execSQL(vacant_lot_lot_details.createQuery());
        db.execSQL(vacant_lot_lot_valuation_details.createQuery());
        db.execSQL(vacant_lot_main_prev_appraisal.createQuery());
        db.execSQL(vacant_lot_prev_appraisal.createQuery());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
//		db.execSQL("DROP TABLE IF EXISTS " + tbl_request_form);
//		// Create tables again
//		onCreate(db);

        /*Log.v("Test", "Within onUpgrade. Old is: " + oldVersion + " New is: " + newVersion);
        String CREATE_tbl_submission_logs = "CREATE TABLE IF NOT EXISTS submission_logs " +
                "(id INTEGER, record_id TEXT, date_submitted TEXT, status TEXT, status_reason TEXT, PRIMARY KEY(id))";


        switch (oldVersion) {
            case 1:
                // we want both updates, so no break statement here...
            case 2:
                db.execSQL(CREATE_tbl_submission_logs);
        }*/
    }

    /**
     * Delete all
     */
    public int deleteAllDB2(String record_id, String tbl_name) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_name,
                Report_record_id + " = ?",
                new String[]{record_id});
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    public void addLand_Improvements_Lot_Details(Land_Improvements_API_Lot_Details li) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", li.getrecord_id());
        values.put("report_propdesc_tct_no", li.getreport_propdesc_tct_no());
        values.put("report_propdesc_lot", li.getreport_propdesc_lot());
        values.put("report_propdesc_block", li.getreport_propdesc_block());
        values.put("report_propdesc_survey_nos", li.getreport_propdesc_survey_nos());
        values.put("report_propdesc_area", li.getreport_propdesc_area());
        values.put("valrep_landimp_propdesc_registry_date", li.getvalrep_landimp_propdesc_registry_date());
        values.put("report_propdesc_registered_owner", li.getreport_propdesc_registered_owner());
        values.put("report_propdesc_registry_of_deeds", li.getreport_propdesc_registry_of_deeds());
        // Inserting Row
        db.insert(tbl_land_improvements_lot_details, null, values);
        db.close(); // Closing database connection
    }

    // Getting selected Land improvements Prop Desc
    public List<Land_Improvements_API_Lot_Details> getLand_Improvements_Lot_Details(String record_id) {
        List<Land_Improvements_API_Lot_Details> land_improvementsList = new ArrayList<Land_Improvements_API_Lot_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_land_improvements_lot_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Land_Improvements_API_Lot_Details li = new Land_Improvements_API_Lot_Details();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_propdesc_tct_no(cursor.getString(2));
                li.setreport_propdesc_lot(cursor.getString(3));
                li.setreport_propdesc_block(cursor.getString(4));
                li.setreport_propdesc_survey_nos(cursor.getString(5));
                li.setreport_propdesc_area(cursor.getString(6));
                li.setvalrep_landimp_propdesc_registry_date(cursor.getString(7));
                li.setreport_propdesc_registered_owner(cursor.getString(8));
                li.setreport_propdesc_registry_of_deeds(cursor.getString(9));

                // Adding Land Improvements
                land_improvementsList.add(li);
            } while (cursor.moveToNext());
        }
        db.close();
        // return land_improvementsList
        return land_improvementsList;
    }

    // Getting selected Land improvements Prop Desc with lot_details_id
    public List<Land_Improvements_API_Lot_Details> getLand_Improvements_Lot_Details_with_lot_details_id(String record_id, String lot_details_id) {
        List<Land_Improvements_API_Lot_Details> land_improvementsList = new ArrayList<Land_Improvements_API_Lot_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_land_improvements_lot_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND " + KEY_ID + " = \"" + lot_details_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Land_Improvements_API_Lot_Details li = new Land_Improvements_API_Lot_Details();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_propdesc_tct_no(cursor.getString(2));
                li.setreport_propdesc_lot(cursor.getString(3));
                li.setreport_propdesc_block(cursor.getString(4));
                li.setreport_propdesc_survey_nos(cursor.getString(5));
                li.setreport_propdesc_area(cursor.getString(6));
                li.setvalrep_landimp_propdesc_registry_date(cursor.getString(7));
                li.setreport_propdesc_registered_owner(cursor.getString(8));
                li.setreport_propdesc_registry_of_deeds(cursor.getString(9));

                // Adding Land Improvements
                land_improvementsList.add(li);
            } while (cursor.moveToNext());
        }
        db.close();
        // return land_improvementsList
        return land_improvementsList;
    }

    // Updating single land improvements prop desc with lot_details_id
    public int updateLand_Improvements_Lot_Details(Land_Improvements_API_Lot_Details li,
                                                   String record_id, String lot_details_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(report_propdesc_tct_no, li.getreport_propdesc_tct_no());
        values.put(report_propdesc_area, li.getreport_propdesc_area());
        values.put(report_propdesc_lot, li.getreport_propdesc_lot());
        values.put(report_propdesc_block, li.getreport_propdesc_block());
        values.put(report_propdesc_survey_nos, li.getreport_propdesc_survey_nos());
        values.put(valrep_landimp_propdesc_registry_date, li.getvalrep_landimp_propdesc_registry_date());
        values.put(report_propdesc_registered_owner, li.getreport_propdesc_registered_owner());
        values.put(report_propdesc_registry_of_deeds, li.getreport_propdesc_registry_of_deeds());
        // updating row
        return db.update(tbl_land_improvements_lot_details, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, lot_details_id});
    }

    // Deleting All Land Improvements Prop Desc
    public int deleteLand_Improvements_API_Lot_Details(Land_Improvements_API_Lot_Details li) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_land_improvements_lot_details, Report_record_id + " = ?",
                new String[]{String.valueOf(li.getrecord_id())});
    }

    // Deleting single prop desc via record_id and lot_details_id
    public int deleteLand_Improvements_API_Lot_Details_Single(String record_id, String lot_details_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_land_improvements_lot_details,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, lot_details_id});
    }

    /**
     * Land imp Desc of Imp / Imp Details CRUD
     */
    //adding desc of imp
    public void addLand_Improvements_Imp_Details(Land_Improvements_API_Imp_Details li) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", li.getrecord_id());
        values.put("report_impsummary1_no_of_floors", li.getreport_impsummary1_no_of_floors());
        values.put("report_impsummary1_building_desc", li.getreport_impsummary1_building_desc());
        values.put("report_impsummary1_erected_on_lot", li.getreport_impsummary1_erected_on_lot());
        values.put("report_impsummary1_fa", li.getreport_impsummary1_fa());
        values.put("report_impsummary1_fa_per_td", li.getreport_impsummary1_fa_per_td());
        values.put("report_impsummary1_actual_utilization", li.getreport_impsummary1_actual_utilization());
        values.put("report_impsummary1_usage_declaration", li.getreport_impsummary1_usage_declaration());
        values.put("report_impsummary1_owner", li.getreport_impsummary1_owner());

        values.put("report_impsummary1_socialized_housing", li.getreport_impsummary1_socialized_housing());
        values.put("report_desc_property_type", li.getreport_desc_property_type());

        values.put("report_desc_foundation", li.getreport_desc_foundation());
        values.put("report_desc_columns_posts", li.getreport_desc_columns_posts());
        values.put("report_desc_beams", li.getreport_desc_beams());
        values.put("report_desc_exterior_walls", li.getreport_desc_exterior_walls());
        values.put("report_desc_interior_walls", li.getreport_desc_interior_walls());
        values.put("report_desc_imp_flooring", li.getreport_desc_imp_flooring());
        values.put("report_desc_doors", li.getreport_desc_doors());
        values.put("report_desc_imp_windows", li.getreport_desc_imp_windows());
        values.put("report_desc_ceiling", li.getreport_desc_ceiling());
        values.put("report_desc_imp_roofing", li.getreport_desc_imp_roofing());
        values.put("report_desc_trusses", li.getreport_desc_trusses());
        values.put("report_desc_economic_life", li.getreport_desc_economic_life());
        values.put("report_desc_effective_age", li.getreport_desc_effective_age());
        values.put("report_desc_imp_remain_life", li.getreport_desc_imp_remain_life());
        values.put("report_desc_occupants", li.getreport_desc_occupants());
        values.put("report_desc_owned_or_leased", li.getreport_desc_owned_or_leased());
        values.put("report_desc_imp_floor_area", li.getreport_desc_imp_floor_area());
        values.put("report_desc_confirmed_thru", li.getreport_desc_confirmed_thru());
        values.put("report_desc_observed_condition", li.getreport_desc_observed_condition());
        values.put("report_ownership_of_property", li.getreport_ownership_of_property());
        values.put("report_impsummary1_no_of_bedrooms", li.getreport_impsummary1_no_of_bedrooms());
        values.put("valrep_landimp_impsummary1_no_of_tb", li.getvalrep_landimp_impsummary1_no_of_tb());
        values.put("valrep_landimp_desc_stairs", li.getvalrep_landimp_desc_stairs());
        // Inserting Row
        db.insert(tbl_land_improvements_imp_details, null, values);
        db.close(); // Closing database connection
    }

    // Getting selected Land improvements Desc of Imp via record id only
    public List<Land_Improvements_API_Imp_Details> getLand_Improvements_Imp_Details(String record_id) {
        List<Land_Improvements_API_Imp_Details> land_improvementsList = new ArrayList<Land_Improvements_API_Imp_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_land_improvements_imp_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Land_Improvements_API_Imp_Details li = new Land_Improvements_API_Imp_Details();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_impsummary1_no_of_floors(cursor.getString(2));
                li.setreport_impsummary1_building_desc(cursor.getString(3));
                li.setreport_impsummary1_erected_on_lot(cursor.getString(4));
                li.setreport_impsummary1_fa(cursor.getString(5));
                li.setreport_impsummary1_fa_per_td(cursor.getString(6));
                li.setreport_impsummary1_actual_utilization(cursor.getString(7));
                li.setreport_impsummary1_usage_declaration(cursor.getString(8));
                li.setreport_impsummary1_owner(cursor.getString(9));
                li.setreport_impsummary1_socialized_housing(cursor.getString(10));
                li.setreport_desc_property_type(cursor.getString(11));
                li.setreport_desc_foundation(cursor.getString(12));
                li.setreport_desc_columns_posts(cursor.getString(13));
                li.setreport_desc_beams(cursor.getString(14));
                li.setreport_desc_exterior_walls(cursor.getString(15));
                li.setreport_desc_interior_walls(cursor.getString(16));
                li.setreport_desc_imp_flooring(cursor.getString(17));
                li.setreport_desc_doors(cursor.getString(18));
                li.setreport_desc_imp_windows(cursor.getString(19));
                li.setreport_desc_ceiling(cursor.getString(20));
                li.setreport_desc_imp_roofing(cursor.getString(21));
                li.setreport_desc_trusses(cursor.getString(22));
                li.setreport_desc_economic_life(cursor.getString(23));
                li.setreport_desc_effective_age(cursor.getString(24));
                li.setreport_desc_imp_remain_life(cursor.getString(25));
                li.setreport_desc_occupants(cursor.getString(26));
                li.setreport_desc_owned_or_leased(cursor.getString(27));
                li.setreport_desc_imp_floor_area(cursor.getString(28));
                li.setreport_desc_confirmed_thru(cursor.getString(29));
                li.setreport_desc_observed_condition(cursor.getString(30));
                li.setreport_ownership_of_property(cursor.getString(31));
                li.setreport_impsummary1_no_of_bedrooms(cursor.getString(32));
                li.setvalrep_landimp_impsummary1_no_of_tb(cursor.getString(33));
                li.setvalrep_landimp_desc_stairs(cursor.getString(34));
                // Adding Land Improvements
                land_improvementsList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return land_improvementsList;
    }

    // Getting selected Land improvements Desc of Imp with imp_details_id
    public List<Land_Improvements_API_Imp_Details> getLand_Improvements_Imp_Details_with_bldg_desc(String record_id, String imp_details_id) {
        List<Land_Improvements_API_Imp_Details> land_improvementsList = new ArrayList<Land_Improvements_API_Imp_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_land_improvements_imp_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND imp_details_id = \"" + imp_details_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Land_Improvements_API_Imp_Details li = new Land_Improvements_API_Imp_Details();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_impsummary1_no_of_floors(cursor.getString(2));
                li.setreport_impsummary1_building_desc(cursor.getString(3));
                li.setreport_impsummary1_erected_on_lot(cursor.getString(4));
                li.setreport_impsummary1_fa(cursor.getString(5));
                li.setreport_impsummary1_fa_per_td(cursor.getString(6));
                li.setreport_impsummary1_actual_utilization(cursor.getString(7));
                li.setreport_impsummary1_usage_declaration(cursor.getString(8));
                li.setreport_impsummary1_owner(cursor.getString(9));
                li.setreport_impsummary1_socialized_housing(cursor.getString(10));
                li.setreport_desc_property_type(cursor.getString(11));
                li.setreport_desc_foundation(cursor.getString(12));
                li.setreport_desc_columns_posts(cursor.getString(13));
                li.setreport_desc_beams(cursor.getString(14));
                li.setreport_desc_exterior_walls(cursor.getString(15));
                li.setreport_desc_interior_walls(cursor.getString(16));
                li.setreport_desc_imp_flooring(cursor.getString(17));
                li.setreport_desc_doors(cursor.getString(18));
                li.setreport_desc_imp_windows(cursor.getString(19));
                li.setreport_desc_ceiling(cursor.getString(20));
                li.setreport_desc_imp_roofing(cursor.getString(21));
                li.setreport_desc_trusses(cursor.getString(22));
                li.setreport_desc_economic_life(cursor.getString(23));
                li.setreport_desc_effective_age(cursor.getString(24));
                li.setreport_desc_imp_remain_life(cursor.getString(25));
                li.setreport_desc_occupants(cursor.getString(26));
                li.setreport_desc_owned_or_leased(cursor.getString(27));
                li.setreport_desc_imp_floor_area(cursor.getString(28));
                li.setreport_desc_confirmed_thru(cursor.getString(29));
                li.setreport_desc_observed_condition(cursor.getString(30));
                li.setreport_ownership_of_property(cursor.getString(31));
                li.setreport_impsummary1_no_of_bedrooms(cursor.getString(32));
                li.setvalrep_landimp_impsummary1_no_of_tb(cursor.getString(33));
                li.setvalrep_landimp_desc_stairs(cursor.getString(34));
                // Adding Land Improvements
                land_improvementsList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return land_improvementsList;
    }

    // Updating single land improvements desc of imp with imp_details_id
    public int updateLand_Improvements_Imp_Details(Land_Improvements_API_Imp_Details li,
                                                   String record_id, String imp_details_id_str) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_impsummary1_no_of_floors", li.getreport_impsummary1_no_of_floors());
        values.put("report_impsummary1_building_desc", li.getreport_impsummary1_building_desc());
        values.put("report_impsummary1_erected_on_lot", li.getreport_impsummary1_erected_on_lot());
        values.put("report_impsummary1_fa", li.getreport_impsummary1_fa());
        values.put("report_impsummary1_fa_per_td", li.getreport_impsummary1_fa_per_td());
        values.put("report_impsummary1_actual_utilization", li.getreport_impsummary1_actual_utilization());
        values.put("report_impsummary1_usage_declaration", li.getreport_impsummary1_usage_declaration());
        values.put("report_impsummary1_owner", li.getreport_impsummary1_owner());
        values.put("report_impsummary1_socialized_housing", li.getreport_impsummary1_socialized_housing());
        values.put("report_desc_property_type", li.getreport_desc_property_type());
        values.put("report_desc_foundation", li.getreport_desc_foundation());
        values.put("report_desc_columns_posts", li.getreport_desc_columns_posts());
        values.put("report_desc_beams", li.getreport_desc_beams());
        values.put("report_desc_exterior_walls", li.getreport_desc_exterior_walls());
        values.put("report_desc_interior_walls", li.getreport_desc_interior_walls());
        values.put("report_desc_imp_flooring", li.getreport_desc_imp_flooring());
        values.put("report_desc_doors", li.getreport_desc_doors());
        values.put("report_desc_imp_windows", li.getreport_desc_imp_windows());
        values.put("report_desc_ceiling", li.getreport_desc_ceiling());
        values.put("report_desc_imp_roofing", li.getreport_desc_imp_roofing());
        values.put("report_desc_trusses", li.getreport_desc_trusses());
        values.put("report_desc_economic_life", li.getreport_desc_economic_life());
        values.put("report_desc_effective_age", li.getreport_desc_effective_age());
        values.put("report_desc_imp_remain_life", li.getreport_desc_imp_remain_life());
        values.put("report_desc_occupants", li.getreport_desc_occupants());
        values.put("report_desc_owned_or_leased", li.getreport_desc_owned_or_leased());
        values.put("report_desc_imp_floor_area", li.getreport_desc_imp_floor_area());
        values.put("report_desc_confirmed_thru", li.getreport_desc_confirmed_thru());
        values.put("report_desc_observed_condition", li.getreport_desc_observed_condition());
        values.put("report_ownership_of_property", li.getreport_ownership_of_property());
        values.put("report_impsummary1_no_of_bedrooms", li.getreport_impsummary1_no_of_bedrooms());
        values.put("valrep_landimp_impsummary1_no_of_tb", li.getvalrep_landimp_impsummary1_no_of_tb());
        values.put("valrep_landimp_desc_stairs", li.getvalrep_landimp_desc_stairs());
        // updating row
        return db.update(tbl_land_improvements_imp_details, values,
                Report_record_id + " = ? AND " + imp_details_id + " = ?",
                new String[]{record_id, imp_details_id_str});
    }

    // Deleting All Land Improvements Desc of Imp
    public int deleteLand_Improvements_API_Imp_Details(Land_Improvements_API_Imp_Details li) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_land_improvements_imp_details, Report_record_id + " = ?",
                new String[]{String.valueOf(li.getrecord_id())});
    }

    // Deleting single desc of imp via record_id and imp_details_id
    public int deleteLand_Improvements_API_Imp_Details_Single(String record_id, String imp_details_id_str) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_land_improvements_imp_details,
                Report_record_id + " = ? AND " + imp_details_id + " = ?",
                new String[]{record_id, imp_details_id_str});
    }

    /**
     * Land imp Desc of Imp Features CRUD
     */
    //adding desc of imp features
    public void addLand_Improvements_Imp_Details_Features(Land_Improvements_API_Imp_Details_Features li) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", li.getrecord_id());
        values.put("imp_details_id", li.getimp_details_id());
        values.put("report_impsummary1_building_desc", li.getreport_impsummary1_building_desc());
        values.put("report_desc_features_area", li.getreport_desc_features_area());
        values.put("report_desc_features_area_desc", li.getreport_desc_features_area_desc());
        // Inserting Row
        db.insert(tbl_land_improvements_imp_details_features, null, values);
        db.close(); // Closing database connection
    }

    // Getting All Land improvements Desc of Imp Features via record id only for json
    public List<Land_Improvements_API_Imp_Details_Features> getAllLand_Improvements_Imp_Details_Features(
            String record_id) {
        List<Land_Improvements_API_Imp_Details_Features> land_improvementsList = new ArrayList<Land_Improvements_API_Imp_Details_Features>();
        // Select All Query
        String selectQuery = "Select * FROM "
                + tbl_land_improvements_imp_details_features + " WHERE "
                + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Land_Improvements_API_Imp_Details_Features li = new Land_Improvements_API_Imp_Details_Features();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setimp_details_id(cursor.getString(2));
                li.setreport_impsummary1_building_desc(cursor.getString(3));
                li.setreport_desc_features_area(cursor.getString(4));
                li.setreport_desc_features_area_desc(cursor.getString(5));

                // Adding Land Improvements
                land_improvementsList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return land_improvementsList;
    }

    // Getting selected Land improvements Desc of Imp Features via record id and imp_details_id
    public List<Land_Improvements_API_Imp_Details_Features> getLand_Improvements_Imp_Details_Features(
            String record_id, String imp_details_id_str) {
        List<Land_Improvements_API_Imp_Details_Features> land_improvementsList = new ArrayList<Land_Improvements_API_Imp_Details_Features>();
        // Select All Query
        String selectQuery = "Select * FROM "
                + tbl_land_improvements_imp_details_features + " WHERE "
                + Report_record_id + " =  \"" + record_id + "\" AND "
                + imp_details_id + " =  \"" + imp_details_id_str + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Land_Improvements_API_Imp_Details_Features li = new Land_Improvements_API_Imp_Details_Features();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setimp_details_id(cursor.getString(2));
                li.setreport_impsummary1_building_desc(cursor.getString(3));
                li.setreport_desc_features_area(cursor.getString(4));
                li.setreport_desc_features_area_desc(cursor.getString(5));

                // Adding Land Improvements
                land_improvementsList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return land_improvementsList;
    }

    // Getting selected Land improvements Desc of Imp Features via record id and imp_details_features_id
    public List<Land_Improvements_API_Imp_Details_Features> getLand_Improvements_Imp_Details_Features_Single(
            String record_id, String imp_details_features_id_str) {
        List<Land_Improvements_API_Imp_Details_Features> land_improvementsList = new ArrayList<Land_Improvements_API_Imp_Details_Features>();
        // Select All Query
        String selectQuery = "Select * FROM "
                + tbl_land_improvements_imp_details_features + " WHERE "
                + Report_record_id + " =  \"" + record_id + "\" AND "
                + imp_details_features_id + " =  \"" + imp_details_features_id_str + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Land_Improvements_API_Imp_Details_Features li = new Land_Improvements_API_Imp_Details_Features();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setimp_details_id(cursor.getString(2));
                li.setreport_impsummary1_building_desc(cursor.getString(3));
                li.setreport_desc_features_area(cursor.getString(4));
                li.setreport_desc_features_area_desc(cursor.getString(5));

                // Adding Land Improvements
                land_improvementsList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return land_improvementsList;
    }

    // Updating single land improvements lot details features with imp_details_id
    public int updateLand_Improvements_Imp_Details_Features(Land_Improvements_API_Imp_Details_Features li,
                                                            String record_id, String imp_details_str) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(report_desc_features_area, li.getreport_desc_features_area());
        values.put(report_desc_features_area_desc, li.getreport_desc_features_area_desc());

        // updating row
        return db.update(tbl_land_improvements_imp_details_features, values,
                Report_record_id + " = ? AND " + imp_details_features_id + " = ?",
                new String[]{record_id, imp_details_str});
    }

    // Deleting single Desc of Imp via record_id and imp_details_id
    public int deleteLand_Improvements_API_Imp_Details_Features(String record_id, String imp_details_id_str) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_land_improvements_imp_details_features,
                Report_record_id + " = ? AND " + imp_details_id + " = ?",
                new String[]{record_id, imp_details_id_str});
    }

    // Deleting single desc of imp via record_id and imp_details_features_id
    public int deleteLand_Improvements_API_Imp_Details_Features_Single(String record_id, String imp_details_features_id_str) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_land_improvements_imp_details_features,
                Report_record_id + " = ? AND " + imp_details_features_id + " = ?",
                new String[]{record_id, imp_details_features_id_str});
    }

    /**
     * Land imp lot_details_valuation CRUD (Market Valuation)
     */
    public void addLand_Improvements_Lot_Valuation_Details(Land_Improvements_API_Lot_Valuation_Details li) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", li.getrecord_id());
        values.put("report_value_tct_no", li.getreport_value_tct_no());
        values.put("report_value_lot_no", li.getreport_value_lot_no());
        values.put("report_value_block_no", li.getreport_value_block_no());
        values.put("report_value_area", li.getreport_value_area());
        values.put("report_value_deduction", li.getreport_value_deduction());
        values.put("report_value_net_area", li.getreport_value_net_area());
        values.put("report_value_unit_value", li.getreport_value_unit_value());
        values.put("report_value_land_value", li.getreport_value_land_value());

        // Inserting Row
        db.insert(tbl_land_improvements_lot_valuation_details, null, values);
        db.close(); // Closing database connection
    }

    // Getting selected Land improvements Lot Valuation Details
    public List<Land_Improvements_API_Lot_Valuation_Details> getLand_Improvements_Lot_Valuation_Details(String record_id) {
        List<Land_Improvements_API_Lot_Valuation_Details> land_improvementsList = new ArrayList<Land_Improvements_API_Lot_Valuation_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_land_improvements_lot_valuation_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Land_Improvements_API_Lot_Valuation_Details li = new Land_Improvements_API_Lot_Valuation_Details();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_value_tct_no(cursor.getString(2));
                li.setreport_value_lot_no(cursor.getString(3));
                li.setreport_value_block_no(cursor.getString(4));
                li.setreport_value_area(cursor.getString(5));
                li.setreport_value_deduction(cursor.getString(6));
                li.setreport_value_net_area(cursor.getString(7));
                li.setreport_value_unit_value(cursor.getString(8));
                li.setreport_value_land_value(cursor.getString(9));

                // Adding Land Improvements
                land_improvementsList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return land_improvementsList;
    }

    //getting the total land value
    public String getLand_Improvements_API_Total_Land_Value_Lot_Valuation_Details(String record_id) {
        String result = "0";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT coalesce(SUM(report_value_land_value),0) FROM " + tbl_land_improvements_lot_valuation_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"", null);
        if (cursor.moveToFirst()) {
            result = cursor.getString(0);
        }


        if(cursor != null) {
            cursor.close();
        }

        return result;
    }


    // Getting selected Land improvements Lot Valuation Details with lot_valuation_details_id
    public List<Land_Improvements_API_Lot_Valuation_Details> getLand_Improvements_Lot_Valuation_Details_with_lot_valuation_details_id(String record_id, String lot_valuation_details_id) {
        List<Land_Improvements_API_Lot_Valuation_Details> land_improvementsList = new ArrayList<Land_Improvements_API_Lot_Valuation_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_land_improvements_lot_valuation_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND " + KEY_ID + " = \"" + lot_valuation_details_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Land_Improvements_API_Lot_Valuation_Details li = new Land_Improvements_API_Lot_Valuation_Details();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_value_tct_no(cursor.getString(2));
                li.setreport_value_lot_no(cursor.getString(3));
                li.setreport_value_block_no(cursor.getString(4));
                li.setreport_value_area(cursor.getString(5));
                li.setreport_value_deduction(cursor.getString(6));
                li.setreport_value_net_area(cursor.getString(7));
                li.setreport_value_unit_value(cursor.getString(8));
                li.setreport_value_land_value(cursor.getString(9));

                // Adding Land Improvements
                land_improvementsList.add(li);
            } while (cursor.moveToNext());
        }
        db.close();
        // return land_improvementsList
        return land_improvementsList;
    }

    // Updating single land improvements Lot Valuation Details with lot_valuation_details_id
    public int updateLand_Improvements_Lot_Valuation_Details(Land_Improvements_API_Lot_Valuation_Details li,
                                                             String record_id, String lot_valuation_details_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_value_tct_no", li.getreport_value_tct_no());
        values.put("report_value_lot_no", li.getreport_value_lot_no());
        values.put("report_value_block_no", li.getreport_value_block_no());
        values.put("report_value_area", li.getreport_value_area());
        values.put("report_value_deduction", li.getreport_value_deduction());
        values.put("report_value_net_area", li.getreport_value_net_area());
        values.put("report_value_unit_value", li.getreport_value_unit_value());
        values.put("report_value_land_value", li.getreport_value_land_value());
        // updating row
        return db.update(tbl_land_improvements_lot_valuation_details, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, lot_valuation_details_id});
    }

    // Updating single land improvements Lot Valuation Details with lot_valuation_details_id
    public int updateLand_Improvements_Lot_Valuation_Details2(Land_Improvements_API_Lot_Valuation_Details li,
                                                              String record_id, String lot_valuation_details_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_value_tct_no", li.getreport_value_tct_no());
        values.put("report_value_lot_no", li.getreport_value_lot_no());
        values.put("report_value_block_no", li.getreport_value_block_no());
        values.put("report_value_area", li.getreport_value_area());
        values.put("report_value_net_area", li.getreport_value_net_area());
        values.put("report_value_land_value", li.getreport_value_land_value());
        // updating row
        return db.update(tbl_land_improvements_lot_valuation_details, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, lot_valuation_details_id});
    }

    // Updating single land improvements Lot Valuation Details with lot_valuation_details_id
    public int updateLand_Improvements_Lot_Valuation_Details3(Land_Improvements_API_Lot_Valuation_Details li,
                                                              String record_id, String lot_valuation_details_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_value_net_area", li.getreport_value_net_area());
        values.put("report_value_land_value", li.getreport_value_land_value());
        // updating row
        return db.update(tbl_land_improvements_lot_valuation_details, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, lot_valuation_details_id});
    }

    //getting the total land value
    public String getLand_Improvements_API_grand_total_land_value(String record_id) {
        String result = "0";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT coalesce(sum(report_value_land_value),0) FROM " + tbl_land_improvements_lot_valuation_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"", null);
        if (cursor.moveToFirst()) {
            result = cursor.getString(0);
        }


        if(cursor != null) {
            cursor.close();
        }

        return result;
    }

    // Deleting All Land Improvements Lot Valuation Details
    public int deleteLand_Improvements_API_Lot_Valuation_Details(Land_Improvements_API_Lot_Valuation_Details li) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_land_improvements_lot_valuation_details, Report_record_id + " = ?",
                new String[]{String.valueOf(li.getrecord_id())});
    }

    // Deleting single Lot Valuation Details via record_id and lot_valuation_details_id
    public int deleteLand_Improvements_API_Lot_Valuation_Details_Single(String record_id, String lot_details_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_land_improvements_lot_valuation_details,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, lot_details_id});
    }

    /**
     * Land imp Cost Approach CRUD (Cost Valuation)
     */
    public void addLand_Improvements_Cost_Valuation(Land_Improvements_API_Cost_Valuation li) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", li.getrecord_id());
        values.put("report_imp_value_description", li.getreport_imp_value_description());
        values.put("report_imp_value_total_area", li.getreport_imp_value_total_area());
        values.put("report_imp_value_total_reproduction_cost", li.getreport_imp_value_total_reproduction_cost());
        values.put("report_imp_value_depreciation", li.getreport_imp_value_depreciation());
        values.put("report_imp_value_less_reproduction", li.getreport_imp_value_less_reproduction());
        values.put("report_imp_value_depreciated_value", li.getreport_imp_value_depreciated_value());

        values.put("valrep_landimp_imp_value_phys_cur_unit_value", li.getvalrep_landimp_imp_value_phys_cur_unit_value());
        values.put("valrep_landimp_imp_value_phys_cur_amt", li.getvalrep_landimp_imp_value_phys_cur_amt());
        values.put("valrep_landimp_imp_value_func_cur_unit_value", li.getvalrep_landimp_imp_value_func_cur_unit_value());
        values.put("valrep_landimp_imp_value_func_cur_amt", li.getvalrep_landimp_imp_value_func_cur_amt());
        values.put("valrep_landimp_imp_value_less_cur", li.getvalrep_landimp_imp_value_less_cur());

        // Inserting Row
        db.insert(tbl_land_improvements_imp_valuation, null, values);
        db.close(); // Closing database connection
    }

    // Getting selected Land improvements cost Valuation all
    public List<Land_Improvements_API_Cost_Valuation> getLand_Improvements_Cost_Valuation(String record_id) {
        List<Land_Improvements_API_Cost_Valuation> land_improvementsList = new ArrayList<Land_Improvements_API_Cost_Valuation>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_land_improvements_imp_valuation
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Land_Improvements_API_Cost_Valuation li = new Land_Improvements_API_Cost_Valuation();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_imp_value_description(cursor.getString(2));
                li.setreport_imp_value_total_area(cursor.getString(3));
                li.setreport_imp_value_total_reproduction_cost(cursor.getString(4));
                li.setreport_imp_value_depreciation(cursor.getString(5));
                li.setreport_imp_value_less_reproduction(cursor.getString(6));
                li.setreport_imp_value_depreciated_value(cursor.getString(7));

                li.setvalrep_landimp_imp_value_phys_cur_unit_value(cursor.getString(8));
                li.setvalrep_landimp_imp_value_phys_cur_amt(cursor.getString(9));
                li.setvalrep_landimp_imp_value_func_cur_unit_value(cursor.getString(10));
                li.setvalrep_landimp_imp_value_func_cur_amt(cursor.getString(11));
                li.setvalrep_landimp_imp_value_less_cur(cursor.getString(12));

                // Adding Land Improvements
                land_improvementsList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return land_improvementsList;
    }

    // Getting specific Land improvements cost Valuation based on Desc of imp row
    public List<Land_Improvements_API_Cost_Valuation> getLand_Improvements_Cost_Valuation_ID(String record_id, int rownum) {

        List<Land_Improvements_API_Cost_Valuation> land_improvementsList = new ArrayList<Land_Improvements_API_Cost_Valuation>();
        // Select All Query
        String selectQuery = "Select imp_valuation_id FROM " + tbl_land_improvements_imp_valuation
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        //String selectQuery = "Select imp_valuation_id FROM " + tbl_land_improvements_imp_valuation
        //		+ " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND ROWID = "+rownum;
        //String selectQuery = "Select imp_valuation_id FROM land_improvements_imp_valuation WHERE record_id =  \"" + record_id + "\"";
        //(Select count(imp_valuation_id) FROM land_improvements_imp_valuation WHERE record_id =  \"" + record_id + "\") 
        /*Select imp_valuation_id
		FROM land_improvements_imp_valuation
		WHERE record_id = "54617ab9f1f179b782009258"
		LIMIT (
		Select count(imp_valuation_id)
		FROM land_improvements_imp_valuation
		WHERE record_id = "54617ab9f1f179b782009258"
		)
		OFFSET rownum*/

        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Land_Improvements_API_Cost_Valuation li = new Land_Improvements_API_Cost_Valuation();

                li.setID(Integer.parseInt(cursor.getString(0)));

                // Adding Land Improvements
                land_improvementsList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return land_improvementsList;
    }

    // Getting selected Land improvements cost Valuation single
    public List<Land_Improvements_API_Cost_Valuation> getLand_Improvements_Cost_Valuation(String record_id, String cost_valuation_id) {
        List<Land_Improvements_API_Cost_Valuation> land_improvementsList = new ArrayList<Land_Improvements_API_Cost_Valuation>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_land_improvements_imp_valuation
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND " + imp_valuation_id + " = \"" + cost_valuation_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Land_Improvements_API_Cost_Valuation li = new Land_Improvements_API_Cost_Valuation();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_imp_value_description(cursor.getString(2));
                li.setreport_imp_value_total_area(cursor.getString(3));
                li.setreport_imp_value_total_reproduction_cost(cursor.getString(4));
                li.setreport_imp_value_depreciation(cursor.getString(5));
                li.setreport_imp_value_less_reproduction(cursor.getString(6));
                li.setreport_imp_value_depreciated_value(cursor.getString(7));

                li.setvalrep_landimp_imp_value_phys_cur_unit_value(cursor.getString(8));
                li.setvalrep_landimp_imp_value_phys_cur_amt(cursor.getString(9));
                li.setvalrep_landimp_imp_value_func_cur_unit_value(cursor.getString(10));
                li.setvalrep_landimp_imp_value_func_cur_amt(cursor.getString(11));
                li.setvalrep_landimp_imp_value_less_cur(cursor.getString(12));
                // Adding Land Improvements
                land_improvementsList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return land_improvementsList;
    }

    // Updating single land improvements cost Valuation
    public int updateLand_Improvements_Cost_Valuation(Land_Improvements_API_Cost_Valuation li,
                                                      String record_id, String cost_valuation_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("report_imp_value_description", li.getreport_imp_value_description());
        values.put("report_imp_value_total_area", li.getreport_imp_value_total_area());
        values.put("report_imp_value_total_reproduction_cost", li.getreport_imp_value_total_reproduction_cost());
        values.put("report_imp_value_depreciation", li.getreport_imp_value_depreciation());
        values.put("report_imp_value_less_reproduction", li.getreport_imp_value_less_reproduction());
        values.put("report_imp_value_depreciated_value", li.getreport_imp_value_depreciated_value());

        values.put("valrep_landimp_imp_value_phys_cur_unit_value", li.getvalrep_landimp_imp_value_phys_cur_unit_value());
        values.put("valrep_landimp_imp_value_phys_cur_amt", li.getvalrep_landimp_imp_value_phys_cur_amt());
        values.put("valrep_landimp_imp_value_func_cur_unit_value", li.getvalrep_landimp_imp_value_func_cur_unit_value());
        values.put("valrep_landimp_imp_value_func_cur_amt", li.getvalrep_landimp_imp_value_func_cur_amt());
        values.put("valrep_landimp_imp_value_less_cur", li.getvalrep_landimp_imp_value_less_cur());
        // updating row
        return db.update(tbl_land_improvements_imp_valuation, values,
                Report_record_id + " = ? AND " + imp_valuation_id + " = ?",
                new String[]{record_id, cost_valuation_id});
    }

    //getting the total depreciated value
    public String getLand_Improvements_API_Total_depreciated_value(String record_id) {
        String result = "0";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT coalesce(sum(report_imp_value_depreciated_value),0) FROM " + tbl_land_improvements_imp_valuation
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"", null);
        if (cursor.moveToFirst()) {
            result = cursor.getString(0);
        }


        if(cursor != null) {
            cursor.close();
        }

        return result;
    }

    //special update
    // Updating single land improvements cost Valuation
    public int updateLand_Improvements_Cost_Valuation_2(Land_Improvements_API_Cost_Valuation li,
                                                        String record_id, String cost_valuation_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("report_imp_value_total_area", li.getreport_imp_value_total_area());
        values.put("report_imp_value_total_reproduction_cost", li.getreport_imp_value_total_reproduction_cost());
        // updating row
        return db.update(tbl_land_improvements_imp_valuation, values,
                Report_record_id + " = ? AND " + imp_valuation_id + " = ?",
                new String[]{record_id, cost_valuation_id});
    }

    // Updating single land improvements cost Valuation
    public int updateLand_Improvements_Cost_Valuation3(Land_Improvements_API_Cost_Valuation li,
                                                       String record_id, String cost_valuation_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("report_imp_value_less_reproduction", li.getreport_imp_value_less_reproduction());
        values.put("report_imp_value_depreciated_value", li.getreport_imp_value_depreciated_value());
        // updating row
        return db.update(tbl_land_improvements_imp_valuation, values,
                Report_record_id + " = ? AND " + imp_valuation_id + " = ?",
                new String[]{record_id, cost_valuation_id});
    }

    //special update
    // Updating single land improvements cost Valuation report_imp_value_depreciation only
    public int updateLand_Improvements_Cost_Valuation_3(Land_Improvements_API_Cost_Valuation li,
                                                        String record_id, String cost_valuation_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("report_imp_value_depreciation", li.getreport_imp_value_depreciation());
        // updating row
        return db.update(tbl_land_improvements_imp_valuation, values,
                Report_record_id + " = ? AND " + imp_valuation_id + " = ?",
                new String[]{record_id, cost_valuation_id});
    }

    // Deleting All Land Improvements cost Valuation
    public int deleteLand_Improvements_Cost_Valuation(String record_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_land_improvements_imp_valuation,
                Report_record_id + " = ?",
                new String[]{record_id});
    }

    // Deleting single cost Valuation Details
    public int deleteLand_Improvements_Cost_Valuation(String record_id, String cost_valuation_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_land_improvements_imp_valuation,
                Report_record_id + " = ? AND " + imp_valuation_id + " = ?",
                new String[]{record_id, cost_valuation_id});
    }

    /**
     * Land imp Cost Approach Details CRUD (Cost Valuation Details)
     */
    public void addLand_Improvements_Cost_Valuation_Details(Land_Improvements_API_Cost_Valuation_Details li) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", li.getrecord_id());
        values.put("imp_valuation_id", li.getimp_valuation_id());
        values.put("report_imp_value_kind", li.getreport_imp_value_kind());
        values.put("report_imp_value_area", li.getreport_imp_value_area());
        values.put("report_imp_value_cost_per_sqm", li.getreport_imp_value_cost_per_sqm());
        values.put("report_imp_value_reproduction_cost", li.getreport_imp_value_reproduction_cost());

        // Inserting Row
        db.insert(tbl_land_improvements_imp_valuation_details, null, values);
        db.close(); // Closing database connection
    }

    // Getting selected Land improvements cost Valuation details all
    public List<Land_Improvements_API_Cost_Valuation_Details> getLand_Improvements_Cost_Valuation_Details(String record_id) {
        List<Land_Improvements_API_Cost_Valuation_Details> land_improvementsList = new ArrayList<Land_Improvements_API_Cost_Valuation_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_land_improvements_imp_valuation_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Land_Improvements_API_Cost_Valuation_Details li = new Land_Improvements_API_Cost_Valuation_Details();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setimp_valuation_id(cursor.getString(2));
                li.setreport_imp_value_kind(cursor.getString(3));
                li.setreport_imp_value_area(cursor.getString(4));
                li.setreport_imp_value_cost_per_sqm(cursor.getString(5));
                li.setreport_imp_value_reproduction_cost(cursor.getString(6));


                // Adding Land Improvements
                land_improvementsList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return land_improvementsList;
    }

    // Getting selected Land improvements cost Valuation details via imp_valuation_id
    public List<Land_Improvements_API_Cost_Valuation_Details> getLand_Improvements_Cost_Valuation_Details(String record_id, String cost_valuation_id) {
        List<Land_Improvements_API_Cost_Valuation_Details> land_improvementsList = new ArrayList<Land_Improvements_API_Cost_Valuation_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_land_improvements_imp_valuation_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND " + imp_valuation_id + " = \"" + cost_valuation_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Land_Improvements_API_Cost_Valuation_Details li = new Land_Improvements_API_Cost_Valuation_Details();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setimp_valuation_id(cursor.getString(2));
                li.setreport_imp_value_kind(cursor.getString(3));
                li.setreport_imp_value_area(cursor.getString(4));
                li.setreport_imp_value_cost_per_sqm(cursor.getString(5));
                li.setreport_imp_value_reproduction_cost(cursor.getString(6));

                // Adding Land Improvements
                land_improvementsList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return land_improvementsList;
    }

    // Getting selected Land improvements cost Valuation details single
    public List<Land_Improvements_API_Cost_Valuation_Details> getLand_Improvements_Cost_Valuation_Details_Single(String record_id, String cost_valuation_details_id) {
        List<Land_Improvements_API_Cost_Valuation_Details> land_improvementsList = new ArrayList<Land_Improvements_API_Cost_Valuation_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_land_improvements_imp_valuation_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND " + imp_valuation_details_id + " = \"" + cost_valuation_details_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Land_Improvements_API_Cost_Valuation_Details li = new Land_Improvements_API_Cost_Valuation_Details();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setimp_valuation_id(cursor.getString(2));
                li.setreport_imp_value_kind(cursor.getString(3));
                li.setreport_imp_value_area(cursor.getString(4));
                li.setreport_imp_value_cost_per_sqm(cursor.getString(5));
                li.setreport_imp_value_reproduction_cost(cursor.getString(6));

                // Adding Land Improvements
                land_improvementsList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return land_improvementsList;
    }

    // Updating single land improvements cost Valuation details
    public int updateLand_Improvements_Cost_Valuation_Details(Land_Improvements_API_Cost_Valuation_Details li,
                                                              String record_id, String cost_valuation_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("report_imp_value_kind", li.getreport_imp_value_kind());
        values.put("report_imp_value_area", li.getreport_imp_value_area());
        values.put("report_imp_value_cost_per_sqm", li.getreport_imp_value_cost_per_sqm());
        values.put("report_imp_value_reproduction_cost", li.getreport_imp_value_reproduction_cost());
        // updating row
        return db.update(tbl_land_improvements_imp_valuation_details, values,
                Report_record_id + " = ? AND " + imp_valuation_details_id + " = ?",
                new String[]{record_id, cost_valuation_id});
    }

    // Deleting All Land Improvements cost Valuation
    public int deleteLand_Improvements_Cost_Valuation_Details(String record_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_land_improvements_imp_valuation_details,
                Report_record_id + " = ?",
                new String[]{record_id});
    }

    // Deleting All Land Improvements cost Valuation
    public int deleteLand_Improvements_Cost_Valuation_Details(String record_id, String cost_valuation_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_land_improvements_imp_valuation_details,
                Report_record_id + " = ? AND " + imp_valuation_id + " = ?",
                new String[]{record_id, cost_valuation_id});
    }

    // Deleting single cost Valuation Details
    public int deleteLand_Improvements_Cost_Valuation_Details_Single(String record_id, String cost_valuation_details_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_land_improvements_imp_valuation_details,
                Report_record_id + " = ? AND " + imp_valuation_details_id + " = ?",
                new String[]{record_id, cost_valuation_details_id});
    }

    /**
     * Land imp RPDS CRUD
     */
    public void addLand_Improvements_RDPS(Land_Improvements_API_RDPS li) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", li.getrecord_id());
        values.put("report_prev_app_name", li.getreport_prev_app_name());
        values.put("report_prev_app_location", li.getreport_prev_app_location());
        values.put("report_prev_app_area", li.getreport_prev_app_area());
        values.put("report_prev_app_value", li.getreport_prev_app_value());
        values.put("report_prev_app_date", li.getreport_prev_app_date());
        // Inserting Row
        db.insert(tbl_land_improvements_prev_appraisal, null, values);
        db.close(); // Closing database connection
    }

    // Getting selected Land improvements rdps
    public List<Land_Improvements_API_RDPS> getLand_Improvements_RDPS(String record_id) {
        List<Land_Improvements_API_RDPS> land_improvementsList = new ArrayList<Land_Improvements_API_RDPS>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_land_improvements_prev_appraisal
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Land_Improvements_API_RDPS li = new Land_Improvements_API_RDPS();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_prev_app_name(cursor.getString(2));
                li.setreport_prev_app_location(cursor.getString(3));
                li.setreport_prev_app_area(cursor.getString(4));
                li.setreport_prev_app_value(cursor.getString(5));
                li.setreport_prev_app_date(cursor.getString(6));

                // Adding Land Improvements
                land_improvementsList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return land_improvementsList;
    }

    // Getting selected Land improvements rdps with id
    public List<Land_Improvements_API_RDPS> getLand_Improvements_RDPS_Single(String record_id, String rdps_id) {
        List<Land_Improvements_API_RDPS> land_improvementsList = new ArrayList<Land_Improvements_API_RDPS>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_land_improvements_prev_appraisal
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND " + KEY_ID + " = \"" + rdps_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Land_Improvements_API_RDPS li = new Land_Improvements_API_RDPS();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_prev_app_name(cursor.getString(2));
                li.setreport_prev_app_location(cursor.getString(3));
                li.setreport_prev_app_area(cursor.getString(4));
                li.setreport_prev_app_value(cursor.getString(5));
                li.setreport_prev_app_date(cursor.getString(6));

                // Adding Land Improvements
                land_improvementsList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return land_improvementsList;
    }

    // Deleting All rdps via record_id
    public int deleteLand_Improvements_RDPS(Land_Improvements_API_RDPS li) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_land_improvements_prev_appraisal, Report_record_id + " = ?",
                new String[]{String.valueOf(li.getrecord_id())});
    }

    // Deleting single rdps
    public int deleteLand_Improvements_RDPS_Single(String record_id, String rdps_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_land_improvements_prev_appraisal,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, rdps_id});
    }

    // Updating single Land_Improvements_API_RDPS with rdps_id
    public int updateLand_Improvements_RDPS(Land_Improvements_API_RDPS li,
                                            String record_id, String rdps_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_prev_app_name", li.getreport_prev_app_name());
        values.put("report_prev_app_location", li.getreport_prev_app_location());
        values.put("report_prev_app_area", li.getreport_prev_app_area());
        values.put("report_prev_app_value", li.getreport_prev_app_value());
        values.put("report_prev_app_date", li.getreport_prev_app_date());
        // updating row
        return db.update(tbl_land_improvements_prev_appraisal, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, rdps_id});
    }


    /**
     * Townhouse RPDS CRUD
     */
    public void addTownhouse_RDPS(Townhouse_API_RDPS li) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", li.getrecord_id());
        values.put("report_prev_app_name", li.getreport_prev_app_name());
        values.put("report_prev_app_location", li.getreport_prev_app_location());
        values.put("report_prev_app_area", li.getreport_prev_app_area());
        values.put("report_prev_app_value", li.getreport_prev_app_value());
        values.put("report_prev_app_date", li.getreport_prev_app_date());
        // Inserting Row
        db.insert(tbl_townhouse_prev_appraisal, null, values);
        db.close(); // Closing database connection
    }

    // Getting selected townhouse rdps
    public List<Townhouse_API_RDPS> getTownhouse_RDPS(String record_id) {
        List<Townhouse_API_RDPS> townhouseList = new ArrayList<Townhouse_API_RDPS>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_townhouse_prev_appraisal
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Townhouse_API_RDPS li = new Townhouse_API_RDPS();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_prev_app_name(cursor.getString(2));
                li.setreport_prev_app_location(cursor.getString(3));
                li.setreport_prev_app_area(cursor.getString(4));
                li.setreport_prev_app_value(cursor.getString(5));
                li.setreport_prev_app_date(cursor.getString(6));

                townhouseList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return townhouseList
        return townhouseList;
    }

    // Getting selected townhouse rdps with id
    public List<Townhouse_API_RDPS> getTownhouse_RDPS_Single(String record_id, String rdps_id) {
        List<Townhouse_API_RDPS> townhouseList = new ArrayList<Townhouse_API_RDPS>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_townhouse_prev_appraisal
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND " + KEY_ID + " = \"" + rdps_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Townhouse_API_RDPS li = new Townhouse_API_RDPS();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_prev_app_name(cursor.getString(2));
                li.setreport_prev_app_location(cursor.getString(3));
                li.setreport_prev_app_area(cursor.getString(4));
                li.setreport_prev_app_value(cursor.getString(5));
                li.setreport_prev_app_date(cursor.getString(6));

                // Adding Land Improvements
                townhouseList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return townhouseList
        return townhouseList;
    }

    // Deleting All rdps via record_id
    public int deleteTownhouse_RDPS(Townhouse_API_RDPS li) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_townhouse_prev_appraisal, Report_record_id + " = ?",
                new String[]{String.valueOf(li.getrecord_id())});
    }

    // Deleting single rdps
    public int deleteTownhouse_RDPS_Single(String record_id, String rdps_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_townhouse_prev_appraisal,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, rdps_id});
    }


    // Updating single Land_Improvements_API_RDPS with rdps_id
    public int updateTownhouse_RDPS(Townhouse_API_RDPS li,
                                    String record_id, String rdps_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_prev_app_name", li.getreport_prev_app_name());
        values.put("report_prev_app_location", li.getreport_prev_app_location());
        values.put("report_prev_app_area", li.getreport_prev_app_area());
        values.put("report_prev_app_value", li.getreport_prev_app_value());
        values.put("report_prev_app_date", li.getreport_prev_app_date());
        // updating row
        return db.update(tbl_townhouse_prev_appraisal, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, rdps_id});
    }


    public void addTownhouse_Lot_Details(Townhouse_API_Lot_Details li) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", li.getrecord_id());
        values.put("report_propdesc_tct_no", li.getreport_propdesc_tct_no());
        values.put("report_propdesc_lot", li.getreport_propdesc_lot());
        values.put("report_propdesc_block", li.getreport_propdesc_block());
        values.put("report_propdesc_survey_nos", li.getreport_propdesc_survey_nos());
        values.put("report_propdesc_area", li.getreport_propdesc_area());
        values.put("valrep_townhouse_propdesc_registry_date", li.getvalrep_townhouse_propdesc_registry_date());
        values.put("report_propdesc_registered_owner", li.getreport_propdesc_registered_owner());
        values.put("report_propdesc_registry_of_deeds", li.getreport_propdesc_registry_of_deeds());
        // Inserting Row
        db.insert(tbl_townhouse_lot_details, null, values);
        db.close(); // Closing database connection
    }

    // Getting selected Land improvements Prop Desc
    public List<Townhouse_API_Lot_Details> getTownhouse_Lot_Details(String record_id) {
        List<Townhouse_API_Lot_Details> land_improvementsList = new ArrayList<Townhouse_API_Lot_Details>();
        // Select ID Query
        String selectQuery = "Select * FROM " + tbl_townhouse_lot_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Townhouse_API_Lot_Details li = new Townhouse_API_Lot_Details();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_propdesc_tct_no(cursor.getString(2));
                li.setreport_propdesc_lot(cursor.getString(3));
                li.setreport_propdesc_block(cursor.getString(4));
                li.setreport_propdesc_survey_nos(cursor.getString(5));
                li.setreport_propdesc_area(cursor.getString(6));
                li.setvalrep_townhouse_propdesc_registry_date(cursor.getString(7));
                li.setreport_propdesc_registered_owner(cursor.getString(8));
                li.setreport_propdesc_registry_of_deeds(cursor.getString(9));

                // Adding Land Improvements
                land_improvementsList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return land_improvementsList;
    }

    // Getting specific Land improvements cost Valuation based on Desc of imp row
    public List<Townhouse_API_Lot_Details> getTownhouse_Lot_Details_ID(String record_id, int rownum) {
        List<Townhouse_API_Lot_Details> townhouseList = new ArrayList<Townhouse_API_Lot_Details>();

        // Select ID Query
        String selectQuery = "Select id FROM " + tbl_townhouse_lot_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";

        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Townhouse_API_Lot_Details li = new Townhouse_API_Lot_Details();
                li.setID(Integer.parseInt(cursor.getString(0)));
                // Adding Land Improvements
                townhouseList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return townhouseList;
    }

    // Getting selected Land improvements Prop Desc with lot_details_id
    public List<Townhouse_API_Lot_Details> getTownhouse_Lot_Details_with_lot_details_id(String record_id, String lot_details_id) {
        List<Townhouse_API_Lot_Details> land_improvementsList = new ArrayList<Townhouse_API_Lot_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_townhouse_lot_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND " + KEY_ID + " = \"" + lot_details_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Townhouse_API_Lot_Details li = new Townhouse_API_Lot_Details();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_propdesc_tct_no(cursor.getString(2));
                li.setreport_propdesc_lot(cursor.getString(3));
                li.setreport_propdesc_block(cursor.getString(4));
                li.setreport_propdesc_survey_nos(cursor.getString(5));
                li.setreport_propdesc_area(cursor.getString(6));
                li.setvalrep_townhouse_propdesc_registry_date(cursor.getString(7));
                li.setreport_propdesc_registered_owner(cursor.getString(8));
                li.setreport_propdesc_registry_of_deeds(cursor.getString(9));

                // Adding Land Improvements
                land_improvementsList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return land_improvementsList;
    }

    // Updating single land improvements prop desc with lot_details_id
    public int updateTownhouse_Lot_Details(Townhouse_API_Lot_Details li,
                                           String record_id, String lot_details_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(report_propdesc_tct_no, li.getreport_propdesc_tct_no());
        values.put(report_propdesc_area, li.getreport_propdesc_area());
        values.put(report_propdesc_lot, li.getreport_propdesc_lot());
        values.put(report_propdesc_block, li.getreport_propdesc_block());
        values.put(report_propdesc_survey_nos, li.getreport_propdesc_survey_nos());
        values.put(valrep_townhouse_propdesc_registry_date, li.getvalrep_townhouse_propdesc_registry_date());
        values.put(report_propdesc_registered_owner, li.getreport_propdesc_registered_owner());
        values.put(report_propdesc_registry_of_deeds, li.getreport_propdesc_registry_of_deeds());
        // updating row
        return db.update(tbl_townhouse_lot_details, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, lot_details_id});
    }

    // Deleting All Land Improvements Prop Desc
    public int deleteTownhouse_API_Lot_Details(Townhouse_API_Lot_Details li) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_townhouse_lot_details, Report_record_id + " = ?",
                new String[]{String.valueOf(li.getrecord_id())});
    }

    // Deleting single prop desc via record_id and lot_details_id
    public int deleteTownhouse_API_Lot_Details_Single(String record_id, String lot_details_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_townhouse_lot_details,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, lot_details_id});
    }

    /**
     * Townhouse lot_details_valuation CRUD (Market Valuation)
     */
    public void addTownhouse_Lot_Valuation_Details(Townhouse_API_Lot_Valuation_Details li) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", li.getrecord_id());
        values.put("report_value_tct_no", li.getreport_value_tct_no());
        values.put("report_value_lot_no", li.getreport_value_lot_no());
        values.put("report_value_block_no", li.getreport_value_block_no());
        values.put("report_value_lot_area", li.getreport_value_lot_area());
        values.put("report_value_floor_area", li.getreport_value_floor_area());
        values.put("report_value_unit_value", li.getreport_value_unit_value());
        values.put("report_value_land_value", li.getreport_value_land_value());

        // Inserting Row
        db.insert(tbl_townhouse_lot_valuation_details, null, values);
        db.close(); // Closing database connection
    }

    // Getting selected Townhouserovements Lot Valuation Details
    public List<Townhouse_API_Lot_Valuation_Details> getTownhouse_Lot_Valuation_Details(String record_id) {
        List<Townhouse_API_Lot_Valuation_Details> townhouseList = new ArrayList<Townhouse_API_Lot_Valuation_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_townhouse_lot_valuation_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Townhouse_API_Lot_Valuation_Details li = new Townhouse_API_Lot_Valuation_Details();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_value_tct_no(cursor.getString(2));
                li.setreport_value_lot_no(cursor.getString(3));
                li.setreport_value_block_no(cursor.getString(4));
                li.setreport_value_lot_area(cursor.getString(5));
                li.setreport_value_floor_area(cursor.getString(6));
                li.setreport_value_unit_value(cursor.getString(7));
                li.setreport_value_land_value(cursor.getString(8));

                // Adding Land Improvements
                townhouseList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return townhouseList
        return townhouseList;
    }

    // Getting selected Townhouserovements Lot Valuation Details with lot_valuation_details_id
    public List<Townhouse_API_Lot_Valuation_Details> getTownhouse_Lot_Valuation_Details_with_lot_valuation_details_id(String record_id, String lot_valuation_details_id) {
        List<Townhouse_API_Lot_Valuation_Details> townhouseList = new ArrayList<Townhouse_API_Lot_Valuation_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_townhouse_lot_valuation_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND " + KEY_ID + " = \"" + lot_valuation_details_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Townhouse_API_Lot_Valuation_Details li = new Townhouse_API_Lot_Valuation_Details();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_value_tct_no(cursor.getString(2));
                li.setreport_value_lot_no(cursor.getString(3));
                li.setreport_value_block_no(cursor.getString(4));
                li.setreport_value_lot_area(cursor.getString(5));
                li.setreport_value_floor_area(cursor.getString(6));
                li.setreport_value_unit_value(cursor.getString(7));
                li.setreport_value_land_value(cursor.getString(8));

                // Adding Land Improvements
                townhouseList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return townhouseList
        return townhouseList;
    }

    // Updating single townhouse Lot Valuation Details with lot_valuation_details_id
    public int updateTownhouse_Lot_Valuation_Details(Townhouse_API_Lot_Valuation_Details li,
                                                     String record_id, String lot_valuation_details_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_value_tct_no", li.getreport_value_tct_no());
        values.put("report_value_lot_no", li.getreport_value_lot_no());
        values.put("report_value_block_no", li.getreport_value_block_no());
        values.put("report_value_lot_area", li.getreport_value_lot_area());
        values.put("report_value_floor_area", li.getreport_value_floor_area());
        values.put("report_value_unit_value", li.getreport_value_unit_value());
        values.put("report_value_land_value", li.getreport_value_land_value());
        // updating row
        return db.update(tbl_townhouse_lot_valuation_details, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, lot_valuation_details_id});
    }

    // Updating single townhouse Lot Valuation Details with lot_valuation_details_id
    public int updateTownhouse_Lot_Valuation_Details2(Townhouse_API_Lot_Valuation_Details li,
                                                      String record_id, String lot_valuation_details_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_value_land_value", li.getreport_value_land_value());
        // updating row
        return db.update(tbl_townhouse_lot_valuation_details, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, lot_valuation_details_id});
    }

    //getting the total land value
    public String getTownhouse_API_Total_Land_Value_Lot_Valuation_Details(String record_id) {
        String result = "0";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT coalesce(SUM(report_value_land_value),0) FROM " + tbl_townhouse_lot_valuation_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"", null);
        if (cursor.moveToFirst()) {
            result = cursor.getString(0);
        }


        if(cursor != null) {
            cursor.close();
        }

        return result;
    }


    // Updating single townhouse Lot Valuation Details with lot_valuation_details_id
    public int updateTownhouse_Lot_Valuation_Details_3(Townhouse_API_Lot_Valuation_Details li,
                                                       String record_id, String lot_valuation_details_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_value_tct_no", li.getreport_value_tct_no());
        values.put("report_value_lot_no", li.getreport_value_lot_no());
        values.put("report_value_block_no", li.getreport_value_block_no());
        values.put("report_value_lot_area", li.getreport_value_lot_area());
        // updating row
        return db.update(tbl_townhouse_lot_valuation_details, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, lot_valuation_details_id});
    }

    // Deleting All Land Improvements Lot Valuation Details
    public int deleteTownhouse_API_Lot_Valuation_Details(Townhouse_API_Lot_Valuation_Details li) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_townhouse_lot_valuation_details, Report_record_id + " = ?",
                new String[]{String.valueOf(li.getrecord_id())});
    }

    // Deleting single Lot Valuation Details via record_id and lot_valuation_details_id
    public int deleteTownhouse_API_Lot_Valuation_Details_Single(String record_id, String lot_details_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_townhouse_lot_valuation_details,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, lot_details_id});
    }

    /**
     * townhouse Desc of Imp / Imp Details CRUD
     */
    //adding desc of imp
    public void addTownhouse_Imp_Details(Townhouse_API_Imp_Details li) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", li.getrecord_id());
        values.put("report_impsummary1_no_of_floors", li.getreport_impsummary1_no_of_floors());
        values.put("report_impsummary1_building_desc", li.getreport_impsummary1_building_desc());
        values.put("report_impsummary1_erected_on_lot", li.getreport_impsummary1_erected_on_lot());
        values.put("report_impsummary1_fa", li.getreport_impsummary1_fa());
        values.put("report_impsummary1_fa_per_td", li.getreport_impsummary1_fa_per_td());
        values.put("report_impsummary1_actual_utilization", li.getreport_impsummary1_actual_utilization());
        values.put("report_impsummary1_usage_declaration", li.getreport_impsummary1_usage_declaration());
        values.put("report_impsummary1_owner", li.getreport_impsummary1_owner());
        values.put("report_impsummary1_socialized_housing", li.getreport_impsummary1_socialized_housing());
        values.put("report_desc_property_type", li.getreport_desc_property_type());
        values.put("report_desc_foundation", li.getreport_desc_foundation());
        values.put("report_desc_columns_posts", li.getreport_desc_columns_posts());
        values.put("report_desc_beams", li.getreport_desc_beams());
        values.put("report_desc_exterior_walls", li.getreport_desc_exterior_walls());
        values.put("report_desc_interior_walls", li.getreport_desc_interior_walls());
        values.put("report_desc_imp_flooring", li.getreport_desc_imp_flooring());
        values.put("report_desc_doors", li.getreport_desc_doors());
        values.put("report_desc_imp_windows", li.getreport_desc_imp_windows());
        values.put("report_desc_ceiling", li.getreport_desc_ceiling());
        values.put("report_desc_imp_roofing", li.getreport_desc_imp_roofing());
        values.put("report_desc_trusses", li.getreport_desc_trusses());
        values.put("report_desc_economic_life", li.getreport_desc_economic_life());
        values.put("report_desc_effective_age", li.getreport_desc_effective_age());
        values.put("report_desc_imp_remain_life", li.getreport_desc_imp_remain_life());
        values.put("report_desc_occupants", li.getreport_desc_occupants());
        values.put("report_desc_owned_or_leased", li.getreport_desc_owned_or_leased());
        values.put("report_desc_imp_floor_area", li.getreport_desc_imp_floor_area());
        values.put("report_desc_confirmed_thru", li.getreport_desc_confirmed_thru());
        values.put("report_desc_observed_condition", li.getreport_desc_observed_condition());
        values.put("report_ownership_of_property", li.getreport_ownership_of_property());
        values.put("report_impsummary_no_of_bedroom", li.getreport_impsummary_no_of_bedroom());
        values.put("valrep_townhouse_impsummary_no_of_tb", li.getvalrep_townhouse_impsummary_no_of_tb());
        // Inserting Row
        db.insert(tbl_townhouse_imp_details, null, values);
        db.close(); // Closing database connection
    }

    // Getting selected townhouserovements Desc of Imp via record id only
    public List<Townhouse_API_Imp_Details> getTownhouse_Imp_Details(String record_id) {
        List<Townhouse_API_Imp_Details> townhouseList = new ArrayList<Townhouse_API_Imp_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_townhouse_imp_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Townhouse_API_Imp_Details li = new Townhouse_API_Imp_Details();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_impsummary1_no_of_floors(cursor.getString(2));
                li.setreport_impsummary1_building_desc(cursor.getString(3));
                li.setreport_impsummary1_erected_on_lot(cursor.getString(4));
                li.setreport_impsummary1_fa(cursor.getString(5));
                li.setreport_impsummary1_fa_per_td(cursor.getString(6));
                li.setreport_impsummary1_actual_utilization(cursor.getString(7));
                li.setreport_impsummary1_usage_declaration(cursor.getString(8));
                li.setreport_impsummary1_owner(cursor.getString(9));
                li.setreport_impsummary1_socialized_housing(cursor.getString(10));
                li.setreport_desc_property_type(cursor.getString(11));
                li.setreport_desc_foundation(cursor.getString(12));
                li.setreport_desc_columns_posts(cursor.getString(13));
                li.setreport_desc_beams(cursor.getString(14));
                li.setreport_desc_exterior_walls(cursor.getString(15));
                li.setreport_desc_interior_walls(cursor.getString(16));
                li.setreport_desc_imp_flooring(cursor.getString(17));
                li.setreport_desc_doors(cursor.getString(18));
                li.setreport_desc_imp_windows(cursor.getString(19));
                li.setreport_desc_ceiling(cursor.getString(20));
                li.setreport_desc_imp_roofing(cursor.getString(21));
                li.setreport_desc_trusses(cursor.getString(22));
                li.setreport_desc_economic_life(cursor.getString(23));
                li.setreport_desc_effective_age(cursor.getString(24));
                li.setreport_desc_imp_remain_life(cursor.getString(25));
                li.setreport_desc_occupants(cursor.getString(26));
                li.setreport_desc_owned_or_leased(cursor.getString(27));
                li.setreport_desc_imp_floor_area(cursor.getString(28));
                li.setreport_desc_confirmed_thru(cursor.getString(29));
                li.setreport_desc_observed_condition(cursor.getString(30));
                li.setreport_ownership_of_property(cursor.getString(31));
                li.setreport_impsummary_no_of_bedroom(cursor.getString(32));
                li.setvalrep_townhouse_impsummary_no_of_tb(cursor.getString(33));
                // Adding townhouse
                townhouseList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return townhouseList
        return townhouseList;
    }

    // Getting selected townhouserovements Desc of Imp with imp_details_id
    public List<Townhouse_API_Imp_Details> getTownhouse_Imp_Details_with_bldg_desc(String record_id, String imp_details_id) {
        List<Townhouse_API_Imp_Details> townhouseList = new ArrayList<Townhouse_API_Imp_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_townhouse_imp_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND imp_details_id = \"" + imp_details_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Townhouse_API_Imp_Details li = new Townhouse_API_Imp_Details();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_impsummary1_no_of_floors(cursor.getString(2));
                li.setreport_impsummary1_building_desc(cursor.getString(3));
                li.setreport_impsummary1_erected_on_lot(cursor.getString(4));
                li.setreport_impsummary1_fa(cursor.getString(5));
                li.setreport_impsummary1_fa_per_td(cursor.getString(6));
                li.setreport_impsummary1_actual_utilization(cursor.getString(7));
                li.setreport_impsummary1_usage_declaration(cursor.getString(8));
                li.setreport_impsummary1_owner(cursor.getString(9));
                li.setreport_impsummary1_socialized_housing(cursor.getString(10));
                li.setreport_desc_property_type(cursor.getString(11));
                li.setreport_desc_foundation(cursor.getString(12));
                li.setreport_desc_columns_posts(cursor.getString(13));
                li.setreport_desc_beams(cursor.getString(14));
                li.setreport_desc_exterior_walls(cursor.getString(15));
                li.setreport_desc_interior_walls(cursor.getString(16));
                li.setreport_desc_imp_flooring(cursor.getString(17));
                li.setreport_desc_doors(cursor.getString(18));
                li.setreport_desc_imp_windows(cursor.getString(19));
                li.setreport_desc_ceiling(cursor.getString(20));
                li.setreport_desc_imp_roofing(cursor.getString(21));
                li.setreport_desc_trusses(cursor.getString(22));
                li.setreport_desc_economic_life(cursor.getString(23));
                li.setreport_desc_effective_age(cursor.getString(24));
                li.setreport_desc_imp_remain_life(cursor.getString(25));
                li.setreport_desc_occupants(cursor.getString(26));
                li.setreport_desc_owned_or_leased(cursor.getString(27));
                li.setreport_desc_imp_floor_area(cursor.getString(28));
                li.setreport_desc_confirmed_thru(cursor.getString(29));
                li.setreport_desc_observed_condition(cursor.getString(30));
                li.setreport_ownership_of_property(cursor.getString(31));
                li.setreport_impsummary_no_of_bedroom(cursor.getString(32));
                li.setvalrep_townhouse_impsummary_no_of_tb(cursor.getString(33));
                // Adding townhouse
                townhouseList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return townhouseList
        return townhouseList;
    }

    // Updating single townhouse desc of imp with imp_details_id
    public int updateTownhouse_Imp_Details(Townhouse_API_Imp_Details li,
                                           String record_id, String imp_details_id_str) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_impsummary1_no_of_floors", li.getreport_impsummary1_no_of_floors());
        values.put("report_impsummary1_building_desc", li.getreport_impsummary1_building_desc());
        values.put("report_impsummary1_erected_on_lot", li.getreport_impsummary1_erected_on_lot());
        values.put("report_impsummary1_fa", li.getreport_impsummary1_fa());
        values.put("report_impsummary1_fa_per_td", li.getreport_impsummary1_fa_per_td());
        values.put("report_impsummary1_actual_utilization", li.getreport_impsummary1_actual_utilization());
        values.put("report_impsummary1_usage_declaration", li.getreport_impsummary1_usage_declaration());
        values.put("report_impsummary1_owner", li.getreport_impsummary1_owner());
        values.put("report_impsummary1_socialized_housing", li.getreport_impsummary1_socialized_housing());
        values.put("report_desc_property_type", li.getreport_desc_property_type());
        values.put("report_desc_foundation", li.getreport_desc_foundation());
        values.put("report_desc_columns_posts", li.getreport_desc_columns_posts());
        values.put("report_desc_beams", li.getreport_desc_beams());
        values.put("report_desc_exterior_walls", li.getreport_desc_exterior_walls());
        values.put("report_desc_interior_walls", li.getreport_desc_interior_walls());
        values.put("report_desc_imp_flooring", li.getreport_desc_imp_flooring());
        values.put("report_desc_doors", li.getreport_desc_doors());
        values.put("report_desc_imp_windows", li.getreport_desc_imp_windows());
        values.put("report_desc_ceiling", li.getreport_desc_ceiling());
        values.put("report_desc_imp_roofing", li.getreport_desc_imp_roofing());
        values.put("report_desc_trusses", li.getreport_desc_trusses());
        values.put("report_desc_economic_life", li.getreport_desc_economic_life());
        values.put("report_desc_effective_age", li.getreport_desc_effective_age());
        values.put("report_desc_imp_remain_life", li.getreport_desc_imp_remain_life());
        values.put("report_desc_occupants", li.getreport_desc_occupants());
        values.put("report_desc_owned_or_leased", li.getreport_desc_owned_or_leased());
        values.put("report_desc_imp_floor_area", li.getreport_desc_imp_floor_area());
        values.put("report_desc_confirmed_thru", li.getreport_desc_confirmed_thru());
        values.put("report_desc_observed_condition", li.getreport_desc_observed_condition());
        values.put("report_ownership_of_property", li.getreport_ownership_of_property());
        values.put("report_impsummary_no_of_bedroom", li.getreport_impsummary_no_of_bedroom());
        values.put("valrep_townhouse_impsummary_no_of_tb", li.getvalrep_townhouse_impsummary_no_of_tb());
        // updating row
        return db.update(tbl_townhouse_imp_details, values,
                Report_record_id + " = ? AND " + imp_details_id + " = ?",
                new String[]{record_id, imp_details_id_str});
    }

    // Updating single townhouse desc of imp total only
    public int updateTownhouse_Imp_Details_Total(Townhouse_API_Imp_Details li,
                                                 String record_id, String imp_details_id_str) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("report_desc_imp_floor_area", li.getreport_desc_imp_floor_area());
        // updating row
        return db.update(tbl_townhouse_imp_details, values,
                Report_record_id + " = ? AND " + imp_details_id + " = ?",
                new String[]{record_id, imp_details_id_str});
    }

    // Deleting All townhouse Desc of Imp
    public int deleteTownhouse_API_Imp_Details(Townhouse_API_Imp_Details li) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_townhouse_imp_details, Report_record_id + " = ?",
                new String[]{String.valueOf(li.getrecord_id())});
    }

    // Deleting single desc of imp via record_id and imp_details_id
    public int deleteTownhouse_API_Imp_Details_Single(String record_id, String imp_details_id_str) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_townhouse_imp_details,
                Report_record_id + " = ? AND " + imp_details_id + " = ?",
                new String[]{record_id, imp_details_id_str});
    }

    /**
     * townhouse Desc of Imp Features CRUD
     */
    //adding desc of imp features
    public void addTownhouse_Imp_Details_Features(Townhouse_API_Imp_Details_Features li) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", li.getrecord_id());
        values.put("imp_details_id", li.getimp_details_id());
        values.put("report_desc_features", li.getreport_desc_features());
        values.put("report_desc_features_area", li.getreport_desc_features_area());
        values.put("report_desc_features_area_desc", li.getreport_desc_features_area_desc());
        // Inserting Row
        db.insert(tbl_townhouse_imp_details_features, null, values);
        db.close(); // Closing database connection
    }

    // Getting All townhouserovements Desc of Imp Features via record id only for json
    public List<Townhouse_API_Imp_Details_Features> getAllTownhouse_Imp_Details_Features(
            String record_id) {
        List<Townhouse_API_Imp_Details_Features> townhouseList = new ArrayList<Townhouse_API_Imp_Details_Features>();
        // Select All Query
        String selectQuery = "Select * FROM "
                + tbl_townhouse_imp_details_features + " WHERE "
                + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Townhouse_API_Imp_Details_Features li = new Townhouse_API_Imp_Details_Features();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setimp_details_id(cursor.getString(2));
                li.setreport_desc_features(cursor.getString(3));
                li.setreport_desc_features_area(cursor.getString(4));
                li.setreport_desc_features_area_desc(cursor.getString(5));

                // Adding Land Improvements
                townhouseList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return townhouseList
        return townhouseList;
    }

    // Getting selected townhouserovements Desc of Imp Features via record id and imp_details_id
    public List<Townhouse_API_Imp_Details_Features> getTownhouse_Imp_Details_Features(
            String record_id, String imp_details_id_str) {
        List<Townhouse_API_Imp_Details_Features> townhouseList = new ArrayList<Townhouse_API_Imp_Details_Features>();
        // Select All Query
        String selectQuery = "Select * FROM "
                + tbl_townhouse_imp_details_features + " WHERE "
                + Report_record_id + " =  \"" + record_id + "\" AND "
                + imp_details_id + " =  \"" + imp_details_id_str + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Townhouse_API_Imp_Details_Features li = new Townhouse_API_Imp_Details_Features();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setimp_details_id(cursor.getString(2));
                li.setreport_desc_features(cursor.getString(3));
                li.setreport_desc_features_area(cursor.getString(4));
                li.setreport_desc_features_area_desc(cursor.getString(5));

                // Adding Land Improvements
                townhouseList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return townhouseList
        return townhouseList;
    }

    // Getting selected townhouserovements Desc of Imp Features via record id and imp_details_features_id
    public List<Townhouse_API_Imp_Details_Features> getTownhouse_Imp_Details_Features_Single(
            String record_id, String imp_details_features_id_str) {
        List<Townhouse_API_Imp_Details_Features> townhouseList = new ArrayList<Townhouse_API_Imp_Details_Features>();
        // Select All Query
        String selectQuery = "Select * FROM "
                + tbl_townhouse_imp_details_features + " WHERE "
                + Report_record_id + " =  \"" + record_id + "\" AND "
                + imp_details_features_id + " =  \"" + imp_details_features_id_str + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Townhouse_API_Imp_Details_Features li = new Townhouse_API_Imp_Details_Features();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setimp_details_id(cursor.getString(2));
                li.setreport_desc_features(cursor.getString(3));
                li.setreport_desc_features_area(cursor.getString(4));
                li.setreport_desc_features_area_desc(cursor.getString(5));

                // Adding Land Improvements
                townhouseList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return townhouseList
        return townhouseList;
    }

    // Updating single townhouse lot details features with imp_details_id
    public int updateTownhouse_Imp_Details_Features(Townhouse_API_Imp_Details_Features li,
                                                    String record_id, String imp_details_str) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(report_desc_features, li.getreport_desc_features());
        values.put(report_desc_features_area, li.getreport_desc_features_area());
        values.put(report_desc_features_area_desc, li.getreport_desc_features_area_desc());

        // updating row
        return db.update(tbl_townhouse_imp_details_features, values,
                Report_record_id + " = ? AND " + imp_details_features_id + " = ?",
                new String[]{record_id, imp_details_str});
    }

    // Deleting single Desc of Imp via record_id and imp_details_id
    public int deleteTownhouse_API_Imp_Details_Features(String record_id, String imp_details_id_str) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_townhouse_imp_details_features,
                Report_record_id + " = ? AND " + imp_details_id + " = ?",
                new String[]{record_id, imp_details_id_str});
    }

    // Deleting single desc of imp via record_id and imp_details_features_id
    public int deleteTownhouse_API_Imp_Details_Features_Single(String record_id, String imp_details_features_id_str) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_townhouse_imp_details_features,
                Report_record_id + " = ? AND " + imp_details_features_id + " = ?",
                new String[]{record_id, imp_details_features_id_str});
    }

    /**
     * VACANT LOT CRUDs
     */
    public void addVacant_Lot_Lot_Details(Vacant_Lot_API_Lot_Details vl) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", vl.getrecord_id());
        values.put("report_propdesc_tct_no", vl.getreport_propdesc_tct_no());
        values.put("report_propdesc_lot", vl.getreport_propdesc_lot());
        values.put("report_propdesc_block", vl.getreport_propdesc_block());
        values.put("report_propdesc_survey_nos", vl.getreport_propdesc_survey_nos());
        values.put("report_propdesc_area", vl.getreport_propdesc_area());
        values.put("valrep_land_propdesc_registry_date", vl.getvalrep_land_propdesc_registry_date());
        values.put("report_propdesc_registered_owner", vl.getreport_propdesc_registered_owner());
        values.put("report_propdesc_deeds", vl.getreport_propdesc_deeds());
        // Inserting Row
        db.insert(tbl_vacant_lot_lot_details, null, values);
        db.close(); // Closing database connection
    }

    // Getting selected Vacant_Lot Prop Desc
    public List<Vacant_Lot_API_Lot_Details> getVacant_Lot_Lot_Details(String record_id) {
        List<Vacant_Lot_API_Lot_Details> land_improvementsList = new ArrayList<Vacant_Lot_API_Lot_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_vacant_lot_lot_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Vacant_Lot_API_Lot_Details vl = new Vacant_Lot_API_Lot_Details();
                vl.setID(Integer.parseInt(cursor.getString(0)));
                vl.setrecord_id(cursor.getString(1));
                vl.setreport_propdesc_tct_no(cursor.getString(2));
                vl.setreport_propdesc_lot(cursor.getString(3));
                vl.setreport_propdesc_block(cursor.getString(4));
                vl.setreport_propdesc_survey_nos(cursor.getString(5));
                vl.setreport_propdesc_area(cursor.getString(6));
                vl.setvalrep_land_propdesc_registry_date(cursor.getString(7));
                vl.setreport_propdesc_registered_owner(cursor.getString(8));
                vl.setreport_propdesc_deeds(cursor.getString(9));

                // Adding Land Improvements
                land_improvementsList.add(vl);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return land_improvementsList;
    }

    // Getting selected Vacant_Lot Prop Desc with lot_details_id
    public List<Vacant_Lot_API_Lot_Details> getVacant_Lot_Lot_Details_with_lot_details_id(String record_id, String lot_details_id) {
        List<Vacant_Lot_API_Lot_Details> land_improvementsList = new ArrayList<Vacant_Lot_API_Lot_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_vacant_lot_lot_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND " + KEY_ID + " = \"" + lot_details_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Vacant_Lot_API_Lot_Details vl = new Vacant_Lot_API_Lot_Details();
                vl.setID(Integer.parseInt(cursor.getString(0)));
                vl.setrecord_id(cursor.getString(1));
                vl.setreport_propdesc_tct_no(cursor.getString(2));
                vl.setreport_propdesc_lot(cursor.getString(3));
                vl.setreport_propdesc_block(cursor.getString(4));
                vl.setreport_propdesc_survey_nos(cursor.getString(5));
                vl.setreport_propdesc_area(cursor.getString(6));
                vl.setvalrep_land_propdesc_registry_date(cursor.getString(7));
                vl.setreport_propdesc_registered_owner(cursor.getString(8));
                vl.setreport_propdesc_deeds(cursor.getString(9));

                // Adding Land Improvements
                land_improvementsList.add(vl);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return land_improvementsList;
    }

    // Updating single Vacant_Lot prop desc with lot_details_id
    public int updateVacant_Lot_Lot_Details(Vacant_Lot_API_Lot_Details vl,
                                            String record_id, String lot_details_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(report_propdesc_tct_no, vl.getreport_propdesc_tct_no());
        values.put(report_propdesc_area, vl.getreport_propdesc_area());
        values.put(report_propdesc_lot, vl.getreport_propdesc_lot());
        values.put(report_propdesc_block, vl.getreport_propdesc_block());
        values.put(report_propdesc_survey_nos, vl.getreport_propdesc_survey_nos());
        values.put(valrep_land_propdesc_registry_date, vl.getvalrep_land_propdesc_registry_date());
        values.put(report_propdesc_registered_owner, vl.getreport_propdesc_registered_owner());
        values.put(report_propdesc_deeds, vl.getreport_propdesc_deeds());
        // updating row
        return db.update(tbl_vacant_lot_lot_details, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, lot_details_id});
    }

    // Deleting All Land Improvements Prop Desc
    public int deleteVacant_Lot_Lot_Details_Single(String record_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_vacant_lot_lot_details,
                Report_record_id + " = ?",
                new String[]{record_id});
    }

    // Deleting single prop desc via record_id and lot_details_id
    public int deleteVacant_Lot_Lot_Details_Single(String record_id, String lot_details_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_vacant_lot_lot_details,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, lot_details_id});
    }

    /**
     * Vacant Lot lot_details_valuation CRUD (Market Valuation)
     */
    public void addVacant_Lot_Lot_Valuation_Details(Vacant_Lot_API_Lot_Valuation_Details vl) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", vl.getrecord_id());
        values.put("report_value_tct_no", vl.getreport_value_tct_no());
        values.put("report_value_lot_no", vl.getreport_value_lot_no());
        values.put("report_value_block_no", vl.getreport_value_block_no());
        values.put("report_value_area", vl.getreport_value_area());
        values.put("report_value_deduction", vl.getreport_value_deduction());
        values.put("report_value_net_area", vl.getreport_value_net_area());
        values.put("report_value_unit_value", vl.getreport_value_unit_value());
        values.put("report_value_land_value", vl.getreport_value_land_value());

        // Inserting Row
        db.insert(tbl_vacant_lot_lot_valuation_details, null, values);
        db.close(); // Closing database connection
    }

    // Getting selected Vacant_Lot Lot Valuation Details
    public List<Vacant_Lot_API_Lot_Valuation_Details> getVacant_Lot_Lot_Valuation_Details(String record_id) {
        List<Vacant_Lot_API_Lot_Valuation_Details> land_improvementsList = new ArrayList<Vacant_Lot_API_Lot_Valuation_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_vacant_lot_lot_valuation_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Vacant_Lot_API_Lot_Valuation_Details vl = new Vacant_Lot_API_Lot_Valuation_Details();
                vl.setID(Integer.parseInt(cursor.getString(0)));
                vl.setrecord_id(cursor.getString(1));
                vl.setreport_value_tct_no(cursor.getString(2));
                vl.setreport_value_lot_no(cursor.getString(3));
                vl.setreport_value_block_no(cursor.getString(4));
                vl.setreport_value_area(cursor.getString(5));
                vl.setreport_value_deduction(cursor.getString(6));
                vl.setreport_value_net_area(cursor.getString(7));
                vl.setreport_value_unit_value(cursor.getString(8));
                vl.setreport_value_land_value(cursor.getString(9));

                // Adding Land Improvements
                land_improvementsList.add(vl);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return land_improvementsList;
    }

    //getting the total land value
    public String getVacant_Lot_API_Total_Land_Value_Lot_Valuation_Details(String record_id) {
        String result = "0";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT coalesce(SUM(report_value_land_value),0) FROM " + tbl_vacant_lot_lot_valuation_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"", null);
        if (cursor.moveToFirst()) {
            result = cursor.getString(0);
        }


        if(cursor != null) {
            cursor.close();
        }

        return result;
    }

    // Getting selected Land improvements Lot Valuation Details with lot_valuation_details_id
    public List<Vacant_Lot_API_Lot_Valuation_Details> getVacant_Lot_Lot_Valuation_Details_with_lot_valuation_details_id(String record_id, String lot_valuation_details_id) {
        List<Vacant_Lot_API_Lot_Valuation_Details> land_improvementsList = new ArrayList<Vacant_Lot_API_Lot_Valuation_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_vacant_lot_lot_valuation_details
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND " + KEY_ID + " = \"" + lot_valuation_details_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Vacant_Lot_API_Lot_Valuation_Details vl = new Vacant_Lot_API_Lot_Valuation_Details();
                vl.setID(Integer.parseInt(cursor.getString(0)));
                vl.setrecord_id(cursor.getString(1));
                vl.setrecord_id(cursor.getString(1));
                vl.setreport_value_tct_no(cursor.getString(2));
                vl.setreport_value_lot_no(cursor.getString(3));
                vl.setreport_value_block_no(cursor.getString(4));
                vl.setreport_value_area(cursor.getString(5));
                vl.setreport_value_deduction(cursor.getString(6));
                vl.setreport_value_net_area(cursor.getString(7));
                vl.setreport_value_unit_value(cursor.getString(8));
                vl.setreport_value_land_value(cursor.getString(9));

                // Adding Land Improvements
                land_improvementsList.add(vl);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return land_improvementsList;
    }

    // Updating single land improvements Lot Valuation Details with lot_valuation_details_id
    public int updateVacant_Lot_Lot_Valuation_Details(Vacant_Lot_API_Lot_Valuation_Details vl,
                                                      String record_id, String lot_valuation_details_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_value_tct_no", vl.getreport_value_tct_no());
        values.put("report_value_lot_no", vl.getreport_value_lot_no());
        values.put("report_value_block_no", vl.getreport_value_block_no());
        values.put("report_value_area", vl.getreport_value_area());
        values.put("report_value_deduction", vl.getreport_value_deduction());
        values.put("report_value_net_area", vl.getreport_value_net_area());
        values.put("report_value_unit_value", vl.getreport_value_unit_value());
        values.put("report_value_land_value", vl.getreport_value_land_value());
        // updating row
        return db.update(tbl_vacant_lot_lot_valuation_details, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, lot_valuation_details_id});
    }


    // Updating single land improvements Lot Valuation Details with lot_valuation_details_id
    public int updateVacant_Lot_Lot_Valuation_Details2(Vacant_Lot_API_Lot_Valuation_Details vl,
                                                       String record_id, String lot_valuation_details_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_value_tct_no", vl.getreport_value_tct_no());
        values.put("report_value_lot_no", vl.getreport_value_lot_no());
        values.put("report_value_block_no", vl.getreport_value_block_no());
        values.put("report_value_area", vl.getreport_value_area());
        values.put("report_value_area", vl.getreport_value_area());
        values.put("report_value_net_area", vl.getreport_value_net_area());
        values.put("report_value_land_value", vl.getreport_value_land_value());
        // updating row
        return db.update(tbl_vacant_lot_lot_valuation_details, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, lot_valuation_details_id});
    }

    // Updating single land improvements Lot Valuation Details with lot_valuation_details_id
    public int updateVacant_Lot_Lot_Valuation_Details3(Vacant_Lot_API_Lot_Valuation_Details vl,
                                                       String record_id, String lot_valuation_details_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();


        values.put("report_value_net_area", vl.getreport_value_net_area());
        values.put("report_value_land_value", vl.getreport_value_land_value());
        // updating row
        return db.update(tbl_vacant_lot_lot_valuation_details, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, lot_valuation_details_id});
    }

    // Deleting All Land Improvements Lot Valuation Details
    public int deleteVacant_Lot_Lot_Valuation_Details_Single(String record_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_vacant_lot_lot_valuation_details,
                Report_record_id + " = ?",
                new String[]{record_id});
    }

    // Deleting single Lot Valuation Details via record_id and lot_valuation_details_id
    public int deleteVacant_Lot_Lot_Valuation_Details_Single(String record_id, String lot_details_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_vacant_lot_lot_valuation_details,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, lot_details_id});
    }

    /**
     * vacant lot RPDS CRUD
     */
    public void addVacant_Lot_RDPS(Vacant_Lot_API_RDPS vl) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", vl.getrecord_id());
        values.put("report_prev_app_name", vl.getreport_prev_app_name());
        values.put("report_prev_app_location", vl.getreport_prev_app_location());
        values.put("report_prev_app_area", vl.getreport_prev_app_area());
        values.put("report_prev_app_value", vl.getreport_prev_app_value());
        values.put("report_prev_app_date", vl.getreport_prev_app_date());
        // Inserting Row
        db.insert(tbl_vacant_lot_prev_appraisal, null, values);
        db.close(); // Closing database connection
    }


    // Getting selected Land improvements rdps
    public List<Vacant_Lot_API_RDPS> getVacant_Lot_RDPS(String record_id) {
        List<Vacant_Lot_API_RDPS> vacant_lotList = new ArrayList<Vacant_Lot_API_RDPS>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_vacant_lot_prev_appraisal
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Vacant_Lot_API_RDPS vl = new Vacant_Lot_API_RDPS();
                vl.setID(Integer.parseInt(cursor.getString(0)));
                vl.setrecord_id(cursor.getString(1));
                vl.setreport_prev_app_name(cursor.getString(2));
                vl.setreport_prev_app_location(cursor.getString(3));
                vl.setreport_prev_app_area(cursor.getString(4));
                vl.setreport_prev_app_value(cursor.getString(5));
                vl.setreport_prev_app_date(cursor.getString(6));

                // Adding Land Improvements
                vacant_lotList.add(vl);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return vacant_lotList
        return vacant_lotList;
    }

    // Getting selected Land improvements rdps with id
    public List<Vacant_Lot_API_RDPS> getVacant_Lot_RDPS_Single(String record_id, String rdps_id) {
        List<Vacant_Lot_API_RDPS> vacant_lotList = new ArrayList<Vacant_Lot_API_RDPS>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_vacant_lot_prev_appraisal
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND " + KEY_ID + " = \"" + rdps_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Vacant_Lot_API_RDPS vl = new Vacant_Lot_API_RDPS();
                vl.setID(Integer.parseInt(cursor.getString(0)));
                vl.setrecord_id(cursor.getString(1));
                vl.setreport_prev_app_name(cursor.getString(2));
                vl.setreport_prev_app_location(cursor.getString(3));
                vl.setreport_prev_app_area(cursor.getString(4));
                vl.setreport_prev_app_value(cursor.getString(5));
                vl.setreport_prev_app_date(cursor.getString(6));

                // Adding Land Improvements
                vacant_lotList.add(vl);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return vacant_lotList
        return vacant_lotList;
    }

    // Deleting All rdps via record_id
    public int deleteVacant_Lot_RDPS_Single(String record_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_vacant_lot_prev_appraisal,
                Report_record_id + " = ?",
                new String[]{record_id});
    }

    // Deleting single rdps
    public int deleteVacant_Lot_RDPS_Single(String record_id, String rdps_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_vacant_lot_prev_appraisal,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, rdps_id});
    }

    // Updating single Vacant_Lot_API_RDPS with rdps_id
    public int updateVacant_Lot_RDPS(Vacant_Lot_API_RDPS li,
                                     String record_id, String rdps_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_prev_app_name", li.getreport_prev_app_name());
        values.put("report_prev_app_location", li.getreport_prev_app_location());
        values.put("report_prev_app_area", li.getreport_prev_app_area());
        values.put("report_prev_app_value", li.getreport_prev_app_value());
        values.put("report_prev_app_date", li.getreport_prev_app_date());
        // updating row
        return db.update(tbl_vacant_lot_prev_appraisal, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, rdps_id});
    }

    /**
     * vacant lot PREV Appraisal CRUD
     */
    public void addVacant_Lot_Prev_Appraisal(Vacant_Lot_API_Prev_Appraisal vl) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", vl.getrecord_id());
        values.put("report_main_prev_desc", vl.getreport_main_prev_desc());
        values.put("report_main_prev_area", vl.getreport_main_prev_area());
        values.put("report_main_prev_unit_value", vl.getreport_main_prev_unit_value());
        values.put("report_main_prev_appraised_value", vl.getreport_main_prev_appraised_value());
        values.put("valrep_land_prev_land_value", vl.getvalrep_land_prev_land_value());
        values.put("valrep_land_prev_imp_value",  vl.getvalrep_land_prev_imp_value());
        // Inserting Row
        db.insert(tbl_vacant_lot_main_prev_appraisal, null, values);
        db.close(); // Closing database connection
    }


    // Getting selected Vacant Lot PREV Appraisal
    public List<Vacant_Lot_API_Prev_Appraisal> getVacant_Lot_API_Prev_Appraisal(String record_id) {
        List<Vacant_Lot_API_Prev_Appraisal> vacant_lotList = new ArrayList<Vacant_Lot_API_Prev_Appraisal>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_vacant_lot_main_prev_appraisal
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Vacant_Lot_API_Prev_Appraisal vl = new Vacant_Lot_API_Prev_Appraisal();
                vl.setID(Integer.parseInt(cursor.getString(0)));
                vl.setrecord_id(cursor.getString(1));
                vl.setreport_main_prev_desc(cursor.getString(2));
                vl.setreport_main_prev_area(cursor.getString(3));
                vl.setreport_main_prev_unit_value(cursor.getString(4));
                vl.setreport_main_prev_appraised_value(cursor.getString(5));
                vl.setvalrep_land_prev_land_value(cursor.getString(6));
                vl.setvalrep_land_prev_imp_value(cursor.getString(7));

                // Adding Land Improvements
                vacant_lotList.add(vl);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return vacant_lotList
        return vacant_lotList;
    }

    // Updating single vacant comp1
	/*public int updateVacant_Lot_comp1(Vacant_Lot_API vl, String record_id) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();

		values.put(report_comp1_remarks, vl.getreport_comp1_remarks());
		// updating row
		return db.update(tbl_vacant_lot, values, Report_record_id
				+ " = ?", new String[] { record_id });
	}*/
    // Getting selected Vacant Lot PREV Appraisal total appraised
    public String getVacant_Lot_API_Total_Appraised(String record_id) {
        String result = "0";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT coalesce(SUM(report_main_prev_appraised_value),0) FROM " + tbl_vacant_lot_main_prev_appraisal
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"", null);
        if (cursor.moveToFirst()) {
            result = cursor.getString(0);
        }


        if(cursor != null) {
            cursor.close();
        }

        return result;
    }

    public void updateVacant_Lot_Total_Appraised_Value(String record_id, String value) {
        String UpdateQuery = "UPDATE vacant_lot SET report_prev_total_appraised_value = \"" + value + "\" WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(UpdateQuery);
        db.close();
    }

    // Getting selected Land improvements PREV Appraisal with id
    public List<Vacant_Lot_API_Prev_Appraisal> getVacant_Lot_API_Prev_Appraisal_Single(String record_id, String prev_appraisal_id) {
        List<Vacant_Lot_API_Prev_Appraisal> vacant_lotList = new ArrayList<Vacant_Lot_API_Prev_Appraisal>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_vacant_lot_main_prev_appraisal
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND " + KEY_ID + " = \"" + prev_appraisal_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Vacant_Lot_API_Prev_Appraisal vl = new Vacant_Lot_API_Prev_Appraisal();
                vl.setID(Integer.parseInt(cursor.getString(0)));
                vl.setrecord_id(cursor.getString(1));
                vl.setreport_main_prev_desc(cursor.getString(2));
                vl.setreport_main_prev_area(cursor.getString(3));
                vl.setreport_main_prev_unit_value(cursor.getString(4));
                vl.setreport_main_prev_appraised_value(cursor.getString(5));
                vl.setvalrep_land_prev_land_value(cursor.getString(6));
                vl.setvalrep_land_prev_imp_value(cursor.getString(7));

                // Adding Land Improvements
                vacant_lotList.add(vl);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return vacant_lotList
        return vacant_lotList;
    }


    // Deleting All PREV Appraisal via record_id
    public int deleteVacant_Lot_API_Prev_Appraisal_Single(String record_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_vacant_lot_main_prev_appraisal,
                Report_record_id + " = ?",
                new String[]{record_id});
    }

    // Deleting single PREV Appraisal
    public int deleteVacant_Lot_API_Prev_Appraisal_Single(String record_id, String prev_appraisal_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_vacant_lot_main_prev_appraisal,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, prev_appraisal_id});
    }


    // Updating single vacant lot main prev Appraisal with prev_appraisal_id
    public int updateVacant_Lot_Main_Prev_Appraisal(Vacant_Lot_API_Prev_Appraisal li,
                                                    String record_id, String prev_appraisal_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_main_prev_desc", li.getreport_main_prev_desc());
        values.put("report_main_prev_area", li.getreport_main_prev_area());
        values.put("report_main_prev_unit_value", li.getreport_main_prev_unit_value());
        values.put("report_main_prev_appraised_value", li.getreport_main_prev_appraised_value());
        values.put("valrep_land_prev_land_value", li.getvalrep_land_prev_land_value());
        values.put("valrep_land_prev_imp_value", li.getvalrep_land_prev_imp_value());
        // updating row
        return db.update(tbl_vacant_lot_main_prev_appraisal, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, prev_appraisal_id});
    }


    /**
     * Land improvements main Prev
     */

    public void updateLand_Improvements_Total_Appraised_Value(String record_id, String value) {
        String UpdateQuery = "UPDATE land_improvements SET report_prev_total_appraised_value = \"" + value + "\" WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(UpdateQuery);
        db.close();
    }


    // Getting selected Land Improvements PREV Appraisal total appraised
    public String getLand_Improvements_API_Total_Appraised(String record_id) {
        String result = "0";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT coalesce(SUM(report_main_prev_appraised_value),0) FROM " + tbl_land_improvements_main_prev_appraisal
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"", null);
        if (cursor.moveToFirst()) {
            result = cursor.getString(0);
        }


        if(cursor != null) {
            cursor.close();
        }

        return result;
    }

    public void addLand_Improvements_Prev_Appraisal(Land_Improvements_API_Prev_Appraisal li) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", li.getrecord_id());
        values.put("report_main_prev_desc", li.getreport_main_prev_desc());
        values.put("report_main_prev_area", li.getreport_main_prev_area());
        values.put("report_main_prev_unit_value", li.getreport_main_prev_unit_value());
        values.put("report_main_prev_appraised_value", li.getreport_main_prev_appraised_value());
        values.put("valrep_land_prev_land_value", li.getvalrep_land_prev_land_value());
        values.put("valrep_land_prev_imp_value", li.getvalrep_land_prev_imp_value());
        // Inserting Row
        db.insert(tbl_land_improvements_main_prev_appraisal, null, values);
        db.close(); // Closing database connection
    }

    // Getting selected Land improvements PREV Appraisal with id
    public List<Land_Improvements_API_Prev_Appraisal> getLand_Improvements_API_Prev_Appraisal_Single(String record_id, String prev_appraisal_id) {
        List<Land_Improvements_API_Prev_Appraisal> land_improvementList = new ArrayList<Land_Improvements_API_Prev_Appraisal>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_land_improvements_main_prev_appraisal
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND " + KEY_ID + " = \"" + prev_appraisal_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Land_Improvements_API_Prev_Appraisal li = new Land_Improvements_API_Prev_Appraisal();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_main_prev_desc(cursor.getString(2));
                li.setreport_main_prev_area(cursor.getString(3));
                li.setreport_main_prev_unit_value(cursor.getString(4));
                li.setreport_main_prev_appraised_value(cursor.getString(5));
                li.setvalrep_land_prev_land_value(cursor.getString(6));
                li.setvalrep_land_prev_imp_value(cursor.getString(7));

                // Adding Land Improvements
                land_improvementList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return vacant_lotList
        return land_improvementList;
    }

    // Deleting single Land Improvements PREV Appraisal
    public int deleteLand_Improvements_API_Prev_Appraisal_Single(String record_id, String prev_appraisal_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_land_improvements_main_prev_appraisal,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, prev_appraisal_id});
    }

    // Getting selected Vacant Lot PREV Appraisal
    public List<Land_Improvements_API_Prev_Appraisal> getLand_Improvements_API_Prev_Appraisal(String record_id) {
        List<Land_Improvements_API_Prev_Appraisal> land_improvementsList = new ArrayList<Land_Improvements_API_Prev_Appraisal>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_land_improvements_main_prev_appraisal
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Land_Improvements_API_Prev_Appraisal li = new Land_Improvements_API_Prev_Appraisal();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_main_prev_desc(cursor.getString(2));
                li.setreport_main_prev_area(cursor.getString(3));
                li.setreport_main_prev_unit_value(cursor.getString(4));
                li.setreport_main_prev_appraised_value(cursor.getString(5));
                li.setvalrep_land_prev_land_value(cursor.getString(6));
                li.setvalrep_land_prev_imp_value(cursor.getString(7));

                // Adding Land Improvements
                land_improvementsList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return land_improvementsList;
    }

    // Updating single land improvements main prev Appraisal with prev_appraisal_id
    public int updateLand_Improvements_Main_Prev_Appraisal(Land_Improvements_API_Prev_Appraisal li,
                                                           String record_id, String prev_appraisal_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_main_prev_desc", li.getreport_main_prev_desc());
        values.put("report_main_prev_area", li.getreport_main_prev_area());
        values.put("report_main_prev_unit_value", li.getreport_main_prev_unit_value());
        values.put("report_main_prev_appraised_value", li.getreport_main_prev_appraised_value());
        values.put("valrep_land_prev_land_value", li.getvalrep_land_prev_land_value());
        values.put("valrep_land_prev_imp_value", li.getvalrep_land_prev_imp_value());
        // updating row
        return db.update(tbl_land_improvements_main_prev_appraisal, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, prev_appraisal_id});
    }


    /**
     * Condo main Prev CRUD
     */

    public void updateCondo_Total_Appraised_Value(String record_id, String value) {
        String UpdateQuery = "UPDATE condo SET report_prev_total_appraised_value = \"" + value + "\" WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(UpdateQuery);
        db.close();
    }


    // Getting selected Condo PREV Appraisal total appraised
    public String getCondo_API_Total_Appraised(String record_id) {
        String result = "0";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT coalesce(SUM(report_main_prev_appraised_value),0) FROM " + tbl_condo_main_prev_appraisal
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"", null);
        if (cursor.moveToFirst()) {
            result = cursor.getString(0);
        }


        if(cursor != null) {
            cursor.close();
        }

        return result;
    }


    public void addCondo_Prev_Appraisal(Condo_API_Prev_Appraisal li) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", li.getrecord_id());
        values.put("report_main_prev_desc", li.getreport_main_prev_desc());
        values.put("report_main_prev_area", li.getreport_main_prev_area());
        values.put("report_main_prev_unit_value", li.getreport_main_prev_unit_value());
        values.put("report_main_prev_appraised_value", li.getreport_main_prev_appraised_value());
        // Inserting Row
        db.insert(tbl_condo_main_prev_appraisal, null, values);
        db.close(); // Closing database connection
    }

    // Getting selected Condo PREV Appraisal with id
    public List<Condo_API_Prev_Appraisal> getCondo_API_Prev_Appraisal_Single(String record_id, String prev_appraisal_id) {
        List<Condo_API_Prev_Appraisal> condoList = new ArrayList<Condo_API_Prev_Appraisal>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_condo_main_prev_appraisal
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND " + KEY_ID + " = \"" + prev_appraisal_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Condo_API_Prev_Appraisal li = new Condo_API_Prev_Appraisal();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_main_prev_desc(cursor.getString(2));
                li.setreport_main_prev_area(cursor.getString(3));
                li.setreport_main_prev_unit_value(cursor.getString(4));
                li.setreport_main_prev_appraised_value(cursor.getString(5));

                // Adding condo
                condoList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return condoList
        return condoList;
    }

    // Deleting single Condo PREV Appraisal
    public int deleteCondo_API_Prev_Appraisal_Single(String record_id, String prev_appraisal_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_condo_main_prev_appraisal,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, prev_appraisal_id});
    }


    // Getting selected Vacant Lot PREV Appraisal
    public List<Condo_API_Prev_Appraisal> getCondo_API_Prev_Appraisal(String record_id) {
        List<Condo_API_Prev_Appraisal> condoList = new ArrayList<Condo_API_Prev_Appraisal>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_condo_main_prev_appraisal
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Condo_API_Prev_Appraisal li = new Condo_API_Prev_Appraisal();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_main_prev_desc(cursor.getString(2));
                li.setreport_main_prev_area(cursor.getString(3));
                li.setreport_main_prev_unit_value(cursor.getString(4));
                li.setreport_main_prev_appraised_value(cursor.getString(5));

                // Adding condo
                condoList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return condoList
        return condoList;
    }

    // Updating single condo main prev Appraisal with prev_appraisal_id
    public int updateCondo_Main_Prev_Appraisal(Condo_API_Prev_Appraisal li,
                                               String record_id, String prev_appraisal_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_main_prev_desc", li.getreport_main_prev_desc());
        values.put("report_main_prev_area", li.getreport_main_prev_area());
        values.put("report_main_prev_unit_value", li.getreport_main_prev_unit_value());
        values.put("report_main_prev_appraised_value", li.getreport_main_prev_appraised_value());
        // updating row
        return db.update(tbl_condo_main_prev_appraisal, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, prev_appraisal_id});
    }

    /**
     * Townhouse PREV Appraisal CRUD
     */
    public void addTownhouse_Prev_Appraisal(Townhouse_API_Prev_Appraisal li) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", li.getrecord_id());
        values.put("report_main_prev_desc", li.getreport_main_prev_desc());
        values.put("report_main_prev_area", li.getreport_main_prev_area());
        values.put("report_main_prev_unit_value", li.getreport_main_prev_unit_value());
        values.put("report_main_prev_appraised_value", li.getreport_main_prev_appraised_value());
        // Inserting Row
        db.insert(tbl_townhouse_main_prev_appraisal, null, values);
        db.close(); // Closing database connection
    }

    // Getting selected Townhouse PREV Appraisal with id
    public List<Townhouse_API_Prev_Appraisal> getTownhouse_API_Prev_Appraisal_Single(String record_id, String prev_appraisal_id) {
        List<Townhouse_API_Prev_Appraisal> townhouseList = new ArrayList<Townhouse_API_Prev_Appraisal>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_townhouse_main_prev_appraisal
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND " + KEY_ID + " = \"" + prev_appraisal_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Townhouse_API_Prev_Appraisal li = new Townhouse_API_Prev_Appraisal();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_main_prev_desc(cursor.getString(2));
                li.setreport_main_prev_area(cursor.getString(3));
                li.setreport_main_prev_unit_value(cursor.getString(4));
                li.setreport_main_prev_appraised_value(cursor.getString(5));

                // Adding townhouse
                townhouseList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return townhouseList
        return townhouseList;
    }

    // Deleting single townhouse PREV Appraisal
    public int deleteTownhouse_API_Prev_Appraisal_Single(String record_id, String prev_appraisal_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_townhouse_main_prev_appraisal,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, prev_appraisal_id});
    }

    public void updateTownhouse_Total_Appraised_Value(String record_id, String value) {
        String UpdateQuery = "UPDATE townhouse SET report_prev_total_appraised_value = \"" + value + "\" WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(UpdateQuery);
        db.close();
    }


    // Getting selected Townhouse PREV Appraisal total appraised
    public String getTownhouse_API_Total_Appraised(String record_id) {
        String result = "0";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT coalesce(SUM(report_main_prev_appraised_value),0) FROM " + tbl_townhouse_main_prev_appraisal
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"", null);
        if (cursor.moveToFirst()) {
            result = cursor.getString(0);
        }


        if(cursor != null) {
            cursor.close();
        }

        return result;
    }

    // Getting selected Townhouse PREV Appraisal
    public List<Townhouse_API_Prev_Appraisal> getTownhouse_API_Prev_Appraisal(String record_id) {
        List<Townhouse_API_Prev_Appraisal> townhouseList = new ArrayList<Townhouse_API_Prev_Appraisal>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_townhouse_main_prev_appraisal
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Townhouse_API_Prev_Appraisal li = new Townhouse_API_Prev_Appraisal();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_main_prev_desc(cursor.getString(2));
                li.setreport_main_prev_area(cursor.getString(3));
                li.setreport_main_prev_unit_value(cursor.getString(4));
                li.setreport_main_prev_appraised_value(cursor.getString(5));

                // Adding townhouse
                townhouseList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return townhouseList
        return townhouseList;
    }

    // Updating single Townhouse main prev Appraisal with prev_appraisal_id
    public int updateTownhouse_Main_Prev_Appraisal(Townhouse_API_Prev_Appraisal li,
                                                   String record_id, String prev_appraisal_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_main_prev_desc", li.getreport_main_prev_desc());
        values.put("report_main_prev_area", li.getreport_main_prev_area());
        values.put("report_main_prev_unit_value", li.getreport_main_prev_unit_value());
        values.put("report_main_prev_appraised_value", li.getreport_main_prev_appraised_value());
        // updating row
        return db.update(tbl_townhouse_main_prev_appraisal, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, prev_appraisal_id});
    }


/**
 * CONDO CRUD
 */
    /**
     * CONDO/TOWNHOUSE RDPS CRUD
     */
    public void addCondo_RDPS(Condo_API_RDPS c) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", c.getrecord_id());
        values.put("report_prev_app_account", c.getreport_prev_app_name());
        values.put("report_prev_app_location", c.getreport_prev_app_location());
        values.put("report_prev_app_area", c.getreport_prev_app_area());
        values.put("report_prev_app_value", c.getreport_prev_app_value());
        values.put("report_prev_app_date", c.getreport_prev_app_date());
        // Inserting Row
        db.insert(tbl_condo_prev_appraisal, null, values);
        db.close(); // Closing database connection
    }

    /*public void addTownhouse_RDPS(Condo_API_RDPS c) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put("record_id", c.getrecord_id());
		values.put("report_prev_app_name", c.getreport_prev_app_name());
		values.put("report_prev_app_location", c.getreport_prev_app_location());
		values.put("report_prev_app_area", c.getreport_prev_app_area());
		values.put("report_prev_app_value", c.getreport_prev_app_value());
		values.put("report_prev_app_date", c.getreport_prev_app_date());
		// Inserting Row
		db.insert(tbl_condo_prev_appraisal, null, values);
		db.close(); // Closing database connection
	}*/
    // Getting selected Land improvements rdps
    public List<Condo_API_RDPS> getCondo_RDPS(String record_id, String tbl_name) {
        List<Condo_API_RDPS> condoList = new ArrayList<Condo_API_RDPS>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_name
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Condo_API_RDPS c = new Condo_API_RDPS();
                c.setID(Integer.parseInt(cursor.getString(0)));
                c.setrecord_id(cursor.getString(1));
                c.setreport_prev_app_name(cursor.getString(2));
                c.setreport_prev_app_location(cursor.getString(3));
                c.setreport_prev_app_area(cursor.getString(4));
                c.setreport_prev_app_value(cursor.getString(5));
                c.setreport_prev_app_date(cursor.getString(6));

                // Adding Land Improvements
                condoList.add(c);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return vacant_lotList
        return condoList;
    }

    // Getting selected condo rdps with id
    public List<Condo_API_RDPS> getCondo_RDPS_Single(String record_id, String rdps_id, String tbl_name) {
        List<Condo_API_RDPS> condoList = new ArrayList<Condo_API_RDPS>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_name
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND " + KEY_ID + " = \"" + rdps_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Condo_API_RDPS c = new Condo_API_RDPS();
                c.setID(Integer.parseInt(cursor.getString(0)));
                c.setrecord_id(cursor.getString(1));
                c.setreport_prev_app_name(cursor.getString(2));
                c.setreport_prev_app_location(cursor.getString(3));
                c.setreport_prev_app_area(cursor.getString(4));
                c.setreport_prev_app_value(cursor.getString(5));
                c.setreport_prev_app_date(cursor.getString(6));

                // Adding condoList
                condoList.add(c);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return vacant_lotList
        return condoList;
    }

    // Deleting All rdps via record_id
    public int deleteCondo_RDPS_Single(String record_id, String tbl_name) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_name,
                Report_record_id + " = ?",
                new String[]{record_id});
    }

    // Deleting single rdps
    public int deleteCondo_RDPS_Single(String record_id, String rdps_id, String tbl_name) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_name,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, rdps_id});
    }


    // Updating single Codno_API_RDPS with rdps_id
    public int updateCondo_RDPS(Condo_API_RDPS li,
                                String record_id, String rdps_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_prev_app_account", li.getreport_prev_app_name());
        values.put("report_prev_app_location", li.getreport_prev_app_location());
        values.put("report_prev_app_area", li.getreport_prev_app_area());
        values.put("report_prev_app_value", li.getreport_prev_app_value());
        values.put("report_prev_app_date", li.getreport_prev_app_date());
        // updating row
        return db.update(tbl_condo_prev_appraisal, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, rdps_id});
    }

    /**
     * condo title details crud
     */
    public void addCondo_Title_Details(Condo_API_Title_Details vl) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", vl.getrecord_id());
        values.put("report_propdesc_property_type", vl.getreport_propdesc_property_type());
        values.put("report_propdesc_cct_no", vl.getreport_propdesc_cct_no());
        values.put("report_prodesc_unit_no", vl.getreport_propdesc_unit_no());
        values.put("report_prodesc_floor", vl.getreport_propdesc_floor());
        values.put("report_prodesc_bldg_name", vl.getreport_propdesc_bldg_name());
        values.put("report_propdesc_area", vl.getreport_propdesc_area());
        values.put("valrep_condo_propdesc_registry_date", vl.getvalrep_condo_propdesc_registry_date());
        values.put("report_prodesc_reg_of_deeds", vl.getreport_propdesc_reg_of_deeds());
        values.put("report_propdesc_registered_owner", vl.getreport_propdesc_registered_owner());
        // Inserting Row
        db.insert(tbl_condo_title_details, null, values);
        db.close(); // Closing database connection
    }
    //add townhouse title details
	/*public void addTownhouse_Title_Details(Condo_API_Title_Details vl) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put("record_id", vl.getrecord_id());
		values.put("report_propdesc_property_type", vl.getreport_propdesc_property_type());
		values.put("report_propdesc_cct_no", vl.getreport_propdesc_cct_no());
		values.put("report_propdesc_unit_no", vl.getreport_propdesc_unit_no());
		values.put("report_propdesc_floor", vl.getreport_propdesc_floor());
		values.put("report_propdesc_bldg_name", vl.getreport_propdesc_bldg_name());
		values.put("report_propdesc_area", vl.getreport_propdesc_area());
		values.put("report_propdesc_registry_date_month", vl.getreport_propdesc_registry_date_month());
		values.put("report_propdesc_registry_date_day", vl.getreport_propdesc_registry_date_day());
		values.put("report_propdesc_registry_date_year", vl.getreport_propdesc_registry_date_year());
		values.put("report_propdesc_reg_of_deeds", vl.getreport_propdesc_reg_of_deeds());
		values.put("report_propdesc_registered_owner", vl.getreport_propdesc_registered_owner());
		// Inserting Row
		db.insert(tbl_condo_title_details, null, values);
		db.close(); // Closing database connection
	}*/

    // Getting selected Condo Prop Desc All or Single (depends on passed variables)
    //all
    public List<Condo_API_Title_Details> getCondo_Title_Details(String record_id, String tbl_name) {
        List<Condo_API_Title_Details> condoList = new ArrayList<Condo_API_Title_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_name
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Condo_API_Title_Details c = new Condo_API_Title_Details();
                c.setID(Integer.parseInt(cursor.getString(0)));
                c.setrecord_id(cursor.getString(1));
                c.setreport_propdesc_property_type(cursor.getString(2));
                c.setreport_propdesc_cct_no(cursor.getString(3));
                c.setreport_propdesc_unit_no(cursor.getString(4));
                c.setreport_propdesc_floor(cursor.getString(5));
                c.setreport_propdesc_bldg_name(cursor.getString(6));
                c.setreport_propdesc_area(cursor.getString(7));
                c.setvalrep_condo_propdesc_registry_date(cursor.getString(8));
                c.setreport_propdesc_reg_of_deeds(cursor.getString(9));
                c.setreport_propdesc_registered_owner(cursor.getString(10));

                // Adding Land Improvements
                condoList.add(c);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return condoList;
    }

    //single
    public List<Condo_API_Title_Details> getCondo_Title_Details(String record_id, String title_details_id, String tbl_name) {
        List<Condo_API_Title_Details> condoList = new ArrayList<Condo_API_Title_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_name
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND " + KEY_ID + " = \"" + title_details_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Condo_API_Title_Details c = new Condo_API_Title_Details();
                c.setID(Integer.parseInt(cursor.getString(0)));
                c.setrecord_id(cursor.getString(1));
                c.setreport_propdesc_property_type(cursor.getString(2));
                c.setreport_propdesc_cct_no(cursor.getString(3));
                c.setreport_propdesc_unit_no(cursor.getString(4));
                c.setreport_propdesc_floor(cursor.getString(5));
                c.setreport_propdesc_bldg_name(cursor.getString(6));
                c.setreport_propdesc_area(cursor.getString(7));
                c.setvalrep_condo_propdesc_registry_date(cursor.getString(8));
                c.setreport_propdesc_reg_of_deeds(cursor.getString(9));
                c.setreport_propdesc_registered_owner(cursor.getString(10));

                // Adding Land Improvements
                condoList.add(c);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return condoList;
    }
    //Added by IAN
    public String[] getCondo_Title_Details_Unit_no(String record_id) {


        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cur = db.rawQuery("SELECT report_prodesc_unit_no FROM " + "condo_title_details"
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" and report_propdesc_property_type = \"Unit\"", null);
        cur.moveToFirst();
        ArrayList<String> ids = new ArrayList<String>();
        while(!cur.isAfterLast()) {
            ids.add(cur.getString(cur.getColumnIndex("report_prodesc_unit_no")));
            cur.moveToNext();
        }
        cur.close();
        return ids.toArray(new String[ids.size()]);

    }

    // Updating single Condo prop desc with title_details_id
    public int updateCondo_Title_Details(Condo_API_Title_Details vl,
                                         String record_id, String title_details_id, String tbl_name) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(report_propdesc_property_type, vl.getreport_propdesc_property_type());
        values.put(report_propdesc_cct_no, vl.getreport_propdesc_cct_no());

        values.put(report_prodesc_unit_no, vl.getreport_propdesc_unit_no());
        values.put(report_prodesc_floor, vl.getreport_propdesc_floor());
        values.put(report_prodesc_bldg_name, vl.getreport_propdesc_bldg_name());
        values.put(report_propdesc_area, vl.getreport_propdesc_area());
        values.put(valrep_condo_propdesc_registry_date, vl.getvalrep_condo_propdesc_registry_date());
        values.put(report_prodesc_reg_of_deeds, vl.getreport_propdesc_reg_of_deeds());
        values.put(report_propdesc_registered_owner, vl.getreport_propdesc_registered_owner());
        // updating row
        return db.update(tbl_name, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, title_details_id});
    }


    // Updating single townhouse Lot Valuation Details with lot_valuation_details_id
    public int updateCondo_Title_Details_Valuation(Condo_API_Valuation vl,
                                                   String record_id, String lot_valuation_details_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_value_property_type", vl.getreport_value_property_type());
        values.put("report_value_cct_no", vl.getreport_value_cct_no());
        values.put("report_value_unit_no", vl.getreport_value_unit_no());
        values.put("report_value_floor_area", vl.getreport_value_floor_area());
        values.put("report_value_net_area", vl.getreport_value_net_area());
        values.put("report_value_appraised_value", vl.getreport_value_appraised_value());
        // updating row
        return db.update("condo_unit_valuation", values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, lot_valuation_details_id});
    }

    // Updating single townhouse Lot Valuation Details with lot_valuation_details_id
    public int updateCondo_Title_Details_Valuation2(Condo_API_Valuation vl,
                                                    String record_id, String lot_valuation_details_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_value_net_area", vl.getreport_value_net_area());
        values.put("report_value_appraised_value", vl.getreport_value_appraised_value());
        // updating row
        return db.update("condo_unit_valuation", values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, lot_valuation_details_id});
    }

    // Deleting Condo prop desc All or Single
    //all
    public int deleteCondo_Title_Details(String record_id, String title_details_id, String tbl_name) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_name,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, title_details_id});
    }

    //single
    public int deleteCondo_Title_Details(String record_id, String tbl_name) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_name,
                Report_record_id + " = ?",
                new String[]{record_id});
    }
    /**
     * condo_unit_details crud 
     */
    /**
     * condo unit details crud
     */
    public void addCondo_Unit_Details(Condo_API_Unit_Details c) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", c.getrecord_id());
        values.put("report_unit_description", c.getreport_unit_description());
        values.put("report_unit_no_of_storeys", c.getreport_unit_no_of_storeys());
        values.put("report_unit_unit_no", c.getreport_unit_unit_no());
        values.put("report_unit_floor_location", c.getreport_unit_floor_location());
        values.put("report_unit_floor_area", c.getreport_unit_floor_area());
        values.put("report_unit_interior_flooring", c.getreport_unit_interior_flooring());
        values.put("report_unit_interior_partitions", c.getreport_unit_interior_partitions());
        values.put("report_unit_interior_doors", c.getreport_unit_interior_doors());
        values.put("report_unit_interior_windows", c.getreport_unit_interior_windows());
        values.put("report_unit_interior_ceiling", c.getreport_unit_interior_ceiling());
        values.put("report_unit_features", c.getreport_unit_features());
        values.put("report_unit_occupants", c.getreport_unit_occupants());
        values.put("report_unit_owned_or_leased", c.getreport_unit_owned_or_leased());
        values.put("report_unit_observed_condition", c.getreport_unit_observed_condition());
        values.put("report_unit_ownership_of_property", c.getreport_unit_ownership_of_property());
        values.put("report_unit_socialized_housing", c.getreport_unit_socialized_housing());
        values.put("report_unit_type_of_property", c.getreport_unit_type_of_property());
        values.put("report_no_of_bedrooms", c.getreport_no_of_bedrooms());
        //ADDED By IAN
        values.put("report_bldg_age", c.getreport_bldg_age());
        values.put("report_developer_name", c.getreport_developer_name());
        // Inserting Row
        db.insert(tbl_condo_unit_details, null, values);
        db.close(); // Closing database connection
    }

    // Getting selected Condo
    //all
    public List<Condo_API_Unit_Details> getCondo_Unit_Details(String record_id, String tbl_name) {
        List<Condo_API_Unit_Details> condoList = new ArrayList<Condo_API_Unit_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_name
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Condo_API_Unit_Details c = new Condo_API_Unit_Details();
                c.setID(Integer.parseInt(cursor.getString(0)));
                c.setrecord_id(cursor.getString(1));
                c.setreport_unit_description(cursor.getString(2));
                c.setreport_unit_no_of_storeys(cursor.getString(3));
                c.setreport_unit_unit_no(cursor.getString(4));
                c.setreport_unit_floor_location(cursor.getString(5));
                c.setreport_unit_floor_area(cursor.getString(6));
                c.setreport_unit_interior_flooring(cursor.getString(7));
                c.setreport_unit_interior_partitions(cursor.getString(8));
                c.setreport_unit_interior_doors(cursor.getString(9));
                c.setreport_unit_interior_windows(cursor.getString(10));
                c.setreport_unit_interior_ceiling(cursor.getString(11));
                c.setreport_unit_features(cursor.getString(12));
                c.setreport_unit_occupants(cursor.getString(13));
                c.setreport_unit_owned_or_leased(cursor.getString(14));
                c.setreport_unit_observed_condition(cursor.getString(15));
                c.setreport_unit_ownership_of_property(cursor.getString(16));
                c.setreport_unit_socialized_housing(cursor.getString(17));
                c.setreport_unit_type_of_property(cursor.getString(18));
                c.setreport_no_of_bedrooms(cursor.getString(19));

                //Added By IAN
                c.setreport_bldg_age(cursor.getString(20));
                c.setreport_developer_name(cursor.getString(21));
                // Adding Land Improvements
                condoList.add(c);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return condoList;
    }

    //single
    public List<Condo_API_Unit_Details> getCondo_Unit_Details(String record_id, String unit_details_id, String tbl_name) {
        List<Condo_API_Unit_Details> condoList = new ArrayList<Condo_API_Unit_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_name
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND id = \"" + unit_details_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Condo_API_Unit_Details c = new Condo_API_Unit_Details();
                c.setID(Integer.parseInt(cursor.getString(0)));
                c.setrecord_id(cursor.getString(1));
                c.setreport_unit_description(cursor.getString(2));
                c.setreport_unit_no_of_storeys(cursor.getString(3));
                c.setreport_unit_unit_no(cursor.getString(4));
                c.setreport_unit_floor_location(cursor.getString(5));
                c.setreport_unit_floor_area(cursor.getString(6));
                c.setreport_unit_interior_flooring(cursor.getString(7));
                c.setreport_unit_interior_partitions(cursor.getString(8));
                c.setreport_unit_interior_doors(cursor.getString(9));
                c.setreport_unit_interior_windows(cursor.getString(10));
                c.setreport_unit_interior_ceiling(cursor.getString(11));
                c.setreport_unit_features(cursor.getString(12));
                c.setreport_unit_occupants(cursor.getString(13));
                c.setreport_unit_owned_or_leased(cursor.getString(14));
                c.setreport_unit_observed_condition(cursor.getString(15));
                c.setreport_unit_ownership_of_property(cursor.getString(16));
                c.setreport_unit_socialized_housing(cursor.getString(17));
                c.setreport_unit_type_of_property(cursor.getString(18));
                c.setreport_no_of_bedrooms(cursor.getString(19));
                //ADDED By IAN
                c.setreport_bldg_age(cursor.getString(20));
                c.setreport_developer_name(cursor.getString(21));

                // Adding Land Improvements
                condoList.add(c);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return condoList;
    }

    // Updating single Condo unit details with unit_details_id
    public int updateCondo_Unit_Details(Condo_API_Unit_Details c,
                                        String record_id, String unit_details_id, String tbl_name) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("report_unit_description", c.getreport_unit_description());
        values.put("report_unit_no_of_storeys", c.getreport_unit_no_of_storeys());
        values.put("report_unit_unit_no", c.getreport_unit_unit_no());
        values.put("report_unit_floor_location", c.getreport_unit_floor_location());
        values.put("report_unit_floor_area", c.getreport_unit_floor_area());
        values.put("report_unit_interior_flooring", c.getreport_unit_interior_flooring());
        values.put("report_unit_interior_partitions", c.getreport_unit_interior_partitions());
        values.put("report_unit_interior_doors", c.getreport_unit_interior_doors());
        values.put("report_unit_interior_windows", c.getreport_unit_interior_windows());
        values.put("report_unit_interior_ceiling", c.getreport_unit_interior_ceiling());
        values.put("report_unit_features", c.getreport_unit_features());
        values.put("report_unit_occupants", c.getreport_unit_occupants());
        values.put("report_unit_owned_or_leased", c.getreport_unit_owned_or_leased());
        values.put("report_unit_observed_condition", c.getreport_unit_observed_condition());
        values.put("report_unit_ownership_of_property", c.getreport_unit_ownership_of_property());
        values.put("report_unit_socialized_housing", c.getreport_unit_socialized_housing());
        values.put("report_unit_type_of_property", c.getreport_unit_type_of_property());
        values.put("report_no_of_bedrooms", c.getreport_no_of_bedrooms());
        //Added By IAN
        values.put("report_bldg_age", c.getreport_bldg_age());
        values.put("report_developer_name", c.getreport_developer_name());
        // updating row
        return db.update(tbl_name, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, unit_details_id});
    }

    // Deleting Condo prop desc All or Single
    //all
    public int deleteCondo_Unit_Details(String record_id, String tbl_name) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_name,
                Report_record_id + " = ?",
                new String[]{record_id});
    }

    //single
    public int deleteCondo_Unit_Details(String record_id, String unit_details_id, String tbl_name) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_name,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, unit_details_id});
    }

    /**
     * Condo Valuation CRUD
     */
    public void addCondo_Valuation(Condo_API_Valuation vl) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", vl.getrecord_id());
        values.put("report_value_property_type", vl.getreport_value_property_type());
        values.put("report_value_cct_no", vl.getreport_value_cct_no());
        values.put("report_value_unit_no", vl.getreport_value_unit_no());
        values.put("report_value_floor_area", vl.getreport_value_floor_area());
        values.put("report_value_deduction", vl.getreport_value_deduction());
        values.put("report_value_net_area", vl.getreport_value_net_area());
        values.put("report_value_unit_value", vl.getreport_value_unit_value());
        values.put("report_value_appraised_value", vl.getreport_value_appraised_value());

        // Inserting Row
        db.insert(tbl_condo_unit_valuation, null, values);
        db.close(); // Closing database connection
    }

    // Getting all condo unit valuation
    public List<Condo_API_Valuation> getCondo_Valuation(String record_id, String tbl_name) {
        List<Condo_API_Valuation> land_improvementsList = new ArrayList<Condo_API_Valuation>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_name
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Condo_API_Valuation vl = new Condo_API_Valuation();
                vl.setID(Integer.parseInt(cursor.getString(0)));
                vl.setrecord_id(cursor.getString(1));
                vl.setreport_value_property_type(cursor.getString(2));
                vl.setreport_value_cct_no(cursor.getString(3));
                vl.setreport_value_unit_no(cursor.getString(4));
                vl.setreport_value_floor_area(cursor.getString(5));
                vl.setreport_value_deduction(cursor.getString(6));
                vl.setreport_value_net_area(cursor.getString(7));
                vl.setreport_value_unit_value(cursor.getString(8));
                vl.setreport_value_appraised_value(cursor.getString(9));
                // Adding Land Improvements
                land_improvementsList.add(vl);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return land_improvementsList;
    }

    // Getting single condo unit_valuation
    public List<Condo_API_Valuation> getCondo_Valuation(String record_id, String unit_valuation_id, String tbl_name) {
        List<Condo_API_Valuation> land_improvementsList = new ArrayList<Condo_API_Valuation>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_name
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND " + KEY_ID + " = \"" + unit_valuation_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Condo_API_Valuation vl = new Condo_API_Valuation();
                vl.setID(Integer.parseInt(cursor.getString(0)));
                vl.setrecord_id(cursor.getString(1));
                vl.setrecord_id(cursor.getString(1));
                vl.setreport_value_property_type(cursor.getString(2));
                vl.setreport_value_cct_no(cursor.getString(3));
                vl.setreport_value_unit_no(cursor.getString(4));
                vl.setreport_value_floor_area(cursor.getString(5));
                vl.setreport_value_deduction(cursor.getString(6));
                vl.setreport_value_net_area(cursor.getString(7));
                vl.setreport_value_unit_value(cursor.getString(8));
                vl.setreport_value_appraised_value(cursor.getString(9));

                // Adding Land Improvements
                land_improvementsList.add(vl);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return land_improvementsList;
    }


    //getting the total land value
    public String getCondo_API_Total_Land_Value_Lot_Valuation_Details(String record_id) {
        String result = "0";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT coalesce(SUM(report_value_appraised_value),0) FROM " + "condo_unit_valuation"
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"", null);
        if (cursor.moveToFirst()) {
            result = cursor.getString(0);
        }


        if(cursor != null) {
            cursor.close();
        }

        return result;
    }

    //getting the total land value
    public String getCount_of_Condo_valuations(String record_id) {
        String result = "0";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT coalesce(count(*),0) FROM " + "condo_unit_valuation"
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"", null);
        if (cursor.moveToFirst()) {
            result = cursor.getString(0);
        }


        if(cursor != null) {
            cursor.close();
        }

        return result;
    }
//Added by IAN
    public String[] getID_of_Condo_Valuation(String record_id) {


        SQLiteDatabase db = this.getWritableDatabase();

         Cursor cur = db.rawQuery("SELECT id FROM " + "condo_unit_valuation"
                 + " WHERE " + Report_record_id + " =  \"" + record_id + "\"", null);
        cur.moveToFirst();
        ArrayList<String> ids = new ArrayList<String>();
        while(!cur.isAfterLast()) {
            ids.add(cur.getString(cur.getColumnIndex("id")));
            cur.moveToNext();
        }
        cur.close();
        return ids.toArray(new String[ids.size()]);

    }

    // Updating condo unit_valuation
    public int updateCondo_Valuation(Condo_API_Valuation vl,
                                     String record_id, String unit_valuation_id, String tbl_name) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_value_property_type", vl.getreport_value_property_type());
        values.put("report_value_cct_no", vl.getreport_value_cct_no());
        values.put("report_value_unit_no", vl.getreport_value_unit_no());
        values.put("report_value_floor_area", vl.getreport_value_floor_area());
        values.put("report_value_deduction", vl.getreport_value_deduction());
        values.put("report_value_net_area", vl.getreport_value_net_area());
        values.put("report_value_unit_value", vl.getreport_value_unit_value());
        values.put("report_value_appraised_value", vl.getreport_value_appraised_value());
        // updating row
        return db.update(tbl_name, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, unit_valuation_id});
    }


    //all
    public int deleteCondo_Valuation(String record_id, String tbl_name) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_name,
                Report_record_id + " = ?",
                new String[]{record_id});
    }

    //single
    public int deleteCondo_Valuation(String record_id, String unit_valuation_id, String tbl_name) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_name,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, unit_valuation_id});
    }

    /**
     * construction ebm roomlist CRUD
     */
    public void addConstruction_Ebm_Room_List(Construction_Ebm_API_Room_List ce) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", ce.getrecord_id());
        values.put("report_room_list_area", ce.getreport_room_list_area());
        values.put("report_room_list_description", ce.getreport_room_list_description());
        values.put("report_room_list_est_proj_cost", ce.getreport_room_list_est_proj_cost());
        values.put("report_room_list_floor", ce.getreport_room_list_floor());
        values.put("report_room_list_rcn", ce.getreport_room_list_rcn());
        // Inserting Row
        db.insert(tbl_construction_ebm_room_list, null, values);
        db.close(); // Closing database connection
    }

    // Getting selected Construction Ebm rdps
    public List<Construction_Ebm_API_Room_List> getConstruction_Ebm_Room_List(String record_id) {
        List<Construction_Ebm_API_Room_List> ceList = new ArrayList<Construction_Ebm_API_Room_List>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_construction_ebm_room_list
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Construction_Ebm_API_Room_List ce = new Construction_Ebm_API_Room_List();
                ce.setID(Integer.parseInt(cursor.getString(0)));
                ce.setrecord_id(cursor.getString(1));
                ce.setreport_room_list_area(cursor.getString(2));
                ce.setreport_room_list_description(cursor.getString(3));
                ce.setreport_room_list_est_proj_cost(cursor.getString(4));
                ce.setreport_room_list_floor(cursor.getString(5));
                ce.setreport_room_list_rcn(cursor.getString(6));

                // Adding Construction Ebm
                ceList.add(ce);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return ceList
        return ceList;
    }

    // Getting selected Construction Ebm rdps with id
    public List<Construction_Ebm_API_Room_List> getConstruction_Ebm_Room_List_Single(String record_id, String cerl_id) {
        List<Construction_Ebm_API_Room_List> ceList = new ArrayList<Construction_Ebm_API_Room_List>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_construction_ebm_room_list
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND " + KEY_ID + " = \"" + cerl_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Construction_Ebm_API_Room_List ce = new Construction_Ebm_API_Room_List();
                ce.setID(Integer.parseInt(cursor.getString(0)));
                ce.setrecord_id(cursor.getString(1));
                ce.setreport_room_list_area(cursor.getString(2));
                ce.setreport_room_list_description(cursor.getString(3));
                ce.setreport_room_list_est_proj_cost(cursor.getString(4));
                ce.setreport_room_list_floor(cursor.getString(5));
                ce.setreport_room_list_rcn(cursor.getString(6));

                // Adding Construction Ebm
                ceList.add(ce);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return ceList
        return ceList;
    }

    // Updating single Construction Ebm
    public int updateConstruction_Ebm_Room_List(Construction_Ebm_API_Room_List ce,
                                                String record_id, String cerl_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_room_list_area", ce.getreport_room_list_area());
        values.put("report_room_list_description", ce.getreport_room_list_description());
        values.put("report_room_list_est_proj_cost", ce.getreport_room_list_est_proj_cost());
        values.put("report_room_list_floor", ce.getreport_room_list_floor());
        values.put("report_room_list_rcn", ce.getreport_room_list_rcn());
        // updating row
        return db.update(tbl_construction_ebm_room_list, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, cerl_id});
    }

    // Deleting All cerl via record_id
    public int deleteConstruction_Ebm_Room_List(String record_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_construction_ebm_room_list,
                Report_record_id + " = ?",
                new String[]{record_id});
    }

    // Deleting single cerl
    public int deleteConstruction_Ebm_Room_List(String record_id, String cerl_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_construction_ebm_room_list,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, cerl_id});
    }

    /**
     * MV Prev Appraisal CRUD
     */

    public void addMotor_Vehicle_Prev_App(Motor_Vehicle_API_Prev_Appraisal li) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", li.getrecord_id());
        values.put("report_prev_date", li.getreport_prev_date());
        values.put("report_prev_appraiser", li.getreport_prev_appraiser());
        values.put("report_prev_requestor", li.getreport_prev_requestor());
        values.put("report_prev_appraised_value", li.getreport_prev_appraised_value());
        // Inserting Row
        db.insert(tbl_motor_vehicle_prev_appraisal, null, values);
        db.close(); // Closing database connection
    }

    // Getting selected MV Prev Appraisal with id
    public List<Motor_Vehicle_API_Prev_Appraisal> getMV_Prev_appraisal_Single(String record_id, String rdps_id) {
        List<Motor_Vehicle_API_Prev_Appraisal> mv_prevList = new ArrayList<Motor_Vehicle_API_Prev_Appraisal>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_motor_vehicle_prev_appraisal
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\" AND " + KEY_ID + " = \"" + rdps_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Motor_Vehicle_API_Prev_Appraisal mv = new Motor_Vehicle_API_Prev_Appraisal();
                mv.setID(Integer.parseInt(cursor.getString(0)));
                mv.setrecord_id(cursor.getString(1));
                mv.setreport_prev_date(cursor.getString(2));
                mv.setreport_prev_appraiser(cursor.getString(3));
                mv.setreport_prev_requestor(cursor.getString(4));
                mv.setreport_prev_appraised_value(cursor.getString(5));


                // Adding Land Improvements
                mv_prevList.add(mv);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return mv_prevList
        return mv_prevList;
    }

    // Updating single  with
    public int updateMV_Prev_appraisal(Motor_Vehicle_API_Prev_Appraisal li,
                                            String record_id, String prev_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_prev_date", li.getreport_prev_date());
        values.put("report_prev_appraiser", li.getreport_prev_appraiser());
        values.put("report_prev_requestor", li.getreport_prev_requestor());
        values.put("report_prev_appraised_value", li.getreport_prev_appraised_value());
        // updating row
        return db.update(tbl_motor_vehicle_prev_appraisal, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, prev_id});
    }

    // Deleting single prev
    public int deleteMV_Prev_Single(String record_id, String prev_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_motor_vehicle_prev_appraisal,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, prev_id});
    }
    // Getting selected
    public List<Motor_Vehicle_API_Prev_Appraisal> getMV_Prev(String record_id) {
        List<Motor_Vehicle_API_Prev_Appraisal> prevList = new ArrayList<Motor_Vehicle_API_Prev_Appraisal>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_motor_vehicle_prev_appraisal
                + " WHERE " + Report_record_id + " =  \"" + record_id + "\"";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Motor_Vehicle_API_Prev_Appraisal li = new Motor_Vehicle_API_Prev_Appraisal();
                li.setID(Integer.parseInt(cursor.getString(0)));
                li.setrecord_id(cursor.getString(1));
                li.setreport_prev_date(cursor.getString(2));
                li.setreport_prev_appraiser(cursor.getString(3));
                li.setreport_prev_requestor(cursor.getString(4));
                li.setreport_prev_appraised_value(cursor.getString(5));

                // Adding
                prevList.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return land_improvementsList
        return prevList;
    }

    /**
     * MV Ownership History CRUD
     */

    public void addMotor_Vehicle_Ownership_History(Motor_Vehicle_API_Ownership_History mv) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", mv.getrecord_id());
        values.put("report_verification_ownerhistory_date_day", mv.getreport_verification_ownerhistory_date_day());
        values.put("report_verification_ownerhistory_date_month", mv.getreport_verification_ownerhistory_date_month());
        values.put("report_verification_ownerhistory_date_year", mv.getreport_verification_ownerhistory_date_year());
        values.put("report_verification_ownerhistory_name", mv.getreport_verification_ownerhistory_name());
        // Inserting Row
        db.insert(tbl_motor_vehicle_ownership_history, null, values);
        db.close(); // Closing database connection
    }


    // Getting selected Motor_Vehicle
    public List<Motor_Vehicle_API_Ownership_History> getMotor_Vehicle_Ownership_History(String record_id) {
        List<Motor_Vehicle_API_Ownership_History> motor_vehicleList = new ArrayList<Motor_Vehicle_API_Ownership_History>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_motor_vehicle_ownership_history + " WHERE "
                + Report_record_id + " =  \"" + record_id + "\"";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Motor_Vehicle_API_Ownership_History mv = new Motor_Vehicle_API_Ownership_History();
                mv.setID(Integer.parseInt(cursor.getString(0)));
                mv.setrecord_id(cursor.getString(1));
                mv.setreport_verification_ownerhistory_date_day(cursor.getString(2));
                mv.setreport_verification_ownerhistory_date_month(cursor.getString(3));
                mv.setreport_verification_ownerhistory_date_year(cursor.getString(4));
                mv.setreport_verification_ownerhistory_name(cursor.getString(5));

                motor_vehicleList.add(mv);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        return motor_vehicleList;
    }

    // Getting selected Motor_Vehicle single
    public List<Motor_Vehicle_API_Ownership_History> getMotor_Vehicle_Ownership_History(String record_id, String mvoh_id) {
        List<Motor_Vehicle_API_Ownership_History> motor_vehicleList = new ArrayList<Motor_Vehicle_API_Ownership_History>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_motor_vehicle_ownership_history + " WHERE "
                + Report_record_id + " =  \"" + record_id + "\" AND " + KEY_ID + " = \"" + mvoh_id + "\"";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Motor_Vehicle_API_Ownership_History mv = new Motor_Vehicle_API_Ownership_History();
                mv.setID(Integer.parseInt(cursor.getString(0)));
                mv.setrecord_id(cursor.getString(1));
                mv.setreport_verification_ownerhistory_date_day(cursor.getString(2));
                mv.setreport_verification_ownerhistory_date_month(cursor.getString(3));
                mv.setreport_verification_ownerhistory_date_year(cursor.getString(4));
                mv.setreport_verification_ownerhistory_name(cursor.getString(5));

                motor_vehicleList.add(mv);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        return motor_vehicleList;
    }

    // Updating single mvoh
    public int updateMotor_Vehicle_Ownership_History(Motor_Vehicle_API_Ownership_History mv,
                                                     String record_id, String mvoh_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_verification_ownerhistory_date_day", mv.getreport_verification_ownerhistory_date_day());
        values.put("report_verification_ownerhistory_date_month", mv.getreport_verification_ownerhistory_date_month());
        values.put("report_verification_ownerhistory_date_year", mv.getreport_verification_ownerhistory_date_year());
        values.put("report_verification_ownerhistory_name", mv.getreport_verification_ownerhistory_name());
        // updating row
        return db.update(tbl_motor_vehicle_ownership_history, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, mvoh_id});
    }

    // Deleting All mvoh via record_id
    public int deleteMotor_Vehicle_Ownership_History(String record_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_motor_vehicle_ownership_history,
                Report_record_id + " = ?",
                new String[]{record_id});
    }

    // Deleting single mvoh
    public int deleteMotor_Vehicle_Ownership_History(String record_id, String mvoh_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_motor_vehicle_ownership_history,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, mvoh_id});
    }

    /**
     * MV Ownership Source CRUD
     */

    public void addMotor_Vehicle_Ownership_Source(Motor_Vehicle_API_Ownership_Source mv) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", mv.getrecord_id());
        values.put("report_source_mileage", mv.getreport_source_mileage());
        values.put("report_source_range_min", mv.getreport_source_range_min());
        values.put("report_source_range_max", mv.getreport_source_range_max());
        values.put("report_source_source", mv.getreport_source_source());
        values.put("report_source_vehicle_type", mv.getreport_source_vehicle_type());
        // Inserting Row
        db.insert(tbl_motor_vehicle_ownership_source, null, values);
        db.close(); // Closing database connection
    }


    // Getting selected Motor_Vehicle
    public List<Motor_Vehicle_API_Ownership_Source> getMotor_Vehicle_Ownership_Source(String record_id) {
        List<Motor_Vehicle_API_Ownership_Source> motor_vehicleList = new ArrayList<Motor_Vehicle_API_Ownership_Source>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_motor_vehicle_ownership_source + " WHERE "
                + Report_record_id + " =  \"" + record_id + "\"";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Motor_Vehicle_API_Ownership_Source mv = new Motor_Vehicle_API_Ownership_Source();
                mv.setID(Integer.parseInt(cursor.getString(0)));
                mv.setrecord_id(cursor.getString(1));
                mv.setreport_source_mileage(cursor.getString(2));
                mv.setreport_source_range_min(cursor.getString(3));
                mv.setreport_source_range_max(cursor.getString(4));
                mv.setreport_source_source(cursor.getString(5));
                mv.setreport_source_vehicle_type(cursor.getString(6));

                motor_vehicleList.add(mv);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        return motor_vehicleList;
    }

    // Getting minmax Motor_Vehicle os
    public List<Motor_Vehicle_API_Ownership_Source> getMotor_Vehicle_Ownership_Source_Min(String record_id) {
        List<Motor_Vehicle_API_Ownership_Source> motor_vehicleList = new ArrayList<Motor_Vehicle_API_Ownership_Source>();
        // Select All Query *1.0 to convert 10 to double
        String selectQuery = "Select " +
                "MIN(NULLIF(motor_vehicle_ownership_source.report_source_range_min*1.0,0)), " +
                "MIN(NULLIF(motor_vehicle_ownership_source.report_source_range_max*1.0,0))" +
                " FROM " + tbl_motor_vehicle_ownership_source + " WHERE "
                + Report_record_id + " =  \"" + record_id + "\"";
        //MIN(NULLIF(value, 0)) ignores 0 value
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Motor_Vehicle_API_Ownership_Source mv = new Motor_Vehicle_API_Ownership_Source();
                //check if MIN(NULLIF(value, 0)) returns null then value will become 0
                if (cursor.getString(0) != null) {
                    mv.setreport_source_range_min(cursor.getString(0));
                } else {
                    mv.setreport_source_range_min("0");
                }
                if (cursor.getString(1) != null) {
                    mv.setreport_source_range_max(cursor.getString(1));
                } else {
                    mv.setreport_source_range_max("0");
                }
                //mv.setreport_source_range_min(cursor.getString(0));
                //mv.setreport_source_range_max(cursor.getString(1));
                motor_vehicleList.add(mv);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        return motor_vehicleList;
    }

    // Getting minmax Motor_Vehicle os
    public List<Motor_Vehicle_API_Ownership_Source> getMotor_Vehicle_Ownership_Source_Max(String record_id) {
        List<Motor_Vehicle_API_Ownership_Source> motor_vehicleList = new ArrayList<Motor_Vehicle_API_Ownership_Source>();
        // Select All Query
        String selectQuery = "Select " +
                "MAX(motor_vehicle_ownership_source.report_source_range_min*1.0) AS DOUBLE, " +
                "MAX(motor_vehicle_ownership_source.report_source_range_max*1.0) AS DOUBLE" +
                " FROM " + tbl_motor_vehicle_ownership_source + " WHERE "
                + Report_record_id + " =  \"" + record_id + "\"";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Motor_Vehicle_API_Ownership_Source mv = new Motor_Vehicle_API_Ownership_Source();
                mv.setreport_source_range_min(cursor.getString(0));
                mv.setreport_source_range_max(cursor.getString(1));
                motor_vehicleList.add(mv);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        return motor_vehicleList;
    }

    // Getting selected Motor_Vehicle single
    public List<Motor_Vehicle_API_Ownership_Source> getMotor_Vehicle_Ownership_Source(String record_id, String mvos_id) {
        List<Motor_Vehicle_API_Ownership_Source> motor_vehicleList = new ArrayList<Motor_Vehicle_API_Ownership_Source>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_motor_vehicle_ownership_source + " WHERE "
                + Report_record_id + " =  \"" + record_id + "\" AND " + KEY_ID + " = \"" + mvos_id + "\"";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Motor_Vehicle_API_Ownership_Source mv = new Motor_Vehicle_API_Ownership_Source();
                mv.setID(Integer.parseInt(cursor.getString(0)));
                mv.setrecord_id(cursor.getString(1));
                mv.setreport_source_mileage(cursor.getString(2));
                mv.setreport_source_range_min(cursor.getString(3));
                mv.setreport_source_range_max(cursor.getString(4));
                mv.setreport_source_source(cursor.getString(5));
                mv.setreport_source_vehicle_type(cursor.getString(6));

                motor_vehicleList.add(mv);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        return motor_vehicleList;
    }

    // Updating single mvoh
    public int updateMotor_Vehicle_Ownership_Source(Motor_Vehicle_API_Ownership_Source mv,
                                                    String record_id, String mvos_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_source_mileage", mv.getreport_source_mileage());
        values.put("report_source_range_min", mv.getreport_source_range_min());
        values.put("report_source_range_max", mv.getreport_source_range_max());
        values.put("report_source_source", mv.getreport_source_source());
        values.put("report_source_vehicle_type", mv.getreport_source_vehicle_type());
        // updating row
        return db.update(tbl_motor_vehicle_ownership_source, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, mvos_id});
    }

    // Deleting All mvoh via record_id
    public int deleteMotor_Vehicle_Ownership_Source(String record_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_motor_vehicle_ownership_source,
                Report_record_id + " = ?",
                new String[]{record_id});
    }

    // Deleting single mvoh
    public int deleteMotor_Vehicle_Ownership_Source(String record_id, String mvos_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_motor_vehicle_ownership_source,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, mvos_id});
    }

    /**
     * Ppcr Details CRUD
     */
    public void addPpcr_Details(Ppcr_API_Details pd) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("record_id", pd.getrecord_id());
        values.put("report_visit_month", pd.getreport_visit_month());
        values.put("report_visit_day", pd.getreport_visit_day());
        values.put("report_visit_year", pd.getreport_visit_year());
        values.put("report_nth", pd.getreport_nth());
        values.put("report_value_foundation", pd.getreport_value_foundation());
        values.put("report_value_columns", pd.getreport_value_columns());
        values.put("report_value_beams_girders", pd.getreport_value_beams_girders());
        values.put("report_value_surround_walls", pd.getreport_value_surround_walls());
        values.put("report_value_interior_partition", pd.getreport_value_interior_partition());
        values.put("report_value_roofing_works", pd.getreport_value_roofing_works());
        values.put("report_value_plumbing_rough_in", pd.getreport_value_plumbing_rough_in());
        values.put("report_value_electrical_rough_in", pd.getreport_value_electrical_rough_in());
        values.put("report_value_ceiling_works", pd.getreport_value_ceiling_works());
        values.put("report_value_doors_windows", pd.getreport_value_doors_windows());
        values.put("report_value_plastering_wall", pd.getreport_value_plastering_wall());
        values.put("report_value_slab_include_finish", pd.getreport_value_slab_include_finish());
        values.put("report_value_painting_finishing", pd.getreport_value_painting_finishing());
        values.put("report_value_plumbing_elect_fix", pd.getreport_value_plumbing_elect_fix());
        values.put("report_value_utilities_tapping", pd.getreport_value_utilities_tapping());
        values.put("report_completion_foundation", pd.getreport_completion_foundation());
        values.put("report_completion_columns", pd.getreport_completion_columns());
        values.put("report_completion_beams_girders", pd.getreport_completion_beams_girders());
        values.put("report_completion_surround_walls", pd.getreport_completion_surround_walls());
        values.put("report_completion_interior_partition", pd.getreport_completion_interior_partition());
        values.put("report_completion_roofing_works", pd.getreport_completion_roofing_works());
        values.put("report_completion_plumbing_rough_in", pd.getreport_completion_plumbing_rough_in());
        values.put("report_completion_electrical_rough_in", pd.getreport_completion_electrical_rough_in());
        values.put("report_completion_ceiling_works", pd.getreport_completion_ceiling_works());
        values.put("report_completion_doors_windows", pd.getreport_completion_doors_windows());
        values.put("report_completion_plastering_wall", pd.getreport_completion_plastering_wall());
        values.put("report_completion_slab_include_finish", pd.getreport_completion_slab_include_finish());
        values.put("report_completion_painting_finishing", pd.getreport_completion_painting_finishing());
        values.put("report_completion_plumbing_elect_fix", pd.getreport_completion_plumbing_elect_fix());
        values.put("report_completion_utilities_tapping", pd.getreport_completion_utilities_tapping());
        values.put("report_total_value", pd.getreport_total_value());
        // Inserting Row
        db.insert(tbl_ppcr_details, null, values);
        db.close(); // Closing database connection
    }


    // Getting selected Ppcr
    public List<Ppcr_API_Details> getPpcr_Details(String record_id) {
        List<Ppcr_API_Details> PpcrList = new ArrayList<Ppcr_API_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_ppcr_details + " WHERE "
                + Report_record_id + " =  \"" + record_id + "\"";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Ppcr_API_Details pd = new Ppcr_API_Details();
                pd.setID(Integer.parseInt(cursor.getString(0)));
                pd.setrecord_id(cursor.getString(1));
                pd.setreport_visit_month(cursor.getString(2));
                pd.setreport_visit_day(cursor.getString(3));
                pd.setreport_visit_year(cursor.getString(4));
                pd.setreport_nth(cursor.getString(5));
                pd.setreport_value_foundation(cursor.getString(6));
                pd.setreport_value_columns(cursor.getString(7));
                pd.setreport_value_beams_girders(cursor.getString(8));
                pd.setreport_value_surround_walls(cursor.getString(9));
                pd.setreport_value_interior_partition(cursor.getString(10));
                pd.setreport_value_roofing_works(cursor.getString(11));
                pd.setreport_value_plumbing_rough_in(cursor.getString(12));
                pd.setreport_value_electrical_rough_in(cursor.getString(13));
                pd.setreport_value_ceiling_works(cursor.getString(14));
                pd.setreport_value_doors_windows(cursor.getString(15));
                pd.setreport_value_plastering_wall(cursor.getString(16));
                pd.setreport_value_slab_include_finish(cursor.getString(17));
                pd.setreport_value_painting_finishing(cursor.getString(18));
                pd.setreport_value_plumbing_elect_fix(cursor.getString(19));
                pd.setreport_value_utilities_tapping(cursor.getString(20));
                pd.setreport_completion_foundation(cursor.getString(21));
                pd.setreport_completion_columns(cursor.getString(22));
                pd.setreport_completion_beams_girders(cursor.getString(23));
                pd.setreport_completion_surround_walls(cursor.getString(24));
                pd.setreport_completion_interior_partition(cursor.getString(25));
                pd.setreport_completion_roofing_works(cursor.getString(26));
                pd.setreport_completion_plumbing_rough_in(cursor.getString(27));
                pd.setreport_completion_electrical_rough_in(cursor.getString(28));
                pd.setreport_completion_ceiling_works(cursor.getString(29));
                pd.setreport_completion_doors_windows(cursor.getString(30));
                pd.setreport_completion_plastering_wall(cursor.getString(31));
                pd.setreport_completion_slab_include_finish(cursor.getString(32));
                pd.setreport_completion_painting_finishing(cursor.getString(33));
                pd.setreport_completion_plumbing_elect_fix(cursor.getString(34));
                pd.setreport_completion_utilities_tapping(cursor.getString(35));
                pd.setreport_total_value(cursor.getString(36));

                PpcrList.add(pd);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        return PpcrList;
    }

    // Getting selected Ppcr single
    public List<Ppcr_API_Details> getPpcr_Details(String record_id, String pd_id) {
        List<Ppcr_API_Details> PpcrList = new ArrayList<Ppcr_API_Details>();
        // Select All Query
        String selectQuery = "Select * FROM " + tbl_ppcr_details + " WHERE "
                + Report_record_id + " =  \"" + record_id + "\" AND " + KEY_ID + " = \"" + pd_id + "\"";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Ppcr_API_Details pd = new Ppcr_API_Details();
                pd.setID(Integer.parseInt(cursor.getString(0)));
                pd.setrecord_id(cursor.getString(1));
                pd.setreport_visit_month(cursor.getString(2));
                pd.setreport_visit_day(cursor.getString(3));
                pd.setreport_visit_year(cursor.getString(4));
                pd.setreport_nth(cursor.getString(5));
                pd.setreport_value_foundation(cursor.getString(6));
                pd.setreport_value_columns(cursor.getString(7));
                pd.setreport_value_beams_girders(cursor.getString(8));
                pd.setreport_value_surround_walls(cursor.getString(9));
                pd.setreport_value_interior_partition(cursor.getString(10));
                pd.setreport_value_roofing_works(cursor.getString(11));
                pd.setreport_value_plumbing_rough_in(cursor.getString(12));
                pd.setreport_value_electrical_rough_in(cursor.getString(13));
                pd.setreport_value_ceiling_works(cursor.getString(14));
                pd.setreport_value_doors_windows(cursor.getString(15));
                pd.setreport_value_plastering_wall(cursor.getString(16));
                pd.setreport_value_slab_include_finish(cursor.getString(17));
                pd.setreport_value_painting_finishing(cursor.getString(18));
                pd.setreport_value_plumbing_elect_fix(cursor.getString(19));
                pd.setreport_value_utilities_tapping(cursor.getString(20));
                pd.setreport_completion_foundation(cursor.getString(21));
                pd.setreport_completion_columns(cursor.getString(22));
                pd.setreport_completion_beams_girders(cursor.getString(23));
                pd.setreport_completion_surround_walls(cursor.getString(24));
                pd.setreport_completion_interior_partition(cursor.getString(25));
                pd.setreport_completion_roofing_works(cursor.getString(26));
                pd.setreport_completion_plumbing_rough_in(cursor.getString(27));
                pd.setreport_completion_electrical_rough_in(cursor.getString(28));
                pd.setreport_completion_ceiling_works(cursor.getString(29));
                pd.setreport_completion_doors_windows(cursor.getString(30));
                pd.setreport_completion_plastering_wall(cursor.getString(31));
                pd.setreport_completion_slab_include_finish(cursor.getString(32));
                pd.setreport_completion_painting_finishing(cursor.getString(33));
                pd.setreport_completion_plumbing_elect_fix(cursor.getString(34));
                pd.setreport_completion_utilities_tapping(cursor.getString(35));
                pd.setreport_total_value(cursor.getString(36));

                PpcrList.add(pd);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        return PpcrList;
    }

    // Updating single pdoh
    public int updatePpcr_Details(Ppcr_API_Details pd,
                                  String record_id, String pd_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("report_visit_month", pd.getreport_visit_month());
        values.put("report_visit_day", pd.getreport_visit_day());
        values.put("report_visit_year", pd.getreport_visit_year());
        values.put("report_nth", pd.getreport_nth());
        values.put("report_value_foundation", pd.getreport_value_foundation());
        values.put("report_value_columns", pd.getreport_value_columns());
        values.put("report_value_beams_girders", pd.getreport_value_beams_girders());
        values.put("report_value_surround_walls", pd.getreport_value_surround_walls());
        values.put("report_value_interior_partition", pd.getreport_value_interior_partition());
        values.put("report_value_roofing_works", pd.getreport_value_roofing_works());
        values.put("report_value_plumbing_rough_in", pd.getreport_value_plumbing_rough_in());
        values.put("report_value_electrical_rough_in", pd.getreport_value_electrical_rough_in());
        values.put("report_value_ceiling_works", pd.getreport_value_ceiling_works());
        values.put("report_value_doors_windows", pd.getreport_value_doors_windows());
        values.put("report_value_plastering_wall", pd.getreport_value_plastering_wall());
        values.put("report_value_slab_include_finish", pd.getreport_value_slab_include_finish());
        values.put("report_value_painting_finishing", pd.getreport_value_painting_finishing());
        values.put("report_value_plumbing_elect_fix", pd.getreport_value_plumbing_elect_fix());
        values.put("report_value_utilities_tapping", pd.getreport_value_utilities_tapping());
        values.put("report_completion_foundation", pd.getreport_completion_foundation());
        values.put("report_completion_columns", pd.getreport_completion_columns());
        values.put("report_completion_beams_girders", pd.getreport_completion_beams_girders());
        values.put("report_completion_surround_walls", pd.getreport_completion_surround_walls());
        values.put("report_completion_interior_partition", pd.getreport_completion_interior_partition());
        values.put("report_completion_roofing_works", pd.getreport_completion_roofing_works());
        values.put("report_completion_plumbing_rough_in", pd.getreport_completion_plumbing_rough_in());
        values.put("report_completion_electrical_rough_in", pd.getreport_completion_electrical_rough_in());
        values.put("report_completion_ceiling_works", pd.getreport_completion_ceiling_works());
        values.put("report_completion_doors_windows", pd.getreport_completion_doors_windows());
        values.put("report_completion_plastering_wall", pd.getreport_completion_plastering_wall());
        values.put("report_completion_slab_include_finish", pd.getreport_completion_slab_include_finish());
        values.put("report_completion_painting_finishing", pd.getreport_completion_painting_finishing());
        values.put("report_completion_plumbing_elect_fix", pd.getreport_completion_plumbing_elect_fix());
        values.put("report_completion_utilities_tapping", pd.getreport_completion_utilities_tapping());
        values.put("report_total_value", pd.getreport_total_value());
        // updating row
        return db.update(tbl_ppcr_details, values,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, pd_id});
    }

    // Deleting All pd via record_id
    public int deletePpcr_Details(String record_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_ppcr_details,
                Report_record_id + " = ?",
                new String[]{record_id});
    }

    // Deleting single pd
    public int deletePpcr_Details(String record_id, String pd_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        return db.delete(tbl_ppcr_details,
                Report_record_id + " = ? AND " + KEY_ID + " = ?",
                new String[]{record_id, pd_id});
    }

    // Getting latest added record in th imp details TH
    public List<Townhouse_API_Imp_Details> getLatest_Data_TH(String record_id) {
        List<Townhouse_API_Imp_Details> mylist = new ArrayList<Townhouse_API_Imp_Details>();
        //get latest added record
        String selectQuery = "Select " + imp_details_id + " FROM " + tbl_townhouse_imp_details
                + " WHERE "
                + Report_record_id + " =  \"" + record_id + "\""
                + " ORDER BY ROWID DESC LIMIT 1";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Townhouse_API_Imp_Details th = new Townhouse_API_Imp_Details();
                th.setID(Integer.parseInt(cursor.getString(0)));
                // Adding Townhouse Imp Details
                mylist.add(th);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return mylist
        return mylist;
    }

    // Getting latest added record in imp details LI
    public List<Land_Improvements_API_Imp_Details> getLatest_Data_LI(String record_id) {
        List<Land_Improvements_API_Imp_Details> mylist = new ArrayList<Land_Improvements_API_Imp_Details>();
        //get latest added record
        String selectQuery = "Select " + imp_details_id + " FROM " + tbl_land_improvements_imp_details
                + " WHERE "
                + Report_record_id + " =  \"" + record_id + "\""
                + " ORDER BY ROWID DESC LIMIT 1";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Land_Improvements_API_Imp_Details li = new Land_Improvements_API_Imp_Details();
                li.setID(Integer.parseInt(cursor.getString(0)));
                // Adding Townhouse Imp Details
                mylist.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return mylist
        return mylist;
    }

    // Getting latest added record in th imp details LI
    public List<Land_Improvements_API_Cost_Valuation> getLatest_Data_CV_LI(String record_id) {
        List<Land_Improvements_API_Cost_Valuation> mylist = new ArrayList<Land_Improvements_API_Cost_Valuation>();
        //get latest added record
        String selectQuery = "Select " + imp_valuation_id + " FROM " + tbl_land_improvements_imp_valuation
                + " WHERE "
                + Report_record_id + " =  \"" + record_id + "\""
                + " ORDER BY ROWID DESC LIMIT 1";
        Log.e("", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Land_Improvements_API_Cost_Valuation li = new Land_Improvements_API_Cost_Valuation();
                li.setID(Integer.parseInt(cursor.getString(0)));
                // Adding Townhouse Imp Details
                mylist.add(li);
            } while (cursor.moveToNext());
        }
        if(cursor != null) {
            cursor.close();
        }
        // return mylist
        return mylist;
    }


    //get record from chua
    public ArrayList<String> getRecord(String fieldName, String whereClause, String[] whereArgs, String table) {
        ArrayList<String> results = new ArrayList<String>();
        String selectQuery = "Select " + fieldName + " FROM " + table;
        //Log.e("selectQuery", selectQuery);
        String args = "args", value = "";

        for(int i = 0; i < whereArgs.length; i++){

            whereClause = whereClause.replaceFirst(args, "\""+whereArgs[i]+"\"");

        }
        //Log.e("where", whereClause);
        String query = selectQuery + " " + whereClause;

        Log.e("query", query);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);


        if (cursor.moveToFirst()) {
            do {
                // get  the  data into array,or class variable

                value = gds.nullChecker(cursor.getString(cursor.getColumnIndex(fieldName)));
                results.add(value);

            } while (cursor.moveToNext());
        }else{
            results.add(value);
        }

        if(cursor != null) {
            cursor.close();
        }

        return results;
    }
    public void addRecord(ArrayList<String> value, ArrayList<String> fields, String table){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        for (int i = 0; i < value.size(); i++) {
            values.put(fields.get(i).toString(), value.get(i).toString());
        }

        db.insert(table, null, values);
        db.close();


    }
    public void updateRecord(ArrayList<String> updatedvalues, ArrayList<String> fields, String whereClause, String[] whereArgs, String table){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        for(int i = 0; i < updatedvalues.size(); i++){
            values.put(fields.get(i).toString(), updatedvalues.get(i).toString());
            Log.e("values",fields.get(i).toString()+" "+updatedvalues.get(i).toString());
        }

        db.update(table, values, whereClause, whereArgs);
        //db.close();

    }
    public void deleteRecord(String table, String whereClause, String[] whereArgs){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(table, whereClause, whereArgs);
        //  db.close();
        Log.e("Deleted",""+table);
    }

}