package com.gds.appraisalmaybank.database2;

public class Ppcr_API_Details {

	// private variables
	int id;
	String record_id;
	String report_visit_month;
	String report_visit_day;
	String report_visit_year;
	String report_nth;
	String report_value_foundation;
	String report_value_columns;
	String report_value_beams_girders;
	String report_value_surround_walls;
	String report_value_interior_partition;
	String report_value_roofing_works;
	String report_value_plumbing_rough_in;
	String report_value_electrical_rough_in;
	String report_value_ceiling_works;
	String report_value_doors_windows;
	String report_value_plastering_wall;
	String report_value_slab_include_finish;
	String report_value_painting_finishing;
	String report_value_plumbing_elect_fix;
	String report_value_utilities_tapping;
	String report_completion_foundation;
	String report_completion_columns;
	String report_completion_beams_girders;
	String report_completion_surround_walls;
	String report_completion_interior_partition;
	String report_completion_roofing_works;
	String report_completion_plumbing_rough_in;
	String report_completion_electrical_rough_in;
	String report_completion_ceiling_works;
	String report_completion_doors_windows;
	String report_completion_plastering_wall;
	String report_completion_slab_include_finish;
	String report_completion_painting_finishing;
	String report_completion_plumbing_elect_fix;
	String report_completion_utilities_tapping;
	String report_total_value;
	
	// Empty constructor
	public Ppcr_API_Details() {
	}
	
	public Ppcr_API_Details(int id, String record_id,
			String report_visit_month, String report_visit_day,
			String report_visit_year, String report_nth,
			String report_value_foundation, String report_value_columns,
			String report_value_beams_girders,
			String report_value_surround_walls,
			String report_value_interior_partition,
			String report_value_roofing_works,
			String report_value_plumbing_rough_in,
			String report_value_electrical_rough_in,
			String report_value_ceiling_works,
			String report_value_doors_windows,
			String report_value_plastering_wall,
			String report_value_slab_include_finish,
			String report_value_painting_finishing,
			String report_value_plumbing_elect_fix,
			String report_value_utilities_tapping,
			String report_completion_foundation,
			String report_completion_columns,
			String report_completion_beams_girders,
			String report_completion_surround_walls,
			String report_completion_interior_partition,
			String report_completion_roofing_works,
			String report_completion_plumbing_rough_in,
			String report_completion_electrical_rough_in,
			String report_completion_ceiling_works,
			String report_completion_doors_windows,
			String report_completion_plastering_wall,
			String report_completion_slab_include_finish,
			String report_completion_painting_finishing,
			String report_completion_plumbing_elect_fix,
			String report_completion_utilities_tapping,
			String report_total_value) {
		this.id = id;
		this.record_id = record_id;
		this.report_visit_month = report_visit_month;
		this.report_visit_day = report_visit_day;
		this.report_visit_year = report_visit_year;
		this.report_nth = report_nth;
		this.report_value_foundation = report_value_foundation;
		this.report_value_columns = report_value_columns;
		this.report_value_beams_girders = report_value_beams_girders;
		this.report_value_surround_walls = report_value_surround_walls;
		this.report_value_interior_partition = report_value_interior_partition;
		this.report_value_roofing_works = report_value_roofing_works;
		this.report_value_plumbing_rough_in = report_value_plumbing_rough_in;
		this.report_value_electrical_rough_in = report_value_electrical_rough_in;
		this.report_value_ceiling_works = report_value_ceiling_works;
		this.report_value_doors_windows = report_value_doors_windows;
		this.report_value_plastering_wall = report_value_plastering_wall;
		this.report_value_slab_include_finish = report_value_slab_include_finish;
		this.report_value_painting_finishing = report_value_painting_finishing;
		this.report_value_plumbing_elect_fix = report_value_plumbing_elect_fix;
		this.report_value_utilities_tapping = report_value_utilities_tapping;
		this.report_completion_foundation = report_completion_foundation;
		this.report_completion_columns = report_completion_columns;
		this.report_completion_beams_girders = report_completion_beams_girders;
		this.report_completion_surround_walls = report_completion_surround_walls;
		this.report_completion_interior_partition = report_completion_interior_partition;
		this.report_completion_roofing_works = report_completion_roofing_works;
		this.report_completion_plumbing_rough_in = report_completion_plumbing_rough_in;
		this.report_completion_electrical_rough_in = report_completion_electrical_rough_in;
		this.report_completion_ceiling_works = report_completion_ceiling_works;
		this.report_completion_doors_windows = report_completion_doors_windows;
		this.report_completion_plastering_wall = report_completion_plastering_wall;
		this.report_completion_slab_include_finish = report_completion_slab_include_finish;
		this.report_completion_painting_finishing = report_completion_painting_finishing;
		this.report_completion_plumbing_elect_fix = report_completion_plumbing_elect_fix;
		this.report_completion_utilities_tapping = report_completion_utilities_tapping;
		this.report_total_value = report_total_value;
	}
	
	public Ppcr_API_Details(String record_id,
			String report_visit_month, String report_visit_day,
			String report_visit_year, String report_nth,
			String report_value_foundation, String report_value_columns,
			String report_value_beams_girders,
			String report_value_surround_walls,
			String report_value_interior_partition,
			String report_value_roofing_works,
			String report_value_plumbing_rough_in,
			String report_value_electrical_rough_in,
			String report_value_ceiling_works,
			String report_value_doors_windows,
			String report_value_plastering_wall,
			String report_value_slab_include_finish,
			String report_value_painting_finishing,
			String report_value_plumbing_elect_fix,
			String report_value_utilities_tapping,
			String report_completion_foundation,
			String report_completion_columns,
			String report_completion_beams_girders,
			String report_completion_surround_walls,
			String report_completion_interior_partition,
			String report_completion_roofing_works,
			String report_completion_plumbing_rough_in,
			String report_completion_electrical_rough_in,
			String report_completion_ceiling_works,
			String report_completion_doors_windows,
			String report_completion_plastering_wall,
			String report_completion_slab_include_finish,
			String report_completion_painting_finishing,
			String report_completion_plumbing_elect_fix,
			String report_completion_utilities_tapping,
			String report_total_value) {
		this.record_id = record_id;
		this.report_visit_month = report_visit_month;
		this.report_visit_day = report_visit_day;
		this.report_visit_year = report_visit_year;
		this.report_nth = report_nth;
		this.report_value_foundation = report_value_foundation;
		this.report_value_columns = report_value_columns;
		this.report_value_beams_girders = report_value_beams_girders;
		this.report_value_surround_walls = report_value_surround_walls;
		this.report_value_interior_partition = report_value_interior_partition;
		this.report_value_roofing_works = report_value_roofing_works;
		this.report_value_plumbing_rough_in = report_value_plumbing_rough_in;
		this.report_value_electrical_rough_in = report_value_electrical_rough_in;
		this.report_value_ceiling_works = report_value_ceiling_works;
		this.report_value_doors_windows = report_value_doors_windows;
		this.report_value_plastering_wall = report_value_plastering_wall;
		this.report_value_slab_include_finish = report_value_slab_include_finish;
		this.report_value_painting_finishing = report_value_painting_finishing;
		this.report_value_plumbing_elect_fix = report_value_plumbing_elect_fix;
		this.report_value_utilities_tapping = report_value_utilities_tapping;
		this.report_completion_foundation = report_completion_foundation;
		this.report_completion_columns = report_completion_columns;
		this.report_completion_beams_girders = report_completion_beams_girders;
		this.report_completion_surround_walls = report_completion_surround_walls;
		this.report_completion_interior_partition = report_completion_interior_partition;
		this.report_completion_roofing_works = report_completion_roofing_works;
		this.report_completion_plumbing_rough_in = report_completion_plumbing_rough_in;
		this.report_completion_electrical_rough_in = report_completion_electrical_rough_in;
		this.report_completion_ceiling_works = report_completion_ceiling_works;
		this.report_completion_doors_windows = report_completion_doors_windows;
		this.report_completion_plastering_wall = report_completion_plastering_wall;
		this.report_completion_slab_include_finish = report_completion_slab_include_finish;
		this.report_completion_painting_finishing = report_completion_painting_finishing;
		this.report_completion_plumbing_elect_fix = report_completion_plumbing_elect_fix;
		this.report_completion_utilities_tapping = report_completion_utilities_tapping;
		this.report_total_value = report_total_value;
	}
	
	public Ppcr_API_Details(String report_visit_month, String report_visit_day,
			String report_visit_year, String report_nth,
			String report_value_foundation, String report_value_columns,
			String report_value_beams_girders,
			String report_value_surround_walls,
			String report_value_interior_partition,
			String report_value_roofing_works,
			String report_value_plumbing_rough_in,
			String report_value_electrical_rough_in,
			String report_value_ceiling_works,
			String report_value_doors_windows,
			String report_value_plastering_wall,
			String report_value_slab_include_finish,
			String report_value_painting_finishing,
			String report_value_plumbing_elect_fix,
			String report_value_utilities_tapping,
			String report_completion_foundation,
			String report_completion_columns,
			String report_completion_beams_girders,
			String report_completion_surround_walls,
			String report_completion_interior_partition,
			String report_completion_roofing_works,
			String report_completion_plumbing_rough_in,
			String report_completion_electrical_rough_in,
			String report_completion_ceiling_works,
			String report_completion_doors_windows,
			String report_completion_plastering_wall,
			String report_completion_slab_include_finish,
			String report_completion_painting_finishing,
			String report_completion_plumbing_elect_fix,
			String report_completion_utilities_tapping,
			String report_total_value) {
		this.report_visit_month = report_visit_month;
		this.report_visit_day = report_visit_day;
		this.report_visit_year = report_visit_year;
		this.report_nth = report_nth;
		this.report_value_foundation = report_value_foundation;
		this.report_value_columns = report_value_columns;
		this.report_value_beams_girders = report_value_beams_girders;
		this.report_value_surround_walls = report_value_surround_walls;
		this.report_value_interior_partition = report_value_interior_partition;
		this.report_value_roofing_works = report_value_roofing_works;
		this.report_value_plumbing_rough_in = report_value_plumbing_rough_in;
		this.report_value_electrical_rough_in = report_value_electrical_rough_in;
		this.report_value_ceiling_works = report_value_ceiling_works;
		this.report_value_doors_windows = report_value_doors_windows;
		this.report_value_plastering_wall = report_value_plastering_wall;
		this.report_value_slab_include_finish = report_value_slab_include_finish;
		this.report_value_painting_finishing = report_value_painting_finishing;
		this.report_value_plumbing_elect_fix = report_value_plumbing_elect_fix;
		this.report_value_utilities_tapping = report_value_utilities_tapping;
		this.report_completion_foundation = report_completion_foundation;
		this.report_completion_columns = report_completion_columns;
		this.report_completion_beams_girders = report_completion_beams_girders;
		this.report_completion_surround_walls = report_completion_surround_walls;
		this.report_completion_interior_partition = report_completion_interior_partition;
		this.report_completion_roofing_works = report_completion_roofing_works;
		this.report_completion_plumbing_rough_in = report_completion_plumbing_rough_in;
		this.report_completion_electrical_rough_in = report_completion_electrical_rough_in;
		this.report_completion_ceiling_works = report_completion_ceiling_works;
		this.report_completion_doors_windows = report_completion_doors_windows;
		this.report_completion_plastering_wall = report_completion_plastering_wall;
		this.report_completion_slab_include_finish = report_completion_slab_include_finish;
		this.report_completion_painting_finishing = report_completion_painting_finishing;
		this.report_completion_plumbing_elect_fix = report_completion_plumbing_elect_fix;
		this.report_completion_utilities_tapping = report_completion_utilities_tapping;
		this.report_total_value = report_total_value;
	}
	
	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	// getting record_id
	public String getrecord_id() {
		return this.record_id;
	}

	// setting record_id
	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}
	
	//getters
	public String getreport_visit_month() {
		return this.report_visit_month;
	}

	public String getreport_visit_day() {
		return this.report_visit_day;
	}

	public String getreport_visit_year() {
		return this.report_visit_year;
	}

	public String getreport_nth() {
		return this.report_nth;
	}

	public String getreport_value_foundation() {
		return this.report_value_foundation;
	}

	public String getreport_value_columns() {
		return this.report_value_columns;
	}

	public String getreport_value_beams_girders() {
		return this.report_value_beams_girders;
	}

	public String getreport_value_surround_walls() {
		return this.report_value_surround_walls;
	}

	public String getreport_value_interior_partition() {
		return this.report_value_interior_partition;
	}

	public String getreport_value_roofing_works() {
		return this.report_value_roofing_works;
	}

	public String getreport_value_plumbing_rough_in() {
		return this.report_value_plumbing_rough_in;
	}

	public String getreport_value_electrical_rough_in() {
		return this.report_value_electrical_rough_in;
	}

	public String getreport_value_ceiling_works() {
		return this.report_value_ceiling_works;
	}

	public String getreport_value_doors_windows() {
		return this.report_value_doors_windows;
	}

	public String getreport_value_plastering_wall() {
		return this.report_value_plastering_wall;
	}

	public String getreport_value_slab_include_finish() {
		return this.report_value_slab_include_finish;
	}

	public String getreport_value_painting_finishing() {
		return this.report_value_painting_finishing;
	}

	public String getreport_value_plumbing_elect_fix() {
		return this.report_value_plumbing_elect_fix;
	}

	public String getreport_value_utilities_tapping() {
		return this.report_value_utilities_tapping;
	}

	public String getreport_completion_foundation() {
		return this.report_completion_foundation;
	}

	public String getreport_completion_columns() {
		return this.report_completion_columns;
	}

	public String getreport_completion_beams_girders() {
		return this.report_completion_beams_girders;
	}

	public String getreport_completion_surround_walls() {
		return this.report_completion_surround_walls;
	}

	public String getreport_completion_interior_partition() {
		return this.report_completion_interior_partition;
	}

	public String getreport_completion_roofing_works() {
		return this.report_completion_roofing_works;
	}

	public String getreport_completion_plumbing_rough_in() {
		return this.report_completion_plumbing_rough_in;
	}

	public String getreport_completion_electrical_rough_in() {
		return this.report_completion_electrical_rough_in;
	}

	public String getreport_completion_ceiling_works() {
		return this.report_completion_ceiling_works;
	}

	public String getreport_completion_doors_windows() {
		return this.report_completion_doors_windows;
	}

	public String getreport_completion_plastering_wall() {
		return this.report_completion_plastering_wall;
	}

	public String getreport_completion_slab_include_finish() {
		return this.report_completion_slab_include_finish;
	}

	public String getreport_completion_painting_finishing() {
		return this.report_completion_painting_finishing;
	}

	public String getreport_completion_plumbing_elect_fix() {
		return this.report_completion_plumbing_elect_fix;
	}


	public String getreport_completion_utilities_tapping() {
		return this.report_completion_utilities_tapping;
	}

	public String getreport_total_value() {
		return this.report_total_value;
	}
	
	//setters
	public void setreport_visit_month(String report_visit_month) {
		this.report_visit_month = report_visit_month;
	}

	public void setreport_visit_day(String report_visit_day) {
		this.report_visit_day = report_visit_day;
	}

	public void setreport_visit_year(String report_visit_year) {
		this.report_visit_year = report_visit_year;
	}

	public void setreport_nth(String report_nth) {
		this.report_nth = report_nth;
	}

	public void setreport_value_foundation(String report_value_foundation) {
		this.report_value_foundation = report_value_foundation;
	}

	public void setreport_value_columns(String report_value_columns) {
		this.report_value_columns = report_value_columns;
	}

	public void setreport_value_beams_girders(String report_value_beams_girders) {
		this.report_value_beams_girders = report_value_beams_girders;
	}

	public void setreport_value_surround_walls(
			String report_value_surround_walls) {
		this.report_value_surround_walls = report_value_surround_walls;
	}

	public void setreport_value_interior_partition(
			String report_value_interior_partition) {
		this.report_value_interior_partition = report_value_interior_partition;
	}

	public void setreport_value_roofing_works(String report_value_roofing_works) {
		this.report_value_roofing_works = report_value_roofing_works;
	}

	public void setreport_value_plumbing_rough_in(
			String report_value_plumbing_rough_in) {
		this.report_value_plumbing_rough_in = report_value_plumbing_rough_in;
	}

	public void setreport_value_electrical_rough_in(
			String report_value_electrical_rough_in) {
		this.report_value_electrical_rough_in = report_value_electrical_rough_in;
	}

	public void setreport_value_ceiling_works(String report_value_ceiling_works) {
		this.report_value_ceiling_works = report_value_ceiling_works;
	}

	public void setreport_value_doors_windows(String report_value_doors_windows) {
		this.report_value_doors_windows = report_value_doors_windows;
	}

	public void setreport_value_plastering_wall(
			String report_value_plastering_wall) {
		this.report_value_plastering_wall = report_value_plastering_wall;
	}

	public void setreport_value_slab_include_finish(
			String report_value_slab_include_finish) {
		this.report_value_slab_include_finish = report_value_slab_include_finish;
	}

	public void setreport_value_painting_finishing(
			String report_value_painting_finishing) {
		this.report_value_painting_finishing = report_value_painting_finishing;
	}

	public void setreport_value_plumbing_elect_fix(
			String report_value_plumbing_elect_fix) {
		this.report_value_plumbing_elect_fix = report_value_plumbing_elect_fix;
	}

	public void setreport_value_utilities_tapping(
			String report_value_utilities_tapping) {
		this.report_value_utilities_tapping = report_value_utilities_tapping;
	}

	public void setreport_completion_foundation(
			String report_completion_foundation) {
		this.report_completion_foundation = report_completion_foundation;
	}

	public void setreport_completion_columns(String report_completion_columns) {
		this.report_completion_columns = report_completion_columns;
	}

	public void setreport_completion_beams_girders(
			String report_completion_beams_girders) {
		this.report_completion_beams_girders = report_completion_beams_girders;
	}

	public void setreport_completion_surround_walls(
			String report_completion_surround_walls) {
		this.report_completion_surround_walls = report_completion_surround_walls;
	}

	public void setreport_completion_interior_partition(
			String report_completion_interior_partition) {
		this.report_completion_interior_partition = report_completion_interior_partition;
	}

	public void setreport_completion_roofing_works(
			String report_completion_roofing_works) {
		this.report_completion_roofing_works = report_completion_roofing_works;
	}

	public void setreport_completion_plumbing_rough_in(
			String report_completion_plumbing_rough_in) {
		this.report_completion_plumbing_rough_in = report_completion_plumbing_rough_in;
	}

	public void setreport_completion_electrical_rough_in(
			String report_completion_electrical_rough_in) {
		this.report_completion_electrical_rough_in = report_completion_electrical_rough_in;
	}

	public void setreport_completion_ceiling_works(
			String report_completion_ceiling_works) {
		this.report_completion_ceiling_works = report_completion_ceiling_works;
	}

	public void setreport_completion_doors_windows(
			String report_completion_doors_windows) {
		this.report_completion_doors_windows = report_completion_doors_windows;
	}

	public void setreport_completion_plastering_wall(
			String report_completion_plastering_wall) {
		this.report_completion_plastering_wall = report_completion_plastering_wall;
	}

	public void setreport_completion_slab_include_finish(
			String report_completion_slab_include_finish) {
		this.report_completion_slab_include_finish = report_completion_slab_include_finish;
	}

	public void setreport_completion_painting_finishing(
			String report_completion_painting_finishing) {
		this.report_completion_painting_finishing = report_completion_painting_finishing;
	}

	public void setreport_completion_plumbing_elect_fix(
			String report_completion_plumbing_elect_fix) {
		this.report_completion_plumbing_elect_fix = report_completion_plumbing_elect_fix;
	}


	public void setreport_completion_utilities_tapping(
			String report_completion_utilities_tapping) {
		this.report_completion_utilities_tapping = report_completion_utilities_tapping;
	}

	public void setreport_total_value(String report_total_value) {
		this.report_total_value = report_total_value;
	}
}
