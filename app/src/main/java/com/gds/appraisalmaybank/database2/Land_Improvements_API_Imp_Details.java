package com.gds.appraisalmaybank.database2;

public class Land_Improvements_API_Imp_Details {
	// private variables
	int imp_details_id;
	String record_id;
	String report_impsummary1_no_of_floors;
	String report_impsummary1_building_desc;
	String report_impsummary1_erected_on_lot;
	String report_impsummary1_fa;
	String report_impsummary1_fa_per_td;
	String report_impsummary1_actual_utilization;
	String report_impsummary1_usage_declaration;
	String report_impsummary1_owner;
	String report_impsummary1_socialized_housing;
	String report_desc_property_type;
	String report_desc_foundation;
	String report_desc_columns_posts;
	String report_desc_beams;
	String report_desc_exterior_walls;
	String report_desc_interior_walls;
	String report_desc_imp_flooring;
	String report_desc_doors;
	String report_desc_imp_windows;
	String report_desc_ceiling;
	String report_desc_imp_roofing;
	String report_desc_trusses;
	String report_desc_economic_life;
	String report_desc_effective_age;
	String report_desc_imp_remain_life;
	String report_desc_occupants;
	String report_desc_owned_or_leased;
	String report_desc_imp_floor_area;
	String report_desc_confirmed_thru;
	String report_desc_observed_condition;
	String report_ownership_of_property;
	String report_impsummary1_no_of_bedrooms;
	String valrep_landimp_impsummary1_no_of_tb;
	String valrep_landimp_desc_stairs;

	// Empty constructor
	public Land_Improvements_API_Imp_Details() {

	}

	public Land_Improvements_API_Imp_Details(String record_id) {
		this.record_id = record_id;
	}

	public Land_Improvements_API_Imp_Details(int imp_details_id, String record_id,
			String report_impsummary1_no_of_floors,
			String report_impsummary1_building_desc,
			String report_impsummary1_erected_on_lot,
			String report_impsummary1_fa, String report_impsummary1_fa_per_td,
			String report_impsummary1_actual_utilization,
			String report_impsummary1_usage_declaration,
			String report_impsummary1_owner,
			
			String report_impsummary1_socialized_housing,
			String report_desc_property_type,
			
			String report_desc_foundation, String report_desc_columns_posts,
			String report_desc_beams, String report_desc_exterior_walls,
			String report_desc_interior_walls, String report_desc_imp_flooring,
			String report_desc_doors, String report_desc_imp_windows,
			String report_desc_ceiling, String report_desc_imp_roofing,
			String report_desc_trusses, String report_desc_economic_life,
			String report_desc_effective_age,
			String report_desc_imp_remain_life, String report_desc_occupants,
			String report_desc_owned_or_leased,
			String report_desc_imp_floor_area,
			String report_desc_confirmed_thru,
			String report_desc_observed_condition,
			String report_ownership_of_property,
											 String report_impsummary1_no_of_bedrooms,
											 String valrep_landimp_impsummary1_no_of_tb,
											 String valrep_landimp_desc_stairs) {
		this.imp_details_id = imp_details_id;
		this.record_id = record_id;
		this.report_impsummary1_no_of_floors = report_impsummary1_no_of_floors;
		this.report_impsummary1_building_desc = report_impsummary1_building_desc;
		this.report_impsummary1_erected_on_lot = report_impsummary1_erected_on_lot;
		this.report_impsummary1_fa = report_impsummary1_fa;
		this.report_impsummary1_fa_per_td = report_impsummary1_fa_per_td;
		this.report_impsummary1_actual_utilization = report_impsummary1_actual_utilization;
		this.report_impsummary1_usage_declaration = report_impsummary1_usage_declaration;
		this.report_impsummary1_owner = report_impsummary1_owner;
		
		this.report_impsummary1_socialized_housing = report_impsummary1_socialized_housing;
		this.report_desc_property_type = report_desc_property_type;
		
		this.report_desc_foundation = report_desc_foundation;
		this.report_desc_columns_posts = report_desc_columns_posts;
		this.report_desc_beams = report_desc_beams;
		this.report_desc_exterior_walls = report_desc_exterior_walls;
		this.report_desc_interior_walls = report_desc_interior_walls;
		this.report_desc_imp_flooring = report_desc_imp_flooring;
		this.report_desc_doors = report_desc_doors;
		this.report_desc_imp_windows = report_desc_imp_windows;
		this.report_desc_ceiling = report_desc_ceiling;
		this.report_desc_imp_roofing = report_desc_imp_roofing;
		this.report_desc_trusses = report_desc_trusses;
		this.report_desc_economic_life = report_desc_economic_life;
		this.report_desc_effective_age = report_desc_effective_age;
		this.report_desc_imp_remain_life = report_desc_imp_remain_life;
		this.report_desc_occupants = report_desc_occupants;
		this.report_desc_owned_or_leased = report_desc_owned_or_leased;
		this.report_desc_imp_floor_area = report_desc_imp_floor_area;
		this.report_desc_confirmed_thru = report_desc_confirmed_thru;
		this.report_desc_observed_condition = report_desc_observed_condition;
		this.report_ownership_of_property = report_ownership_of_property;
		this.report_impsummary1_no_of_bedrooms = report_impsummary1_no_of_bedrooms;
		this.valrep_landimp_impsummary1_no_of_tb = valrep_landimp_impsummary1_no_of_tb;
		this.valrep_landimp_desc_stairs = valrep_landimp_desc_stairs;
	}

	// without int id
	public Land_Improvements_API_Imp_Details(String record_id,
			String report_impsummary1_no_of_floors,
			String report_impsummary1_building_desc,
			String report_impsummary1_erected_on_lot,
			String report_impsummary1_fa, String report_impsummary1_fa_per_td,
			String report_impsummary1_actual_utilization,
			String report_impsummary1_usage_declaration,
			String report_impsummary1_owner,
			String report_impsummary1_socialized_housing,
			String report_desc_property_type,
			String report_desc_foundation, String report_desc_columns_posts,
			String report_desc_beams, String report_desc_exterior_walls,
			String report_desc_interior_walls, String report_desc_imp_flooring,
			String report_desc_doors, String report_desc_imp_windows,
			String report_desc_ceiling, String report_desc_imp_roofing,
			String report_desc_trusses, String report_desc_economic_life,
			String report_desc_effective_age,
			String report_desc_imp_remain_life, String report_desc_occupants,
			String report_desc_owned_or_leased,
			String report_desc_imp_floor_area,
			String report_desc_confirmed_thru,
			String report_desc_observed_condition, String report_ownership_of_property,
											 String report_impsummary1_no_of_bedrooms,
											 String valrep_landimp_impsummary1_no_of_tb,
											 String valrep_landimp_desc_stairs) {
		this.record_id = record_id;
		this.report_impsummary1_no_of_floors = report_impsummary1_no_of_floors;
		this.report_impsummary1_building_desc = report_impsummary1_building_desc;
		this.report_impsummary1_erected_on_lot = report_impsummary1_erected_on_lot;
		this.report_impsummary1_fa = report_impsummary1_fa;
		this.report_impsummary1_fa_per_td = report_impsummary1_fa_per_td;
		this.report_impsummary1_actual_utilization = report_impsummary1_actual_utilization;
		this.report_impsummary1_usage_declaration = report_impsummary1_usage_declaration;
		this.report_impsummary1_owner = report_impsummary1_owner;
		this.report_impsummary1_socialized_housing = report_impsummary1_socialized_housing;
		this.report_desc_property_type = report_desc_property_type;
		this.report_desc_foundation = report_desc_foundation;
		this.report_desc_columns_posts = report_desc_columns_posts;
		this.report_desc_beams = report_desc_beams;
		this.report_desc_exterior_walls = report_desc_exterior_walls;
		this.report_desc_interior_walls = report_desc_interior_walls;
		this.report_desc_imp_flooring = report_desc_imp_flooring;
		this.report_desc_doors = report_desc_doors;
		this.report_desc_imp_windows = report_desc_imp_windows;
		this.report_desc_ceiling = report_desc_ceiling;
		this.report_desc_imp_roofing = report_desc_imp_roofing;
		this.report_desc_trusses = report_desc_trusses;
		this.report_desc_economic_life = report_desc_economic_life;
		this.report_desc_effective_age = report_desc_effective_age;
		this.report_desc_imp_remain_life = report_desc_imp_remain_life;
		this.report_desc_occupants = report_desc_occupants;
		this.report_desc_owned_or_leased = report_desc_owned_or_leased;
		this.report_desc_imp_floor_area = report_desc_imp_floor_area;
		this.report_desc_confirmed_thru = report_desc_confirmed_thru;
		this.report_desc_observed_condition = report_desc_observed_condition;
		this.report_ownership_of_property = report_ownership_of_property;
		this.report_impsummary1_no_of_bedrooms = report_impsummary1_no_of_bedrooms;
		this.valrep_landimp_impsummary1_no_of_tb = valrep_landimp_impsummary1_no_of_tb;
		this.valrep_landimp_desc_stairs = valrep_landimp_desc_stairs;
	}

	// without record_id
	public Land_Improvements_API_Imp_Details(
			String report_impsummary1_no_of_floors,
			String report_impsummary1_building_desc,
			String report_impsummary1_erected_on_lot,
			String report_impsummary1_fa, String report_impsummary1_fa_per_td,
			String report_impsummary1_actual_utilization,
			String report_impsummary1_usage_declaration,
			String report_impsummary1_owner, 
			String report_impsummary1_socialized_housing,
			String report_desc_property_type,
			String report_desc_foundation, String report_desc_columns_posts,
			String report_desc_beams, String report_desc_exterior_walls,
			String report_desc_interior_walls, String report_desc_imp_flooring,
			String report_desc_doors, String report_desc_imp_windows,
			String report_desc_ceiling, String report_desc_imp_roofing,
			String report_desc_trusses, String report_desc_economic_life,
			String report_desc_effective_age,
			String report_desc_imp_remain_life, String report_desc_occupants,
			String report_desc_owned_or_leased,
			String report_desc_imp_floor_area,
			String report_desc_confirmed_thru,
			String report_desc_observed_condition, String report_ownership_of_property,
			String report_impsummary1_no_of_bedrooms,
			String valrep_landimp_impsummary1_no_of_tb,
			String valrep_landimp_desc_stairs) {
		this.report_impsummary1_no_of_floors = report_impsummary1_no_of_floors;
		this.report_impsummary1_building_desc = report_impsummary1_building_desc;
		this.report_impsummary1_erected_on_lot = report_impsummary1_erected_on_lot;
		this.report_impsummary1_fa = report_impsummary1_fa;
		this.report_impsummary1_fa_per_td = report_impsummary1_fa_per_td;
		this.report_impsummary1_actual_utilization = report_impsummary1_actual_utilization;
		this.report_impsummary1_usage_declaration = report_impsummary1_usage_declaration;
		this.report_impsummary1_owner = report_impsummary1_owner;
		this.report_impsummary1_socialized_housing = report_impsummary1_socialized_housing;
		this.report_desc_property_type = report_desc_property_type;
		this.report_desc_foundation = report_desc_foundation;
		this.report_desc_columns_posts = report_desc_columns_posts;
		this.report_desc_beams = report_desc_beams;
		this.report_desc_exterior_walls = report_desc_exterior_walls;
		this.report_desc_interior_walls = report_desc_interior_walls;
		this.report_desc_imp_flooring = report_desc_imp_flooring;
		this.report_desc_doors = report_desc_doors;
		this.report_desc_imp_windows = report_desc_imp_windows;
		this.report_desc_ceiling = report_desc_ceiling;
		this.report_desc_imp_roofing = report_desc_imp_roofing;
		this.report_desc_trusses = report_desc_trusses;
		this.report_desc_economic_life = report_desc_economic_life;
		this.report_desc_effective_age = report_desc_effective_age;
		this.report_desc_imp_remain_life = report_desc_imp_remain_life;
		this.report_desc_occupants = report_desc_occupants;
		this.report_desc_owned_or_leased = report_desc_owned_or_leased;
		this.report_desc_imp_floor_area = report_desc_imp_floor_area;
		this.report_desc_confirmed_thru = report_desc_confirmed_thru;
		this.report_desc_observed_condition = report_desc_observed_condition;
		this.report_ownership_of_property = report_ownership_of_property;
		this.report_impsummary1_no_of_bedrooms = report_impsummary1_no_of_bedrooms;
		this.valrep_landimp_impsummary1_no_of_tb = valrep_landimp_impsummary1_no_of_tb;
		this.valrep_landimp_desc_stairs = valrep_landimp_desc_stairs;
	}

	// getting ID
	public int getID() {
		return this.imp_details_id;
	}

	// setting id
	public void setID(int imp_details_id) {
		this.imp_details_id = imp_details_id;
	}

	public String getrecord_id() {
		return this.record_id;
	}

	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}

	// getters
	public String getreport_impsummary1_no_of_floors() {
		return this.report_impsummary1_no_of_floors;
	}

	public String getreport_impsummary1_building_desc() {
		return this.report_impsummary1_building_desc;
	}

	public String getreport_impsummary1_erected_on_lot() {
		return this.report_impsummary1_erected_on_lot;
	}

	public String getreport_impsummary1_fa() {
		return this.report_impsummary1_fa;
	}

	public String getreport_impsummary1_fa_per_td() {
		return this.report_impsummary1_fa_per_td;
	}

	public String getreport_impsummary1_actual_utilization() {
		return this.report_impsummary1_actual_utilization;
	}

	public String getreport_impsummary1_usage_declaration() {
		return this.report_impsummary1_usage_declaration;
	}

	public String getreport_impsummary1_owner() {
		return this.report_impsummary1_owner;
	}

	public String getreport_impsummary1_socialized_housing() {
		return this.report_impsummary1_socialized_housing;
	}

	public String getreport_desc_property_type() {
		return this.report_desc_property_type;
	}

	public String getreport_desc_foundation() {
		return this.report_desc_foundation;
	}

	public String getreport_desc_columns_posts() {
		return this.report_desc_columns_posts;
	}

	public String getreport_desc_beams() {
		return this.report_desc_beams;
	}

	public String getreport_desc_exterior_walls() {
		return this.report_desc_exterior_walls;
	}

	public String getreport_desc_interior_walls() {
		return this.report_desc_interior_walls;
	}

	public String getreport_desc_imp_flooring() {
		return this.report_desc_imp_flooring;
	}

	public String getreport_desc_doors() {
		return this.report_desc_doors;
	}

	public String getreport_desc_imp_windows() {
		return this.report_desc_imp_windows;
	}

	public String getreport_desc_ceiling() {
		return this.report_desc_ceiling;
	}

	public String getreport_desc_imp_roofing() {
		return this.report_desc_imp_roofing;
	}

	public String getreport_desc_trusses() {
		return this.report_desc_trusses;
	}

	public String getreport_desc_economic_life() {
		return this.report_desc_economic_life;
	}

	public String getreport_desc_effective_age() {
		return this.report_desc_effective_age;
	}

	public String getreport_desc_imp_remain_life() {
		return this.report_desc_imp_remain_life;
	}

	public String getreport_desc_occupants() {
		return this.report_desc_occupants;
	}

	public String getreport_desc_owned_or_leased() {
		return this.report_desc_owned_or_leased;
	}

	public String getreport_desc_imp_floor_area() {
		return this.report_desc_imp_floor_area;
	}

	public String getreport_desc_confirmed_thru() {
		return this.report_desc_confirmed_thru;
	}

	public String getreport_desc_observed_condition() {
		return this.report_desc_observed_condition;
	}
	
	public String getreport_ownership_of_property() {
		return this.report_ownership_of_property;
	}

	public String getreport_impsummary1_no_of_bedrooms() {
		return this.report_impsummary1_no_of_bedrooms;
	}
	public String getvalrep_landimp_impsummary1_no_of_tb() {
		return this.valrep_landimp_impsummary1_no_of_tb;
	}
	public String getvalrep_landimp_desc_stairs() {
		return this.valrep_landimp_desc_stairs;
	}

	// setters
	public void setreport_impsummary1_no_of_floors(
			String report_impsummary1_no_of_floors) {
		this.report_impsummary1_no_of_floors = report_impsummary1_no_of_floors;
	}

	public void setreport_impsummary1_building_desc(
			String report_impsummary1_building_desc) {
		this.report_impsummary1_building_desc = report_impsummary1_building_desc;
	}

	public void setreport_impsummary1_erected_on_lot(
			String report_impsummary1_erected_on_lot) {
		this.report_impsummary1_erected_on_lot = report_impsummary1_erected_on_lot;
	}

	public void setreport_impsummary1_fa(String report_impsummary1_fa) {
		this.report_impsummary1_fa = report_impsummary1_fa;
	}

	public void setreport_impsummary1_fa_per_td(
			String report_impsummary1_fa_per_td) {
		this.report_impsummary1_fa_per_td = report_impsummary1_fa_per_td;
	}

	public void setreport_impsummary1_actual_utilization(
			String report_impsummary1_actual_utilization) {
		this.report_impsummary1_actual_utilization = report_impsummary1_actual_utilization;
	}

	public void setreport_impsummary1_usage_declaration(
			String report_impsummary1_usage_declaration) {
		this.report_impsummary1_usage_declaration = report_impsummary1_usage_declaration;
	}

	public void setreport_impsummary1_owner(String report_impsummary1_owner) {
		this.report_impsummary1_owner = report_impsummary1_owner;
	}

	public void setreport_impsummary1_socialized_housing(
			String report_impsummary1_socialized_housing) {
		this.report_impsummary1_socialized_housing = report_impsummary1_socialized_housing;
	}

	public void setreport_desc_property_type(String report_desc_property_type) {
		this.report_desc_property_type = report_desc_property_type;
	}

	public void setreport_desc_foundation(String report_desc_foundation) {
		this.report_desc_foundation = report_desc_foundation;
	}

	public void setreport_desc_columns_posts(String report_desc_columns_posts) {
		this.report_desc_columns_posts = report_desc_columns_posts;
	}

	public void setreport_desc_beams(String report_desc_beams) {
		this.report_desc_beams = report_desc_beams;
	}

	public void setreport_desc_exterior_walls(String report_desc_exterior_walls) {
		this.report_desc_exterior_walls = report_desc_exterior_walls;
	}

	public void setreport_desc_interior_walls(String report_desc_interior_walls) {
		this.report_desc_interior_walls = report_desc_interior_walls;
	}

	public void setreport_desc_imp_flooring(String report_desc_imp_flooring) {
		this.report_desc_imp_flooring = report_desc_imp_flooring;
	}

	public void setreport_desc_doors(String report_desc_doors) {
		this.report_desc_doors = report_desc_doors;
	}

	public void setreport_desc_imp_windows(String report_desc_imp_windows) {
		this.report_desc_imp_windows = report_desc_imp_windows;
	}

	public void setreport_desc_ceiling(String report_desc_ceiling) {
		this.report_desc_ceiling = report_desc_ceiling;
	}

	public void setreport_desc_imp_roofing(String report_desc_imp_roofing) {
		this.report_desc_imp_roofing = report_desc_imp_roofing;
	}

	public void setreport_desc_trusses(String report_desc_trusses) {
		this.report_desc_trusses = report_desc_trusses;
	}

	public void setreport_desc_economic_life(String report_desc_economic_life) {
		this.report_desc_economic_life = report_desc_economic_life;
	}

	public void setreport_desc_effective_age(String report_desc_effective_age) {
		this.report_desc_effective_age = report_desc_effective_age;
	}

	public void setreport_desc_imp_remain_life(
			String report_desc_imp_remain_life) {
		this.report_desc_imp_remain_life = report_desc_imp_remain_life;
	}

	public void setreport_desc_occupants(String report_desc_occupants) {
		this.report_desc_occupants = report_desc_occupants;
	}

	public void setreport_desc_owned_or_leased(
			String report_desc_owned_or_leased) {
		this.report_desc_owned_or_leased = report_desc_owned_or_leased;
	}

	public void setreport_desc_imp_floor_area(String report_desc_imp_floor_area) {
		this.report_desc_imp_floor_area = report_desc_imp_floor_area;
	}

	public void setreport_desc_confirmed_thru(String report_desc_confirmed_thru) {
		this.report_desc_confirmed_thru = report_desc_confirmed_thru;
	}

	public void setreport_desc_observed_condition(
			String report_desc_observed_condition) {
		this.report_desc_observed_condition = report_desc_observed_condition;
	}
	
	public void setreport_ownership_of_property(
			String report_ownership_of_property) {
		this.report_ownership_of_property = report_ownership_of_property;
	}
	

	public void setreport_impsummary1_no_of_bedrooms(
			String report_impsummary1_no_of_bedrooms) {
		this.report_impsummary1_no_of_bedrooms = report_impsummary1_no_of_bedrooms;
	}

	public void setvalrep_landimp_impsummary1_no_of_tb(
			String valrep_landimp_impsummary1_no_of_tb) {
		this.valrep_landimp_impsummary1_no_of_tb = valrep_landimp_impsummary1_no_of_tb;
	}

	public void setvalrep_landimp_desc_stairs(
			String valrep_landimp_desc_stairs) {
		this.valrep_landimp_desc_stairs = valrep_landimp_desc_stairs;
	}
}
