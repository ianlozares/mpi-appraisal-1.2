package com.gds.appraisalmaybank.database2;


public class Condo_API_Valuation {
	// private variables
	int id;
	String record_id;
	String report_value_property_type;
	String report_value_cct_no;
	String report_value_unit_no;
	String report_value_floor_area;
	String report_value_deduction;
	String report_value_net_area;
	String report_value_unit_value;
	String report_value_appraised_value;
	
	// Empty constructor
	public Condo_API_Valuation(){
		
	}
	
	public Condo_API_Valuation(String record_id) {
		this.record_id = record_id;
	}
	
	public Condo_API_Valuation(int id, String record_id,
			String report_value_property_type, String report_value_cct_no,
			String report_value_unit_no, String report_value_floor_area,
			String report_value_deduction, String report_value_net_area,
			String report_value_unit_value, String report_value_appraised_value) {
		this.id = id;
		this.record_id = record_id;
		this.report_value_property_type = report_value_property_type;
		this.report_value_cct_no = report_value_cct_no;
		this.report_value_unit_no = report_value_unit_no;
		this.report_value_floor_area = report_value_floor_area;
		this.report_value_deduction = report_value_deduction;
		this.report_value_net_area = report_value_net_area;
		this.report_value_unit_value = report_value_unit_value;
		this.report_value_appraised_value = report_value_appraised_value;
	}
	
	public Condo_API_Valuation(String record_id,
			String report_value_property_type, String report_value_cct_no,
			String report_value_unit_no, String report_value_floor_area,
			String report_value_deduction, String report_value_net_area,
			String report_value_unit_value, String report_value_appraised_value) {
		this.record_id = record_id;
		this.report_value_property_type = report_value_property_type;
		this.report_value_cct_no = report_value_cct_no;
		this.report_value_unit_no = report_value_unit_no;
		this.report_value_floor_area = report_value_floor_area;
		this.report_value_deduction = report_value_deduction;
		this.report_value_net_area = report_value_net_area;
		this.report_value_unit_value = report_value_unit_value;
		this.report_value_appraised_value = report_value_appraised_value;
	}
	
	public Condo_API_Valuation(String report_value_property_type,
			String report_value_cct_no, String report_value_unit_no,
			String report_value_floor_area, String report_value_deduction,
			String report_value_net_area, String report_value_unit_value,
			String report_value_appraised_value) {
		this.report_value_property_type = report_value_property_type;
		this.report_value_cct_no = report_value_cct_no;
		this.report_value_unit_no = report_value_unit_no;
		this.report_value_floor_area = report_value_floor_area;
		this.report_value_deduction = report_value_deduction;
		this.report_value_net_area = report_value_net_area;
		this.report_value_unit_value = report_value_unit_value;
		this.report_value_appraised_value = report_value_appraised_value;
	}
	
	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	public String getrecord_id() {
		return this.record_id;
	}

	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}
	
	//getters
	public String getreport_value_property_type() {
		return this.report_value_property_type;
	}

	public String getreport_value_cct_no() {
		return this.report_value_cct_no;
	}

	public String getreport_value_unit_no() {
		return this.report_value_unit_no;
	}

	public String getreport_value_floor_area() {
		return this.report_value_floor_area;
	}

	public String getreport_value_deduction() {
		return this.report_value_deduction;
	}

	public String getreport_value_net_area() {
		return this.report_value_net_area;
	}

	public String getreport_value_unit_value() {
		return this.report_value_unit_value;
	}

	public String getreport_value_appraised_value() {
		return this.report_value_appraised_value;
	}
	
	//setters
	public void setreport_value_property_type(String report_value_property_type) {
		this.report_value_property_type = report_value_property_type;
	}

	public void setreport_value_cct_no(String report_value_cct_no) {
		this.report_value_cct_no = report_value_cct_no;
	}

	public void setreport_value_unit_no(String report_value_unit_no) {
		this.report_value_unit_no = report_value_unit_no;
	}

	public void setreport_value_floor_area(String report_value_floor_area) {
		this.report_value_floor_area = report_value_floor_area;
	}

	public void setreport_value_deduction(String report_value_deduction) {
		this.report_value_deduction = report_value_deduction;
	}

	public void setreport_value_net_area(String report_value_net_area) {
		this.report_value_net_area = report_value_net_area;
	}

	public void setreport_value_unit_value(String report_value_unit_value) {
		this.report_value_unit_value = report_value_unit_value;
	}

	public void setreport_value_appraised_value(String report_value_appraised_value) {
		this.report_value_appraised_value = report_value_appraised_value;
	}
	
}
