package com.gds.appraisalmaybank.database2;

public class Land_Improvements_API_Cost_Valuation {
	//private variables
	int id;
	String record_id;
	String report_imp_value_description;
	String report_imp_value_total_area;
	String report_imp_value_total_reproduction_cost;
	String report_imp_value_depreciation;
	String report_imp_value_less_reproduction;
	String report_imp_value_depreciated_value;
	String valrep_landimp_imp_value_phys_cur_unit_value;
	String valrep_landimp_imp_value_phys_cur_amt;
	String valrep_landimp_imp_value_func_cur_unit_value;
	String valrep_landimp_imp_value_func_cur_amt;
	String valrep_landimp_imp_value_less_cur;
	
	// Empty constructor
	public Land_Improvements_API_Cost_Valuation(){
		
	}
	
	public Land_Improvements_API_Cost_Valuation(String record_id) {
		this.record_id = record_id;
	}
	
	public Land_Improvements_API_Cost_Valuation(int id, String record_id,
			String report_imp_value_description, String report_imp_value_total_area,
			String report_imp_value_total_reproduction_cost, String report_imp_value_depreciation,
			String report_imp_value_less_reproduction, String report_imp_value_depreciated_value,
			String valrep_landimp_imp_value_phys_cur_unit_value,  String valrep_landimp_imp_value_phys_cur_amt,
			String valrep_landimp_imp_value_func_cur_unit_value,  String valrep_landimp_imp_value_func_cur_amt,
			String valrep_landimp_imp_value_less_cur) {
		this.id = id;
		this.record_id = record_id;
		this.report_imp_value_description = report_imp_value_description;
		this.report_imp_value_total_area = report_imp_value_total_area;
		this.report_imp_value_total_reproduction_cost = report_imp_value_total_reproduction_cost;
		this.report_imp_value_depreciation = report_imp_value_depreciation;
		this.report_imp_value_less_reproduction = report_imp_value_less_reproduction;
		this.report_imp_value_depreciated_value = report_imp_value_depreciated_value;

		this.valrep_landimp_imp_value_phys_cur_unit_value = valrep_landimp_imp_value_phys_cur_unit_value;
		this.valrep_landimp_imp_value_phys_cur_amt = valrep_landimp_imp_value_phys_cur_amt;
		this.valrep_landimp_imp_value_func_cur_unit_value = valrep_landimp_imp_value_func_cur_unit_value;
		this.valrep_landimp_imp_value_func_cur_amt = valrep_landimp_imp_value_func_cur_amt;
		this.valrep_landimp_imp_value_less_cur = valrep_landimp_imp_value_less_cur;
	}

	public Land_Improvements_API_Cost_Valuation(String record_id,
			String report_imp_value_description, String report_imp_value_total_area,
			String report_imp_value_total_reproduction_cost, String report_imp_value_depreciation,
			String report_imp_value_less_reproduction, String report_imp_value_depreciated_value,
												String valrep_landimp_imp_value_phys_cur_unit_value,  String valrep_landimp_imp_value_phys_cur_amt,
												String valrep_landimp_imp_value_func_cur_unit_value,  String valrep_landimp_imp_value_func_cur_amt,
												String valrep_landimp_imp_value_less_cur) {
		this.record_id = record_id;
		this.report_imp_value_description = report_imp_value_description;
		this.report_imp_value_total_area = report_imp_value_total_area;
		this.report_imp_value_total_reproduction_cost = report_imp_value_total_reproduction_cost;
		this.report_imp_value_depreciation = report_imp_value_depreciation;
		this.report_imp_value_less_reproduction = report_imp_value_less_reproduction;
		this.report_imp_value_depreciated_value = report_imp_value_depreciated_value;

		this.valrep_landimp_imp_value_phys_cur_unit_value = valrep_landimp_imp_value_phys_cur_unit_value;
		this.valrep_landimp_imp_value_phys_cur_amt = valrep_landimp_imp_value_phys_cur_amt;
		this.valrep_landimp_imp_value_func_cur_unit_value = valrep_landimp_imp_value_func_cur_unit_value;
		this.valrep_landimp_imp_value_func_cur_amt = valrep_landimp_imp_value_func_cur_amt;
		this.valrep_landimp_imp_value_less_cur = valrep_landimp_imp_value_less_cur;
	}

	public Land_Improvements_API_Cost_Valuation(String report_imp_value_description,
			String report_imp_value_total_area, String report_imp_value_total_reproduction_cost,
			String report_imp_value_depreciation, String report_imp_value_less_reproduction,
			String report_imp_value_depreciated_value,
												String valrep_landimp_imp_value_phys_cur_unit_value,  String valrep_landimp_imp_value_phys_cur_amt,
												String valrep_landimp_imp_value_func_cur_unit_value,  String valrep_landimp_imp_value_func_cur_amt,
												String valrep_landimp_imp_value_less_cur) {
		this.report_imp_value_description = report_imp_value_description;
		this.report_imp_value_total_area = report_imp_value_total_area;
		this.report_imp_value_total_reproduction_cost = report_imp_value_total_reproduction_cost;
		this.report_imp_value_depreciation = report_imp_value_depreciation;
		this.report_imp_value_less_reproduction = report_imp_value_less_reproduction;
		this.report_imp_value_depreciated_value = report_imp_value_depreciated_value;

		this.valrep_landimp_imp_value_phys_cur_unit_value = valrep_landimp_imp_value_phys_cur_unit_value;
		this.valrep_landimp_imp_value_phys_cur_amt = valrep_landimp_imp_value_phys_cur_amt;
		this.valrep_landimp_imp_value_func_cur_unit_value = valrep_landimp_imp_value_func_cur_unit_value;
		this.valrep_landimp_imp_value_func_cur_amt = valrep_landimp_imp_value_func_cur_amt;
		this.valrep_landimp_imp_value_less_cur = valrep_landimp_imp_value_less_cur;
	}
	
	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	public String getrecord_id() {
		return this.record_id;
	}

	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}
	
	//getters
	public String getreport_imp_value_description() {
		return this.report_imp_value_description;
	}

	public String getreport_imp_value_total_area() {
		return this.report_imp_value_total_area;
	}

	public String getreport_imp_value_total_reproduction_cost() {
		return this.report_imp_value_total_reproduction_cost;
	}

	public String getreport_imp_value_depreciation() {
		return this.report_imp_value_depreciation;
	}

	public String getreport_imp_value_less_reproduction() {
		return this.report_imp_value_less_reproduction;
	}

	public String getreport_imp_value_depreciated_value() {
		return this.report_imp_value_depreciated_value;
	}

	public String getvalrep_landimp_imp_value_phys_cur_unit_value() {
		return this.valrep_landimp_imp_value_phys_cur_unit_value;
	}

	public String getvalrep_landimp_imp_value_phys_cur_amt() {
		return this.valrep_landimp_imp_value_phys_cur_amt;
	}

	public String getvalrep_landimp_imp_value_func_cur_unit_value() {
		return this.valrep_landimp_imp_value_func_cur_unit_value;
	}

	public String getvalrep_landimp_imp_value_func_cur_amt() {
		return this.valrep_landimp_imp_value_func_cur_amt;
	}

	public String getvalrep_landimp_imp_value_less_cur() {
		return this.valrep_landimp_imp_value_less_cur;
	}

	//setters
	public void setreport_imp_value_description(String report_imp_value_description) {
		this.report_imp_value_description = report_imp_value_description;
	}

	public void setreport_imp_value_total_area(String report_imp_value_total_area) {
		this.report_imp_value_total_area = report_imp_value_total_area;
	}

	public void setreport_imp_value_total_reproduction_cost(
			String report_imp_value_total_reproduction_cost) {
		this.report_imp_value_total_reproduction_cost = report_imp_value_total_reproduction_cost;
	}

	public void setreport_imp_value_depreciation(String report_imp_value_depreciation) {
		this.report_imp_value_depreciation = report_imp_value_depreciation;
	}

	public void setreport_imp_value_less_reproduction(String report_imp_value_less_reproduction) {
		this.report_imp_value_less_reproduction = report_imp_value_less_reproduction;
	}

	public void setreport_imp_value_depreciated_value(String report_imp_value_depreciated_value) {
		this.report_imp_value_depreciated_value = report_imp_value_depreciated_value;
	}

	public void setvalrep_landimp_imp_value_phys_cur_unit_value(String valrep_landimp_imp_value_phys_cur_unit_value) {
		this.valrep_landimp_imp_value_phys_cur_unit_value = valrep_landimp_imp_value_phys_cur_unit_value;
	}

	public void setvalrep_landimp_imp_value_phys_cur_amt(String valrep_landimp_imp_value_phys_cur_amt) {
		this.valrep_landimp_imp_value_phys_cur_amt = valrep_landimp_imp_value_phys_cur_amt;
	}

	public void setvalrep_landimp_imp_value_func_cur_unit_value(String valrep_landimp_imp_value_func_cur_unit_value) {
		this.valrep_landimp_imp_value_func_cur_unit_value = valrep_landimp_imp_value_func_cur_unit_value;
	}

	public void setvalrep_landimp_imp_value_func_cur_amt(String valrep_landimp_imp_value_func_cur_amt) {
		this.valrep_landimp_imp_value_func_cur_amt = valrep_landimp_imp_value_func_cur_amt;
	}

	public void setvalrep_landimp_imp_value_less_cur(String valrep_landimp_imp_value_less_cur) {
		this.valrep_landimp_imp_value_less_cur = valrep_landimp_imp_value_less_cur;
	}
}
