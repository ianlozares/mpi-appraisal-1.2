package com.gds.appraisalmaybank.database2;

public class Condo_API_RDPS {
	// private variables
	int id;
	String record_id;
	String report_prev_app_name;
	String report_prev_app_location;
	String report_prev_app_area;
	String report_prev_app_value;
	String report_prev_app_date;
	
	// Empty constructor
	public Condo_API_RDPS(){
		
	}
	
	public Condo_API_RDPS(String record_id) {
		this.record_id = record_id;
	}
	
	public Condo_API_RDPS(int id,
			String record_id, String report_prev_app_name,
			String report_prev_app_location, String report_prev_app_area,
			String report_prev_app_value, String report_prev_app_date) {		
		this.id = id;
		this.record_id = record_id;
		this.report_prev_app_name = report_prev_app_name;
		this.report_prev_app_location = report_prev_app_location;
		this.report_prev_app_area = report_prev_app_area;
		this.report_prev_app_value = report_prev_app_value;
		this.report_prev_app_date = report_prev_app_date;
	}
	
	public Condo_API_RDPS(String record_id,
			String report_prev_app_name, String report_prev_app_location,
			String report_prev_app_area, String report_prev_app_value,
			String report_prev_app_date) {
		this.record_id = record_id;
		this.report_prev_app_name = report_prev_app_name;
		this.report_prev_app_location = report_prev_app_location;
		this.report_prev_app_area = report_prev_app_area;
		this.report_prev_app_value = report_prev_app_value;
		this.report_prev_app_date = report_prev_app_date;
	}
	
	public Condo_API_RDPS(String report_prev_app_name,
			String report_prev_app_location, String report_prev_app_area,
			String report_prev_app_value, String report_prev_app_date) {
		this.report_prev_app_name = report_prev_app_name;
		this.report_prev_app_location = report_prev_app_location;
		this.report_prev_app_area = report_prev_app_area;
		this.report_prev_app_value = report_prev_app_value;
		this.report_prev_app_date = report_prev_app_date;
	}
	
	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}
	
	public String getrecord_id() {
		return this.record_id;
	}

	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}
	
	// getters
	public String getreport_prev_app_name() {
		return this.report_prev_app_name;
	}

	public String getreport_prev_app_location() {
		return this.report_prev_app_location;
	}

	public String getreport_prev_app_area() {
		return this.report_prev_app_area;
	}

	public String getreport_prev_app_value() {
		return this.report_prev_app_value;
	}

	public String getreport_prev_app_date() {
		return this.report_prev_app_date;
	}
	
	//setters
	public void setreport_prev_app_name(String report_prev_app_name) {
		this.report_prev_app_name = report_prev_app_name;
	}

	public void setreport_prev_app_location(String report_prev_app_location) {
		this.report_prev_app_location = report_prev_app_location;
	}

	public void setreport_prev_app_area(String report_prev_app_area) {
		this.report_prev_app_area = report_prev_app_area;
	}

	public void setreport_prev_app_value(String report_prev_app_value) {
		this.report_prev_app_value = report_prev_app_value;
	}

	public void setreport_prev_app_date(String report_prev_app_date) {
		this.report_prev_app_date = report_prev_app_date;
	}
}