package com.gds.appraisalmaybank.database2;

public class Condo_API_Unit_Details {
	// private variables
	int unit_details_id;
	String record_id;
	String report_unit_description;
	String report_unit_no_of_storeys;
	String report_unit_unit_no;
	String report_unit_floor_location;
	String report_unit_floor_area;
	String report_unit_interior_flooring;
	String report_unit_interior_partitions;
	String report_unit_interior_doors;
	String report_unit_interior_windows;
	String report_unit_interior_ceiling;
	String report_unit_features;
	String report_unit_occupants;
	String report_unit_owned_or_leased;
	String report_unit_observed_condition;
	String report_unit_ownership_of_property;
	String report_unit_socialized_housing;
	String report_unit_type_of_property;
	String report_no_of_bedrooms;
	//ADDED From IAN
	String report_bldg_age;
	String report_developer_name;
	
	// Empty constructor
	public Condo_API_Unit_Details() {

	}

	public Condo_API_Unit_Details(String record_id) {
		this.record_id = record_id;
	}
	
	public Condo_API_Unit_Details(int imp_details_id,
			String record_id,
			String report_unit_description,
			String report_unit_no_of_storeys,
			String report_unit_unit_no,
			String report_unit_floor_location,
			String report_unit_floor_area,
			String report_unit_interior_flooring,
			String report_unit_interior_partitions,
			String report_unit_interior_doors,
			String report_unit_interior_windows,
			String report_unit_interior_ceiling,
			String report_unit_features,
			String report_unit_occupants,
			String report_unit_owned_or_leased,
			String report_unit_observed_condition,
			String report_unit_ownership_of_property,
			String report_unit_socialized_housing,
			String report_unit_type_of_property,
			String report_no_of_bedrooms,
								  //Added From IAN
								  String report_bldg_age,
								  String report_developer_name) {
		this.unit_details_id = imp_details_id;
		this.record_id = record_id;
		this.report_unit_description = report_unit_description;
		this.report_unit_no_of_storeys = report_unit_no_of_storeys;
		this.report_unit_unit_no = report_unit_unit_no;
		this.report_unit_floor_location = report_unit_floor_location;
		this.report_unit_floor_area = report_unit_floor_area;
		this.report_unit_interior_flooring = report_unit_interior_flooring;
		this.report_unit_interior_partitions = report_unit_interior_partitions;
		this.report_unit_interior_doors = report_unit_interior_doors;
		this.report_unit_interior_windows = report_unit_interior_windows;
		this.report_unit_interior_ceiling = report_unit_interior_ceiling;
		this.report_unit_features = report_unit_features;
		this.report_unit_occupants = report_unit_occupants;
		this.report_unit_owned_or_leased = report_unit_owned_or_leased;
		this.report_unit_observed_condition = report_unit_observed_condition;
		this.report_unit_ownership_of_property = report_unit_ownership_of_property;
		this.report_unit_socialized_housing = report_unit_socialized_housing;
		this.report_unit_type_of_property = report_unit_type_of_property;
		this.report_no_of_bedrooms = report_no_of_bedrooms;
		//Added From IAN
		this.report_bldg_age = report_bldg_age;
		this.report_developer_name = report_developer_name;
	}
	
	public Condo_API_Unit_Details(String record_id,
			String report_unit_description,
			String report_unit_no_of_storeys,
			String report_unit_unit_no,
			String report_unit_floor_location,
			String report_unit_floor_area,
			String report_unit_interior_flooring,
			String report_unit_interior_partitions,
			String report_unit_interior_doors,
			String report_unit_interior_windows,
			String report_unit_interior_ceiling,
			String report_unit_features,
			String report_unit_occupants,
			String report_unit_owned_or_leased,
			String report_unit_observed_condition,
			String report_unit_ownership_of_property,
			String report_unit_socialized_housing,
			String report_unit_type_of_property,
			String report_no_of_bedrooms,
								  //ADDED From IAN
								  String report_bldg_age,
								  String report_developer_name) {
		this.record_id = record_id;
		this.report_unit_description = report_unit_description;
		this.report_unit_no_of_storeys = report_unit_no_of_storeys;
		this.report_unit_unit_no = report_unit_unit_no;
		this.report_unit_floor_location = report_unit_floor_location;
		this.report_unit_floor_area = report_unit_floor_area;
		this.report_unit_interior_flooring = report_unit_interior_flooring;
		this.report_unit_interior_partitions = report_unit_interior_partitions;
		this.report_unit_interior_doors = report_unit_interior_doors;
		this.report_unit_interior_windows = report_unit_interior_windows;
		this.report_unit_interior_ceiling = report_unit_interior_ceiling;
		this.report_unit_features = report_unit_features;
		this.report_unit_occupants = report_unit_occupants;
		this.report_unit_owned_or_leased = report_unit_owned_or_leased;
		this.report_unit_observed_condition = report_unit_observed_condition;
		this.report_unit_ownership_of_property = report_unit_ownership_of_property;
		this.report_unit_socialized_housing = report_unit_socialized_housing;
		this.report_unit_type_of_property = report_unit_type_of_property;
		this.report_no_of_bedrooms = report_no_of_bedrooms;
		//Added From IAN
		this.report_bldg_age = report_bldg_age;
		this.report_developer_name = report_developer_name;
	}
	
	public Condo_API_Unit_Details(String report_unit_description,
			String report_unit_no_of_storeys,
			String report_unit_unit_no,
			String report_unit_floor_location,
			String report_unit_floor_area,
			String report_unit_interior_flooring,
			String report_unit_interior_partitions,
			String report_unit_interior_doors,
			String report_unit_interior_windows,
			String report_unit_interior_ceiling,
			String report_unit_features,
			String report_unit_occupants,
			String report_unit_owned_or_leased,
			String report_unit_observed_condition,
			String report_unit_ownership_of_property,
			String report_unit_socialized_housing,
			String report_unit_type_of_property,
			String report_no_of_bedrooms,
								  //ADDED From IAN
								  String report_bldg_age,
										  String report_developer_name) {
		this.report_unit_description = report_unit_description;
		this.report_unit_no_of_storeys = report_unit_no_of_storeys;
		this.report_unit_unit_no = report_unit_unit_no;
		this.report_unit_floor_location = report_unit_floor_location;
		this.report_unit_floor_area = report_unit_floor_area;
		this.report_unit_interior_flooring = report_unit_interior_flooring;
		this.report_unit_interior_partitions = report_unit_interior_partitions;
		this.report_unit_interior_doors = report_unit_interior_doors;
		this.report_unit_interior_windows = report_unit_interior_windows;
		this.report_unit_interior_ceiling = report_unit_interior_ceiling;
		this.report_unit_features = report_unit_features;
		this.report_unit_occupants = report_unit_occupants;
		this.report_unit_owned_or_leased = report_unit_owned_or_leased;
		this.report_unit_observed_condition = report_unit_observed_condition;
		this.report_unit_ownership_of_property = report_unit_ownership_of_property;
		this.report_unit_socialized_housing = report_unit_socialized_housing;
		this.report_unit_type_of_property = report_unit_type_of_property;
		this.report_no_of_bedrooms = report_no_of_bedrooms;
		//ADDED From IAN
		this.report_bldg_age = report_bldg_age;
		this.report_developer_name = report_developer_name;
	}
	
	// getting ID
	public int getID() {
		return this.unit_details_id;
	}

	// setting id
	public void setID(int imp_details_id) {
		this.unit_details_id = imp_details_id;
	}

	public String getrecord_id() {
		return this.record_id;
	}

	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}

	// getters
	public String getreport_unit_description() {
		return this.report_unit_description;
	}

	public String getreport_unit_no_of_storeys() {
		return this.report_unit_no_of_storeys;
	}

	public String getreport_unit_unit_no() {
		return this.report_unit_unit_no;
	}

	public String getreport_unit_floor_location() {
		return this.report_unit_floor_location;
	}

	public String getreport_unit_floor_area() {
		return this.report_unit_floor_area;
	}

	public String getreport_unit_interior_flooring() {
		return this.report_unit_interior_flooring;
	}

	public String getreport_unit_interior_partitions() {
		return this.report_unit_interior_partitions;
	}

	public String getreport_unit_interior_doors() {
		return this.report_unit_interior_doors;
	}

	public String getreport_unit_interior_windows() {
		return this.report_unit_interior_windows;
	}

	public String getreport_unit_interior_ceiling() {
		return this.report_unit_interior_ceiling;
	}

	public String getreport_unit_features() {
		return this.report_unit_features;
	}

	public String getreport_unit_occupants() {
		return this.report_unit_occupants;
	}

	public String getreport_unit_owned_or_leased() {
		return this.report_unit_owned_or_leased;
	}

	public String getreport_unit_observed_condition() {
		return this.report_unit_observed_condition;
	}

	public String getreport_unit_ownership_of_property() {
		return this.report_unit_ownership_of_property;
	}

	public String getreport_unit_socialized_housing() {
		return this.report_unit_socialized_housing;
	}

	public String getreport_unit_type_of_property() {
		return this.report_unit_type_of_property;
	}
	
	public String getreport_no_of_bedrooms() {
		return this.report_no_of_bedrooms;
	}

	//ADDED From IAN
	public String getreport_bldg_age() {
		return this.report_bldg_age;
	}

	public String getreport_developer_name() {
		return this.report_developer_name;
	}


	//setters
	public void setreport_unit_description(String report_unit_description) {
		this.report_unit_description = report_unit_description;
	}

	public void setreport_unit_no_of_storeys(String report_unit_no_of_storeys) {
		this.report_unit_no_of_storeys = report_unit_no_of_storeys;
	}

	public void setreport_unit_unit_no(String report_unit_unit_no) {
		this.report_unit_unit_no = report_unit_unit_no;
	}

	public void setreport_unit_floor_location(String report_unit_floor_location) {
		this.report_unit_floor_location = report_unit_floor_location;
	}

	public void setreport_unit_floor_area(String report_unit_floor_area) {
		this.report_unit_floor_area = report_unit_floor_area;
	}

	public void setreport_unit_interior_flooring(String report_unit_interior_flooring) {
		this.report_unit_interior_flooring = report_unit_interior_flooring;
	}

	public void setreport_unit_interior_partitions(String report_unit_interior_partitions) {
		this.report_unit_interior_partitions = report_unit_interior_partitions;
	}

	public void setreport_unit_interior_doors(String report_unit_interior_doors) {
		this.report_unit_interior_doors = report_unit_interior_doors;
	}

	public void setreport_unit_interior_windows(String report_unit_interior_windows) {
		this.report_unit_interior_windows = report_unit_interior_windows;
	}

	public void setreport_unit_interior_ceiling(String report_unit_interior_ceiling) {
		this.report_unit_interior_ceiling = report_unit_interior_ceiling;
	}

	public void setreport_unit_features(String report_unit_features) {
		this.report_unit_features = report_unit_features;
	}

	public void setreport_unit_occupants(String report_unit_occupants) {
		this.report_unit_occupants = report_unit_occupants;
	}

	public void setreport_unit_owned_or_leased(String report_unit_owned_or_leased) {
		this.report_unit_owned_or_leased = report_unit_owned_or_leased;
	}

	public void setreport_unit_observed_condition(String report_unit_observed_condition) {
		this.report_unit_observed_condition = report_unit_observed_condition;
	}

	public void setreport_unit_ownership_of_property(String report_unit_ownership_of_property) {
		this.report_unit_ownership_of_property = report_unit_ownership_of_property;
	}

	public void setreport_unit_socialized_housing(String report_unit_socialized_housing) {
		this.report_unit_socialized_housing = report_unit_socialized_housing;
	}

	public void setreport_unit_type_of_property(String report_unit_type_of_property) {
		this.report_unit_type_of_property = report_unit_type_of_property;
	}
	
	public void setreport_no_of_bedrooms(String report_no_of_bedrooms) {
		this.report_no_of_bedrooms = report_no_of_bedrooms;
	}

	//ADDED From IAN

	public void setreport_bldg_age(String report_bldg_age) {
		this.report_bldg_age = report_bldg_age;
	}

	public void setreport_developer_name(String report_developer_name) {
		this.report_developer_name = report_developer_name;
	}

}
