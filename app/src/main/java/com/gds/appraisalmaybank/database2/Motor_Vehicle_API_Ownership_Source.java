package com.gds.appraisalmaybank.database2;

public class Motor_Vehicle_API_Ownership_Source {
	//private variables
	int id;
	String record_id;
	String report_source_mileage;
	String report_source_range_max;
	String report_source_range_min;
	String report_source_source;
	String report_source_vehicle_type;
	
	public Motor_Vehicle_API_Ownership_Source() {

	}
	
	public Motor_Vehicle_API_Ownership_Source(int id, String record_id,
			String report_source_mileage, String report_source_range_min,
			String report_source_range_max, String report_source_source,
			String report_source_vehicle_type) {
		this.id = id;
		this.record_id = record_id;
		this.report_source_mileage = report_source_mileage;
		this.report_source_range_min = report_source_range_min;
		this.report_source_range_max = report_source_range_max;
		this.report_source_source = report_source_source;
		this.report_source_vehicle_type = report_source_vehicle_type;
	}
	
	public Motor_Vehicle_API_Ownership_Source(String record_id,
			String report_source_mileage, String report_source_range_min,
			String report_source_range_max, String report_source_source,
			String report_source_vehicle_type) {
		this.record_id = record_id;
		this.report_source_mileage = report_source_mileage;
		this.report_source_range_min = report_source_range_min;
		this.report_source_range_max = report_source_range_max;
		this.report_source_source = report_source_source;
		this.report_source_vehicle_type = report_source_vehicle_type;
	}
	
	public Motor_Vehicle_API_Ownership_Source(
			String report_source_mileage, String report_source_range_min,
			String report_source_range_max, String report_source_source,
			String report_source_vehicle_type) {
		this.report_source_mileage = report_source_mileage;
		this.report_source_range_min = report_source_range_min;
		this.report_source_range_max = report_source_range_max;
		this.report_source_source = report_source_source;
		this.report_source_vehicle_type = report_source_vehicle_type;
	}
	
	// getting ID
	public int getID() {
		return this.id;
	}

	// setting id
	public void setID(int id) {
		this.id = id;
	}

	// getting record_id
	public String getrecord_id() {
		return this.record_id;
	}

	public void setrecord_id(String record_id) {
		this.record_id = record_id;
	}
	
	//getters
	public String getreport_source_mileage() {
		return this.report_source_mileage;
	}

	public String getreport_source_range_min() {
		return this.report_source_range_min;
	}

	public String getreport_source_range_max() {
		return this.report_source_range_max;
	}

	public String getreport_source_source() {
		return this.report_source_source;
	}

	public String getreport_source_vehicle_type() {
		return this.report_source_vehicle_type;
	}
	
	//setters
	public void setreport_source_mileage(String report_source_mileage) {
		this.report_source_mileage = report_source_mileage;
	}

	public void setreport_source_range_min(String report_source_range_min) {
		this.report_source_range_min = report_source_range_min;
	}

	public void setreport_source_range_max(String report_source_range_max) {
		this.report_source_range_max = report_source_range_max;
	}

	public void setreport_source_source(String report_source_source) {
		this.report_source_source = report_source_source;
	}

	public void setreport_source_vehicle_type(String report_source_vehicle_type) {
		this.report_source_vehicle_type = report_source_vehicle_type;
	}
}
