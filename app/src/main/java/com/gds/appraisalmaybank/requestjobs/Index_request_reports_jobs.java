package com.gds.appraisalmaybank.requestjobs;

import android.app.Dialog;
import android.app.TabActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.TextView;

import com.gds.appraisalmaybank.attachments.Index_Attachments;
import com.gds.appraisalmaybank.database_new.DatabaseHandler_New;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

public class Index_request_reports_jobs extends TabActivity implements TabHost.OnTabChangeListener {
	SharedPreferences appPrefs;
	SharedPreferences.Editor appPrefsEditor;
	String pref_username, pref_user_roles;
	GDS_methods gds = new GDS_methods();
	Session_Timer st = new Session_Timer(this);
	WifiManager wifiManager;
//	WifiLock lock;
	TabHost tabHost;

	String active_count="0",acceptance_count,rework_count,submitted_count,tab_num;
//	DatabaseHandler2 db2;// = new DatabaseHandler2(this);
	DatabaseHandler_New db_new;// = new DatabaseHandler2(this);
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.index_request_reports_jobs);
		st.resetDisconnectTimer();
		appPrefs = getSharedPreferences("preference", 0);
		appPrefsEditor = appPrefs.edit();
		pref_username = appPrefs.getString("username", "");
		pref_user_roles = appPrefs.getString("user_roles", "");
		acceptance_count = appPrefs.getString("accepted_job_count","0");
		rework_count = appPrefs.getString("rework_job_count","0");
		tab_num=appPrefs.getString("tab","0");
		Log.e("tab",tab_num);
		//active_count = appPrefs.getString("active_job_count","0");

//		db2 = new DatabaseHandler2(this);
		db_new = new DatabaseHandler_New(this);

		if(!gds.nullCheck2(db_new.getRecord("fname","",new String[]{},"tbl_report_accepted_jobs").get(0)).contentEquals("0")){
			active_count = gds.nullCheck2(""+db_new.getRecord("id","",new String[]{},"tbl_report_accepted_jobs").size());
		}else{
			active_count = "0";
		}
		Log.e("active_count",active_count);

		 // The activity TabHost
		tabHost = getTabHost();
		TabHost.TabSpec spec; // Reusable TabSpec for each tab
		Intent intent; // Reusable Intent for each tab
		Resources res = getResources(); // Resource object to get Drawables


		// Create an Intent to launch an Activity for the tab (to be reused)
		intent = new Intent().setClass(this, Index_request_report.class);
		// Initialize a TabSpec for each tab and add it to the TabHost
		spec = tabHost.newTabSpec("tabOne");
		spec.setContent(intent);
		spec.setIndicator("Active Jobs ("+active_count+")", res.getDrawable(R.drawable.icon1));
		tabHost.addTab(spec);

		///tabHost.setCurrentTab(1);


		tabHost.setOnTabChangedListener(this);


			// Do the same for the other tabs
			intent = new Intent().setClass(this, Index_request_jobs.class);
			spec = tabHost.newTabSpec("tabOne");
			spec.setContent(intent);
			spec.setIndicator("Job acceptance ("+acceptance_count+")", res.getDrawable(R.drawable.icon2));
			tabHost.addTab(spec);

		// Do the same for the other tabs
		intent = new Intent().setClass(this, Index_request_jobs_rework.class);
		spec = tabHost.newTabSpec("tabOne");
		spec.setContent(intent);
		spec.setIndicator("Re-work ("+rework_count+")", res.getDrawable(R.drawable.icon3));
		tabHost.addTab(spec);
// Do the same for the other tabs
		intent = new Intent().setClass(this, Index_request_submitted.class);
		spec = tabHost.newTabSpec("tabOne");
		spec.setContent(intent);
		spec.setIndicator("Submitted", res.getDrawable(R.drawable.icon3));
		tabHost.addTab(spec);



		for(int i=0;i<tabHost.getTabWidget().getChildCount();i++) {
			tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.parseColor("#ffc80b"));
			TextView tv = (TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title); //Unselected Tabs
			tv.setTextColor(Color.parseColor("#242001"));
			//tv.setTextSize(10);

		}

		/*if(tab_num.contentEquals("1")){
			tabHost.getTabWidget().setCurrentTab(1);
			tabHost.getTabWidget().getChildAt(1).setBackgroundColor(Color.parseColor("#ffeb57"));
		}else if(tab_num.contentEquals("0")){
			tabHost.getTabWidget().setCurrentTab(0);
			tabHost.getTabWidget().getChildAt(0).setBackgroundColor(Color.parseColor("#ffeb57"));
		}else{
			tabHost.getTabWidget().setCurrentTab(0);
			tabHost.getTabWidget().getChildAt(0).setBackgroundColor(Color.parseColor("#ffeb57"));
		}*/

		tabHost.getTabWidget().setCurrentTab(0);
		tabHost.getTabWidget().getChildAt(0).setBackgroundColor(Color.parseColor("#ffeb57"));
		TextView tv = (TextView) tabHost.getCurrentTabView().findViewById(android.R.id.title); //for Selected Tab
		tv.setTextColor(Color.parseColor("#242001"));
		//tv.setTextSize(10);
		tabHost.getTabWidget().setDividerDrawable(R.drawable.tab_divider);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater menuInflater = getMenuInflater();
		menuInflater.inflate(R.menu.menu, menu);
		menu.findItem(R.id.search).setVisible(false);
		menu.findItem(R.id.attachments).setVisible(true);
		menu.findItem(R.id.logout).setVisible(true);
		menu.findItem(R.id.refresh).setVisible(true);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.attachments:
			attachments();
			return true;
			case R.id.logout:
				logout();
				return true;
			case R.id.search:

				return true;
			case R.id.refresh:
				finish();
				Intent i = new Intent(Index_request_reports_jobs.this, Index_request_reports_jobs.class);
				startActivity(i);
				return true;
			default:
			return super.onOptionsItemSelected(item);
		}
	}

	
	public void attachments() {
		startActivity(new Intent(Index_request_reports_jobs.this, Index_Attachments.class));
	}
	
	public void logout() {

		SharedPreferences.Editor prefsEditor = appPrefs.edit();
		prefsEditor.putString("keep", "false");
		prefsEditor.commit();
		
		Intent i = new Intent(Index_request_reports_jobs.this, Index_request_jobs_auth.class);
		// set the new task and clear flags
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);//all class
		startActivity(i);
		
		finish();
	}
	
	@Override
	public void onBackPressed() {
		exit_dialog();
	}

	public void exit_dialog() {
		// custom dialog
		final Dialog dialog = new Dialog(Index_request_reports_jobs.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.yes_no_dialog);

		// set the custom dialog components - text, image and button
		TextView text = (TextView) dialog.findViewById(R.id.tv_question);
		text.setText("Are you sure you want to exit the application?");
		Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
		Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

		// if button is clicked, close the custom dialog
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				lock.release();
//				finish();
				android.os.Process.killProcess(android.os.Process.myPid());//kill the app
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}


	@Override
	public void onTabChanged(String tabId) {
		for(int i=0;i<tabHost.getTabWidget().getChildCount();i++) {
			tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.parseColor("#ffc80b"));
			TextView tv = (TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title); //Unselected Tabs
			tv.setTextColor(Color.parseColor("#242001"));
			//tv.setTextSize(10);


		}

		tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundColor(Color.parseColor("#ffeb57"));//selected
		TextView tv = (TextView) tabHost.getCurrentTabView().findViewById(android.R.id.title); //for Selected Tab
		tv.setTextColor(Color.parseColor("#242001"));
		//tv.setTextSize(10);

	}

	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}


}
