package com.gds.appraisalmaybank.requestjobs;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.check_network.NetworkUtil;
import com.gds.appraisalmaybank.json.MyHttpClient;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;
import com.gds.appraisalmaybank.request.Request_Type_add;

import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

public class Index_request_jobs_rework extends Activity implements OnClickListener {
	private static final String TAG_RECORD_ID = "record_id";
	private static final String TAG_PASS_APPRAISAL_TYPE = "pass_appraisal_type";
	String pass_appraisal_type;
	String username, pref_username;
	String output_appraisal_type;
	String c_fname, c_mname, c_lname, c_request_appraisal,c_is_company,c_company_name;
	SharedPreferences appPrefs;
	SharedPreferences.Editor appPrefsEditor;
	LayoutInflater inflater;
	ArrayList<HashMap<String, Object>> searchResults;
	String only_response = "[\"system.record_id\", \"app_account_first_name\",\"app_account_middle_name\", \"app_account_last_name\", \"app_account_is_company\", \"app_account_company_name\", \"appraisal_request.app_request_appraisal\""
			+ ", \"app_daterequested_day\", \"app_daterequested_month\", \"app_daterequested_year\"]";
	ProgressDialog pDialog;
	ListView playerListView;
	Session_Timer st = new Session_Timer(this);
	String[][] names;
	Integer[] photos;
	Global gs;
	//for tls
	JSONObject jsonResponse = new JSONObject();
	ArrayList<String> field = new ArrayList<>();
	ArrayList<String> value = new ArrayList<>();
	ArrayList<HashMap<String, Object>> originalValues;
	String fname, lname, mname, request_appraisal = null, record_id,is_comp,comp_name;
	public static Request_Type_add instance = null;
	String full_name;
	String values;
	String dr_day, dr_month, dr_year, full_dr_date;
	// for API
	String xml;
	DefaultHttpClient httpClient = new MyHttpClient(this);
	CustomAdapter adapter;
	// show more
	Button btn_show_more;
	int counter_total = 10;
	TextView txt_total;
	TextView txt_index;
	String responseCode = "";
	GDS_methods gds = new GDS_methods();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.index_jobs);
		gs = ((Global) getApplicationContext());
		appPrefs = getSharedPreferences("preference", 0);
		appPrefsEditor = appPrefs.edit();
		pref_username = appPrefs.getString("username", "");
		btn_show_more = (Button) findViewById(R.id.btn_show_more);
		final EditText txt_search = (EditText) findViewById(R.id.txt_search);
		playerListView = (ListView) findViewById(android.R.id.list);
		txt_total = (TextView) findViewById(R.id.txt_total);
		txt_index = (TextView) findViewById(R.id.txt_index);
		st.resetDisconnectTimer();
		// onclickitem
		btn_show_more.setOnClickListener(this);
		playerListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				values = ((TextView) arg1.findViewById(R.id.txt_values))
						.getText().toString();
				if (values.equals("")) {
					Toast.makeText(getApplicationContext(), "no record found",
							Toast.LENGTH_SHORT).show();
				} else {
					NetworkUtil
							.getConnectivityStatusString(getApplicationContext());
					if (!NetworkUtil.status.equals("Network not available")) {
						Toast.makeText(getApplicationContext(), values,
								Toast.LENGTH_SHORT).show();
						gs.record_id = values;
						
						//go to Request_Jobs_Info_Rework.class and pass record_id and appraisal_type
						//(values needed for data sync from CC
						Intent in;
						in = new Intent(getApplicationContext(), Request_Jobs_Info_Rework.class);
						in.putExtra(TAG_RECORD_ID, gs.record_id);
						in.putExtra(TAG_PASS_APPRAISAL_TYPE, pass_appraisal_type);
						startActivityForResult(in, 100);
						
						//startActivityForResult(new Intent(getApplicationContext(), Request_Jobs_Info_Rework.class), 0);
						
						//finish();
					} else {
						Toast.makeText(getApplicationContext(),
								"Network not available", Toast.LENGTH_SHORT)
								.show();
					}

				}

			}

		});
		NetworkUtil.getConnectivityStatusString(this);
		if (!NetworkUtil.status.equals("Network not available")) {
			new SendData().execute();
			playerListView.setFastScrollEnabled(true);
			txt_search.addTextChangedListener(new TextWatcher() {

				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// get the text in the EditText
					String searchString = txt_search.getText().toString();
					int textLength = searchString.length();
					searchResults.clear();

					for (int i = 0; i < originalValues.size(); i++) {
						String playerName = originalValues.get(i).get("name")
								.toString();
						if (textLength <= playerName.length()) {
							// compare the String in EditText with Names in the
							// ArrayList
							if (searchString.equalsIgnoreCase(playerName
									.substring(0, textLength)))
								searchResults.add(originalValues.get(i));
						}
					}

					adapter.notifyDataSetChanged();
				}

				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {

				}

				public void afterTextChanged(Editable s) {

				}
			});
		} else {
			Toast.makeText(getApplicationContext(), "Network not available",
					Toast.LENGTH_SHORT).show();
			btn_show_more.setVisibility(View.GONE);
		}

	}

	// define your custom adapter
	private class CustomAdapter extends ArrayAdapter<HashMap<String, Object>> {

		public CustomAdapter(Context context, int textViewResourceId,
				ArrayList<HashMap<String, Object>> Strings) {

			// let android do the initializing :)
			super(context, textViewResourceId, Strings);
		}

		// class for caching the views in a row
		private class ViewHolder {
			ImageView photo;
			TextView name;
			TextView app_type;
			TextView txt_values;
			TextView txt_index;
		}

		ViewHolder viewHolder;

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			if (convertView == null) {
				convertView = inflater
						.inflate(R.layout.index_jobs_layout, null);
				viewHolder = new ViewHolder();

				// cache the views
				viewHolder.photo = (ImageView) convertView
						.findViewById(R.id.mono);
				viewHolder.name = (TextView) convertView
						.findViewById(R.id.txt_image_list);
				viewHolder.app_type = (TextView) convertView
						.findViewById(R.id.app_type);
				viewHolder.txt_values = (TextView) convertView
						.findViewById(R.id.txt_values);
				viewHolder.txt_index = (TextView) convertView
						.findViewById(R.id.txt_index);
				// link the cached views to the convertview
				convertView.setTag(viewHolder);

			} else
				viewHolder = (ViewHolder) convertView.getTag();

			int photoId = (Integer) searchResults.get(position).get("photo");

			// set the data to be displayed
			viewHolder.photo.setImageDrawable(getResources().getDrawable(
					photoId));
			viewHolder.name.setText(searchResults.get(position).get("name")
					.toString());
			viewHolder.app_type.setText(searchResults.get(position)
					.get("app_type").toString());
			viewHolder.txt_values.setText(searchResults.get(position)
					.get("txt_values").toString());
			viewHolder.txt_index.setText(searchResults.get(position)
					.get("txt_index").toString());
			// return the view to be displayed
			return convertView;
		}

	}

	private class SendData extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(Index_request_jobs_rework.this);
			pDialog.setMessage("Fetching data..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.setCanceledOnTouchOutside(false);
			pDialog.show();
		}

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			responseCode = gds.google_ping();
			if (responseCode.equals("200")) {
				http_json();
				Log.e("", "200");
			} else if (responseCode.equals("404")) {
				Log.e("", "404");
			} else if (responseCode.equals("")) {
				Log.e("", "0");
			}

			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			if (responseCode.equals("200")) {
				display_list();
			} else {
				btn_show_more.setVisibility(View.GONE);
				//custom toast
				Toast toast = Toast.makeText(getBaseContext(), "Unable to connect to the server", Toast.LENGTH_LONG);
				View view = toast.getView();
				view.setBackgroundResource(R.color.toast_red);
				toast.show();
				serverError();
			}

			pDialog.dismiss();

		}
	}

	Dialog myDialog;
	public void serverError(){
		
		myDialog = new Dialog(Index_request_jobs_rework.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.warning_dialog);
		myDialog.setCancelable(true);
		final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
		final Button btn_ok = (Button) myDialog.findViewById(R.id.btn_ok);
		
		tv_question.setText(R.string.server_error);
		btn_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		myDialog.show();
	}
	
	/*public void google_ping() {
		try {
//			URL obj = new URL("https://www.google.com.ph/?gws_rd=cr&ei=td9kUrP0H8mjigenuoHAAg");
			URL obj = new URL(gs.server_url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");

			// add request header
			con.setRequestProperty("User-Agent", "Mozilla/5.0");

			responseCode = String.valueOf(con.getResponseCode());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/

	public void http_json() {
		inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		JSONObject json_query = new JSONObject();
		String query;
		try {
			json_query.put("app_simul_type", "sub");
			json_query.put("application_status", "for_rework");
			json_query.put("rework_accepted", "false");//limit to unaccepted rework
			
			json_query.put("appraisal_request.appraiser_username", pref_username);
			json_query.put("system.hidden", false);
			query = json_query.toString();
			// API

			//for tls
			field.clear();
			field.add("auth_token");
			field.add("query");
			field.add("limit");
			field.add("only");

			value.clear();
			value.add(gs.auth_token);
			value.add(query);
			value.add("100");
			value.add(only_response);

			if(Global.type.contentEquals("tls")){
				jsonResponse = gds.makeHttpsRequest(gs.records_url, "POST", field, value);
				xml = jsonResponse.toString();
			}else{
				xml=gds.http_posting(field,value,gs.records_url,Index_request_jobs_rework.this);
			}

			Log.e("response", xml);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("", e.toString());
		}

		// UserFunctions userFunction = new UserFunctions();
		// userFunction.CaseCenterResponse(query,only_response);

		try {
			JSONObject json = new JSONObject(xml);
			JSONArray array_length = json.getJSONArray("records");
			if (array_length.length() != 0) {
				names = new String[array_length.length()][4];
				photos = new Integer[array_length.length()];
				for (int i = 0; i < array_length.length(); i++) {
					JSONObject json_data = array_length.getJSONObject(i);
					fname = json_data.getString("app_account_first_name");
					lname = json_data.getString("app_account_last_name");
					mname = json_data.getString("app_account_middle_name");
					dr_day = json_data.getString("app_daterequested_day");
					dr_month = json_data.getString("app_daterequested_month");
					dr_year = json_data.getString("app_daterequested_year");
					is_comp=json_data.getString("app_account_is_company");
					comp_name=json_data.getString("app_account_company_name");
					Log.e("is_comp",is_comp);
					Log.e("comp_name",comp_name);
					JSONArray jArray2 = json_data
							.getJSONArray("appraisal_request");
					for (int x = 0; x < jArray2.length(); x++) {
						JSONObject json_data2 = jArray2.getJSONObject(x);
						request_appraisal = json_data2
								.getString("app_request_appraisal");

					}

					JSONObject system = json_data.getJSONObject("system");
					record_id = system.getString("record_id");

					// set on listview
					c_fname = string_checker(fname);
					c_mname = string_checker(mname);
					c_lname = string_checker(lname);
					c_is_company=gds.nullCheck3(is_comp);
					c_company_name=gds.nullCheck3(comp_name);
					c_request_appraisal = string_checker(request_appraisal);
					full_name = c_fname + c_mname + c_lname;
					full_dr_date = dr_month + dr_day + dr_year;
					if(c_is_company.contentEquals("true")){
						names[i][0] = c_company_name;
					}else{
						names[i][0] = full_name;
					}
					names[i][1] = record_id;
					for (int x = 0; x < gs.appraisal_type_value.length; x++) {
						if (request_appraisal
								.equals(gs.appraisal_type_value[x])) {
							output_appraisal_type = gs.appraisal_output[x];
							//pass appraisal type to next class
							pass_appraisal_type = gs.appraisal_type_value[x];
						}
					}
					names[i][2] = output_appraisal_type;
					names[i][3] = full_dr_date;
					photos[i] = R.drawable.monotoneright_white;
				}
				appPrefsEditor.putString("rework_job_count", ""+array_length.length());
				appPrefsEditor.commit();
			} else {
				names = new String[1][4];
				photos = new Integer[1];
				names[0][0] = "no record found";
				names[0][1] = "";
				names[0][2] = "";
				names[0][3] = "";
				photos[0] = R.drawable.monotoneright_white;
				appPrefsEditor.putString("rework_job_count", "0");
				appPrefsEditor.commit();
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		// sort
		Arrays.sort(names, new Comparator<String[]>() {
			@Override
			public int compare(String[] entry1, String[] entry2) {
				String time1 = entry1[3];
				String time2 = entry2[3];
				return time1.compareTo(time2);
			}
		});
	}

	public void display_list() {
		originalValues = new ArrayList<HashMap<String, Object>>();

		// temporary HashMap for populating the
		// Items in the ListView
		HashMap<String, Object> temp;
		int index = 1;
		// now populate the ArrayList players
		// checking of len
		if (names.length < 10) {
			for (int i = 0; i < names.length; i++) {
				temp = new HashMap<String, Object>();

				temp.put("name", names[i][0]);
				temp.put("txt_values", names[i][1]);
				temp.put("app_type", names[i][2]);
				if (names.length == 1) {
					temp.put("txt_index", "");
				} else {
					temp.put("txt_index", index + ".");
				}
				temp.put("photo", photos[i]);
				index++;
				// add the row to the ArrayList
				originalValues.add(temp);
				btn_show_more.setVisibility(View.GONE);
			}
		} else {

			for (int i = 0; i < counter_total; i++) {
				temp = new HashMap<String, Object>();
				temp.put("name", names[i][0]);
				temp.put("txt_values", names[i][1]);
				temp.put("app_type", names[i][2]);
				temp.put("txt_index", index + ".");
				temp.put("photo", photos[i]);
				index++;
				// add the row to the ArrayList
				originalValues.add(temp);
				txt_total.setText(counter_total + " / " + names.length);
			}

		}
		// searchResults=OriginalValues initially
		searchResults = new ArrayList<HashMap<String, Object>>(originalValues);
		// will populate the ListView
		adapter = new CustomAdapter(Index_request_jobs_rework.this,
				R.layout.index_jobs_layout, searchResults);

		// finally,set the adapter to the default ListView
		playerListView.setAdapter(adapter);
	}

	public void show_more() {
		counter_total = counter_total + 10;
		if (counter_total < names.length) {
			display_list();
		} else {
			counter_total = names.length;
			display_list();
		}

	}

	public String string_checker(String old_string) {
		String new_string = old_string;
		if (old_string.equals("null")) {
			new_string = "";
		} else if (old_string.equals("0")) {
			new_string = "";
		} else {
			new_string = old_string + " ";
		}

		return new_string;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_show_more:
			show_more();
			break;

		default:
			break;
		}
	}
	
	@Override
	public void onBackPressed() {
		this.getParent().onBackPressed();
	}

	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}

}