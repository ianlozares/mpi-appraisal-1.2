package com.gds.appraisalmaybank.requestjobs;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.check_network.NetworkUtil;
import com.gds.appraisalmaybank.database.Attachments_API;
import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.json.JSONParser2;
import com.gds.appraisalmaybank.json.TLSConnection;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


public class Uploaded_Attachments_All extends ListActivity {
	ActionBar actionBar;
	DatabaseHandler dbMain = new DatabaseHandler(this);
	Global gs;
	Session_Timer st = new Session_Timer(this);
	// Progress Dialog
	private ProgressDialog pDialog;
	// Creating JSON Parser object
	JSONParser2 jParser = new JSONParser2();
	ArrayList<HashMap<String, String>> fileList;

	private static final String TAG_PASS_STAT = "pass_stat";
	String pass_stat="";
	// JSON Node names
	private static final String TAG_SUCCESS = "success";
	int success;
	private static final String TAG_UID = "uid";
	private static final String TAG_FILES = "files";//
	private static final String TAG_FILE = "file";
	private static final String TAG_FILE_NAME = "file_name";
	String uid="";
	String file="";
	String file_name = "",file_name_jpg = "";
	private static final String TAG_AUTH_KEY = "auth_key";
	String auth_key="QweAsdZxc";
	// file JSONArray
	JSONArray files = null;
	//for tls
	TLSConnection tlscon = new TLSConnection();
	JSONObject jsonResponse = new JSONObject();
	ArrayList<String> field = new ArrayList<>();
	ArrayList<String> value = new ArrayList<>();
	GDS_methods gds = new GDS_methods();
	JSONObject json;
	//offline
	String record_id="";
	private static final String TAG_RECORD_ID = "record_id";

	//file view
	String url_webby, url_webby_jpg;
	String uploaded_attachment;
	File uploaded_file;
	//String[] commandArray = new String[] { "View Attachment in Browser","Download Attachment" };
	String[] commandArray = new String[] { "Download Attachment" };
	String[] viewImageOptionArray = new String[] { "View File", "Open Lot Plotting App" };
	String dir = "gds_appraisal";
	File myDirectory = new File(Environment.getExternalStorageDirectory() + "/"
			+ dir + "/");
	int responseCode;
	// Progress dialog type (0 - for Horizontal progress bar)
	public static final int progress_bar_type = 0;
	String where ="WHERE record_id = args";
	String[] args = new String[1];
	DatabaseHandler2 db2 = new DatabaseHandler2(this);
	String appraisal_type;
	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.uploaded_attachments_all);
		st.resetDisconnectTimer();

		actionBar = getActionBar();
		actionBar.setIcon(R.drawable.attachment);
		actionBar.setTitle("Uploaded Attachments");

		gs = ((Global) getApplicationContext());

		// getting record_id from intent / previous class
		Intent i = getIntent();
		uid = i.getStringExtra(TAG_UID);
		record_id = i.getStringExtra(TAG_RECORD_ID);
		pass_stat = i.getStringExtra(TAG_PASS_STAT);
		args[0]=record_id;
		appraisal_type = db2.getRecord("appraisal_type", where, args, "tbl_report_accepted_jobs").get(0);
		// Hashmap for ListView
		fileList = new ArrayList<HashMap<String, String>>();

		//new LoadAllFiles().execute();
		if (pass_stat.equals("online")){
			new LoadAllFiles().execute();
			Log.e("Load All Files","true");
		} else if (pass_stat.equals("offline")){
			new LoadAllSQLite().execute();
			Log.e("Load All SQLite","true");
		}


		// Get listview
		ListView lv = getListView();

		// on seleting single product
		// launching Edit Product Screen
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {
				// getting values from selected ListItem
				file_name = ((TextView) view.findViewById(R.id.item_file_name)).getText().toString();

				if (file_name.isEmpty()){

				} else {
					//convert filename of .pdf to .jpg 
					/*String substr = file_name.substring(file_name.length() - 3);
					if (substr.equals("pdf")) {
						file_name = file_name.replace(".pdf",".jpg");//change last 4 chars to jpg
					} else if (substr.equals("PDF")){
						file_name = file_name.replace(".PDF",".jpg");//change last 4 chars to jpg
					}*/
					//Toast.makeText(getApplicationContext(),""+file_name,Toast.LENGTH_SHORT).show();
//					open_land_plotting_app();


					uploaded_attachments();
//					if (pass_stat.equals("online")){
//						uploaded_attachments();
//					} else if (pass_stat.equals("offline")){
//						open_land_plotting_app();
//					}
				}

			}
		});

	}

	public void open_land_plotting_app(){
		Toast.makeText(getApplicationContext(),"Filename "+file_name,Toast.LENGTH_SHORT).show();
		try{
			//convert filename .pdf to .jpg 
//			String substr = file_name.substring(file_name.length() - 3);
//			if (substr.equals("pdf")) {
//				file_name = file_name.replace(".pdf",".jpg");//change last 4 chars to jpg
//			} else if (substr.equals("PDF")){
//				file_name = file_name.replace(".PDF",".jpg");//change last 4 chars to jpg
//
//			}

			Intent sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			File file2 = new File(myDirectory+"/"+file_name);//get path concat with slash and file name
			sendIntent.putExtra(Intent.EXTRA_TEXT, file2.getAbsolutePath());
			sendIntent.putExtra("logged_in", "true");
			sendIntent.setType("text/plain");
			//sendIntent.setPackage("com.gds.projects.lotplottingapp");
			sendIntent.setPackage("com.gds.projects.plotter");
			startActivityForResult(sendIntent, 4);
			Toast.makeText(getApplicationContext(),"file 2:\n"+file2,Toast.LENGTH_SHORT).show();
		} catch (Exception e){
			e.printStackTrace();
			Toast.makeText(getApplicationContext(),"Please install the lot plotting application.",Toast.LENGTH_SHORT).show();
		}

	}

	public void uploaded_attachments() {
		// set value
		uploaded_attachment = file_name;
		// directory
		uploaded_file = new File(myDirectory,
				uploaded_attachment);
		uploaded_attachment = uploaded_attachment.replaceAll(
				" ", "%20");

		Log.e("UPLOADED ATTACHMENT",""+uploaded_attachment);

		url_webby = gs.pdf_loc_url + uploaded_attachment;
		//Toast.makeText(getApplicationContext(),""+url_webby,Toast.LENGTH_LONG).show();
		view_pdf(uploaded_file);

	}

	public void view_pdf(File file_f) {
//		Toast.makeText(getApplicationContext(),"file name "+file_name,Toast.LENGTH_SHORT).show();
		if (file_f.exists()) {
			if (file_name.equals("")){
				Toast.makeText(getApplicationContext(),"No attachment found",Toast.LENGTH_SHORT).show();
			}else{

				if (pass_stat.equals("offline")){
					final File filecopy = file_f;

					if(appraisal_type.contentEquals("motor_vehicle")){
						String substr = file_name.substring(file_name.length() - 3);
						Uri path = Uri.fromFile(filecopy);
						Intent intent = new Intent();
						intent.setAction(Intent.ACTION_VIEW);
						if ((substr.equals("pdf")) || (substr.equals("PDF"))) {
							intent.setDataAndType(path, "application/pdf");
						} else {
							intent.setDataAndType(path, "image/*");

						}
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

						try {
							startActivity(intent);
						} catch (ActivityNotFoundException e) {
							Toast.makeText(getApplicationContext(),
									"No Application Available to View PDF. Please download PDF Reader",
									Toast.LENGTH_SHORT).show();
						}
					}else{
						//open dialog for options
						android.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
						builder.setItems(viewImageOptionArray, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int index) {
								if (index == 0) {

									String substr = file_name.substring(file_name.length() - 3);

									Uri path = Uri.fromFile(filecopy);
									Intent intent = new Intent();
									intent.setAction(Intent.ACTION_VIEW);
									if ((substr.equals("pdf")) || (substr.equals("PDF"))) {
										intent.setDataAndType(path, "application/pdf");
									} else {
										intent.setDataAndType(path, "image/*");

									}
									intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

									try {
										startActivity(intent);
									} catch (ActivityNotFoundException e) {
										Toast.makeText(getApplicationContext(),
												"No Application Available to View PDF",
												Toast.LENGTH_SHORT).show();
									}
								} else if (index == 1) {
									open_land_plotting_app();
								}
							}
						});
						AlertDialog alert = builder.create();
						alert.show();
					}






				} else if (pass_stat.equals("online")){

					String substr = file_name.substring(file_name.length() - 3);
					Uri path = Uri.fromFile(file_f);
					Intent intent = new Intent();
					intent.setAction(Intent.ACTION_VIEW);
					if ((substr.equals("pdf")) || (substr.equals("PDF"))) {
						intent.setDataAndType(path, "application/pdf");
					} else {
						intent.setDataAndType(path, "image/*");
					}
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

					try {
						startActivity(intent);
					} catch (ActivityNotFoundException e) {
						Toast.makeText(getApplicationContext(),
								"No Application Available to View PDF",
								Toast.LENGTH_SHORT).show();
					}
				}
			}
		} else {
			NetworkUtil.getConnectivityStatusString(this);
			if (!NetworkUtil.status.equals("Network not available")) {
				Log.e("Convert","start");
				if(file.contentEquals("CCT")||file.contentEquals("TCT")){
					//new ConvertPDFtoJPG().execute();
				}

				new Attachment_validation().execute();
			} else {
				Toast.makeText(getApplicationContext(),
						"Network not available", Toast.LENGTH_SHORT).show();
			}

		}
	}

	private class Attachment_validation extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(Uploaded_Attachments_All.this);
			pDialog.setMessage("Checking attachment..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			try {
				Log.e("validation","start" );
				URL obj = new URL(url_webby);
				/*HttpURLConnection con = (HttpURLConnection) obj
						.openConnection();*/
				disableSSLCertificateChecking();
				HttpsURLConnection con;
				HttpURLConnection con2;
				if(Global.type.contentEquals("tls")){
					con = tlscon.setUpHttpsConnection("" +obj);
					con.setRequestMethod("GET");
					con.setRequestProperty("User-Agent", "Mozilla/5.0");
					responseCode = con.getResponseCode();
				}else{
					con2 = (HttpURLConnection) obj.openConnection();
					con2.setRequestMethod("GET");
					con2.setRequestProperty("User-Agent", "Mozilla/5.0");
					responseCode = con2.getResponseCode();
				}
				Log.e("", String.valueOf(responseCode));
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				Log.e("validation","fail" );
				e.printStackTrace();
			} catch (ProtocolException e) {
				// TODO Auto-generated catch block
				Log.e("validation","fail" );
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				Log.e("validation","fail" );
				e.printStackTrace();
			}

			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			Log.e("", url_webby);
			if (responseCode == 200) {
				view_download();
			} else if (responseCode == 404) {
				Toast.makeText(getApplicationContext(),
						"Attachment doesn't exist", Toast.LENGTH_SHORT).show();
			}
			pDialog.dismiss();

		}
	}

	private void view_download() {
		android.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Actions :");
		builder.setItems(commandArray, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int index) {
				/*if (index == 0) {
					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
							.parse(url_webby));
					startActivity(browserIntent);
					dialog.dismiss();
				} else if (index == 1) {
					new DownloadFileFromURL().execute(url_webby);
					dialog.dismiss();
				}*/
				if (index == 0) {
					new DownloadFileFromURL().execute();
					/*Log.e("File",file);
					if(file.contentEquals("CCT")||file.contentEquals("TCT")){
						new DownloadFileFromURLJPG().execute();
					}*/
					dialog.dismiss();
				}
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	/**
	 * Showing Dialog
	 * */
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
			case progress_bar_type:
				pDialog = new ProgressDialog(this);
				pDialog.setMessage("Downloading file. Please wait...");
				pDialog.setIndeterminate(false);
				pDialog.setMax(100);
				pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				pDialog.setCancelable(true);
				pDialog.setCanceledOnTouchOutside(false);
				pDialog.show();
				return pDialog;
			default:
				return null;
		}
	}

	/**
	 * Background Async Task to download file
	 * */
	class DownloadFileFromURL extends AsyncTask<Void, String, Boolean> {

		/**
		 * Before starting background thread Show Progress Bar Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showDialog(progress_bar_type);
		}

		/**
		 * Downloading file in background thread
		 * */
		@Override
		protected Boolean doInBackground(Void... arg0) {
			int count;
			boolean connect=true,connect1=true;
			try {

				// Create a new trust manager that trust all certificates
				TrustManager[] trustAllCerts = new TrustManager[]{
						new X509TrustManager() {
							public java.security.cert.X509Certificate[] getAcceptedIssuers() {
								return null;
							}
							public void checkClientTrusted(
									java.security.cert.X509Certificate[] certs, String authType) {
							}
							public void checkServerTrusted(
									java.security.cert.X509Certificate[] certs, String authType) {
							}
						}
				};

// Activate the new trust manager
				try {
					SSLContext sc = SSLContext.getInstance("SSL");
					sc.init(null, trustAllCerts, new java.security.SecureRandom());
					HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
				} catch (Exception e) {
					connect=false;
				}

// And as before now you can use URL and URLConnection
				URL url = new URL(url_webby);
				URLConnection connection = url.openConnection();
				InputStream input = connection.getInputStream();
				int lenghtOfFile = connection.getContentLength();

				// Output stream to write file
				OutputStream output = new FileOutputStream(
						uploaded_file.toString());

				byte data[] = new byte[1024];

				long total = 0;

				while ((count = input.read(data)) != -1) {
					total += count;
					// publishing the progress....
					// After this onProgressUpdate will be called
					publishProgress("" + (int) ((total * 100) / lenghtOfFile));

					// writing data to file
					output.write(data, 0, count);
				}

				// flushing output
				output.flush();

				// closing streams
				output.close();
				input.close();

			} catch (Exception e) {
				Log.e("Error: ", e.getMessage());
				connect1=false;
			}
			Log.e("connect",""+connect);
			return gds.pingAttachment()&&connect&&connect1;
		}

		/**
		 * Updating progress bar
		 * */
		protected void onProgressUpdate(String... progress) {
			// setting progress percentage
			pDialog.setProgress(Integer.parseInt(progress[0]));
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		@Override
		protected void onPostExecute(Boolean result) {
			Log.e("result",""+result);
			// dismiss the dialog after the file was downloaded
			removeDialog(progress_bar_type);
			if(result){
				Toast.makeText(getApplicationContext(), "Attachment Downloaded",
						Toast.LENGTH_LONG).show();
			}else{
				Toast.makeText(getApplicationContext(), "Error Downloading. Please check the Server/Internet connection.",
						Toast.LENGTH_LONG).show();

			}

//			view_pdf(uploaded_file);

		}

	}
	class DownloadFileFromURLJPG extends AsyncTask<Void, String, Boolean> {

		/**
		 * Before starting background thread Show Progress Bar Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showDialog(progress_bar_type);
		}

		/**
		 * Downloading file in background thread
		 * */
		@Override
		protected Boolean doInBackground(Void... arg0) {
			int count;
			boolean connect=true,connect1=true;
			try {

				// Create a new trust manager that trust all certificates
				TrustManager[] trustAllCerts = new TrustManager[]{
						new X509TrustManager() {
							public java.security.cert.X509Certificate[] getAcceptedIssuers() {
								return null;
							}
							public void checkClientTrusted(
									java.security.cert.X509Certificate[] certs, String authType) {
							}
							public void checkServerTrusted(
									java.security.cert.X509Certificate[] certs, String authType) {
							}
						}
				};

// Activate the new trust manager
				try {
					SSLContext sc = SSLContext.getInstance("SSL");
					sc.init(null, trustAllCerts, new java.security.SecureRandom());
					HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
				} catch (Exception e) {
					connect=false;
				}

// And as before now you can use URL and URLConnection
				Log.d("url_webby_jpg",url_webby_jpg);

				URL url = new URL(url_webby_jpg);
				URLConnection connection = url.openConnection();
				InputStream input = connection.getInputStream();
				int lenghtOfFile = connection.getContentLength();

				// Output stream to write file
				OutputStream output = new FileOutputStream(
						uploaded_file.toString());

				byte data[] = new byte[1024];

				long total = 0;

				while ((count = input.read(data)) != -1) {
					total += count;
					// publishing the progress....
					// After this onProgressUpdate will be called
					publishProgress("" + (int) ((total * 100) / lenghtOfFile));

					// writing data to file
					output.write(data, 0, count);
				}

				// flushing output
				output.flush();

				// closing streams
				output.close();
				input.close();

			} catch (Exception e) {
				Log.e("Error: ", e.getMessage());
				connect1=false;
			}
			Log.e("connect",""+connect);
			return gds.pingAttachment()&&connect&&connect1;
		}

		/**
		 * Updating progress bar
		 * */
		protected void onProgressUpdate(String... progress) {
			// setting progress percentage
			pDialog.setProgress(Integer.parseInt(progress[0]));
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		@Override
		protected void onPostExecute(Boolean result) {
			Log.e("result",""+result);
			// dismiss the dialog after the file was downloaded
			removeDialog(progress_bar_type);
			if(result){
				Toast.makeText(getApplicationContext(), "Attachment Downloaded",
						Toast.LENGTH_LONG).show();
			}else{
				Toast.makeText(getApplicationContext(), "Error Downloading. Please check the Server/Internet connection.",
						Toast.LENGTH_LONG).show();

			}

//			view_pdf(uploaded_file);

		}

	}
	/**
	 * Background Async Task to Load all product by making HTTP Request
	 * */
	class LoadAllFiles extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Uploaded_Attachments_All.this);
			pDialog.setMessage("Loading files. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All files from url
		 * */
		protected String doInBackground(String... args) {
			// Building Parameters

			//for tls
			field.clear();
			field.add(TAG_UID);
			field.add(TAG_AUTH_KEY);

			value.clear();
			value.add(uid);
			value.add(auth_key);

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			Log.e("UID",""+uid);
			JSONObject json;
			if(Global.type.contentEquals("tls")){
				json= gds.makeHttpsRequest(gs.get_files_url, "POST", field, value);
			}else{
				params.add(new BasicNameValuePair(TAG_UID, uid));
				params.add(new BasicNameValuePair(TAG_AUTH_KEY, auth_key));
				// getting JSON string from URL
				json = gds.makeHttpRequest(gs.get_files_url, "POST", params);
			}


			// Check your log cat for JSON reponse
			Log.d("All Files: ", json.toString());

			try {
				// Checking for SUCCESS TAG
				success = json.getInt(TAG_SUCCESS);

				if (success == 1) {
					// products found
					// Getting Array of Products
					files = json.getJSONArray(TAG_FILES);

					// looping through All Products
					for (int i = 0; i < files.length(); i++) {
						JSONObject c = files.getJSONObject(i);

						// Storing each json item in variable
						String uid = c.getString(TAG_UID);
						String file = c.getString(TAG_FILE);
						String file_name = c.getString(TAG_FILE_NAME);

						// creating new HashMap
						HashMap<String, String> map = new HashMap<String, String>();

						// adding each child node to HashMap key => value
						map.put(TAG_UID, uid);
						map.put(TAG_FILE, file);
						map.put(TAG_FILE_NAME, file_name);

						// adding HashList to ArrayList
						fileList.add(map);
					}
				} else {
					// no files found
					// Launch prev Activity
					/*Intent i = new Intent(getApplicationContext(),
							Request_Jobs_Info.class);
					// Closing all previous activities
					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(i);
					finish();*/
					HashMap<String, String> map = new HashMap<String, String>();

					// adding each child node to HashMap key => value
//					map.put(TAG_UID, uid);
					map.put(TAG_FILE, "No attachments found");
//					map.put(TAG_FILE_NAME, "");

					// adding HashList to ArrayList
					fileList.add(map);
				}
			} catch (JSONException e) {
				e.printStackTrace();
				/*if (success==0){
					Intent i = new Intent(getApplicationContext(),
							Request_Jobs_Info.class);
					// Closing all previous activities
					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(i);
					finish();
				}*/

			}

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					ListAdapter adapter = new SimpleAdapter(
							Uploaded_Attachments_All.this, fileList,
							R.layout.uploaded_attachments_list, new String[] { TAG_UID,
							TAG_FILE, TAG_FILE_NAME},
							new int[] { R.id.item_uid, R.id.item_file, R.id.item_file_name});
					// updating listview
					setListAdapter(adapter);
				}
			});

		}
	}

	class LoadAllSQLite extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Uploaded_Attachments_All.this);
			pDialog.setMessage("Loading Lists. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All
		 * */
		protected String doInBackground(String... args) {

			List<Attachments_API> attachmentsList = dbMain.getAll_Attachments(record_id);
			if (!attachmentsList.isEmpty()) {
				for (Attachments_API att : attachmentsList) {
					record_id = att.getrecord_id();
					uid = att.getuid();
					file = att.getfile();
					file_name = att.getfile_name();

					HashMap<String, String> map = new HashMap<String, String>();
					map.put(TAG_UID, uid);
					map.put(TAG_FILE, file);
					map.put(TAG_FILE_NAME, file_name);
					// adding HashList to ArrayList
					fileList.add(map);
				}
			} else {
				HashMap<String, String> map = new HashMap<String, String>();

				// adding each child node to HashMap key => value
//				map.put(TAG_UID, uid);
				map.put(TAG_FILE, "No attachments found");
//				map.put(TAG_FILE_NAME, "");

				// adding HashList to ArrayList
				fileList.add(map);
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					ListAdapter adapter = new SimpleAdapter(
							Uploaded_Attachments_All.this, fileList,
							R.layout.uploaded_attachments_list, new String[] { TAG_UID,
							TAG_FILE, TAG_FILE_NAME},
							new int[] { R.id.item_uid, R.id.item_file, R.id.item_file_name});
					// updating listview
					setListAdapter(adapter);
				}
			});

		}
	}


	/**
	 * Convert pdf to jpg php then download it
	 */
	class ConvertPDFtoJPG extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Uploaded_Attachments_All.this);
			pDialog.setMessage("Converting file. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All files from url
		 * */
		protected String doInBackground(String... args) {

			//convert selected jpg filename pdf
			file_name_jpg = file_name;
			String substr = file_name_jpg.substring(file_name_jpg.length() - 3);
			if (substr.equals("jpg")) {
				file_name_jpg = file_name_jpg.replace(".jpg",".pdf");//change last 4 chars to pdf
			} else if (substr.equals("PDF")){
				file_name_jpg = file_name_jpg.replace(".JPG",".pdf");//change last 4 chars to pdf
			}

			// Building Parameters

			//for tls
			field.clear();
			field.add(TAG_FILE_NAME);
			field.add(TAG_AUTH_KEY);

			value.clear();
			value.add(file_name_jpg);
			value.add(auth_key);


			if(Global.type.contentEquals("tls")){
				json = gds.makeHttpsRequest(gs.pdf_to_jpg_converter_url, "POST", field, value);

			}else{
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair(TAG_FILE_NAME, file_name));
				params.add(new BasicNameValuePair(TAG_AUTH_KEY, auth_key));
				json = gds.makeHttpRequest(gs.pdf_to_jpg_converter_url, "POST", params);
			}


			// Check your log cat for JSON reponse
			Log.d("Convert log: ", ""+json.toString());

			try {
				// Checking for SUCCESS TAG
				success = json.getInt(TAG_SUCCESS);

			} catch (JSONException e) {
				e.printStackTrace();

			}

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			//convert whole url filename of .pdf to .jpg 
			url_webby_jpg = url_webby;
			Log.d("url_webby_jpg",url_webby_jpg);
			String substr = url_webby_jpg.substring(url_webby_jpg.length() - 3);
			if (substr.equals("pdf")) {
				url_webby_jpg = url_webby_jpg.replace(".pdf",".jpg");//change last 4 chars to jpg
			} else if (substr.equals("PDF")){
				url_webby_jpg = url_webby_jpg.replace(".PDF",".jpg");//change last 4 chars to jpg
			}
			Log.d("url_webby_jpg",url_webby_jpg);
			new Attachment_validation().execute();

		}
	}

	/**
	 * Disables the SSL certificate checking for new instances of {@link HttpsURLConnection} This has been created to
	 * aid testing on a local box, not for use on production.
	 */
	private static void disableSSLCertificateChecking() {
		TrustManager[] trustAllCerts = new TrustManager[] {
				new X509TrustManager() {

					@Override
					public void checkClientTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
						// not implemented
					}

					@Override
					public void checkServerTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
						// not implemented
					}

					@Override
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return null;
					}

				}
		};

		try {

			HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {

				@Override
				public boolean verify(String s, SSLSession sslSession) {
					return true;
				}

			});
			SSLContext sc = SSLContext.getInstance("TLS");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}
}