package com.gds.appraisalmaybank.requestjobs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Report_Submitted_Jobs;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;
import com.gds.appraisalmaybank.report.View_Report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ianlo on 02/08/2016.
 */
public class Index_request_submitted  extends Activity {
    String username, pref_username;
    SharedPreferences appPrefs;
    LayoutInflater inflater;
    Session_Timer st = new Session_Timer(this);
    ArrayList<HashMap<String, Object>> searchResults;
    private ProgressDialog pDialog;
    ListView playerListView;
    String[][] names;
    AlertDialog.Builder builder;
    AlertDialog dialog_box;
    Integer[] photos;
    Global gs;

    ArrayList<HashMap<String, Object>> originalValues;

    DatabaseHandler db;
    DatabaseHandler2 db2;
    GDS_methods gds = new GDS_methods();
    int i;
    String first_name,last_name,middle_name,appraisal_type;
    String output_appraisal_type;
    CustomAdapter adapter;
    String app_type_values, values;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.index_reports);

        st.resetDisconnectTimer();
        db = new DatabaseHandler(this);
        db2 = new DatabaseHandler2(this);
        gs = ((Global) getApplicationContext());
        appPrefs = getSharedPreferences("preference", 0);
        pref_username = appPrefs.getString("username", "");
        final EditText txt_search = (EditText) findViewById(R.id.txt_search);
        playerListView = (ListView) findViewById(android.R.id.list);
        builder = new AlertDialog.Builder(Index_request_submitted.this);
        // onclickitem
        playerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                values = ((TextView) arg1.findViewById(R.id.txt_values))
                        .getText().toString();
                app_type_values = ((TextView) arg1
                        .findViewById(R.id.app_type_values)).getText()
                        .toString();

                if (values.equals("")) {
                    Toast.makeText(getApplicationContext(), "No record found",
                            Toast.LENGTH_SHORT).show();
                } else {
                                gs.record_id = values;
                                if (app_type_values.contentEquals("Condominium")) {
                                    Intent in = new Intent(getApplicationContext(), View_Report.class);
                                    in.putExtra("record_id", values);
                                    in.putExtra("appraisal_type", "townhouse_condo");
                                    startActivityForResult(in, 100);
                                } else if (app_type_values.contentEquals("Motor Vehicle")) {
                                    Intent in = new Intent(getApplicationContext(), View_Report.class);
                                    in.putExtra("record_id", values);
                                    in.putExtra("appraisal_type", "motor_vehicle");
                                    startActivityForResult(in, 100);
                                } else if (app_type_values.contentEquals("Vacant Lot")) {
                                    Intent in = new Intent(getApplicationContext(), View_Report.class);
                                    in.putExtra("record_id", values);
                                    in.putExtra("appraisal_type", "vacant_lot");
                                    startActivityForResult(in, 100);
                                } else if (app_type_values.contentEquals("Land with Improvements")) {
                                    Intent in = new Intent(getApplicationContext(), View_Report.class);
                                    in.putExtra("record_id", values);
                                    in.putExtra("appraisal_type", "land_improvements");
                                    startActivityForResult(in, 100);
                                } else if (app_type_values.contentEquals("Construction with EBM")) {
                                    Intent in = new Intent(getApplicationContext(), View_Report.class);
                                    in.putExtra("record_id", values);
                                    in.putExtra("appraisal_type", "construction_ebm");
                                    startActivityForResult(in, 100);
                                } else if (app_type_values.contentEquals("PPCR")) {
                                    Intent in = new Intent(getApplicationContext(), View_Report.class);
                                    in.putExtra("record_id", values);
                                    in.putExtra("appraisal_type", "ppcr");
                                    startActivityForResult(in, 100);
                                } else if (app_type_values.contentEquals("Townhouse")) {
                                    Intent in = new Intent(getApplicationContext(), View_Report.class);
                                    in.putExtra("record_id", values);
                                    in.putExtra("appraisal_type", "townhouse");
                                    startActivityForResult(in, 100);
                                }else if (app_type_values.contentEquals("WAF")) {
                                    Intent in = new Intent(getApplicationContext(), View_Report.class);
                                    in.putExtra("record_id", values);
                                    in.putExtra("appraisal_type", "waf");
                                    startActivityForResult(in, 100);
                                }




                }

            }

        });
        playerListView.setLongClickable(true);
        playerListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           final int arg2, long arg3) {
                values = ((TextView) arg1.findViewById(R.id.txt_values))
                        .getText().toString();


                builder.setMessage("Click 'Delete' to delete this record or 'Delete All' to delete all submitted records.")
                        .setTitle("Delete Record");

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK button
                        dialog_box.dismiss();
                    }
                });
            builder.setNeutralButton("Delete All", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
              db2.deleteRecord("tbl_report_submitted", "status = ?", new String[]{"submitted"});
              startActivity(new Intent(Index_request_submitted.this,
                      Index_request_reports_jobs.class));
              finish();
              Toast toast = Toast.makeText(getBaseContext(), "Deleted All", Toast.LENGTH_LONG);
              View view = toast.getView();
              view.setBackgroundResource(R.color.toast_color);
              toast.show();

                                 }
                         });
                builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id2) {
                        // User clicked OK button

                        db2.deleteRecord("tbl_report_submitted", "record_id = ?", new String[]{values});
                        startActivity(new Intent(Index_request_submitted.this,
                                Index_request_reports_jobs.class));
                        finish();
                        Toast toast = Toast.makeText(getBaseContext(), "Deleted", Toast.LENGTH_LONG);
                        View view = toast.getView();
                        view.setBackgroundResource(R.color.toast_color);
                        toast.show();



                    }
                });

                dialog_box = builder.create();
                dialog_box.show();

                return true;
            }
        });
        new SendData().execute();
        txt_search.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // get the text in the EditText
                String searchString = txt_search.getText().toString();
                int textLength = searchString.length();
                searchResults.clear();

                for (int i = 0; i < originalValues.size(); i++) {
                    String playerName = originalValues.get(i).get("name")
                            .toString();
                    if (textLength <= playerName.length()) {
                        // compare the String in EditText with Names in the
                        // ArrayList
                        if (searchString.equalsIgnoreCase(playerName.substring(
                                0, textLength)))
                            searchResults.add(originalValues.get(i));
                    }
                }

                adapter.notifyDataSetChanged();
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

            }
        });
    }

    private class SendData extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pDialog = new ProgressDialog(Index_request_submitted.this);
            pDialog.setMessage("Fetching data..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            // listview
            // ArrayList thats going to hold the search results

            // get the LayoutInflater for inflating the customomView
            // this will be used in the custom adapter
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            List<Report_Submitted_Jobs> report_submitted_jobs = db
                    .getAllReport_Submitted_Jobs();

            if (!report_submitted_jobs.isEmpty()) {
                // initialize the array
                names = new String[report_submitted_jobs.size()][4];
                photos = new Integer[report_submitted_jobs.size()];

                for (Report_Submitted_Jobs im : report_submitted_jobs) {
                    first_name = string_checker(im.getfirst_name());
                    middle_name = string_checker(im.getmiddle_name());
                    last_name = string_checker(im.getlast_name());


                    names[i][0] = first_name + middle_name + last_name;
                    names[i][1] = im.getrecord_id();
                    names[i][2] = im.getappraisal_type();
                    names[i][3] = im.getappraisal_type();
                    photos[i] = R.drawable.monotoneright_white;
                    i++;

                }
            } else {
                names = new String[1][4];
                photos = new Integer[1];
                names[0][0] = "no record found";
                names[0][1] = "";
                names[0][2] = "";
                names[0][3] = "";
                photos[0] = R.drawable.monotoneright_white;
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub

            originalValues = new ArrayList<HashMap<String, Object>>();

            // temporary HashMap for populating the
            // Items in the ListView
            HashMap<String, Object> temp;

            // total number of rows in the ListView
            int noOfPlayers = names.length;
            int index = 1;
            // now populate the ArrayList players
            for (int i = 0; i < noOfPlayers; i++) {
                temp = new HashMap<String, Object>();

                temp.put("name", names[i][0]);
                temp.put("txt_values", names[i][1]);
                temp.put("app_type", names[i][2]);
                temp.put("app_type_values", names[i][3]);
                temp.put("photo", photos[i]);
                if (noOfPlayers == 1) {
                    temp.put("txt_index", "");
                } else {
                    temp.put("txt_index", index + ".");
                }
                // add the row to the ArrayList
                index++;
                originalValues.add(temp);
            }

            // searchResults=OriginalValues initially
            searchResults = new ArrayList<HashMap<String, Object>>(
                    originalValues);
            // will populate the ListView
            adapter = new CustomAdapter(Index_request_submitted.this,
                    R.layout.index_reports_layout, searchResults);

            // finally,set the adapter to the default ListView
            playerListView.setAdapter(adapter);
            pDialog.dismiss();

        }
    }

    // define your custom adapter
    private class CustomAdapter extends ArrayAdapter<HashMap<String, Object>> {

        public CustomAdapter(Context context, int textViewResourceId,
                             ArrayList<HashMap<String, Object>> Strings) {

            // let android do the initializing :)
            super(context, textViewResourceId, Strings);
        }

        // class for caching the views in a row
        private class ViewHolder {
            ImageView photo;
            TextView name;
            TextView app_type;
            TextView txt_values;
            TextView app_type_values;
            TextView txt_index;
        }

        ViewHolder viewHolder;

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.index_reports_layout,
                        null);
                viewHolder = new ViewHolder();

                // cache the views
                viewHolder.photo = (ImageView) convertView
                        .findViewById(R.id.mono);
                viewHolder.name = (TextView) convertView
                        .findViewById(R.id.txt_image_list);
                viewHolder.app_type = (TextView) convertView
                        .findViewById(R.id.app_type);
                viewHolder.txt_values = (TextView) convertView
                        .findViewById(R.id.txt_values);
                viewHolder.app_type_values = (TextView) convertView
                        .findViewById(R.id.app_type_values);
                viewHolder.txt_index = (TextView) convertView
                        .findViewById(R.id.txt_index);
                // link the cached views to the convertview
                convertView.setTag(viewHolder);

            } else
                viewHolder = (ViewHolder) convertView.getTag();

            int photoId = (Integer) searchResults.get(position).get("photo");

            // set the data to be displayed
            viewHolder.photo.setImageDrawable(getResources().getDrawable(
                    photoId));
            viewHolder.name.setText(searchResults.get(position).get("name")
                    .toString());
            viewHolder.app_type.setText(searchResults.get(position)
                    .get("app_type").toString());
            viewHolder.txt_values.setText(searchResults.get(position)
                    .get("txt_values").toString());
            viewHolder.app_type_values.setText(searchResults.get(position)
                    .get("app_type_values").toString());
            viewHolder.txt_index.setText(searchResults.get(position)
                    .get("txt_index").toString());
            // return the view to be displayed
            return convertView;
        }

    }

    public String string_checker(String old_string) {
        String new_string = old_string;
        if (old_string.equals(" ")) {
            new_string = "";
        } else if (old_string.equals("")) {
            new_string = "";
        } else {
            new_string = old_string + " ";
        }

        return new_string;
    }

    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Intent intent = new Intent(getApplicationContext(),
                Index_request_reports_jobs.class);
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);

    }

    @Override
    public void onBackPressed() {
        this.getParent().onBackPressed();
    }



    @Override
    public void onUserInteraction(){
        st.resetDisconnectTimer();
    }
    @Override
    public void onStop() {
        super.onStop();
        st.stopDisconnectTimer();
    }
    @Override
    public void onResume() {
        super.onResume();
        st.resetDisconnectTimer();
    }
}
