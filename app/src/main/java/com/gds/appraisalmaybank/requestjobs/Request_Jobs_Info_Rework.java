package com.gds.appraisalmaybank.requestjobs;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.attachments.Attachments_Inflater;
import com.gds.appraisalmaybank.check_network.NetworkUtil;
import com.gds.appraisalmaybank.database.Attachments_API;
import com.gds.appraisalmaybank.database.Construction_Ebm_API;
import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Ppcr_API;
import com.gds.appraisalmaybank.database.Report_Accepted_Jobs;
import com.gds.appraisalmaybank.database.Report_Accepted_Jobs_Contacts;
import com.gds.appraisalmaybank.database.Report_filename;
import com.gds.appraisalmaybank.database.Required_Attachments;
import com.gds.appraisalmaybank.database.TableObjects;
import com.gds.appraisalmaybank.database2.Condo_API_Prev_Appraisal;
import com.gds.appraisalmaybank.database2.Condo_API_RDPS;
import com.gds.appraisalmaybank.database2.Condo_API_Title_Details;
import com.gds.appraisalmaybank.database2.Condo_API_Unit_Details;
import com.gds.appraisalmaybank.database2.Condo_API_Valuation;
import com.gds.appraisalmaybank.database2.Construction_Ebm_API_Room_List;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database2.Land_Improvements_API_Cost_Valuation_Details;
import com.gds.appraisalmaybank.database2.Land_Improvements_API_Imp_Details_Features;
import com.gds.appraisalmaybank.database2.Land_Improvements_API_Lot_Valuation_Details;
import com.gds.appraisalmaybank.database2.Ppcr_API_Details;
import com.gds.appraisalmaybank.database2.Townhouse_API_Imp_Details;
import com.gds.appraisalmaybank.database2.Townhouse_API_Imp_Details_Features;
import com.gds.appraisalmaybank.database2.Townhouse_API_Lot_Details;
import com.gds.appraisalmaybank.database2.Townhouse_API_Lot_Valuation_Details;
import com.gds.appraisalmaybank.database2.Townhouse_API_Prev_Appraisal;
import com.gds.appraisalmaybank.database2.Townhouse_API_RDPS;
import com.gds.appraisalmaybank.database2.Vacant_Lot_API_Lot_Details;
import com.gds.appraisalmaybank.database2.Vacant_Lot_API_Lot_Valuation_Details;
import com.gds.appraisalmaybank.database2.Vacant_Lot_API_Prev_Appraisal;
import com.gds.appraisalmaybank.database2.Vacant_Lot_API_RDPS;
import com.gds.appraisalmaybank.database_new.Tables;
import com.gds.appraisalmaybank.json.JSONParser2;
import com.gds.appraisalmaybank.json.MyHttpClient;
import com.gds.appraisalmaybank.json.TLSConnection;
import com.gds.appraisalmaybank.json.UserFunctions;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import org.apache.http.NameValuePair;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

@SuppressLint("InflateParams")
public class Request_Jobs_Info_Rework extends Activity implements
        OnClickListener {

    ActionBar actionBar;
    TableObjects to = new TableObjects();
    private static final String TAG_RECORD_ID = "record_id";
    private static final String TAG_UID = "uid";
    String uid = "";

    Session_Timer st = new Session_Timer(this);
    private static final String TAG_PASS_STAT = "pass_stat";
    String pass_stat = "online";

    private static final String TAG_PASS_APPRAISAL_TYPE = "pass_appraisal_type";
    String pass_appraisal_type;

    Global gs;
    String record_id;
    private ProgressDialog pDialog;

    //for tls
    JSONObject jsonResponse = new JSONObject();
    ArrayList<String> field = new ArrayList<>();
    ArrayList<String> value = new ArrayList<>();
    GDS_methods gds = new GDS_methods();
    TLSConnection tlscon = new TLSConnection();
    TextView tv_requested_month, tv_requested_day, tv_requested_year,
            tv_classification, tv_account_name,tv_company_name, tv_requesting_party,tv_requestor,
            tv_control_no, tv_appraisal_type, tv_nature_appraisal,tv_kind_of_appraisal,
            tv_ins_month1, tv_ins_day1, tv_ins_year1, tv_ins_month2,
            tv_ins_day2, tv_ins_year2, tv_com_month, tv_com_day, tv_com_year,
            tv_tct_no, tv_unit_no, tv_bldg_name, tv_street_no, tv_street_name,
            tv_village, tv_district, tv_zip_code, tv_city, tv_province,
            tv_region, tv_country, tv_address, tv_tct_cct, tv_lot_no,
            tv_block_no,tv_app_request_remarks,tv_app_branch_code;
    TextView tv_vehicle_type, tv_car_model, tv_car_loan_purpose,
            tv_car_mileage, tv_car_series, tv_car_color, tv_car_plate_no,
            tv_car_body_type, tv_car_displacement, tv_car_motor_no,
            tv_car_chassis_no, tv_car_no_cylinders, tv_car_cr_no, tv_car_cr_date;
    String requested_month, requested_day, requested_year, classification,
            account_fname, account_mname, account_lname, requesting_party,requestor,
            control_no, appraisal_type, nature_appraisal,kind_of_appraisal, ins_month1, ins_day1,
            ins_year1, ins_month2, ins_day2, ins_year2, com_month, com_day,
            com_year, tct_no, unit_no, bldg_name, street_no, street_name,
            village, district, zip_code, city, province, region, country,
            lot_no, block_no, counter,app_request_remarks,app_branch_code,app_main_id,app_account_is_company,
            app_account_company_name,app_unacceptable_collateral,application_status;
    // motor_vehicle special fields
    String vehicle_type, car_model, car_loan_purpose, car_mileage, car_series,
            car_color, car_plate_no, car_body_type, car_displacement,
            car_motor_no, car_chassis_no, car_no_cylinders, car_cr_no, car_cr_date_month,
            car_cr_date_day, car_cr_date_year;
    String c_requested_month, c_requested_day, c_requested_year,
            c_classification, c_account_fname, c_account_mname,
            c_account_lname, c_requesting_party, c_requestor,c_control_no,
            output_appraisal_type, c_appraisal_type, c_nature_appraisal,c_kind_of_appraisal,
            c_ins_month1, c_ins_day1, c_ins_year1, c_ins_month2, c_ins_day2,
            c_ins_year2, c_com_month, c_com_day, c_com_year, c_tct_no,
            c_unit_no, c_bldg_name, c_street_no, c_street_name, c_village,
            c_district, c_zip_code, c_city, c_province, c_region, c_country,
            db_street_name, db_village, db_city, db_province, c_lot_no,
            c_block_no,c_app_request_remarks,c_app_branch_code,c_app_main_id,c_app_account_is_company,
            c_app_account_company_name,c_app_unacceptable_collateral,c_application_status;
    String c_vehicle_type, c_car_model, c_car_loan_purpose, c_car_mileage,
            c_car_series, c_car_color, c_car_plate_no, c_car_body_type,
            c_car_motor_no, c_car_cr_no, c_car_displacement, c_car_chassis_no, c_car_no_cylinders,
            c_car_cr_date_month, c_car_cr_date_day, c_car_cr_date_year;
    TextView tv_rework_reason_header, tv_rework_reason;
    String rework_reason, c_rework_reason;
    LinearLayout ll_contact;
    TableRow ll_tct, ll_address, ll_header_address;
    TableRow ll_vehicle_type, ll_car_model, ll_car_loan_purpose,
            ll_car_mileage, ll_car_series, ll_car_color, ll_car_plate_no,
            ll_car_body_type, ll_car_displacement, ll_car_motor_no,
            ll_car_chassis_no, ll_car_no_cylinders,ll_car_cr_no, ll_car_cr_date;
    List<String> ContactsArray_contact_person = new ArrayList<String>();
    List<String> ContactsArray_mobile_prefix = new ArrayList<String>();
    List<String> ContactsArray_mobile = new ArrayList<String>();
    List<String> ContactsArray_landline = new ArrayList<String>();
    String c_contact_person, c_c_mobile_prefix, c_mobile, c_landline;
    // case center attachments
    ImageView btn_mysql_attachments;

    // accept yes or no
    Button btn_yes, btn_no;
    String call_type;
    String decision;
    String app_status;
    String rework_accepted;
    String dialog_status;
    String decline_remarks;
    DatabaseHandler db = new DatabaseHandler(this);
    DatabaseHandler2 db2 = new DatabaseHandler2(this);
    // for API
    String xml;
    String xml2;
    String xmlCondo;
    DefaultHttpClient httpClient = new MyHttpClient(this);
    // dialog
    final Context context = this;
    // uploaded attachments
    LinearLayout container_attachments;
    String uploaded_attachment;
    String url_webby;
    UserFunctions userFunction = new UserFunctions();
    File uploaded_file;
    String[] commandArray = new String[]{"View Attachment in Browser",
            "Download Attachment"};
    Index_request_report irr = new Index_request_report();
    String dir = "gds_appraisal";
    File myDirectory = new File(Environment.getExternalStorageDirectory() + "/"
            + dir + "/");
    int responseCode;
    // Progress dialog type (0 - for Horizontal progress bar)
    public static final int progress_bar_type = 0;

    // for getting attachments
    // Creating JSON Parser object
    JSONParser2 jParser = new JSONParser2();
    // url to get all file list
    // private static String get_files_url =
    // "http://dev.gdslinkasia.com:8080/get_all_uploaded_files.php";
    // private static String get_files_url =
    // "https://gds.securitybank.com:8080/get_all_uploaded_files.php";
    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    int success;
    private static final String TAG_FILES = "files";
    private static final String TAG_FILE = "file";
    private static final String TAG_FILE_NAME = "file_name";
    String file_name = "";
    private static final String TAG_AUTH_KEY = "auth_key";
    String auth_key = "QweAsdZxc";
    // file JSONArray
    JSONArray files = null;

    List<String> id_stack = new ArrayList<String>();
    String id_holder = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_jobs_info);
        Authenticator.setDefault(new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("gdsuser", "gdsuser01"
                        .toCharArray());
            }
        });
        st.resetDisconnectTimer();
        actionBar = getActionBar();
        actionBar.setIcon(R.drawable.ic_rework);

        // getting record_id from intent / previous class
        Intent i = getIntent();
        record_id = i.getStringExtra(TAG_RECORD_ID);
        pass_appraisal_type = i.getStringExtra(TAG_PASS_APPRAISAL_TYPE);
        Toast.makeText(getApplicationContext(), record_id + " " + pass_appraisal_type,
                Toast.LENGTH_SHORT).show();

        container_attachments = (LinearLayout) findViewById(R.id.container_attachments);
        ll_contact = (LinearLayout) findViewById(R.id.container);
        ll_tct = (TableRow) findViewById(R.id.ll_tct);
        ll_address = (TableRow) findViewById(R.id.ll_address);
        ll_header_address = (TableRow) findViewById(R.id.ll_header_address);

        ll_vehicle_type = (TableRow) findViewById(R.id.ll_vehicle_type);
        ll_car_model = (TableRow) findViewById(R.id.ll_car_model);
        ll_car_loan_purpose = (TableRow) findViewById(R.id.ll_car_loan_purpose);
        ll_car_mileage = (TableRow) findViewById(R.id.ll_car_mileage);
        ll_car_series = (TableRow) findViewById(R.id.ll_car_series);
        ll_car_color = (TableRow) findViewById(R.id.ll_car_color);
        ll_car_plate_no = (TableRow) findViewById(R.id.ll_car_plate_no);
        ll_car_body_type = (TableRow) findViewById(R.id.ll_car_body_type);
        ll_car_displacement = (TableRow) findViewById(R.id.ll_car_displacement);
        ll_car_motor_no = (TableRow) findViewById(R.id.ll_car_motor_no);
        ll_car_chassis_no = (TableRow) findViewById(R.id.ll_car_chassis_no);
        ll_car_no_cylinders = (TableRow) findViewById(R.id.ll_car_no_cylinders);
        ll_car_cr_no = (TableRow) findViewById(R.id.ll_car_cr_no);
        ll_car_cr_date = (TableRow) findViewById(R.id.ll_car_cr_date);

        tv_requested_month = (TextView) findViewById(R.id.tv_requested_month);
        tv_requested_day = (TextView) findViewById(R.id.tv_requested_day);
        tv_requested_year = (TextView) findViewById(R.id.tv_requested_year);
        tv_classification = (TextView) findViewById(R.id.tv_classification);
        tv_account_name = (TextView) findViewById(R.id.tv_account_name);
        tv_company_name = (TextView) findViewById(R.id.tv_company_name);
        tv_requesting_party = (TextView) findViewById(R.id.tv_requesting_party);
        tv_requestor = (TextView) findViewById(R.id.tv_requestor);
        tv_control_no = (TextView) findViewById(R.id.tv_control_no);
        tv_appraisal_type = (TextView) findViewById(R.id.tv_appraisal_type);
        tv_nature_appraisal = (TextView) findViewById(R.id.tv_nature_appraisal);
        tv_kind_of_appraisal = (TextView) findViewById(R.id.tv_kind_of_appraisal);
        tv_ins_month1 = (TextView) findViewById(R.id.tv_ins_month1);
        tv_ins_day1 = (TextView) findViewById(R.id.tv_ins_day1);
        tv_ins_year1 = (TextView) findViewById(R.id.tv_ins_year1);
        tv_ins_month2 = (TextView) findViewById(R.id.tv_ins_month2);
        tv_ins_day2 = (TextView) findViewById(R.id.tv_ins_day2);
        tv_ins_year2 = (TextView) findViewById(R.id.tv_ins_year2);
        tv_com_month = (TextView) findViewById(R.id.tv_com_month);
        tv_com_day = (TextView) findViewById(R.id.tv_com_day);
        tv_com_year = (TextView) findViewById(R.id.tv_com_year);
        tv_tct_no = (TextView) findViewById(R.id.tv_tct_no);
        tv_unit_no = (TextView) findViewById(R.id.tv_unit_no);
        tv_bldg_name = (TextView) findViewById(R.id.tv_bldg_name);
        tv_street_no = (TextView) findViewById(R.id.tv_street_no);
        tv_lot_no = (TextView) findViewById(R.id.tv_lot_no);
        tv_block_no = (TextView) findViewById(R.id.tv_block_no);
        tv_street_name = (TextView) findViewById(R.id.tv_street_name);
        tv_village = (TextView) findViewById(R.id.tv_village);
        tv_district = (TextView) findViewById(R.id.tv_district);
        tv_zip_code = (TextView) findViewById(R.id.tv_zip_code);
        tv_city = (TextView) findViewById(R.id.tv_city);
        tv_province = (TextView) findViewById(R.id.tv_province);
        tv_region = (TextView) findViewById(R.id.tv_region);
        tv_country = (TextView) findViewById(R.id.tv_country);
        tv_address = (TextView) findViewById(R.id.tv_address);
        tv_tct_cct = (TextView) findViewById(R.id.tv_tct_cct);
        tv_app_request_remarks = (TextView) findViewById(R.id.tv_app_request_remarks);
        tv_app_branch_code = (TextView) findViewById(R.id.tv_app_branch_code);

        tv_vehicle_type = (TextView) findViewById(R.id.tv_vehicle_type);
        tv_car_model = (TextView) findViewById(R.id.tv_car_model);
        tv_car_loan_purpose = (TextView) findViewById(R.id.tv_car_loan_purpose);
        tv_car_mileage = (TextView) findViewById(R.id.tv_car_mileage);
        tv_car_series = (TextView) findViewById(R.id.tv_car_series);
        tv_car_color = (TextView) findViewById(R.id.tv_car_color);
        tv_car_plate_no = (TextView) findViewById(R.id.tv_car_plate_no);
        tv_car_body_type = (TextView) findViewById(R.id.tv_car_body_type);
        tv_car_displacement = (TextView) findViewById(R.id.tv_car_displacement);
        tv_car_motor_no = (TextView) findViewById(R.id.tv_car_motor_no);
        tv_car_chassis_no = (TextView) findViewById(R.id.tv_car_chassis_no);
        tv_car_no_cylinders = (TextView) findViewById(R.id.tv_car_no_cylinders);
        tv_car_cr_no = (TextView) findViewById(R.id.tv_car_cr_no);
        tv_car_cr_date = (TextView) findViewById(R.id.tv_car_cr_date);

        tv_rework_reason_header = (TextView) findViewById(R.id.tv_rework_reason_header);
        tv_rework_reason = (TextView) findViewById(R.id.tv_rework_reason);
        tv_rework_reason_header.setVisibility(View.VISIBLE);
        tv_rework_reason.setVisibility(View.VISIBLE);

        btn_yes = (Button) findViewById(R.id.btn_yes);
        btn_no = (Button) findViewById(R.id.btn_no);
        btn_yes.setOnClickListener(this);
        btn_no.setOnClickListener(this);
        gs = ((Global) getApplicationContext());
        record_id = gs.record_id;
        call_type = "fetch";

        btn_mysql_attachments = (ImageView) findViewById(R.id.btn_mysql_attachments);
        btn_mysql_attachments.setOnClickListener(this);
        new FetchData().execute();

    }

    public void fetch_data() {

        JSONObject json_query = new JSONObject();
        String query;
        try {
            json_query.put("system.record_id", record_id);
            query = json_query.toString();
            // API

            //for tls
            field.clear();
            field.add("auth_token");
            field.add("query");

            value.clear();
            value.add(gs.auth_token);
            value.add(query);

            if(Global.type.contentEquals("tls")){
                jsonResponse = gds.makeHttpsRequest(gs.show_url, "POST", field, value);
                xml = jsonResponse.toString();
            }else{
                xml=gds.http_posting(field,value,gs.show_url,Request_Jobs_Info_Rework.this);
            }


            xml2 = xml;
            xmlCondo = xml;
            Log.e("response", xml);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }  catch (Exception e) {
            Log.e("", e.toString());
        }

        try {
            JSONObject records = new JSONObject(xml);
            counter = records.getString("Counter");
            counter = String.valueOf(Integer.parseInt(counter) - 1);
            requested_month = records.getString("app_daterequested_month");
            requested_day = records.getString("app_daterequested_day");
            requested_year = records.getString("app_daterequested_year");

            account_fname = records.getString("app_account_first_name");
            account_mname = records.getString("app_account_middle_name");
            account_lname = records.getString("app_account_last_name");

            requesting_party = records.getString("app_requesting_party");
            requestor = records.getString("app_requestor");
            app_request_remarks = records.getString("app_request_remarks");
            app_main_id = records.getString("app_main_id");
            app_account_is_company = records.getString("app_account_is_company");
            app_account_company_name = records.getString("app_account_company_name");
            app_unacceptable_collateral = records.getString("app_unacceptable_collateral");
            application_status = records.getString("application_status");

            if (records.has("approval_decline_reasons")) {
                rework_reason = records.getString("approval_decline_reasons");
            } else {
                rework_reason = "";
            }


            JSONArray appraisal_request_array = records
                    .getJSONArray("appraisal_request");
            for (int x = 0; x < appraisal_request_array.length(); x++) {
                JSONObject appraisal_request_records = appraisal_request_array
                        .getJSONObject(x);

                control_no = appraisal_request_records
                        .getString("app_control_no");
                appraisal_type = appraisal_request_records
                        .getString("app_request_appraisal");
                nature_appraisal = appraisal_request_records
                        .getString("app_purpose_appraisal");
                kind_of_appraisal = appraisal_request_records
                        .getString("app_kind_of_appraisal");

                app_branch_code = appraisal_request_records.getString("app_branch_code");
                // -------------------------------------//
                // catch if CC table has this field name
                if (appraisal_request_array.getJSONObject(x).has(
                        "app_pref_inspection_date1_month")) {
                    ins_month1 = appraisal_request_records
                            .getString("app_pref_inspection_date1_month");
                } else if (appraisal_request_array.getJSONObject(x).has(
                        "app_application_reference")) {
                    ins_month1 = appraisal_request_records
                            .getString("app_application_reference");
                } else {
                    ins_month1 = "";
                }
                // ins_month1 = appraisal_request_records
                // .getString("app_pref_inspection_date1_month");
                // -------------------------------------//app_pref_inspection_date1_month
                // --- app_application_reference
                ins_day1 = appraisal_request_records
                        .getString("app_pref_inspection_date1_day");
                ins_year1 = appraisal_request_records
                        .getString("app_pref_inspection_date1_year");
                ins_month2 = appraisal_request_records
                        .getString("app_pref_inspection_date2_month");
                ins_day2 = appraisal_request_records
                        .getString("app_pref_inspection_date2_day");
                ins_year2 = appraisal_request_records
                        .getString("app_pref_inspection_date2_year");
                com_month = appraisal_request_records
                        .getString("app_pref_completion_date_month");
                com_day = appraisal_request_records
                        .getString("app_pref_completion_date_day");
                com_year = appraisal_request_records
                        .getString("app_pref_completion_date_year");

                // catch if CC table has this field name
                // motor_vehicle special fields
                if (appraisal_request_array.getJSONObject(x).has(
                        "app_vehicle_type")) {
                    vehicle_type = appraisal_request_records
                            .getString("app_vehicle_type");
                } else {
                    vehicle_type = "";
                }
                if (appraisal_request_array.getJSONObject(x).has(
                        "app_car_model")) {
                    car_model = appraisal_request_records
                            .getString("app_car_model");
                } else {
                    car_model = "";
                }
                if (appraisal_request_array.getJSONObject(x).has(
                        "app_car_loan_purpose")) {
                    car_loan_purpose = appraisal_request_records
                            .getString("app_car_loan_purpose");
                } else {
                    car_loan_purpose = "";
                }
                if (appraisal_request_array.getJSONObject(x).has(
                        "app_car_mileage")) {
                    car_mileage = appraisal_request_records
                            .getString("app_car_mileage");
                } else {
                    car_mileage = "";
                }
                if (appraisal_request_array.getJSONObject(x).has(
                        "app_car_series")) {
                    car_series = appraisal_request_records
                            .getString("app_car_series");
                } else {
                    car_series = "";
                }
                if (appraisal_request_array.getJSONObject(x).has(
                        "app_car_color")) {
                    car_color = appraisal_request_records
                            .getString("app_car_color");
                } else {
                    car_color = "";
                }
                if (appraisal_request_array.getJSONObject(x).has(
                        "app_car_plate_no")) {
                    car_plate_no = appraisal_request_records
                            .getString("app_car_plate_no");
                } else {
                    car_plate_no = "";
                }
                if (appraisal_request_array.getJSONObject(x).has(
                        "app_car_body_type")) {
                    car_body_type = appraisal_request_records
                            .getString("app_car_body_type");
                } else {
                    car_body_type = "";
                }
                if (appraisal_request_array.getJSONObject(x).has(
                        "app_car_displacement")) {
                    car_displacement = appraisal_request_records
                            .getString("app_car_displacement");
                } else {
                    car_displacement = "";
                }
                if (appraisal_request_array.getJSONObject(x).has(
                        "app_car_motor_no")) {
                    car_motor_no = appraisal_request_records
                            .getString("app_car_motor_no");
                } else {
                    car_motor_no = "";
                }
                if (appraisal_request_array.getJSONObject(x).has(
                        "app_car_chassis_no")) {
                    car_chassis_no = appraisal_request_records
                            .getString("app_car_chassis_no");
                } else {
                    car_chassis_no = "";
                }
                if (appraisal_request_array.getJSONObject(x).has(
                        "app_car_no_of_cylinders")) {
                    car_no_cylinders = appraisal_request_records
                            .getString("app_car_no_of_cylinders");
                } else {
                    car_no_cylinders = "";
                }
                if (appraisal_request_array.getJSONObject(x).has(
                        "app_car_cr_no")) {
                    car_cr_no = appraisal_request_records
                            .getString("app_car_cr_no");
                } else {
                    car_cr_no = "";
                }
                if (appraisal_request_array.getJSONObject(x).has(
                        "app_car_cr_date_month")) {
                    car_cr_date_month = appraisal_request_records
                            .getString("app_car_cr_date_month");
                } else {
                    car_cr_date_month = "";
                }
                if (appraisal_request_array.getJSONObject(x).has(
                        "app_car_cr_date_day")) {
                    car_cr_date_day = appraisal_request_records
                            .getString("app_car_cr_date_day");
                } else {
                    car_cr_date_day = "";
                }
                if (appraisal_request_array.getJSONObject(x).has(
                        "app_car_cr_date_year")) {
                    car_cr_date_year = appraisal_request_records
                            .getString("app_car_cr_date_year");
                } else {
                    car_cr_date_year = "";
                }

                // collateral address
                JSONArray app_collateral_address_array = appraisal_request_records
                        .getJSONArray("app_collateral_address");
                for (int i = 0; i < app_collateral_address_array.length(); i++) {
                    JSONObject app_collateral_address_records = app_collateral_address_array
                            .getJSONObject(i);
                    tct_no = app_collateral_address_records
                            .getString("app_tct_no");
                    unit_no = app_collateral_address_records
                            .getString("app_unit_no");
                    bldg_name = app_collateral_address_records
                            .getString("app_bldg");
                    street_no = app_collateral_address_records
                            .getString("app_street_no");
                    lot_no = app_collateral_address_records
                            .getString("app_lot_no");
                    block_no = app_collateral_address_records
                            .getString("app_block_no");
                    street_name = app_collateral_address_records
                            .getString("app_street_name");
                    village = app_collateral_address_records
                            .getString("app_village");
                    district = app_collateral_address_records
                            .getString("app_district");
                    zip_code = app_collateral_address_records
                            .getString("app_zip");
                    city = app_collateral_address_records.getString("app_city");
                    province = app_collateral_address_records
                            .getString("app_province");
                    region = app_collateral_address_records
                            .getString("app_region");
                    country = app_collateral_address_records
                            .getString("app_country");

                }
                // contact person
                JSONArray app_contact_person_array = appraisal_request_records
                        .getJSONArray("app_contact_person");
                for (int i = 0; i < app_contact_person_array.length(); i++) {
                    JSONObject app_contact_person_records = app_contact_person_array
                            .getJSONObject(i);
                    ContactsArray_contact_person.add(app_contact_person_records
                            .getString("app_contact_person_name"));
                    ContactsArray_mobile_prefix.add(app_contact_person_records
                            .getString("app_mobile_prefix"));
                    ContactsArray_mobile.add(app_contact_person_records
                            .getString("app_mobile_no"));
                    ContactsArray_landline.add(app_contact_person_records
                            .getString("app_telephone_no"));
                }
            }
            // set uid for attachments class
            uid = app_main_id;

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void fill() {
        c_requested_month = string_checker(requested_month);
        c_requested_day = string_checker(requested_day);
        c_requested_year = string_checker(requested_year);
        c_account_fname = string_checker(account_fname);
        c_account_mname = string_checker(account_mname);
        c_account_lname = string_checker(account_lname);
        c_requesting_party = string_checker(requesting_party);
        c_requestor = string_checker(requestor);
        c_control_no = string_checker(control_no);
        c_appraisal_type = string_checker(appraisal_type);
        output_appraisal_type = string_checker(appraisal_type);
        c_nature_appraisal = string_checker(nature_appraisal);
        c_kind_of_appraisal = string_checker(kind_of_appraisal);
        c_ins_month1 = string_checker(ins_month1);
        c_ins_day1 = string_checker(ins_day1);
        c_ins_year1 = string_checker(ins_year1);
        c_ins_month2 = string_checker(ins_month2);
        c_ins_day2 = string_checker(ins_day2);
        c_ins_year2 = string_checker(ins_year2);
        c_com_month = string_checker(com_month);
        c_com_day = string_checker(com_day);
        c_com_year = string_checker(com_year);
        c_tct_no = string_checker(tct_no);
        c_unit_no = string_checker(unit_no);
        c_bldg_name = string_checker(bldg_name);
        c_street_no = string_checker(street_no);
        c_lot_no = string_checker(lot_no);
        c_block_no = string_checker(block_no);
        c_app_request_remarks = string_checker(app_request_remarks);
        c_app_main_id = string_checker(app_main_id);
        c_app_account_is_company = gds.nullCheck3(app_account_is_company);
        c_app_account_company_name = string_checker(app_account_company_name);
        c_app_unacceptable_collateral = string_checker(app_unacceptable_collateral);
        c_application_status = string_checker(application_status);
        c_app_branch_code = string_checker(app_branch_code);
        if (!string_checker(street_name).equals("")) {
            c_street_name = string_checker(street_name + ", ");
        } else {
            c_street_name = string_checker(street_name);
        }
        if (!string_checker(village).equals("")) {
            c_village = string_checker(village + ", ");
        } else {
            c_village = string_checker(village);
        }
        if (!string_checker(city).equals("")) {
            c_city = string_checker(city + ", ");
        } else {
            c_city = string_checker(city);
        }
        if (!string_checker(province).equals("")) {
            c_province = string_checker(province + ", ");
        } else {
            c_province = string_checker(province);
        }
        db_street_name = string_checker(street_name);
        db_village = string_checker(village);
        db_city = string_checker(city);
        db_province = string_checker(province);
        c_district = string_checker(district);
        c_zip_code = string_checker(zip_code);
        c_region = string_checker(region);
        c_country = string_checker(country);

        c_vehicle_type = string_checker(vehicle_type);
        c_car_model = string_checker(car_model);
        c_car_loan_purpose = string_checker(car_loan_purpose);
        c_car_mileage = string_checker(car_mileage);
        c_car_series = string_checker(car_series);
        c_car_color = string_checker(car_color);
        c_car_plate_no = string_checker(car_plate_no);
        c_car_body_type = string_checker(car_body_type);
        c_car_displacement = string_checker(car_displacement);
        c_car_motor_no = string_checker(car_motor_no);
        c_car_chassis_no = string_checker(car_chassis_no);
        c_car_no_cylinders = string_checker(car_no_cylinders);
        c_car_cr_no = string_checker(car_cr_no);
        c_car_cr_date_month = string_checker(car_cr_date_month);
        c_car_cr_date_day = string_checker(car_cr_date_day);
        c_car_cr_date_year = string_checker(car_cr_date_year);

        c_rework_reason = string_checker(rework_reason);
        tv_rework_reason.setText(c_rework_reason);

        tv_vehicle_type.setText(c_vehicle_type);
        tv_car_model.setText(c_car_model);
        tv_car_loan_purpose.setText(c_car_loan_purpose);
        tv_car_mileage.setText(c_car_mileage);
        tv_car_series.setText(c_car_series);
        tv_car_color.setText(c_car_color);
        tv_car_plate_no.setText(c_car_plate_no);
        tv_car_body_type.setText(c_car_body_type);
        tv_car_displacement.setText(c_car_displacement);
        tv_car_motor_no.setText(c_car_motor_no);
        tv_car_chassis_no.setText(c_car_chassis_no);
        tv_car_no_cylinders.setText(c_car_no_cylinders);
        tv_car_cr_no.setText(c_car_cr_no);
        tv_car_cr_date.setText(c_car_cr_date_month + "/" + c_car_cr_date_day
                + "/" + c_car_cr_date_year);

        tv_requested_month.setText(c_requested_month + "/");
        tv_requested_day.setText(c_requested_day + "/");
        tv_requested_year.setText(c_requested_year);

        tv_account_name.setText(c_account_fname + " " + c_account_mname + " "
                + c_account_lname);
        tv_company_name.setText(app_account_company_name);
        if(app_account_is_company.contentEquals("true")){
            actionBar.setTitle(app_account_company_name);

        }else{
            actionBar.setTitle(gds.nullCheck3(account_fname) + " " + gds.nullCheck3(account_mname) + " " + gds.nullCheck3(account_lname));
        }
        actionBar.setSubtitle("Re-work");

        tv_requesting_party.setText(c_requesting_party);
        tv_requestor.setText(c_requestor);
        tv_control_no.setText(c_control_no);
        for (int x = 0; x < gs.appraisal_type_value.length; x++) {
            if (c_appraisal_type.equals(gs.appraisal_type_value[x])) {
                output_appraisal_type = gs.appraisal_output[x];
            }
        }
        tv_appraisal_type.setText(output_appraisal_type);
        if (c_appraisal_type.equals("vacant_lot")
                || c_appraisal_type.equals("land_improvements")
                || c_appraisal_type.equals("construction_ebm")) {
            tv_tct_cct.setText("TCT No.");
        } else if (c_appraisal_type.equals("townhouse_condo")) {
            tv_tct_cct.setText("CCT No.");
        }
        tv_nature_appraisal.setText(c_nature_appraisal);
        tv_kind_of_appraisal.setText(c_kind_of_appraisal + "\n");
        tv_ins_month1.setText(c_ins_month1 + "/");
        tv_ins_day1.setText(c_ins_day1 + "/");
        tv_ins_year1.setText(c_ins_year1);
        tv_ins_month2.setText(c_ins_month2 + "/");
        tv_ins_day2.setText(c_ins_day2 + "/");
        tv_ins_year2.setText(c_ins_year2);
        tv_com_month.setText(c_com_month + "/");
        tv_com_day.setText(c_com_day + "/");
        tv_com_year.setText(c_com_year);

        tv_tct_no.setText(c_tct_no);
        tv_unit_no.setText(c_unit_no);
        tv_bldg_name.setText(c_bldg_name);
        tv_street_no.setText(c_street_no);
        tv_lot_no.setText(c_lot_no);
        tv_block_no.setText(c_block_no);
        tv_street_name.setText(c_street_name);
        tv_village.setText(c_village);
        tv_district.setText(c_district);
        tv_zip_code.setText(c_zip_code);
        tv_city.setText(c_city);
        tv_province.setText(c_province);
        tv_region.setText(c_region);
        tv_country.setText(c_country);
        tv_app_request_remarks.setText(c_app_request_remarks);
        tv_app_branch_code.setText(c_app_branch_code);
        if (c_appraisal_type.equals("motor_vehicle")) {
            ll_tct.setVisibility(View.GONE);
            ll_address.setVisibility(View.GONE);
            ll_header_address.setVisibility(View.GONE);

            ll_vehicle_type.setVisibility(View.VISIBLE);
            ll_car_model.setVisibility(View.VISIBLE);
            ll_car_loan_purpose.setVisibility(View.VISIBLE);
            ll_car_mileage.setVisibility(View.VISIBLE);
            ll_car_series.setVisibility(View.VISIBLE);
            ll_car_color.setVisibility(View.VISIBLE);
            ll_car_plate_no.setVisibility(View.VISIBLE);
            ll_car_body_type.setVisibility(View.VISIBLE);
            ll_car_displacement.setVisibility(View.VISIBLE);
            ll_car_motor_no.setVisibility(View.VISIBLE);
            ll_car_chassis_no.setVisibility(View.VISIBLE);
            ll_car_no_cylinders.setVisibility(View.VISIBLE);
            ll_car_cr_no.setVisibility(View.VISIBLE);
            ll_car_cr_date.setVisibility(View.VISIBLE);
        }
        tv_address.setText(c_unit_no + " " + c_bldg_name + " " + c_lot_no + " "
                + c_block_no + " " + c_street_name + c_village + c_district
                + " " + c_city + c_region + " " + c_province + c_country + " "
                + c_zip_code);
        // contact
        for (int x = 0; x < ContactsArray_contact_person.size(); x++) {
            LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View addView = layoutInflater.inflate(
                    R.layout.request_jobs_info_contact_layout, null);

            final TextView tv_contact_person = (TextView) addView
                    .findViewById(R.id.tv_contact_person);
            final TextView tv_contact_prefix = (TextView) addView
                    .findViewById(R.id.tv_contact_prefix);
            final TextView tv_contact_mobile = (TextView) addView
                    .findViewById(R.id.tv_contact_mobile);
            final TextView tv_contact_landline = (TextView) addView
                    .findViewById(R.id.tv_contact_landline);
            c_contact_person = string_checker(ContactsArray_contact_person
                    .get(x));
            c_c_mobile_prefix = string_checker(ContactsArray_mobile_prefix
                    .get(x));
            c_mobile = string_checker(ContactsArray_mobile.get(x));
            c_landline = string_checker(ContactsArray_landline.get(x));

            tv_contact_person.setText(c_contact_person);
            tv_contact_prefix.setText(c_c_mobile_prefix);
            tv_contact_mobile.setText(c_mobile);
            tv_contact_landline.setText(c_landline);
            addView.setEnabled(false);
            addView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    db.addReport_Accepted_Jobs_Contacts(new Report_Accepted_Jobs_Contacts(
                            record_id, tv_contact_person.getText().toString(),
                            tv_contact_prefix.getText().toString(),
                            tv_contact_mobile.getText().toString(),
                            tv_contact_landline.getText().toString()));
                }
            });
            ll_contact.addView(addView);
        }
    }

    private class FetchData extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pDialog = new ProgressDialog(Request_Jobs_Info_Rework.this);
            if (call_type.equals("fetch")) {
                pDialog.setMessage("Fetching data..");
            } else if (call_type.equals("update")) {
                pDialog.setMessage("Updating data..");
            }

            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            // TODO Auto-generated method stub

            if (call_type.equals("fetch")) {
                fetch_data();
            } else if (call_type.equals("update")) {
                update_job();
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            pDialog.dismiss();
            if (call_type.equals("fetch")) {
                fill();
                uploaded_attachments();
            } else if (call_type.equals("update")) {
                if (decision.equals("yes")) {
                    save_db();

                    //go back to main tab class
                    Intent i = new Intent(getApplicationContext(), Index_request_reports_jobs.class);
                    // set the new task and clear flags
//					i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }else{

                    //go back to main tab class
                    Intent i = new Intent(getApplicationContext(), Index_request_reports_jobs.class);
                    // set the new task and clear flags
//					i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
                finish();
            }

        }
    }

    public void update_dialog() {
        // custom dialog
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.yes_no_dialog);

        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_question);
        text.setText("Are you sure you want to " + dialog_status
                + " this Job ?");
        Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

        // if button is clicked, close the custom dialog
        btn_yes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                new FetchData().execute();

            }
        });
        btn_no.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void update_job() {
        String query, data;
        if (decision.equals("yes")) {
            app_status = "for_rework";
            rework_accepted = "true";
        } else if (decision.equals("no")) {
            app_status = "decline_job";
        }
        JSONObject json_query = new JSONObject();
        JSONObject data_query = new JSONObject();
        JSONObject data_status = new JSONObject();
        try {
            json_query.put("system.record_id", record_id);
            //data_status.put("app_declined_reason", decline_remarks);
            data_status.put("application_status", app_status);
            data_status.put("rework_accepted", rework_accepted);
            data_query.put("record", data_status);

            query = json_query.toString();
            data = data_query.toString();

            // API
            //for tls
            JSONObject jsonResponse = new JSONObject();
            ArrayList<String> field = new ArrayList<>();
            ArrayList<String> value = new ArrayList<>();
            field.clear();
            field.add("auth_token");
            field.add("query");
            field.add("data");

            value.clear();
            value.add(gs.auth_token);
            value.add(query);
            value.add(data);

            if(Global.type.contentEquals("tls")){
                jsonResponse = gds.makeHttpsRequest(gs.update_url, "POST", field, value);
                xml = jsonResponse.toString();
            }else{
                xml=gds.http_posting(field,value,gs.update_url,Request_Jobs_Info_Rework.this);
            }
           /* HttpPost httpPost = new HttpPost(gs.update_url);
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("auth_token", gs.auth_token));
            params.add(new BasicNameValuePair("query", query));
            params.add(new BasicNameValuePair("data", data));
            httpPost.setEntity(new UrlEncodedFormEntity(params));
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity httpEntity = response.getEntity();
            xml = EntityUtils.toString(httpEntity);*/
            //Log.e("request", params.toString());
            Log.e("response", xml);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.e("", e.toString());
        }

    }

    public String string_checker(String old_string) {
        String new_string = old_string;
        if (old_string.equals("null")) {
            new_string = "";
        } else if (old_string.equals("0")) {
            new_string = "";
        }

        return new_string;
    }

    public void save_db() {

        db.addReport_Accepted_Jobs(new Report_Accepted_Jobs(record_id,
                c_requested_month, c_requested_day, c_requested_year,
                c_classification, c_account_fname, c_account_mname,
                c_account_lname, c_requesting_party,c_requestor, c_control_no,
                c_appraisal_type, c_nature_appraisal, c_ins_month1, c_ins_day1,
                c_ins_year1, c_ins_month2, c_ins_day2, c_ins_year2,
                c_com_month, c_com_day, c_com_year, c_tct_no, c_unit_no,
                c_bldg_name, c_street_no, db_street_name, db_village,
                c_district, c_zip_code, db_city, db_province, c_region,
                c_country, counter, c_vehicle_type, c_car_model, c_lot_no,
                c_block_no, c_car_loan_purpose, c_car_mileage, c_car_series,
                c_car_color, c_car_plate_no, c_car_body_type,
                c_car_displacement, c_car_motor_no, c_car_chassis_no, c_car_no_cylinders,
                c_car_cr_no, c_car_cr_date_month, c_car_cr_date_day,
                c_car_cr_date_year, c_rework_reason,c_kind_of_appraisal,c_app_request_remarks,c_app_branch_code,c_app_main_id));
        field.clear();
        field.add("app_account_is_company");
        field.add("app_account_company_name");
        field.add("app_unacceptable_collateral");
        field.add("application_status");
        value.clear();
        value.add(c_app_account_is_company);
        value.add(c_app_account_company_name);
        value.add(c_app_unacceptable_collateral);
        value.add(c_application_status);
        db2.updateRecord(value,field,"record_id = ?",new String[] { record_id },"tbl_report_accepted_jobs");
        db.addReport_filename(new Report_filename(record_id, "", "", "", "", ""));

        // contact
        int childcount = ll_contact.getChildCount();
        for (int i = 0; i < childcount; i++) {
            View view = ll_contact.getChildAt(i);
            view.performClick();
        }
        if (c_appraisal_type.equals("townhouse_condo")) {
            syncCondo();
        } else if (c_appraisal_type.equals("motor_vehicle") || (c_appraisal_type.equals("machinery_equipment"))) {
            syncMotorVehicle();
        } else if (c_appraisal_type.equals("vacant_lot")) {
            syncVacantLot();
        } else if (c_appraisal_type.equals("land_improvements")) {
            syncLandImprovements();
        } else if (c_appraisal_type.equals("construction_ebm")) {
            syncConstructionEbm();
        } else if (c_appraisal_type.equals("ppcr")) {
            syncPpcr();
        } else if (c_appraisal_type.equals("townhouse")) {
            syncTownhouse();
        }else if (c_appraisal_type.equals("waf")) {
            syncWaf();
        }else if (c_appraisal_type.equals("spv_land_improvements")) {
            syncSpv();
        }
        new LoadAllFiles().execute();
        finish();
        Toast.makeText(getApplicationContext(), "Request Job Accepted",
                Toast.LENGTH_SHORT).show();

    }
    /* @Override
     public void onBackPressed() {
         finish();
     }*/
    public void remarks_decline_dialog() {
        // custom dialog
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.input_dialog);

        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_question);
        final EditText txt_remarks = (EditText) dialog
                .findViewById(R.id.txt_remarks);
        text.setText("Decline Remarks");
        Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

        // if button is clicked, close the custom dialog
        btn_yes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                decline_remarks = txt_remarks.getText().toString();
                dialog.dismiss();
                update_dialog();
            }
        });
        btn_no.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void uploaded_attachments() {
        // set value
        List<Required_Attachments> required_attachments = db
                .getAllRequired_Attachments_byid(String.valueOf(appraisal_type));
        if (!required_attachments.isEmpty()) {
            for (final Required_Attachments ra : required_attachments) {
                LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                final View addView = layoutInflater.inflate(
                        R.layout.report_uploaded_attachments_dynamic, null);
                final TextView tv_attachment = (TextView) addView
                        .findViewById(R.id.tv_attachment);
                final ImageView btn_view = (ImageView) addView
                        .findViewById(R.id.btn_view);
                tv_attachment.setText(ra.getattachment());
                btn_view.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        uploaded_attachment = account_lname + "_"
                                + account_fname + "_" + requested_month
                                + requested_day + requested_year + "_"
                                + ra.getattachment() + "_" + counter + ".pdf";
                        // directory
                        uploaded_file = new File(myDirectory,
                                uploaded_attachment);
                        uploaded_attachment = uploaded_attachment.replaceAll(
                                " ", "%20");
                        url_webby = gs.pdf_loc_url + uploaded_attachment;
                        view_pdf(uploaded_file);
                    }
                });
                container_attachments.addView(addView);

            }

        }

    }

    public void view_pdf(File file) {
        if (file.exists()) {
            Uri path = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(getApplicationContext(),
                        "No Application Available to View PDF",
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            NetworkUtil.getConnectivityStatusString(this);
            if (!NetworkUtil.status.equals("Network not available")) {
                new Attachment_validation().execute();
            } else {
                Toast.makeText(getApplicationContext(),
                        "Network not available", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private class Attachment_validation extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pDialog = new ProgressDialog(Request_Jobs_Info_Rework.this);
            pDialog.setMessage("Checking attachment..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            // TODO Auto-generated method stub

            try {
                URL obj = new URL(url_webby);
                HttpsURLConnection con;
                HttpURLConnection con2;
                if(Global.type.contentEquals("tls")){
                    con = tlscon.setUpHttpsConnection("" +obj);
                    con.setRequestMethod("GET");
                    con.setRequestProperty("User-Agent", "Mozilla/5.0");
                    responseCode = con.getResponseCode();
                }else{
                    con2 = (HttpURLConnection) obj.openConnection();
                    con2.setRequestMethod("GET");
                    con2.setRequestProperty("User-Agent", "Mozilla/5.0");
                    responseCode = con2.getResponseCode();
                }
                Log.e("", String.valueOf(responseCode));
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            Log.e("", url_webby);
            if (responseCode == 200) {
                view_download();
            } else if (responseCode == 404) {
                Toast.makeText(getApplicationContext(),
                        "Attachment doesn't exist", Toast.LENGTH_SHORT).show();
            }
            pDialog.dismiss();

        }
    }

    private void view_download() {
        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Actions :");
        builder.setItems(commandArray, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int index) {
                if (index == 0) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
                            .parse(url_webby));
                    startActivity(browserIntent);
                    dialog.dismiss();
                } else if (index == 1) {
                    new DownloadFileFromURL().execute(url_webby);
                    dialog.dismiss();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * Showing Dialog
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type:
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.setCanceledOnTouchOutside(false);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    /**
     * Background Async Task to download file
     */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                // Output stream to write file
                OutputStream output = new FileOutputStream(
                        uploaded_file.toString());

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            removeDialog(progress_bar_type);
            Toast.makeText(getApplicationContext(), "Attachment Downloaded",
                    Toast.LENGTH_LONG).show();
            view_pdf(uploaded_file);

        }

    }

    /**
     * Background Async Task to Load all product by making HTTP Request
     */
    class LoadAllFiles extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Request_Jobs_Info_Rework.this);
            pDialog.setMessage("Loading files. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting All files from url
         */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            //for tls
            field.clear();
            field.add(TAG_UID);
            field.add(TAG_AUTH_KEY);

            value.clear();
            value.add(uid);
            value.add(auth_key);
            if(Global.type.contentEquals("tls")){
                jsonResponse = gds.makeHttpsRequest(gs.get_files_url, "POST", field, value);
            }else{
                params.add(new BasicNameValuePair(TAG_UID, uid));
                params.add(new BasicNameValuePair(TAG_AUTH_KEY, auth_key));
                // getting JSON string from URL
                jsonResponse = gds.makeHttpRequest(gs.get_files_url, "POST", params);
            }
            //change json to jsonResponse
            // Check your log cat for JSON reponse
            Log.d("All Files: ", jsonResponse.toString());

            try {
                // Checking for SUCCESS TAG
                success = jsonResponse.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // products found
                    // Getting Array of Products
                    files = jsonResponse.getJSONArray(TAG_FILES);

                    // looping through All Products
                    for (int i = 0; i < files.length(); i++) {
                        JSONObject c = files.getJSONObject(i);

                        // Storing each json item in variable
                        String uid = c.getString(TAG_UID);
                        String file = c.getString(TAG_FILE);
                        String file_name = c.getString(TAG_FILE_NAME);

                        db.addAttachments(new Attachments_API(record_id, uid,
                                appraisal_type, file, file_name));
                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
                /*
                 * if (success==0){ Intent i = new
				 * Intent(getApplicationContext(), Request_Jobs_Info.class); //
				 * Closing all previous activities
				 * i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); startActivity(i);
				 * finish(); }
				 */

            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            // pDialog.dismiss();
        }

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.btn_yes:
                NetworkUtil.getConnectivityStatusString(this);
                if (!NetworkUtil.status.equals("Network not available")) {
                    decision = "yes";
                    call_type = "update";
                    dialog_status = "Accept";
                    update_dialog();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Network not available", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.btn_no:
                decision = "no";
                call_type = "update";
                dialog_status = "Decline";
                remarks_decline_dialog();

                break;
            case R.id.btn_mysql_attachments:
                Intent in = new Intent(getApplicationContext(),
                        Attachments_Inflater.class);
                in.putExtra(TAG_UID, uid);
                in.putExtra(TAG_RECORD_ID, record_id);
                in.putExtra(TAG_PASS_STAT, pass_stat);
                startActivityForResult(in, 100);

                break;
            default:
                break;
        }
    }

    /**
     * Sync data from CaseCenter to SQLite
     */
    public void syncCondo() {
        // TODO Auto-generated method stub
        // main table variables
        String report_date_inspected_month = "", report_date_inspected_day = "", report_date_inspected_year = "", report_id_admin = "", report_id_unit_numbering = "", report_id_bldg_plan = "", report_unitclass_per_tax_dec = "", report_unitclass_actual_usage = "", report_unitclass_neighborhood = "", report_unitclass_highest_best_use = "", report_trans_jeep = "", report_trans_bus = "", report_trans_taxi = "", report_trans_tricycle = "", report_faci_function_room = "", report_faci_gym = "", report_faci_sauna = "", report_faci_pool = "", report_faci_fire_alarm = "", report_faci_cctv = "", report_faci_elevator = "", report_util_electricity = "", report_util_water = "", report_util_telephone = "", report_util_garbage = "", report_landmarks_1 = "", report_landmarks_2 = "", report_landmarks_3 = "", report_landmarks_4 = "", report_landmarks_5 = "", report_distance_1 = "", report_distance_2 = "", report_distance_3 = "", report_distance_4 = "", report_distance_5 = "", report_zonal_location = "", report_zonal_lot_classification = "", report_zonal_value = "", report_final_value_total_appraised_value = "", report_final_value_recommended_deductions_desc = "", report_final_value_recommended_deductions_rate = "", report_final_value_recommended_deductions_amt = "", report_final_value_net_appraised_value = "", report_final_value_quick_sale_value_rate = "", report_final_value_quick_sale_value_amt = "", report_factors_of_concern = "", report_suggested_corrective_actions = "", report_remarks = "", report_requirements = "", report_time_inspected = "", report_comp1_date_month = "", report_comp1_date_day = "", report_comp1_date_year = "", report_comp1_source = "", report_comp1_contact_no = "", report_comp1_location = "", report_comp1_area = "", report_comp1_base_price = "", report_comp1_price_sqm = "", report_comp1_discount_rate = "", report_comp1_discounts = "", report_comp1_selling_price = "", report_comp1_rec_location = "", report_comp1_rec_unit_floor_location = "", report_comp1_rec_orientation = "", report_comp1_rec_size = "", report_comp1_rec_unit_condition = "", report_comp1_rec_amenities = "", report_comp1_rec_unit_features = "", report_comp1_rec_time_element = "", report_comp1_concluded_adjustment = "", report_comp1_adjusted_value = "", report_comp1_remarks = "", report_comp2_date_month = "", report_comp2_date_day = "", report_comp2_date_year = "", report_comp2_source = "", report_comp2_contact_no = "", report_comp2_location = "", report_comp2_area = "", report_comp2_base_price = "", report_comp2_price_sqm = "", report_comp2_discount_rate = "", report_comp2_discounts = "", report_comp2_selling_price = "", report_comp2_rec_location = "", report_comp2_rec_unit_floor_location = "", report_comp2_rec_orientation = "", report_comp2_rec_size = "", report_comp2_rec_unit_condition = "", report_comp2_rec_amenities = "", report_comp2_rec_unit_features = "", report_comp2_rec_time_element = "", report_comp2_concluded_adjustment = "", report_comp2_adjusted_value = "", report_comp2_remarks = "", report_comp3_date_month = "", report_comp3_date_day = "", report_comp3_date_year = "", report_comp3_source = "", report_comp3_contact_no = "", report_comp3_location = "", report_comp3_area = "", report_comp3_base_price = "", report_comp3_price_sqm = "", report_comp3_discount_rate = "", report_comp3_discounts = "", report_comp3_selling_price = "", report_comp3_rec_location = "", report_comp3_rec_unit_floor_location = "", report_comp3_rec_orientation = "", report_comp3_rec_size = "", report_comp3_rec_unit_condition = "", report_comp3_rec_amenities = "", report_comp3_rec_unit_features = "", report_comp3_rec_time_element = "", report_comp3_concluded_adjustment = "", report_comp3_adjusted_value = "", report_comp3_remarks = "", report_comp4_date_month = "", report_comp4_date_day = "", report_comp4_date_year = "", report_comp4_source = "", report_comp4_contact_no = "", report_comp4_location = "", report_comp4_area = "", report_comp4_base_price = "", report_comp4_price_sqm = "", report_comp4_discount_rate = "", report_comp4_discounts = "", report_comp4_selling_price = "", report_comp4_rec_location = "", report_comp4_rec_unit_floor_location = "", report_comp4_rec_orientation = "", report_comp4_rec_size = "", report_comp4_rec_unit_condition = "", report_comp4_rec_amenities = "", report_comp4_rec_unit_features = "", report_comp4_rec_time_element = "", report_comp4_concluded_adjustment = "", report_comp4_adjusted_value = "", report_comp4_remarks = "", report_comp5_date_month = "", report_comp5_date_day = "", report_comp5_date_year = "", report_comp5_source = "", report_comp5_contact_no = "", report_comp5_location = "", report_comp5_area = "", report_comp5_base_price = "", report_comp5_price_sqm = "", report_comp5_discount_rate = "", report_comp5_discounts = "", report_comp5_selling_price = "", report_comp5_rec_location = "", report_comp5_rec_unit_floor_location = "", report_comp5_rec_orientation = "", report_comp5_rec_size = "", report_comp5_rec_unit_condition = "", report_comp5_rec_amenities = "", report_comp5_rec_unit_features = "", report_comp5_rec_time_element = "", report_comp5_concluded_adjustment = "", report_comp5_adjusted_value = "", report_comp5_remarks = "", report_comp_average = "", report_comp_rounded_to = "",
                report_prev_date = "", report_prev_appraiser = "", report_prev_total_appraised_value = "";
        // title details
        String report_propdesc_property_type = "", report_propdesc_cct_no = "", report_propdesc_unit_no = "", report_propdesc_floor = "", report_propdesc_bldg_name = "", report_propdesc_area = "", report_propdesc_registry_date_month = "", report_propdesc_registry_date_day = "", report_propdesc_registry_date_year = "", report_propdesc_reg_of_deeds = "", report_propdesc_registered_owner = "";
        // unit details
        String report_unit_description = "", report_unit_no_of_storeys = "", report_unit_unit_no = "", report_unit_floor_location = "", report_unit_floor_area = "", report_unit_interior_flooring = "", report_unit_interior_partitions = "", report_unit_interior_doors = "", report_unit_interior_windows = "", report_unit_interior_ceiling = "", report_unit_features = "", report_unit_occupants = "", report_unit_owned_or_leased = "", report_unit_observed_condition = "", report_unit_ownership_of_property = "", report_unit_socialized_housing = "", report_unit_type_of_property = "", report_no_of_bedrooms = "", report_bldg_age = "", report_developer_name = "";
        // valuation
        String report_value_property_type = "", report_value_cct_no = "", report_value_unit_no = "", report_value_floor_area = "", report_value_deduction = "", report_value_net_area = "", report_value_unit_value = "", report_value_appraised_value = "";
        // rdps
        String report_prev_app_name = "", report_prev_app_location = "", report_prev_app_area = "", report_prev_app_value = "", report_prev_app_date = "";

        //ADDED From IAN
        String report_main_prev_desc = "", valrep_condo_propdesc_registry_date="",report_main_prev_area = "", report_main_prev_unit_value = "", report_main_prev_appraised_value = "";

        try {
            Log.e("sync Condo", xml2);
            JSONObject records2 = new JSONObject(xml2);
            db2.addRecord(gds.emptyFields(to.condo_db().size() - 2,record_id), to.condo_db(),"condo");
            ArrayList<String> condodb = new ArrayList<String>();
            condodb=  to.condo_db();
            condodb.remove(0);
            gds.reworkMethod(xml2, db2, condodb, to.condo_cc, record_id, "condo");
            Log.e("Condo Rework", "done");


            if (records2.has("valrep_condo_title_details")) {
                //load data into array string (sequentially sync to their equivalent string)
                String[] sqlite_string2 = new String[]{
                        report_propdesc_property_type,
                        report_propdesc_cct_no,
                        report_propdesc_unit_no,
                        report_propdesc_floor,
                        report_propdesc_bldg_name,
                        report_propdesc_area,
                        valrep_condo_propdesc_registry_date,
                        report_propdesc_reg_of_deeds,
                        report_propdesc_registered_owner
                };
                String[] case_center_string2 = new String[]{
                        "valrep_condo_propdesc_property_type",
                        "valrep_condo_propdesc_cct_no",
                        "valrep_condo_prodesc_unit_no",
                        "valrep_condo_prodesc_floor",
                        "valrep_condo_prodesc_bldg_name",
                        "valrep_condo_propdesc_area",
                        "valrep_condo_propdesc_registry_date",
                        "valrep_condo_prodesc_reg_of_deeds",
                        "valrep_condo_propdesc_registered_owner"
                };
                //title details
                JSONArray lot_details_array = records2.getJSONArray("valrep_condo_title_details");
                for (int x = 0; x < lot_details_array.length(); x++) {
                    JSONObject lot_details_object = lot_details_array.getJSONObject(x);
                    for (int y = 0; y <= sqlite_string2.length - 1; y++) {
                        if (lot_details_array.getJSONObject(x).has(case_center_string2[y])) {
                            sqlite_string2[y] = lot_details_object.getString(case_center_string2[y]);
                        } else {
                            sqlite_string2[y] = "";
                        }
                    }

                    db2.addCondo_Title_Details(new Condo_API_Title_Details(
                            record_id,
                            sqlite_string2[0],//report_propdesc_property_type,
                            sqlite_string2[1],//report_propdesc_cct_no,
                            sqlite_string2[2],//report_propdesc_unit_no,
                            sqlite_string2[3],//report_propdesc_floor,
                            sqlite_string2[4],//report_propdesc_bldg_name,
                            sqlite_string2[5],//report_propdesc_area,
                            sqlite_string2[6],//valrep_condo_propdesc_registry_date,
                            sqlite_string2[7],//report_propdesc_reg_of_deeds,
                            sqlite_string2[8]));//report_propdesc_registered_owner
                }
            }

            if (records2.has("valrep_condo_unit_details")) {
                //load data into array string (sequentially sync to their equivalent string)
                String[] sqlite_string3 = new String[]{
                        report_unit_description,
                        report_unit_no_of_storeys,
                        report_unit_unit_no,
                        report_unit_floor_location,
                        report_unit_floor_area,
                        report_unit_interior_flooring,
                        report_unit_interior_partitions,
                        report_unit_interior_doors,
                        report_unit_interior_windows,
                        report_unit_interior_ceiling,
                        report_unit_features,
                        report_unit_occupants,
                        report_unit_owned_or_leased,
                        report_unit_observed_condition,
                        report_unit_ownership_of_property,
                        report_unit_socialized_housing,
                        report_unit_type_of_property,
                        report_no_of_bedrooms,
                        report_bldg_age,
                        report_developer_name
                };
                String[] case_center_string3 = new String[]{
                        "valrep_condo_unit_description",
                        "valrep_condo_unit_no_of_storeys",
                        "valrep_condo_unit_unit_no",
                        "valrep_condo_unit_floor_location",
                        "valrep_condo_unit_floor_area",
                        "valrep_condo_unit_interior_flooring",
                        "valrep_condo_unit_interior_partitions",
                        "valrep_condo_unit_interior_doors",
                        "valrep_condo_unit_interior_windows",
                        "valrep_condo_unit_interior_ceiling",
                        "valrep_condo_unit_features",
                        "valrep_condo_unit_occupants",
                        "valrep_condo_unit_owned_or_leased",
                        "valrep_condo_unit_observed_condition",
                        "valrep_condo_unit_ownership_of_property",
                        "valrep_condo_unit_socialized_housing",
                        "valrep_condo_unit_type_of_property",
                        "valrep_condo_unit_no_of_bedrooms",
                        "valrep_condo_bldg_age",
                        "valrep_condo_developer_name"
                };

                //unit details
                JSONArray unit_details_array = records2.getJSONArray("valrep_condo_unit_details");
                for (int x = 0; x < unit_details_array.length(); x++) {
                    JSONObject unit_details_object = unit_details_array.getJSONObject(x);

                    for (int y = 0; y <= sqlite_string3.length - 1; y++) {
                        if (unit_details_array.getJSONObject(x).has(case_center_string3[y])) {
                            sqlite_string3[y] = unit_details_object.getString(case_center_string3[y]);
                        } else {
                            sqlite_string3[y] = "";
                        }
                    }

                    db2.addCondo_Unit_Details(new Condo_API_Unit_Details(
                            record_id,
                            sqlite_string3[0],//report_unit_description,
                            sqlite_string3[1],//report_unit_no_of_storeys,
                            sqlite_string3[2],//report_unit_unit_no,
                            sqlite_string3[3],//report_unit_floor_location,
                            sqlite_string3[4],//report_unit_floor_area,
                            sqlite_string3[5],//report_unit_interior_flooring,
                            sqlite_string3[6],//report_unit_interior_partitions,
                            sqlite_string3[7],//report_unit_interior_doors,
                            sqlite_string3[8],//report_unit_interior_windows,
                            sqlite_string3[9],//report_unit_interior_ceiling,
                            sqlite_string3[10],//report_unit_features,
                            sqlite_string3[11],//report_unit_occupants,
                            sqlite_string3[12],//report_unit_owned_or_leased,
                            sqlite_string3[13],//report_unit_observed_condition,
                            sqlite_string3[14],//report_unit_ownership_of_property,
                            sqlite_string3[15],//report_unit_socialized_housing,
                            sqlite_string3[16],//report_unit_type_of_property
                            sqlite_string3[17],//report_no_of_bedrooms));
                            sqlite_string3[18],//report_bldg_age
                            sqlite_string3[19]//report_developer_name
                    ));
                }
            }

            if (records2.has("valrep_condo_unit_valuation")) {
                String[] sqlite_string4 = new String[]{
                        report_value_property_type,
                        report_value_cct_no,
                        report_value_unit_no,
                        report_value_floor_area,
                        report_value_deduction,
                        report_value_net_area,
                        report_value_unit_value,
                        report_value_appraised_value
                };
                String[] case_center_string4 = new String[]{
                        "valrep_condo_value_property_type",
                        "valrep_condo_value_cct_no",
                        "valrep_condo_value_unit_no",
                        "valrep_condo_value_floor_area",
                        "valrep_condo_value_deduction",
                        "valrep_condo_value_net_area",
                        "valrep_condo_value_unit_value",
                        "valrep_condo_value_appraised_value"
                };
                //valuation
                JSONArray val_ary = records2.getJSONArray("valrep_condo_unit_valuation");
                for (int x = 0; x < val_ary.length(); x++) {
                    JSONObject val_obj = val_ary.getJSONObject(x);

                    for (int y = 0; y <= sqlite_string4.length - 1; y++) {
                        if (val_ary.getJSONObject(x).has(case_center_string4[y])) {
                            sqlite_string4[y] = val_obj.getString(case_center_string4[y]);
                        } else {
                            sqlite_string4[y] = "";
                        }
                    }

                    db2.addCondo_Valuation(new Condo_API_Valuation(
                            record_id,
                            sqlite_string4[0],//report_value_property_type,
                            sqlite_string4[1],//report_value_cct_no,
                            sqlite_string4[2],//report_value_unit_no,
                            sqlite_string4[3],//report_value_floor_area,
                            sqlite_string4[4],//report_value_deduction,
                            sqlite_string4[5],//report_value_net_area,
                            sqlite_string4[6],//report_value_unit_value,
                            sqlite_string4[7]));//report_value_appraised_value
                }
            }

            if (records2.has("valrep_condo_prev_appraisal")) {
                String[] sqlite_string5 = new String[]{
                        report_prev_app_name,
                        report_prev_app_location,
                        report_prev_app_area,
                        report_prev_app_value,
                        report_prev_app_date
                };
                String[] case_center_string5 = new String[]{
                        "valrep_condo_prev_app_account",
                        "valrep_condo_prev_app_location",
                        "valrep_condo_prev_app_area",
                        "valrep_condo_prev_app_value",
                        "valrep_condo_prev_app_date"
                };
                // rdps
                JSONArray rdps_ary = records2.getJSONArray("valrep_condo_prev_appraisal");
                for (int x = 0; x < rdps_ary.length(); x++) {
                    JSONObject rdps_obj = rdps_ary.getJSONObject(x);

                    for (int y = 0; y <= sqlite_string5.length - 1; y++) {
                        if (rdps_ary.getJSONObject(x).has(case_center_string5[y])) {
                            sqlite_string5[y] = rdps_obj.getString(case_center_string5[y]);
                        } else {
                            sqlite_string5[y] = "";
                        }
                    }

                    db2.addCondo_RDPS(new Condo_API_RDPS(
                            record_id,
                            sqlite_string5[0],//report_prev_app_name,
                            sqlite_string5[1],//report_prev_app_location,
                            sqlite_string5[2],//report_prev_app_area,
                            sqlite_string5[3],//report_prev_app_value,
                            sqlite_string5[4]));//report_prev_app_date
                }
            }

            //ADDED From IAN Prev APP

            if (records2.has("valrep_condo_main_prev_appraisal")) {
                String[] sqlite_string5 = new String[]{
                        report_main_prev_desc,
                        report_main_prev_area,
                        report_main_prev_unit_value,
                        report_main_prev_appraised_value
                };
                String[] case_center_string5 = new String[]{
                        "valrep_condo_prev_desc",
                        "valrep_condo_prev_area",
                        "valrep_condo_prev_unit_value",
                        "valrep_condo_prev_appraised_value"
                };
                // Prev Appraisal
                JSONArray prev_app_ary = records2.getJSONArray("valrep_condo_main_prev_appraisal");
                for (int x = 0; x < prev_app_ary.length(); x++) {
                    JSONObject prev_app_obj = prev_app_ary.getJSONObject(x);

                    for (int y = 0; y <= sqlite_string5.length - 1; y++) {
                        if (prev_app_ary.getJSONObject(x).has(case_center_string5[y])) {
                            sqlite_string5[y] = prev_app_obj.getString(case_center_string5[y]);
                        } else {
                            sqlite_string5[y] = "";
                        }
                    }

                    db2.addCondo_Prev_Appraisal(new Condo_API_Prev_Appraisal(
                            record_id,
                            sqlite_string5[0],//report_main_prev_desc,
                            sqlite_string5[1],//report_main_prev_area,
                            sqlite_string5[2],//report_main_prev_unit_value,
                            sqlite_string5[3]));//report_main_prev_appraised_value
                }
            }


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            Log.e("", e.toString());
        }
    }

    public void syncMotorVehicle() {
        // TODO Auto-generated method stub

        //Aug 18 2017

        db.addRecord(gds.emptyFields(db.MV_free_fields_mobile().size()-2,record_id), db.MV_free_fields_mobile(), db.tbl_motor_vehicle);
        ArrayList<String> mvFields= new ArrayList<String>();
        mvFields = db.MV_free_fields_mobile();
        mvFields.remove(0);
        gds.reworkMethod(xml2, db2, mvFields, db.MV_free_fields_cc, record_id, db.tbl_motor_vehicle);
        Log.e("MV - freefields", "done");

        gds.reworkMethod(xml2, "valrep_mv_prev_appraisal", db2, db.MV_prev_fields_mobile(), db.MV_prev_fields_cc, record_id, db.tbl_motor_vehicle_prev);
        Log.e("valrep_mv_prev_appraisal", "done");


    }

    public void syncVacantLot() {
        // TODO Auto-generated method stub
        //main
        //lot details
        String report_propdesc_tct_no = "", report_propdesc_lot = "", report_propdesc_block = "", report_propdesc_survey_nos = "", report_propdesc_area = "", report_propdesc_registry_date_month = "", report_propdesc_registry_date_day = "", report_propdesc_registry_date_year = "", report_propdesc_registered_owner = "", report_propdesc_deeds = "";
        //market valuation
        // rdps
        String report_prev_app_name = "", report_prev_app_location = "", report_prev_app_area = "", report_prev_app_value = "", report_prev_app_date = "";
        //Added By IAN

        //ADDED By IAN Vacant_Lot Prev Appraisal
        String report_main_prev_desc = "", valrep_land_propdesc_registry_date="",report_main_prev_area = "", report_main_prev_unit_value = "", report_main_prev_appraised_value = "";
        String valrep_land_prev_land_value="", valrep_land_prev_imp_value="";

        try {
            Log.e("recheck", xml2);
            JSONObject records2 = new JSONObject(xml2);


            db2.addRecord(gds.emptyFields(to.vl_db().size() - 2,record_id), to.vl_db(),"vacant_lot");
            ArrayList<String> vldb = new ArrayList<String>();
            vldb=  to.vl_db();
            vldb.remove(0);
            gds.reworkMethod(xml2, db2, vldb, to.vl_cc, record_id, "vacant_lot");
            Log.e("Vacant Lot Rework", "done");

            if (records2.has("valrep_land_lot_details")) {

                //load data into array string (sequentially sync to their equivalent string)
                String[] sqlite_string2 = new String[]{
                        report_propdesc_tct_no,
                        report_propdesc_lot,
                        report_propdesc_block,
                        report_propdesc_survey_nos,
                        report_propdesc_area,
                        valrep_land_propdesc_registry_date,
                        report_propdesc_registered_owner,
                        report_propdesc_deeds
                };
                String[] case_center_string2 = new String[]{
                        "valrep_land_propdesc_tct_no",
                        "valrep_land_propdesc_lot",
                        "valrep_land_propdesc_block",
                        "valrep_land_propdesc_survey_nos",
                        "valrep_land_propdesc_area",
                        "valrep_land_propdesc_registry_date",
                        "valrep_land_propdesc_registered_owner",
                        "valrep_land_propdesc_deeds"
                };

                JSONArray lot_details_array = records2.getJSONArray("valrep_land_lot_details");
                for (int x = 0; x < lot_details_array.length(); x++) {
                    JSONObject lot_details_object = lot_details_array.getJSONObject(x);
                    for (int y = 0; y <= sqlite_string2.length - 1; y++) {
                        if (lot_details_array.getJSONObject(x).has(case_center_string2[y])) {
                            sqlite_string2[y] = lot_details_object.getString(case_center_string2[y]);
                        } else {
                            sqlite_string2[y] = "";
                        }
                    }
                    db2.addVacant_Lot_Lot_Details(new Vacant_Lot_API_Lot_Details(
                            record_id,
                            sqlite_string2[0],//report_propdesc_tct_no,
                            sqlite_string2[1],//report_propdesc_lot,
                            sqlite_string2[2],//report_propdesc_block,
                            sqlite_string2[3],//report_propdesc_survey_nos,
                            sqlite_string2[4],//report_propdesc_area,
                            sqlite_string2[5],//valrep_land_propdesc_registry_date,
                            sqlite_string2[6],//report_propdesc_registered_owner,
                            sqlite_string2[7]//report_propdesc_deeds
                    ));
                }
            }


            //sync nested
            if (records2.has("valrep_land_lot_valuation")) {
                /**
                 * sync nested details (should run inside the loop, for getting the right id
                 */
                JSONObject mainObj = new JSONObject(xml2);
                if (mainObj != null) {
                    JSONArray list = mainObj.getJSONArray("valrep_land_lot_valuation");//2nd layer
                    Log.e("list", list.toString());
                    if (list != null) {
                        for (int i = 0; i < list.length(); i++) {
                            JSONObject elem = list.getJSONObject(i);
                            Log.e("elem", elem.toString() + " index:" + i);
                            if (elem != null) {
                                if (elem.has("valrep_land_lot_valuation_details")) {//check json has this field
                                    JSONArray prods = elem.getJSONArray("valrep_land_lot_valuation_details");//3rd layer
                                    Log.e("prods", prods.toString());
                                    if (prods != null) {
                                        for (int j = 0; j < prods.length(); j++) {
                                            JSONObject innerElem = prods.getJSONObject(j);
                                            Log.e("innerElem", innerElem.toString() + " index:" + j);
                                            if (innerElem != null) {
                                                String tct_no = innerElem.getString("valrep_land_value_tct_no");
                                                String lot_no = innerElem.getString("valrep_land_value_lot_no");
                                                String block_no = innerElem.getString("valrep_land_value_block_no");
                                                String area = innerElem.getString("valrep_land_value_total_area");
                                                String deduction = innerElem.getString("valrep_land_value_deduction");
                                                String net_area = innerElem.getString("valrep_land_value_net_area");
                                                String unit_value = innerElem.getString("valrep_land_value_unit_value");
                                                String land_value = innerElem.getString("valrep_land_value_total_land_value");

                                                db2.addVacant_Lot_Lot_Valuation_Details(new Vacant_Lot_API_Lot_Valuation_Details(
                                                        record_id,
                                                        tct_no,
                                                        lot_no,
                                                        block_no,
                                                        area,
                                                        deduction,
                                                        net_area,
                                                        unit_value,
                                                        land_value
                                                ));
                                                Log.e("val details", record_id + "\n" + tct_no + "\n" + lot_no + "\n" + block_no);
                                            }
                                        }
                                    }
                                }//check json has this field
                            }
                        }
                    }
                }
                //end of sync of nested details
            }


            if (records2.has("valrep_land_prev_appraisal")) {
                String[] sqlite_string4 = new String[]{
                        report_prev_app_name,
                        report_prev_app_location,
                        report_prev_app_area,
                        report_prev_app_value,
                        report_prev_app_date
                };
                String[] case_center_string4 = new String[]{
                        "valrep_land_prev_app_account",
                        "valrep_land_prev_app_location",
                        "valrep_land_prev_app_area",
                        "valrep_land_prev_app_value",
                        "valrep_land_prev_app_date"
                };
                // rdps
                JSONArray rdps_ary = records2.getJSONArray("valrep_land_prev_appraisal");
                for (int x = 0; x < rdps_ary.length(); x++) {
                    JSONObject rdps_obj = rdps_ary.getJSONObject(x);

                    for (int y = 0; y <= sqlite_string4.length - 1; y++) {
                        if (rdps_ary.getJSONObject(x).has(case_center_string4[y])) {
                            sqlite_string4[y] = rdps_obj.getString(case_center_string4[y]);
                        } else {
                            sqlite_string4[y] = "";
                        }
                    }

                    db2.addVacant_Lot_RDPS(new Vacant_Lot_API_RDPS(
                            record_id,
                            sqlite_string4[0],//report_prev_app_name,
                            sqlite_string4[1],//report_prev_app_location,
                            sqlite_string4[2],//report_prev_app_area,
                            sqlite_string4[3],//report_prev_app_value,
                            sqlite_string4[4]));//report_prev_app_date
                }
            }

            //ADDED By IAN
            if (records2.has("valrep_land_main_prev_appraisal")) {
                String[] sqlite_string4 = new String[]{
                        report_main_prev_desc,
                        report_main_prev_area,
                        report_main_prev_unit_value,
                        report_main_prev_appraised_value,
                        valrep_land_prev_land_value,
                        valrep_land_prev_imp_value
                };
                String[] case_center_string4 = new String[]{
                        "valrep_land_prev_desc",
                        "valrep_land_prev_area",
                        "valrep_land_prev_unit_value",
                        "valrep_land_prev_appraised_value",
                        "valrep_land_prev_land_value",
                        "valrep_land_prev_imp_value"
                };
                // prev app
                JSONArray prev_app_ary = records2.getJSONArray("valrep_land_main_prev_appraisal");
                for (int x = 0; x < prev_app_ary.length(); x++) {
                    JSONObject prev_app_obj = prev_app_ary.getJSONObject(x);

                    for (int y = 0; y <= sqlite_string4.length - 1; y++) {
                        if (prev_app_ary.getJSONObject(x).has(case_center_string4[y])) {
                            sqlite_string4[y] = prev_app_obj.getString(case_center_string4[y]);
                        } else {
                            sqlite_string4[y] = "";
                        }
                    }

                    db2.addVacant_Lot_Prev_Appraisal(new Vacant_Lot_API_Prev_Appraisal(
                            record_id,
                            sqlite_string4[0],//report_main_prev_desc,
                            sqlite_string4[1],//report_main_prev_area,
                            sqlite_string4[2],//report_main_prev_unit_value,
                            sqlite_string4[3],//report_main_prev_unit_value,
                            sqlite_string4[4],
                            sqlite_string4[5]));

                }
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            Log.e("", e.toString());
        }
    }

    public void syncLandImprovements() {
        // TODO Auto-generated method stub

        try {
            Log.e("recheck", xml2);
            JSONObject records2 = new JSONObject(xml2);

            db2.addRecord(gds.emptyFields(to.landimp_db().size() - 2,record_id), to.landimp_db(),"land_improvements");
            ArrayList<String> landimpdb = new ArrayList<String>();
            landimpdb=  to.landimp_db();
            landimpdb.remove(0);
            gds.reworkMethod(xml2, db2, landimpdb, to.landimp_cc, record_id, "land_improvements");
            Log.e("Landimp Rework", "done");

            //ownership
            gds.reworkMethod(xml2, "valrep_landimp_lot_details", db2, to.LI_ownership_property(), to.LI_ownership_object, record_id, Tables.land_improvements_lot_details.table_name);

            //description of improvements
            gds.reworkMethod(xml2, "valrep_landimp_imp_details", db2, to.LI_description_improvement(), to.LI_description_imp, record_id, Tables.land_improvements_imp_details.table_name);

            //imp valuation cost approach
            gds.reworkMethod(xml2, "valrep_landimp_imp_valuation", db2, to.LI_imp_valuation(), to.LI_imp_valuation, record_id, Tables.land_improvements_imp_valuation.table_name);

            //rdps
            gds.reworkMethod(xml2, "valrep_landimp_prev_appraisal", db2, to.LI_rdps(), to.LI_rdps, record_id, Tables.land_improvements_prev_appraisal.table_name);

            //prev appraisal
            gds.reworkMethod(xml2, "valrep_landimp_main_prev_appraisal", db2, to.LI_prev_appraisal(), to.LI_prev_appraisal, record_id, Tables.land_improvements_main_prev_appraisal.table_name);


            String where ="WHERE record_id = args";
            String[] args = new String[1];
            args[0] = record_id;


            //desc of imp
            if (records2.has("valrep_landimp_imp_details")) {
                Log.e("point 2", "valrep_landimp_imp_details");

                int size =db2.getRecord("imp_details_id",where,args,Tables.land_improvements_imp_details.table_name).size();
                for (int y = 0; y<size; y++) {
                    String imp_details_id = db2.getRecord("imp_details_id",where,args,Tables.land_improvements_imp_details.table_name).get(y);
                    id_stack.add(imp_details_id);
                }

                Log.e("id_stack", id_stack.toString());

            }
//features and division
            if (records2.has("valrep_landimp_imp_details")){
                Log.e("point 3", "valrep_landimp_imp_details");
                /**
                 * sync nested details (should run inside the loop, for getting the right id
                 */

                JSONObject mainObj = new JSONObject(xml2);
                Log.e("record_shell1", mainObj.toString());

                if(mainObj != null){
                    JSONArray list = mainObj.getJSONArray("valrep_landimp_imp_details");//2nd layer
                    Log.e("list", list.toString());
                    if(list != null){
                        for(int i = 0; i < list.length();i++){
                            JSONObject elem = list.getJSONObject(i);
                            Log.e("elem",elem.toString()+" index:"+i);
                            if(elem != null){
                                if (elem.has("valrep_landimp_imp_details_features")){//check json has this field
                                    JSONArray prods = elem.getJSONArray("valrep_landimp_imp_details_features");//3rd layer
                                    Log.e("prods",prods.toString());
                                    if(prods != null){
                                        for(int j = 0; j < prods.length();j++){
                                            JSONObject innerElem = prods.getJSONObject(j);
                                            Log.e("innerElem",innerElem.toString()+" index:"+j);
                                            if(innerElem != null){
                                                Log.e("id_stack_features", id_stack.toString());
                                                String feature_area = innerElem.getString("valrep_landimp_desc_features_area");
                                                String feature_description = innerElem.getString("valrep_landimp_desc_features_area_desc");

                                                db2.addLand_Improvements_Imp_Details_Features(new Land_Improvements_API_Imp_Details_Features(
                                                        record_id,
                                                        id_stack.get(i),//id_holder,//id
                                                        "",//bldg desc (null)
                                                        feature_area,//sqlite_string4[1],//report_desc_features_area,
                                                        feature_description//sqlite_string4[2]//report_desc_features_area_desc
                                                ));
                                                Log.e("features",record_id+"\n"+feature_area+"\n"+feature_description);
                                            }
                                        }
                                    }
                                }//check json has this field else do not do this loop
                            }
                        }
                    }
                }
                //end of sync of nested details
                id_stack.clear();
                Log.e("point 3.1", "valrep_landimp_imp_details");
            }


            //market data approach
            if (records2.has("valrep_landimp_lot_valuation")) {
                /**
                 * sync nested details (should run inside the loop, for getting the right id
                 */
                Log.e("point 4", "valrep_landimp_lot_valuation market data approach");
                JSONObject mainObj3 = new JSONObject(xml2);
                if (mainObj3 != null) {
                    JSONArray list3 = mainObj3.getJSONArray("valrep_landimp_lot_valuation");//2nd layer
                    Log.e("list3", list3.toString());
                    if (list3 != null) {
                        for (int i = 0; i < list3.length(); i++) {
                            JSONObject elem3 = list3.getJSONObject(i);
                            Log.e("elem3", elem3.toString() + " index:" + i);
                            if (elem3 != null) {
                                if (elem3.has("valrep_landimp_lot_valuation_details")) {//check json has this field
                                    JSONArray prods3 = elem3.getJSONArray("valrep_landimp_lot_valuation_details");//3rd layer
                                    Log.e("prods2", prods3.toString());
                                    if (prods3 != null) {
                                        for (int j = 0; j < prods3.length(); j++) {
                                            JSONObject innerelem3 = prods3.getJSONObject(j);
                                            Log.e("innerelem3", innerelem3.toString() + " index:" + j);
                                            if (innerelem3 != null) {
                                                String tct_no = innerelem3.getString("valrep_landimp_value_tct_no");
                                                String lot_no = innerelem3.getString("valrep_landimp_value_lot_no");
                                                String block = innerelem3.getString("valrep_landimp_value_block_no");
                                                String total_area = innerelem3.getString("valrep_landimp_value_area");
                                                String deduc = innerelem3.getString("valrep_landimp_value_deduction");
                                                String net_area = innerelem3.getString("valrep_landimp_value_net_area");
                                                String unit_value = innerelem3.getString("valrep_landimp_value_unit_value");
                                                String total_land_val = innerelem3.getString("valrep_landimp_value_land_value");

                                                db2.addLand_Improvements_Lot_Valuation_Details(new Land_Improvements_API_Lot_Valuation_Details(
                                                        record_id,
                                                        tct_no,
                                                        lot_no,
                                                        block,
                                                        total_area,
                                                        deduc,
                                                        net_area,
                                                        unit_value,
                                                        total_land_val
                                                ));
                                                Log.e("lot_valuation_details", record_id + "\n" + unit_value + "\n" + total_land_val);
                                            }
                                        }
                                    }
                                } //check json has this field
                            }
                        }
                    }
                }
                //end of sync of nested details
                Log.e("point 4.1", "valrep_landimp_lot_valuation market data approach");
            }

            //cost approach
            if (records2.has("valrep_landimp_imp_valuation")) {
                int size =db2.getRecord("imp_valuation_id",where,args,Tables.land_improvements_imp_valuation.table_name).size();
                for (int y = 0; y<size; y++) {
                    String imp_valuation_id = db2.getRecord("imp_valuation_id",where,args,Tables.land_improvements_imp_valuation.table_name).get(y);
                    id_stack.add(imp_valuation_id);
                }

            }

            if (records2.has("valrep_landimp_imp_valuation")) {
                /**
                 * sync nested details (should run inside the loop, for getting the right id
                 */
                JSONObject mainObj2 = new JSONObject(xml2);
                if (mainObj2 != null) {
                    JSONArray list2 = mainObj2.getJSONArray("valrep_landimp_imp_valuation");//2nd layer
                    Log.e("list2", list2.toString());
                    if (list2 != null) {
                        for (int i = 0; i < list2.length(); i++) {
                            JSONObject elem2 = list2.getJSONObject(i);
                            Log.e("elem2", elem2.toString() + " index:" + i);
                            if (elem2 != null) {
                                if (elem2.has("valrep_landimp_imp_valuation_details")) {//check json has this field
                                    JSONArray prods2 = elem2.getJSONArray("valrep_landimp_imp_valuation_details");//3rd layer
                                    Log.e("prods2", prods2.toString());
                                    if (prods2 != null) {
                                        for (int j = 0; j < prods2.length(); j++) {
                                            JSONObject innerelem2 = prods2.getJSONObject(j);
                                            Log.e("innerelem2", innerelem2.toString() + " index:" + j);
                                            if (innerelem2 != null) {
                                                String value_kind = innerelem2.getString("valrep_landimp_imp_value_kind");
                                                String value_area = innerelem2.getString("valrep_landimp_imp_value_area");
                                                String value_cost_per_sqm = innerelem2.getString("valrep_landimp_imp_value_cost_per_sqm");
                                                String reproduction_cost = innerelem2.getString("valrep_landimp_imp_value_reproduction_cost");

                                                db2.addLand_Improvements_Cost_Valuation_Details(new Land_Improvements_API_Cost_Valuation_Details(
                                                        record_id,
                                                        id_stack.get(i),
                                                        value_kind,
                                                        value_area,
                                                        value_cost_per_sqm,
                                                        reproduction_cost
                                                ));
                                                Log.e("valuation details", record_id + "\n" + value_kind + "\n" + value_area);
                                            }
                                        }
                                    }
                                }//check json has this field
                            }
                        }
                    }
                }
                //end of sync of nested details
                id_stack.clear();
            }




        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            Log.e("", e.toString());
        }
    }

    public void syncConstructionEbm() {
        //main table variables
        String report_date_inspected_month = "", report_date_inspected_day = "", report_date_inspected_year = "", report_time_inspected = "", report_project_type = "", report_floor_area = "", report_storeys = "", report_expected_economic_life = "", report_type_of_housing_unit = "", report_const_type_reinforced_concrete = "", report_const_type_semi_concrete = "", report_const_type_mixed_materials = "", report_foundation_concrete = "", report_foundation_other = "", report_foundation_other_value = "", report_post_concrete = "", report_post_concrete_timber = "", report_post_steel = "", report_post_other = "", report_post_other_value = "", report_beams_concrete = "", report_beams_timber = "", report_beams_steel = "", report_beams_other = "", report_beams_other_value = "", report_floors_concrete = "", report_floors_tiles = "", report_floors_tiles_cement = "", report_floors_laminated_wood = "", report_floors_ceramic_tiles = "", report_floors_wood_planks = "", report_floors_marble_washout = "", report_floors_concrete_boards = "", report_floors_granite_tiles = "", report_floors_marble_wood = "", report_floors_carpet = "", report_floors_other = "", report_floors_other_value = "", report_walls_chb = "", report_walls_chb_cement = "", report_walls_anay = "", report_walls_chb_wood = "", report_walls_precast = "", report_walls_decorative_stone = "", report_walls_adobe = "", report_walls_ceramic_tiles = "", report_walls_cast_in_place = "", report_walls_sandblast = "", report_walls_mactan_stone = "", report_walls_painted = "", report_walls_other = "", report_walls_other_value = "", report_partitions_chb = "", report_partitions_painted_cement = "", report_partitions_anay = "", report_partitions_wood_boards = "", report_partitions_precast = "", report_partitions_decorative_stone = "", report_partitions_adobe = "", report_partitions_granite = "", report_partitions_cast_in_place = "", report_partitions_sandblast = "", report_partitions_mactan_stone = "", report_partitions_ceramic_tiles = "", report_partitions_chb_plywood = "", report_partitions_hardiflex = "", report_partitions_wallpaper = "", report_partitions_painted = "", report_partitions_other = "", report_partitions_other_value = "", report_windows_steel_casement = "", report_windows_fixed_view = "", report_windows_analok_sliding = "", report_windows_alum_glass = "", report_windows_aluminum_sliding = "", report_windows_awning_type = "", report_windows_powder_coated = "", report_windows_wooden_frame = "", report_windows_other = "", report_windows_other_value = "", report_doors_wood_panel = "", report_doors_pvc = "", report_doors_analok_sliding = "", report_doors_screen_door = "", report_doors_flush = "", report_doors_molded_door = "", report_doors_aluminum_sliding = "", report_doors_flush_french = "", report_doors_other = "", report_doors_other_value = "", report_ceiling_plywood = "", report_ceiling_painted_gypsum = "", report_ceiling_soffit_slab = "", report_ceiling_metal_deck = "", report_ceiling_hardiflex = "", report_ceiling_plywood_tg = "", report_ceiling_plywood_pvc = "", report_ceiling_painted = "", report_ceiling_with_cornice = "", report_ceiling_with_moulding = "", report_ceiling_drop_ceiling = "", report_ceiling_other = "", report_ceiling_other_value = "", report_roof_pre_painted = "", report_roof_rib_type = "", report_roof_tilespan = "", report_roof_tegula_asphalt = "", report_roof_tegula_longspan = "", report_roof_tegula_gi = "", report_roof_steel_concrete = "", report_roof_polycarbonate = "", report_roof_on_steel_trusses = "", report_roof_on_wooden_trusses = "", report_roof_other = "", report_roof_other_value = "", report_valuation_total_area = "", report_valuation_total_proj_cost = "", report_valuation_remarks = "";
        //room list table variables
        String report_room_list_area = "", report_room_list_description = "", report_room_list_est_proj_cost = "", report_room_list_floor = "", report_room_list_rcn = "";

        String[] sqlite_string = new String[]{
                report_date_inspected_month,
                report_date_inspected_day,
                report_date_inspected_year,
                report_time_inspected,
                report_project_type,
                report_floor_area,
                report_storeys,
                report_expected_economic_life,
                report_type_of_housing_unit,
                report_const_type_reinforced_concrete,
                report_const_type_semi_concrete,
                report_const_type_mixed_materials,
                report_foundation_concrete,
                report_foundation_other,
                report_foundation_other_value,
                report_post_concrete,
                report_post_concrete_timber,
                report_post_steel,
                report_post_other,
                report_post_other_value,
                report_beams_concrete,
                report_beams_timber,
                report_beams_steel,
                report_beams_other,
                report_beams_other_value,
                report_floors_concrete,
                report_floors_tiles,
                report_floors_tiles_cement,
                report_floors_laminated_wood,
                report_floors_ceramic_tiles,
                report_floors_wood_planks,
                report_floors_marble_washout,
                report_floors_concrete_boards,
                report_floors_granite_tiles,
                report_floors_marble_wood,
                report_floors_carpet,
                report_floors_other,
                report_floors_other_value,
                report_walls_chb,
                report_walls_chb_cement,
                report_walls_anay,
                report_walls_chb_wood,
                report_walls_precast,
                report_walls_decorative_stone,
                report_walls_adobe,
                report_walls_ceramic_tiles,
                report_walls_cast_in_place,
                report_walls_sandblast,
                report_walls_mactan_stone,
                report_walls_painted,
                report_walls_other,
                report_walls_other_value,
                report_partitions_chb,
                report_partitions_painted_cement,
                report_partitions_anay,
                report_partitions_wood_boards,
                report_partitions_precast,
                report_partitions_decorative_stone,
                report_partitions_adobe,
                report_partitions_granite,
                report_partitions_cast_in_place,
                report_partitions_sandblast,
                report_partitions_mactan_stone,
                report_partitions_ceramic_tiles,
                report_partitions_chb_plywood,
                report_partitions_hardiflex,
                report_partitions_wallpaper,
                report_partitions_painted,
                report_partitions_other,
                report_partitions_other_value,
                report_windows_steel_casement,
                report_windows_fixed_view,
                report_windows_analok_sliding,
                report_windows_alum_glass,
                report_windows_aluminum_sliding,
                report_windows_awning_type,
                report_windows_powder_coated,
                report_windows_wooden_frame,
                report_windows_other,
                report_windows_other_value,
                report_doors_wood_panel,
                report_doors_pvc,
                report_doors_analok_sliding,
                report_doors_screen_door,
                report_doors_flush,
                report_doors_molded_door,
                report_doors_aluminum_sliding,
                report_doors_flush_french,
                report_doors_other,
                report_doors_other_value,
                report_ceiling_plywood,
                report_ceiling_painted_gypsum,
                report_ceiling_soffit_slab,
                report_ceiling_metal_deck,
                report_ceiling_hardiflex,
                report_ceiling_plywood_tg,
                report_ceiling_plywood_pvc,
                report_ceiling_painted,
                report_ceiling_with_cornice,
                report_ceiling_with_moulding,
                report_ceiling_drop_ceiling,
                report_ceiling_other,
                report_ceiling_other_value,
                report_roof_pre_painted,
                report_roof_rib_type,
                report_roof_tilespan,
                report_roof_tegula_asphalt,
                report_roof_tegula_longspan,
                report_roof_tegula_gi,
                report_roof_steel_concrete,
                report_roof_polycarbonate,
                report_roof_on_steel_trusses,
                report_roof_on_wooden_trusses,
                report_roof_other,
                report_roof_other_value,
                report_valuation_total_area,
                report_valuation_total_proj_cost,
                report_valuation_remarks
        };

        String[] case_center_string = new String[]{
                "valrep_constebm_date_inspected_month",
                "valrep_constebm_date_inspected_day",
                "valrep_constebm_date_inspected_year",
                "valrep_constebm_time_inspected",
                "valrep_constebm_project_type",
                "valrep_constebm_floor_area",
                "valrep_constebm_storeys",
                "valrep_constebm_expected_economic_life",
                "valrep_constebm_type_of_housing_unit",
                "valrep_constebm_const_type_reinforced_concrete",
                "valrep_constebm_const_type_semi_concrete",
                "valrep_constebm_const_type_mixed_materials",
                "valrep_constebm_foundation_concrete",
                "valrep_constebm_foundation_other",
                "valrep_constebm_foundation_other_value",
                "valrep_constebm_post_concrete",
                "valrep_constebm_post_concrete_timber",
                "valrep_constebm_post_steel",
                "valrep_constebm_post_other",
                "valrep_constebm_post_other_value",
                "valrep_constebm_beams_concrete",
                "valrep_constebm_beams_timber",
                "valrep_constebm_beams_steel",
                "valrep_constebm_beams_other",
                "valrep_constebm_beams_other_value",
                "valrep_constebm_floors_concrete",
                "valrep_constebm_floors_tiles",
                "valrep_constebm_floors_tiles_cement",
                "valrep_constebm_floors_laminated_wood",
                "valrep_constebm_floors_ceramic_tiles",
                "valrep_constebm_floors_wood_planks",
                "valrep_constebm_floors_marble_washout",
                "valrep_constebm_floors_concrete_boards",
                "valrep_constebm_floors_granite_tiles",
                "valrep_constebm_floors_marble_wood",
                "valrep_constebm_floors_carpet",
                "valrep_constebm_floors_other",
                "valrep_constebm_floors_other_value",
                "valrep_constebm_walls_chb",
                "valrep_constebm_walls_chb_cement",
                "valrep_constebm_walls_anay",
                "valrep_constebm_walls_chb_wood",
                "valrep_constebm_walls_precast",
                "valrep_constebm_walls_decorative_stone",
                "valrep_constebm_walls_adobe",
                "valrep_constebm_walls_ceramic_tiles",
                "valrep_constebm_walls_cast_in_place",
                "valrep_constebm_walls_sandblast",
                "valrep_constebm_walls_mactan_stone",
                "valrep_constebm_walls_painted",
                "valrep_constebm_walls_other",
                "valrep_constebm_walls_other_value",
                "valrep_constebm_partitions_chb",
                "valrep_constebm_partitions_painted_cement",
                "valrep_constebm_partitions_anay",
                "valrep_constebm_partitions_wood_boards",
                "valrep_constebm_partitions_precast",
                "valrep_constebm_partitions_decorative_stone",
                "valrep_constebm_partitions_adobe",
                "valrep_constebm_partitions_granite",
                "valrep_constebm_partitions_cast_in_place",
                "valrep_constebm_partitions_sandblast",
                "valrep_constebm_partitions_mactan_stone",
                "valrep_constebm_partitions_ceramic_tiles",
                "valrep_constebm_partitions_chb_plywood",
                "valrep_constebm_partitions_hardiflex",
                "valrep_constebm_partitions_wallpaper",
                "valrep_constebm_partitions_painted",
                "valrep_constebm_partitions_other",
                "valrep_constebm_partitions_other_value",
                "valrep_constebm_windows_steel_casement",
                "valrep_constebm_windows_fixed_view",
                "valrep_constebm_windows_analok_sliding",
                "valrep_constebm_windows_alum_glass",
                "valrep_constebm_windows_aluminum_sliding",
                "valrep_constebm_windows_awning_type",
                "valrep_constebm_windows_powder_coated",
                "valrep_constebm_windows_wooden_frame",
                "valrep_constebm_windows_other",
                "valrep_constebm_windows_other_value",
                "valrep_constebm_doors_wood_panel",
                "valrep_constebm_doors_pvc",
                "valrep_constebm_doors_analok_sliding",
                "valrep_constebm_doors_screen_door",
                "valrep_constebm_doors_flush",
                "valrep_constebm_doors_molded_door",
                "valrep_constebm_doors_aluminum_sliding",
                "valrep_constebm_doors_flush_french",
                "valrep_constebm_doors_other",
                "valrep_constebm_doors_other_value",
                "valrep_constebm_ceiling_plywood",
                "valrep_constebm_ceiling_painted_gypsum",
                "valrep_constebm_ceiling_soffit_slab",
                "valrep_constebm_ceiling_metal_deck",
                "valrep_constebm_ceiling_hardiflex",
                "valrep_constebm_ceiling_plywood_tg",
                "valrep_constebm_ceiling_plywood_pvc",
                "valrep_constebm_ceiling_painted",
                "valrep_constebm_ceiling_with_cornice",
                "valrep_constebm_ceiling_with_moulding",
                "valrep_constebm_ceiling_drop_ceiling",
                "valrep_constebm_ceiling_other",
                "valrep_constebm_ceiling_other_value",
                "valrep_constebm_roof_pre_painted",
                "valrep_constebm_roof_rib_type",
                "valrep_constebm_roof_tilespan",
                "valrep_constebm_roof_tegula_asphalt",
                "valrep_constebm_roof_tegula_longspan",
                "valrep_constebm_roof_tegula_gi",
                "valrep_constebm_roof_steel_concrete",
                "valrep_constebm_roof_polycarbonate",
                "valrep_constebm_roof_on_steel_trusses",
                "valrep_constebm_roof_on_wooden_trusses",
                "valrep_constebm_roof_other",
                "valrep_constebm_roof_other_value",
                "valrep_constebm_valuation_total_area",
                "valrep_constebm_valuation_total_proj_cost",
                "valrep_constebm_valuation_remarks"
        };

        try {
            Log.e("recheck", xml2);
            JSONObject records2 = new JSONObject(xml2);

            //for main
            for (int a = 0; a <= sqlite_string.length - 1; a++) {
                if (records2.has(case_center_string[a])) {
                    sqlite_string[a] = records2.getString(case_center_string[a]);
                } else {
                    sqlite_string[a] = "";
                }
            }


            db.addConstruction_Ebm(new Construction_Ebm_API(
                    record_id,
                    sqlite_string[0],//report_date_inspected_month,
                    sqlite_string[1],//report_date_inspected_day,
                    sqlite_string[2],//report_date_inspected_year,
                    sqlite_string[3],//report_time_inspected,
                    sqlite_string[4],//report_project_type,
                    sqlite_string[5],//report_floor_area,
                    sqlite_string[6],//report_storeys,
                    sqlite_string[7],//report_expected_economic_life,
                    sqlite_string[8],//report_type_of_housing_unit,
                    sqlite_string[9],//report_const_type_reinforced_concrete,
                    sqlite_string[10],//report_const_type_semi_concrete,
                    sqlite_string[11],//report_const_type_mixed_materials,
                    sqlite_string[12],//report_foundation_concrete,
                    sqlite_string[13],//report_foundation_other,
                    sqlite_string[14],//report_foundation_other_value,
                    sqlite_string[15],//report_post_concrete,
                    sqlite_string[16],//report_post_concrete_timber,
                    sqlite_string[17],//report_post_steel,
                    sqlite_string[18],//report_post_other,
                    sqlite_string[19],//report_post_other_value,
                    sqlite_string[20],//report_beams_concrete,
                    sqlite_string[21],//report_beams_timber,
                    sqlite_string[22],//report_beams_steel,
                    sqlite_string[23],//report_beams_other,
                    sqlite_string[24],//report_beams_other_value,
                    sqlite_string[25],//report_floors_concrete,
                    sqlite_string[26],//report_floors_tiles,
                    sqlite_string[27],//report_floors_tiles_cement,
                    sqlite_string[28],//report_floors_laminated_wood,
                    sqlite_string[29],//report_floors_ceramic_tiles,
                    sqlite_string[30],//report_floors_wood_planks,
                    sqlite_string[31],//report_floors_marble_washout,
                    sqlite_string[32],//report_floors_concrete_boards,
                    sqlite_string[33],//report_floors_granite_tiles,
                    sqlite_string[34],//report_floors_marble_wood,
                    sqlite_string[35],//report_floors_carpet,
                    sqlite_string[36],//report_floors_other,
                    sqlite_string[37],//report_floors_other_value,
                    sqlite_string[38],//report_walls_chb,
                    sqlite_string[39],//report_walls_chb_cement,
                    sqlite_string[40],//report_walls_anay,
                    sqlite_string[41],//report_walls_chb_wood,
                    sqlite_string[42],//report_walls_precast,
                    sqlite_string[43],//report_walls_decorative_stone,
                    sqlite_string[44],//report_walls_adobe,
                    sqlite_string[45],//report_walls_ceramic_tiles,
                    sqlite_string[46],//report_walls_cast_in_place,
                    sqlite_string[47],//report_walls_sandblast,
                    sqlite_string[48],//report_walls_mactan_stone,
                    sqlite_string[49],//report_walls_painted,
                    sqlite_string[50],//report_walls_other,
                    sqlite_string[51],//report_walls_other_value,
                    sqlite_string[52],//report_partitions_chb,
                    sqlite_string[53],//report_partitions_painted_cement,
                    sqlite_string[54],//report_partitions_anay,
                    sqlite_string[55],//report_partitions_wood_boards,
                    sqlite_string[56],//report_partitions_precast,
                    sqlite_string[57],//report_partitions_decorative_stone,
                    sqlite_string[58],//report_partitions_adobe,
                    sqlite_string[59],//report_partitions_granite,
                    sqlite_string[60],//report_partitions_cast_in_place,
                    sqlite_string[61],//report_partitions_sandblast,
                    sqlite_string[62],//report_partitions_mactan_stone,
                    sqlite_string[63],//report_partitions_ceramic_tiles,
                    sqlite_string[64],//report_partitions_chb_plywood,
                    sqlite_string[65],//report_partitions_hardiflex,
                    sqlite_string[66],//report_partitions_wallpaper,
                    sqlite_string[67],//report_partitions_painted,
                    sqlite_string[68],//report_partitions_other,
                    sqlite_string[69],//report_partitions_other_value,
                    sqlite_string[70],//report_windows_steel_casement,
                    sqlite_string[71],//report_windows_fixed_view,
                    sqlite_string[72],//report_windows_analok_sliding,
                    sqlite_string[73],//report_windows_alum_glass,
                    sqlite_string[74],//report_windows_aluminum_sliding,
                    sqlite_string[75],//report_windows_awning_type,
                    sqlite_string[76],//report_windows_powder_coated,
                    sqlite_string[77],//report_windows_wooden_frame,
                    sqlite_string[78],//report_windows_other,
                    sqlite_string[79],//report_windows_other_value,
                    sqlite_string[80],//report_doors_wood_panel,
                    sqlite_string[81],//report_doors_pvc,
                    sqlite_string[82],//report_doors_analok_sliding,
                    sqlite_string[83],//report_doors_screen_door,
                    sqlite_string[84],//report_doors_flush,
                    sqlite_string[85],//report_doors_molded_door,
                    sqlite_string[86],//report_doors_aluminum_sliding,
                    sqlite_string[87],//report_doors_flush_french,
                    sqlite_string[88],//report_doors_other,
                    sqlite_string[89],//report_doors_other_value,
                    sqlite_string[90],//report_ceiling_plywood,
                    sqlite_string[91],//report_ceiling_painted_gypsum,
                    sqlite_string[92],//report_ceiling_soffit_slab,
                    sqlite_string[93],//report_ceiling_metal_deck,
                    sqlite_string[94],//report_ceiling_hardiflex,
                    sqlite_string[95],//report_ceiling_plywood_tg,
                    sqlite_string[96],//report_ceiling_plywood_pvc,
                    sqlite_string[97],//report_ceiling_painted,
                    sqlite_string[98],//report_ceiling_with_cornice,
                    sqlite_string[99],//report_ceiling_with_moulding,
                    sqlite_string[100],//report_ceiling_drop_ceiling,
                    sqlite_string[101],//report_ceiling_other,
                    sqlite_string[102],//report_ceiling_other_value,
                    sqlite_string[103],//report_roof_pre_painted,
                    sqlite_string[104],//report_roof_rib_type,
                    sqlite_string[105],//report_roof_tilespan,
                    sqlite_string[106],//report_roof_tegula_asphalt,
                    sqlite_string[107],//report_roof_tegula_longspan,
                    sqlite_string[108],//report_roof_tegula_gi,
                    sqlite_string[109],//report_roof_steel_concrete,
                    sqlite_string[110],//report_roof_polycarbonate,
                    sqlite_string[111],//report_roof_on_steel_trusses,
                    sqlite_string[112],//report_roof_on_wooden_trusses,
                    sqlite_string[113],//report_roof_other,
                    sqlite_string[114],//report_roof_other_value,
                    sqlite_string[115],//report_valuation_total_area,
                    sqlite_string[116],//report_valuation_total_proj_cost,
                    sqlite_string[117]));//report_valuation_remarks

            if (records2.has("valrep_constebm_room_list")) {

                //load data into array string (sequentially sync to their equivalent string)
                String[] sqlite_string2 = new String[]{
                        report_room_list_area,
                        report_room_list_description,
                        report_room_list_est_proj_cost,
                        report_room_list_floor,
                        report_room_list_rcn};
                String[] case_center_string2 = new String[]{
                        "valrep_constebm_room_list_area",
                        "valrep_constebm_room_list_description",
                        "valrep_constebm_room_list_est_proj_cost",
                        "valrep_constebm_room_list_floor",
                        "valrep_constebm_room_list_rcn"};

                JSONArray cerl_ary = records2.getJSONArray("valrep_constebm_room_list");
                for (int x = 0; x < cerl_ary.length(); x++) {
                    JSONObject cerl_obj = cerl_ary.getJSONObject(x);
                    for (int y = 0; y <= sqlite_string2.length - 1; y++) {
                        if (cerl_ary.getJSONObject(x).has(case_center_string2[y])) {
                            sqlite_string2[y] = cerl_obj.getString(case_center_string2[y]);
                        } else {
                            sqlite_string2[y] = "";
                        }
                    }
                    db2.addConstruction_Ebm_Room_List(new Construction_Ebm_API_Room_List(
                            record_id,
                            sqlite_string2[0],//report_room_list_area,
                            sqlite_string2[1],//report_room_list_description,
                            sqlite_string2[2],//report_room_list_est_proj_cost,
                            sqlite_string2[3],//report_room_list_floor,
                            sqlite_string2[4]));//report_room_list_rcn));
                }
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            Log.e("", e.toString());
        }
    }

    public void syncPpcr() {
        // TODO Auto-generated method stub
        //main table variables
        String report_date_inspected_month, report_date_inspected_day,
                report_date_inspected_year, report_time_inspected, report_registered_owner,
                report_project_type, report_is_condo, report_remarks;

        String report_visit_month = "", report_visit_day = "", report_visit_year = "", report_nth = "", report_value_foundation = "", report_value_columns = "", report_value_beams_girders = "", report_value_surround_walls = "", report_value_interior_partition = "", report_value_roofing_works = "", report_value_plumbing_rough_in = "", report_value_electrical_rough_in = "", report_value_ceiling_works = "", report_value_doors_windows = "", report_value_plastering_wall = "", report_value_slab_include_finish = "", report_value_painting_finishing = "", report_value_plumbing_elect_fix = "", report_value_utilities_tapping = "", report_completion_foundation = "", report_completion_columns = "", report_completion_beams_girders = "", report_completion_surround_walls = "", report_completion_interior_partition = "", report_completion_roofing_works = "", report_completion_plumbing_rough_in = "", report_completion_electrical_rough_in = "", report_completion_ceiling_works = "", report_completion_doors_windows = "", report_completion_plastering_wall = "", report_completion_slab_include_finish = "", report_completion_painting_finishing = "", report_completion_plumbing_elect_fix = "", report_completion_utilities_tapping = "", report_total_value = "";

        try {
            Log.e("recheck", xml);
            JSONObject records2 = new JSONObject(xml2);

            if (records2.has("valrep_ppcr_date_inspected_month")) {
                report_date_inspected_month = records2.getString("valrep_ppcr_date_inspected_month");
            } else {
                report_date_inspected_month = "";
            }
            if (records2.has("valrep_ppcr_date_inspected_day")) {
                report_date_inspected_day = records2.getString("valrep_ppcr_date_inspected_day");
            } else {
                report_date_inspected_day = "";
            }
            if (records2.has("valrep_ppcr_date_inspected_year")) {
                report_date_inspected_year = records2.getString("valrep_ppcr_date_inspected_year");
            } else {
                report_date_inspected_year = "";
            }
            if (records2.has("valrep_ppcr_time_inspected")) {
                report_time_inspected = records2.getString("valrep_ppcr_time_inspected");
            } else {
                report_time_inspected = "";
            }
            if (records2.has("valrep_ppcr_registered_owner")) {
                report_registered_owner = records2.getString("valrep_ppcr_registered_owner");
            } else {
                report_registered_owner = "";
            }
            if (records2.has("valrep_ppcr_project_type")) {
                report_project_type = records2.getString("valrep_ppcr_project_type");
            } else {
                report_project_type = "";
            }
            if (records2.has("valrep_ppcr_is_condo")) {
                report_is_condo = records2.getString("valrep_ppcr_is_condo");
            } else {
                report_is_condo = "";
            }
            if (records2.has("valrep_ppcr_remarks")) {
                report_remarks = records2.getString("valrep_ppcr_remarks");
            } else {
                report_remarks = "";
            }

            db.addPpcr(new Ppcr_API(record_id, report_date_inspected_month, report_date_inspected_day,
                    report_date_inspected_year, report_time_inspected, report_registered_owner,
                    report_project_type, report_is_condo,
                    report_remarks));

            if (records2.has("valrep_ppcr_details")) {

                //load data into array string (sequentially sync to their equivalent string)
                String[] sqlite_string = new String[]{
                        report_visit_month,
                        report_visit_day,
                        report_visit_year,
                        report_nth,
                        report_value_foundation,
                        report_value_columns,
                        report_value_beams_girders,
                        report_value_surround_walls,
                        report_value_interior_partition,
                        report_value_roofing_works,
                        report_value_plumbing_rough_in,
                        report_value_electrical_rough_in,
                        report_value_ceiling_works,
                        report_value_doors_windows,
                        report_value_plastering_wall,
                        report_value_slab_include_finish,
                        report_value_painting_finishing,
                        report_value_plumbing_elect_fix,
                        report_value_utilities_tapping,
                        report_completion_foundation,
                        report_completion_columns,
                        report_completion_beams_girders,
                        report_completion_surround_walls,
                        report_completion_interior_partition,
                        report_completion_roofing_works,
                        report_completion_plumbing_rough_in,
                        report_completion_electrical_rough_in,
                        report_completion_ceiling_works,
                        report_completion_doors_windows,
                        report_completion_plastering_wall,
                        report_completion_slab_include_finish,
                        report_completion_painting_finishing,
                        report_completion_plumbing_elect_fix,
                        report_completion_utilities_tapping,
                        report_total_value};
                String[] case_center_string = new String[]{
                        "valrep_ppcr_visit_month",
                        "valrep_ppcr_visit_day",
                        "valrep_ppcr_visit_year",
                        "valrep_ppcr_nth",
                        "valrep_ppcr_value_foundation",
                        "valrep_ppcr_value_columns",
                        "valrep_ppcr_value_beams_girders",
                        "valrep_ppcr_value_sorround_walls",
                        "valrep_ppcr_value_interior_partition",
                        "valrep_ppcr_value_roofing_works",
                        "valrep_ppcr_value_plumbing_rough_in",
                        "valrep_ppcr_value_electrical_rough_in",
                        "valrep_ppcr_value_ceiling_works",
                        "valrep_ppcr_value_doors_windows",
                        "valrep_ppcr_value_plastering_wall",
                        "valrep_ppcr_value_slab_include_finish",
                        "valrep_ppcr_value_painting_finishing",
                        "valrep_ppcr_value_plumbing_elect_fix",
                        "valrep_ppcr_value_utilities_tapping",
                        "valrep_ppcr_completion_foundation",
                        "valrep_ppcr_completion_columns",
                        "valrep_ppcr_completion_beams_girders",
                        "valrep_ppcr_completion_surround_walls",
                        "valrep_ppcr_completion_interior_partition",
                        "valrep_ppcr_completion_roofing_works",
                        "valrep_ppcr_completion_plumbing_rough_in",
                        "valrep_ppcr_completion_electrical_rough_in",
                        "valrep_ppcr_completion_ceiling_works",
                        "valrep_ppcr_completion_doors_windows",
                        "valrep_ppcr_completion_plastering_wall",
                        "valrep_ppcr_completion_slab_include_finish",
                        "valrep_ppcr_completion_painting_finishing",
                        "valrep_ppcr_completion_plumbing_elect_fix",
                        "valrep_ppcr_completion_utilities_tapping",
                        "valrep_ppcr_total_value"};

                JSONArray details_ary = records2.getJSONArray("valrep_ppcr_details");
                for (int x = 0; x < details_ary.length(); x++) {
                    JSONObject details_obj = details_ary.getJSONObject(x);


                    for (int y = 0; y <= sqlite_string.length - 1; y++) {
                        if (details_ary.getJSONObject(x).has(case_center_string[y])) {
                            sqlite_string[y] = details_obj.getString(case_center_string[y]);
                        } else {
                            sqlite_string[y] = "";
                        }
                    }


                    db2.addPpcr_Details(new Ppcr_API_Details(
                            record_id,
                            sqlite_string[0],//report_visit_month,
                            sqlite_string[1],//report_visit_day,
                            sqlite_string[2],//report_visit_year,
                            sqlite_string[3],//report_nth,
                            sqlite_string[4],//report_value_foundation,
                            sqlite_string[5],//report_value_columns,
                            sqlite_string[6],//report_value_beams_girders,
                            sqlite_string[7],//report_value_surround_walls,
                            sqlite_string[8],//report_value_interior_partition,
                            sqlite_string[9],//report_value_roofing_works,
                            sqlite_string[10],//report_value_plumbing_rough_in,
                            sqlite_string[11],//report_value_electrical_rough_in,
                            sqlite_string[12],//report_value_ceiling_works,
                            sqlite_string[13],//report_value_doors_windows,
                            sqlite_string[14],//report_value_plastering_wall,
                            sqlite_string[15],//report_value_slab_include_finish,
                            sqlite_string[16],//report_value_painting_finishing,
                            sqlite_string[17],//report_value_plumbing_elect_fix,
                            sqlite_string[18],//report_value_utilities_tapping,
                            sqlite_string[19],//report_completion_foundation,
                            sqlite_string[20],//report_completion_columns,
                            sqlite_string[21],//report_completion_beams_girders,
                            sqlite_string[22],//report_completion_surround_walls,
                            sqlite_string[23],//report_completion_interior_partition,
                            sqlite_string[24],//report_completion_roofing_works,
                            sqlite_string[25],//report_completion_plumbing_rough_in,
                            sqlite_string[26],//report_completion_electrical_rough_in,
                            sqlite_string[27],//report_completion_ceiling_works,
                            sqlite_string[28],//report_completion_doors_windows,
                            sqlite_string[29],//report_completion_plastering_wall,
                            sqlite_string[30],//report_completion_slab_include_finish,
                            sqlite_string[31],//report_completion_painting_finishing,
                            sqlite_string[32],//report_completion_plumbing_elect_fix,
                            sqlite_string[33],//report_completion_utilities_tapping,
                            sqlite_string[34]//report_total_value
                    ));
                }
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            Log.e("", e.toString());
        }
    }

    public void syncTownhouse() {
        // TODO Auto-generated method stub
        //main table
        //lot details
        String report_propdesc_tct_no = "", report_propdesc_lot = "", report_propdesc_block = "", report_propdesc_survey_nos = "", report_propdesc_area = "", report_propdesc_registry_date_month = "", report_propdesc_registry_date_day = "", report_propdesc_registry_date_year = "", report_propdesc_registered_owner = "", report_propdesc_registry_of_deeds = "";
        //imp details
        String report_impsummary1_no_of_floors = "", report_impsummary1_building_desc = "", report_impsummary1_erected_on_lot = "", report_impsummary1_fa = "", report_impsummary1_fa_per_td = "", report_impsummary1_actual_utilization = "", report_impsummary1_usage_declaration = "", report_impsummary1_owner = "", report_desc_property_type = "", report_impsummary1_socialized_housing = "", report_desc_foundation = "", report_desc_columns_posts = "", report_desc_beams = "", report_desc_exterior_walls = "", report_desc_interior_walls = "", report_desc_imp_flooring = "", report_desc_doors = "", report_desc_imp_windows = "", report_desc_ceiling = "", report_desc_imp_roofing = "", report_desc_trusses = "", report_desc_economic_life = "", report_desc_effective_age = "", report_desc_imp_remain_life = "", report_desc_occupants = "", report_desc_owned_or_leased = "", report_desc_imp_floor_area = "", report_desc_confirmed_thru = "", report_desc_observed_condition = "", report_ownership_of_property = "",report_impsummary_no_of_bedroom = "", valrep_townhouse_impsummary_no_of_tb = "";
        String report_imp_features = "";//nested

        //valuation
        String report_value_tct_no = "", report_value_lot_no = "", report_value_block_no = "", report_value_lot_area = "", report_value_floor_area = "", report_value_unit_value = "", report_value_land_value = "";
        // rdps
        String report_prev_app_name = "", report_prev_app_location = "", report_prev_app_area = "", report_prev_app_value = "", report_prev_app_date = "";



        //ADDED From IAN

        String report_main_prev_desc = "", report_main_prev_area = "", report_main_prev_unit_value = "", report_main_prev_appraised_value = "",
                valrep_townhouse_propdesc_registry_date="";


        try {
            Log.e("recheck", xml2);
            JSONObject records2 = new JSONObject(xml2);


            db2.addRecord(gds.emptyFields(to.townhouse_db().size() - 2,record_id), to.townhouse_db(),"townhouse");
            ArrayList<String> townhousedb = new ArrayList<String>();
            townhousedb=  to.townhouse_db();
            townhousedb.remove(0);
            gds.reworkMethod(xml2, db2, townhousedb, to.townhouse_cc, record_id, "townhouse");
            Log.e("Townhouse Rework", "done");



            if (records2.has("valrep_townhouse_title_details")) {

                //load data into array string (sequentially sync to their equivalent string)
                String[] sqlite_string2 = new String[]{
                        report_propdesc_tct_no,
                        report_propdesc_lot,
                        report_propdesc_block,
                        report_propdesc_survey_nos,
                        report_propdesc_area,
                        valrep_townhouse_propdesc_registry_date,
                        report_propdesc_registered_owner,
                        report_propdesc_registry_of_deeds
                };

                String[] case_center_string2 = new String[]{
                        "valrep_townhouse_propdesc_title_no",
                        "valrep_townhouse_propdesc_lot_no",
                        "valrep_townhouse_propdesc_block_no",
                        "valrep_townhouse_propdesc_survey_no",
                        "valrep_townhouse_propdesc_area",
                        "valrep_townhouse_propdesc_registry_date",
                        "valrep_townhouse_propdesc_registered_owner",
                        "valrep_townhouse_propdesc_reg_of_deeds"
                };

                JSONArray lot_details_array = records2.getJSONArray("valrep_townhouse_title_details");
                for (int x = 0; x < lot_details_array.length(); x++) {
                    JSONObject lot_details_object = lot_details_array.getJSONObject(x);
                    for (int y = 0; y <= sqlite_string2.length - 1; y++) {
                        if (lot_details_array.getJSONObject(x).has(case_center_string2[y])) {
                            sqlite_string2[y] = lot_details_object.getString(case_center_string2[y]);
                        } else {
                            sqlite_string2[y] = "";
                        }
                    }
                    db2.addTownhouse_Lot_Details(new Townhouse_API_Lot_Details(
                            record_id, sqlite_string2[0],// report_propdesc_tct_no,
                            sqlite_string2[1],// report_propdesc_lot,
                            sqlite_string2[2],// report_propdesc_block,
                            sqlite_string2[3],// report_propdesc_survey_nos,
                            sqlite_string2[4],// report_propdesc_area,
                            sqlite_string2[5],// valrep_townhouse_propdesc_registry_date,
                            sqlite_string2[6],// report_propdesc_registered_owner,
                            sqlite_string2[7]));// report_propdesc_registry_of_deeds,));
                }
            }

            if (records2.has("valrep_townhouse_imp_details")) {
                //load data into array string (sequentially sync to their equivalent string)
                String[] sqlite_string3 = new String[]{
                        report_impsummary1_no_of_floors,
                        report_impsummary1_building_desc,
                        report_impsummary1_erected_on_lot,
                        report_impsummary1_fa,
                        report_impsummary1_fa_per_td,
                        report_impsummary1_actual_utilization,
                        report_impsummary1_usage_declaration,
                        report_impsummary1_owner,
                        report_desc_property_type,
                        report_impsummary1_socialized_housing,
                        report_desc_foundation,
                        report_desc_columns_posts,
                        report_desc_beams,
                        report_desc_exterior_walls,
                        report_desc_interior_walls,
                        report_desc_imp_flooring,
                        report_desc_doors,
                        report_desc_imp_windows,
                        report_desc_ceiling,
                        report_desc_imp_roofing,
                        report_desc_trusses,
                        report_desc_economic_life,
                        report_desc_effective_age,
                        report_desc_imp_remain_life,
                        report_desc_occupants,
                        report_desc_owned_or_leased,
                        report_desc_imp_floor_area,
                        report_desc_confirmed_thru,
                        report_desc_observed_condition,
                        report_ownership_of_property,
                        report_impsummary_no_of_bedroom,
                        valrep_townhouse_impsummary_no_of_tb,
                        report_imp_features//nested table
                };

                String[] case_center_string3 = new String[]{
                        "valrep_townhouse_impsummary_no_of_floors",
                        "valrep_townhouse_impsummary_desc_of_bldg",
                        "valrep_townhouse_impsummary_erected_on_lot",
                        "valrep_townhouse_impsummary_fa",
                        "valrep_townhouse_impsummary_fa_per_td",
                        "valrep_townhouse_impsummary_actual_utilization",
                        "valrep_townhouse_impsummary_declaration_as_to_usage",
                        "valrep_townhouse_impsummary_owners",
                        "valrep_townhouse_impsummary_socialized_housing",
                        "valrep_townhouse_impsummary_ownership_of_property",
                        "valrep_townhouse_imp_foundation",
                        "valrep_townhouse_imp_columns_posts",
                        "valrep_townhouse_imp_beams",
                        "valrep_townhouse_imp_exterior_walls",
                        "valrep_townhouse_imp_interior_walls",
                        "valrep_townhouse_imp_flooring",
                        "valrep_townhouse_imp_doors",
                        "valrep_townhouse_imp_windows",
                        "valrep_townhouse_imp_ceiling",
                        "valrep_townhouse_imp_roofing",
                        "valrep_townhouse_imp_trusses",
                        "valrep_townhouse_imp_economic_life",
                        "valrep_townhouse_imp_effective_age",
                        "valrep_townhouse_imp_remaining_eco_life",
                        "valrep_townhouse_imp_occupants",
                        "valrep_townhouse_imp_owned_leased",
                        "valrep_townhouse_imp_total_floor_area",
                        "valrep_townhouse_imp_confirmed_thru",
                        "valrep_townhouse_imp_observed_condition",
                        "valrep_townhouse_imp_property_type",
                        "valrep_townhouse_impsummary_no_of_bedrooms",
                        "valrep_townhouse_impsummary_no_of_tb",
                        "valrep_townhouse_imp_features"//nested table
                };
                JSONArray doi_ary = records2.getJSONArray("valrep_townhouse_imp_details");
                for (int x = 0; x < doi_ary.length(); x++) {
                    JSONObject doi_obj = doi_ary.getJSONObject(x);
                    for (int y = 0; y <= sqlite_string3.length - 1; y++) {
                        if (doi_ary.getJSONObject(x).has(case_center_string3[y])) {
                            sqlite_string3[y] = doi_obj.getString(case_center_string3[y]);
                        } else {
                            sqlite_string3[y] = "";
                        }
                    }
                    db2.addTownhouse_Imp_Details(new Townhouse_API_Imp_Details(
                            record_id,
                            sqlite_string3[0],//report_impsummary1_no_of_floors,
                            sqlite_string3[1],//report_impsummary1_building_desc,
                            sqlite_string3[2],//report_impsummary1_erected_on_lot,
                            sqlite_string3[3],//report_impsummary1_fa,
                            sqlite_string3[4],//report_impsummary1_fa_per_td,
                            sqlite_string3[5],//report_impsummary1_actual_utilization,
                            sqlite_string3[6],//report_impsummary1_usage_declaration,
                            sqlite_string3[7],//report_impsummary1_owner,
                            sqlite_string3[8],//report_desc_property_type,
                            sqlite_string3[9],//report_impsummary1_socialized_housing,
                            sqlite_string3[10],//report_desc_foundation,
                            sqlite_string3[11],//report_desc_columns_posts,
                            sqlite_string3[12],//report_desc_beams,
                            sqlite_string3[13],//report_desc_exterior_walls,
                            sqlite_string3[14],//report_desc_interior_walls,
                            sqlite_string3[15],//report_desc_imp_flooring,
                            sqlite_string3[16],//report_desc_doors,
                            sqlite_string3[17],//report_desc_imp_windows,
                            sqlite_string3[18],//report_desc_ceiling,
                            sqlite_string3[19],//report_desc_imp_roofing,
                            sqlite_string3[20],//report_desc_trusses,
                            sqlite_string3[21],//report_desc_economic_life,
                            sqlite_string3[22],//report_desc_effective_age,
                            sqlite_string3[23],//report_desc_imp_remain_life,
                            sqlite_string3[24],//report_desc_occupants,
                            sqlite_string3[25],//report_desc_owned_or_leased,
                            sqlite_string3[26],//report_desc_imp_floor_area,
                            sqlite_string3[27],//report_desc_confirmed_thru,
                            sqlite_string3[28],//report_desc_observed_condition,
                            sqlite_string3[29],//report_ownership_of_property,
                            sqlite_string3[30],//report_impsummary_no_of_bedroom,
                            sqlite_string3[31]//report_impsummary_no_of_bedroom,
                    ));
                    //get latest imp_details_id and hold for saving
                    List<Townhouse_API_Imp_Details> liidf = db2.getLatest_Data_TH(record_id);
                    if (!liidf.isEmpty()) {
                        for (Townhouse_API_Imp_Details imidf : liidf) {
                            id_holder = String.valueOf(imidf.getID());
                        }
                    }
                    //get latest imp_details_id and hold for saving
                    id_stack.add(id_holder);
                }
            }
//sync nested
            if (records2.has("valrep_townhouse_imp_details")) {
                /**
                 * sync nested details (should run inside the loop, for getting the right id
                 */
                JSONObject mainObj = new JSONObject(xml2);
                if (mainObj != null) {
                    JSONArray list = mainObj.getJSONArray("valrep_townhouse_imp_details");//2nd layer
                    Log.e("list", list.toString());
                    if (list != null) {
                        for (int i = 0; i < list.length(); i++) {
                            JSONObject elem = list.getJSONObject(i);
                            Log.e("elem", elem.toString() + " index:" + i);
                            if (elem != null) {
                                if (elem.has("valrep_townhouse_imp_features")) {//check json has this field
                                    JSONArray prods = elem.getJSONArray("valrep_townhouse_imp_features");//3rd layer
                                    Log.e("prods", prods.toString());
                                    if (prods != null) {
                                        for (int j = 0; j < prods.length(); j++) {
                                            JSONObject innerElem = prods.getJSONObject(j);
                                            Log.e("innerElem", innerElem.toString() + " index:" + j);
                                            if (innerElem != null) {
                                                String feature_area = innerElem.getString("valrep_townhouse_imp_feature_area");
                                                String floor_area = innerElem.getString("valrep_townhouse_imp_feature_floor_area");
                                                String feature_description = innerElem.getString("valrep_townhouse_imp_feature_description");

                                                db2.addTownhouse_Imp_Details_Features(new Townhouse_API_Imp_Details_Features(
                                                        record_id,
                                                        id_stack.get(i),//id_holder,//id
                                                        feature_area,//sqlite_string4[0],//report_desc_features,
                                                        floor_area,//sqlite_string4[1],//report_desc_features_area,
                                                        feature_description//sqlite_string4[2]//report_desc_features_area_desc
                                                ));
                                                Log.e("features", record_id + "\n" + feature_area + "\n" + floor_area + "\n" + feature_description);
                                            }
                                        }
                                    }
                                }//check json has this field
                            }
                        }
                    }
                }
                //end of sync of nested details
                id_stack.clear();
            }

            if (records2.has("valrep_townhouse_unit_valuation")) {

                //load data into array string (sequentially sync to their equivalent string)
                String[] sqlite_string5 = new String[]{
                        report_value_tct_no,
                        report_value_lot_no,
                        report_value_block_no,
                        report_value_lot_area,
                        report_value_floor_area,
                        report_value_unit_value,
                        report_value_land_value
                };

                String[] case_center_string5 = new String[]{
                        "valrep_townhouse_value_tct_no",
                        "valrep_townhouse_value_lot_no",
                        "valrep_townhouse_value_block_no",
                        "valrep_townhouse_value_lot_area",
                        "valrep_townhouse_value_floor_area",
                        "valrep_townhouse_value_unit_value",
                        "valrep_townhouse_value_total_land_value"
                };

                JSONArray doif_ary = records2.getJSONArray("valrep_townhouse_unit_valuation");
                for (int x = 0; x < doif_ary.length(); x++) {
                    JSONObject doif_obj = doif_ary.getJSONObject(x);
                    for (int y = 0; y <= sqlite_string5.length - 1; y++) {
                        if (doif_ary.getJSONObject(x).has(case_center_string5[y])) {
                            sqlite_string5[y] = doif_obj.getString(case_center_string5[y]);
                        } else {
                            sqlite_string5[y] = "";
                        }
                    }

                    db2.addTownhouse_Lot_Valuation_Details(new Townhouse_API_Lot_Valuation_Details(
                            record_id,
                            sqlite_string5[0],//report_value_tct_no,
                            sqlite_string5[1],//report_value_lot_no,
                            sqlite_string5[2],//report_value_block_no,
                            sqlite_string5[3],//report_value_lot_area,
                            sqlite_string5[4],//report_value_floor_area,
                            sqlite_string5[5],//report_value_unit_value,
                            sqlite_string5[6]));//report_value_land_value
                }
            }//end of sub if (valuation)


            if (records2.has("valrep_townhouse_prev_appraisal")) {
                String[] sqlite_string6 = new String[]{
                        report_prev_app_name,
                        report_prev_app_location,
                        report_prev_app_area,
                        report_prev_app_value,
                        report_prev_app_date
                };
                String[] case_center_string6 = new String[]{
                        "valrep_townhouse_prev_app_account",
                        "valrep_townhouse_prev_app_location",
                        "valrep_townhouse_prev_app_area",
                        "valrep_townhouse_prev_app_value",
                        "valrep_townhouse_prev_app_date"
                };
                // rdps
                JSONArray rdps_ary = records2.getJSONArray("valrep_townhouse_prev_appraisal");
                for (int x = 0; x < rdps_ary.length(); x++) {
                    JSONObject rdps_obj = rdps_ary.getJSONObject(x);

                    for (int y = 0; y <= sqlite_string6.length - 1; y++) {
                        if (rdps_ary.getJSONObject(x).has(case_center_string6[y])) {
                            sqlite_string6[y] = rdps_obj.getString(case_center_string6[y]);
                        } else {
                            sqlite_string6[y] = "";
                        }
                    }

                    db2.addTownhouse_RDPS(new Townhouse_API_RDPS(
                            record_id,
                            sqlite_string6[0],//report_prev_app_name,
                            sqlite_string6[1],//report_prev_app_location,
                            sqlite_string6[2],//report_prev_app_area,
                            sqlite_string6[3],//report_prev_app_value,
                            sqlite_string6[4]));//report_prev_app_date
                }
            }


            //ADDED From IAN Prev App
            if (records2.has("valrep_townhouse_main_prev_appraisal")) {
                String[] sqlite_string6 = new String[]{
                        report_main_prev_desc,
                        report_main_prev_area,
                        report_main_prev_unit_value,
                        report_main_prev_appraised_value

                };
                String[] case_center_string6 = new String[]{
                        "valrep_townhouse_prev_desc",
                        "valrep_townhouse_prev_area",
                        "valrep_townhouse_prev_unit_value",
                        "valrep_townhouse_prev_appraised_value"
                };
                // rdps
                JSONArray prev_app_ary = records2.getJSONArray("valrep_townhouse_main_prev_appraisal");
                for (int x = 0; x < prev_app_ary.length(); x++) {
                    JSONObject prev_app_obj = prev_app_ary.getJSONObject(x);

                    for (int y = 0; y <= sqlite_string6.length - 1; y++) {
                        if (prev_app_ary.getJSONObject(x).has(case_center_string6[y])) {
                            sqlite_string6[y] = prev_app_obj.getString(case_center_string6[y]);
                        } else {
                            sqlite_string6[y] = "";
                        }
                    }

                    db2.addTownhouse_Prev_Appraisal(new Townhouse_API_Prev_Appraisal(
                            record_id,
                            sqlite_string6[0],//report_main_prev_desc,
                            sqlite_string6[1],//report_main_prev_area,
                            sqlite_string6[2],//report_main_prev_unit_value,
                            sqlite_string6[3]));//report_main_prev_appraised_value
                }
            }


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            Log.e("", e.toString());
        }
    }

    public void syncWaf(){
        try {
            Log.e("recheck", xml2);

            db2.addRecord(gds.emptyFields(to.waf_db().size() - 2,record_id), to.waf_db(),Tables.waf.table_name);
            ArrayList<String> wafdb = new ArrayList<String>();
            wafdb=  to.waf_db();
            wafdb.remove(0);
            gds.reworkMethod(xml2, db2, wafdb, to.waf_cc, record_id, Tables.waf.table_name);
            Log.e("Waf Rework", "done");

            //listing
            gds.reworkMethod(xml2, "valrep_waf_listings_table", db2, to.Waf_listing(), to.Waf_Listing, record_id, Tables.waf.table_name);


        } catch (Exception e) {
            Log.e("", e.toString());
        }
    }

    public void syncSpv(){
        try {
            Log.e("recheck", xml2);


            db2.addRecord(gds.emptyFields(to.spv_db().size() - 2,record_id), to.spv_db(),Tables.spv.table_name);
            ArrayList<String> spvdb;
            spvdb=  to.spv_db();
            spvdb.remove(0);
            gds.reworkMethod(xml2, db2, spvdb, to.spv_cc, record_id, Tables.spv.table_name);
            Log.e("spv Rework", "done");

            gds.reworkMethod(xml2, "valrep_landimp_imp_details", db2, to.valrep_landimp_imp_details_db(), to.valrep_landimp_imp_details_cc, record_id, Tables.valrep_landimp_imp_details.table_name);

            gds.reworkMethod(xml2, "valrep_spv_land_imp_comps_table", db2, to.valrep_spv_land_imp_comps_table_db(), to.valrep_spv_land_imp_comps_table_cc, record_id, Tables.valrep_spv_land_imp_comps_table.table_name);

            gds.reworkMethodNestedArray2(xml2, db2, record_id, "valrep_landimp_imp_valuation", "valrep_landimp_imp_valuation_details",
                    to.valrep_landimp_imp_valuation_db(),to.valrep_landimp_imp_valuation_details_db(),
                    to.valrep_landimp_imp_valuation_cc,to.valrep_landimp_imp_valuation_details_cc,
                    Tables.valrep_landimp_imp_valuation.table_name,Tables.valrep_landimp_imp_valuation_details.table_name);

            gds.reworkMethod(xml2, "valrep_spv_landimp_summary_of_value_table", db2, to.valrep_spv_landimp_summary_of_value_table_db(),
                    to.valrep_spv_landimp_summary_of_value_table_cc, record_id, Tables.valrep_spv_landimp_summary_of_value_table.table_name);


        }catch (Exception e) {
            Log.e("", e.toString());
        }
    }
    @Override
    public void onUserInteraction(){
        st.resetDisconnectTimer();
    }
    @Override
    public void onStop() {
        super.onStop();
        st.stopDisconnectTimer();
    }
    @Override
    public void onResume() {
        super.onResume();
        st.resetDisconnectTimer();
    }


}