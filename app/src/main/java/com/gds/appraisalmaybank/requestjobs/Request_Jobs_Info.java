package com.gds.appraisalmaybank.requestjobs;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.attachments.Attachments_Inflater;
import com.gds.appraisalmaybank.check_network.NetworkUtil;
import com.gds.appraisalmaybank.database.Attachments_API;
import com.gds.appraisalmaybank.database.Condo_API;
import com.gds.appraisalmaybank.database.Construction_Ebm_API;
import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Motor_Vehicle_API;
import com.gds.appraisalmaybank.database.Ppcr_API;
import com.gds.appraisalmaybank.database.Report_Accepted_Jobs;
import com.gds.appraisalmaybank.database.Report_Accepted_Jobs_Contacts;
import com.gds.appraisalmaybank.database.Report_filename;
import com.gds.appraisalmaybank.database.Required_Attachments;
import com.gds.appraisalmaybank.database.TableObjects;
import com.gds.appraisalmaybank.database.Vacant_Lot_API;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database2.Land_Improvements_API_Lot_Details;
import com.gds.appraisalmaybank.database2.Land_Improvements_API_Lot_Valuation_Details;
import com.gds.appraisalmaybank.database2.Ppcr_API_Details;
import com.gds.appraisalmaybank.database2.Townhouse_API_Imp_Details;
import com.gds.appraisalmaybank.database2.Townhouse_API_Lot_Details;
import com.gds.appraisalmaybank.database2.Townhouse_API_Lot_Valuation_Details;
import com.gds.appraisalmaybank.database2.Vacant_Lot_API_Lot_Details;
import com.gds.appraisalmaybank.database2.Vacant_Lot_API_Lot_Valuation_Details;
import com.gds.appraisalmaybank.database_new.DatabaseHandler_New;
import com.gds.appraisalmaybank.database_new.Tables;
import com.gds.appraisalmaybank.json.JSONParser2;
import com.gds.appraisalmaybank.json.MyHttpClient;
import com.gds.appraisalmaybank.json.TLSConnection;
import com.gds.appraisalmaybank.json.UserFunctions;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import org.apache.http.NameValuePair;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

@SuppressLint("InflateParams")
public class Request_Jobs_Info extends Activity implements OnClickListener {
    ActionBar actionBar;
    private static final String TAG_RECORD_ID = "record_id";
    private static final String TAG_UID = "uid";
    String uid = "";
    private static final String TAG_PASS_STAT = "pass_stat";
    String pass_stat = "online";
    Global gs;
    Session_Timer st = new Session_Timer(this);
    String record_id;
    private ProgressDialog pDialog;

    //for tls
    JSONObject jsonResponse = new JSONObject();
    ArrayList<String> field = new ArrayList<>();
    ArrayList<String> value = new ArrayList<>();
    GDS_methods gds = new GDS_methods();
    TableObjects to = new TableObjects();
    TLSConnection tlscon = new TLSConnection();
    TextView tv_requested_month, tv_requested_day, tv_requested_year,
            tv_classification, tv_account_name,tv_company_name, tv_requesting_party, tv_requestor,
            tv_control_no, tv_appraisal_type, tv_nature_appraisal, tv_kind_of_appraisal,
            tv_ins_month1, tv_ins_day1, tv_ins_year1, tv_ins_month2,
            tv_ins_day2, tv_ins_year2, tv_com_month, tv_com_day, tv_com_year,
            tv_tct_no, tv_unit_no, tv_bldg_name, tv_street_no, tv_street_name,
            tv_village, tv_district, tv_zip_code, tv_city, tv_province,
            tv_region, tv_country, tv_address, tv_tct_cct,
            tv_lot_no, tv_block_no, tv_app_request_remarks, tv_app_branch_code;
    TextView tv_vehicle_type, tv_car_model, tv_car_loan_purpose,
            tv_car_mileage, tv_car_series, tv_car_color, tv_car_plate_no,
            tv_car_body_type, tv_car_displacement, tv_car_motor_no,
            tv_car_chassis_no, tv_car_no_cylinders, tv_car_cr_no, tv_car_cr_date;
    String requested_month, requested_day, requested_year, classification,
            account_fname, account_mname, account_lname,is_comp,comp_name, requesting_party, requestor,
            control_no, appraisal_type, nature_appraisal, kind_of_appraisal, ins_month1, ins_day1,
            ins_year1, ins_month2, ins_day2, ins_year2, com_month, com_day,
            com_year, tct_no, unit_no, bldg_name, street_no, street_name,
            village, district, zip_code, city, province, region, country,
            lot_no, block_no,
            counter, app_request_remarks, app_branch_code,app_main_id,app_account_is_company,app_account_company_name,app_unacceptable_collateral;
    //lot details
    String lot_tct_no,lot_lot_no,lot_block_no,lot_survey_no,lot_area,lot_registry_date,lot_registered_owner,lot_deeds;
    //motor_vehicle special fields
    String vehicle_type, car_model, car_loan_purpose, car_mileage, car_series,
            car_color, car_plate_no, car_body_type, car_displacement,
            car_motor_no, car_chassis_no, car_no_cylinders, car_cr_no, car_cr_date_month,
            car_cr_date_day, car_cr_date_year;
    String c_requested_month, c_requested_day, c_requested_year,
            c_classification, c_account_fname, c_account_mname,
            c_account_lname,c_is_company,c_company_name, c_requesting_party, c_requestor, c_control_no,
            output_appraisal_type, c_appraisal_type, c_nature_appraisal, c_kind_of_appraisal,
            c_ins_month1, c_ins_day1, c_ins_year1, c_ins_month2, c_ins_day2,
            c_ins_year2, c_com_month, c_com_day, c_com_year, c_tct_no,
            c_unit_no, c_bldg_name, c_street_no, c_street_name, c_village,
            c_district, c_zip_code, c_city, c_province, c_region, c_country,
            db_street_name, db_village, db_city, db_province,
            c_lot_no, c_block_no, c_app_request_remarks, c_app_branch_code,c_app_main_id,c_app_account_is_company,c_app_account_company_name,c_app_unacceptable_collateral;
    String c_vehicle_type, c_car_model, c_car_loan_purpose, c_car_mileage, c_car_series,
            c_car_color, c_car_plate_no, c_car_body_type, c_car_motor_no, c_car_cr_no,
            c_car_displacement, c_car_chassis_no, c_car_no_cylinders, c_car_cr_date_month, c_car_cr_date_day,
            c_car_cr_date_year;
    LinearLayout ll_contact,ll_lot;
    TableRow ll_tct, ll_address, ll_header_address;
    TableRow ll_vehicle_type, ll_car_model, ll_car_loan_purpose,
            ll_car_mileage, ll_car_series, ll_car_color, ll_car_plate_no,
            ll_car_body_type, ll_car_displacement, ll_car_motor_no,
            ll_car_chassis_no, ll_car_no_cylinders, ll_car_cr_no, ll_car_cr_date;
    List<String> ContactsArray_contact_person = new ArrayList<String>();
    List<String> ContactsArray_mobile_prefix = new ArrayList<String>();
    List<String> ContactsArray_mobile = new ArrayList<String>();
    List<String> ContactsArray_landline = new ArrayList<String>();

    List<String> LotArray_tct_no = new ArrayList<String>();
    List<String> LotArray_lot_no = new ArrayList<String>();
    List<String> LotArray_block_no = new ArrayList<String>();
    List<String> LotArray_survey_no = new ArrayList<String>();
    List<String> LotArray_area = new ArrayList<String>();
    List<String> LotArray_reg_date = new ArrayList<String>();
    List<String> LotArray_reg_owner = new ArrayList<String>();
    List<String> LotArray_deed = new ArrayList<String>();

    String c_contact_person, c_c_mobile_prefix, c_mobile, c_landline;
    //case center attachments
    ImageView btn_mysql_attachments;

    // accept yes or no
    Button btn_yes, btn_no;
    String call_type;
    String decision;
    String app_status;
    String dialog_status;
    String decline_remarks;
    DatabaseHandler db = new DatabaseHandler(this);
    DatabaseHandler2 db2 = new DatabaseHandler2(this);
    // for API
    String xml, xml2;
    DefaultHttpClient httpClient = new MyHttpClient(this);
    // dialog
    final Context context = this;
    // uploaded attachments
    LinearLayout container_attachments;
    String uploaded_attachment;
    String url_webby;
    UserFunctions userFunction = new UserFunctions();
    File uploaded_file;
    String[] commandArray = new String[]{"View Attachment in Browser",
            "Download Attachment"};
    Index_request_report irr = new Index_request_report();
    String dir = "gds_appraisal";
    File myDirectory = new File(Environment.getExternalStorageDirectory() + "/"
            + dir + "/");
    int responseCode;
    // Progress dialog type (0 - for Horizontal progress bar)
    public static final int progress_bar_type = 0;

    //for getting attachments
    // Creating JSON Parser object
    JSONParser2 jParser = new JSONParser2();
    // url to get all file list
    //private static String get_files_url = "http://dev.gdslinkasia.com:8080/get_all_uploaded_files.php";
    //private static String get_files_url = "https://gds.securitybank.com:8080/get_all_uploaded_files.php";
    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    int success;
    private static final String TAG_FILES = "files";
    private static final String TAG_FILE = "file";
    private static final String TAG_FILE_NAME = "file_name";
    String file_name = "";
    private static final String TAG_AUTH_KEY = "auth_key";
    String auth_key = "QweAsdZxc";
    // file JSONArray
    JSONArray files = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_jobs_info);
        Authenticator.setDefault(new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("gdsuser", "gdsuser01".toCharArray());
            }
        });
        st.resetDisconnectTimer();

        actionBar = getActionBar();
        actionBar.setIcon(R.drawable.gds_icon);

        container_attachments = (LinearLayout) findViewById(R.id.container_attachments);
        ll_contact = (LinearLayout) findViewById(R.id.container);
        ll_lot = (LinearLayout) findViewById(R.id.lot_container);
        ll_tct = (TableRow) findViewById(R.id.ll_tct);
        ll_address = (TableRow) findViewById(R.id.ll_address);
        ll_header_address = (TableRow) findViewById(R.id.ll_header_address);

        ll_vehicle_type = (TableRow) findViewById(R.id.ll_vehicle_type);
        ll_car_model = (TableRow) findViewById(R.id.ll_car_model);
        ll_car_loan_purpose = (TableRow) findViewById(R.id.ll_car_loan_purpose);
        ll_car_mileage = (TableRow) findViewById(R.id.ll_car_mileage);
        ll_car_series = (TableRow) findViewById(R.id.ll_car_series);
        ll_car_color = (TableRow) findViewById(R.id.ll_car_color);
        ll_car_plate_no = (TableRow) findViewById(R.id.ll_car_plate_no);
        ll_car_body_type = (TableRow) findViewById(R.id.ll_car_body_type);
        ll_car_displacement = (TableRow) findViewById(R.id.ll_car_displacement);
        ll_car_motor_no = (TableRow) findViewById(R.id.ll_car_motor_no);
        ll_car_chassis_no = (TableRow) findViewById(R.id.ll_car_chassis_no);
        ll_car_no_cylinders = (TableRow) findViewById(R.id.ll_car_no_cylinders);
        ll_car_cr_no = (TableRow) findViewById(R.id.ll_car_cr_no);
        ll_car_cr_date = (TableRow) findViewById(R.id.ll_car_cr_date);

        tv_requested_month = (TextView) findViewById(R.id.tv_requested_month);
        tv_requested_day = (TextView) findViewById(R.id.tv_requested_day);
        tv_requested_year = (TextView) findViewById(R.id.tv_requested_year);
        tv_classification = (TextView) findViewById(R.id.tv_classification);
        tv_account_name = (TextView) findViewById(R.id.tv_account_name);
        tv_company_name = (TextView) findViewById(R.id.tv_company_name);
        tv_requesting_party = (TextView) findViewById(R.id.tv_requesting_party);
        tv_requestor = (TextView) findViewById(R.id.tv_requestor);
        tv_control_no = (TextView) findViewById(R.id.tv_control_no);
        tv_appraisal_type = (TextView) findViewById(R.id.tv_appraisal_type);
        tv_nature_appraisal = (TextView) findViewById(R.id.tv_nature_appraisal);
        tv_kind_of_appraisal = (TextView) findViewById(R.id.tv_kind_of_appraisal);
        tv_ins_month1 = (TextView) findViewById(R.id.tv_ins_month1);
        tv_ins_day1 = (TextView) findViewById(R.id.tv_ins_day1);
        tv_ins_year1 = (TextView) findViewById(R.id.tv_ins_year1);
        tv_ins_month2 = (TextView) findViewById(R.id.tv_ins_month2);
        tv_ins_day2 = (TextView) findViewById(R.id.tv_ins_day2);
        tv_ins_year2 = (TextView) findViewById(R.id.tv_ins_year2);
        tv_com_month = (TextView) findViewById(R.id.tv_com_month);
        tv_com_day = (TextView) findViewById(R.id.tv_com_day);
        tv_com_year = (TextView) findViewById(R.id.tv_com_year);
        tv_tct_no = (TextView) findViewById(R.id.tv_tct_no);
        tv_unit_no = (TextView) findViewById(R.id.tv_unit_no);
        tv_bldg_name = (TextView) findViewById(R.id.tv_bldg_name);
        tv_street_no = (TextView) findViewById(R.id.tv_street_no);
        tv_lot_no = (TextView) findViewById(R.id.tv_lot_no);
        tv_block_no = (TextView) findViewById(R.id.tv_block_no);
        tv_street_name = (TextView) findViewById(R.id.tv_street_name);
        tv_village = (TextView) findViewById(R.id.tv_village);
        tv_district = (TextView) findViewById(R.id.tv_district);
        tv_zip_code = (TextView) findViewById(R.id.tv_zip_code);
        tv_city = (TextView) findViewById(R.id.tv_city);
        tv_province = (TextView) findViewById(R.id.tv_province);
        tv_region = (TextView) findViewById(R.id.tv_region);
        tv_country = (TextView) findViewById(R.id.tv_country);
        tv_address = (TextView) findViewById(R.id.tv_address);
        tv_tct_cct = (TextView) findViewById(R.id.tv_tct_cct);
        tv_app_request_remarks = (TextView) findViewById(R.id.tv_app_request_remarks);
        tv_app_branch_code = (TextView) findViewById(R.id.tv_app_branch_code);

        tv_vehicle_type = (TextView) findViewById(R.id.tv_vehicle_type);
        tv_car_model = (TextView) findViewById(R.id.tv_car_model);
        tv_car_loan_purpose = (TextView) findViewById(R.id.tv_car_loan_purpose);
        tv_car_mileage = (TextView) findViewById(R.id.tv_car_mileage);
        tv_car_series = (TextView) findViewById(R.id.tv_car_series);
        tv_car_color = (TextView) findViewById(R.id.tv_car_color);
        tv_car_plate_no = (TextView) findViewById(R.id.tv_car_plate_no);
        tv_car_body_type = (TextView) findViewById(R.id.tv_car_body_type);
        tv_car_displacement = (TextView) findViewById(R.id.tv_car_displacement);
        tv_car_motor_no = (TextView) findViewById(R.id.tv_car_motor_no);
        tv_car_chassis_no = (TextView) findViewById(R.id.tv_car_chassis_no);
        tv_car_no_cylinders = (TextView) findViewById(R.id.tv_car_no_cylinders);
        tv_car_cr_no = (TextView) findViewById(R.id.tv_car_cr_no);
        tv_car_cr_date = (TextView) findViewById(R.id.tv_car_cr_date);

        btn_yes = (Button) findViewById(R.id.btn_yes);
        btn_no = (Button) findViewById(R.id.btn_no);
        btn_yes.setOnClickListener(this);
        btn_no.setOnClickListener(this);
        gs = ((Global) getApplicationContext());
        record_id = gs.record_id;
        call_type = "fetch";

        btn_mysql_attachments = (ImageView) findViewById(R.id.btn_mysql_attachments);
        btn_mysql_attachments.setOnClickListener(this);
        new FetchData().execute();

        uid=app_main_id;
    }

    public void fetch_data() {
        //valordec14
        JSONObject json_query = new JSONObject();
        String query;
        try {
            json_query.put("system.record_id", record_id);
            query = json_query.toString();
            // API
            //for tls
            field.clear();
            field.add("auth_token");
            field.add("query");

            value.clear();
            value.add(gs.auth_token);
            value.add(query);

            if (Global.type.contentEquals("tls")) {
                jsonResponse = gds.makeHttpsRequest(gs.show_url, "POST", field, value);
                xml = jsonResponse.toString();
            } else {
                xml = gds.http_posting(field, value, gs.show_url, Request_Jobs_Info.this);
            }


            xml2 = xml;
            Log.e("response", xml);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.e("", e.toString());
        }

        try {
            JSONObject records = new JSONObject(xml);
            counter = records.getString("Counter");
            counter = String.valueOf(Integer.parseInt(counter) - 1);
            requested_month = records.getString("app_daterequested_month");
            requested_day = records.getString("app_daterequested_day");
            requested_year = records.getString("app_daterequested_year");

            account_fname = records.getString("app_account_first_name");
            account_mname = records.getString("app_account_middle_name");
            account_lname = records.getString("app_account_last_name");
            is_comp = records.getString("app_account_is_company");
            comp_name = records.getString("app_account_company_name");

            requesting_party = records.getString("app_requesting_party");
            requestor = records.getString("app_requestor");
            app_request_remarks = records.getString("app_request_remarks");
            app_main_id = records.getString("app_main_id");
            app_account_is_company = records.getString("app_account_is_company");
            app_account_company_name = records.getString("app_account_company_name");
            app_unacceptable_collateral = records.getString("app_unacceptable_collateral");


            JSONArray appraisal_request_array = records
                    .getJSONArray("appraisal_request");
            for (int x = 0; x < appraisal_request_array.length(); x++) {
                JSONObject appraisal_request_records = appraisal_request_array
                        .getJSONObject(x);

                control_no = appraisal_request_records
                        .getString("app_control_no");
                appraisal_type = appraisal_request_records
                        .getString("app_request_appraisal");
                nature_appraisal = appraisal_request_records
                        .getString("app_purpose_appraisal");
                kind_of_appraisal = appraisal_request_records
                        .getString("app_kind_of_appraisal");
                app_branch_code = appraisal_request_records.getString("app_branch_code");
//-------------------------------------//
//catch if CC table has this field name
                if (appraisal_request_array.getJSONObject(x).has("app_pref_inspection_date1_month")) {
                    ins_month1 = appraisal_request_records.getString("app_pref_inspection_date1_month");
                } else if (appraisal_request_array.getJSONObject(x).has("app_application_reference")) {
                    ins_month1 = appraisal_request_records.getString("app_application_reference");
                } else {
                    ins_month1 = "";
                }
                //ins_month1 = appraisal_request_records
                //		.getString("app_pref_inspection_date1_month");
//-------------------------------------//app_pref_inspection_date1_month --- app_application_reference
                ins_day1 = appraisal_request_records
                        .getString("app_pref_inspection_date1_day");
                ins_year1 = appraisal_request_records
                        .getString("app_pref_inspection_date1_year");
                ins_month2 = appraisal_request_records
                        .getString("app_pref_inspection_date2_month");
                ins_day2 = appraisal_request_records
                        .getString("app_pref_inspection_date2_day");
                ins_year2 = appraisal_request_records
                        .getString("app_pref_inspection_date2_year");
                com_month = appraisal_request_records
                        .getString("app_pref_completion_date_month");
                com_day = appraisal_request_records
                        .getString("app_pref_completion_date_day");
                com_year = appraisal_request_records
                        .getString("app_pref_completion_date_year");

                //catch if CC table has this field name
                //motor_vehicle special fields
                if (appraisal_request_array.getJSONObject(x).has("app_vehicle_type")) {
                    vehicle_type = appraisal_request_records.getString("app_vehicle_type");
                } else {
                    vehicle_type = "";
                }
                if (appraisal_request_array.getJSONObject(x).has("app_car_model")) {
                    car_model = appraisal_request_records.getString("app_car_model");
                } else {
                    car_model = "";
                }
                if (appraisal_request_array.getJSONObject(x).has("app_car_loan_purpose")) {
                    car_loan_purpose = appraisal_request_records.getString("app_car_loan_purpose");
                } else {
                    car_loan_purpose = "";
                }
                if (appraisal_request_array.getJSONObject(x).has("app_car_mileage")) {
                    car_mileage = appraisal_request_records.getString("app_car_mileage");
                } else {
                    car_mileage = "";
                }
                if (appraisal_request_array.getJSONObject(x).has("app_car_series")) {
                    car_series = appraisal_request_records.getString("app_car_series");
                } else {
                    car_series = "";
                }
                if (appraisal_request_array.getJSONObject(x).has("app_car_color")) {
                    car_color = appraisal_request_records.getString("app_car_color");
                } else {
                    car_color = "";
                }
                if (appraisal_request_array.getJSONObject(x).has("app_car_plate_no")) {
                    car_plate_no = appraisal_request_records.getString("app_car_plate_no");
                } else {
                    car_plate_no = "";
                }
                if (appraisal_request_array.getJSONObject(x).has("app_car_body_type")) {
                    car_body_type = appraisal_request_records.getString("app_car_body_type");
                } else {
                    car_body_type = "";
                }
                if (appraisal_request_array.getJSONObject(x).has("app_car_displacement")) {
                    car_displacement = appraisal_request_records.getString("app_car_displacement");
                } else {
                    car_displacement = "";
                }
                if (appraisal_request_array.getJSONObject(x).has("app_car_motor_no")) {
                    car_motor_no = appraisal_request_records.getString("app_car_motor_no");
                } else {
                    car_motor_no = "";
                }
                if (appraisal_request_array.getJSONObject(x).has("app_car_chassis_no")) {
                    car_chassis_no = appraisal_request_records.getString("app_car_chassis_no");
                } else {
                    car_chassis_no = "";
                }
                if (appraisal_request_array.getJSONObject(x).has("app_car_no_of_cylinders")) {
                    car_no_cylinders = appraisal_request_records.getString("app_car_no_of_cylinders");
                } else {
                    car_no_cylinders = "";
                }
                if (appraisal_request_array.getJSONObject(x).has("app_car_cr_no")) {
                    car_cr_no = appraisal_request_records.getString("app_car_cr_no");
                } else {
                    car_cr_no = "";
                }
                if (appraisal_request_array.getJSONObject(x).has("app_car_cr_date_month")) {
                    car_cr_date_month = appraisal_request_records.getString("app_car_cr_date_month");
                } else {
                    car_cr_date_month = "";
                }
                if (appraisal_request_array.getJSONObject(x).has("app_car_cr_date_day")) {
                    car_cr_date_day = appraisal_request_records.getString("app_car_cr_date_day");
                } else {
                    car_cr_date_day = "";
                }
                if (appraisal_request_array.getJSONObject(x).has("app_car_cr_date_year")) {
                    car_cr_date_year = appraisal_request_records.getString("app_car_cr_date_year");
                } else {
                    car_cr_date_year = "";
                }
                /*vehicle_type = appraisal_request_records.getString("app_vehicle_type");
                car_model = appraisal_request_records.getString("app_car_model");
				car_loan_purpose = appraisal_request_records.getString("app_car_loan_purpose");
				car_mileage = appraisal_request_records.getString("app_car_mileage");
				car_series = appraisal_request_records.getString("app_car_series");
				car_color = appraisal_request_records.getString("app_car_color");
				car_plate_no = appraisal_request_records.getString("app_car_plate_no");
				car_body_type = appraisal_request_records.getString("app_car_body_type");
				car_displacement = appraisal_request_records.getString("app_car_displacement");
				car_motor_no = appraisal_request_records.getString("app_car_motor_no");
				car_chassis_no = appraisal_request_records.getString("app_car_chassis_no");
				car_cr_no = appraisal_request_records.getString("app_car_cr_no");
				car_cr_date_month = appraisal_request_records.getString("app_car_cr_date_month");
				car_cr_date_day = appraisal_request_records.getString("app_car_cr_date_day");
				car_cr_date_year = appraisal_request_records.getString("app_car_cr_date_year");*/

                // collateral address
                JSONArray app_collateral_address_array = appraisal_request_records
                        .getJSONArray("app_collateral_address");
                for (int i = 0; i < app_collateral_address_array.length(); i++) {
                    JSONObject app_collateral_address_records = app_collateral_address_array
                            .getJSONObject(i);
                    tct_no = app_collateral_address_records
                            .getString("app_tct_no");
                    unit_no = app_collateral_address_records
                            .getString("app_unit_no");
                    bldg_name = app_collateral_address_records
                            .getString("app_bldg");
                    street_no = app_collateral_address_records
                            .getString("app_street_no");
                    lot_no = app_collateral_address_records.getString("app_lot_no");
                    block_no = app_collateral_address_records.getString("app_block_no");
                    street_name = app_collateral_address_records
                            .getString("app_street_name");
                    village = app_collateral_address_records
                            .getString("app_village");
                    district = app_collateral_address_records
                            .getString("app_district");
                    zip_code = app_collateral_address_records
                            .getString("app_zip");
                    city = app_collateral_address_records.getString("app_city");
                    province = app_collateral_address_records
                            .getString("app_province");
                    region = app_collateral_address_records
                            .getString("app_region");
                    country = app_collateral_address_records
                            .getString("app_country");

                }
                // contact person
                JSONArray app_contact_person_array = appraisal_request_records
                        .getJSONArray("app_contact_person");
                for (int i = 0; i < app_contact_person_array.length(); i++) {
                    JSONObject app_contact_person_records = app_contact_person_array
                            .getJSONObject(i);
                    ContactsArray_contact_person.add(app_contact_person_records
                            .getString("app_contact_person_name"));
                    ContactsArray_mobile_prefix.add(app_contact_person_records
                            .getString("app_mobile_prefix"));
                    ContactsArray_mobile.add(app_contact_person_records
                            .getString("app_mobile_no"));
                    ContactsArray_landline.add(app_contact_person_records
                            .getString("app_telephone_no"));
                }

                // lot_details
                JSONArray app_lot_details_array = appraisal_request_records
                        .getJSONArray("landimp_title_details");
                for (int i = 0; i < app_lot_details_array.length(); i++) {
                    JSONObject app_lot_details_records = app_lot_details_array
                            .getJSONObject(i);

                    LotArray_tct_no.add(app_lot_details_records.getString("landimp_title_details_tct"));
                    LotArray_lot_no.add(app_lot_details_records.getString("landimp_title_details_lot_no"));
                    LotArray_block_no.add(app_lot_details_records.getString("landimp_title_details_block"));
                    LotArray_survey_no.add(app_lot_details_records.getString("landimp_title_details_survey"));
                    LotArray_area.add(app_lot_details_records.getString("landimp_title_details_area"));
                    LotArray_reg_date.add(app_lot_details_records.getString("landimp_title_details_date_registry"));
                    LotArray_reg_owner.add(app_lot_details_records.getString("landimp_title_details_reg_owner"));
                    LotArray_deed.add(app_lot_details_records.getString("landimp_title_details_reg_deeds"));

                }
            }
            //set uid for attachments class
            uid = app_main_id;

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void fill() {
        c_requested_month = string_checker(requested_month);
        c_requested_day = string_checker(requested_day);
        c_requested_year = string_checker(requested_year);
        c_account_fname = string_checker(account_fname);
        c_account_mname = string_checker(account_mname);
        c_account_lname = string_checker(account_lname);
        c_is_company = gds.nullCheck3(is_comp);
        c_company_name = gds.nullCheck3(comp_name);
        Log.e("c_is_company",c_is_company);
        Log.e("c_company_name",c_company_name);
        c_requesting_party = string_checker(requesting_party);
        c_requestor = string_checker(requestor);
        c_control_no = string_checker(control_no);
        c_appraisal_type = string_checker(appraisal_type);
        output_appraisal_type = string_checker(appraisal_type);
        c_nature_appraisal = string_checker(nature_appraisal);
        c_kind_of_appraisal = string_checker(kind_of_appraisal);
        c_ins_month1 = string_checker(ins_month1);
        c_ins_day1 = string_checker(ins_day1);
        c_ins_year1 = string_checker(ins_year1);
        c_ins_month2 = string_checker(ins_month2);
        c_ins_day2 = string_checker(ins_day2);
        c_ins_year2 = string_checker(ins_year2);
        c_com_month = string_checker(com_month);
        c_com_day = string_checker(com_day);
        c_com_year = string_checker(com_year);
        c_tct_no = string_checker(tct_no);
        c_unit_no = string_checker(unit_no);
        c_bldg_name = string_checker(bldg_name);
        c_street_no = string_checker(street_no);
        c_lot_no = string_checker(lot_no);
        c_block_no = string_checker(block_no);
        c_app_request_remarks = string_checker(app_request_remarks);
        c_app_main_id= string_checker(app_main_id);
        c_app_account_is_company= gds.nullCheck3(app_account_is_company);
        c_app_account_company_name= string_checker(app_account_company_name);
        c_app_unacceptable_collateral= string_checker(app_unacceptable_collateral);
        c_app_branch_code = string_checker(app_branch_code);



        if (!string_checker(street_name).equals("")) {
            c_street_name = string_checker(street_name + ", ");
        } else {
            c_street_name = string_checker(street_name);
        }
        if (!string_checker(village).equals("")) {
            c_village = string_checker(village + ", ");
        } else {
            c_village = string_checker(village);
        }
        if (!string_checker(city).equals("")) {
            c_city = string_checker(city + ", ");
        } else {
            c_city = string_checker(city);
        }
        if (!string_checker(province).equals("")) {
            c_province = string_checker(province + ", ");
        } else {
            c_province = string_checker(province);
        }
        db_street_name = string_checker(street_name);
        db_village = string_checker(village);
        db_city = string_checker(city);
        db_province = string_checker(province);
        c_district = string_checker(district);
        c_zip_code = string_checker(zip_code);
        c_region = string_checker(region);
        c_country = string_checker(country);


        c_vehicle_type = string_checker(vehicle_type);
        c_car_model = string_checker(car_model);
        c_car_loan_purpose = string_checker(car_loan_purpose);
        c_car_mileage = string_checker(car_mileage);
        c_car_series = string_checker(car_series);
        c_car_color = string_checker(car_color);
        c_car_plate_no = string_checker(car_plate_no);
        c_car_body_type = string_checker(car_body_type);
        c_car_displacement = string_checker(car_displacement);
        c_car_motor_no = string_checker(car_motor_no);
        c_car_chassis_no = string_checker(car_chassis_no);
        c_car_no_cylinders = string_checker(car_no_cylinders);
        c_car_cr_no = string_checker(car_cr_no);
        c_car_cr_date_month = string_checker(car_cr_date_month);
        c_car_cr_date_day = string_checker(car_cr_date_day);
        c_car_cr_date_year = string_checker(car_cr_date_year);

        tv_vehicle_type.setText(c_vehicle_type);
        tv_car_model.setText(c_car_model);
        tv_car_loan_purpose.setText(c_car_loan_purpose);
        tv_car_mileage.setText(c_car_mileage);
        tv_car_series.setText(c_car_series);
        tv_car_color.setText(c_car_color);
        tv_car_plate_no.setText(c_car_plate_no);
        tv_car_body_type.setText(c_car_body_type);
        tv_car_displacement.setText(c_car_displacement);
        tv_car_motor_no.setText(c_car_motor_no);
        tv_car_chassis_no.setText(c_car_chassis_no);
        tv_car_no_cylinders.setText(c_car_no_cylinders);
        tv_car_cr_no.setText(c_car_cr_no);
        tv_car_cr_date.setText(c_car_cr_date_month + "/" + c_car_cr_date_day + "/" + c_car_cr_date_year);


        tv_requested_month.setText(c_requested_month + "/");
        tv_requested_day.setText(c_requested_day + "/");
        tv_requested_year.setText(c_requested_year);

        tv_account_name.setText(c_account_fname + " " + c_account_mname + " "
                + c_account_lname);
        tv_company_name.setText(c_company_name);
        if(c_is_company.contentEquals("true")){
            actionBar.setTitle(c_company_name);

        }else{
            actionBar.setTitle(gds.nullCheck3(account_fname) + " " + gds.nullCheck3(account_mname) + " " + gds.nullCheck3(account_lname));
        }
        actionBar.setSubtitle("Job Acceptance");

        tv_requesting_party.setText(c_requesting_party);//?
        tv_requestor.setText(c_requestor);//?
        tv_control_no.setText(c_control_no);
        for (int x = 0; x < gs.appraisal_type_value.length; x++) {
            if (c_appraisal_type.equals(gs.appraisal_type_value[x])) {
                output_appraisal_type = gs.appraisal_output[x];
            }
        }
        tv_appraisal_type.setText(output_appraisal_type);
        if (c_appraisal_type.equals("vacant_lot")
                || c_appraisal_type.equals("land_improvements")
                || c_appraisal_type.equals("construction_ebm")) {
            tv_tct_cct.setText("TCT No.");
        } else if (c_appraisal_type.equals("townhouse_condo")) {
            tv_tct_cct.setText("CCT No.");
        }
        tv_nature_appraisal.setText(c_nature_appraisal);
        tv_kind_of_appraisal.setText(c_kind_of_appraisal);
        tv_ins_month1.setText(c_ins_month1 + "/");
        tv_ins_day1.setText(c_ins_day1 + "/");
        tv_ins_year1.setText(c_ins_year1);
        tv_ins_month2.setText(c_ins_month2 + "/");
        tv_ins_day2.setText(c_ins_day2 + "/");
        tv_ins_year2.setText(c_ins_year2);
        tv_com_month.setText(c_com_month + "/");
        tv_com_day.setText(c_com_day + "/");
        tv_com_year.setText(c_com_year);
        //tv_vehicle_type.setText(c_vehicle_type);
        //tv_car_model.setText(c_car_model);

        tv_tct_no.setText(c_tct_no);
        tv_unit_no.setText(c_unit_no);
        tv_bldg_name.setText(c_bldg_name);
        tv_street_no.setText(c_street_no);
        tv_lot_no.setText(c_lot_no);
        tv_block_no.setText(c_block_no);
        tv_street_name.setText(c_street_name);
        tv_village.setText(c_village);
        tv_district.setText(c_district);
        tv_zip_code.setText(c_zip_code);
        tv_city.setText(c_city);
        tv_province.setText(c_province);
        tv_region.setText(c_region);
        tv_country.setText(c_country);

        tv_app_request_remarks.setText(c_app_request_remarks);
        tv_app_branch_code.setText(c_app_branch_code);
        if (c_appraisal_type.equals("motor_vehicle")) {
            ll_tct.setVisibility(View.GONE);
            ll_address.setVisibility(View.GONE);
            ll_header_address.setVisibility(View.GONE);

            ll_vehicle_type.setVisibility(View.VISIBLE);
            ll_car_model.setVisibility(View.VISIBLE);
            ll_car_loan_purpose.setVisibility(View.VISIBLE);
            ll_car_mileage.setVisibility(View.VISIBLE);
            ll_car_series.setVisibility(View.VISIBLE);
            ll_car_color.setVisibility(View.VISIBLE);
            ll_car_plate_no.setVisibility(View.VISIBLE);
            ll_car_body_type.setVisibility(View.VISIBLE);
            ll_car_displacement.setVisibility(View.VISIBLE);
            ll_car_motor_no.setVisibility(View.VISIBLE);
            ll_car_chassis_no.setVisibility(View.VISIBLE);
            ll_car_no_cylinders.setVisibility(View.VISIBLE);
            ll_car_cr_no.setVisibility(View.VISIBLE);
            ll_car_cr_date.setVisibility(View.VISIBLE);
        }
        tv_address.setText(c_unit_no + " " + c_bldg_name + " " + c_lot_no + " " + c_block_no
                + " " + c_street_name + c_village + c_district + " " + c_city
                + c_region + " " + c_province + c_country + " " + c_zip_code);
        // contact
        for (int x = 0; x < ContactsArray_contact_person.size(); x++) {
            LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View addView = layoutInflater.inflate(
                    R.layout.request_jobs_info_contact_layout, null);

            final TextView tv_contact_person = (TextView) addView
                    .findViewById(R.id.tv_contact_person);
            final TextView tv_contact_prefix = (TextView) addView
                    .findViewById(R.id.tv_contact_prefix);
            final TextView tv_contact_mobile = (TextView) addView
                    .findViewById(R.id.tv_contact_mobile);
            final TextView tv_contact_landline = (TextView) addView
                    .findViewById(R.id.tv_contact_landline);
            c_contact_person = string_checker(ContactsArray_contact_person
                    .get(x));
            c_c_mobile_prefix = string_checker(ContactsArray_mobile_prefix
                    .get(x));
            c_mobile = string_checker(ContactsArray_mobile.get(x));
            c_landline = string_checker(ContactsArray_landline.get(x));

            tv_contact_person.setText(c_contact_person);
            tv_contact_prefix.setText(c_c_mobile_prefix);
            tv_contact_mobile.setText(c_mobile);
            tv_contact_landline.setText(c_landline);
            addView.setEnabled(false);
            addView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    db.addReport_Accepted_Jobs_Contacts(new Report_Accepted_Jobs_Contacts(
                            record_id, tv_contact_person.getText().toString(),
                            tv_contact_prefix.getText().toString(),
                            tv_contact_mobile.getText().toString(),
                            tv_contact_landline.getText().toString()));
                }
            });
            ll_contact.addView(addView);
        }
        // lot details
        for (int x = 0; x < LotArray_tct_no.size(); x++) {
            LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View addView = layoutInflater.inflate(
                    R.layout.request_job_info_lot_details_layout, null);

            final TextView tv_tct_no = (TextView) addView.findViewById(R.id.tv_tct_no);
            final TextView tv_lot_no = (TextView) addView.findViewById(R.id.tv_lot_no);
            final TextView tv_block_no = (TextView) addView.findViewById(R.id.tv_block_no);
            final TextView tv_survey_no = (TextView) addView.findViewById(R.id.tv_survey_no);
            final TextView tv_area = (TextView) addView.findViewById(R.id.tv_area);
            final TextView tv_reg_date = (TextView) addView.findViewById(R.id.tv_reg_date);
            final TextView tv_reg_owner = (TextView) addView.findViewById(R.id.tv_reg_owner);
            final TextView tv_deed = (TextView) addView.findViewById(R.id.tv_deed);

            lot_tct_no = string_checker(LotArray_tct_no.get(x));
            lot_lot_no = string_checker(LotArray_lot_no.get(x));
            lot_block_no = string_checker(LotArray_block_no.get(x));
            lot_survey_no = string_checker(LotArray_survey_no.get(x));
            lot_area = string_checker(LotArray_area.get(x));
            lot_registry_date = string_checker(LotArray_reg_date.get(x));
            lot_registered_owner = string_checker(LotArray_reg_owner.get(x));
            lot_deeds = string_checker(LotArray_deed.get(x));

            if(lot_tct_no.contentEquals("")){
                tv_tct_no.setVisibility(View.GONE);
                tv_lot_no.setVisibility(View.GONE);
                tv_block_no.setVisibility(View.GONE);
                tv_survey_no.setVisibility(View.GONE);
                tv_area.setVisibility(View.GONE);
                tv_reg_date.setVisibility(View.GONE);
                tv_reg_owner.setVisibility(View.GONE);
                tv_deed.setVisibility(View.GONE);
            }else{
                tv_tct_no.setVisibility(View.VISIBLE);
                tv_lot_no.setVisibility(View.VISIBLE);
                tv_block_no.setVisibility(View.VISIBLE);
                tv_survey_no.setVisibility(View.VISIBLE);
                tv_area.setVisibility(View.VISIBLE);
                tv_reg_date.setVisibility(View.VISIBLE);
                tv_reg_owner.setVisibility(View.VISIBLE);
                tv_deed.setVisibility(View.VISIBLE);
            }
            tv_tct_no.setText(lot_tct_no);
            tv_lot_no.setText(lot_lot_no);
            tv_block_no.setText(lot_block_no);
            tv_survey_no.setText(lot_survey_no);
            tv_area.setText(lot_area);
            tv_reg_date.setText(lot_registry_date);
            tv_reg_owner.setText(lot_registered_owner);
            tv_deed.setText(lot_deeds);

            addView.setEnabled(false);
            addView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (appraisal_type.contentEquals("vacant_lot")) {
                        if(!lot_tct_no.contentEquals("")){
                        db2.addVacant_Lot_Lot_Details(new Vacant_Lot_API_Lot_Details(
                                record_id,
                                tv_tct_no.getText().toString(),
                                tv_lot_no.getText().toString(),
                                tv_block_no.getText().toString(),
                                tv_survey_no.getText().toString(),
                                tv_area.getText().toString(),
                                tv_reg_date.getText().toString(),
                                tv_reg_owner.getText().toString(),
                                tv_deed.getText().toString()));

                        db2.addVacant_Lot_Lot_Valuation_Details(new Vacant_Lot_API_Lot_Valuation_Details(
                                record_id,
                                tv_tct_no.getText().toString(),
                                tv_lot_no.getText().toString(),
                                tv_block_no.getText().toString(),
                                tv_area.getText().toString(),
                                "0",
                                "0",
                                "0",
                                "0"));
                    }}
                    if (appraisal_type.contentEquals("land_improvements")) {
                        if(!lot_tct_no.contentEquals("")){
                        db2.addLand_Improvements_Lot_Details(new Land_Improvements_API_Lot_Details(
                                record_id,
                                tv_tct_no.getText().toString(),
                                tv_lot_no.getText().toString(),
                                tv_block_no.getText().toString(),
                                tv_survey_no.getText().toString(),
                                tv_area.getText().toString(),
                                tv_reg_date.getText().toString(),
                                tv_reg_owner.getText().toString(),
                                tv_deed.getText().toString()));

                        db2.addLand_Improvements_Lot_Valuation_Details(new Land_Improvements_API_Lot_Valuation_Details(
                                record_id,
                                tv_tct_no.getText().toString(),
                                tv_lot_no.getText().toString(),
                                tv_block_no.getText().toString(),
                                tv_area.getText().toString(),
                                "0",
                                "0",
                                "0",
                                "0"));
                    }}
                    if (appraisal_type.contentEquals("townhouse")) {
                        if(!lot_tct_no.contentEquals("")){

                            db2.addTownhouse_Lot_Details(new Townhouse_API_Lot_Details(
                                    record_id,
                                    tv_tct_no.getText().toString(),
                                    tv_lot_no.getText().toString(),
                                    tv_block_no.getText().toString(),
                                    tv_survey_no.getText().toString(),
                                    gds.replaceFormat(tv_area.getText().toString()),
                                    tv_reg_date.getText().toString(),
                                    tv_reg_owner.getText().toString(),
                                    tv_deed.getText().toString()));
                            //sync with valuation (if 3 records added, then 3 records should also be created to valuation
                            db2.addTownhouse_Lot_Valuation_Details(new Townhouse_API_Lot_Valuation_Details(
                                    record_id,
                                    tv_tct_no.getText().toString(),
                                    tv_lot_no.getText().toString(),
                                    tv_block_no.getText().toString(),
                                    gds.replaceFormat(tv_area.getText().toString()),
                                    "0",//report_floor_area.getText().toString(),
                                    "0",//report_unit_value.getText().toString(),
                                    "0"));//report_total_land_val.getText().toString()));
                            //sync with desc of imp
                            db2.addTownhouse_Imp_Details(new Townhouse_API_Imp_Details(
                                    record_id,
                                    "",//report_num_of_floors.getText().toString(),
                                    "",//report_desc_of_bldg.getText().toString(),
                                    "",//report_erected_on_lot.getText().toString(),
                                    "",//report_fa.getText().toString(),
                                    "",//report_fa_per_td.getText().toString(),
                                    "",//report_actual_utilization.getText().toString(),
                                    "",//report_declaration_as_to_usage.getText().toString(),
                                    "",//report_declared_owner.getText().toString(),
                                    "Yes",//spinner_socialized_housing.getSelectedItem().toString(),
                                    "New",//spinner_type_of_property.getSelectedItem().toString(),
                                    "",//report_foundation.getText().toString(),
                                    "",//report_columns.getText().toString(),
                                    "",//report_beams.getText().toString(),
                                    "",//report_exterior_walls.getText().toString(),
                                    "",//report_interior_walls.getText().toString(),
                                    "",//report_flooring.getText().toString(),
                                    "",//report_doors.getText().toString(),
                                    "",//report_windows.getText().toString(),
                                    "",//report_ceiling.getText().toString(),
                                    "",//report_roofing.getText().toString(),
                                    "",//report_trusses.getText().toString(),
                                    "",//report_economic_life.getText().toString(),
                                    "",//report_effective_age.getText().toString(),
                                    "",//report_remaining_eco_life.getText().toString(),
                                    "",//report_occupants.getText().toString(),
                                    "",//report_owned_or_leased.getText().toString(),
                                    "0",//report_floor_area.getText().toString(),
                                    "Actual Measurement",//spinner_confirmed_thru.getSelectedItem().toString(),
                                    "Well Maintained",//spinner_observed_condition.getSelectedItem().toString(),
                                    "Single Detached",//spinner_ownership_of_property.getSelectedItem().toString()));
                                    "",""));

                        }}
                }
            });
            ll_lot.addView(addView);
        }
    }


    private class FetchData extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pDialog = new ProgressDialog(Request_Jobs_Info.this);
            if (call_type.equals("fetch")) {
                pDialog.setMessage("Fetching data..");
            } else if (call_type.equals("update")) {
                pDialog.setMessage("Updating data..");
            }

            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            // TODO Auto-generated method stub

            if (call_type.equals("fetch")) {
                fetch_data();
            } else if (call_type.equals("update")) {
                update_job();
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            pDialog.dismiss();
            if (call_type.equals("fetch")) {
                fill();
                uploaded_attachments();
            } else if (call_type.equals("update")) {
                if (decision.equals("yes")) {
                    save_db();

                    //go back to main tab class
                    Intent i = new Intent(getApplicationContext(), Index_request_reports_jobs.class);
                    // set the new task and clear flags
//					i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
                finish();
            }

        }
    }

    public void update_dialog() {
        // custom dialog
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.yes_no_dialog);

        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_question);
        text.setText("Are you sure you want to " + dialog_status
                + " this Job ?");
        Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

        // if button is clicked, close the custom dialog
        btn_yes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new FetchData().execute();
                dialog.dismiss();
            }
        });
        btn_no.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void update_job() {
        String query, data;
        if (decision.equals("yes")) {
            app_status = "accepted_job";
        } else if (decision.equals("no")) {
            app_status = "decline_job";
        }
        JSONObject json_query = new JSONObject();
        JSONObject data_query = new JSONObject();
        JSONObject data_status = new JSONObject();
        try {
            json_query.put("system.record_id", record_id);
            data_status.put("app_declined_reason", decline_remarks);
            data_status.put("application_status", app_status);
            data_query.put("record", data_status);

            query = json_query.toString();
            data = data_query.toString();

            // API
            //for tls
            JSONObject jsonResponse = new JSONObject();
            ArrayList<String> field = new ArrayList<>();
            ArrayList<String> value = new ArrayList<>();
            field.clear();
            field.add("auth_token");
            field.add("query");
            field.add("data");

            value.clear();
            value.add(gs.auth_token);
            value.add(query);
            value.add(data);


            if (Global.type.contentEquals("tls")) {
                jsonResponse = gds.makeHttpsRequest(gs.update_url, "POST", field, value);
                xml = jsonResponse.toString();
            } else {
                xml = gds.http_posting(field, value, gs.update_url, Request_Jobs_Info.this);
            }

            Log.e("response", xml);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.e("", e.toString());
        }

    }

    public String string_checker(String old_string) {
        String new_string = old_string;
        if (old_string.equals("null")) {
            new_string = "";
        } else if (old_string.equals("0")) {
            new_string = "";
        }

        return new_string;
    }

    public void save_db() {

        db.addReport_Accepted_Jobs(new Report_Accepted_Jobs(record_id,
                c_requested_month, c_requested_day, c_requested_year,
                c_classification, c_account_fname, c_account_mname,
                c_account_lname, c_requesting_party, c_requestor, c_control_no,
                c_appraisal_type, c_nature_appraisal, c_ins_month1, c_ins_day1,
                c_ins_year1, c_ins_month2, c_ins_day2, c_ins_year2,
                c_com_month, c_com_day, c_com_year, c_tct_no, c_unit_no,
                c_bldg_name, c_street_no, db_street_name, db_village,
                c_district, c_zip_code, db_city, db_province, c_region,
                c_country, counter, c_vehicle_type, c_car_model,
                c_lot_no, c_block_no,
                c_car_loan_purpose, c_car_mileage, c_car_series, c_car_color,
                c_car_plate_no, c_car_body_type, c_car_displacement, c_car_motor_no, c_car_chassis_no, c_car_no_cylinders,
                c_car_cr_no, c_car_cr_date_month, c_car_cr_date_day, c_car_cr_date_year, "", c_kind_of_appraisal
                , c_app_request_remarks, c_app_branch_code,c_app_main_id));

        field.clear();
        field.add("app_account_is_company");
        field.add("app_account_company_name");
        field.add("app_unacceptable_collateral");
        field.add("application_status");
        value.clear();
        value.add(c_app_account_is_company);
        value.add(c_app_account_company_name);
        value.add(c_app_unacceptable_collateral);
        value.add("");
        db2.updateRecord(value,field,"record_id = ?",new String[] { record_id },"tbl_report_accepted_jobs");

        db.addReport_filename(new Report_filename(record_id, "", "", "", "", ""));

        // contact
        int childcount = ll_contact.getChildCount();
        for (int i = 0; i < childcount; i++) {
            View view = ll_contact.getChildAt(i);
            view.performClick();
        }
        // lot details
        int childcount2 = ll_lot.getChildCount();
        for (int i = 0; i < childcount2; i++) {
            View view = ll_lot.getChildAt(i);
            view.performClick();
        }
        
        
        if (c_appraisal_type.equals("townhouse_condo")) {
            // report Townhouse_condo
            
            // report Townhouse_condo
            /*db.addTownhouse_Condo(new Townhouse_Condo_API(record_id, "", "",
					"", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
					"", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
					"", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
					"", "", "", "", "", "", "", "", ""));*/
            db.addCondo(new Condo_API(record_id,
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", ""));//168 including record_id
        } else if (c_appraisal_type.equals("motor_vehicle")
                || (c_appraisal_type.equals("machinery_equipment"))) {
            db.addMotor_Vehicle(new Motor_Vehicle_API(record_id,
                    "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "","","","","","","","","","","","","","","","",
                    "", "", "", "", "", "", "", ""));//53 inc rec_id
        } else if (c_appraisal_type.equals("vacant_lot")) {
            db.addVacant_Lot(new Vacant_Lot_API(record_id,
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""));
        } else if (c_appraisal_type.equals("land_improvements")) {



            DatabaseHandler_New db_new = new DatabaseHandler_New(this);
            //add record_id and blank fields
            ArrayList<String> mainfields = db_new.getAllColumn(Tables.land_improvements.table_name);
            mainfields.remove(0);//remove id to start at record_id

            ArrayList<String> mainvalues = new ArrayList<>();
            mainvalues.clear();
            mainvalues.add(record_id);
//            mainvalues.add(c_appraisal_type);

            for (int x = 2; x <= mainfields.size(); x++){//start at 3 to skip id, record_id, appraisal_type
                mainvalues.add("");
            }

            db_new.addRecord(mainvalues, mainfields, Tables.land_improvements.table_name);



        } else if (c_appraisal_type.equals("construction_ebm")) {
            db.addConstruction_Ebm(new Construction_Ebm_API(record_id,
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""));//119
        } else if (c_appraisal_type.equals("ppcr")) {
            addPpcr();
        } else if (c_appraisal_type.equals("townhouse")) {
            db2.addRecord(gds.emptyFields(to.townhouse_db().size() - 2,record_id), to.townhouse_db(),"townhouse");
        } else if (c_appraisal_type.equals("waf")) {

            DatabaseHandler_New db_new = new DatabaseHandler_New(this);
            //add record_id and blank fields
            ArrayList<String> mainfields = db_new.getAllColumn(Tables.waf.table_name);
            mainfields.remove(0);//remove id to start at record_id

            ArrayList<String> mainvalues = new ArrayList<>();
            mainvalues.clear();
            mainvalues.add(record_id);
            for (int x = 2; x <= mainfields.size(); x++){//start at 2 to skip id, record_id, appraisal_type
                mainvalues.add("");
            }

            db_new.addRecord(mainvalues, mainfields, Tables.waf.table_name);

        } else if (c_appraisal_type.equals("spv_land_improvements")) {

            DatabaseHandler_New db_new = new DatabaseHandler_New(this);
            //add record_id and blank fields
            ArrayList<String> mainfields = db_new.getAllColumn(Tables.spv.table_name);
            mainfields.remove(0);//remove id to start at record_id

            ArrayList<String> mainvalues = new ArrayList<>();
            mainvalues.clear();
            mainvalues.add(record_id);
            for (int x = 2; x <= mainfields.size(); x++){//start at 2 to skip id, record_id, appraisal_type
                mainvalues.add("");
            }

            db_new.addRecord(mainvalues, mainfields, Tables.spv.table_name);

        }
        new LoadAllFiles().execute();
        finish();
        Toast.makeText(getApplicationContext(), "Request Job Accepted",
                Toast.LENGTH_SHORT).show();

    }

    public void remarks_decline_dialog() {
        // custom dialog
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.input_dialog);

        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_question);
        final EditText txt_remarks = (EditText) dialog
                .findViewById(R.id.txt_remarks);
        text.setText("Decline Remarks");
        Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

        // if button is clicked, close the custom dialog
        btn_yes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                decline_remarks = txt_remarks.getText().toString();
                update_dialog();
            }
        });
        btn_no.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void uploaded_attachments() {
        // set value
        List<Required_Attachments> required_attachments = db
                .getAllRequired_Attachments_byid(String.valueOf(appraisal_type));
        if (!required_attachments.isEmpty()) {
            for (final Required_Attachments ra : required_attachments) {
                LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                final View addView = layoutInflater.inflate(
                        R.layout.report_uploaded_attachments_dynamic, null);
                final TextView tv_attachment = (TextView) addView
                        .findViewById(R.id.tv_attachment);
                final ImageView btn_view = (ImageView) addView
                        .findViewById(R.id.btn_view);
                tv_attachment.setText(ra.getattachment());
                btn_view.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        uploaded_attachment = uid + "_"
                                + ra.getattachment() + "_" + counter + ".pdf";
                        // directory
                        uploaded_file = new File(myDirectory,
                                uploaded_attachment);
                        uploaded_attachment = uploaded_attachment.replaceAll(
                                " ", "%20");
                        url_webby = gs.pdf_loc_url + uploaded_attachment;
                        view_pdf(uploaded_file);
                    }
                });
                container_attachments.addView(addView);

            }

        }

    }

    public void view_pdf(File file) {
        if (file.exists()) {
            Uri path = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(getApplicationContext(),
                        "No Application Available to View PDF",
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            NetworkUtil.getConnectivityStatusString(this);
            if (!NetworkUtil.status.equals("Network not available")) {
                new Attachment_validation().execute();
            } else {
                Toast.makeText(getApplicationContext(),
                        "Network not available", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private class Attachment_validation extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pDialog = new ProgressDialog(Request_Jobs_Info.this);
            pDialog.setMessage("Checking attachment..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            // TODO Auto-generated method stub

            try {
                URL obj = new URL(url_webby);
                HttpsURLConnection con;
                HttpURLConnection con2;
                if (Global.type.contentEquals("tls")) {
                    con = tlscon.setUpHttpsConnection("" + obj);
                    con.setRequestMethod("GET");
                    con.setRequestProperty("User-Agent", "Mozilla/5.0");
                    responseCode = con.getResponseCode();
                } else {
                    con2 = (HttpURLConnection) obj.openConnection();
                    con2.setRequestMethod("GET");
                    con2.setRequestProperty("User-Agent", "Mozilla/5.0");
                    responseCode = con2.getResponseCode();
                }
                Log.e("", String.valueOf(responseCode));
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            Log.e("", url_webby);
            if (responseCode == 200) {
                view_download();
            } else if (responseCode == 404) {
                Toast.makeText(getApplicationContext(),
                        "Attachment doesn't exist", Toast.LENGTH_SHORT).show();
            }
            pDialog.dismiss();

        }
    }

    private void view_download() {
        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Actions :");
        builder.setItems(commandArray, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int index) {
                if (index == 0) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
                            .parse(url_webby));
                    startActivity(browserIntent);
                    dialog.dismiss();
                } else if (index == 1) {
                    new DownloadFileFromURL().execute(url_webby);
                    dialog.dismiss();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * Showing Dialog
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type:
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.setCanceledOnTouchOutside(false);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    /**
     * Background Async Task to download file
     */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                // Output stream to write file
                OutputStream output = new FileOutputStream(
                        uploaded_file.toString());

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            removeDialog(progress_bar_type);
            Toast.makeText(getApplicationContext(), "Attachment Downloaded",
                    Toast.LENGTH_LONG).show();
            view_pdf(uploaded_file);

        }

    }

    /**
     * Background Async Task to Load all product by making HTTP Request
     */
    class LoadAllFiles extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Request_Jobs_Info.this);
            pDialog.setMessage("Loading files. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting All files from url
         */
        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            //for tls
            field.clear();
            field.add(TAG_UID);
            field.add(TAG_AUTH_KEY);

            value.clear();
            value.add(uid);
            value.add(auth_key);

            if (Global.type.contentEquals("tls")) {
                jsonResponse = gds.makeHttpsRequest(gs.get_files_url, "POST", field, value);
            } else {
                params.add(new BasicNameValuePair(TAG_UID, uid));
                params.add(new BasicNameValuePair(TAG_AUTH_KEY, auth_key));
                // getting JSON string from URL
                jsonResponse = gds.makeHttpRequest(gs.get_files_url, "POST", params);
            }

            //change json to jsonResponse
            // Check your log cat for JSON reponse
            Log.d("All Files: ", "" + jsonResponse.toString());

            try {
                // Checking for SUCCESS TAG
                success = jsonResponse.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // products found
                    // Getting Array of Products
                    files = jsonResponse.getJSONArray(TAG_FILES);

                    // looping through All Products
                    for (int i = 0; i < files.length(); i++) {
                        JSONObject c = files.getJSONObject(i);

                        // Storing each json item in variable
                        String uid = c.getString(TAG_UID);
                        String file = c.getString(TAG_FILE);
                        String file_name = c.getString(TAG_FILE_NAME);

                        db.addAttachments(new Attachments_API(
                                record_id,
                                uid,
                                appraisal_type,
                                file,
                                file_name));
                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();


            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            //pDialog.dismiss();
        }

    }

    public void addPpcr() {
        //main table variables
        String report_date_inspected_month = "", report_date_inspected_day = "",
                report_date_inspected_year = "";

        try {
            Log.e("recheck", xml);
            JSONObject records2 = new JSONObject(xml2);

            if (records2.has("valrep_ppcr_date_inspected_month")) {
                report_date_inspected_month = records2.getString("valrep_ppcr_date_inspected_month");
            } else {
                report_date_inspected_month = "";
            }
            if (records2.has("valrep_ppcr_date_inspected_day")) {
                report_date_inspected_day = records2.getString("valrep_ppcr_date_inspected_day");
            } else {
                report_date_inspected_day = "";
            }
            if (records2.has("valrep_ppcr_date_inspected_year")) {
                report_date_inspected_year = records2.getString("valrep_ppcr_date_inspected_year");
            } else {
                report_date_inspected_year = "";
            }

            if ((report_date_inspected_month.equals("null")) || (report_date_inspected_day.equals("null")) || (report_date_inspected_year.equals("null"))) {
                db.addPpcr(new Ppcr_API(record_id, "", "", "", "", "", "", "", ""));
            } else {
                syncPpcr();
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            Log.e("", e.toString());
        }
    }

    public void syncPpcr() {
        //main table variables
        String report_date_inspected_month = "", report_date_inspected_day = "",
                report_date_inspected_year = "", report_time_inspected = "", report_registered_owner = "",
                report_project_type = "", report_is_condo = "", report_remarks = "";

        String report_visit_month = "", report_visit_day = "", report_visit_year = "", report_nth = "", report_value_foundation = "", report_value_columns = "", report_value_beams_girders = "", report_value_surround_walls = "", report_value_interior_partition = "", report_value_roofing_works = "", report_value_plumbing_rough_in = "", report_value_electrical_rough_in = "", report_value_ceiling_works = "", report_value_doors_windows = "", report_value_plastering_wall = "", report_value_slab_include_finish = "", report_value_painting_finishing = "", report_value_plumbing_elect_fix = "", report_value_utilities_tapping = "", report_completion_foundation = "", report_completion_columns = "", report_completion_beams_girders = "", report_completion_surround_walls = "", report_completion_interior_partition = "", report_completion_roofing_works = "", report_completion_plumbing_rough_in = "", report_completion_electrical_rough_in = "", report_completion_ceiling_works = "", report_completion_doors_windows = "", report_completion_plastering_wall = "", report_completion_slab_include_finish = "", report_completion_painting_finishing = "", report_completion_plumbing_elect_fix = "", report_completion_utilities_tapping = "", report_total_value = "";

        try {
            Log.e("recheck", xml);
            JSONObject records2 = new JSONObject(xml2);

            if (records2.has("valrep_ppcr_date_inspected_month")) {
                report_date_inspected_month = records2.getString("valrep_ppcr_date_inspected_month");
            } else {
                report_date_inspected_month = "";
            }
            if (records2.has("valrep_ppcr_date_inspected_day")) {
                report_date_inspected_day = records2.getString("valrep_ppcr_date_inspected_day");
            } else {
                report_date_inspected_day = "";
            }
            if (records2.has("valrep_ppcr_date_inspected_year")) {
                report_date_inspected_year = records2.getString("valrep_ppcr_date_inspected_year");
            } else {
                report_date_inspected_year = "";
            }
            if (records2.has("valrep_ppcr_time_inspected")) {
                report_time_inspected = records2.getString("valrep_ppcr_time_inspected");
            } else {
                report_time_inspected = "";
            }
            if (records2.has("valrep_ppcr_registered_owner")) {
                report_registered_owner = records2.getString("valrep_ppcr_registered_owner");
            } else {
                report_registered_owner = "";
            }
            if (records2.has("valrep_ppcr_project_type")) {
                report_project_type = records2.getString("valrep_ppcr_project_type");
            } else {
                report_project_type = "";
            }
            if (records2.has("valrep_ppcr_is_condo")) {
                report_is_condo = records2.getString("valrep_ppcr_is_condo");
            } else {
                report_is_condo = "";
            }
            if (records2.has("valrep_ppcr_remarks")) {
                report_remarks = records2.getString("valrep_ppcr_remarks");
            } else {
                report_remarks = "";
            }

            db.addPpcr(new Ppcr_API(record_id, report_date_inspected_month, report_date_inspected_day,
                    report_date_inspected_year, report_time_inspected, report_registered_owner,
                    report_project_type, report_is_condo,
                    report_remarks));

            if (records2.has("valrep_ppcr_details")) {

                //load data into array string (sequentially sync to their equivalent string)
                String[] sqlite_string = new String[]{
                        report_visit_month,
                        report_visit_day,
                        report_visit_year,
                        report_nth,
                        report_value_foundation,
                        report_value_columns,
                        report_value_beams_girders,
                        report_value_surround_walls,
                        report_value_interior_partition,
                        report_value_roofing_works,
                        report_value_plumbing_rough_in,
                        report_value_electrical_rough_in,
                        report_value_ceiling_works,
                        report_value_doors_windows,
                        report_value_plastering_wall,
                        report_value_slab_include_finish,
                        report_value_painting_finishing,
                        report_value_plumbing_elect_fix,
                        report_value_utilities_tapping,
                        report_completion_foundation,
                        report_completion_columns,
                        report_completion_beams_girders,
                        report_completion_surround_walls,
                        report_completion_interior_partition,
                        report_completion_roofing_works,
                        report_completion_plumbing_rough_in,
                        report_completion_electrical_rough_in,
                        report_completion_ceiling_works,
                        report_completion_doors_windows,
                        report_completion_plastering_wall,
                        report_completion_slab_include_finish,
                        report_completion_painting_finishing,
                        report_completion_plumbing_elect_fix,
                        report_completion_utilities_tapping,
                        report_total_value};
                String[] case_center_string = new String[]{
                        "valrep_ppcr_visit_month",
                        "valrep_ppcr_visit_day",
                        "valrep_ppcr_visit_year",
                        "valrep_ppcr_nth",
                        "valrep_ppcr_value_foundation",
                        "valrep_ppcr_value_columns",
                        "valrep_ppcr_value_beams_girders",
                        "valrep_ppcr_value_sorround_walls",
                        "valrep_ppcr_value_interior_partition",
                        "valrep_ppcr_value_roofing_works",
                        "valrep_ppcr_value_plumbing_rough_in",
                        "valrep_ppcr_value_electrical_rough_in",
                        "valrep_ppcr_value_ceiling_works",
                        "valrep_ppcr_value_doors_windows",
                        "valrep_ppcr_value_plastering_wall",
                        "valrep_ppcr_value_slab_include_finish",
                        "valrep_ppcr_value_painting_finishing",
                        "valrep_ppcr_value_plumbing_elect_fix",
                        "valrep_ppcr_value_utilities_tapping",
                        "valrep_ppcr_completion_foundation",
                        "valrep_ppcr_completion_columns",
                        "valrep_ppcr_completion_beams_girders",
                        "valrep_ppcr_completion_surround_walls",
                        "valrep_ppcr_completion_interior_partition",
                        "valrep_ppcr_completion_roofing_works",
                        "valrep_ppcr_completion_plumbing_rough_in",
                        "valrep_ppcr_completion_electrical_rough_in",
                        "valrep_ppcr_completion_ceiling_works",
                        "valrep_ppcr_completion_doors_windows",
                        "valrep_ppcr_completion_plastering_wall",
                        "valrep_ppcr_completion_slab_include_finish",
                        "valrep_ppcr_completion_painting_finishing",
                        "valrep_ppcr_completion_plumbing_elect_fix",
                        "valrep_ppcr_completion_utilities_tapping",
                        "valrep_ppcr_total_value"};

                JSONArray details_ary = records2.getJSONArray("valrep_ppcr_details");
                for (int x = 0; x < details_ary.length(); x++) {
                    JSONObject details_obj = details_ary.getJSONObject(x);
	
					/*if (details_ary.getJSONObject(x).has("valrep_ppcr_visit_month")) {
						report_visit_month = details_obj.getString("valrep_ppcr_visit_month");
					}
					if (details_ary.getJSONObject(x).has("valrep_ppcr_visit_day")) {
						report_visit_day = details_obj.getString("valrep_ppcr_visit_day");
					}
					if (details_ary.getJSONObject(x).has("valrep_ppcr_visit_year")) {
						report_visit_year = details_obj.getString("valrep_ppcr_visit_year");
					}
					if (details_ary.getJSONObject(x).has("valrep_ppcr_nth")) {
						report_nth = details_obj.getString("valrep_ppcr_nth");
					}
					if (details_ary.getJSONObject(x).has("valrep_ppcr_value_foundation")) {
						report_value_foundation = details_obj.getString("valrep_ppcr_value_foundation");
					}
					if (details_ary.getJSONObject(x).has("valrep_ppcr_value_columns")) {
						report_value_columns = details_obj.getString("valrep_ppcr_value_columns");
					}
					if (details_ary.getJSONObject(x).has("valrep_ppcr_value_beams_girders")) {
						report_value_beams_girders = details_obj.getString("valrep_ppcr_value_beams_girders");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_value_sorround_walls")) {
						report_value_surround_walls = details_obj
								.getString("valrep_ppcr_value_sorround_walls");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_value_interior_partition")) {
						report_value_interior_partition = details_obj
								.getString("valrep_ppcr_value_interior_partition");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_value_roofing_works")) {
						report_value_roofing_works = details_obj
								.getString("valrep_ppcr_value_roofing_works");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_value_plumbing_rough_in")) {
						report_value_plumbing_rough_in = details_obj
								.getString("valrep_ppcr_value_plumbing_rough_in");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_value_electrical_rough_in")) {
						report_value_electrical_rough_in = details_obj
								.getString("valrep_ppcr_value_electrical_rough_in");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_value_ceiling_works")) {
						report_value_ceiling_works = details_obj
								.getString("valrep_ppcr_value_ceiling_works");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_value_doors_windows")) {
						report_value_doors_windows = details_obj
								.getString("valrep_ppcr_value_doors_windows");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_value_plastering_wall")) {
						report_value_plastering_wall = details_obj
								.getString("valrep_ppcr_value_plastering_wall");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_value_slab_include_finish")) {
						report_value_slab_include_finish = details_obj
								.getString("valrep_ppcr_value_slab_include_finish");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_value_painting_finishing")) {
						report_value_painting_finishing = details_obj
								.getString("valrep_ppcr_value_painting_finishing");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_value_plumbing_elect_fix")) {
						report_value_plumbing_elect_fix = details_obj
								.getString("valrep_ppcr_value_plumbing_elect_fix");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_value_utilities_tapping")) {
						report_value_utilities_tapping = details_obj
								.getString("valrep_ppcr_value_utilities_tapping");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_completion_foundation")) {
						report_completion_foundation = details_obj
								.getString("valrep_ppcr_completion_foundation");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_completion_columns")) {
						report_completion_columns = details_obj
								.getString("valrep_ppcr_completion_columns");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_completion_beams_girders")) {
						report_completion_beams_girders = details_obj
								.getString("valrep_ppcr_completion_beams_girders");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_completion_surround_walls")) {
						report_completion_surround_walls = details_obj
								.getString("valrep_ppcr_completion_surround_walls");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_completion_interior_partition")) {
						report_completion_interior_partition = details_obj
								.getString("valrep_ppcr_completion_interior_partition");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_completion_roofing_works")) {
						report_completion_roofing_works = details_obj
								.getString("valrep_ppcr_completion_roofing_works");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_completion_plumbing_rough_in")) {
						report_completion_plumbing_rough_in = details_obj
								.getString("valrep_ppcr_completion_plumbing_rough_in");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_completion_electrical_rough_in")) {
						report_completion_electrical_rough_in = details_obj
								.getString("valrep_ppcr_completion_electrical_rough_in");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_completion_ceiling_works")) {
						report_completion_ceiling_works = details_obj
								.getString("valrep_ppcr_completion_ceiling_works");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_completion_doors_windows")) {
						report_completion_doors_windows = details_obj
								.getString("valrep_ppcr_completion_doors_windows");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_completion_plastering_wall")) {
						report_completion_plastering_wall = details_obj
								.getString("valrep_ppcr_completion_plastering_wall");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_completion_slab_include_finish")) {
						report_completion_slab_include_finish = details_obj
								.getString("valrep_ppcr_completion_slab_include_finish");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_completion_painting_finishing")) {
						report_completion_painting_finishing = details_obj
								.getString("valrep_ppcr_completion_painting_finishing");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_completion_plumbing_elect_fix")) {
						report_completion_plumbing_elect_fix = details_obj
								.getString("valrep_ppcr_completion_plumbing_elect_fix");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_completion_utilities_tapping")) {
						report_completion_utilities_tapping = details_obj
								.getString("valrep_ppcr_completion_utilities_tapping");
					}
					if (details_ary.getJSONObject(x).has(
							"valrep_ppcr_total_value")) {
						report_total_value = details_obj
								.getString("valrep_ppcr_total_value");
					}*/
                    for (int y = 0; y <= sqlite_string.length - 1; y++) {
                        if (details_ary.getJSONObject(x).has(case_center_string[y])) {
                            sqlite_string[y] = details_obj.getString(case_center_string[y]);
                        } else {
                            sqlite_string[y] = "";
                        }
                    }


                    db2.addPpcr_Details(new Ppcr_API_Details(
                            record_id,
                            sqlite_string[0],//report_visit_month,
                            sqlite_string[1],//report_visit_day,
                            sqlite_string[2],//report_visit_year,
                            sqlite_string[3],//report_nth,
                            sqlite_string[4],//report_value_foundation,
                            sqlite_string[5],//report_value_columns,
                            sqlite_string[6],//report_value_beams_girders,
                            sqlite_string[7],//report_value_surround_walls,
                            sqlite_string[8],//report_value_interior_partition,
                            sqlite_string[9],//report_value_roofing_works,
                            sqlite_string[10],//report_value_plumbing_rough_in,
                            sqlite_string[11],//report_value_electrical_rough_in,
                            sqlite_string[12],//report_value_ceiling_works,
                            sqlite_string[13],//report_value_doors_windows,
                            sqlite_string[14],//report_value_plastering_wall,
                            sqlite_string[15],//report_value_slab_include_finish,
                            sqlite_string[16],//report_value_painting_finishing,
                            sqlite_string[17],//report_value_plumbing_elect_fix,
                            sqlite_string[18],//report_value_utilities_tapping,
                            sqlite_string[19],//report_completion_foundation,
                            sqlite_string[20],//report_completion_columns,
                            sqlite_string[21],//report_completion_beams_girders,
                            sqlite_string[22],//report_completion_surround_walls,
                            sqlite_string[23],//report_completion_interior_partition,
                            sqlite_string[24],//report_completion_roofing_works,
                            sqlite_string[25],//report_completion_plumbing_rough_in,
                            sqlite_string[26],//report_completion_electrical_rough_in,
                            sqlite_string[27],//report_completion_ceiling_works,
                            sqlite_string[28],//report_completion_doors_windows,
                            sqlite_string[29],//report_completion_plastering_wall,
                            sqlite_string[30],//report_completion_slab_include_finish,
                            sqlite_string[31],//report_completion_painting_finishing,
                            sqlite_string[32],//report_completion_plumbing_elect_fix,
                            sqlite_string[33],//report_completion_utilities_tapping,
                            sqlite_string[34]//report_total_value
                    ));
                }


            }


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            Log.e("", e.toString());
        }
    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.btn_yes:
                NetworkUtil.getConnectivityStatusString(this);
                if (!NetworkUtil.status.equals("Network not available")) {
                    decision = "yes";
                    call_type = "update";
                    dialog_status = "Accept";
                    update_dialog();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Network not available", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.btn_no:
                decision = "no";
                call_type = "update";
                dialog_status = "Decline";
                remarks_decline_dialog();

                break;
            case R.id.btn_mysql_attachments:
                Intent in = new Intent(getApplicationContext(), Attachments_Inflater.class);
                in.putExtra(TAG_UID, uid);
                in.putExtra(TAG_RECORD_ID, record_id);
                in.putExtra(TAG_PASS_STAT, pass_stat);
                startActivityForResult(in, 100);//101 for online)

                break;
            default:
                break;
        }
    }

    @Override
    public void onUserInteraction() {
        st.resetDisconnectTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        st.stopDisconnectTimer();
    }

    @Override
    public void onResume() {
        super.onResume();
        st.resetDisconnectTimer();
    }


}
