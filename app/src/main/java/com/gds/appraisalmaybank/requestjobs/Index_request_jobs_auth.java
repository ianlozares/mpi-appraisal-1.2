package com.gds.appraisalmaybank.requestjobs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.check_network.NetworkUtil;
import com.gds.appraisalmaybank.database_new.DatabaseHandler_New;
import com.gds.appraisalmaybank.json.TLSConnection;
import com.gds.appraisalmaybank.main.BuildConfig;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;


@SuppressLint("NewApi")
public class Index_request_jobs_auth extends Activity implements
		OnClickListener {
	EditText et_username, et_password;
	Button btn_login;
	TextView tv_download,tv_timeout;
	String username, pref_username, user_roles, pref_user_roles;
	String password, pref_password;
	SharedPreferences appPrefs;
	SharedPreferences.Editor appPrefsEditor;
	GDS_methods gds= new GDS_methods();
	// for API
	String xml, success;

	Global gs;
	DatabaseHandler_New db;
	private ProgressDialog pDialog;
	String google_response = "";
	boolean network_status;
	Dialog myDialog;
	TLSConnection tlscon = new TLSConnection();
	String xmlRoles, users, roles;
	ArrayList<String> roleList;
	
	CheckBox cbRemember, cbKeep;
	String remember = "", keep = "";
	String pref_remember, pref_keep;
	//HttpClient httpclient = new DefaultHttpClient();
	WifiManager wifiManager;
	WifiLock lock;
	//for tls
	JSONObject jsonResponse = new JSONObject();
	ArrayList<String> field = new ArrayList<>();
	ArrayList<String> value = new ArrayList<>();
	Session_Timer st = new Session_Timer(this);

	TextView tv_version;
	TextView android_version;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.index_auth);
		st.resetDisconnectTimer();
		gs = ((Global) getApplicationContext());

		db = new DatabaseHandler_New(this);
		// create / open tables
		db.getWritableDatabase();

		create_folder();

		appPrefs = getSharedPreferences("preference", 0);
		appPrefsEditor = appPrefs.edit();
		
		et_username = (EditText) findViewById(R.id.et_username);
		et_password = (EditText) findViewById(R.id.et_password);
		btn_login = (Button) findViewById(R.id.btn_login);
		btn_login.setOnClickListener(this);
		cbRemember = (CheckBox) findViewById(R.id.cbRemember);
		cbKeep = (CheckBox) findViewById(R.id.cbKeep);
		tv_timeout = (TextView) findViewById(R.id.tv_timeout);
		tv_download = (TextView) findViewById(R.id.tv_download);
		tv_download.setOnClickListener(this);

		tv_version = (TextView) findViewById(R.id.tv_version);
        android_version = (TextView) findViewById(R.id.android_version);

		int versionCode = BuildConfig.VERSION_CODE;
		String versionName = BuildConfig.VERSION_NAME;
		Log.e("versionCode", "" + versionCode);
		tv_version.setText(getResources().getString(R.string.app_name) + " v" + versionName +"\n"+gs.server_type);
        android_version.setText("Android version "+Build.VERSION.RELEASE);

		/*wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
//		lock = wifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL, "LockTag");
		lock = wifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF, "HIGH_WIFI");
		lock.acquire();*/

		tv_download.requestFocus();
		et_username.setHint("@maybank.com.ph");

		String timeout_error = appPrefs.getString("error", "");
		tv_timeout.setText(timeout_error);
		auth();

	}

	String dir = "gds_appraisal";
	public void create_folder() {
		// create dir
		File myDirectory = new File(Environment.getExternalStorageDirectory()
				+ "/" + dir + "/");
		if (!myDirectory.exists()) {
			myDirectory.mkdirs();
		}
	}

	public void login() {
		username = et_username.getText().toString().toLowerCase();
		password = et_password.getText().toString();

		String uname = username.replaceAll("\\.", "@");//replace dot with @
		String username_only = uname.split("@")[0];//trim char @ and its following char
		Log.i("username_only", username_only);
		try {
			// API
			//for tls
			field.clear();
			field.add("user[email]");
			field.add("user[password]");

			value.clear();
			value.add(username);
			value.add(password);

			if(Global.type.contentEquals("tls")){
				jsonResponse = gds.makeHttpsRequest(gs.sign_in_url, "POST", field, value);
				xml = jsonResponse.toString();
			}else{
				xml=gds.http_posting(field,value,gs.sign_in_url,Index_request_jobs_auth.this);
			}
			/*HttpPost httpPost = new HttpPost(gs.sign_in_url);
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("user[email]", username));
			params.add(new BasicNameValuePair("user[password]", password));
			httpPost.setEntity(new UrlEncodedFormEntity(params));
			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity httpEntity = response.getEntity();
			xml = EntityUtils.toString(httpEntity);*/
			Log.e("response", xml);
			//===
			
			// API

			//for tls
			field.clear();
			field.add("");

			value.clear();
			value.add("");
			/*URL url = new URL(gs.user_role_url+username_only+gs.user_role_end_url);
			jsonResponse = gds.makeHttpsRequest(""+url, "GET", field, value);
			xmlRoles = jsonResponse.toString();*/

			if(Global.type.contentEquals("tls")){
				URL urlRole = new URL(gs.user_role_url+username_only+gs.user_role_end_url);
				jsonResponse = gds.makeHttpsRequest("" + urlRole, "GET", field, value);
				xmlRoles = jsonResponse.toString();

				Log.e("URL Role", ""+urlRole);
				Log.e("xmlRoles response", ""+xmlRoles);
			}else{
				/*HttpGet httpGetUserRole = new HttpGet(gs.user_role_url+username_only+gs.user_role_end_url);
				HttpResponse responseT = httpclient.execute(httpGetUserRole);
				HttpEntity httpEntityT = responseT.getEntity();
				xmlRoles = EntityUtils.toString(httpEntityT);*/
			}



			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("", e.toString());
		}

	}


	public void auth() {

		remember = appPrefs.getString("remember", "");
        keep = appPrefs.getString("keep", "");
        
		pref_remember = appPrefs.getString("remember", "");
        if(pref_remember.equals("true")){
        	cbRemember.setChecked(true);
        	et_username.setText(appPrefs.getString("username", ""));
//        	et_password.setText(appPrefs.getString("password", ""));
        }
        
//		pref_username = appPrefs.getString("username", "");
//		if (!pref_username.equals("")) {
//			startActivity(new Intent(Index_request_jobs_auth.this,
//					Index_request_reports_jobs.class));
//			finish();
//		}
        
		pref_keep = appPrefs.getString("keep", "");
        if(pref_keep.equals("true")){
        	startActivity(new Intent(Index_request_jobs_auth.this, Index_request_reports_jobs.class));
			finish();
        } 
		
	}
	
	private class FetchData extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(Index_request_jobs_auth.this);
			pDialog.setMessage("Logging in..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.setCanceledOnTouchOutside(false);
			pDialog.show();
		}

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			NetworkUtil
					.getConnectivityStatusString(Index_request_jobs_auth.this);
			if (!NetworkUtil.status.equals("Network not available")) {
				google_response = gds.google_ping();
				if (google_response.equals("200")) {
					login();
					network_status = true;
				} else {
					network_status = false;
					
					runOnUiThread(new Runnable() {
						public void run() {
							//custom toast
							Toast toast = Toast.makeText(getBaseContext(), "Unable to connect to the server", Toast.LENGTH_LONG);
							View view = toast.getView();
							view.setBackgroundResource(R.color.toast_red);
							toast.show();
							serverError();
						}
					});
				}

			} else {
				network_status = false;

			}

			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			if (network_status == false) {
				Toast.makeText(getApplicationContext(),
						"Network not available", Toast.LENGTH_SHORT).show();
			} else if (network_status == true) {
				try {
					// json
					JSONObject records = new JSONObject(xml);
					success = records.getString("success");
					
					//roles
					if(Global.type.contentEquals("tls")){
						JSONObject userObj = new JSONObject(xmlRoles);
						JSONArray userArray = userObj.getJSONArray("users");
						Log.i("userArray",userArray.toString());
						for (int x = 0; x < userArray.length(); x++) {
							JSONObject roleObj = userArray.getJSONObject(x);
							Log.i("roleObj",roleObj.toString());

							roles = roleObj.getString("roles");//
							Log.i("role",roles.toString());//

							roles = roles.replace("[", "");//remove [
							roles = roles.replace("]", "");//remove ]
							roles = roles.replace("\"", "");//remove "

							roleList = new ArrayList<String>(Arrays.asList(roles.split(",")));
							for(int a = 0; a <= roleList.size()-1; a++){
								Log.i("list ", a+" "+roleList.get(a).toString());
							}
						}
					}
					//roles
					
					if (success.equals("true")) {
						//remember user
						SharedPreferences.Editor prefsEditor = appPrefs.edit();
						prefsEditor.putString("username", username);
						prefsEditor.putString("user_roles", roles);
						prefsEditor.commit();
						
						//keep signed in
						if (cbKeep.isChecked()){
//				        	SharedPreferences.Editor prefsEditor = appPrefs.edit();
				        	prefsEditor.putString("keep", "true");
				        	prefsEditor.commit();
				        } else {
//				        	SharedPreferences.Editor prefsEditor = appPrefs.edit();
				        	prefsEditor.putString("keep", "false");
				        	prefsEditor.commit();
				        }
						
						startActivity(new Intent(Index_request_jobs_auth.this,
								Index_request_reports_jobs.class));
						finish();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					if (!e.equals("")) {
						Toast.makeText(getApplicationContext(),
								"Invalid e-mail or password",
								Toast.LENGTH_SHORT).show();
					}

				}
			}
			pDialog.dismiss();
		}
	}

	/*public void google_ping() {
		try {
//			URL obj = new URL("https://www.google.com.ph/?gws_rd=cr&ei=td9kUrP0H8mjigenuoHAAg");
			URL obj = new URL(gs.server_url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");

			// add request header
			con.setRequestProperty("User-Agent", "Mozilla/5.0");

			google_response = String.valueOf(con.getResponseCode());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	
	public void serverError(){
		
		myDialog = new Dialog(Index_request_jobs_auth.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.warning_dialog);
		myDialog.setCancelable(true);
		final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
		final Button btn_ok = (Button) myDialog.findViewById(R.id.btn_ok);
		
		tv_question.setText(R.string.server_error);
		btn_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		myDialog.show();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_login:
			
			if ((et_username.getText().toString().length()==0)
					||(et_password.getText().toString().length()==0)){
				Toast.makeText(getApplicationContext(), "Please fill up all fields", Toast.LENGTH_SHORT).show();
			} else {
				NetworkUtil.getConnectivityStatusString(this);
				if (!NetworkUtil.status.equals("Network not available")) {
					new FetchData().execute();
				} else {
					Toast.makeText(getApplicationContext(),
							"Network not available", Toast.LENGTH_SHORT).show();
				}

				if (cbRemember.isChecked()){
		        	SharedPreferences.Editor prefsEditor = appPrefs.edit();
		        	prefsEditor.putString("username", et_username.getText().toString().toLowerCase());
//		        	prefsEditor.putString("password", et_password.getText().toString());
		        	prefsEditor.putString("remember", "true");
		    		prefsEditor.commit();
		        } else {
		        	SharedPreferences.Editor prefsEditor = appPrefs.edit();
		        	prefsEditor.putString("username", "");
		        	prefsEditor.putString("password", "");
		        	prefsEditor.putString("remember", "false");
		        	prefsEditor.commit();
		        }
			}

			break;
			
		case R.id.tv_download:
//			final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
			//final String appPackageName = "com.gds.projects.lotplottingapp"; // getPackageName() from Context or Activity object
			final String appPackageName = "com.gds.projects.plotter"; // getPackageName() from Context or Activity object
			try {
			    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
			} catch (android.content.ActivityNotFoundException anfe) {
			    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		exit_dialog();
	}

	public void exit_dialog() {
		// custom dialog
		final Dialog dialog = new Dialog(Index_request_jobs_auth.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.yes_no_dialog);

		// set the custom dialog components - text, image and button
		TextView text = (TextView) dialog.findViewById(R.id.tv_question);
		text.setText("Are you sure you want to exit the application?");
		Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
		Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

		// if button is clicked, close the custom dialog
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				lock.release();
//				finish();
				android.os.Process.killProcess(android.os.Process.myPid());//kill the app
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}
	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}
	
}