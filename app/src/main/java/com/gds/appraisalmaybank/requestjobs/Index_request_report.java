package com.gds.appraisalmaybank.requestjobs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.condo.Condo;
import com.gds.appraisalmaybank.construction_ebm.Construction_ebm;
import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Report_Accepted_Jobs;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.land_improvements.Land_Improvements;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.Session_Timer;
import com.gds.appraisalmaybank.motor_vehicle.Motor_vehicle;
import com.gds.appraisalmaybank.ppcr.Ppcr;
import com.gds.appraisalmaybank.spv.Spv;
import com.gds.appraisalmaybank.townhouse.Townhouse;
import com.gds.appraisalmaybank.vacant_lot.Vacant_lot;
import com.gds.appraisalmaybank.waf.Waf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SuppressLint("NewApi")
public class Index_request_report extends Activity {
	Session_Timer st = new Session_Timer(this);
	String username, pref_username;
	SharedPreferences appPrefs;
	SharedPreferences.Editor appPrefsEditor;
	LayoutInflater inflater;
	ArrayList<HashMap<String, Object>> searchResults;
	private ProgressDialog pDialog;
	ListView playerListView;
	String[][] names;

	Integer[] photos;
	Global gs;
	ArrayList<HashMap<String, Object>> originalValues;

	DatabaseHandler db;

	int i;
	String temp_fname, temp_mname, temp_lname,temp_is_company,temp_company_name;
	String output_appraisal_type;
	CustomAdapter adapter;
	String app_type_values, values;
	DatabaseHandler2 db2 = new DatabaseHandler2(this);
	String where = "WHERE record_id = args";
	String[] args = new String[1];
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.index_reports);

		st.resetDisconnectTimer();
		db = new DatabaseHandler(this);
		db2 = new DatabaseHandler2(this);
		gs = ((Global) getApplicationContext());
		appPrefs = getSharedPreferences("preference", 0);
		appPrefsEditor = appPrefs.edit();
		pref_username = appPrefs.getString("username", "");
		final EditText txt_search = (EditText) findViewById(R.id.txt_search);
		playerListView = (ListView) findViewById(android.R.id.list);
		// onclickitem
		playerListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				values = ((TextView) arg1.findViewById(R.id.txt_values))
						.getText().toString();
				app_type_values = ((TextView) arg1
						.findViewById(R.id.app_type_values)).getText()
						.toString();

				if (values.equals("")) {
					Toast.makeText(getApplicationContext(), "No record found",
							Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(getApplicationContext(), values,
							Toast.LENGTH_SHORT).show();
					gs.record_id = values;
					if (app_type_values.equals("townhouse_condo")) {
						/*startActivityForResult(
								new Intent(getApplicationContext(),
										Townhouse_condo.class), 0);*/
						startActivityForResult(
								new Intent(getApplicationContext(),
										Condo.class), 0);
					} else if (app_type_values.equals("motor_vehicle")) {
						startActivityForResult(new Intent(
								getApplicationContext(), Motor_vehicle.class),
								0);
					} else if (app_type_values.equals("vacant_lot")) {
						startActivityForResult(new Intent(
								getApplicationContext(), Vacant_lot.class), 0);
					} else if (app_type_values.equals("land_improvements")) {
						startActivityForResult(new Intent(
								getApplicationContext(),
								Land_Improvements.class), 0);
					} else if (app_type_values.equals("construction_ebm")) {
						startActivityForResult(
								new Intent(getApplicationContext(),
										Construction_ebm.class), 0);
					} else if (app_type_values.equals("ppcr")) {
						startActivityForResult(new Intent(
								getApplicationContext(), Ppcr.class), 0);
					}
					//added classes
					else if (app_type_values.equals("condominium")) {
						startActivityForResult(
								new Intent(getApplicationContext(),
										Condo.class), 0);
					} else if (app_type_values.equals("townhouse")) {
						startActivityForResult(
								new Intent(getApplicationContext(),
										Townhouse.class), 0);
					}else if (app_type_values.equals("waf")) {
						startActivityForResult(
								new Intent(getApplicationContext(),
										Waf.class), 0);
					}else if (app_type_values.equals("spv_land_improvements")) {
						startActivityForResult(
								new Intent(getApplicationContext(),
										Spv.class), 0);
					}

				}

			}

		});

		new SendData().execute();
		txt_search.addTextChangedListener(new TextWatcher() {

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// get the text in the EditText
				String searchString = txt_search.getText().toString();
				int textLength = searchString.length();
				searchResults.clear();

				for (int i = 0; i < originalValues.size(); i++) {
					String playerName = originalValues.get(i).get("name")
							.toString();
					if (textLength <= playerName.length()) {
						// compare the String in EditText with Names in the
						// ArrayList
						if (searchString.equalsIgnoreCase(playerName.substring(
								0, textLength)))
							searchResults.add(originalValues.get(i));
					}
				}

				adapter.notifyDataSetChanged();
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			public void afterTextChanged(Editable s) {

			}
		});
	}

	private class SendData extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(Index_request_report.this);
			pDialog.setMessage("Fetching data..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			// listview
			// ArrayList thats going to hold the search results

			// get the LayoutInflater for inflating the customView
			// this will be used in the custom adapter
			inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			List<Report_Accepted_Jobs> report_accepted_jobs = db
					.getAllReport_Accepted_Jobs();

			if (!report_accepted_jobs.isEmpty()) {
				// initialize the array
				names = new String[report_accepted_jobs.size()][4];
				photos = new Integer[report_accepted_jobs.size()];

				for (Report_Accepted_Jobs im : report_accepted_jobs) {
					temp_fname = string_checker(im.getfname());
					temp_mname = string_checker(im.getmname());
					temp_lname = string_checker(im.getlname());
					args[0]=im.getrecord_id();
					temp_is_company = db2.getRecord("app_account_is_company",where,args,"tbl_report_accepted_jobs").get(0);
					temp_company_name = db2.getRecord("app_account_company_name",where,args,"tbl_report_accepted_jobs").get(0);
					for (int x = 0; x < gs.appraisal_type_value.length; x++) {
						if (im.getappraisal_type().equals(
								gs.appraisal_type_value[x])) {
							output_appraisal_type = gs.appraisal_output[x];
						}
					}
					if(temp_is_company.contentEquals("true")){
						names[i][0] = temp_company_name;
					}else{
						names[i][0] = temp_fname + temp_mname + temp_lname;
					}
					names[i][1] = im.getrecord_id();
					names[i][2] = output_appraisal_type;
					names[i][3] = im.getappraisal_type();
					photos[i] = R.drawable.monotoneright_white;
					i++;

				}

			} else {
				names = new String[1][4];
				photos = new Integer[1];
				names[0][0] = "no record found";
				names[0][1] = "";
				names[0][2] = "";
				names[0][3] = "";
				photos[0] = R.drawable.monotoneright_white;


			}

			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub



			originalValues = new ArrayList<HashMap<String, Object>>();

			// temporary HashMap for populating the
			// Items in the ListView
			HashMap<String, Object> temp;

			// total number of rows in the ListView
			int noOfPlayers = names.length;
			int index = 1;
			// now populate the ArrayList players
			for (int i = 0; i < noOfPlayers; i++) {
				temp = new HashMap<String, Object>();

				temp.put("name", names[i][0]);
				temp.put("txt_values", names[i][1]);
				temp.put("app_type", names[i][2]);
				temp.put("app_type_values", names[i][3]);
				temp.put("photo", photos[i]);
				if (noOfPlayers == 1) {
					temp.put("txt_index", "");
				} else {
					temp.put("txt_index", index + ".");
				}
				// add the row to the ArrayList
				index++;
				originalValues.add(temp);
			}

			// searchResults=OriginalValues initially
			searchResults = new ArrayList<HashMap<String, Object>>(
					originalValues);
			// will populate the ListView
			adapter = new CustomAdapter(Index_request_report.this,
					R.layout.index_reports_layout, searchResults);

			// finally,set the adapter to the default ListView
			playerListView.setAdapter(adapter);
			pDialog.dismiss();


		}
	}

	// define your custom adapter
	private class CustomAdapter extends ArrayAdapter<HashMap<String, Object>> {

		public CustomAdapter(Context context, int textViewResourceId,
				ArrayList<HashMap<String, Object>> Strings) {

			// let android do the initializing :)
			super(context, textViewResourceId, Strings);
		}

		// class for caching the views in a row
		private class ViewHolder {
			ImageView photo;
			TextView name;
			TextView app_type;
			TextView txt_values;
			TextView app_type_values;
			TextView txt_index;
		}

		ViewHolder viewHolder;

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.index_reports_layout,
						null);
				viewHolder = new ViewHolder();

				// cache the views
				viewHolder.photo = (ImageView) convertView
						.findViewById(R.id.mono);
				viewHolder.name = (TextView) convertView
						.findViewById(R.id.txt_image_list);
				viewHolder.app_type = (TextView) convertView
						.findViewById(R.id.app_type);
				viewHolder.txt_values = (TextView) convertView
						.findViewById(R.id.txt_values);
				viewHolder.app_type_values = (TextView) convertView
						.findViewById(R.id.app_type_values);
				viewHolder.txt_index = (TextView) convertView
						.findViewById(R.id.txt_index);
				// link the cached views to the convertview
				convertView.setTag(viewHolder);

			} else
				viewHolder = (ViewHolder) convertView.getTag();

			int photoId = (Integer) searchResults.get(position).get("photo");

			// set the data to be displayed
			viewHolder.photo.setImageDrawable(getResources().getDrawable(
					photoId));
			viewHolder.name.setText(searchResults.get(position).get("name")
					.toString());
			viewHolder.app_type.setText(searchResults.get(position)
					.get("app_type").toString());
			viewHolder.txt_values.setText(searchResults.get(position)
					.get("txt_values").toString());
			viewHolder.app_type_values.setText(searchResults.get(position)
					.get("app_type_values").toString());
			viewHolder.txt_index.setText(searchResults.get(position)
					.get("txt_index").toString());
			// return the view to be displayed
			return convertView;
		}

	}

	public String string_checker(String old_string) {
		String new_string = old_string;
		if (old_string.equals(" ")) {
			new_string = "";
		} else if (old_string.equals("")) {
			new_string = "";
		} else {
			new_string = old_string + " ";
		}

		return new_string;
	}

	@SuppressLint("NewApi")
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Intent intent = new Intent(getApplicationContext(),
				Index_request_reports_jobs.class);
		overridePendingTransition(0, 0);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		finish();
		overridePendingTransition(0, 0);
		startActivity(intent);

	}
	
	@Override
	public void onBackPressed() {
		this.getParent().onBackPressed();
	}
	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}


}
