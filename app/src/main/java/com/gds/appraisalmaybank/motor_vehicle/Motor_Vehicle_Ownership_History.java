package com.gds.appraisalmaybank.motor_vehicle;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database2.Motor_Vehicle_API_Ownership_History;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Motor_Vehicle_Ownership_History extends ListActivity implements OnClickListener {
	DatabaseHandler2 db = new DatabaseHandler2(this);
	GDS_methods gds = new GDS_methods();
	Button btn_add_oh, btn_save_oh;
	Button btn_create, btn_cancel, btn_save;

	Session_Timer st = new Session_Timer(this);
	String record_id="";
	private static final String TAG_RECORD_ID = "record_id";

	Dialog myDialog;
	//create dialog
	EditText  report_verification_ownerhistory_name;
	DatePicker report_verification_ownerhistory_date;

	//forListViewDeclarations
	private ProgressDialog pDialog;
	ArrayList<HashMap<String, String>> itemList;
	private static final String tag_mvoh_id = "mvoh_id";
	private static final String tag_name = "name";
	private static final String tag_ownerhistory_date = "ownerhistory_date";
	String mvoh_id="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reports_mv_ownership_history);

		st.resetDisconnectTimer();
		btn_add_oh = (Button) findViewById(R.id.btn_add_oh);
		btn_add_oh.setOnClickListener(this);

		btn_save_oh = (Button) findViewById(R.id.btn_save_oh);
		btn_save_oh.setOnClickListener(this);

		// getting record_id from intent / previous class
		Intent i = getIntent();
		record_id = i.getStringExtra(TAG_RECORD_ID);
		//list
		itemList = new ArrayList<HashMap<String, String>>();
		// Loading in Background Thread
		new LoadAll().execute();
		// Get listview
		ListView lv = getListView();

		// on seleting single item
		// launching Edit item Screen
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {
				// getting values from selected ListItem
				mvoh_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
				view_details();
			}
		});
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

			public boolean onItemLongClick(AdapterView<?> parent, View view,
										   int position, long id) {
				// TODO Auto-generated method stub

				Log.v("long clicked","pos: " + position);
				mvoh_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
				delete_item();
				return true;
			}
		});

	}
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
			case R.id.btn_add_oh:
				add_new();
				break;
			case R.id.btn_save_oh:
				finish();
				break;
			default:
				break;
		}
	}

	public void add_new(){
		Toast.makeText(getApplicationContext(), "Add New",
				Toast.LENGTH_SHORT).show();
		myDialog = new Dialog(Motor_Vehicle_Ownership_History.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.reports_mv_ownership_history_form);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);

		btn_create = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_create.setText("Create");
		btn_cancel.setText("Cancel");
		report_verification_ownerhistory_name = (EditText) myDialog.findViewById(R.id.report_verification_ownerhistory_name);
		report_verification_ownerhistory_date = (DatePicker) myDialog.findViewById(R.id.report_verification_ownerhistory_date);

		btn_create.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				//list of EditTexts put in array

					//adjust for month
					String adjMonth="";
					if (String.valueOf(report_verification_ownerhistory_date.getMonth()).length() == 1) {//if month is 1 digit add 0 to initial +1 to value
						if (String.valueOf(report_verification_ownerhistory_date.getMonth())
								.equals("9")) {//if october==9 just add 1
							adjMonth = String.valueOf(report_verification_ownerhistory_date.getMonth() + 1);
						} else {
							adjMonth = "0"+ String.valueOf(report_verification_ownerhistory_date.getMonth() + 1);
						}
					} else {
						adjMonth = String.valueOf(report_verification_ownerhistory_date.getMonth() + 1);
					}

					//add data in SQLite
					db.addMotor_Vehicle_Ownership_History(new Motor_Vehicle_API_Ownership_History(
							record_id,
							String.valueOf(report_verification_ownerhistory_date.getDayOfMonth()),
							adjMonth,
							String.valueOf(report_verification_ownerhistory_date.getYear()),
							report_verification_ownerhistory_name.getText().toString()));

					Toast.makeText(getApplicationContext(),"Data created", Toast.LENGTH_SHORT).show();
					myDialog.dismiss();
					reloadClass();


			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});

		gds.fill_in_error(new EditText[]{report_verification_ownerhistory_name});
		myDialog.show();
	}

	public void view_details(){
		myDialog = new Dialog(Motor_Vehicle_Ownership_History.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.reports_mv_ownership_history_form);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);

		btn_save = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_save.setText("Save");
		btn_cancel.setText("Cancel");

		report_verification_ownerhistory_name = (EditText) myDialog.findViewById(R.id.report_verification_ownerhistory_name);
		report_verification_ownerhistory_date = (DatePicker) myDialog.findViewById(R.id.report_verification_ownerhistory_date);
		List<Motor_Vehicle_API_Ownership_History> mvohList = db
				.getMotor_Vehicle_Ownership_History(String.valueOf(record_id), String.valueOf(mvoh_id));
		if (!mvohList.isEmpty()) {
			for (Motor_Vehicle_API_Ownership_History im : mvohList) {
				//date of inspection
				// set current date into datepicker
				if ((!im.getreport_verification_ownerhistory_date_year().equals(""))||(!im.getreport_verification_ownerhistory_date_month().equals(""))) {
					report_verification_ownerhistory_date.init(
							Integer.parseInt(im.getreport_verification_ownerhistory_date_year()),
							Integer.parseInt(im.getreport_verification_ownerhistory_date_month())-1,
							Integer.parseInt(im.getreport_verification_ownerhistory_date_day()),
							null);
				}
				report_verification_ownerhistory_name.setText(im.getreport_verification_ownerhistory_name());
			}
		}

		btn_save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

					//update data in SQLite
					Motor_Vehicle_API_Ownership_History mv = new Motor_Vehicle_API_Ownership_History();

					//date
					mv.setreport_verification_ownerhistory_date_day(String.valueOf(report_verification_ownerhistory_date.getDayOfMonth()));
					if (String.valueOf(report_verification_ownerhistory_date.getMonth()).length() == 1) {//if month is 1 digit add 0 to initial +1 to value
						if (String.valueOf(report_verification_ownerhistory_date.getMonth())
								.equals("9")) {//if october==9 just add 1
							mv.setreport_verification_ownerhistory_date_month(String
									.valueOf(report_verification_ownerhistory_date.getMonth() + 1));
						} else {
							mv.setreport_verification_ownerhistory_date_month("0"
									+ String.valueOf(report_verification_ownerhistory_date
									.getMonth() + 1));
						}
					} else {
						mv.setreport_verification_ownerhistory_date_month(String
								.valueOf(report_verification_ownerhistory_date.getMonth() + 1));
					}
					mv.setreport_verification_ownerhistory_date_year(String.valueOf(report_verification_ownerhistory_date.getYear()));
					mv.setreport_verification_ownerhistory_name(report_verification_ownerhistory_name.getText().toString());

					db.updateMotor_Vehicle_Ownership_History(mv, record_id, mvoh_id);
					db.close();
					myDialog.dismiss();
					Toast.makeText(getApplicationContext(), "Saved",Toast.LENGTH_SHORT).show();
					reloadClass();

			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});


		gds.fill_in_error(new EditText[]{report_verification_ownerhistory_name});
		myDialog.show();
	}


	public void reloadClass(){
		Intent intent = getIntent();
		finish();
		startActivity(intent);
	}
	public void delete_item(){
		myDialog = new Dialog(Motor_Vehicle_Ownership_History.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.delete_dialog);
		myDialog.setCancelable(true);
		final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
		final Button btn_yes = (Button) myDialog.findViewById(R.id.btn_yes);
		final Button btn_no = (Button) myDialog.findViewById(R.id.btn_no);
		tv_question.setText("Are you sure you want to delete the selected item?");
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				db.deleteMotor_Vehicle_Ownership_History(record_id, mvoh_id);
				db.close();
				myDialog.dismiss();
				reloadClass();
				Toast.makeText(getApplicationContext(), "Data Deleted",Toast.LENGTH_SHORT).show();
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});

		myDialog.show();
	}


	/**
	 * Background Async Task to Load all
	 * */
	class LoadAll extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Motor_Vehicle_Ownership_History.this);
			pDialog.setMessage("Loading Lists. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All
		 * */
		protected String doInBackground(String... args) {
			String name="", ownerhistory_date="";


			List<Motor_Vehicle_API_Ownership_History> lirdps = db.getMotor_Vehicle_Ownership_History(record_id);
			if (!lirdps.isEmpty()) {
				for (Motor_Vehicle_API_Ownership_History imrdps : lirdps) {
					mvoh_id = String.valueOf(imrdps.getID());
					record_id = imrdps.getrecord_id();
					name = imrdps.getreport_verification_ownerhistory_name();
					ownerhistory_date = imrdps.getreport_verification_ownerhistory_date_month()
							+"/"+imrdps.getreport_verification_ownerhistory_date_day()
							+"/"+imrdps.getreport_verification_ownerhistory_date_year();							;

					HashMap<String, String> map = new HashMap<String, String>();
					map.put(TAG_RECORD_ID, record_id);
					map.put(tag_mvoh_id, mvoh_id);
					map.put(tag_name, name);
					map.put(tag_ownerhistory_date, ownerhistory_date);

					// adding HashList to ArrayList
					itemList.add(map);
				}
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					ListAdapter adapter = new SimpleAdapter(
							Motor_Vehicle_Ownership_History.this, itemList,
							R.layout.reports_mv_ownership_history_list, new String[] { TAG_RECORD_ID, tag_mvoh_id, tag_name,
							tag_ownerhistory_date},
							new int[] { R.id.report_record_id, R.id.primary_key, R.id.item_name, R.id.item_date});
					// updating listview
					setListAdapter(adapter);
				}
			});

		}
	}

	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}


}