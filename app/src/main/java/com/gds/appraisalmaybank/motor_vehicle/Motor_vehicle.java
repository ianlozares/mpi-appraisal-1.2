package com.gds.appraisalmaybank.motor_vehicle;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.attachments.Attachments_Inflater;
import com.gds.appraisalmaybank.attachments.Collage;
import com.gds.appraisalmaybank.check_network.NetworkUtil;
import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Images;
import com.gds.appraisalmaybank.database.Logs;
import com.gds.appraisalmaybank.database.Motor_Vehicle_API;
import com.gds.appraisalmaybank.database.Report_Accepted_Jobs;
import com.gds.appraisalmaybank.database.Report_Accepted_Jobs_Contacts;
import com.gds.appraisalmaybank.database.Report_filename;
import com.gds.appraisalmaybank.database.Required_Attachments;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database2.Motor_Vehicle_API_Prev_Appraisal;
import com.gds.appraisalmaybank.database_new.DatabaseHandler_New;
import com.gds.appraisalmaybank.database_new.Tables;
import com.gds.appraisalmaybank.json.MyHttpClient;
import com.gds.appraisalmaybank.json.TLSConnection;
import com.gds.appraisalmaybank.json.UserFunctions;
import com.gds.appraisalmaybank.main.Connectivity;
import com.gds.appraisalmaybank.main.Global;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;
import com.gds.appraisalmaybank.report.View_Report;
import com.gds.appraisalmaybank.required_fields.Fields_Checker;
import com.gds.appraisalmaybank.townhouse_condo.Pdf_Townhouse_condo;

import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import javax.net.ssl.HttpsURLConnection;

@SuppressLint("InflateParams")
public class Motor_vehicle extends Activity implements OnClickListener {
    GDS_methods gds = new GDS_methods();
    Session_Timer st = new Session_Timer(this);
    ActionBar actionBar;
    boolean fieldsOK_page2 = false;

    String submit_record_id;
    private static final String TAG_RECORD_ID = "record_id";
    private static final String TAG_PASS_STAT = "pass_stat";
    private static final String TAG_UID = "uid";
    String pass_stat = "offline";

    Double matrix_value;
    TextView tv_requested_month, tv_requested_day, tv_requested_year,
            tv_classification, tv_account_name,tv_company_name, tv_requesting_party, tv_requestor,
            tv_control_no, tv_appraisal_type, tv_nature_appraisal, tv_kind_of_appraisal,
            tv_ins_month1, tv_ins_day1, tv_ins_year1, tv_ins_month2,
            tv_ins_day2, tv_ins_year2, tv_com_month, tv_com_day, tv_com_year,
            tv_tct_no, tv_unit_no, tv_bldg_name, tv_street_no, tv_street_name,
            tv_village, tv_district, tv_zip_code, tv_city, tv_province,
            tv_region, tv_country, tv_address, tv_attachment;
    TextView tv_vehicle_type, tv_car_model, tv_car_loan_purpose,
            tv_car_mileage, tv_car_series, tv_car_color, tv_car_plate_no,
            tv_car_body_type, tv_car_displacement, tv_car_motor_no,
            tv_car_chassis_no, tv_car_no_cylinders, tv_car_cr_no,
            tv_car_cr_date, tv_app_request_remarks, tv_app_kind_of_appraisal, tv_app_branch_code;
    LinearLayout ll_contact;
    TableRow ll_tct, ll_address, ll_header_address;
    TableRow ll_vehicle_type, ll_car_model, ll_car_loan_purpose,
            ll_car_mileage, ll_car_series, ll_car_color, ll_car_plate_no,
            ll_car_body_type, ll_car_displacement, ll_car_motor_no,
            ll_car_chassis_no, ll_car_no_cylinders, ll_car_cr_no, ll_car_cr_date;
    DatabaseHandler db = new DatabaseHandler(this);
    DatabaseHandler2 db2 = new DatabaseHandler2(this);
    ArrayList<String> field = new ArrayList<String>();
    ArrayList<String> value = new ArrayList<String>();
    Global gs;
    TLSConnection tlscon = new TLSConnection();
    String requested_month, requested_day, requested_year, classification,
            account_fname, account_mname, account_lname,account_is_company,account_company_name, requesting_party, requestor,
            control_no, appraisal_type, nature_appraisal, kind_of_appraisal, ins_month1, ins_day1,
            ins_year1, ins_month2, ins_day2, ins_year2, com_month, com_day,
            com_year, tct_no, unit_no, bldg_name,
            street_no, street_name, village, district, zip_code, city,
            province, region, country, counter, output_appraisal_type,
            output_street_name, output_village, output_city,
            output_province, app_request_remarks, app_kind_of_appraisal, app_branch_code;
    //motor_vehicle special fields
    String vehicle_type, car_model, car_loan_purpose, car_mileage, car_series,
            car_color, car_plate_no, car_body_type, car_displacement,
            car_motor_no, car_chassis_no, car_no_cylinders, car_cr_no, car_cr_date_month,
            car_cr_date_day, car_cr_date_year;
    String record_id, kind_appraisal,has_prev,nature;
    ImageView btn_report_page1, btn_report_page2, btn_report_page3;
    TextView tv_report_page1, tv_report_page3;
    Button btn_report_update_cc, btn_report_view_report;
    ProgressDialog pDialog;
    TableRow tableRowPage1;
    // attachments
    int responseCode;
    String google_response = "";
    String pdf_name;
    ImageView btn_select_pdf, btn_report_pdf;
    ListView dlg_priority_lvw = null;
    Context context = this;
    String item, filename;
    String dir = "gds_appraisal";
    File myDirectory = new File(Environment.getExternalStorageDirectory() + "/"
            + dir + "/");
    String attachment_selected, uid, file, candidate_done;
    int serverResponseCode = 0;
    //for fileUpload checking
    boolean fileUploaded = false;

    String db_app_uid, db_app_file, db_app_filename, db_app_appraisal_type,
            db_app_candidate_done;
    //car_details_dialog
    EditText et_car_model, et_car_mileage, et_car_series, et_car_color,
            et_car_plate_no, et_car_displacement,
            et_car_motor_no, et_car_chassis_no, et_car_no_cylinders, et_car_cr_no;
    AutoCompleteTextView et_vehicle_type,
            et_car_loan_purpose, et_car_body_type;
    DatePicker report_car_cr_date;
    EditText report_date_requested;
    Button btn_update_car_details, btn_update_app_details, btn_save_car_details, btn_cancel_car_details, btn_save_app_details, btn_cancel_app_details;
    EditText et_first_name, et_middle_name, et_last_name, et_requestor,app_account_company_name;
    Spinner spinner_purpose_appraisal;
    EditText et_app_kind_of_appraisal;
    CheckBox app_account_is_company;
    AutoCompleteTextView et_requesting_party;
    CheckBox cb_car_cr_date,cb_date_first_registered;
    // page1
    Dialog myDialog, myDialog2;
    DatePicker report_date_first_registered;
    EditText report_time_inspected, report_date_inspected_day, report_date_inspected_month, report_date_inspected_year;
    Spinner report_interior_dashboard, report_interior_sidings,
            report_interior_seats, report_interior_windows,
            report_interior_ceiling, report_interior_instrument,
            report_bodytype_grille, report_bodytype_headlight,
            report_bodytype_fbumper, report_bodytype_hood,
            report_bodytype_rf_fender, report_bodytype_rf_door,
            report_bodytype_rr_door, report_bodytype_rr_fender,
            report_bodytype_backdoor, report_bodytype_taillight,
            report_bodytype_r_bumper, report_bodytype_lr_fender,
            report_bodytype_lr_door, report_bodytype_lf_door,
            report_bodytype_lf_fender, report_bodytype_top,
            report_bodytype_paint, report_bodytype_flooring, valrep_mv_bodytype_trunk;
    AutoCompleteTextView report_misc_stereo, report_misc_tools, report_misc_speakers, report_misc_tires, report_misc_mag_wheels,
            report_misc_carpet, report_misc_others,report_misc_airbag;
    AutoCompleteTextView report_enginearea_transmission1, report_enginearea_transmission2;
    Spinner report_enginearea_fuel, report_enginearea_chassis, report_enginearea_battery,
            report_enginearea_electrical, report_enginearea_engine;
    Button btn_update_page1;
    //page2
    Spinner report_verification;
    EditText report_verification_registered_owner, report_verification_file_no,
            report_market_valuation_fair_value, report_market_valuation_min,
            report_market_valuation_max, report_market_valuation_remarks, valrep_mv_market_valuation_previous_remarks;
    TextView tv_report_verification_registered_owner;
    AutoCompleteTextView report_place_inspected,report_verification_encumbrance, report_verification_district_office;
    TextView tv_market_valuation_remarks;
    Button btn_ownership_history, btn_ownership_source, btn_prev_appraisal;
    Button btn_update_page2;

    Button btn_submit;

    //case center attachments
    ImageView btn_mysql_attachments;
    String date_day = "", date_month = "", date_year = "", time = "";
    Button btn_manual_delete;

    // uploaded attachments
    LinearLayout container_attachments;
    String uploaded_attachment;
    String url_webby;
    UserFunctions userFunction = new UserFunctions();
    File uploaded_file;
    String[] commandArray = new String[]{"View Attachment in Browser",
            "Download Attachment"};
    // for API
    String xml;
    DefaultHttpClient httpClient = new MyHttpClient(this);
    boolean network_status;
    // Progress dialog type (0 - for Horizontal progress bar)
    public static final int progress_bar_type = 0;

    TextView tv_rework_reason_header, tv_rework_reason;
    String rework_reason;
    boolean withAttachment = false, freshData = true;
    //boolean fieldsComplete = false;
    boolean appStatReviewer = false;

    //detect internet connection
    TextView tv_connection;
    boolean wifi = false;
    boolean data = false;
    boolean data_speed = false;
    boolean freeFields;
    private NetworkChangeReceiver receiver;
    private boolean isConnected = false;
    boolean globalSave = false;

    String where = "WHERE record_id = args";
    String[] args = new String[1];

    public class NetworkChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            isNetworkAvailable(context);
        }

        private boolean isNetworkAvailable(final Context context) {
            ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null) {
                    for (int i = 0; i < info.length; i++) {
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            if (!isConnected) {
                                tv_connection.setText("Connected");
                                tv_connection.setBackgroundColor(Color.parseColor("#3399ff"));
                                isConnected = true;
                                // do your processing here ---
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        // Do something after 1s = 1000ms
                                        wifi = Connectivity.isConnectedWifi(context);
                                        data = Connectivity.isConnectedMobile(context);
                                        data_speed = Connectivity.isConnected(context);
                                        if (wifi) {
                                            tv_connection.setText(R.string.wifi_connected);
                                            tv_connection.setBackgroundColor(Color.parseColor("#39b476"));
                                        } else if (data) {
                                            if (data_speed) {
                                                tv_connection.setText(R.string.data_good);
                                                tv_connection.setBackgroundColor(Color.parseColor("#39b476"));
                                            } else if (data_speed == false) {
                                                tv_connection.setText(R.string.data_weak);
                                                tv_connection.setBackgroundColor(Color.parseColor("#FF9900"));
                                            }
                                        }
                                    }
                                }, 1000);

                            }
                            return true;
                        }
                    }
                }
            }
            tv_connection.setText("No Internet connection");
            tv_connection.setBackgroundColor(Color.parseColor("#CC0000"));
            isConnected = false;
            return false;
        }
    }
    //detect internet connection

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.motor_vehicle);
        st.resetDisconnectTimer();
        Authenticator.setDefault(new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("gdsuser", "gdsuser01".toCharArray());
            }
        });

        actionBar = getActionBar();
        actionBar.setIcon(R.drawable.ic_mv);
        actionBar.setSubtitle("Motor Vehicle");
        String open = "0";
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            open = bundle.getString("keyopen");
        }
        tv_connection = (TextView) findViewById(R.id.tv_connection);
        tv_connection.setVisibility(View.GONE);
        /*IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
		receiver = new NetworkChangeReceiver();
		registerReceiver(receiver, filter);*/

        gs = ((Global) getApplicationContext());
        container_attachments = (LinearLayout) findViewById(R.id.container_attachments);
        ll_contact = (LinearLayout) findViewById(R.id.container);
        ll_tct = (TableRow) findViewById(R.id.ll_tct);
        ll_address = (TableRow) findViewById(R.id.ll_address);
        ll_header_address = (TableRow) findViewById(R.id.ll_header_address);

        ll_vehicle_type = (TableRow) findViewById(R.id.ll_vehicle_type);
        ll_car_model = (TableRow) findViewById(R.id.ll_car_model);
        ll_car_loan_purpose = (TableRow) findViewById(R.id.ll_car_loan_purpose);
        ll_car_mileage = (TableRow) findViewById(R.id.ll_car_mileage);
        ll_car_series = (TableRow) findViewById(R.id.ll_car_series);
        ll_car_color = (TableRow) findViewById(R.id.ll_car_color);
        ll_car_plate_no = (TableRow) findViewById(R.id.ll_car_plate_no);
        ll_car_body_type = (TableRow) findViewById(R.id.ll_car_body_type);
        ll_car_displacement = (TableRow) findViewById(R.id.ll_car_displacement);
        ll_car_motor_no = (TableRow) findViewById(R.id.ll_car_motor_no);
        ll_car_chassis_no = (TableRow) findViewById(R.id.ll_car_chassis_no);
        ll_car_no_cylinders = (TableRow) findViewById(R.id.ll_car_no_cylinders);
        ll_car_cr_no = (TableRow) findViewById(R.id.ll_car_cr_no);
        ll_car_cr_date = (TableRow) findViewById(R.id.ll_car_cr_date);

        tv_requested_month = (TextView) findViewById(R.id.tv_requested_month);
        tv_requested_day = (TextView) findViewById(R.id.tv_requested_day);
        tv_requested_year = (TextView) findViewById(R.id.tv_requested_year);
        tv_classification = (TextView) findViewById(R.id.tv_classification);
        tv_account_name = (TextView) findViewById(R.id.tv_account_name);
        tv_company_name = (TextView) findViewById(R.id.tv_company_name);
        tv_requesting_party = (TextView) findViewById(R.id.tv_requesting_party);
        tv_requestor = (TextView) findViewById(R.id.tv_requestor);
        tv_control_no = (TextView) findViewById(R.id.tv_control_no);
        tv_appraisal_type = (TextView) findViewById(R.id.tv_appraisal_type);
        tv_nature_appraisal = (TextView) findViewById(R.id.tv_nature_appraisal);
        tv_kind_of_appraisal = (TextView) findViewById(R.id.tv_kind_of_appraisal);
        tv_ins_month1 = (TextView) findViewById(R.id.tv_ins_month1);
        tv_ins_day1 = (TextView) findViewById(R.id.tv_ins_day1);
        tv_ins_year1 = (TextView) findViewById(R.id.tv_ins_year1);
        tv_ins_month2 = (TextView) findViewById(R.id.tv_ins_month2);
        tv_ins_day2 = (TextView) findViewById(R.id.tv_ins_day2);
        tv_ins_year2 = (TextView) findViewById(R.id.tv_ins_year2);
        tv_com_month = (TextView) findViewById(R.id.tv_com_month);
        tv_com_day = (TextView) findViewById(R.id.tv_com_day);
        tv_com_year = (TextView) findViewById(R.id.tv_com_year);
        tv_tct_no = (TextView) findViewById(R.id.tv_tct_no);
        tv_unit_no = (TextView) findViewById(R.id.tv_unit_no);
        tv_bldg_name = (TextView) findViewById(R.id.tv_bldg_name);
        tv_street_no = (TextView) findViewById(R.id.tv_street_no);
        tv_street_name = (TextView) findViewById(R.id.tv_street_name);
        tv_village = (TextView) findViewById(R.id.tv_village);
        tv_district = (TextView) findViewById(R.id.tv_district);
        tv_zip_code = (TextView) findViewById(R.id.tv_zip_code);
        tv_city = (TextView) findViewById(R.id.tv_city);
        tv_province = (TextView) findViewById(R.id.tv_province);
        tv_region = (TextView) findViewById(R.id.tv_region);
        tv_country = (TextView) findViewById(R.id.tv_country);
        tv_address = (TextView) findViewById(R.id.tv_address);

        tv_vehicle_type = (TextView) findViewById(R.id.tv_vehicle_type);
        tv_car_model = (TextView) findViewById(R.id.tv_car_model);
        tv_car_loan_purpose = (TextView) findViewById(R.id.tv_car_loan_purpose);
        tv_car_mileage = (TextView) findViewById(R.id.tv_car_mileage);
        tv_car_series = (TextView) findViewById(R.id.tv_car_series);
        tv_car_color = (TextView) findViewById(R.id.tv_car_color);
        tv_car_plate_no = (TextView) findViewById(R.id.tv_car_plate_no);
        tv_car_body_type = (TextView) findViewById(R.id.tv_car_body_type);
        tv_car_displacement = (TextView) findViewById(R.id.tv_car_displacement);
        tv_car_motor_no = (TextView) findViewById(R.id.tv_car_motor_no);
        tv_car_chassis_no = (TextView) findViewById(R.id.tv_car_chassis_no);
        tv_car_no_cylinders = (TextView) findViewById(R.id.tv_car_no_cylinders);
        tv_car_cr_no = (TextView) findViewById(R.id.tv_car_cr_no);
        tv_car_cr_date = (TextView) findViewById(R.id.tv_car_cr_date);
        tv_app_request_remarks = (TextView) findViewById(R.id.tv_app_request_remarks);
        tv_app_kind_of_appraisal = (TextView) findViewById(R.id.tv_app_kind_of_appraisal);
        tv_app_branch_code = (TextView) findViewById(R.id.tv_app_branch_code);

        tv_attachment = (TextView) findViewById(R.id.tv_attachment);
        btn_report_page1 = (ImageView) findViewById(R.id.btn_report_page1);
        btn_report_page2 = (ImageView) findViewById(R.id.btn_report_page2);
        btn_report_page3 = (ImageView) findViewById(R.id.btn_report_page3);
        tv_report_page1 = (TextView) findViewById(R.id.tv_report_page1);
        tv_report_page3 = (TextView) findViewById(R.id.tv_report_page3);
        btn_report_update_cc = (Button) findViewById(R.id.btn_report_update_cc);
        btn_report_view_report = (Button) findViewById(R.id.btn_report_view_report);
        btn_report_pdf = (ImageView) findViewById(R.id.btn_report_pdf);
        btn_select_pdf = (ImageView) findViewById(R.id.btn_select_pdf);
        tableRowPage1 = (TableRow) findViewById(R.id.tableRowPage1);
        btn_report_page1.setOnClickListener(this);
        btn_report_page2.setOnClickListener(this);
        btn_report_page3.setOnClickListener(this);
        btn_report_update_cc.setOnClickListener(this);
        btn_report_view_report.setOnClickListener(this);
        btn_report_pdf.setOnClickListener(this);
        btn_select_pdf.setOnClickListener(this);
        tv_attachment.setOnClickListener(this);


        //car details
        btn_update_car_details = (Button) findViewById(R.id.btn_update_car_details);
        btn_update_car_details.setOnClickListener(this);
        //app details
        btn_update_app_details = (Button) findViewById(R.id.btn_update_app_details);
        btn_update_app_details.setOnClickListener(this);
        //case center attachments
        btn_mysql_attachments = (ImageView) findViewById(R.id.btn_mysql_attachments);
        btn_mysql_attachments.setOnClickListener(this);

        tv_rework_reason_header = (TextView) findViewById(R.id.tv_rework_reason_header);
        tv_rework_reason = (TextView) findViewById(R.id.tv_rework_reason);

        btn_manual_delete = (Button) findViewById(R.id.btn_manual_delete);
        btn_manual_delete.setOnClickListener(this);

        record_id = gs.record_id;
        fill_details();
        uploaded_attachments();
        statusDisplay();

        if (open.contentEquals("1")) {
            custom_dialog_page3();
        }
        kind_appraisal = gds.nullCheck3(db2.getRecord("kind_of_appraisal", "WHERE record_id = args", new String[]{record_id}, "tbl_report_accepted_jobs").get(0));
		/*if(!kind_appraisal.contentEquals("Actual Inspection")){
            report_place_inspected.setVisibility(View.GONE);
                    report_time_inspected.setVisibility(View.GONE);
            report_date_inspected_day.setVisibility(View.GONE);
                    report_date_inspected_month.setVisibility(View.GONE);
            report_date_inspected_year.setVisibility(View.GONE);
		}*/

        args[0] = record_id;
        uid = db2.getRecord("app_main_id", where, args, "tbl_report_accepted_jobs").get(0);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // TODO Auto-generated method stub
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        menu.findItem(R.id.logs).setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case R.id.logs:
                logsDisplay();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void fill_details() {
        args[0] = record_id;
        List<Report_Accepted_Jobs> report_accepted_jobs = db
                .getReport_Accepted_Jobs(record_id);
        if (!report_accepted_jobs.isEmpty()) {
            for (Report_Accepted_Jobs im : report_accepted_jobs) {
                requested_month = im.getdr_month();
                requested_day = im.getdr_day();
                requested_year = im.getdr_year();
                classification = im.getclassification();
                account_fname = im.getfname();
                account_mname = im.getmname();
                account_lname = im.getlname();
                account_is_company=db2.getRecord("app_account_is_company",where,args,"tbl_report_accepted_jobs").get(0);
                account_company_name=db2.getRecord("app_account_company_name",where,args,"tbl_report_accepted_jobs").get(0);

                requesting_party = im.getrequesting_party();
                requestor = im.getrequestor();
                control_no = im.getcontrol_no();
                appraisal_type = im.getappraisal_type();
                nature_appraisal = im.getnature_appraisal();
                kind_of_appraisal = im.getkind_of_appraisal();
                ins_month1 = im.getins_date1_month();
                ins_day1 = im.getins_date1_day();
                ins_year1 = im.getins_date1_year();
                ins_month2 = im.getins_date2_month();
                ins_day2 = im.getins_date2_day();
                ins_year2 = im.getins_date2_year();
                com_month = im.getcom_month();
                com_day = im.getcom_day();
                com_year = im.getcom_year();
                vehicle_type = im.getvehicle_type();
                car_model = im.getcar_model();
                tct_no = im.gettct_no();
                unit_no = im.getunit_no();
                bldg_name = im.getbuilding_name();
                street_no = im.getstreet_no();
                street_name = im.getstreet_name();
                village = im.getvillage();
                district = im.getdistrict();
                zip_code = im.getzip_code();
                city = im.getcity();
                province = im.getprovince();
                region = im.getregion();
                country = im.getcountry();
                counter = im.getcounter();

                car_loan_purpose = im.getcar_loan_purpose();
                car_mileage = im.getcar_mileage();
                car_series = im.getcar_series();
                car_color = im.getcar_color();
                car_plate_no = im.getcar_plate_no();
                car_body_type = im.getcar_body_type();
                car_displacement = im.getcar_displacement();
                car_motor_no = im.getcar_motor_no();
                car_chassis_no = im.getcar_chassis_no();
                car_no_cylinders = im.getcar_no_cylinders();
                car_cr_no = im.getcar_cr_no();
                car_cr_date_month = im.getcar_cr_date_month();
                car_cr_date_day = im.getcar_cr_date_day();
                car_cr_date_year = im.getcar_cr_date_year();
                app_request_remarks = im.getapp_request_remarks();
                app_kind_of_appraisal = im.getkind_of_appraisal();
                app_branch_code = im.getapp_branch_code();
                rework_reason = im.getrework_reason();

            }

            List<Report_Accepted_Jobs_Contacts> report_Accepted_Jobs_Contacts = db
                    .getReport_Accepted_Jobs_Contacts(record_id);
            if (!report_Accepted_Jobs_Contacts.isEmpty()) {
                for (Report_Accepted_Jobs_Contacts im : report_Accepted_Jobs_Contacts) {
                    LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
                            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View addView = layoutInflater.inflate(
                            R.layout.request_jobs_info_contact_layout, null);

                    final TextView tv_contact_person = (TextView) addView
                            .findViewById(R.id.tv_contact_person);
                    final TextView tv_contact_prefix = (TextView) addView
                            .findViewById(R.id.tv_contact_prefix);
                    final TextView tv_contact_mobile = (TextView) addView
                            .findViewById(R.id.tv_contact_mobile);
                    final TextView tv_contact_landline = (TextView) addView
                            .findViewById(R.id.tv_contact_landline);

                    tv_contact_person.setText(im.getcontact_person());
                    tv_contact_prefix.setText(im.getcontact_mobile_no_prefix());
                    tv_contact_mobile.setText(im.getcontact_mobile_no());
                    tv_contact_landline.setText(im.getcontact_landline());
                    ll_contact.addView(addView);
                }
            }
            List<Report_filename> rf = db.getReport_filename(record_id);
            if (!rf.isEmpty()) {
                for (Report_filename im : rf) {
                    tv_attachment.setText(im.getfilename());
                }
            }
            tv_requested_month.setText(requested_month + "/");
            tv_requested_day.setText(requested_day + "/");
            tv_requested_year.setText(requested_year);
            tv_classification.setText(classification);
            tv_account_name.setText(account_fname + " " + account_mname + " "
                    + account_lname);
            tv_company_name.setText(account_company_name);
            if(account_is_company.contentEquals("true")){
                actionBar.setTitle(account_company_name);
            }else{
                actionBar.setTitle(account_fname + " " + account_mname + " " + account_lname);
            }
            tv_requesting_party.setText(requesting_party);
            tv_requestor.setText(requestor);
            tv_control_no.setText(control_no);
            for (int x = 0; x < gs.appraisal_type_value.length; x++) {
                if (appraisal_type.equals(gs.appraisal_type_value[x])) {
                    output_appraisal_type = gs.appraisal_output[x];
                }
            }
            tv_appraisal_type.setText(output_appraisal_type);
            tv_nature_appraisal.setText(nature_appraisal);
            tv_kind_of_appraisal.setText(kind_of_appraisal);
            tv_ins_month1.setText(ins_month1 + "/");
            tv_ins_day1.setText(ins_day1 + "/");
            tv_ins_year1.setText(ins_year1);
            tv_ins_month2.setText(ins_month2 + "/");
            tv_ins_day2.setText(ins_day2 + "/");
            tv_ins_year2.setText(ins_year2);
            tv_com_month.setText(com_month + "/");
            tv_com_day.setText(com_day + "/");
            tv_com_year.setText(com_year);

            tv_vehicle_type.setText(vehicle_type);
            tv_car_model.setText(car_model);
            tv_car_loan_purpose.setText(car_loan_purpose);
            tv_car_mileage.setText(car_mileage);
            tv_car_series.setText(car_series);
            tv_car_color.setText(car_color);
            tv_car_plate_no.setText(car_plate_no);
            tv_car_body_type.setText(car_body_type);
            tv_car_displacement.setText(car_displacement);
            tv_car_motor_no.setText(car_motor_no);
            tv_car_chassis_no.setText(car_chassis_no);
            tv_car_no_cylinders.setText(car_no_cylinders);
            tv_car_cr_no.setText(car_cr_no);
            if(car_cr_date_month.contentEquals("")&&car_cr_date_day.contentEquals("")&&car_cr_date_year.contentEquals("")){
                tv_car_cr_date.setText("N/A");
            }else{
                tv_car_cr_date.setText(car_cr_date_month + "/" + car_cr_date_day + "/" + car_cr_date_year);
            }


            tv_tct_no.setText(tct_no);
            tv_unit_no.setText(unit_no);
            tv_bldg_name.setText(bldg_name);
            tv_street_no.setText(street_no);
            tv_street_name.setText(street_name);
            tv_village.setText(village);
            tv_district.setText(district);
            tv_zip_code.setText(zip_code);
            tv_city.setText(city);
            tv_province.setText(province);
            tv_region.setText(region);
            tv_country.setText(country);
            tv_app_request_remarks.setText(app_request_remarks);
            tv_app_kind_of_appraisal.setText(app_kind_of_appraisal);
            tv_app_branch_code.setText(app_branch_code);
            if (!street_name.equals("")) {
                output_street_name = street_name + ", ";
            } else {
                output_street_name = street_name;
            }
            if (!village.equals("")) {
                output_village = village + ", ";
            } else {
                output_village = village;
            }
            if (!city.equals("")) {
                output_city = city + ", ";
            } else {
                output_city = city;
            }
            if (!province.equals("")) {
                output_province = province + ", ";
            } else {
                output_province = province;
            }
            tv_address.setText(unit_no + " " + bldg_name + " " + street_no
                    + " " + output_street_name + output_village + district
                    + " " + output_city + region + " " + output_province
                    + country + " " + zip_code);

            ll_tct.setVisibility(View.GONE);
            ll_address.setVisibility(View.GONE);
            ll_header_address.setVisibility(View.GONE);

            ll_vehicle_type.setVisibility(View.VISIBLE);
            ll_car_model.setVisibility(View.VISIBLE);
            ll_car_loan_purpose.setVisibility(View.VISIBLE);
            ll_car_mileage.setVisibility(View.VISIBLE);
            ll_car_series.setVisibility(View.VISIBLE);
            ll_car_color.setVisibility(View.VISIBLE);
            ll_car_plate_no.setVisibility(View.VISIBLE);
            ll_car_body_type.setVisibility(View.VISIBLE);
            ll_car_displacement.setVisibility(View.VISIBLE);
            ll_car_motor_no.setVisibility(View.VISIBLE);
            ll_car_chassis_no.setVisibility(View.VISIBLE);
            ll_car_no_cylinders.setVisibility(View.VISIBLE);
            ll_car_cr_no.setVisibility(View.VISIBLE);
            ll_car_cr_date.setVisibility(View.VISIBLE);
            args[0]=record_id;
            if(db2.getRecord("application_status",where,args,"tbl_report_accepted_jobs").get(0).contentEquals("for_rework")){
                tv_rework_reason_header.setVisibility(View.VISIBLE);
                tv_rework_reason.setVisibility(View.VISIBLE);
                tv_rework_reason.setText(rework_reason);
            }

        }
    }

    public void custom_dialog_car_details() {
        myDialog = new Dialog(Motor_vehicle.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.car_details_dialog);
        myDialog.setCancelable(true);

        android.view.WindowManager.LayoutParams params = myDialog.getWindow()
                .getAttributes();
        params.height = LayoutParams.WRAP_CONTENT;
        params.width = LayoutParams.FILL_PARENT;
        myDialog.getWindow().setAttributes(
                (android.view.WindowManager.LayoutParams) params);
        et_vehicle_type = (AutoCompleteTextView) myDialog.findViewById(R.id.et_vehicle_type);
        et_car_model = (EditText) myDialog.findViewById(R.id.et_car_model);
        et_car_loan_purpose = (AutoCompleteTextView) myDialog.findViewById(R.id.et_car_loan_purpose);
        et_car_mileage = (EditText) myDialog.findViewById(R.id.et_car_mileage);
        et_car_series = (EditText) myDialog.findViewById(R.id.et_car_series);
        et_car_color = (EditText) myDialog.findViewById(R.id.et_car_color);
        et_car_plate_no = (EditText) myDialog.findViewById(R.id.et_car_plate_no);
        et_car_body_type = (AutoCompleteTextView) myDialog.findViewById(R.id.et_car_body_type);
        et_car_displacement = (EditText) myDialog.findViewById(R.id.et_car_displacement);
        et_car_motor_no = (EditText) myDialog.findViewById(R.id.et_car_motor_no);
        et_car_chassis_no = (EditText) myDialog.findViewById(R.id.et_car_chassis_no);
        et_car_no_cylinders = (EditText) myDialog.findViewById(R.id.et_car_no_cylinders);
        et_car_cr_no = (EditText) myDialog.findViewById(R.id.et_car_cr_no);
        report_car_cr_date = (DatePicker) myDialog.findViewById(R.id.report_car_cr_date);
        cb_car_cr_date = (CheckBox) myDialog.findViewById(R.id.cb_car_cr_date);

        btn_save_car_details = (Button) myDialog.findViewById(R.id.btn_right);
        btn_cancel_car_details = (Button) myDialog.findViewById(R.id.btn_left);
        gds.autocomplete(et_vehicle_type, getResources().getStringArray(R.array.make_brand), this);
        gds.autocomplete(et_car_loan_purpose, getResources().getStringArray(R.array.purpose_appraisal), this);
        gds.autocomplete(et_car_body_type, getResources().getStringArray(R.array.body_type), this);



        btn_save_car_details.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method
                //set click
                //winder



                    Report_Accepted_Jobs mv = new Report_Accepted_Jobs();
                    mv.setcar_loan_purpose(et_car_loan_purpose.getText().toString());
                    mv.setvehicle_type(et_vehicle_type.getText().toString());
                    mv.setcar_model(et_car_model.getText().toString());
                    mv.setcar_mileage(et_car_mileage.getText().toString());
                    mv.setcar_series(et_car_series.getText().toString());
                    mv.setcar_color(et_car_color.getText().toString());
                    mv.setcar_plate_no(et_car_plate_no.getText().toString());
                    mv.setcar_body_type(et_car_body_type.getText().toString());
                    mv.setcar_displacement(et_car_displacement.getText().toString());
                    mv.setcar_motor_no(et_car_motor_no.getText().toString());
                    mv.setcar_chassis_no(et_car_chassis_no.getText().toString());
                    mv.setcar_no_cylinders(et_car_no_cylinders.getText().toString());
                    mv.setcar_cr_no(et_car_cr_no.getText().toString());


                    //date
                    if(cb_car_cr_date.isChecked()){
                        mv.setcar_cr_date_month("");
                        mv.setcar_cr_date_day("");
                        mv.setcar_cr_date_year("");

                    }else{
                        if (String.valueOf(report_car_cr_date.getMonth()).length() == 1) { //if month is 1 digit add 0 to initial +1 to value
                            if (String.valueOf(report_car_cr_date.getMonth())
                                    .equals("9")) {//if october==9 just add 1
                                mv.setcar_cr_date_month(String
                                        .valueOf(report_car_cr_date.getMonth() + 1));
                            } else {
                                mv.setcar_cr_date_month("0"
                                        + String.valueOf(report_car_cr_date
                                        .getMonth() + 1));
                            }
                        } else {
                            mv.setcar_cr_date_month(String
                                    .valueOf(report_car_cr_date.getMonth() + 1));
                        }
                        mv.setcar_cr_date_day(String.valueOf(report_car_cr_date.getDayOfMonth()));
                        mv.setcar_cr_date_year(String.valueOf(report_car_cr_date.getYear()));

                    }

                    db.updateCar_Details(mv, record_id);
                    db.close();
                    Toast.makeText(getApplicationContext(), "Saved",
                            Toast.LENGTH_SHORT).show();
                    myDialog.dismiss();
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);



            }
        });

        btn_cancel_car_details.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method
                myDialog.dismiss();
            }
        });

        List<Report_Accepted_Jobs> report_accepted_jobs = db
                .getReport_Accepted_Jobs(record_id);
        if (!report_accepted_jobs.isEmpty()) {
            for (Report_Accepted_Jobs im : report_accepted_jobs) {
                et_vehicle_type.setText(im.getvehicle_type());
                et_car_model.setText(im.getcar_model());
                et_car_loan_purpose.setText(im.getcar_loan_purpose());
                et_car_mileage.setText(im.getcar_mileage());
                et_car_series.setText(im.getcar_series());
                et_car_color.setText(im.getcar_color());
                et_car_plate_no.setText(im.getcar_plate_no());
                et_car_body_type.setText(im.getcar_body_type());
                et_car_displacement.setText(im.getcar_displacement());
                et_car_motor_no.setText(im.getcar_motor_no());
                et_car_chassis_no.setText(im.getcar_chassis_no());
                et_car_no_cylinders.setText(im.getcar_no_cylinders());
                et_car_cr_no.setText(im.getcar_cr_no());
                // set current date into datepicker
                if ((!im.getcar_cr_date_year().equals("")) && (!im.getcar_cr_date_month().equals("")) && (!im.getcar_cr_date_day().equals(""))) {
                    report_car_cr_date.init(
                            Integer.parseInt(im.getcar_cr_date_year()),
                            Integer.parseInt(im.getcar_cr_date_month()) - 1,
                            Integer.parseInt(im.getcar_cr_date_day()),
                            null);
                    gds.cbDisplay(cb_car_cr_date, "false");
                } else if ((im.getcar_cr_date_year().equals("")) || (im.getcar_cr_date_month().equals("")) || (im.getcar_cr_date_day().equals("")) ||
                        (im.getcar_cr_date_year().equals("null")) || (im.getcar_cr_date_month().equals("null")) || (im.getcar_cr_date_day().equals("null"))) {
                    Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR);
                    int mMonth = c.get(Calendar.MONTH);
                    int mDay = c.get(Calendar.DAY_OF_MONTH);
                    report_car_cr_date.init(mYear, mMonth, mDay, null);

                    gds.cbDisplay(cb_car_cr_date, "true");


                }
            }
        }


                gds.fill_in_error(new EditText[]{
                        et_vehicle_type,et_car_model,et_car_mileage,et_car_series,et_car_color,et_car_plate_no,
                        et_car_body_type,et_car_displacement,et_car_motor_no,et_car_chassis_no,et_car_cr_no,et_car_no_cylinders});

        myDialog.show();
    }

    public void custom_dialog_app_details() {
        myDialog = new Dialog(Motor_vehicle.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.app_details_dialog);
        myDialog.setCancelable(true);

        android.view.WindowManager.LayoutParams params = myDialog.getWindow()
                .getAttributes();
        params.height = LayoutParams.WRAP_CONTENT;
        params.width = LayoutParams.FILL_PARENT;
        myDialog.getWindow().setAttributes(
                (android.view.WindowManager.LayoutParams) params);

        report_date_requested = (EditText) myDialog.findViewById(R.id.report_date_requested);
        et_first_name = (EditText) myDialog.findViewById(R.id.et_first_name);
        et_middle_name = (EditText) myDialog.findViewById(R.id.et_middle_name);
        et_last_name = (EditText) myDialog.findViewById(R.id.et_last_name);
        et_requesting_party = (AutoCompleteTextView) myDialog.findViewById(R.id.et_requesting_party);
        et_requestor = (EditText) myDialog.findViewById(R.id.et_requestor);
        app_account_company_name = (EditText) myDialog.findViewById(R.id.app_account_company_name);
        app_account_is_company = (CheckBox) myDialog.findViewById(R.id.app_account_is_company);
        spinner_purpose_appraisal = (Spinner) myDialog.findViewById(R.id.app_purpose_appraisal);
        et_app_kind_of_appraisal = (EditText) myDialog.findViewById(R.id.app_kind_of_appraisal);

        gds.autocomplete(et_requesting_party, getResources().getStringArray(R.array.requesting_party), this);

        if(app_account_is_company.isChecked()){
            app_account_company_name.setEnabled(true);
            et_first_name.setText("-");
            et_middle_name.setText("-");
            et_last_name.setText("-");

            et_first_name.setEnabled(false);
            et_middle_name.setEnabled(false);
            et_last_name.setEnabled(false);
        }else{
            app_account_company_name.setEnabled(false);
            app_account_company_name.setText("-");
            et_first_name.setEnabled(true);
            et_middle_name.setEnabled(true);
            et_last_name.setEnabled(true);
        }
        app_account_is_company.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    app_account_company_name.setEnabled(true);
                    et_first_name.setText("-");
                    et_middle_name.setText("-");
                    et_last_name.setText("-");
                    et_first_name.setEnabled(false);
                    et_middle_name.setEnabled(false);
                    et_last_name.setEnabled(false);
                } else {
                    app_account_company_name.setEnabled(false);
                    app_account_company_name.setText("-");
                    et_first_name.setEnabled(true);
                    et_middle_name.setEnabled(true);
                    et_last_name.setEnabled(true);
                }
            }
        });

        btn_save_app_details = (Button) myDialog.findViewById(R.id.btn_right);
        btn_cancel_app_details = (Button) myDialog.findViewById(R.id.btn_left);
        btn_save_app_details.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method
                boolean fieldsOK2;
                gds.fill_in_error(new EditText[]{
                        et_first_name,et_last_name});
                fieldsOK_page2 = gds.validate(new EditText[]{
                        et_first_name,et_last_name});

                if(app_account_is_company.isChecked()){
                    gds.fill_in_error(new EditText[]{
                            app_account_company_name});
                    fieldsOK2 = gds.validate(new EditText[]{
                            app_account_company_name});
                }else{
                    fieldsOK2=true;
                }
                if(fieldsOK_page2&&fieldsOK2) {
                    Report_Accepted_Jobs mv = new Report_Accepted_Jobs();


                    mv.setfname(et_first_name.getText().toString());
                    mv.setmname(et_middle_name.getText().toString());
                    mv.setlname(et_last_name.getText().toString());
                    mv.setrequesting_party(et_requesting_party.getText().toString());
                    mv.setrequestor(et_requestor.getText().toString());

                    db.updateApp_Details(mv, record_id);
                    field.clear();
                    field.add("app_account_is_company");
                    field.add("app_account_company_name");
                    field.add("nature_appraisal");
                    field.add("kind_of_appraisal");
                    value.clear();
                    value.add(gds.cbChecker(app_account_is_company));
                    value.add(app_account_company_name.getText().toString());
                    value.add(spinner_purpose_appraisal.getSelectedItem().toString());
                    value.add(et_app_kind_of_appraisal.getText().toString());
                    db2.updateRecord(value,field,"record_id = ?",new String[] { record_id },"tbl_report_accepted_jobs");
                    db.close();
                    Toast.makeText(getApplicationContext(), "Saved",
                            Toast.LENGTH_SHORT).show();
                    myDialog.dismiss();
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(),
                            "Please fill up required fields", Toast.LENGTH_SHORT).show();

                }
            }
        });

        btn_cancel_app_details.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method
                myDialog.dismiss();
            }
        });

        List<Report_Accepted_Jobs> report_accepted_jobs = db
                .getReport_Accepted_Jobs(record_id);
        if (!report_accepted_jobs.isEmpty()) {
            for (Report_Accepted_Jobs im : report_accepted_jobs) {
                et_first_name.setText(im.getfname());
                et_middle_name.setText(im.getmname());
                et_last_name.setText(im.getlname());
                et_requesting_party.setText(im.getrequesting_party());
                et_requestor.setText(im.getrequestor());
                gds.cbDisplay(app_account_is_company, db2.getRecord("app_account_is_company", where, args, "tbl_report_accepted_jobs").get(0));
                app_account_company_name.setText(db2.getRecord("app_account_company_name", where, args, "tbl_report_accepted_jobs").get(0));
                spinner_purpose_appraisal.setSelection(gds.spinnervalue(spinner_purpose_appraisal, db2.getRecord("nature_appraisal", where, args, "tbl_report_accepted_jobs").get(0)));
                et_app_kind_of_appraisal.setText(db2.getRecord("kind_of_appraisal", where, args, "tbl_report_accepted_jobs").get(0));

                // set current date into datepicker
                if ((!im.getdr_year().equals("")) && (!im.getdr_month().equals("")) && (!im.getdr_day().equals(""))) {
                    report_date_requested.setText(im.getdr_month() + "/" + im.getdr_day() + "/" + im.getdr_year());
                } else {
                    report_date_requested.setText("");
                }
            }
        }

        myDialog.show();
    }

    public void custom_dialog_page1() {
        myDialog = new Dialog(Motor_vehicle.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.reports_mv_page1);
        // myDialog.setTitle("My Dialog");
        //myDialog.setCancelable(true);

        android.view.WindowManager.LayoutParams params = myDialog.getWindow()
                .getAttributes();
        params.height = LayoutParams.FILL_PARENT;
        params.width = LayoutParams.FILL_PARENT;
        myDialog.getWindow().setAttributes(
                (android.view.WindowManager.LayoutParams) params);

        report_date_inspected_day = (EditText) myDialog.findViewById(R.id.report_date_inspected_day);
        report_date_inspected_month = (EditText) myDialog.findViewById(R.id.report_date_inspected_month);
        report_date_inspected_year = (EditText) myDialog.findViewById(R.id.report_date_inspected_year);
        report_time_inspected = (EditText) myDialog.findViewById(R.id.report_time_inspected);
        args[0] = record_id;
        if(db2.getRecord("application_status",where,args,"tbl_report_accepted_jobs").get(0).contentEquals("for_rework")){
            report_date_inspected_day.setEnabled(false);
            report_date_inspected_month.setEnabled(false);
            report_date_inspected_year.setEnabled(false);
            report_time_inspected.setEnabled(false);
        }else{
            report_date_inspected_day.setEnabled(true);
            report_date_inspected_month.setEnabled(true);
            report_date_inspected_year.setEnabled(true);
            report_time_inspected.setEnabled(true);
        }
        report_interior_dashboard = (Spinner) myDialog.findViewById(R.id.report_interior_dashboard);
        report_interior_sidings = (Spinner) myDialog.findViewById(R.id.report_interior_sidings);
        report_interior_seats = (Spinner) myDialog.findViewById(R.id.report_interior_seats);
        report_interior_windows = (Spinner) myDialog.findViewById(R.id.report_interior_windows);
        report_interior_ceiling = (Spinner) myDialog.findViewById(R.id.report_interior_ceiling);
        report_interior_instrument = (Spinner) myDialog.findViewById(R.id.report_interior_instrument);
        report_bodytype_grille = (Spinner) myDialog.findViewById(R.id.report_bodytype_grille);
        report_bodytype_headlight = (Spinner) myDialog.findViewById(R.id.report_bodytype_headlight);
        report_bodytype_fbumper = (Spinner) myDialog.findViewById(R.id.report_bodytype_fbumper);
        report_bodytype_hood = (Spinner) myDialog.findViewById(R.id.report_bodytype_hood);
        report_bodytype_rf_fender = (Spinner) myDialog.findViewById(R.id.report_bodytype_rf_fender);
        report_bodytype_rf_door = (Spinner) myDialog.findViewById(R.id.report_bodytype_rf_door);
        report_bodytype_rr_door = (Spinner) myDialog.findViewById(R.id.report_bodytype_rr_door);
        report_bodytype_rr_fender = (Spinner) myDialog.findViewById(R.id.report_bodytype_rr_fender);
        report_bodytype_backdoor = (Spinner) myDialog.findViewById(R.id.report_bodytype_backdoor);
        report_bodytype_taillight = (Spinner) myDialog.findViewById(R.id.report_bodytype_taillight);
        report_bodytype_r_bumper = (Spinner) myDialog.findViewById(R.id.report_bodytype_r_bumper);
        report_bodytype_lr_fender = (Spinner) myDialog.findViewById(R.id.report_bodytype_lr_fender);
        report_bodytype_lr_door = (Spinner) myDialog.findViewById(R.id.report_bodytype_lr_door);
        report_bodytype_lf_door = (Spinner) myDialog.findViewById(R.id.report_bodytype_lf_door);
        report_bodytype_lf_fender = (Spinner) myDialog.findViewById(R.id.report_bodytype_lf_fender);
        report_bodytype_top = (Spinner) myDialog.findViewById(R.id.report_bodytype_top);
        report_bodytype_paint = (Spinner) myDialog.findViewById(R.id.report_bodytype_paint);
        report_bodytype_flooring = (Spinner) myDialog.findViewById(R.id.report_bodytype_flooring);
        valrep_mv_bodytype_trunk = (Spinner) myDialog.findViewById(R.id.valrep_mv_bodytype_trunk);

        report_misc_stereo = (AutoCompleteTextView) myDialog.findViewById(R.id.report_misc_stereo);
        report_misc_tools = (AutoCompleteTextView) myDialog.findViewById(R.id.report_misc_tools);
        report_misc_speakers = (AutoCompleteTextView) myDialog.findViewById(R.id.report_misc_speakers);
        report_misc_airbag = (AutoCompleteTextView) myDialog.findViewById(R.id.report_misc_airbag);
        report_misc_tires = (AutoCompleteTextView) myDialog.findViewById(R.id.report_misc_tires);
        report_misc_mag_wheels = (AutoCompleteTextView) myDialog.findViewById(R.id.report_misc_mag_wheels);
        report_misc_carpet = (AutoCompleteTextView) myDialog.findViewById(R.id.report_misc_carpet);
        report_misc_others = (AutoCompleteTextView) myDialog.findViewById(R.id.report_misc_others);
        report_enginearea_transmission1 = (AutoCompleteTextView) myDialog.findViewById(R.id.report_enginearea_transmission1);
        report_enginearea_transmission2 = (AutoCompleteTextView) myDialog.findViewById(R.id.report_enginearea_transmission2);

        report_enginearea_fuel = (Spinner) myDialog.findViewById(R.id.report_enginearea_fuel);
        report_enginearea_chassis = (Spinner) myDialog.findViewById(R.id.report_enginearea_chassis);
        report_enginearea_battery = (Spinner) myDialog.findViewById(R.id.report_enginearea_battery);
        report_enginearea_electrical = (Spinner) myDialog.findViewById(R.id.report_enginearea_electrical);
        report_enginearea_engine = (Spinner) myDialog.findViewById(R.id.report_enginearea_engine);

        gds.autocomplete(report_enginearea_transmission1, getResources().getStringArray(R.array.transmission), this);
        gds.autocomplete(report_enginearea_transmission2, getResources().getStringArray(R.array.transmission), this);
        gds.autocomplete(report_misc_stereo, getResources().getStringArray(R.array.factory), this);
        gds.autocomplete(report_misc_airbag, getResources().getStringArray(R.array.factory), this);
        gds.autocomplete(report_misc_tools, getResources().getStringArray(R.array.basic), this);
        gds.autocomplete(report_misc_speakers, getResources().getStringArray(R.array.factory), this);
        gds.autocomplete(report_misc_tires, getResources().getStringArray(R.array.tires), this);
        gds.autocomplete(report_misc_mag_wheels, getResources().getStringArray(R.array.factory), this);
        gds.autocomplete(report_misc_carpet, getResources().getStringArray(R.array.factory), this);
        gds.autocomplete(report_misc_others, getResources().getStringArray(R.array.others), this);


        btn_update_page1 = (Button) myDialog.findViewById(R.id.btn_update_page1);
        btn_update_page1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                    Motor_Vehicle_API mv = new Motor_Vehicle_API();

                    mv.setreport_interior_dashboard(report_interior_dashboard.getSelectedItem().toString());
                    mv.setreport_interior_sidings(report_interior_sidings.getSelectedItem().toString());
                    mv.setreport_interior_seats(report_interior_seats.getSelectedItem().toString());
                    mv.setreport_interior_windows(report_interior_windows.getSelectedItem().toString());
                    mv.setreport_interior_ceiling(report_interior_ceiling.getSelectedItem().toString());
                    mv.setreport_interior_instrument(report_interior_instrument.getSelectedItem().toString());
                    mv.setreport_bodytype_grille(report_bodytype_grille.getSelectedItem().toString());
                    mv.setreport_bodytype_headlight(report_bodytype_headlight.getSelectedItem().toString());
                    mv.setreport_bodytype_fbumper(report_bodytype_fbumper.getSelectedItem().toString());
                    mv.setreport_bodytype_hood(report_bodytype_hood.getSelectedItem().toString());
                    mv.setreport_bodytype_rf_fender(report_bodytype_rf_fender.getSelectedItem().toString());
                    mv.setreport_bodytype_rf_door(report_bodytype_rf_door.getSelectedItem().toString());
                    mv.setreport_bodytype_rr_door(report_bodytype_rr_door.getSelectedItem().toString());
                    mv.setreport_bodytype_rr_fender(report_bodytype_rr_fender.getSelectedItem().toString());
                    mv.setreport_bodytype_backdoor(report_bodytype_backdoor.getSelectedItem().toString());
                    mv.setreport_bodytype_taillight(report_bodytype_taillight.getSelectedItem().toString());
                    mv.setreport_bodytype_r_bumper(report_bodytype_r_bumper.getSelectedItem().toString());
                    mv.setreport_bodytype_lr_fender(report_bodytype_lr_fender.getSelectedItem().toString());
                    mv.setreport_bodytype_lr_door(report_bodytype_lr_door.getSelectedItem().toString());
                    mv.setreport_bodytype_lf_door(report_bodytype_lf_door.getSelectedItem().toString());
                    mv.setreport_bodytype_lf_fender(report_bodytype_lf_fender.getSelectedItem().toString());
                    mv.setreport_bodytype_top(report_bodytype_top.getSelectedItem().toString());
                    mv.setreport_bodytype_paint(report_bodytype_paint.getSelectedItem().toString());
                    mv.setreport_bodytype_flooring(report_bodytype_flooring.getSelectedItem().toString());

                    mv.setreport_misc_stereo(report_misc_stereo.getText().toString());
                    mv.setreport_misc_tools(report_misc_tools.getText().toString());
                    mv.setreport_misc_speakers(report_misc_speakers.getText().toString());
                    mv.setreport_misc_airbag(report_misc_airbag.getText().toString());
                    mv.setreport_misc_tires(report_misc_tires.getText().toString());
                    mv.setreport_misc_mag_wheels(report_misc_mag_wheels.getText().toString());
                    mv.setreport_misc_carpet(report_misc_carpet.getText().toString());
                    mv.setreport_misc_others(report_misc_others.getText().toString());

                    mv.setreport_enginearea_fuel(report_enginearea_fuel.getSelectedItem().toString());
                    mv.setreport_enginearea_transmission1(report_enginearea_transmission1.getText().toString());
                    mv.setreport_enginearea_transmission2(report_enginearea_transmission2.getText().toString());
                    mv.setreport_enginearea_chassis(report_enginearea_chassis.getSelectedItem().toString());
                    mv.setreport_enginearea_battery(report_enginearea_battery.getSelectedItem().toString());
                    mv.setreport_enginearea_electrical(report_enginearea_electrical.getSelectedItem().toString());
                    mv.setreport_enginearea_engine(report_enginearea_engine.getSelectedItem().toString());


                    //date
                    mv.setreport_date_inspected_day(report_date_inspected_day.getText().toString());

                    mv.setreport_date_inspected_year(report_date_inspected_year.getText().toString());
                    mv.setreport_date_inspected_month(report_date_inspected_month.getText().toString());

                    mv.setreport_time_inspected(report_time_inspected.getText().toString());
                    mv.setvalrep_mv_bodytype_trunk(valrep_mv_bodytype_trunk.getSelectedItem().toString());

                    db.updateMotor_Vehicle_page1(mv, record_id);
                    db.close();
                    Toast.makeText(getApplicationContext(), "Saved",
                            Toast.LENGTH_SHORT).show();

                    myDialog.dismiss();
                Intent intent = getIntent();
                finish();
                startActivity(intent);

            }
        });


        if (!db2.getRecord("report_interior_dashboard", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_interior_dashboard.setSelection(gds.spinnervalue(report_interior_dashboard, db2.getRecord("report_interior_dashboard", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_interior_sidings", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_interior_sidings.setSelection(gds.spinnervalue(report_interior_sidings, db2.getRecord("report_interior_sidings", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_interior_seats", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_interior_seats.setSelection(gds.spinnervalue(report_interior_seats, db2.getRecord("report_interior_seats", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_interior_windows", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_interior_windows.setSelection(gds.spinnervalue(report_interior_windows, db2.getRecord("report_interior_windows", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_interior_ceiling", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_interior_ceiling.setSelection(gds.spinnervalue(report_interior_ceiling, db2.getRecord("report_interior_ceiling", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_interior_instrument", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_interior_instrument.setSelection(gds.spinnervalue(report_interior_instrument, db2.getRecord("report_interior_instrument", where, args, "motor_vehicle").get(0)));
        }

        if (!db2.getRecord("report_bodytype_grille", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_bodytype_grille.setSelection(gds.spinnervalue(report_bodytype_grille, db2.getRecord("report_bodytype_grille", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_bodytype_headlight", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_bodytype_headlight.setSelection(gds.spinnervalue(report_bodytype_headlight, db2.getRecord("report_bodytype_headlight", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_bodytype_fbumper", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_bodytype_fbumper.setSelection(gds.spinnervalue(report_bodytype_fbumper, db2.getRecord("report_bodytype_fbumper", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_bodytype_hood", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_bodytype_hood.setSelection(gds.spinnervalue(report_bodytype_hood, db2.getRecord("report_bodytype_hood", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_bodytype_rf_fender", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_bodytype_rf_fender.setSelection(gds.spinnervalue(report_bodytype_rf_fender, db2.getRecord("report_bodytype_rf_fender", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_bodytype_rf_door", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_bodytype_rf_door.setSelection(gds.spinnervalue(report_bodytype_rf_door, db2.getRecord("report_bodytype_rf_door", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_bodytype_rr_door", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_bodytype_rr_door.setSelection(gds.spinnervalue(report_bodytype_rr_door, db2.getRecord("report_bodytype_rr_door", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_bodytype_rr_fender", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_bodytype_rr_fender.setSelection(gds.spinnervalue(report_bodytype_rr_fender, db2.getRecord("report_bodytype_rr_fender", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_bodytype_backdoor", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_bodytype_backdoor.setSelection(gds.spinnervalue(report_bodytype_backdoor, db2.getRecord("report_bodytype_backdoor", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_bodytype_taillight", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_bodytype_taillight.setSelection(gds.spinnervalue(report_bodytype_taillight, db2.getRecord("report_bodytype_taillight", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_bodytype_r_bumper", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_bodytype_r_bumper.setSelection(gds.spinnervalue(report_bodytype_r_bumper, db2.getRecord("report_bodytype_r_bumper", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_bodytype_lr_fender", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_bodytype_lr_fender.setSelection(gds.spinnervalue(report_bodytype_lr_fender, db2.getRecord("report_bodytype_lr_fender", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_bodytype_lr_door", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_bodytype_lr_door.setSelection(gds.spinnervalue(report_bodytype_lr_door, db2.getRecord("report_bodytype_lr_door", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_bodytype_lf_door", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_bodytype_lf_door.setSelection(gds.spinnervalue(report_bodytype_lf_door, db2.getRecord("report_bodytype_lf_door", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_bodytype_lf_fender", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_bodytype_lf_fender.setSelection(gds.spinnervalue(report_bodytype_lf_fender, db2.getRecord("report_bodytype_lf_fender", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_bodytype_top", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_bodytype_top.setSelection(gds.spinnervalue(report_bodytype_top, db2.getRecord("report_bodytype_top", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_bodytype_paint", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_bodytype_paint.setSelection(gds.spinnervalue(report_bodytype_paint, db2.getRecord("report_bodytype_paint", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_bodytype_flooring", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_bodytype_flooring.setSelection(gds.spinnervalue(report_bodytype_flooring, db2.getRecord("report_bodytype_flooring", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("valrep_mv_bodytype_trunk", where, args, "motor_vehicle").get(0).contentEquals("")) {
            valrep_mv_bodytype_trunk.setSelection(gds.spinnervalue(valrep_mv_bodytype_trunk, db2.getRecord("valrep_mv_bodytype_trunk", where, args, "motor_vehicle").get(0)));
        }

        if (!db2.getRecord("report_misc_stereo", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_misc_stereo.setText(db2.getRecord("report_misc_stereo", where, args, "motor_vehicle").get(0));
        }
        if (!db2.getRecord("report_misc_tools", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_misc_tools.setText(db2.getRecord("report_misc_tools", where, args, "motor_vehicle").get(0));
        }
        if (!db2.getRecord("report_misc_speakers", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_misc_speakers.setText(db2.getRecord("report_misc_speakers", where, args, "motor_vehicle").get(0));
        }
        if (!db2.getRecord("report_misc_airbag", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_misc_airbag.setText(db2.getRecord("report_misc_airbag", where, args, "motor_vehicle").get(0));
        }
        if (!db2.getRecord("report_misc_tires", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_misc_tires.setText(db2.getRecord("report_misc_tires", where, args, "motor_vehicle").get(0));
        }
        if (!db2.getRecord("report_misc_mag_wheels", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_misc_mag_wheels.setText(db2.getRecord("report_misc_mag_wheels", where, args, "motor_vehicle").get(0));
        }
        if (!db2.getRecord("report_misc_carpet", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_misc_carpet.setText(db2.getRecord("report_misc_carpet", where, args, "motor_vehicle").get(0));
        }
        if (!db2.getRecord("report_misc_others", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_misc_others.setText(db2.getRecord("report_misc_others", where, args, "motor_vehicle").get(0));
        }

        if (!db2.getRecord("report_enginearea_fuel", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_enginearea_fuel.setSelection(gds.spinnervalue(report_enginearea_fuel, db2.getRecord("report_enginearea_fuel", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_enginearea_transmission1", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_enginearea_transmission1.setText(db2.getRecord("report_enginearea_transmission1", where, args, "motor_vehicle").get(0));
        }
        if (!db2.getRecord("report_enginearea_transmission2", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_enginearea_transmission2.setText(db2.getRecord("report_enginearea_transmission2", where, args, "motor_vehicle").get(0));
        }

        if (!db2.getRecord("report_enginearea_chassis", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_enginearea_chassis.setSelection(gds.spinnervalue(report_enginearea_chassis, db2.getRecord("report_enginearea_chassis", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_enginearea_battery", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_enginearea_battery.setSelection(gds.spinnervalue(report_enginearea_battery, db2.getRecord("report_enginearea_battery", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_enginearea_electrical", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_enginearea_electrical.setSelection(gds.spinnervalue(report_enginearea_electrical, db2.getRecord("report_enginearea_electrical", where, args, "motor_vehicle").get(0)));
        }
        if (!db2.getRecord("report_enginearea_engine", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_enginearea_engine.setSelection(gds.spinnervalue(report_enginearea_engine, db2.getRecord("report_enginearea_engine", where, args, "motor_vehicle").get(0)));
        }

        if (!db2.getRecord("report_time_inspected", where, args, "motor_vehicle").get(0).contentEquals("")) {
            report_time_inspected.setText(db2.getRecord("report_time_inspected", where, args, "motor_vehicle").get(0));
        }

        if (!db2.getRecord("report_date_inspected_month", where, args, "motor_vehicle").get(0).contentEquals("")
                && !db2.getRecord("report_date_inspected_day", where, args, "motor_vehicle").get(0).contentEquals("")
                && !db2.getRecord("report_date_inspected_year", where, args, "motor_vehicle").get(0).contentEquals("")) {
            date_month = gds.nullCheck(db2.getRecord("report_date_inspected_month", where, args, "motor_vehicle").get(0));
            date_day = gds.nullCheck(db2.getRecord("report_date_inspected_day", where, args, "motor_vehicle").get(0));
            date_year = gds.nullCheck(db2.getRecord("report_date_inspected_year", where, args, "motor_vehicle").get(0));

            report_date_inspected_day.setText(date_day);
            report_date_inspected_month.setText(date_month);
            report_date_inspected_year.setText(date_year);

        }
                gds.fill_in_error_spinner(new Spinner[]{report_interior_dashboard,
                        report_interior_sidings,
                        report_interior_seats,
                        report_interior_windows,
                        report_interior_ceiling,
                        report_interior_instrument,
                        report_bodytype_grille,
                        report_bodytype_headlight,
                        report_bodytype_fbumper,
                        report_bodytype_hood,
                        report_bodytype_rf_fender,
                        report_bodytype_rf_door,
                        report_bodytype_rr_door,
                        report_bodytype_rr_fender,
                        report_bodytype_backdoor,
                        report_bodytype_taillight,
                        report_bodytype_r_bumper,
                        report_bodytype_lr_fender,
                        report_bodytype_lr_door,
                        report_bodytype_lf_door,
                        report_bodytype_lf_fender,
                        report_bodytype_top,
                        report_bodytype_paint,
                        report_bodytype_flooring,
                        valrep_mv_bodytype_trunk,
                        report_enginearea_fuel,
                        report_enginearea_chassis,
                        report_enginearea_battery,
                        report_enginearea_electrical,
                        report_enginearea_engine});

                gds.fill_in_error(new EditText[]{report_time_inspected, report_date_inspected_day,
                        report_date_inspected_month,
                        report_date_inspected_year,
                        report_misc_stereo,
                        report_misc_tools,
                        report_misc_speakers,
                        report_misc_airbag,
                        report_misc_tires,
                        report_misc_mag_wheels,
                        report_misc_carpet,
                        report_misc_others,
                        report_enginearea_transmission1});



        myDialog.show();
    }

    public void custom_dialog_page3() {
        myDialog = new Dialog(Motor_vehicle.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.reports_mv_page2);
        //myDialog.setCancelable(true);

        android.view.WindowManager.LayoutParams params = myDialog.getWindow()
                .getAttributes();
        params.height = LayoutParams.FILL_PARENT;
        params.width = LayoutParams.FILL_PARENT;
        myDialog.getWindow().setAttributes(
                (android.view.WindowManager.LayoutParams) params);

        report_verification = (Spinner) myDialog.findViewById(R.id.report_verification);
        report_verification_district_office = (AutoCompleteTextView) myDialog.findViewById(R.id.report_verification_district_office);
        report_verification_encumbrance = (AutoCompleteTextView) myDialog.findViewById(R.id.report_verification_encumbrance);

        gds.autocomplete(report_verification_encumbrance, getResources().getStringArray(R.array.encumbrance), this);
        gds.autocomplete(report_verification_district_office, getResources().getStringArray(R.array.district_office), this);

        report_verification_registered_owner = (EditText) myDialog.findViewById(R.id.report_verification_registered_owner);
        tv_report_verification_registered_owner = (TextView) myDialog.findViewById(R.id.tv_report_verification_registered_owner);
        report_verification_file_no = (EditText) myDialog.findViewById(R.id.report_verification_file_no);
        report_place_inspected = (AutoCompleteTextView) myDialog.findViewById(R.id.report_place_inspected);
        report_date_first_registered = (DatePicker) myDialog.findViewById(R.id.report_date_first_registered);
        cb_date_first_registered = (CheckBox) myDialog.findViewById(R.id.cb_date_first_registered);

        gds.autocomplete(report_place_inspected, getResources().getStringArray(R.array.place_inspected), this);

        report_market_valuation_fair_value = (EditText) myDialog.findViewById(R.id.report_market_valuation_fair_value);
        report_market_valuation_min = (EditText) myDialog.findViewById(R.id.report_market_valuation_min);
        report_market_valuation_max = (EditText) myDialog.findViewById(R.id.report_market_valuation_max);

        report_market_valuation_remarks = (EditText) myDialog.findViewById(R.id.report_market_valuation_remarks);
        valrep_mv_market_valuation_previous_remarks = (EditText) myDialog.findViewById(R.id.valrep_mv_market_valuation_previous_remarks);
        tv_market_valuation_remarks = (TextView) myDialog.findViewById(R.id.tv_market_valuation_remarks);
        tv_market_valuation_remarks.setOnClickListener(this);

        //ADDED By IAN
        btn_prev_appraisal = (Button) myDialog.findViewById(R.id.btn_prev_appraisal);

        btn_ownership_history = (Button) myDialog.findViewById(R.id.btn_ownership_history);
        btn_ownership_source = (Button) myDialog.findViewById(R.id.btn_ownership_source);
        btn_update_page2 = (Button) myDialog.findViewById(R.id.btn_update_page2);

        kind_appraisal = gds.nullCheck3(db2.getRecord("kind_of_appraisal", "WHERE record_id = args", new String[]{record_id}, "tbl_report_accepted_jobs").get(0));
        has_prev = gds.nullCheck2(db2.getRecord("id", "WHERE record_id = args", new String[]{record_id}, "motor_vehicle_previous_appraisal").get(0));
        nature = gds.nullCheck3(db2.getRecord("nature_appraisal","WHERE record_id = args", new String[]{record_id}, "tbl_report_accepted_jobs").get(0));
        Log.e("has_prev",""+has_prev);


        report_market_valuation_fair_value.addTextChangedListener(new TextWatcher() {

            String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(report_market_valuation_fair_value, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });

        btn_prev_appraisal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                btn_update_page2.performClick();//auto-save

                    Intent in = new Intent(getApplicationContext(), Motor_Vehicle_Prev_Appraisal.class);
                    in.putExtra(TAG_RECORD_ID, record_id);
                    startActivityForResult(in, 100);

            }
        });
        btn_ownership_history.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                btn_update_page2.performClick();//auto-save

                    Intent in = new Intent(getApplicationContext(), Motor_Vehicle_Ownership_History.class);
                    in.putExtra(TAG_RECORD_ID, record_id);
                    startActivityForResult(in, 100);

            }
        });

        btn_ownership_source.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                btn_update_page2.performClick();//auto-save

                    Intent in = new Intent(getApplicationContext(), Motor_Vehicle_Ownership_Source.class);
                    in.putExtra(TAG_RECORD_ID, record_id);
                    startActivityForResult(in, 100);
                    myDialog.dismiss();

            }
        });


        btn_update_page2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub


                    Motor_Vehicle_API mv = new Motor_Vehicle_API();
                    mv.setreport_verification(report_verification.getSelectedItem().toString());
                    mv.setreport_verification_district_office(report_verification_district_office.getText().toString());
                    mv.setreport_verification_encumbrance(report_verification_encumbrance.getText().toString());
                    mv.setreport_verification_file_no(report_verification_file_no.getText().toString());
                    mv.setreport_verification_registered_owner(report_verification_registered_owner.getText().toString());
                    mv.setreport_place_inspected(report_place_inspected.getText().toString());
                    mv.setreport_market_valuation_min(gds.replaceFormat(report_market_valuation_min.getText().toString()));
                    mv.setreport_market_valuation_max(gds.replaceFormat(report_market_valuation_max.getText().toString()));
                    mv.setreport_market_valuation_fair_value(gds.replaceFormat(report_market_valuation_fair_value.getText().toString()));
                    mv.setreport_market_valuation_remarks(report_market_valuation_remarks.getText().toString());

                    //date
                    if(cb_date_first_registered.isChecked()){
                        mv.setreport_verification_first_reg_date_day("");
                        mv.setreport_verification_first_reg_date_month("");
                        mv.setreport_verification_first_reg_date_year("");
                    }else{
                        mv.setreport_verification_first_reg_date_day(String.valueOf(report_date_first_registered.getDayOfMonth()));
                        if (String.valueOf(report_date_first_registered.getMonth()).length() == 1) {//if month is 1 digit add 0 to initial +1 to value
                            if (String.valueOf(report_date_first_registered.getMonth())
                                    .equals("9")) {//if october==9 just add 1
                                mv.setreport_verification_first_reg_date_month(String
                                        .valueOf(report_date_first_registered.getMonth() + 1));
                            } else {
                                mv.setreport_verification_first_reg_date_month("0"
                                        + String.valueOf(report_date_first_registered
                                        .getMonth() + 1));
                            }
                        } else {
                            mv.setreport_verification_first_reg_date_month(String
                                    .valueOf(report_date_first_registered.getMonth() + 1));
                        }
                        mv.setreport_verification_first_reg_date_year(String.valueOf(report_date_first_registered.getYear()));

                    }
                    mv.setvalrep_mv_market_valuation_previous_remarks(valrep_mv_market_valuation_previous_remarks.getText().toString());


                    //ADDED By IAN

                    db.updateMotor_Vehicle_page2(mv, record_id);
                    db.close();
                    Toast.makeText(getApplicationContext(), "Saved",
                            Toast.LENGTH_SHORT).show();
                    myDialog.dismiss();
                Intent intent = getIntent();
                finish();
                startActivity(intent);


            }
        });
        List<Motor_Vehicle_API> mv = db.getMotor_Vehicle(record_id);
        if (!mv.isEmpty()) {
            for (Motor_Vehicle_API im : mv) {
                if(!im.getreport_verification().toString().contentEquals("")) {
                    report_verification.setSelection(gds.spinnervalue(report_verification, im.getreport_verification().toString()));
                }
              /*  if (im.getreport_verification().equals("VERIFIED")) {
                    report_verification.setSelection(0);
                } else if (im.getreport_verification().equals("NOT_VERIFIED")) {
                    report_verification.setSelection(1);
                }*/

                if (im.getreport_verification_district_office().toString().equals("")) {
                    report_verification_district_office.setText("");
                } else {
                    report_verification_district_office.setText(im.getreport_verification_district_office().toString());
                }


                report_verification_encumbrance.setText(gds.nullCheck3(im.getreport_verification_encumbrance().toString()));


                report_verification_registered_owner.setText(im.getreport_verification_registered_owner().toString());
                report_place_inspected.setText(im.getreport_place_inspected().toString());

                report_market_valuation_min.setText(gds.numberFormat(im.getreport_market_valuation_min().toString()));
                report_market_valuation_max.setText(gds.numberFormat(im.getreport_market_valuation_max().toString()));
                report_market_valuation_fair_value.setText(gds.numberFormat(im.getreport_market_valuation_fair_value().toString()));

                if (im.getreport_verification_file_no().toString().equals("")) {
                    report_verification_file_no.setText("");
                } else {
                    report_verification_file_no.setText(im.getreport_verification_file_no().toString());
                }
                String where = "WHERE record_id = args";
                String[] args = new String[1];
                args[0]=record_id;

                report_market_valuation_remarks.setText(gds.nullChecker(db2.getRecord("report_market_valuation_remarks", where, args, "motor_vehicle").get(0)));
                valrep_mv_market_valuation_previous_remarks.setText(gds.nullChecker(db2.getRecord("valrep_mv_market_valuation_previous_remarks", where, args, "motor_vehicle").get(0)));


                //date of inspection
                // set current date into datepicker
                if ((!im.getreport_verification_first_reg_date_year().equals("")) || (!im.getreport_verification_first_reg_date_month().equals(""))) {
                    report_date_first_registered.init(
                            Integer.parseInt(im.getreport_verification_first_reg_date_year()),
                            Integer.parseInt(im.getreport_verification_first_reg_date_month()) - 1,
                            Integer.parseInt(im.getreport_verification_first_reg_date_day()),
                            null);
                    gds.cbDisplay(cb_date_first_registered,"false");
                }else{
                    gds.cbDisplay(cb_date_first_registered,"true");
                }

                //ADDED By IAN


                myDialog.show();
            }
        }


         kind_appraisal = new DatabaseHandler_New(context).getRecord("kind_of_appraisal", Tables.tbl_report_accepted_jobs.table_name, "record_id", record_id);
        if(kind_appraisal.contentEquals("Table Appraisal")) {
            gds.fill_in_error(new EditText[]{report_place_inspected,report_market_valuation_remarks,valrep_mv_market_valuation_previous_remarks});
        }else{
            gds.fill_in_error(new EditText[]{report_place_inspected,report_market_valuation_remarks});
        }

        myDialog.show();
    }


    private class SendData extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pDialog = new ProgressDialog(Motor_vehicle.this);
            pDialog.setMessage("Updating data..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            NetworkUtil.getConnectivityStatusString(Motor_vehicle.this);
            if (!NetworkUtil.status.equals("Network not available")) {
                gs.online = true;
                google_response = gds.google_ping();
                if (google_response.equals("200")) {

                    gs.connectedToServer = true;
                    boolean internetSpeedFast = Connectivity.isConnectedFast(getBaseContext());
                    if (internetSpeedFast == true) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                gs.fastInternet = true;
                                Toast toast = Toast.makeText(getBaseContext(), "Connected", Toast.LENGTH_LONG);
                                View view = toast.getView();
                                view.setBackgroundResource(R.color.toast_color);
                                toast.show();
                            }
                        });


                        //wwww


                            if (((withAttachment == true) && (freshData == true)) ||
                                    ((withAttachment == true) && (freshData == false))) {
                                upload_attachment();
                            } else if ((withAttachment == false) && (freshData == false)) {
                                globalSave = false;
                                create_json();
//							db_delete_all();//temp disabled
                                db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, true, gs.sent);
                                finish();
                            }
                       /* }else {
                            Toast.makeText(getApplicationContext(),
                                    "Please fill up required fields", Toast.LENGTH_SHORT).show();
                        }*/
                    } else {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                Toast toast = Toast.makeText(getBaseContext(), "Cannot send the data because your internet connection is slow. \nPlease make sure that you are connected to a stable connection", Toast.LENGTH_LONG);
                                View view = toast.getView();
                                view.setBackgroundResource(R.color.toast_red);
                                toast.show();
                            }
                        });
                    }

                    network_status = true;
                } else {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, true, gs.sent);
                            statusDisplay();
                            Toast toast = Toast.makeText(getBaseContext(), "Unable to connect to the server", Toast.LENGTH_LONG);
                            View view = toast.getView();
                            view.setBackgroundResource(R.color.toast_red);
                            toast.show();
                            serverError();
                        }
                    });

                }
            } else {
                network_status = false;
                runOnUiThread(new Runnable() {
                    public void run() {
                        db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, true, gs.sent);
                        statusDisplay();
                    }
                });
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            pDialog.dismiss();
            if (network_status == false) {
                db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, true, gs.sent);
                statusDisplay();
                Toast.makeText(getApplicationContext(),
                        "Network not available", Toast.LENGTH_SHORT).show();
            } else if (network_status == true) {
                if (google_response.equals("200")) {
                    if (fileUploaded) {

						/*if ((fieldsComplete==true)&&(appStatReviewer==true)){*/
                        if (appStatReviewer) {
                            //gs.fieldsComplete = fieldsComplete;
                            gs.sent = true;
                            db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, true, gs.sent);
                            gs.online = false;
                            gs.connectedToServer = false;
                            gs.fastInternet = false;

                            add_submitted_Delete();
                            gs.sent = false;
                            Toast toast = Toast.makeText(getBaseContext(), "Data Updated", Toast.LENGTH_LONG);
                            View view = toast.getView();
                            view.setBackgroundResource(R.color.toast_blue);
                            toast.show();
                            finish();
                        } else {
                            Toast toast = Toast.makeText(getBaseContext(), "Submmission failed due to incomplete data.", Toast.LENGTH_LONG);
                            View view = toast.getView();
                            view.setBackgroundResource(R.color.toast_red);
                            toast.show();
                            finish();
                        }

                    } else{
                        Toast.makeText(getApplicationContext(), "Data Retained", Toast.LENGTH_LONG).show();
                        error_uploading();//popup dialog error
                    }
                    //finish();
                }
            }

        }

    }

    public void add_submitted_Delete() {

        String temp_is_company = db2.getRecord("app_account_is_company",where,args,"tbl_report_accepted_jobs").get(0);
        String temp_company_name = db2.getRecord("app_account_company_name",where,args,"tbl_report_accepted_jobs").get(0);

        if(temp_is_company.contentEquals("true")){
            account_fname = temp_company_name;
            account_mname="";
            account_lname="";
        }
        ArrayList<String> field = new ArrayList<String>();
        field.clear();
        field.add("record_id");
        field.add("first_name");
        field.add("middle_name");
        field.add("last_name");
        field.add("appraisal_type");
        field.add("status");
        ArrayList<String> value = new ArrayList<String>();
        value.clear();
        value.add(record_id);
        value.add(account_fname);
        value.add(account_mname);
        value.add(account_lname);
        value.add("Motor Vehicle");
        value.add("submitted");


        String r_id = db2.getRecord("record_id", where, args, "tbl_report_submitted").get(0);
        if (r_id.contentEquals(record_id)) {
            db2.deleteRecord("tbl_report_submitted", "record_id = ?", new String[]{record_id});
            db2.addRecord(value, field, "tbl_report_submitted");
        } else {
            db2.addRecord(value, field, "tbl_report_submitted");
        }
        //delete

        db2.deleteRecord("motor_vehicle", "record_id = ?", new String[]{record_id});
        db2.deleteRecord("motor_vehicle_ownership_history", "record_id = ?", new String[]{record_id});
        db2.deleteRecord("motor_vehicle_ownership_source", "record_id = ?", new String[]{record_id});
        db2.deleteRecord("motor_vehicle_previous_appraisal", "record_id = ?", new String[]{record_id});
        db2.deleteRecord("tbl_attachments", "record_id = ?", new String[]{record_id});
        db2.deleteRecord("tbl_report_accepted_jobs", "record_id = ?", new String[]{record_id});
        db2.deleteRecord("tbl_report_accepted_jobs_contacts", "record_id = ?", new String[]{record_id});
        db2.deleteRecord("submission_logs", "record_id = ?", new String[]{record_id});
        db2.deleteRecord("report_filename", "record_id = ?", new String[]{record_id});
    }

    private class GlobalSave extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pDialog = new ProgressDialog(Motor_vehicle.this);
            pDialog.setMessage("Generating report...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            NetworkUtil.getConnectivityStatusString(Motor_vehicle.this);
            if (!NetworkUtil.status.equals("Network not available")) {
                if (gds.google_ping().equals("200")) {
                    if (Connectivity.isConnectedFast(getBaseContext())) {
                        globalSave = true;
                        create_json();
                        return true;
                    } else {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
                                statusDisplay();
                                Toast toast = Toast.makeText(getBaseContext(), "Slow Internet connection.", Toast.LENGTH_LONG);
                                View view = toast.getView();
                                view.setBackgroundResource(R.color.toast_red);
                                toast.show();
                                serverError();
                            }
                        });
                        return false;
                    }

                } else {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
                            statusDisplay();
                            Toast toast = Toast.makeText(getBaseContext(), "Unable to connect to the server", Toast.LENGTH_LONG);
                            View view = toast.getView();
                            view.setBackgroundResource(R.color.toast_red);
                            toast.show();
                            serverError();
                        }
                    });
                    return false;
                }

            } else {
                runOnUiThread(new Runnable() {
                    public void run() {
                        db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, gs.fieldsComplete, gs.sent);
                        statusDisplay();
                        Toast toast = Toast.makeText(getBaseContext(), "Network not available.", Toast.LENGTH_LONG);
                        View view = toast.getView();
                        view.setBackgroundResource(R.color.toast_red);
                        toast.show();
                        serverError();
                    }
                });
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            if (result) {
                Intent in = new Intent(getApplicationContext(), View_Report.class);
                in.putExtra(TAG_RECORD_ID, record_id);
                in.putExtra("appraisal_type", "motor_vehicle");
                startActivityForResult(in, 100);
            }
            pDialog.dismiss();


        }

    }

    public void serverError() {

        myDialog = new Dialog(Motor_vehicle.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.warning_dialog);
        myDialog.setCancelable(true);
        final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
        final Button btn_ok = (Button) myDialog.findViewById(R.id.btn_ok);

        tv_question.setText(R.string.server_error);
        btn_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.show();
    }

    public void error_uploading() {
        myDialog = new Dialog(Motor_vehicle.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.warning_dialog);
        myDialog.setCancelable(true);
        final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
        final Button btn_ok = (Button) myDialog.findViewById(R.id.btn_ok);

        tv_question.setText(R.string.error_uploading);
        btn_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.show();
    }



    private boolean validateSpinner(Spinner[] fields) {
        for (int i = 0; i < fields.length; i++) {
            Spinner currentField = fields[i];
            if (currentField.getSelectedItem().toString().length() <= 0) {
                return false;
            }
        }
        return true;
    }

    public void create_json() {

        List<Motor_Vehicle_API> mv = db.getMotor_Vehicle(record_id);
        if (!mv.isEmpty()) {
            for (Motor_Vehicle_API im : mv) {
                JSONObject query_temp = new JSONObject();
                JSONObject report = new JSONObject();
                JSONObject record_temp = new JSONObject();

                JSONObject appraisal_attachments = new JSONObject();
                JSONObject appraisal_attachments_data = new JSONObject();
                JSONArray app_attachments = new JSONArray();
                JSONObject mvpa_obj = new JSONObject();//Prev appraisal
                JSONArray mvpa_ary = new JSONArray();//Prev appraisal
                JSONObject mvoh_obj = new JSONObject();//mv ownership history
                JSONArray mvoh_ary = new JSONArray();//mv ownership history
                JSONObject mvos_obj = new JSONObject();//mv ownership source
                JSONArray mvos_ary = new JSONArray();//mv ownership source

                JSONObject app_req_obj = new JSONObject();//car_details
                JSONArray app_req_ary = new JSONArray();//car_details

                try {
                    //check first for required fields
                    freeFields = true;
                    //Auguat 18 2017
                    //check if all manda is filled up
                    //submit
                    Log.e("Complete", "All required fields are filled up");


                    query_temp.put("system.record_id", record_id);
                    report.put("from_mobile", "true");
                   report.put("valrep_mv_date_inspected_month", im.getreport_date_inspected_month());
                    report.put("valrep_mv_date_inspected_day", im.getreport_date_inspected_day());
                    report.put("valrep_mv_date_inspected_year", im.getreport_date_inspected_year());
                    report.put("valrep_mv_time_inspected", im.getreport_time_inspected());

                    report.put("valrep_mv_interior_dashboard", im.getreport_interior_dashboard());
                    report.put("valrep_mv_interior_sidings", im.getreport_interior_sidings());
                    report.put("valrep_mv_interior_seats", im.getreport_interior_seats());
                    report.put("valrep_mv_interior_windows", im.getreport_interior_windows());
                    report.put("valrep_mv_interior_ceiling", im.getreport_interior_ceiling());
                    report.put("valrep_mv_interior_instrument", im.getreport_interior_instrument());

                    report.put("valrep_mv_bodytype_grille", im.getreport_bodytype_grille());
                    report.put("valrep_mv_bodytype_headlight", im.getreport_bodytype_headlight());
                    report.put("valrep_mv_bodytype_fbumper", im.getreport_bodytype_fbumper());
                    report.put("valrep_mv_bodytype_hood", im.getreport_bodytype_hood());
                    report.put("valrep_mv_bodytype_rf_fender", im.getreport_bodytype_rf_fender());
                    report.put("valrep_mv_bodytype_rf_door", im.getreport_bodytype_rf_door());
                    report.put("valrep_mv_bodytype_rr_door", im.getreport_bodytype_rr_door());
                    report.put("valrep_mv_bodytype_rr_fender", im.getreport_bodytype_rr_fender());
                    report.put("valrep_mv_bodytype_backdoor", im.getreport_bodytype_backdoor());
                    report.put("valrep_mv_bodytype_taillight", im.getreport_bodytype_taillight());
                    report.put("valrep_mv_bodytype_r_bumper", im.getreport_bodytype_r_bumper());
                    report.put("valrep_mv_bodytype_lr_fender", im.getreport_bodytype_lr_fender());
                    report.put("valrep_mv_bodytype_lr_door", im.getreport_bodytype_lr_door());
                    report.put("valrep_mv_bodytype_lf_door", im.getreport_bodytype_lf_door());
                    report.put("valrep_mv_bodytype_lf_fender", im.getreport_bodytype_lf_fender());
                    report.put("valrep_mv_bodytype_top", im.getreport_bodytype_top());
                    report.put("valrep_mv_bodytype_paint", im.getreport_bodytype_paint());
                    report.put("valrep_mv_bodytype_flooring", im.getreport_bodytype_flooring());
                    report.put("valrep_mv_bodytype_trunk", db2.getRecord("valrep_mv_bodytype_trunk", where, args, "motor_vehicle").get(0));

                    report.put("valrep_mv_misc_stereo", im.getreport_misc_stereo());
                    report.put("valrep_mv_misc_tools", im.getreport_misc_tools());
                    report.put("valrep_mv_misc_speakers", im.getreport_misc_speakers());
                    report.put("valrep_mv_misc_airbag", im.getreport_misc_airbag());
                    report.put("valrep_mv_misc_tires", im.getreport_misc_tires());
                    report.put("valrep_mv_misc_mag_wheels", im.getreport_misc_mag_wheels());
                    report.put("valrep_mv_misc_carpet", im.getreport_misc_carpet());
                    report.put("valrep_mv_misc_others", im.getreport_misc_others());

                    report.put("valrep_mv_enginearea_fuel", im.getreport_enginearea_fuel());
                    report.put("valrep_mv_enginearea_transmission1", im.getreport_enginearea_transmission1());
                    report.put("valrep_mv_enginearea_transmission2", im.getreport_enginearea_transmission2());
                    report.put("valrep_mv_enginearea_chassis", im.getreport_enginearea_chassis());
                    report.put("valrep_mv_enginearea_battery", im.getreport_enginearea_battery());
                    report.put("valrep_mv_enginearea_electrical", im.getreport_enginearea_electrical());
                    report.put("valrep_mv_enginearea_engine", im.getreport_enginearea_engine());

                    report.put("valrep_mv_verification", im.getreport_verification());
                    report.put("valrep_mv_verification_district_office", im.getreport_verification_district_office());
                    report.put("valrep_mv_verification_encumbrance", im.getreport_verification_encumbrance());
                    report.put("valrep_mv_verification_registered_owner", im.getreport_verification_registered_owner());

                    report.put("valrep_mv_place_inspected", im.getreport_place_inspected());

                    report.put("valrep_mv_market_valuation_fair_value", im.getreport_market_valuation_fair_value());
                    report.put("valrep_mv_market_valuation_max", im.getreport_market_valuation_max());
                    report.put("valrep_mv_market_valuation_min", im.getreport_market_valuation_min());
                    report.put("valrep_mv_market_valuation_remarks", im.getreport_market_valuation_remarks());
                    report.put("valrep_mv_market_valuation_previous_remarks", db2.getRecord("valrep_mv_market_valuation_previous_remarks", where, args, "motor_vehicle").get(0));
                    //ADDED By IAN

                    report.put("valrep_mv_comp1_source", im.getreport_comp1_source());
                    report.put("valrep_mv_comp1_tel_no", im.getreport_comp1_tel_no());
                    report.put("valrep_mv_comp1_unit_model", im.getreport_comp1_unit_model());
                    report.put("valrep_mv_comp1_mileage", im.getreport_comp1_mileage());
                    report.put("valrep_mv_comp1_price_min", im.getreport_comp1_price_min());
                    report.put("valrep_mv_comp1_price_max", im.getreport_comp1_price_max());
                    report.put("valrep_mv_comp1_price", im.getreport_comp1_price());
                    report.put("valrep_mv_comp1_less_amt", im.getreport_comp1_less_amt());
                    report.put("valrep_mv_comp1_less_net", im.getreport_comp1_less_net());
                    report.put("valrep_mv_comp1_add_amt", im.getreport_comp1_add_amt());
                    report.put("valrep_mv_comp1_add_net", im.getreport_comp1_add_net());
                    report.put("valrep_mv_comp1_time_adj_value", im.getreport_comp1_time_adj_value());
                    report.put("valrep_mv_comp1_time_adj_amt", im.getreport_comp1_time_adj_amt());
                    report.put("valrep_mv_comp1_adj_value", im.getreport_comp1_adj_value());
                    report.put("valrep_mv_comp1_adj_amt", im.getreport_comp1_adj_amt());
                    report.put("valrep_mv_comp1_index_price", im.getreport_comp1_index_price());

                    report.put("valrep_mv_comp2_source", im.getreport_comp2_source());
                    report.put("valrep_mv_comp2_tel_no", im.getreport_comp2_tel_no());
                    report.put("valrep_mv_comp2_unit_model", im.getreport_comp2_unit_model());
                    report.put("valrep_mv_comp2_mileage", im.getreport_comp2_mileage());
                    report.put("valrep_mv_comp2_price_min", im.getreport_comp2_price_min());
                    report.put("valrep_mv_comp2_price_max", im.getreport_comp2_price_max());
                    report.put("valrep_mv_comp2_price", im.getreport_comp2_price());
                    report.put("valrep_mv_comp2_less_amt", im.getreport_comp2_less_amt());
                    report.put("valrep_mv_comp2_less_net", im.getreport_comp2_less_net());
                    report.put("valrep_mv_comp2_add_amt", im.getreport_comp2_add_amt());
                    report.put("valrep_mv_comp2_add_net", im.getreport_comp2_add_net());
                    report.put("valrep_mv_comp2_time_adj_value", im.getreport_comp2_time_adj_value());
                    report.put("valrep_mv_comp2_time_adj_amt", im.getreport_comp2_time_adj_amt());
                    report.put("valrep_mv_comp2_adj_value", im.getreport_comp2_adj_value());
                    report.put("valrep_mv_comp2_adj_amt", im.getreport_comp2_adj_amt());
                    report.put("valrep_mv_comp2_index_price", im.getreport_comp2_index_price());

                    report.put("valrep_mv_comp3_source", im.getreport_comp3_source());
                    report.put("valrep_mv_comp3_tel_no", im.getreport_comp3_tel_no());
                    report.put("valrep_mv_comp3_unit_model", im.getreport_comp3_unit_model());
                    report.put("valrep_mv_comp3_mileage", im.getreport_comp3_mileage());
                    report.put("valrep_mv_comp3_price_min", im.getreport_comp3_price_min());
                    report.put("valrep_mv_comp3_price_max", im.getreport_comp3_price_max());
                    report.put("valrep_mv_comp3_price", im.getreport_comp3_price());
                    report.put("valrep_mv_comp3_less_amt", im.getreport_comp3_less_amt());
                    report.put("valrep_mv_comp3_less_net", im.getreport_comp3_less_net());
                    report.put("valrep_mv_comp3_add_amt", im.getreport_comp3_add_amt());
                    report.put("valrep_mv_comp3_add_net", im.getreport_comp3_add_net());
                    report.put("valrep_mv_comp3_time_adj_value", im.getreport_comp3_time_adj_value());
                    report.put("valrep_mv_comp3_time_adj_amt", im.getreport_comp3_time_adj_amt());
                    report.put("valrep_mv_comp3_adj_value", im.getreport_comp3_adj_value());
                    report.put("valrep_mv_comp3_adj_amt", im.getreport_comp3_adj_amt());
                    report.put("valrep_mv_comp3_index_price", im.getreport_comp3_index_price());


                    report.put("valrep_mv_comp_ave_index_price", im.getreport_comp_ave_index_price());
                    report.put("valrep_mv_rec_lux_car", im.getreport_rec_lux_car());
                    report.put("valrep_mv_rec_mile_factor", im.getreport_rec_mile_factor());
                    report.put("valrep_mv_rec_taxi_use", im.getreport_rec_taxi_use());
                    report.put("valrep_mv_rec_for_hire", im.getreport_rec_for_hire());
                    report.put("valrep_mv_rec_condition", im.getreport_rec_condition());
                    report.put("valrep_mv_rec_repo", im.getreport_rec_repo());
                    report.put("valrep_mv_rec_other_val", im.getreport_rec_other_val());
                    report.put("valrep_mv_rec_other_desc", im.getreport_rec_other_desc());
                    report.put("valrep_mv_rec_other2_val", im.getreport_rec_other2_val());
                    report.put("valrep_mv_rec_other2_desc", im.getreport_rec_other2_desc());
                    report.put("valrep_mv_rec_other3_val", im.getreport_rec_other3_val());
                    report.put("valrep_mv_rec_other3_desc", im.getreport_rec_other3_desc());
                    report.put("valrep_mv_rec_market_resistance_rec_total", im.getreport_rec_market_resistance_rec_total());
                    report.put("valrep_mv_rec_market_resistance_total", im.getreport_rec_market_resistance_total());
                    report.put("valrep_mv_rec_market_resistance_net_total", im.getreport_rec_market_resistance_net_total());
                    Log.e("net total", im.getreport_rec_market_resistance_total());
                    Log.e("net total all", im.getreport_rec_market_resistance_net_total());
                    report.put("valrep_mv_rep_stereo", im.getreport_rep_stereo());
                    report.put("valrep_mv_rep_speakers", im.getreport_rep_speakers());
                    report.put("valrep_mv_rep_tires_pcs", im.getreport_rep_tires_pcs());
                    report.put("valrep_mv_rep_tires", im.getreport_rep_tires());
                    report.put("valrep_mv_rep_side_mirror_pcs", im.getreport_rep_side_mirror_pcs());
                    report.put("valrep_mv_rep_side_mirror", im.getreport_rep_side_mirror());
                    report.put("valrep_mv_rep_light", im.getreport_rep_light());
                    report.put("valrep_mv_rep_tools", im.getreport_rep_tools());
                    report.put("valrep_mv_rep_battery", im.getreport_rep_battery());
                    report.put("valrep_mv_rep_plates", im.getreport_rep_plates());
                    report.put("valrep_mv_rep_bumpers", im.getreport_rep_bumpers());
                    report.put("valrep_mv_rep_windows", im.getreport_rep_windows());
                    report.put("valrep_mv_rep_body", im.getreport_rep_body());
                    report.put("valrep_mv_rep_engine_wash", im.getreport_rep_engine_wash());
                    report.put("valrep_mv_rep_other_desc", im.getreport_rep_other_desc());
                    report.put("valrep_mv_rep_other", im.getreport_rep_other());
                    report.put("valrep_mv_rep_total", im.getreport_rep_total());
                    report.put("valrep_mv_appraised_value", im.getreport_appraised_value());
                    report.put("valrep_mv_verification_file_no", im.getreport_verification_file_no());
                    report.put("valrep_mv_verification_first_reg_date_month", im.getreport_verification_first_reg_date_month());
                    report.put("valrep_mv_verification_first_reg_date_day", im.getreport_verification_first_reg_date_day());
                    report.put("valrep_mv_verification_first_reg_date_year", im.getreport_verification_first_reg_date_year());
                    report.put("valrep_mv_comp1_mileage_value", im.getreport_comp1_mileage_value());
                    report.put("valrep_mv_comp2_mileage_value", im.getreport_comp2_mileage_value());
                    report.put("valrep_mv_comp3_mileage_value", im.getreport_comp3_mileage_value());
                    report.put("valrep_mv_comp1_mileage_amt", im.getreport_comp1_mileage_amt());
                    report.put("valrep_mv_comp2_mileage_amt", im.getreport_comp2_mileage_amt());
                    report.put("valrep_mv_comp3_mileage_amt", im.getreport_comp3_mileage_amt());
                    report.put("valrep_mv_comp1_adj_desc", im.getreport_comp1_adj_desc());
                    report.put("valrep_mv_comp2_adj_desc", im.getreport_comp2_adj_desc());
                    report.put("valrep_mv_comp3_adj_desc", im.getreport_comp3_adj_desc());
                    args[0] = record_id;
                    report.put("valrep_mv_comp1_new_unit_value", db2.getRecord("valrep_mv_comp1_new_unit_value", where, args, "motor_vehicle").get(0));
                    report.put("valrep_mv_comp2_new_unit_value", db2.getRecord("valrep_mv_comp2_new_unit_value", where, args, "motor_vehicle").get(0));
                    report.put("valrep_mv_comp3_new_unit_value", db2.getRecord("valrep_mv_comp3_new_unit_value", where, args, "motor_vehicle").get(0));
                    report.put("valrep_mv_comp1_new_unit_amt", gds.replaceFormat(db2.getRecord("valrep_mv_comp1_new_unit_amt", where, args, "motor_vehicle").get(0)));
                    report.put("valrep_mv_comp2_new_unit_amt", gds.replaceFormat(db2.getRecord("valrep_mv_comp2_new_unit_amt", where, args, "motor_vehicle").get(0)));
                    report.put("valrep_mv_comp3_new_unit_amt", gds.replaceFormat(db2.getRecord("valrep_mv_comp3_new_unit_amt", where, args, "motor_vehicle").get(0)));
                    report.put("valrep_mv_comp_add_index_price_desc", db2.getRecord("valrep_mv_comp_add_index_price_desc", where, args, "motor_vehicle").get(0));
                    report.put("valrep_mv_comp_add_index_price", gds.replaceFormat(db2.getRecord("valrep_mv_comp_add_index_price", where, args, "motor_vehicle").get(0)));
                    report.put("valrep_mv_comp_temp_ave_index_price", gds.replaceFormat(db2.getRecord("valrep_mv_comp_temp_ave_index_price", where, args, "motor_vehicle").get(0)));

                    report.put("valrep_mv_rep_other2_desc", gds.replaceFormat(db2.getRecord("valrep_mv_rep_other2_desc", where, args, "motor_vehicle").get(0)));
                    report.put("valrep_mv_rep_other2", gds.replaceFormat(db2.getRecord("valrep_mv_rep_other2", where, args, "motor_vehicle").get(0)));
                    report.put("valrep_mv_rep_other3_desc", gds.replaceFormat(db2.getRecord("valrep_mv_rep_other3_desc", where, args, "motor_vehicle").get(0)));
                    report.put("valrep_mv_rep_other3", gds.replaceFormat(db2.getRecord("valrep_mv_rep_other3", where, args, "motor_vehicle").get(0)));

                    if (!globalSave) {

                        String motor_fair_value_s = im.getreport_market_valuation_fair_value();
                        double motor_fair_value_i = 0;
                        if (motor_fair_value_s.equals("")) {
                            motor_fair_value_i = 0;
                        } else {
                            motor_fair_value_i = Double.parseDouble(motor_fair_value_s);
                        }
                        //Matrix MV1
                     /*   if ((motor_fair_value_i >= 0) && (motor_fair_value_i <= 1000000)) {
                            report.put("approval_signor_reviewer", "false");
                            report.put("approval_signor_reviewer_done", "true");
                            report.put("approval_supervisor_reviewer", "false");
                            report.put("approval_supervisor_reviewer_done", "true");

                            report.put("approval_signor", "true");
                            report.put("approval_supervisor", "alt");
                            report.put("approval_manager", "alt");
                            report.put("approval_head", "false");

                            report.put("approval_signor_done", "false");
                            report.put("approval_supervisor_done", "false");
                            report.put("approval_manager_done", "false");
                            report.put("approval_head_done", "true");

                            report.put("approval_matrix_type", "MV1");
                        }

                        //Matrix MV2
                        else if ((motor_fair_value_i > 1000000) && (motor_fair_value_i <= 3000000)) {
                            report.put("approval_signor_reviewer", "false");
                            report.put("approval_signor_reviewer_done", "true");
                            report.put("approval_supervisor_reviewer", "false");
                            report.put("approval_supervisor_reviewer_done", "true");

                            report.put("approval_signor", "false");
                            report.put("approval_supervisor", "true");
                            report.put("approval_manager", "alt");
                            report.put("approval_head", "alt");

                            report.put("approval_signor_done", "true");
                            report.put("approval_supervisor_done", "false");
                            report.put("approval_manager_done", "false");
                            report.put("approval_head_done", "false");

                            report.put("approval_matrix_type", "MV2");
                        }

                        //Matrix MV3
                        else if ((motor_fair_value_i > 3000000) && (motor_fair_value_i <= 10000000)) {
                            report.put("approval_signor_reviewer", "false");
                            report.put("approval_signor_reviewer_done", "true");
                            report.put("approval_supervisor_reviewer", "false");
                            report.put("approval_supervisor_reviewer_done", "true");

                            report.put("approval_signor", "true");
                            report.put("approval_supervisor", "true");
                            report.put("approval_manager", "true");
                            report.put("approval_head", "alt");

                            report.put("approval_signor_done", "false");
                            report.put("approval_supervisor_done", "false");
                            report.put("approval_manager_done", "false");
                            report.put("approval_head_done", "false");

                            report.put("approval_matrix_type", "MV3");
                        }

                        //Matrix MV4
                        else if (motor_fair_value_i > 10000000) {
                            report.put("approval_signor_reviewer", "false");
                            report.put("approval_signor_reviewer_done", "true");
                            report.put("approval_supervisor_reviewer", "false");
                            report.put("approval_supervisor_reviewer_done", "true");

                            report.put("approval_signor", "true");
                            report.put("approval_supervisor", "true");
                            report.put("approval_manager", "true");
                            report.put("approval_head", "true");

                            report.put("approval_signor_done", "false");
                            report.put("approval_supervisor_done", "false");
                            report.put("approval_manager_done", "false");
                            report.put("approval_head_done", "false");

                            report.put("approval_matrix_type", "MV4");
                        }
                        if (requesting_party.contentEquals("AUTO FINANCE GROUP")) {
                            report.put("application_status", "afg_valuation");
                        } else {
                            report.put("application_status", "report_review");
                        }*/
                        if (requesting_party.contentEquals("AUTO FINANCE GROUP")) {
                            report.put("application_status", "afg_valuation");
                        } else {
                            report.put("application_status", "report_review");
                        }
                    }
                    //direct to appraisal manager
                    //end of matrix
//get current date and time and submit to CC
                    TimeZone tz = TimeZone.getTimeZone(gs.gmt);
                    Calendar c = Calendar.getInstance(tz);

                    int cMonth = c.get(Calendar.MONTH);//starts with 0 for january
                    int cDay = c.get(Calendar.DAY_OF_MONTH);
                    int cYear = c.get(Calendar.YEAR);
                    int cAmPm = c.get(Calendar.AM_PM);

                    String sMonth = gs.monthInWord[cMonth];
                    String sAmPm = "";
                    if (cAmPm == 0) {
                        sAmPm = "AM";
                    } else if (cAmPm == 1) {
                        sAmPm = "PM";
                    }

                    String cTime = String.format("%02d", c.get(Calendar.HOUR_OF_DAY)) + ":" +
                            String.format("%02d", c.get(Calendar.MINUTE)) + " " + sAmPm;

                    report.put("valrep_mv_report_date_month", sMonth);
                    report.put("valrep_mv_report_date_day", String.valueOf(cDay));
                    report.put("valrep_mv_report_date_year", String.valueOf(cYear));
                    report.put("valrep_mv_time_of_report", cTime);

                    Log.e("DATE: ", String.valueOf(cMonth + 1) + " " + String.valueOf(cDay) + " " + String.valueOf(cYear));
                    Log.e("TIME: ", cTime);


                    //ADDED By IAN
                    //get  prev app
                    List<Motor_Vehicle_API_Prev_Appraisal> mvprev_app = db2.getMV_Prev(record_id);
                    if (!mvprev_app.isEmpty()) {
                        for (Motor_Vehicle_API_Prev_Appraisal imprev_app : mvprev_app) {
                            mvpa_obj = new JSONObject();
                            mvpa_obj.put("valrep_mv_prev_date", imprev_app.getreport_prev_date());
                            mvpa_obj.put("valrep_mv_prev_appraiser", imprev_app.getreport_prev_appraiser());
                            mvpa_obj.put("valrep_mv_prev_requestor", imprev_app.getreport_prev_requestor());
                            mvpa_obj.put("valrep_mv_prev_appraised_value", imprev_app.getreport_prev_appraised_value());
                            mvpa_ary.put(mvpa_obj);
                        }
                        report.put("valrep_mv_prev_appraisal", mvpa_ary);
                    } else {//if (lirdps.isEmpty()) {
                        report.put("valrep_mv_prev_appraisal", null);
                    }
//get current date and time and submit to CC

					/*List<Motor_Vehicle_API_Ownership_History> mvoh = db2.getMotor_Vehicle_Ownership_History(record_id);
					if (!mvoh.isEmpty()) {
						for (Motor_Vehicle_API_Ownership_History immvoh : mvoh) {
							mvoh_obj = new JSONObject();
							mvoh_obj.put("valrep_mv_verification_ownerhistory_name",immvoh.getreport_verification_ownerhistory_name());
							mvoh_obj.put("valrep_mv_verification_ownerhistory_date_month",immvoh.getreport_verification_ownerhistory_date_month());
							mvoh_obj.put("valrep_mv_verification_ownerhistory_date_day",immvoh.getreport_verification_ownerhistory_date_day());
							mvoh_obj.put("valrep_mv_verification_ownerhistory_date_year",immvoh.getreport_verification_ownerhistory_date_year());
							mvoh_ary.put(mvoh_obj);
						}
						//put to valrep_mv_ownership_history
						report.put("valrep_mv_ownership_history",mvoh_ary);
					}*/


					/*List<Motor_Vehicle_API_Ownership_Source> mvos = db2.getMotor_Vehicle_Ownership_Source(record_id);
					if (!mvos.isEmpty()) {
						for (Motor_Vehicle_API_Ownership_Source immvoh : mvos) {
							mvos_obj = new JSONObject();
							mvos_obj.put("valrep_mv_source_mileage",immvoh.getreport_source_mileage());
							mvos_obj.put("valrep_mv_source_range_min",immvoh.getreport_source_range_min());
							mvos_obj.put("valrep_mv_source_range_max",immvoh.getreport_source_range_max());
							mvos_obj.put("valrep_mv_source_source",immvoh.getreport_source_source());
							mvos_obj.put("valrep_mv_source_vehicle_type",immvoh.getreport_source_vehicle_type());
							mvos_ary.put(mvos_obj);
						}
						fieldsComplete = true;
						gs.fieldsComplete  = true;
					} else {
						fieldsComplete = false;
					}
					//put to valrep_mv_ownership_history
					report.put("valrep_mv_ownership_source",mvos_ary);*/


                    List<Report_Accepted_Jobs> addList = db.getReport_Accepted_Jobs(String.valueOf(record_id));
                    if (!addList.isEmpty()) {
                        for (Report_Accepted_Jobs ad : addList) {
                            app_req_obj = new JSONObject();
                            app_req_obj.put("app_car_loan_purpose", ad.getcar_loan_purpose());
                            app_req_obj.put("app_vehicle_type", ad.getvehicle_type());
                            app_req_obj.put("app_car_model", ad.getcar_model());
                            app_req_obj.put("app_car_mileage", ad.getcar_mileage());
                            app_req_obj.put("app_car_series", ad.getcar_series());
                            app_req_obj.put("app_car_color", ad.getcar_color());
                            app_req_obj.put("app_car_plate_no", ad.getcar_plate_no());
                            app_req_obj.put("app_car_body_type", ad.getcar_body_type());
                            app_req_obj.put("app_car_displacement", ad.getcar_displacement());
                            app_req_obj.put("app_car_motor_no", ad.getcar_motor_no());
                            app_req_obj.put("app_car_chassis_no", ad.getcar_chassis_no());
                            app_req_obj.put("app_car_no_of_cylinders", ad.getcar_no_cylinders());
                            app_req_obj.put("app_car_cr_no", ad.getcar_cr_no());
                            app_req_obj.put("app_car_cr_date_month", ad.getcar_cr_date_month());
                            app_req_obj.put("app_car_cr_date_day", ad.getcar_cr_date_day());
                            app_req_obj.put("app_car_cr_date_year", ad.getcar_cr_date_year());

//ADDED BY IAN
                            report.put("app_daterequested_day", ad.getdr_day());
                            report.put("app_daterequested_year", ad.getdr_year());
                            report.put("app_account_first_name", ad.getfname());
                            report.put("app_account_middle_name", ad.getmname());
                            report.put("app_account_last_name", ad.getlname());
                            report.put("app_requesting_party", ad.getrequesting_party());
                            report.put("app_requestor", ad.getrequestor());
                            args[0] = record_id;
                            report.put("app_account_is_company", db2.getRecord("app_account_is_company", where, args, "tbl_report_accepted_jobs").get(0));
                            report.put("app_account_company_name", db2.getRecord("app_account_company_name", where, args, "tbl_report_accepted_jobs").get(0));
                            args[0]=record_id;
                            app_req_obj.put("app_purpose_appraisal", db2.getRecord("nature_appraisal",where,args,"tbl_report_accepted_jobs").get(0));
                            app_req_obj.put("app_kind_of_appraisal", db2.getRecord("kind_of_appraisal",where,args,"tbl_report_accepted_jobs").get(0));

                            app_req_ary.put(app_req_obj);
                        }
                    }
                    report.put("appraisal_request", app_req_ary);

                    // for whole record
                    record_temp.put("record", report);
                    // for attachment
                    // attachments
                    List<Report_filename> rf = db.getReport_filename(record_id);
                    if (freeFields) {
                        if (!rf.isEmpty()) {
                            for (Report_filename report_file : rf) {
                                db_app_uid = report_file.getuid();
                                db_app_file = report_file.getfile();
                                db_app_filename = report_file.getfilename();
                                db_app_appraisal_type = report_file
                                        .getappraisal_type();
                                db_app_candidate_done = report_file
                                        .getcandidate_done();

                                appraisal_attachments = new JSONObject();
                                appraisal_attachments.put("uid", db_app_uid);
                                appraisal_attachments.put("file", db_app_file);
                                appraisal_attachments.put("filename",
                                        db_app_filename);
                                appraisal_attachments.put("appraisal_type",
                                        db_app_appraisal_type);
                                appraisal_attachments.put("candidate_done",
                                        db_app_candidate_done);
                                app_attachments.put(appraisal_attachments);
                                appraisal_attachments_data.put("attachments",
                                        app_attachments);
                                String query = query_temp.toString();
                                String record = record_temp.toString();

                                // attachments
                                String attachment = appraisal_attachments_data
                                        .toString();
                                userFunction.SendCaseCenter_Attachments(attachment);

                                boolean internetSpeedFast = Connectivity.isConnectedFast(getBaseContext());
                                if (internetSpeedFast == true) {

                                    runOnUiThread(new Runnable() {
                                        public void run() {
                                            gs.fastInternet = true;
                                            db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, true, gs.sent);
                                            statusDisplay();
                                            Toast toast = Toast.makeText(getBaseContext(), "Connected", Toast.LENGTH_LONG);
                                            View view = toast.getView();
                                            view.setBackgroundResource(R.color.toast_color);
                                            toast.show();

                                        }
                                    });

                                    // API
                                    //for tls
                                    JSONObject jsonResponse = new JSONObject();
                                    ArrayList<String> field = new ArrayList<>();
                                    ArrayList<String> value = new ArrayList<>();
                                    field.clear();
                                    field.add("auth_token");
                                    field.add("query");
                                    field.add("data");

                                    value.clear();
                                    value.add(gs.auth_token);
                                    value.add(query);
                                    value.add(record);


                                    if (Global.type.contentEquals("tls")) {
                                        if (globalSave) {
                                            jsonResponse = gds.makeHttpsRequest(gs.update_url, "POST", field, value);
                                        } else {
                                            jsonResponse = gds.makeHttpsRequest(gs.matrix_url, "POST", field, value);
                                        }
                                        xml = jsonResponse.toString();
                                    } else {
//									xml=gds.http_posting(field,value,gs.update_url,getApplicationContext());
                                        if (globalSave) {
                                            xml = gds.http_posting(field, value, gs.update_url, getApplicationContext());
                                        } else {
                                            xml = gds.http_posting(field, value, gs.matrix_url, getApplicationContext());
                                        }
                                    }

                                    Log.e("respone", xml);

                                    //check CC field status
                                    // if application_status == report review then data is submitted
                                    JSONObject mainxml = new JSONObject(xml);
                                    String recordxml, application_status;
                                    if (mainxml.has("record")) {
                                        Log.e("record", mainxml.getString("record"));
                                        if (mainxml.has("record")) {
                                            recordxml = mainxml.getString("record");
                                            Log.e("recordxml", recordxml);
                                            JSONObject json_application_status = new JSONObject(recordxml);
                                            if (json_application_status.has("application_status")) {
                                                application_status = json_application_status.getString("application_status");
                                                Log.e("application_status", application_status);

                                                if (application_status.contentEquals("report_review")) {
                                                    appStatReviewer = true;
                                                } else if (application_status.contentEquals("afg_valuation")) {
                                                    appStatReviewer = true;
                                                } else {
                                                    appStatReviewer = false;
                                                }

                                            }
                                        }
                                    }//end of check CC field

                                } else {
                                    runOnUiThread(new Runnable() {
                                        public void run() {
                                            db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, true, gs.sent);
                                            statusDisplay();
                                            Toast toast = Toast.makeText(getBaseContext(), "Cannot send the data because your internet connection is slow/disconnected/unstable. \nPlease make sure that you are connected to a stable connection", Toast.LENGTH_LONG);
                                            View view = toast.getView();
                                            view.setBackgroundResource(R.color.toast_red);
                                            toast.show();
                                        }
                                    });
                                }
                            }
                        }

                    }//end of if freeFields == true
                    else if (!freeFields) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                db.log_status(record_id, gs.online, gs.connectedToServer, gs.fastInternet, true, gs.sent);
                                statusDisplay();
                                Toast toast = Toast.makeText(getBaseContext(), "You need to fill up all required fields before you can submit the report.", Toast.LENGTH_LONG);
                                View view = toast.getView();
                                view.setBackgroundResource(R.color.toast_red);
                                toast.show();
                            }
                        });
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Update CC Failed",
                            "Exception : " + e.getMessage(), e);
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Failed to send data. Please try again.",
                                    Toast.LENGTH_LONG).show();
                            fileUploaded=false;
                            upload_status(fileUploaded);
                            error_uploading();
                        }
                    });

                }

            }
        }

    }

    public void create_pdf() {
        pdf_name = uid + "_"
                + "Unit Pictures" + "_" + counter;
        SharedPreferences appPrefs = getSharedPreferences("preference",
                MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = appPrefs.edit();
        prefsEditor.putString("pdf", pdf_name);
        prefsEditor.putString("fname", account_fname);
        prefsEditor.putString("lname", account_lname);
        prefsEditor.commit();
        startActivity(new Intent(Motor_vehicle.this, Pdf_Townhouse_condo.class));

    }

    public void open_customdialog() {
        // dialog
        myDialog = new Dialog(context);
        myDialog.setTitle("Select Attachment");
        myDialog.setContentView(R.layout.request_pdf_list);
        // myDialog.setTitle("My Dialog");

        dlg_priority_lvw = (ListView) myDialog
                .findViewById(R.id.dlg_priority_lvw);
        // ListView
        SimpleAdapter adapter = new SimpleAdapter(context, getPriorityList(),
                R.layout.request_pdf_list_layout, new String[]{
                "txt_pdf_list", "mono"}, new int[]{
                R.id.txt_pdf_list, R.id.mono});
        dlg_priority_lvw.setAdapter(adapter);

        // ListView
        dlg_priority_lvw
                .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1,
                                            int arg2, long arg3) {

                        item = ((TextView) arg1.findViewById(R.id.txt_pdf_list))
                                .getText().toString();
                        attachment_selected = item;
                        tv_attachment.setText(attachment_selected);
                        // update

                        Report_filename rf = new Report_filename();
                        rf.setuid(uid);
                        rf.setfile("Unit Pictures");
                        rf.setfilename(attachment_selected);
                        rf.setappraisal_type(appraisal_type + "_" + counter);
                        rf.setcandidate_done("true");
                        db.updateReport_filename(rf, record_id);
                        db.close();
                        myDialog.dismiss();
                        checkFileSize();
                    }
                });
        dlg_priority_lvw.setLongClickable(true);
        dlg_priority_lvw
                .setOnItemLongClickListener(new OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> arg0,
                                                   View arg1, final int arg2, long arg3) {
                        item = ((TextView) arg1.findViewById(R.id.txt_pdf_list))
                                .getText().toString();
                        Toast.makeText(getApplicationContext(), item,
                                Toast.LENGTH_SHORT).show();

                        File file = new File(myDirectory, item);

                        if (file.exists()) {
                            Uri path = Uri.fromFile(file);
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(path, "application/pdf");
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                            try {
                                startActivity(intent);
                            } catch (ActivityNotFoundException e) {
                                Toast.makeText(getApplicationContext(),
                                        "No Application Available to View PDF",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }

                        return true;

                    }
                });
        myDialog.show();
    }

    private List<HashMap<String, Object>> getPriorityList() {
        DatabaseHandler db = new DatabaseHandler(
                context.getApplicationContext());
        List<HashMap<String, Object>> priorityList = new ArrayList<HashMap<String, Object>>();
        List<Images> images = db.getAllImages(uid);//added account lname for specific search result like
        if (!images.isEmpty()) {
            for (Images im : images) {
                filename = im.getfilename();
                String filenameArray[] = filename.split("\\.");
                String extension = filenameArray[filenameArray.length - 1];
                if (extension.equals("pdf")) {
                    HashMap<String, Object> map1 = new HashMap<String, Object>();
                    map1.put("txt_pdf_list", im.getfilename());
                    map1.put("mono", R.drawable.attach_item);
                    priorityList.add(map1);
                }

            }

        }
        return priorityList;
    }

    public void checkFileSize() {
        myDialog = new Dialog(Motor_vehicle.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.warning_dialog);
        myDialog.setCancelable(true);
        final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
        final Button btn_ok = (Button) myDialog.findViewById(R.id.btn_ok);
        tv_question.setText(R.string.file_size_too_large);
        //format double to 2 decimal place
        DecimalFormat df = new DecimalFormat("#.00");
        //get fileSize and convert to KB and MB
        File filenew = new File(myDirectory + "/" + item);//get path concat with slash and file name
        int file_size_kb = Integer.parseInt(String.valueOf(filenew.length() / 1024));
        double file_size_mb = file_size_kb / 1024.00;
        //custom toast
        Toast toast = Toast.makeText(context, item +
                "\nfile size in KB: " + file_size_kb + "KB" +
                "\nfile size in MB: " + df.format(file_size_mb) + "MB", Toast.LENGTH_LONG);
        View view = toast.getView();
        view.setBackgroundResource(R.color.toast_color);
        toast.show();

        if (file_size_kb > 6144) {//restrict the file size up to 6.0MB only
            //clear textView to restict uploading
            Toast.makeText(getApplicationContext(), "file size is too large",
                    Toast.LENGTH_SHORT).show();
            tv_attachment.setText("");
            myDialog.show();
        }

        btn_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void upload_attachment() {
        // upload pdf
        upload_status(gds.uploadFile(Environment.getExternalStorageDirectory() + "/" + dir + "/"
                + tv_attachment.getText().toString()));
    }

  /*  public int uploadFile(String sourceFileUri) {
        TLSConnection tlscon = new TLSConnection();
        String upLoadServerUri = gs.pdf_upload_url;
        String fileName = sourceFileUri;

        HttpsURLConnection conn = null;
        HttpURLConnection conn2 = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(sourceFileUri);
        if (!sourceFile.isFile()) {
            Log.e("uploadFile", "Source File Does not exist");
            return 0;
        }
        try { // open a URL connection to the Servlet
            FileInputStream fileInputStream = new FileInputStream(sourceFile);
            URL url = new URL(upLoadServerUri);


            if (Global.type.contentEquals("tls")) {

                conn = tlscon.setUpHttpsConnection("" + url);

                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type",
                        "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("uploaded_file", fileName);
                dos = new DataOutputStream(conn.getOutputStream());
            } else {

                conn2 = (HttpURLConnection) url.openConnection();
                conn2.setDoInput(true); // Allow Inputs
                conn2.setDoOutput(true); // Allow Outputs
                conn2.setUseCaches(false); // Don't use a Cached Copy
                conn2.setRequestMethod("POST");
                conn2.setRequestProperty("Connection", "Keep-Alive");
                conn2.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn2.setRequestProperty("Content-Type",
                        "multipart/form-data;boundary=" + boundary);
                conn2.setRequestProperty("uploaded_file", fileName);
                dos = new DataOutputStream(conn2.getOutputStream());
            }

			*//*conn = tlscon.setUpHttpsConnection(""+url); // Open a HTTP
			// the URL
			conn.setDoInput(true); // Allow Inputs
			conn.setDoOutput(true); // Allow Outputs
			conn.setUseCaches(false); // Don't use a Cached Copy
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("ENCTYPE", "multipart/form-data");
			conn.setRequestProperty("Content-Type",
					"multipart/form-data;boundary=" + boundary);
			conn.setRequestProperty("uploaded_file", fileName);
			dos = new DataOutputStream(conn.getOutputStream());*//*

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
                    + fileName + "\"" + lineEnd);
            dos.writeBytes(lineEnd);

            bytesAvailable = fileInputStream.available(); // create a buffer of
            // maximum size

            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            // read file and write it into form...
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0) {
                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            // send multipart form data necesssary after file data...
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            // Responses from the server (code and message)
            String serverResponseMessage = "";
            if (Global.type.contentEquals("tls")) {
                serverResponseCode = conn.getResponseCode();
                serverResponseMessage = conn.getResponseMessage();
            } else {
                serverResponseCode = conn2.getResponseCode();
                serverResponseMessage = conn2.getResponseMessage();
            }

			*//*serverResponseCode = conn.getResponseCode();
			String serverResponseMessage = conn.getResponseMessage();*//*

            Log.i("uploadFile", "HTTP Response is : " + serverResponseMessage
                    + ": " + serverResponseCode);
            if (serverResponseCode == 200) {
                runOnUiThread(new Runnable() {
                    public void run() {
                    }
                });
            }

            // close the streams //
            fileInputStream.close();
            dos.flush();
            dos.close();

            //if upload succeeded
            fileUploadFailed = "false";
            upload_status();

        } catch (MalformedURLException ex) {
            ex.printStackTrace();
            Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Upload file to server Exception",
                    "Exception : " + e.getMessage(), e);
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getApplicationContext(), "Failed to upload File / Images",
                            Toast.LENGTH_LONG).show();
                    fileUploadFailed = "true";
                    upload_status();
                }
            });
        }
        return serverResponseCode;
    }*/

    public void upload_status(boolean response) {
        fileUploaded = response;
        if (response){
            globalSave=false;
            create_json();

        }
    }

    public void open_pdf() {
        File file = new File(myDirectory, tv_attachment.getText().toString());

        if (file.exists()) {
            Uri path = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(getApplicationContext(),
                        "No Application Available to View PDF",
                        Toast.LENGTH_SHORT).show();
            }
        }

    }

    public void uploaded_attachments() {
        // set value
        List<Required_Attachments> required_attachments = db
                .getAllRequired_Attachments_byid(String.valueOf(appraisal_type));
        if (!required_attachments.isEmpty()) {
            for (final Required_Attachments ra : required_attachments) {
                LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                final View addView = layoutInflater.inflate(
                        R.layout.report_uploaded_attachments_dynamic, null);
                final TextView tv_attachment = (TextView) addView
                        .findViewById(R.id.tv_attachment);
                final ImageView btn_view = (ImageView) addView
                        .findViewById(R.id.btn_view);


                tv_attachment.setText(ra.getattachment());
                btn_view.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        uploaded_attachment = uid + "_"
                                + ra.getattachment() + "_" + counter + ".pdf";
                        // directory
                        uploaded_file = new File(myDirectory,
                                uploaded_attachment);
                        uploaded_attachment = uploaded_attachment.replaceAll(
                                " ", "%20");
                        //url_webby = "http://dv-dev.gdslinkasia.com/attachments/"
                        //		+ uploaded_attachment;
                        //url_webby = "http://dev.gdslinkasia.com:8080/pdf_attachments/"
                        //		+ uploaded_attachment;
                        url_webby = gs.pdf_loc_url + uploaded_attachment;
                        view_pdf(uploaded_file);
                    }
                });
                container_attachments.addView(addView);

            }

        }

    }

    private void view_download() {
        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Action");
        builder.setItems(commandArray, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int index) {
                if (index == 0) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
                            .parse(url_webby));
                    startActivity(browserIntent);
                    dialog.dismiss();
                } else if (index == 1) {
                    new DownloadFileFromURL().execute(url_webby);
                    dialog.dismiss();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void view_pdf(File file) {
        if (file.exists()) {
            Uri path = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(getApplicationContext(),
                        "No Application Available to View PDF",
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            NetworkUtil.getConnectivityStatusString(this);
            if (!NetworkUtil.status.equals("Network not available")) {
                new Attachment_validation().execute();
            } else {
                Toast.makeText(getApplicationContext(),
                        "Network not available", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void db_delete_all() {
        String[] involvedTables = {"motor_vehicle_ownership_history", "motor_vehicle_ownership_source", "submission_logs"};
        for (int x = 0; x < involvedTables.length; x++) {
            db2.deleteAllDB2(record_id, involvedTables[x]);
            db2.close();
        }


        Report_Accepted_Jobs raj = new Report_Accepted_Jobs();
        raj.setrecord_id(record_id);
        db.deleteReport_Accepted_Jobs(raj);
        db.close();

        Report_Accepted_Jobs_Contacts rajc = new Report_Accepted_Jobs_Contacts();
        rajc.setrecord_id(record_id);
        db.deleteReport_Accepted_Jobs_Contacts(rajc);
        db.close();

        Report_filename rf = new Report_filename();
        rf.setrecord_id(record_id);
        db.deleteReport_filename(rf);
        db.close();

        Motor_Vehicle_API mv = new Motor_Vehicle_API();
        mv.setrecord_id(record_id);
        db.deleteMotor_Vehicle_API(mv);
        db.close();
    }

    public void manual_delete_all() {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.delete_dialog);

        Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btn_no = (Button) dialog.findViewById(R.id.btn_no);
        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_question);
        dialog.show();

        text.setText("Do you want to delete this record?");

        // if button is clicked, close the custom dialog
        btn_yes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                String[] involvedTables = {"motor_vehicle_ownership_history", "motor_vehicle_previous_appraisal", "motor_vehicle_ownership_source"};
                for (int x = 0; x < involvedTables.length; x++) {
                    db2.deleteAllDB2(record_id, involvedTables[x]);
                    db2.close();
                }


                Report_Accepted_Jobs raj = new Report_Accepted_Jobs();
                raj.setrecord_id(record_id);
                db.deleteReport_Accepted_Jobs(raj);
                db.close();

                Report_Accepted_Jobs_Contacts rajc = new Report_Accepted_Jobs_Contacts();
                rajc.setrecord_id(record_id);
                db.deleteReport_Accepted_Jobs_Contacts(rajc);
                db.close();

                Report_filename rf = new Report_filename();
                rf.setrecord_id(record_id);
                db.deleteReport_filename(rf);
                db.close();

                Motor_Vehicle_API mv = new Motor_Vehicle_API();
                mv.setrecord_id(record_id);
                db.deleteMotor_Vehicle_API(mv);
                db.close();

                dialog.dismiss();
                finish();
            }
        });
        btn_no.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    public void check_attachment() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.yes_no_dialog);

        Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btn_no = (Button) dialog.findViewById(R.id.btn_no);
        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_question);
        text.setText("Are you sure you want to submit ?");


        //aug 17
        args[0] = record_id;
        if(db2.getRecord("application_status",where,args,"tbl_report_accepted_jobs").get(0).contentEquals("for_rework")){
            freshData = false;
        }else{
            freshData = true;
        }


        //check if with or without attachment
        //wonderful pdf
     /*   kind_appraisal = gds.nullCheck3(db2.getRecord("kind_of_appraisal", "WHERE record_id = args", new String[]{record_id}, "tbl_report_accepted_jobs").get(0));
        if(kind_appraisal.contentEquals("Actual Inspection")){*/

            if (((!tv_attachment.getText().toString().equals("")) && (freshData == false)) ||
                    ((!tv_attachment.getText().toString().equals("")) && (freshData == true))) {
                dialog.show();
                withAttachment = true;
            } else if ((tv_attachment.getText().toString().equals("")) && (freshData == true)) {
                text.setText("Do you want to submit report without unit pictures attachment?");
                withAttachment = false;
                fileUploaded=true;
                dialog.dismiss();
                warning_dialog();
            } else if ((tv_attachment.getText().toString().equals("")) && (freshData == false)) {
                text.setText("Do you want to submit the report?");
                withAttachment = false;
                fileUploaded=true;
                dialog.show();
            }
       /* }else{
            dialog.show();
            withAttachment = true;
            fileUploadFailed = "true";
        }*/


        // if button is clicked, close the custom dialog
        btn_yes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((withAttachment) && (freshData)) ||
                        (withAttachment && !freshData) ||
                        (!withAttachment && !freshData)) {

                    if (new Fields_Checker().isModuleComplete(record_id, context)) {
                                new SendData().execute();

                           } else {
                                Toast.makeText(getApplicationContext(), "Module fields incomplete", Toast.LENGTH_LONG).show();
                            }

                    dialog.dismiss();
                } else if (!withAttachment && freshData) {
                    no_attachment();
                    dialog.dismiss();
                }
            }
        });
        btn_no.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


    }

    public void warning_dialog() {
        final Dialog dialog2 = new Dialog(context);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.setContentView(R.layout.warning_dialog);

        // set the custom dialog components - text, image and button
        TextView text2 = (TextView) dialog2.findViewById(R.id.tv_question);
        text2.setText("Please attach a unit pictures before you can submit the report");
        Button btn_ok = (Button) dialog2.findViewById(R.id.btn_ok);

        btn_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });

        dialog2.show();
    }

    public void no_attachment() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.yes_no_dialog);

        Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btn_no = (Button) dialog.findViewById(R.id.btn_no);
        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_question);
        dialog.show();

        text.setText("Are you sure you want to submit?");

        // if button is clicked, close the custom dialog
        btn_yes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                withAttachment = false;

                if (new Fields_Checker().isModuleComplete(record_id, context)) {
                    new SendData().execute();

                } else {
                    Toast.makeText(getApplicationContext(), "Module fields incomplete", Toast.LENGTH_LONG).show();
                }
                dialog.dismiss();
            }
        });
        btn_no.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void update_dialog() {
        // custom dialog
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.yes_no_dialog);

        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.tv_question);
        text.setText("Are you sure you want to submit ?");
        Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btn_no = (Button) dialog.findViewById(R.id.btn_no);

        // if button is clicked, close the custom dialog
        btn_yes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new Fields_Checker().isModuleComplete(record_id, context)) {
                    new SendData().execute();

                } else {
                    Toast.makeText(getApplicationContext(), "Module fields incomplete", Toast.LENGTH_LONG).show();
                }
                dialog.dismiss();
            }
        });
        btn_no.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private class Attachment_validation extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pDialog = new ProgressDialog(Motor_vehicle.this);
            pDialog.setMessage("Checking attachment..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            // TODO Auto-generated method stub

            try {
                URL obj = new URL(url_webby);
                HttpsURLConnection con;
                HttpURLConnection con2;
                if (Global.type.contentEquals("tls")) {
                    con = tlscon.setUpHttpsConnection("" + obj);
                    con.setRequestMethod("GET");
                    con.setRequestProperty("User-Agent", "Mozilla/5.0");
                    responseCode = con.getResponseCode();
                } else {
                    con2 = (HttpURLConnection) obj.openConnection();
                    con2.setRequestMethod("GET");
                    con2.setRequestProperty("User-Agent", "Mozilla/5.0");
                    responseCode = con2.getResponseCode();
                }
                Log.e("", String.valueOf(responseCode));
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            Log.e("", url_webby);
            if (responseCode == 200) {
                view_download();
            } else if (responseCode == 404) {
                Toast.makeText(getApplicationContext(),
                        "Attachment doesn't exist", Toast.LENGTH_SHORT).show();
            }
            pDialog.dismiss();

        }
    }

	/*public void google_ping() {
		try {
//			URL obj = new URL("https://www.google.com.ph/?gws_rd=cr&ei=td9kUrP0H8mjigenuoHAAg");
			URL obj = new URL(gs.server_url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");

			// add request header
			con.setRequestProperty("User-Agent", "Mozilla/5.0");

			google_response = String.valueOf(con.getResponseCode());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/

    /**
     * Showing Dialog
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type:
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.setCanceledOnTouchOutside(false);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    /**
     * Background Async Task to download file
     */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                // Output stream to write file
                OutputStream output = new FileOutputStream(
                        uploaded_file.toString());

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            removeDialog(progress_bar_type);
            Toast.makeText(getApplicationContext(), "Attachment Downloaded",
                    Toast.LENGTH_LONG).show();
            view_pdf(uploaded_file);

        }

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        Intent in;
        switch (v.getId()) {
            case R.id.btn_update_car_details: //zonal values
                custom_dialog_car_details();
                break;
            case R.id.btn_update_app_details: //zonal values
                custom_dialog_app_details();
                break;
            case R.id.btn_report_page1:
                custom_dialog_page1();
                break;
            case R.id.btn_report_page2:
                in = new Intent(getApplicationContext(), Motor_Vehicle_Comparatives.class);
                in.putExtra(TAG_RECORD_ID, record_id);
                startActivityForResult(in, 100);
                break;
            case R.id.btn_report_page3:
                custom_dialog_page3();
                break;
            case R.id.btn_mysql_attachments: //attachments
                //in = new Intent(getApplicationContext(), Uploaded_Attachments_All.class);
                in = new Intent(getApplicationContext(), Attachments_Inflater.class);
                in.putExtra(TAG_RECORD_ID, record_id);
                in.putExtra(TAG_PASS_STAT, pass_stat);
                in.putExtra(TAG_UID, uid);
                startActivityForResult(in, 100);
                break;
            case R.id.btn_report_update_cc:
                //update_dialog();
                check_attachment();
                break;
            case R.id.btn_report_view_report:
                globalSave = true;
                new GlobalSave().execute();
                break;
            case R.id.btn_report_pdf:
                //create_pdf();
                Intent createPDF = new Intent(getApplicationContext(), Collage.class);//.putExtra("position", position);
                createPDF.putExtra(TAG_RECORD_ID, record_id);
                startActivity(createPDF);
                break;
            case R.id.btn_select_pdf:
                open_customdialog();
                break;
            case R.id.tv_attachment:
                open_pdf();
                break;
            case R.id.btn_manual_delete:
                manual_delete_all();
                break;

            //summary
            case R.id.tv_market_valuation_remarks:
                remarks_dialog();
                break;
            default:
                break;
        }
    }




    /**
     * for REMARKS
     */
    CheckBox foc_cb0, foc_cb1, foc_cb2, foc_cb3, foc_cb4, foc_cb5, foc_cb6, foc_cb7, foc_cb8, foc_cb9, foc_cb10, foc_cb11,
            foc_cb12, foc_cb13, foc_cb14, foc_cb15, foc_cb16, foc_cb17;
    Button b_ok, b_cancel;
    View v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15, v16, v17;

    public void dialog_declaration() {
        myDialog2 = new Dialog(Motor_vehicle.this);
        myDialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog2.setContentView(R.layout.mv_summary);
        myDialog2.setCancelable(true);

        android.view.WindowManager.LayoutParams params = myDialog2.getWindow()
                .getAttributes();
        params.height = LayoutParams.WRAP_CONTENT;
        params.width = LayoutParams.MATCH_PARENT;
        myDialog2.getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        foc_cb0 = (CheckBox) myDialog2.findViewById(R.id.foc_cb0);
        foc_cb1 = (CheckBox) myDialog2.findViewById(R.id.foc_cb1);
        foc_cb2 = (CheckBox) myDialog2.findViewById(R.id.foc_cb2);
        foc_cb3 = (CheckBox) myDialog2.findViewById(R.id.foc_cb3);
        foc_cb4 = (CheckBox) myDialog2.findViewById(R.id.foc_cb4);
        foc_cb5 = (CheckBox) myDialog2.findViewById(R.id.foc_cb5);
        foc_cb6 = (CheckBox) myDialog2.findViewById(R.id.foc_cb6);
        foc_cb7 = (CheckBox) myDialog2.findViewById(R.id.foc_cb7);
        foc_cb8 = (CheckBox) myDialog2.findViewById(R.id.foc_cb8);
        foc_cb9 = (CheckBox) myDialog2.findViewById(R.id.foc_cb9);
        foc_cb10 = (CheckBox) myDialog2.findViewById(R.id.foc_cb10);
        foc_cb11 = (CheckBox) myDialog2.findViewById(R.id.foc_cb11);
        foc_cb12 = (CheckBox) myDialog2.findViewById(R.id.foc_cb12);
        foc_cb13 = (CheckBox) myDialog2.findViewById(R.id.foc_cb13);
        foc_cb14 = (CheckBox) myDialog2.findViewById(R.id.foc_cb14);
        foc_cb15 = (CheckBox) myDialog2.findViewById(R.id.foc_cb15);
        foc_cb16 = (CheckBox) myDialog2.findViewById(R.id.foc_cb16);
        foc_cb17 = (CheckBox) myDialog2.findViewById(R.id.foc_cb17);


        v0 = (View) myDialog2.findViewById(R.id.v0);
        v1 = (View) myDialog2.findViewById(R.id.v1);
        v2 = (View) myDialog2.findViewById(R.id.v2);
        v3 = (View) myDialog2.findViewById(R.id.v3);
        v4 = (View) myDialog2.findViewById(R.id.v4);
        v5 = (View) myDialog2.findViewById(R.id.v5);
        v6 = (View) myDialog2.findViewById(R.id.v6);
        v7 = (View) myDialog2.findViewById(R.id.v7);
        v8 = (View) myDialog2.findViewById(R.id.v8);
        v9 = (View) myDialog2.findViewById(R.id.v9);
        v10 = (View) myDialog2.findViewById(R.id.v10);
        v11 = (View) myDialog2.findViewById(R.id.v11);
        v12 = (View) myDialog2.findViewById(R.id.v12);
        v13 = (View) myDialog2.findViewById(R.id.v13);
        v14 = (View) myDialog2.findViewById(R.id.v14);
        v15 = (View) myDialog2.findViewById(R.id.v15);
        v16 = (View) myDialog2.findViewById(R.id.v16);
        v17 = (View) myDialog2.findViewById(R.id.v17);

        b_ok = (Button) myDialog2.findViewById(R.id.b_ok);
        b_cancel = (Button) myDialog2.findViewById(R.id.b_cancel);
    }

    private void remarks_dialog() {
        dialog_declaration();


        Resources res = getResources();
        final String[] ary_remarks = res.getStringArray(R.array.ary_mv_remarks);

        final CheckBox[] foc_cb = {
                foc_cb0,
                foc_cb1,
                foc_cb2,
                foc_cb3,
                foc_cb4,
                foc_cb5,
                foc_cb6,
                foc_cb7,
                foc_cb8,
                foc_cb9,
                foc_cb10,
                foc_cb11,
                foc_cb12,
                foc_cb13,
                foc_cb14,
                foc_cb15,
                foc_cb16,
                foc_cb17
        };

        for (int x = 0; x < ary_remarks.length; x++) {
            foc_cb[x].setText(ary_remarks[x]);
        }
        b_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String foc = "";
                int number = 1;
                for (int x = 0; x < ary_remarks.length; x++) {
                    if (foc_cb[x].isChecked()) {
                        foc = foc + "-" + ary_remarks[x] + "\n";
                        number++;
                    }
                }
                String foc_final = report_market_valuation_remarks.getText().toString();
                foc_final = foc_final + "\n" + foc;
                report_market_valuation_remarks.setText(foc_final);
                myDialog2.dismiss();
            }
        });
        b_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog2.dismiss();
            }
        });
        myDialog2.show();
    }

    public void statusDisplay() {
        List<Logs> sd = db.getLogsLatest(record_id);
        if (!sd.isEmpty()) {
            for (Logs si : sd) {
                actionBar.setSubtitle(si.getstatus_reason() + " (" + si.getdate_submitted() + ")");
            }
        } else {
            actionBar.setSubtitle("Not yet submitted");
        }
    }

    public void logsDisplay() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.submission_logs);
        android.view.WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.height = LayoutParams.WRAP_CONTENT;
        params.width = LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
        TextView tvLog = (TextView) dialog.findViewById(R.id.tvLog);
        String logs = "";
        List<Logs> sd = db.getLogs(record_id);
        if (!sd.isEmpty()) {
            for (Logs si : sd) {
                logs = logs + si.getdate_submitted() + "\t" + si.getstatus() + " - " + si.getstatus_reason() + "\n";
            }
        }
        tvLog.setText(logs);
        dialog.show();
    }


    @Override
    public void onUserInteraction() {
        st.resetDisconnectTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        st.stopDisconnectTimer();
    }

    @Override
    public void onResume() {
        super.onResume();
        st.resetDisconnectTimer();
    }


}
