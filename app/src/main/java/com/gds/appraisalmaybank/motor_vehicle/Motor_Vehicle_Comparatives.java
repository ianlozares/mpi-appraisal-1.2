package com.gds.appraisalmaybank.motor_vehicle;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database.Motor_Vehicle_API;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import java.util.ArrayList;
import java.util.List;

public class Motor_Vehicle_Comparatives extends Activity implements OnClickListener {
    DatabaseHandler db = new DatabaseHandler(this);
    DatabaseHandler2 db2 = new DatabaseHandler2(this);
    GDS_methods gds = new GDS_methods();
    String record_id = "";
    private static final String TAG_RECORD_ID = "record_id";
    String comp1_price,comp2_price,comp3_price;
    Session_Timer st = new Session_Timer(this);
    Button btn_comp1, btn_comp2, btn_comp3, btn_save, btn_compute;
    EditText report_average, report_rec_luxury_vehicle, report_rec_mileage_factor, report_rec_taxi_use, report_rec_for_hire, report_rec_observed_condition,
            report_rec_repo, report_rec_other_desc, report_rec_other_val,report_rec_other2_desc, report_rec_other2_val,report_rec_other3_desc, report_rec_other3_val, report_market_resistance_rec_total, report_market_resistance_total,
            report_rec_market_resistance_net_total, report_rep_stereo, report_rep_speakers, report_rep_tires_pcs, report_rep_tires, report_rep_side_mirror_pcs, report_rep_side_mirror,
            report_rep_light, report_rep_tools, report_rep_battery, report_rep_plates, report_rep_bumpers, report_rep_windows, report_rep_body, report_rep_engine_wash, report_rep_other_desc,
            report_rep_other, report_rep_total, report_appraised_value,valrep_mv_comp_temp_ave_index_price,valrep_mv_comp_add_index_price_desc,valrep_mv_comp_add_index_price,
            valrep_mv_rep_other2_desc,valrep_mv_rep_other2,valrep_mv_rep_other3_desc,valrep_mv_rep_other3;
    String market_resistance_net_total;
    //myDialog Objects
    String dialog_title = "";
    Dialog myDialog;
    EditText report_source, report_tel_no, report_unit_model, report_mileage,
            report_price_min, report_price_max, report_price,
            report_less_amt, report_less_net, report_add_amt,
            report_add_net, report_mileage_value,report_time_adj_value, report_mileage_amt,report_time_adj_amt, report_adj_value,
            report_adj_amt, report_adj_desc,report_index_price,valrep_mv_comp_new_unit_value,valrep_mv_comp_new_unit_amt;

    //computation variables
    String i_price1 = "0", i_price2 = "0", i_price3 = "0";

    String where ="WHERE record_id = args";
    String[] args = new String[1];

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        save_average();
        save_comparatives();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mv_comparatives);

        st.resetDisconnectTimer();
        btn_comp1 = (Button) findViewById(R.id.btn_comp1);
        btn_comp1.setOnClickListener(this);
        btn_comp2 = (Button) findViewById(R.id.btn_comp2);
        btn_comp2.setOnClickListener(this);
        btn_comp3 = (Button) findViewById(R.id.btn_comp3);
        btn_comp3.setOnClickListener(this);


        btn_compute = (Button) findViewById(R.id.btn_compute);
        btn_compute.setOnClickListener(this);
        btn_save = (Button) findViewById(R.id.btn_save);
        btn_save.setOnClickListener(this);

        report_average = (EditText) findViewById(R.id.report_average);
        valrep_mv_comp_temp_ave_index_price = (EditText) findViewById(R.id.valrep_mv_comp_temp_ave_index_price);
        valrep_mv_comp_add_index_price_desc = (EditText) findViewById(R.id.valrep_mv_comp_add_index_price_desc);
        valrep_mv_comp_add_index_price = (EditText) findViewById(R.id.valrep_mv_comp_add_index_price);
        report_rec_luxury_vehicle = (EditText) findViewById(R.id.report_rec_luxury_vehicle);
        report_rec_mileage_factor = (EditText) findViewById(R.id.report_rec_mileage_factor);
        report_rec_taxi_use = (EditText) findViewById(R.id.report_rec_taxi_use);
        report_rec_for_hire = (EditText) findViewById(R.id.report_rec_for_hire);
        report_rec_observed_condition = (EditText) findViewById(R.id.report_rec_observed_condition);
        report_rec_repo = (EditText) findViewById(R.id.report_rec_repo);
        report_rec_other_desc = (EditText) findViewById(R.id.report_rec_other_desc);
        report_rec_other_val = (EditText) findViewById(R.id.report_rec_other_val);
        report_rec_other2_desc = (EditText) findViewById(R.id.report_rec_other2_desc);
        report_rec_other2_val = (EditText) findViewById(R.id.report_rec_other2_val);
        report_rec_other3_desc = (EditText) findViewById(R.id.report_rec_other3_desc);
        report_rec_other3_val = (EditText) findViewById(R.id.report_rec_other3_val);
        report_market_resistance_rec_total = (EditText) findViewById(R.id.report_market_resistance_rec_total);
        report_market_resistance_total = (EditText) findViewById(R.id.report_market_resistance_total);
        report_rec_market_resistance_net_total = (EditText) findViewById(R.id.report_rec_market_resistance_net_total);
        report_rep_stereo = (EditText) findViewById(R.id.report_rep_stereo);
        report_rep_speakers = (EditText) findViewById(R.id.report_rep_speakers);
        report_rep_tires_pcs = (EditText) findViewById(R.id.report_rep_tires_pcs);
        report_rep_tires = (EditText) findViewById(R.id.report_rep_tires);
        report_rep_side_mirror_pcs = (EditText) findViewById(R.id.report_rep_side_mirror_pcs);
        report_rep_side_mirror = (EditText) findViewById(R.id.report_rep_side_mirror);
        report_rep_light = (EditText) findViewById(R.id.report_rep_light);
        report_rep_tools = (EditText) findViewById(R.id.report_rep_tools);
        report_rep_battery = (EditText) findViewById(R.id.report_rep_battery);
        report_rep_plates = (EditText) findViewById(R.id.report_rep_plates);
        report_rep_bumpers = (EditText) findViewById(R.id.report_rep_bumpers);
        report_rep_windows = (EditText) findViewById(R.id.report_rep_windows);
        report_rep_body = (EditText) findViewById(R.id.report_rep_body);
        report_rep_engine_wash = (EditText) findViewById(R.id.report_rep_engine_wash);
        report_rep_other_desc = (EditText) findViewById(R.id.report_rep_other_desc);
        report_rep_other = (EditText) findViewById(R.id.report_rep_other);

        valrep_mv_rep_other2_desc = (EditText) findViewById(R.id.valrep_mv_rep_other2_desc);
        valrep_mv_rep_other2 = (EditText) findViewById(R.id.valrep_mv_rep_other2);
        valrep_mv_rep_other3_desc = (EditText) findViewById(R.id.valrep_mv_rep_other3_desc);
        valrep_mv_rep_other3 = (EditText) findViewById(R.id.valrep_mv_rep_other3);

        report_rep_total = (EditText) findViewById(R.id.report_rep_total);
        report_appraised_value = (EditText) findViewById(R.id.report_appraised_value);
        Intent i = getIntent();
        record_id = i.getStringExtra(TAG_RECORD_ID);
        args[0]=record_id;
        fill_data();
        textwatch_rec();
        textwatch_rep();
        valrep_mv_comp_add_index_price.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(valrep_mv_comp_add_index_price, this, s, current);
                textwatch_rec();
                textwatch_rep();

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_average.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(report_average, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        report_rec_luxury_vehicle.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                textwatch_rec();
                textwatch_rep();

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_rec_mileage_factor.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                textwatch_rec();
                textwatch_rep();

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_rec_taxi_use.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                textwatch_rec();
                textwatch_rep();

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_rec_for_hire.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                textwatch_rec();
                textwatch_rep();

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_rec_observed_condition.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                textwatch_rec();
                textwatch_rep();

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_rec_repo.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                textwatch_rec();
                textwatch_rep();


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_rec_other_val.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                textwatch_rec();
                textwatch_rep();

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_rec_other2_val.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                textwatch_rec();
                textwatch_rep();

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_rec_other3_val.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                textwatch_rec();
                textwatch_rep();

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_market_resistance_total.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(report_market_resistance_total, this, s, current);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });
        report_rec_market_resistance_net_total.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(report_rec_market_resistance_net_total, this, s, current);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });


        report_rep_stereo.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(report_rep_stereo, this, s, current);
                textwatch_rec();
                textwatch_rep();

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_rep_speakers.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(report_rep_speakers, this, s, current);

                textwatch_rec();
                textwatch_rep();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_rep_tires.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(report_rep_tires, this, s, current);

                textwatch_rec();
                textwatch_rep();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_rep_side_mirror.addTextChangedListener(new TextWatcher() {
            String current="";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(report_rep_side_mirror, this, s, current);
                textwatch_rec();
                textwatch_rep();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_rep_light.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(report_rep_light, this, s, current);
                textwatch_rec();
                textwatch_rep();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_rep_tools.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(report_rep_tools, this, s, current);
                textwatch_rec();
                textwatch_rep();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_rep_battery.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(report_rep_battery, this, s, current);
                textwatch_rec();
                textwatch_rep();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_rep_plates.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(report_rep_plates, this, s, current);
                textwatch_rec();
                textwatch_rep();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_rep_bumpers.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(report_rep_bumpers, this, s, current);

                textwatch_rec();
                textwatch_rep();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_rep_windows.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(report_rep_windows, this, s, current);
                textwatch_rec();
                textwatch_rep();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_rep_body.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(report_rep_body, this, s, current);
                textwatch_rec();
                textwatch_rep();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_rep_engine_wash.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(report_rep_engine_wash, this, s, current);
                textwatch_rec();
                textwatch_rep();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_rep_other.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(report_rep_other, this, s, current);
                textwatch_rec();
                textwatch_rep();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        valrep_mv_rep_other2.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(valrep_mv_rep_other2, this, s, current);
                textwatch_rec();
                textwatch_rep();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        valrep_mv_rep_other3.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(valrep_mv_rep_other3, this, s, current);

                textwatch_rep();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });


        report_rep_total.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(report_rep_total, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });
        report_appraised_value.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(report_appraised_value, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });

       /* comp1_price=db2.getRecord("report_comp1_price",where,args,"motor_vehicle").get(0);
        comp2_price=db2.getRecord("report_comp2_price",where,args,"motor_vehicle").get(0);
        comp3_price=db2.getRecord("report_comp3_price",where,args,"motor_vehicle").get(0);
*/
       /* if(comp1_price.contentEquals("")||comp1_price.contentEquals("0")||comp1_price.contentEquals("0.00")){
            btn_comp1.setBackgroundColor(Color.RED);
        }
        if(comp2_price.contentEquals("")||comp2_price.contentEquals("0")||comp2_price.contentEquals("0.00")){
            btn_comp2.setBackgroundColor(Color.RED);
        }
        if(comp3_price.contentEquals("")||comp3_price.contentEquals("0")||comp3_price.contentEquals("0.00")){
            btn_comp3.setBackgroundColor(Color.RED);
        }*/

    }

    @Override
    public void onClick(View arg0) {
        // TODO Auto-generated method stub
        switch (arg0.getId()) {
            case R.id.btn_comp1:
                dialog_title = "Data Comparative 1";
                view_comp1();
                break;
            case R.id.btn_comp2:
                dialog_title = "Data Comparative 2";
                view_comp2();
                break;
            case R.id.btn_comp3:
                dialog_title = "Data Comparative 3";
                view_comp3();
                break;

            case R.id.btn_save:
               /* boolean fieldOk1,fieldOk2,fieldOk3;*/
                save_comparatives();
                save_market_net();
                finish();
               /* comp1_price=db2.getRecord("report_comp1_price",where,args,"motor_vehicle").get(0);
                comp2_price=db2.getRecord("report_comp2_price",where,args,"motor_vehicle").get(0);
                comp3_price=db2.getRecord("report_comp3_price",where,args,"motor_vehicle").get(0);*/
                /*if(comp1_price.contentEquals("")||comp1_price.contentEquals("0")||comp1_price.contentEquals("0.00")){
                    btn_comp1.setBackgroundColor(Color.RED);
                    fieldOk1=false;
                }else{
                    fieldOk1=true;
                }
                if(comp2_price.contentEquals("")||comp2_price.contentEquals("0")||comp2_price.contentEquals("0.00")){
                    btn_comp2.setBackgroundColor(Color.RED);
                    fieldOk2=false;
                }else{
                    fieldOk2=true;
                }
                if(comp3_price.contentEquals("")||comp3_price.contentEquals("0")||comp3_price.contentEquals("0.00")){
                    btn_comp3.setBackgroundColor(Color.RED);
                    fieldOk3=false;
                }else{
                    fieldOk3=true;
                }
                if(fieldOk1&&fieldOk2&&fieldOk3){
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(),
                            "Please fill up required fields", Toast.LENGTH_SHORT).show();
                }*/

                break;
            default:
                break;
        }
    }

    public void fill_data() {
        List<Motor_Vehicle_API> vl = db.getMotor_Vehicle(String.valueOf(record_id));
        if (!vl.isEmpty()) {
            for (Motor_Vehicle_API im : vl) {
                report_average.setText(gds.numberFormat(im.getreport_comp_ave_index_price()));
                valrep_mv_comp_add_index_price_desc.setText(db2.getRecord("valrep_mv_comp_add_index_price_desc",where,args,"motor_vehicle").get(0));
                valrep_mv_comp_add_index_price.setText(gds.numberFormat(db2.getRecord("valrep_mv_comp_add_index_price",where,args,"motor_vehicle").get(0)));
                valrep_mv_comp_temp_ave_index_price.setText(gds.numberFormat(db2.getRecord("valrep_mv_comp_temp_ave_index_price",where,args,"motor_vehicle").get(0)));

                valrep_mv_rep_other2_desc.setText(db2.getRecord("valrep_mv_rep_other2_desc",where,args,"motor_vehicle").get(0));
                valrep_mv_rep_other2.setText(gds.numberFormat(db2.getRecord("valrep_mv_rep_other2",where,args,"motor_vehicle").get(0)));
                valrep_mv_rep_other3_desc.setText(db2.getRecord("valrep_mv_rep_other3_desc",where,args,"motor_vehicle").get(0));
                valrep_mv_rep_other3.setText(gds.numberFormat(db2.getRecord("valrep_mv_rep_other3",where,args,"motor_vehicle").get(0)));


                report_rec_luxury_vehicle.setText(im.getreport_rec_lux_car());
                report_rec_mileage_factor.setText("0");
                report_rec_taxi_use.setText(im.getreport_rec_taxi_use());
                report_rec_for_hire.setText(im.getreport_rec_for_hire());
                report_rec_observed_condition.setText(im.getreport_rec_condition());
                report_rec_repo.setText(im.getreport_rec_repo());
                report_rec_other_desc.setText(im.getreport_rec_other_desc());
                report_rec_other_val.setText(im.getreport_rec_other_val());
                report_rec_other2_desc.setText(im.getreport_rec_other2_desc());
                report_rec_other2_val.setText(im.getreport_rec_other2_val());
                report_rec_other3_desc.setText(im.getreport_rec_other3_desc());
                report_rec_other3_val.setText(im.getreport_rec_other3_val());
                report_market_resistance_rec_total.setText(im.getreport_rec_market_resistance_rec_total());
                report_market_resistance_total.setText(gds.numberFormat(im.getreport_rec_market_resistance_total()));
                report_rec_market_resistance_net_total.setText(gds.numberFormat(db2.getRecord("report_rec_market_resistance_net_total",where,args,"motor_vehicle").get(0)));
                report_rep_stereo.setText(gds.numberFormat(im.getreport_rep_stereo()));
                report_rep_speakers.setText(gds.numberFormat(im.getreport_rep_speakers()));
                report_rep_tires_pcs.setText(im.getreport_rep_tires_pcs());
                report_rep_tires.setText(gds.numberFormat(im.getreport_rep_tires()));
                report_rep_side_mirror_pcs.setText(im.getreport_rep_side_mirror_pcs());
                report_rep_side_mirror.setText(gds.numberFormat(im.getreport_rep_side_mirror()));
                report_rep_light.setText(gds.numberFormat(im.getreport_rep_light()));
                report_rep_tools.setText(gds.numberFormat(im.getreport_rep_tools()));
                report_rep_battery.setText(gds.numberFormat(im.getreport_rep_battery()));
                report_rep_plates.setText(gds.numberFormat(im.getreport_rep_plates()));
                report_rep_bumpers.setText(gds.numberFormat(im.getreport_rep_bumpers()));
                report_rep_windows.setText(gds.numberFormat(im.getreport_rep_windows()));
                report_rep_body.setText(gds.numberFormat(im.getreport_rep_body()));
                report_rep_engine_wash.setText(gds.numberFormat(im.getreport_rep_engine_wash()));
                report_rep_other_desc.setText(im.getreport_rep_other_desc());
                report_rep_other.setText(gds.numberFormat(im.getreport_rep_other()));
                report_rep_total.setText(gds.numberFormat(im.getreport_rep_total()));
                report_appraised_value.setText(gds.numberFormat(im.getreport_appraised_value()));

            }
        }
    }

    public void view_comp1() {
        myDialog_config();
        List<Motor_Vehicle_API> vl = db.getMotor_Vehicle(String.valueOf(record_id));
        if (!vl.isEmpty()) {
            for (Motor_Vehicle_API im : vl) {
                //date of inspection
                // set current date into datepicker

                report_source.setText(im.getreport_comp1_source());
                report_tel_no.setText(im.getreport_comp1_tel_no());
                report_unit_model.setText(im.getreport_comp1_unit_model());
                report_mileage.setText(im.getreport_comp1_mileage());
                report_price_min.setText("0");
                report_price_max.setText("0");
                report_price.setText(gds.numberFormat(im.getreport_comp1_price()));
                report_less_amt.setText(gds.numberFormat(im.getreport_comp1_less_amt()));
                report_less_net.setText(gds.numberFormat(im.getreport_comp1_less_net()));
                report_add_amt.setText(gds.numberFormat(im.getreport_comp1_add_amt()));
                report_add_net.setText(gds.numberFormat(im.getreport_comp1_add_net()));
                report_time_adj_value.setText(im.getreport_comp1_time_adj_value());
                report_mileage_value.setText(im.getreport_comp1_mileage_value());
                report_time_adj_amt.setText(gds.numberFormat(im.getreport_comp1_time_adj_amt()));
                report_mileage_amt.setText(gds.numberFormat(im.getreport_comp1_mileage_amt()));
                report_adj_value.setText(im.getreport_comp1_adj_value());
                report_adj_amt.setText(gds.numberFormat(im.getreport_comp1_adj_amt()));
                report_adj_desc.setText(im.getreport_comp1_adj_desc());
                report_index_price.setText(gds.numberFormat(im.getreport_comp1_index_price()));
                String unit_val1=db2.getRecord("valrep_mv_comp1_new_unit_value",where,args,"motor_vehicle").get(0);
                String unit_amt1=db2.getRecord("valrep_mv_comp1_new_unit_amt",where,args,"motor_vehicle").get(0);
                valrep_mv_comp_new_unit_value.setText(unit_val1);
                valrep_mv_comp_new_unit_amt.setText(gds.numberFormat(unit_amt1));

            }
        }
        //TEXT WATCHER ADDED By IAN

        report_price.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(report_price, this, s, current);

                textwatch_comp1_5();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_less_amt.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                current = gds.numberFormat(report_less_amt, this, s, current);

                textwatch_comp1_5();

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_less_net.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                current = gds.numberFormat(report_less_net, this, s, current);

                //textwatch_comp1_5();


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_add_amt.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                current = gds.numberFormat(report_add_amt, this, s, current);
                textwatch_comp1_5();


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_add_net.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                current = gds.numberFormat(report_add_net, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_time_adj_value.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if(!report_time_adj_value.getText().toString().contentEquals("-")){
                    if(!report_time_adj_value.getText().toString().contentEquals(".")){
                        if(!report_time_adj_value.getText().toString().contentEquals("-.")){
                            textwatch_comp1_5();
                        }
                    }
                }



            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_mileage_value.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if(!report_mileage_value.getText().toString().contentEquals("-")){
                    if(!report_mileage_value.getText().toString().contentEquals(".")){
                        if(!report_mileage_value.getText().toString().contentEquals("-.")){
                            textwatch_comp1_5();
                        }
                    }
                }



            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_adj_value.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if(!report_adj_value.getText().toString().contentEquals("-")){
                    if(!report_adj_value.getText().toString().contentEquals(".")){
                        if(!report_adj_value.getText().toString().contentEquals("-.")) {
                            textwatch_comp1_5();
                        }
                    }
                }



            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_time_adj_amt.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(report_time_adj_amt, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });
        report_mileage_amt.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(report_mileage_amt, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });
        report_adj_amt.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(report_adj_amt, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });
        report_index_price.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(report_index_price, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });

        valrep_mv_comp_new_unit_value.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if(!valrep_mv_comp_new_unit_value.getText().toString().contentEquals("-")){
                    if(!valrep_mv_comp_new_unit_value.getText().toString().contentEquals(".")){
                        if(!valrep_mv_comp_new_unit_value.getText().toString().contentEquals("-.")){
                            textwatch_comp1_5();
                        }
                    }
                }



            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        valrep_mv_comp_new_unit_amt.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(valrep_mv_comp_new_unit_amt, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });


        btn_save.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //list of EditTexts put in array
                if (report_mileage_value.getText().length() == 0) {
                    report_mileage_value.setText("0");
                    report_mileage_amt.setText("0.00");
                }
                if (report_time_adj_value.getText().length() == 0) {
                    report_time_adj_value.setText("0");
                    report_time_adj_amt.setText("0.00");
                }
                if (report_adj_value.getText().length() == 0) {
                    report_adj_value.setText("0");
                    report_adj_amt.setText("0.00");
                }if (valrep_mv_comp_new_unit_value.getText().length() == 0) {
                    valrep_mv_comp_new_unit_value.setText("0");
                    valrep_mv_comp_new_unit_amt.setText("0.00");
                }
                if (report_adj_desc.getText().length() == 0) {
                    report_adj_desc.setText("");
                }
               /* gds.fill_in_error(new EditText[]{report_source, report_price, report_less_amt,report_add_amt});
                gds.fill_in_error_price(new EditText[]{report_price});
                boolean fieldsOK2 = validate_price(new EditText[]{report_price});
                boolean fieldsOK = gds.validate(new EditText[]{report_source, report_price, report_less_amt,report_add_amt});*/
              /*  if (fieldsOK&&fieldsOK2) { *///check if all EditTexts in array are filled up
                    //update data in SQLite
                    Motor_Vehicle_API vl = new Motor_Vehicle_API();

                    vl.setreport_comp1_source(report_source.getText().toString());
                    vl.setreport_comp1_tel_no(report_tel_no.getText().toString());
                    vl.setreport_comp1_unit_model(report_unit_model.getText().toString());
                    vl.setreport_comp1_mileage(report_mileage.getText().toString());
                    vl.setreport_comp1_price_min(gds.replaceFormat(report_price_min.getText().toString()));
                    vl.setreport_comp1_price_max(gds.replaceFormat(report_price_max.getText().toString()));
                    vl.setreport_comp1_price(gds.replaceFormat(report_price.getText().toString()));
                    vl.setreport_comp1_less_amt(gds.replaceFormat(report_less_amt.getText().toString()));
                    vl.setreport_comp1_less_net(gds.replaceFormat(report_less_net.getText().toString()));
                    vl.setreport_comp1_add_amt(gds.replaceFormat(report_add_amt.getText().toString()));
                    vl.setreport_comp1_add_net(gds.replaceFormat(report_add_net.getText().toString()));
                    vl.setreport_comp1_time_adj_value(report_time_adj_value.getText().toString());
                    vl.setreport_comp1_time_adj_amt(gds.replaceFormat(report_time_adj_amt.getText().toString()));
                    vl.setreport_comp1_adj_value(report_adj_value.getText().toString());
                    vl.setreport_comp1_adj_amt(gds.replaceFormat(report_adj_amt.getText().toString()));
                    vl.setreport_comp1_index_price(gds.replaceFormat(report_index_price.getText().toString()));
                    vl.setreport_comp1_mileage_value(report_mileage_value.getText().toString());
                    vl.setreport_comp1_mileage_amt(gds.replaceFormat(report_mileage_amt.getText().toString()));
                    vl.setreport_comp1_adj_desc(report_adj_desc.getText().toString());

                    vl.setvalrep_mv_comp1_new_unit_value(valrep_mv_comp_new_unit_value.getText().toString());
                    vl.setvalrep_mv_comp1_new_unit_amt(gds.replaceFormat(valrep_mv_comp_new_unit_amt.getText().toString()));

                    db.updateMotor_Vehicle_comp1(vl, record_id);
                    db.close();
                    myDialog.dismiss();
                    compute_average();
                    save_comparatives();
                    finish();
                    Intent in = new Intent(getApplicationContext(), Motor_Vehicle_Comparatives.class);
                    in.putExtra(TAG_RECORD_ID, record_id);
                    startActivityForResult(in, 100);
                Toast.makeText(getApplicationContext(),
                        "Saved", Toast.LENGTH_SHORT).show();
               /* } else {
                    Toast.makeText(getApplicationContext(),
                            "Please fill up required fields", Toast.LENGTH_SHORT).show();
                }*/
            }
        });
        myDialog.show();
    }

    public void view_comp2() {
        myDialog_config();
        List<Motor_Vehicle_API> vl = db.getMotor_Vehicle(String.valueOf(record_id));
        if (!vl.isEmpty()) {
            for (Motor_Vehicle_API im : vl) {
                //date of inspection
                // set current date into datepicker

                report_source.setText(im.getreport_comp2_source());
                report_tel_no.setText(im.getreport_comp2_tel_no());
                report_unit_model.setText(im.getreport_comp2_unit_model());
                report_mileage.setText(im.getreport_comp2_mileage());
                report_price_min.setText("0");
                report_price_max.setText("0");
                report_price.setText(gds.numberFormat(im.getreport_comp2_price()));
                report_less_amt.setText(gds.numberFormat(im.getreport_comp2_less_amt()));
                report_less_net.setText(gds.numberFormat(im.getreport_comp2_less_net()));

                report_add_amt.setText(gds.numberFormat(im.getreport_comp2_add_amt()));
                report_add_net.setText(gds.numberFormat(im.getreport_comp2_add_net()));
                report_time_adj_value.setText(im.getreport_comp2_time_adj_value());
                report_mileage_value.setText(im.getreport_comp2_mileage_value());
                report_time_adj_amt.setText(gds.numberFormat(im.getreport_comp2_time_adj_amt()));
                report_mileage_amt.setText(gds.numberFormat(im.getreport_comp2_mileage_amt()));
                report_adj_value.setText(im.getreport_comp2_adj_value());
                report_adj_amt.setText(gds.numberFormat(im.getreport_comp2_adj_amt()));
                report_adj_desc.setText(im.getreport_comp2_adj_desc());
                report_index_price.setText(gds.numberFormat(im.getreport_comp2_index_price()));

                String unit_val2=db2.getRecord("valrep_mv_comp2_new_unit_value",where,args,"motor_vehicle").get(0);
                String unit_amt2=db2.getRecord("valrep_mv_comp2_new_unit_amt",where,args,"motor_vehicle").get(0);
                valrep_mv_comp_new_unit_value.setText(unit_val2);
                valrep_mv_comp_new_unit_amt.setText(gds.numberFormat(unit_amt2));

            }
        }

        //TEXT WATCHER ADDED By IAN

        report_price.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(report_price, this, s, current);

                textwatch_comp1_5();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_less_amt.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                current = gds.numberFormat(report_less_amt, this, s, current);

                textwatch_comp1_5();

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_less_net.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                current = gds.numberFormat(report_less_net, this, s, current);

                //textwatch_comp1_5();


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_add_amt.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                current = gds.numberFormat(report_add_amt, this, s, current);
                textwatch_comp1_5();


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_add_net.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                current = gds.numberFormat(report_add_net, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_time_adj_value.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if(!report_time_adj_value.getText().toString().contentEquals("-")){
                    if(!report_time_adj_value.getText().toString().contentEquals(".")){
                        if(!report_time_adj_value.getText().toString().contentEquals("-.")){
                            textwatch_comp1_5();
                        }
                    }
                }



            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_mileage_value.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if(!report_mileage_value.getText().toString().contentEquals("-")){
                    if(!report_mileage_value.getText().toString().contentEquals(".")){
                        if(!report_mileage_value.getText().toString().contentEquals("-.")){
                            textwatch_comp1_5();
                        }
                    }
                }



            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_adj_value.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if(!report_adj_value.getText().toString().contentEquals("-")){
                    if(!report_adj_value.getText().toString().contentEquals(".")){
                        if(!report_adj_value.getText().toString().contentEquals("-.")) {
                            textwatch_comp1_5();
                        }
                    }
                }



            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_time_adj_amt.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(report_time_adj_amt, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });
        report_mileage_amt.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(report_mileage_amt, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });
        report_adj_amt.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(report_adj_amt, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });
        report_index_price.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(report_index_price, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });

        valrep_mv_comp_new_unit_value.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if(!valrep_mv_comp_new_unit_value.getText().toString().contentEquals("-")){
                    if(!valrep_mv_comp_new_unit_value.getText().toString().contentEquals(".")){
                        if(!valrep_mv_comp_new_unit_value.getText().toString().contentEquals("-.")){
                            textwatch_comp1_5();
                        }
                    }
                }



            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        valrep_mv_comp_new_unit_amt.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(valrep_mv_comp_new_unit_amt, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });


        btn_save.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (report_mileage_value.getText().length() == 0) {
                    report_mileage_value.setText("0");
                    report_mileage_amt.setText("0.00");
                }
                if (report_time_adj_value.getText().length() == 0) {
                    report_time_adj_value.setText("0");
                    report_time_adj_amt.setText("0.00");
                }
                if (report_adj_value.getText().length() == 0) {
                    report_adj_value.setText("0");
                    report_adj_amt.setText("0.00");
                }if (valrep_mv_comp_new_unit_value.getText().length() == 0) {
                    valrep_mv_comp_new_unit_value.setText("0");
                    valrep_mv_comp_new_unit_amt.setText("0.00");
                }
                if (report_adj_desc.getText().length() == 0) {
                    report_adj_desc.setText("");
                }
               /* gds.fill_in_error(new EditText[]{report_source, report_price, report_less_amt,report_add_amt});
                gds.fill_in_error_price(new EditText[]{report_price});
                boolean fieldsOK2 = validate_price(new EditText[]{report_price});
                boolean fieldsOK = gds.validate(new EditText[]{report_source, report_price, report_less_amt,report_add_amt});
                if (fieldsOK&&fieldsOK2) {*/ //check if all EditTexts in array are filled up
                    //update data in SQLite
                    Motor_Vehicle_API vl = new Motor_Vehicle_API();


                    vl.setreport_comp2_source(report_source.getText().toString());
                    vl.setreport_comp2_tel_no(report_tel_no.getText().toString());
                    vl.setreport_comp2_unit_model(report_unit_model.getText().toString());
                    vl.setreport_comp2_mileage(report_mileage.getText().toString());
                    vl.setreport_comp2_price_min(gds.replaceFormat(report_price_min.getText().toString()));
                    vl.setreport_comp2_price_max(gds.replaceFormat(report_price_max.getText().toString()));
                    vl.setreport_comp2_price(gds.replaceFormat(report_price.getText().toString()));
                    vl.setreport_comp2_less_amt(gds.replaceFormat(report_less_amt.getText().toString()));
                    vl.setreport_comp2_less_net(gds.replaceFormat(report_less_net.getText().toString()));
                    vl.setreport_comp2_add_amt(gds.replaceFormat(report_add_amt.getText().toString()));
                    vl.setreport_comp2_add_net(gds.replaceFormat(report_add_net.getText().toString()));
                    vl.setreport_comp2_time_adj_value(report_time_adj_value.getText().toString());
                    vl.setreport_comp2_time_adj_amt(gds.replaceFormat(report_time_adj_amt.getText().toString()));
                    vl.setreport_comp2_adj_value(report_adj_value.getText().toString());
                    vl.setreport_comp2_adj_amt(gds.replaceFormat(report_adj_amt.getText().toString()));
                    vl.setreport_comp2_index_price(gds.replaceFormat(report_index_price.getText().toString()));
                    vl.setreport_comp2_mileage_value(report_mileage_value.getText().toString());
                    vl.setreport_comp2_mileage_amt(gds.replaceFormat(report_mileage_amt.getText().toString()));
                    vl.setreport_comp2_adj_desc(report_adj_desc.getText().toString());


                    vl.setvalrep_mv_comp2_new_unit_value(valrep_mv_comp_new_unit_value.getText().toString());
                    vl.setvalrep_mv_comp2_new_unit_amt(gds.replaceFormat(valrep_mv_comp_new_unit_amt.getText().toString()));

                    db.updateMotor_Vehicle_comp2(vl, record_id);
                    db.close();
                    myDialog.dismiss();
                    compute_average();
                    save_comparatives();
                    finish();
                    Intent in = new Intent(getApplicationContext(), Motor_Vehicle_Comparatives.class);
                    in.putExtra(TAG_RECORD_ID, record_id);
                    startActivityForResult(in, 100);
                Toast.makeText(getApplicationContext(),
                        "Saved", Toast.LENGTH_SHORT).show();
               /* } else {
                    Toast.makeText(getApplicationContext(),
                            "Please fill up required fields", Toast.LENGTH_SHORT).show();
                }*/
            }
        });
        myDialog.show();
    }

    public void view_comp3() {
        myDialog_config();
        List<Motor_Vehicle_API> vl = db.getMotor_Vehicle(String.valueOf(record_id));
        if (!vl.isEmpty()) {
            for (Motor_Vehicle_API im : vl) {
                //date of inspection
                // set current date into datepicker

                report_source.setText(im.getreport_comp3_source());
                report_tel_no.setText(im.getreport_comp3_tel_no());
                report_unit_model.setText(im.getreport_comp3_unit_model());
                report_mileage.setText(im.getreport_comp3_mileage());
                report_price_min.setText("0");
                report_price_max.setText("0");
                report_price.setText(gds.numberFormat(im.getreport_comp3_price()));
                report_less_amt.setText(gds.numberFormat(im.getreport_comp3_less_amt()));
                report_less_net.setText(gds.numberFormat(im.getreport_comp3_less_net()));
                report_add_amt.setText(gds.numberFormat(im.getreport_comp3_add_amt()));
                report_add_net.setText(gds.numberFormat(im.getreport_comp3_add_net()));
                report_time_adj_value.setText(im.getreport_comp3_time_adj_value());
                report_mileage_value.setText(im.getreport_comp3_mileage_value());
                report_time_adj_amt.setText(gds.numberFormat(im.getreport_comp3_time_adj_amt()));
                report_mileage_amt.setText(gds.numberFormat(im.getreport_comp3_mileage_amt()));
                report_adj_value.setText(im.getreport_comp3_adj_value());
                report_adj_amt.setText(gds.numberFormat(im.getreport_comp3_adj_amt()));
                report_adj_desc.setText(im.getreport_comp3_adj_desc());
                report_index_price.setText(gds.numberFormat(im.getreport_comp3_index_price()));

                String unit_val3=db2.getRecord("valrep_mv_comp3_new_unit_value",where,args,"motor_vehicle").get(0);
                String unit_amt3=db2.getRecord("valrep_mv_comp3_new_unit_amt",where,args,"motor_vehicle").get(0);
                valrep_mv_comp_new_unit_value.setText(unit_val3);
                valrep_mv_comp_new_unit_amt.setText(gds.numberFormat(unit_amt3));
            }
        }

        //TEXT WATCHER ADDED By IAN

        report_price.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(report_price, this, s, current);

                textwatch_comp1_5();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_less_amt.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                current = gds.numberFormat(report_less_amt, this, s, current);

                textwatch_comp1_5();

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_less_net.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                current = gds.numberFormat(report_less_net, this, s, current);

                //textwatch_comp1_5();


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_add_amt.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                current = gds.numberFormat(report_add_amt, this, s, current);
                textwatch_comp1_5();


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_add_net.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                current = gds.numberFormat(report_add_net, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_time_adj_value.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if(!report_time_adj_value.getText().toString().contentEquals("-")){
                    if(!report_time_adj_value.getText().toString().contentEquals(".")){
                        if(!report_time_adj_value.getText().toString().contentEquals("-.")){
                            textwatch_comp1_5();
                        }
                    }
                }



            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_mileage_value.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if(!report_mileage_value.getText().toString().contentEquals("-")){
                    if(!report_mileage_value.getText().toString().contentEquals(".")){
                        if(!report_mileage_value.getText().toString().contentEquals("-.")){
                            textwatch_comp1_5();
                        }
                    }
                }



            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_adj_value.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if(!report_adj_value.getText().toString().contentEquals("-")){
                    if(!report_adj_value.getText().toString().contentEquals(".")){
                        if(!report_adj_value.getText().toString().contentEquals("-.")) {
                            textwatch_comp1_5();
                        }
                    }
                }



            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        report_time_adj_amt.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(report_time_adj_amt, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });
        report_mileage_amt.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(report_mileage_amt, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });
        report_adj_amt.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(report_adj_amt, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });
        report_index_price.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(report_index_price, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });

        valrep_mv_comp_new_unit_value.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if(!valrep_mv_comp_new_unit_value.getText().toString().contentEquals("-")){
                    if(!valrep_mv_comp_new_unit_value.getText().toString().contentEquals(".")){
                        if(!valrep_mv_comp_new_unit_value.getText().toString().contentEquals("-.")){
                            textwatch_comp1_5();
                        }
                    }
                }



            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        valrep_mv_comp_new_unit_amt.addTextChangedListener(new TextWatcher() {

            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int count,
                                      int after) {
                // TODO Auto-generated method stub


                current = gds.numberFormat(valrep_mv_comp_new_unit_amt, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

        });


        btn_save.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (report_mileage_value.getText().length() == 0) {
                    report_mileage_value.setText("0");
                    report_mileage_amt.setText("0.00");
                }
                if (report_time_adj_value.getText().length() == 0) {
                    report_time_adj_value.setText("0");
                    report_time_adj_amt.setText("0.00");
                }
                if (report_adj_value.getText().length() == 0) {
                    report_adj_value.setText("0");
                    report_adj_amt.setText("0.00");
                }
                if (valrep_mv_comp_new_unit_value.getText().length() == 0) {
                    valrep_mv_comp_new_unit_value.setText("0");
                    valrep_mv_comp_new_unit_amt.setText("0.00");
                }
                if (report_adj_desc.getText().length() == 0) {
                    report_adj_desc.setText("");
                }
               /* gds.fill_in_error(new EditText[]{report_source, report_price, report_less_amt,report_add_amt});
                gds.fill_in_error_price(new EditText[]{report_price});
                boolean fieldsOK2 = validate_price(new EditText[]{report_price});
                boolean fieldsOK = gds.validate(new EditText[]{report_source, report_price, report_less_amt,report_add_amt});
                if (fieldsOK&&fieldsOK2) {*/
                    //update data in SQLite
                    Motor_Vehicle_API vl = new Motor_Vehicle_API();


                    vl.setreport_comp3_source(report_source.getText().toString());
                    vl.setreport_comp3_tel_no(report_tel_no.getText().toString());
                    vl.setreport_comp3_unit_model(report_unit_model.getText().toString());
                    vl.setreport_comp3_mileage(report_mileage.getText().toString());
                    vl.setreport_comp3_price_min(gds.replaceFormat(report_price_min.getText().toString()));
                    vl.setreport_comp3_price_max(gds.replaceFormat(report_price_max.getText().toString()));
                    vl.setreport_comp3_price(gds.replaceFormat(report_price.getText().toString()));
                    vl.setreport_comp3_less_amt(gds.replaceFormat(report_less_amt.getText().toString()));
                    vl.setreport_comp3_less_net(gds.replaceFormat(report_less_net.getText().toString()));
                    vl.setreport_comp3_add_amt(gds.replaceFormat(report_add_amt.getText().toString()));
                    vl.setreport_comp3_add_net(gds.replaceFormat(report_add_net.getText().toString()));
                    vl.setreport_comp3_time_adj_value(report_time_adj_value.getText().toString());
                    vl.setreport_comp3_time_adj_amt(gds.replaceFormat(report_time_adj_amt.getText().toString()));
                    vl.setreport_comp3_adj_value(report_adj_value.getText().toString());
                    vl.setreport_comp3_adj_amt(gds.replaceFormat(report_adj_amt.getText().toString()));
                    vl.setreport_comp3_index_price(gds.replaceFormat(report_index_price.getText().toString()));
                    vl.setreport_comp3_mileage_value(report_mileage_value.getText().toString());
                    vl.setreport_comp3_mileage_amt(gds.replaceFormat(report_mileage_amt.getText().toString()));
                    vl.setreport_comp3_adj_desc(report_adj_desc.getText().toString());


                    vl.setvalrep_mv_comp3_new_unit_value(valrep_mv_comp_new_unit_value.getText().toString());
                    vl.setvalrep_mv_comp3_new_unit_amt(gds.replaceFormat(valrep_mv_comp_new_unit_amt.getText().toString()));

                    db.updateMotor_Vehicle_comp3(vl, record_id);
                    db.close();
                    myDialog.dismiss();
                    compute_average();
                    save_comparatives();
                    finish();
                    Intent in = new Intent(getApplicationContext(), Motor_Vehicle_Comparatives.class);
                    in.putExtra(TAG_RECORD_ID, record_id);
                    startActivityForResult(in, 100);
                Toast.makeText(getApplicationContext(),
                        "Saved", Toast.LENGTH_SHORT).show();
              /*  } else {
                    Toast.makeText(getApplicationContext(),
                            "Please fill up required fields", Toast.LENGTH_SHORT).show();
                }*/
            }
        });
        myDialog.show();
    }


    public void textwatch_comp1_5(){
        String price = gds.nullCheck2(report_price.getText().toString());
        //Less net
        String less_amt = gds.nullCheck2(report_less_amt.getText().toString());
        String less_net = gds.lessAmount(price, less_amt);
        report_less_net.setText(less_net);
        //ADD net
        String add_amt = gds.nullCheck2(report_add_amt.getText().toString());
        String add_net = gds.addition(less_net, add_amt);
        report_add_net.setText(add_net);


        //Mileage VALUE
        String mileage_value = gds.nullCheck2(report_mileage_value.getText().toString());
        String mileage_amt = gds.timeAdjAmount(add_net, mileage_value);
        report_mileage_amt.setText(mileage_amt);

        //TIME ADJ VALUE
        String time_adj_value = gds.nullCheck2(report_time_adj_value.getText().toString());
        String time_adj_amt = gds.timeAdjAmount(add_net, time_adj_value);
        report_time_adj_amt.setText(time_adj_amt);


        //brand_new
        String brand_new_value = gds.nullCheck2(valrep_mv_comp_new_unit_value.getText().toString());
        String brand_new_amt = gds.timeAdjAmount(add_net, brand_new_value);
        valrep_mv_comp_new_unit_amt.setText(brand_new_amt);


        //ADJ Value
        String adj_value = gds.nullCheck2(report_adj_value.getText().toString());
        String adj_amt = gds.timeAdjAmount(add_net, adj_value);
        report_adj_amt.setText(adj_amt);
        //INDEX price
        String index_price = gds.addition(gds.computeIndexPrice(brand_new_amt, mileage_amt,time_adj_amt, adj_amt),add_net);
        report_index_price.setText(index_price);

    }

    private boolean validate_price(EditText[] fields) {
        for (int i = 0; i < fields.length; i++) {
            EditText currentField = fields[i];
            if (currentField.getText().toString().length() <= 0) {
                return false;
            }else if (currentField.getText().toString().contentEquals("0.00")) {
                return false;
            }else if (currentField.getText().toString().contentEquals("0")) {
                return false;
            }else{
                return true;
            }
        }
        return true;
    }
    public void myDialog_config() {
        myDialog = new Dialog(Motor_Vehicle_Comparatives.this);
        //myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setTitle(dialog_title);
        myDialog.setContentView(R.layout.mv_comparatives_form);
        myDialog.setCancelable(true);

        android.view.WindowManager.LayoutParams params = myDialog.getWindow()
                .getAttributes();
        params.height = LayoutParams.FILL_PARENT;
        params.width = LayoutParams.FILL_PARENT;
        myDialog.getWindow().setAttributes(
                (android.view.WindowManager.LayoutParams) params);

        btn_compute = (Button) myDialog.findViewById(R.id.btn_compute);
        btn_save = (Button) myDialog.findViewById(R.id.btn_save);


        report_source = (EditText) myDialog.findViewById(R.id.report_source);
        report_tel_no = (EditText) myDialog.findViewById(R.id.report_tel_no);
        report_unit_model = (EditText) myDialog.findViewById(R.id.report_unit_model);
        report_mileage = (EditText) myDialog.findViewById(R.id.report_mileage);
        report_price_min = (EditText) myDialog.findViewById(R.id.report_price_min);
        report_price_max = (EditText) myDialog.findViewById(R.id.report_price_max);
        report_price = (EditText) myDialog.findViewById(R.id.report_price);
        report_less_amt = (EditText) myDialog.findViewById(R.id.report_less_amt);
        report_less_net = (EditText) myDialog.findViewById(R.id.report_less_net);
        report_add_amt = (EditText) myDialog.findViewById(R.id.report_add_amt);
        report_add_net = (EditText) myDialog.findViewById(R.id.report_add_net);
        report_time_adj_value = (EditText) myDialog.findViewById(R.id.report_time_adj_value);
        report_time_adj_amt = (EditText) myDialog.findViewById(R.id.report_time_adj_amt);
        report_mileage_value = (EditText) myDialog.findViewById(R.id.report_mileage_value);
        report_mileage_amt = (EditText) myDialog.findViewById(R.id.report_mileage_amt);

        report_adj_value = (EditText) myDialog.findViewById(R.id.report_adj_value);
        report_adj_amt = (EditText) myDialog.findViewById(R.id.report_adj_amt);
        report_adj_desc = (EditText) myDialog.findViewById(R.id.report_adj_desc);
        report_index_price = (EditText) myDialog.findViewById(R.id.report_index_price);

        valrep_mv_comp_new_unit_value = (EditText) myDialog.findViewById(R.id.valrep_mv_comp_new_unit_value);
        valrep_mv_comp_new_unit_amt = (EditText) myDialog.findViewById(R.id.valrep_mv_comp_new_unit_amt);


       /* report_less_net.setEnabled(false);
        report_add_net.setEnabled(false);*/
        report_time_adj_amt.setEnabled(false);
        report_mileage_amt.setEnabled(false);
        report_adj_amt.setEnabled(false);
        report_index_price.setEnabled(false);
    }


    public void compute_average() {

        String ave = "0";
        String index_total = "0";
        String count = "0";
        List<Motor_Vehicle_API> vl = db.getMotor_Vehicle_IndexPrice(String.valueOf(record_id));
        if (!vl.isEmpty()) {

            for (Motor_Vehicle_API im : vl) {
                i_price1 = im.getreport_comp1_index_price();
                i_price2 = im.getreport_comp2_index_price();
                i_price3 = im.getreport_comp3_index_price();
                ave = gds.computeAverage(i_price1, i_price2, i_price3);
                index_total = gds.addition(i_price1, gds.addition(i_price2, i_price3));
                count = gds.computeCount(i_price1, i_price2, i_price3);
            }
        }


        valrep_mv_comp_temp_ave_index_price.setText(gds.numberFormat(ave));
        String additional_value=gds.nullCheck2(valrep_mv_comp_add_index_price.getText().toString());
        report_average.setText(gds.numberFormat(gds.addition(ave,additional_value)));
        myDialog = new Dialog(Motor_Vehicle_Comparatives.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.mv_comparative_summary);
        myDialog.setCancelable(true);
        myDialog.setCanceledOnTouchOutside(true);
        final TextView tv_val1 = (TextView) myDialog.findViewById(R.id.tv_val1);
        final TextView tv_val2 = (TextView) myDialog.findViewById(R.id.tv_val2);
        final TextView tv_val3 = (TextView) myDialog.findViewById(R.id.tv_val3);
        final TextView tv_total = (TextView) myDialog.findViewById(R.id.tv_total);
        final TextView tv_numOfAdj = (TextView) myDialog.findViewById(R.id.tv_numOfAdj);
        final TextView tv_average = (TextView) myDialog.findViewById(R.id.tv_average);

        tv_val1.setText(gds.numberFormat(i_price1));
        tv_val2.setText(gds.numberFormat(i_price2));
        tv_val3.setText(gds.numberFormat(i_price3));
        tv_total.setText(gds.numberFormat(index_total));
        tv_numOfAdj.setText(count);
        tv_average.setText(gds.numberFormat(ave));
        //  myDialog.show();
        textwatch_rec();
    }

    public void save_comparatives() {
        //list of EditTexts put in array
        if (valrep_mv_comp_add_index_price.getText().length() == 0) {
            valrep_mv_comp_add_index_price.setText("");
        }
        if (valrep_mv_comp_temp_ave_index_price.getText().length() == 0) {
            valrep_mv_comp_temp_ave_index_price.setText("");
        }
        if (report_average.getText().length() == 0) {
            report_average.setText("");
        }
        if (report_rec_luxury_vehicle.getText().length() == 0) {
            report_rec_luxury_vehicle.setText("");
        }
        if (report_rec_mileage_factor.getText().length() == 0) {
            report_rec_mileage_factor.setText("");
        }
        if (report_rec_taxi_use.getText().length() == 0) {
            report_rec_taxi_use.setText("");
        }
        if (report_rec_for_hire.getText().length() == 0) {
            report_rec_for_hire.setText("");
        }
        if (report_rec_observed_condition.getText().length() == 0) {
            report_rec_observed_condition.setText("");
        }
        if (report_rec_repo.getText().length() == 0) {
            report_rec_repo.setText("");
        }
        if (report_rec_other_desc.getText().length() == 0) {
            report_rec_other_desc.setText("");
        }
        if (report_rec_other_val.getText().length() == 0) {
            report_rec_other_val.setText("");
        }
        if (report_rec_other2_desc.getText().length() == 0) {
            report_rec_other2_desc.setText("");
        }
        if (report_rec_other2_val.getText().length() == 0) {
            report_rec_other2_val.setText("");
        }
        if (report_rec_other3_desc.getText().length() == 0) {
            report_rec_other3_desc.setText("");
        }
        if (report_rec_other3_val.getText().length() == 0) {
            report_rec_other3_val.setText("");
        }

        if (report_rep_stereo.getText().length() == 0) {
            report_rep_stereo.setText("");
        }else{
            report_rep_stereo.setText(gds.stringToDecimal(report_rep_stereo.getText().toString()));
        }
        if (report_rep_speakers.getText().length() == 0) {
            report_rep_speakers.setText("");
        }else{
            report_rep_speakers.setText(gds.stringToDecimal(report_rep_speakers.getText().toString()));
        }
        if (report_rep_tires_pcs.getText().length() == 0) {
            report_rep_tires_pcs.setText("");
        }
        if (report_rep_tires.getText().length() == 0) {
            report_rep_tires.setText("");
        }else{
            report_rep_tires.setText(gds.stringToDecimal(report_rep_tires.getText().toString()));
        }
        if (report_rep_side_mirror_pcs.getText().length() == 0) {
            report_rep_side_mirror_pcs.setText("");
        }
        if (report_rep_side_mirror.getText().length() == 0) {
            report_rep_side_mirror.setText("");
        }else{
            report_rep_side_mirror.setText(gds.stringToDecimal(report_rep_side_mirror.getText().toString()));
        }
        if (report_rep_light.getText().length() == 0) {
            report_rep_light.setText("");
        }else{
            report_rep_light.setText(gds.stringToDecimal(report_rep_light.getText().toString()));
        }
        if (report_rep_tools.getText().length() == 0) {
            report_rep_tools.setText("");
        }else{
            report_rep_tools.setText(gds.stringToDecimal(report_rep_tools.getText().toString()));
        }
        if (report_rep_battery.getText().length() == 0) {
            report_rep_battery.setText("");
        }else{
            report_rep_battery.setText(gds.stringToDecimal(report_rep_battery.getText().toString()));
        }
        if (report_rep_plates.getText().length() == 0) {
            report_rep_plates.setText("");
        }else{
            report_rep_plates.setText(gds.stringToDecimal(report_rep_plates.getText().toString()));
        }
        if (report_rep_bumpers.getText().length() == 0) {
            report_rep_bumpers.setText("");
        }else{
            report_rep_bumpers.setText(gds.stringToDecimal(report_rep_bumpers.getText().toString()));
        }
        if (report_rep_windows.getText().length() == 0) {
            report_rep_windows.setText("");
        }else{
            report_rep_windows.setText(gds.stringToDecimal(report_rep_windows.getText().toString()));
        }
        if (report_rep_body.getText().length() == 0) {
            report_rep_body.setText("");
        }else{
            report_rep_body.setText(gds.stringToDecimal(report_rep_body.getText().toString()));
        }
        if (report_rep_engine_wash.getText().length() == 0) {
            report_rep_engine_wash.setText("");
        }else{
            report_rep_engine_wash.setText(gds.stringToDecimal(report_rep_engine_wash.getText().toString()));
        }
        if (report_rep_other_desc.getText().length() == 0) {
            report_rep_other_desc.setText("");
        }
        if (report_rep_other.getText().length() == 0) {
            report_rep_other.setText("");
        }else{
            report_rep_other.setText(gds.stringToDecimal(report_rep_other.getText().toString()));
        }
        if (valrep_mv_rep_other2_desc.getText().length() == 0) {
            valrep_mv_rep_other2_desc.setText("");
        }
        if (valrep_mv_rep_other2.getText().length() == 0) {
            valrep_mv_rep_other2.setText("");
        }else{
            valrep_mv_rep_other2.setText(gds.stringToDecimal(valrep_mv_rep_other2.getText().toString()));
        }
        if (valrep_mv_rep_other3_desc.getText().length() == 0) {
            valrep_mv_rep_other3_desc.setText("");
        }
        if (valrep_mv_rep_other3.getText().length() == 0) {
            valrep_mv_rep_other3.setText("");
        }else{
            valrep_mv_rep_other3.setText(gds.stringToDecimal(valrep_mv_rep_other3.getText().toString()));
        }
        String valuation_min = gds.nullCheck(gds.addition(db.getMotor_Vehicle_minPrice(record_id),"0"));
        String valuation_max = gds.nullCheck(gds.addition(db.getMotor_Vehicle_maxPrice(record_id),"0"));

        String round_thousand= gds.roundToThousand(gds.nullCheck(report_appraised_value.getText().toString()));



        Motor_Vehicle_API vl = new Motor_Vehicle_API();
        vl.setvalrep_mv_comp_temp_ave_index_price(gds.replaceFormat(valrep_mv_comp_temp_ave_index_price.getText().toString()));
        vl.setvalrep_mv_comp_add_index_price_desc(valrep_mv_comp_add_index_price_desc.getText().toString());
        vl.setvalrep_mv_comp_add_index_price(gds.replaceFormat(valrep_mv_comp_add_index_price.getText().toString()));
        vl.setreport_comp_ave_index_price(gds.replaceFormat(report_average.getText().toString()));

        vl.setvalrep_mv_rep_other2_desc(valrep_mv_rep_other2_desc.getText().toString());
        vl.setvalrep_mv_rep_other2(gds.replaceFormat(valrep_mv_rep_other2.getText().toString()));
        vl.setvalrep_mv_rep_other3_desc(valrep_mv_rep_other3_desc.getText().toString());
        vl.setvalrep_mv_rep_other3(gds.replaceFormat(valrep_mv_rep_other3.getText().toString()));


        vl.setreport_rec_lux_car(report_rec_luxury_vehicle.getText().toString());
        vl.setreport_rec_mile_factor(report_rec_mileage_factor.getText().toString());
        vl.setreport_rec_taxi_use(report_rec_taxi_use.getText().toString());
        vl.setreport_rec_for_hire(report_rec_for_hire.getText().toString());
        vl.setreport_rec_condition(report_rec_observed_condition.getText().toString());
        vl.setreport_rec_repo(report_rec_repo.getText().toString());
        vl.setreport_rec_other_desc(report_rec_other_desc.getText().toString());
        vl.setreport_rec_other_val(report_rec_other_val.getText().toString());
        vl.setreport_rec_other2_desc(report_rec_other2_desc.getText().toString());
        vl.setreport_rec_other2_val(report_rec_other2_val.getText().toString());
        vl.setreport_rec_other3_desc(report_rec_other3_desc.getText().toString());
        vl.setreport_rec_other3_val(report_rec_other3_val.getText().toString());

        vl.setreport_rec_market_resistance_rec_total(report_market_resistance_rec_total.getText().toString());
        vl.setreport_rec_market_resistance_total(gds.replaceFormat(report_market_resistance_total.getText().toString()));
        vl.setreport_rep_stereo(gds.replaceFormat(report_rep_stereo.getText().toString()));
        vl.setreport_rep_speakers(gds.replaceFormat(report_rep_speakers.getText().toString()));
        vl.setreport_rep_tires_pcs(report_rep_tires_pcs.getText().toString());
        vl.setreport_rep_tires(gds.replaceFormat(report_rep_tires.getText().toString()));
        vl.setreport_rep_side_mirror_pcs(report_rep_side_mirror_pcs.getText().toString());
        vl.setreport_rep_side_mirror(gds.replaceFormat(report_rep_side_mirror.getText().toString()));
        vl.setreport_rep_light(gds.replaceFormat(report_rep_light.getText().toString()));
        vl.setreport_rep_tools(gds.replaceFormat(report_rep_tools.getText().toString()));
        vl.setreport_rep_battery(gds.replaceFormat(report_rep_battery.getText().toString()));
        vl.setreport_rep_plates(gds.replaceFormat(report_rep_plates.getText().toString()));
        vl.setreport_rep_bumpers(gds.replaceFormat(report_rep_bumpers.getText().toString()));
        vl.setreport_rep_windows(gds.replaceFormat(report_rep_windows.getText().toString()));
        vl.setreport_rep_body(gds.replaceFormat(report_rep_body.getText().toString()));
        vl.setreport_rep_engine_wash(gds.replaceFormat(report_rep_engine_wash.getText().toString()));
        vl.setreport_rep_other_desc(report_rep_other_desc.getText().toString());
        vl.setreport_rep_other(gds.replaceFormat(report_rep_other.getText().toString()));
        vl.setreport_rep_total(gds.replaceFormat(report_rep_total.getText().toString()));
        vl.setreport_appraised_value(gds.replaceFormat(report_appraised_value.getText().toString()));

        vl.setreport_market_valuation_min(gds.replaceFormat(valuation_min));
        vl.setreport_market_valuation_max(gds.replaceFormat(valuation_max));
        vl.setreport_market_valuation_fair_value(gds.replaceFormat(round_thousand));


        db.updateMotor_Vehicle_comparatives(vl, record_id);

        db.close();
        Toast.makeText(getApplicationContext(),
                "Saved", Toast.LENGTH_SHORT).show();
    }

    public void save_average(){
        if (report_average.getText().length() == 0) {
            report_average.setText("0");
        }
        Motor_Vehicle_API vl = new Motor_Vehicle_API();
        vl.setvalrep_mv_comp_temp_ave_index_price(gds.replaceFormat(valrep_mv_comp_temp_ave_index_price.getText().toString()));
        vl.setvalrep_mv_comp_add_index_price(gds.replaceFormat(valrep_mv_comp_add_index_price.getText().toString()));
        vl.setreport_comp_ave_index_price(gds.replaceFormat(report_average.getText().toString()));

        db.updateMotor_Vehicle_comparatives_Ave(vl, record_id);
        db.close();
    }
    public void textwatch_rec(){
        if(!report_rec_luxury_vehicle.getText().toString().contentEquals("-")&&!report_rec_mileage_factor.getText().toString().contentEquals("-")&&!report_rec_taxi_use.getText().toString().contentEquals("-")
                &&!report_rec_for_hire.getText().toString().contentEquals("-")&&!report_rec_observed_condition.getText().toString().contentEquals("-")&&!report_rec_repo.getText().toString().contentEquals("-")
                &&!report_rec_other_val.getText().toString().contentEquals("-")&&!report_rec_other2_val.getText().toString().contentEquals("-")&&!report_rec_other3_val.getText().toString().contentEquals("-")){
            if(!report_rec_luxury_vehicle.getText().toString().contentEquals(".")&&!report_rec_mileage_factor.getText().toString().contentEquals(".")&&!report_rec_taxi_use.getText().toString().contentEquals(".")
                    &&!report_rec_for_hire.getText().toString().contentEquals(".")&&!report_rec_observed_condition.getText().toString().contentEquals(".")&&!report_rec_repo.getText().toString().contentEquals(".")
                    &&!report_rec_other_val.getText().toString().contentEquals(".")&&!report_rec_other2_val.getText().toString().contentEquals(".")&&!report_rec_other3_val.getText().toString().contentEquals(".")){
                if(!report_rec_luxury_vehicle.getText().toString().contentEquals("-.")&&!report_rec_mileage_factor.getText().toString().contentEquals("-.")&&!report_rec_taxi_use.getText().toString().contentEquals("-.")
                        &&!report_rec_for_hire.getText().toString().contentEquals("-.")&&!report_rec_observed_condition.getText().toString().contentEquals("-.")&&!report_rec_repo.getText().toString().contentEquals("-.")
                        &&!report_rec_other_val.getText().toString().contentEquals("-.")&&!report_rec_other2_val.getText().toString().contentEquals("-.")&&!report_rec_other3_val.getText().toString().contentEquals("-.")){

                    //start
                    String temp_average = gds.nullCheck2(valrep_mv_comp_temp_ave_index_price.getText().toString());
                    String add_ave_value= gds.nullCheck2(valrep_mv_comp_add_index_price.getText().toString());
                    String average = gds.addition(temp_average,add_ave_value);
                    String luxury_vehicle = gds.nullCheck2(report_rec_luxury_vehicle.getText().toString());
                    String mileage_factor = gds.nullCheck2(report_rec_mileage_factor.getText().toString());
                    String taxi_use = gds.nullCheck2(report_rec_taxi_use.getText().toString());
                    String for_hire = gds.nullCheck2(report_rec_for_hire.getText().toString());
                    String observed_condition = gds.nullCheck2(report_rec_observed_condition.getText().toString());
                    String repo = gds.nullCheck2(report_rec_repo.getText().toString());
                    String other_val = gds.nullCheck2(report_rec_other_val.getText().toString());
                    String other_val2 = gds.nullCheck2(report_rec_other2_val.getText().toString());
                    String other_val3 = gds.nullCheck2(report_rec_other3_val.getText().toString());

                    String market_resistance_rec_total = gds.rectif1(luxury_vehicle, mileage_factor, taxi_use, for_hire, observed_condition,
                            repo, other_val, other_val2, other_val3);
                    String market_resistance_total = gds.round(gds.multiplication(average, gds.multiplication(market_resistance_rec_total,"0.01")));

                    market_resistance_net_total = gds.subtraction(average, market_resistance_total);
                    Log.e("report_rec_market_res_net_total",market_resistance_net_total);
                    report_average.setText(gds.numberFormat(average));
                    report_market_resistance_rec_total.setText(gds.numberFormat(market_resistance_rec_total));
                    report_market_resistance_total.setText(gds.numberFormat(market_resistance_total));
                    report_rec_market_resistance_net_total.setText(gds.numberFormat(market_resistance_net_total));


                }
            }
        }
    }
    public void save_market_net(){
        ArrayList<String>field = new ArrayList<String>();
        ArrayList<String>value = new ArrayList<String>();
        field.clear();
        value.clear();
        field.add("report_rec_market_resistance_net_total");
        value.add(gds.replaceFormat(report_rec_market_resistance_net_total.getText().toString()));
        db2.updateRecord(value,field,"record_id = ?",new String []{record_id},"motor_vehicle");
    }
    public void textwatch_rep(){
        market_resistance_net_total = gds.nullCheck2(report_rec_market_resistance_net_total.getText().toString());
        String rep_stereo = gds.nullCheck2(report_rep_stereo.getText().toString());
        String rep_speakers = gds.nullCheck2(report_rep_speakers.getText().toString());
        String rep_tires = gds.nullCheck2(report_rep_tires.getText().toString());
        String rep_side_mirror = gds.nullCheck2(report_rep_side_mirror.getText().toString());
        String rep_light = gds.nullCheck2(report_rep_light.getText().toString());
        String rep_tools = gds.nullCheck2(report_rep_tools.getText().toString());
        String rep_battery = gds.nullCheck2(report_rep_battery.getText().toString());
        String rep_plates = gds.nullCheck2(report_rep_plates.getText().toString());
        String rep_bumpers = gds.nullCheck2(report_rep_bumpers.getText().toString());
        String rep_windows = gds.nullCheck2(report_rep_windows.getText().toString());
        String rep_body = gds.nullCheck2(report_rep_body.getText().toString());
        String rep_engine_wash = gds.nullCheck2(report_rep_engine_wash.getText().toString());
        String rep_other = gds.nullCheck2(report_rep_other.getText().toString());
        String rep_other2 = gds.nullCheck2(valrep_mv_rep_other2.getText().toString());
        String rep_other3 = gds.nullCheck2(valrep_mv_rep_other3.getText().toString());


        String rep_total = gds.rectif2(rep_stereo, rep_speakers, rep_tires, rep_side_mirror,
                rep_light, rep_tools, rep_battery, rep_plates, rep_bumpers, rep_windows, rep_body, rep_engine_wash, rep_other);
        rep_total=gds.addition(gds.addition(rep_total,rep_other2),rep_other3);
        report_rep_total.setText(rep_total);

        String appraised_value = gds.computeAppraisedVal(market_resistance_net_total, rep_total);
        report_appraised_value.setText(appraised_value);
    }

    @Override
    public void onUserInteraction(){
        st.resetDisconnectTimer();
    }
    @Override
    public void onStop() {
        super.onStop();
        st.stopDisconnectTimer();
    }
    @Override
    public void onResume() {
        super.onResume();
        st.resetDisconnectTimer();
    }


}