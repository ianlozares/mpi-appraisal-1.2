package com.gds.appraisalmaybank.motor_vehicle;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database2.Motor_Vehicle_API_Prev_Appraisal;
import com.gds.appraisalmaybank.land_improvements.Land_Improvements;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Motor_Vehicle_Prev_Appraisal extends ListActivity implements OnClickListener {
    DatabaseHandler2 db = new DatabaseHandler2(this);
    Land_Improvements li = new Land_Improvements();
    Button btn_add_prev_app, btn_save_prev_app;
    Button btn_create, btn_cancel, btn_save;
    GDS_methods gds = new GDS_methods();
    Session_Timer st = new Session_Timer(this);
    String record_id="";
    private static final String TAG_RECORD_ID = "record_id";
    String nature_appraisal;
    Dialog myDialog;
    //create dialog
    EditText  report_prev_date, report_prev_appraiser,	report_prev_requestor,report_prev_appraised_value;
    TextView tv_prev_date,tv_prev_appraiser,tv_prev_requestor,tv_prev_appraised_value;
    //forListViewDeclarations
    private ProgressDialog pDialog;
    ArrayList<HashMap<String, String>> itemList;
    private static final String tag_prev_id = "prev_id";
    private static final String tag_appraiser = "appraiser";
    private static final String tag_prev_appraised_value = "prev_appraised_value";
    String prev_id="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reports_mv_prev_appraisal);

        st.resetDisconnectTimer();
        btn_add_prev_app = (Button) findViewById(R.id.btn_add_prev_app);
        btn_add_prev_app.setOnClickListener(this);

        btn_save_prev_app = (Button) findViewById(R.id.btn_save_prev_app);
        btn_save_prev_app.setOnClickListener(this);

        // getting record_id from intent / previous class
        Intent i = getIntent();
        record_id = i.getStringExtra(TAG_RECORD_ID);
        //list
        itemList = new ArrayList<HashMap<String, String>>();
        // Loading in Background Thread
        new LoadAll().execute();
        // Get listview
        ListView lv = getListView();

        // on seleting single item
        // launching Edit item Screen
        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // getting values from selected ListItem
                prev_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
                        .toString();
                view_details();
            }
        });
        lv.setOnItemLongClickListener(new OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {
                // TODO Auto-generated method stub

                Log.v("long clicked","pos: " + position);
                prev_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
                        .toString();
                delete_item();
                return true;
            }
        });
        String where = "WHERE record_id = args";
        String[] args = new String[1];
        args[0] = record_id;
        nature_appraisal = db.getRecord("nature_appraisal", where, args, "tbl_report_accepted_jobs").get(0);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        Intent intent = new Intent(Motor_Vehicle_Prev_Appraisal.this, Motor_vehicle.class);
        intent.putExtra("keyopen","1");
        startActivity(intent);
    }

    @Override
    public void onClick(View arg0) {
        // TODO Auto-generated method stub
        switch (arg0.getId()) {
            case R.id.btn_add_prev_app:
                add_new();
                break;
            case R.id.btn_save_prev_app:
                this.finish();
                Intent intent = new Intent(Motor_Vehicle_Prev_Appraisal.this, Motor_vehicle.class);
                intent.putExtra("keyopen","1");
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    public void add_new(){
        Toast.makeText(getApplicationContext(), "Add New",
                Toast.LENGTH_SHORT).show();
        myDialog = new Dialog(Motor_Vehicle_Prev_Appraisal.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.reports_mv_prev_app_form);
        myDialog.setCancelable(true);

        android.view.WindowManager.LayoutParams params = myDialog.getWindow()
                .getAttributes();
        params.height = LayoutParams.WRAP_CONTENT;
        params.width = LayoutParams.FILL_PARENT;
        myDialog.getWindow().setAttributes(
                (android.view.WindowManager.LayoutParams) params);

        btn_create = (Button) myDialog.findViewById(R.id.btn_right);
        btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
        btn_create.setText("Create");
        btn_cancel.setText("Cancel");
        report_prev_date = (EditText) myDialog.findViewById(R.id.report_prev_date);
        report_prev_appraiser = (EditText) myDialog.findViewById(R.id.report_prev_appraiser);
        report_prev_requestor = (EditText) myDialog.findViewById(R.id.report_prev_requestor);
        report_prev_appraised_value = (EditText) myDialog.findViewById(R.id.report_prev_appraised_value);

        tv_prev_date = (TextView) myDialog.findViewById(R.id.tv_prev_date);
        tv_prev_appraiser = (TextView) myDialog.findViewById(R.id.tv_prev_appraiser);
        tv_prev_requestor = (TextView) myDialog.findViewById(R.id.tv_prev_requestor);
        tv_prev_appraised_value = (TextView) myDialog.findViewById(R.id.tv_prev_appraised_value);
        report_prev_appraised_value.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(report_prev_appraised_value, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });


        btn_create.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                    add_db();


            }
        });
        btn_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        if (nature_appraisal.contentEquals("Re-appraisal")) {
            //gds.fill_in_error(new EditText[]{report_prev_date, report_prev_appraiser, report_prev_requestor, report_prev_appraised_value});
        }
        myDialog.show();
    }
    public void add_db(){
        //add data in SQLite
        db.addMotor_Vehicle_Prev_App(new Motor_Vehicle_API_Prev_Appraisal(
                record_id,
                report_prev_date.getText().toString(),
                report_prev_appraiser.getText().toString(),
                report_prev_requestor.getText().toString(),
                gds.replaceFormat(report_prev_appraised_value.getText().toString())));

        Toast.makeText(getApplicationContext(), "data created", Toast.LENGTH_SHORT).show();
        myDialog.dismiss();
        reloadClass();
    }
    public void view_details(){
        myDialog = new Dialog(Motor_Vehicle_Prev_Appraisal.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.reports_mv_prev_app_form);
        myDialog.setCancelable(true);

        android.view.WindowManager.LayoutParams params = myDialog.getWindow()
                .getAttributes();
        params.height = LayoutParams.WRAP_CONTENT;
        params.width = LayoutParams.FILL_PARENT;
        myDialog.getWindow().setAttributes(
                (android.view.WindowManager.LayoutParams) params);

        btn_save = (Button) myDialog.findViewById(R.id.btn_right);
        btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
        btn_save.setText("Save");
        btn_cancel.setText("Cancel");

        report_prev_date = (EditText) myDialog.findViewById(R.id.report_prev_date);
        report_prev_appraiser = (EditText) myDialog.findViewById(R.id.report_prev_appraiser);
        report_prev_requestor = (EditText) myDialog.findViewById(R.id.report_prev_requestor);
        report_prev_appraised_value = (EditText) myDialog.findViewById(R.id.report_prev_appraised_value);
        tv_prev_date = (TextView) myDialog.findViewById(R.id.tv_prev_date);
        tv_prev_appraiser = (TextView) myDialog.findViewById(R.id.tv_prev_appraiser);
        tv_prev_requestor = (TextView) myDialog.findViewById(R.id.tv_prev_requestor);
        tv_prev_appraised_value = (TextView) myDialog.findViewById(R.id.tv_prev_appraised_value);
        report_prev_appraised_value.addTextChangedListener(new TextWatcher() {
            String current="";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                current = gds.numberFormat(report_prev_appraised_value, this, s, current);

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        List<Motor_Vehicle_API_Prev_Appraisal> prevList = db
                .getMV_Prev_appraisal_Single(String.valueOf(record_id), String.valueOf(prev_id));
        if (!prevList.isEmpty()) {
            for (Motor_Vehicle_API_Prev_Appraisal li : prevList) {
                report_prev_date.setText(li.getreport_prev_date());
                report_prev_appraiser.setText(li.getreport_prev_appraiser());
                report_prev_requestor.setText(li.getreport_prev_requestor());
                report_prev_appraised_value.setText(gds.numberFormat(li.getreport_prev_appraised_value()));

            }
        }

        btn_save.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                    update_db();

            }
        });

        btn_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        if (nature_appraisal.contentEquals("Re-appraisal")) {
            //gds.fill_in_error(new EditText[]{report_prev_date, report_prev_appraiser, report_prev_requestor, report_prev_appraised_value});
        }
        myDialog.show();
    }
    public void update_db(){
        Motor_Vehicle_API_Prev_Appraisal mv = new Motor_Vehicle_API_Prev_Appraisal();
        mv.setreport_prev_date(report_prev_date.getText().toString());
        mv.setreport_prev_appraiser(report_prev_appraiser.getText().toString());
        mv.setreport_prev_requestor(report_prev_requestor.getText().toString());
        mv.setreport_prev_appraised_value(gds.replaceFormat(report_prev_appraised_value.getText().toString()));


        db.updateMV_Prev_appraisal(mv, record_id, String.valueOf(prev_id));
        db.close();
        myDialog.dismiss();
        Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
        reloadClass();
    }

    public void reloadClass(){
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }
    public void delete_item(){
        myDialog = new Dialog(Motor_Vehicle_Prev_Appraisal.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.delete_dialog);
        myDialog.setCancelable(true);
        final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
        final Button btn_yes = (Button) myDialog.findViewById(R.id.btn_yes);
        final Button btn_no = (Button) myDialog.findViewById(R.id.btn_no);
        tv_question.setText("Are you sure you want to delete the selected item?");
        btn_yes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                db.deleteMV_Prev_Single(record_id, prev_id);
                db.close();
                myDialog.dismiss();
                reloadClass();
                Toast.makeText(getApplicationContext(), "Data Deleted",Toast.LENGTH_SHORT).show();
            }
        });
        btn_no.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        myDialog.show();
    }


    /**
     * Background Async Task to Load all
     * */
    class LoadAll extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Motor_Vehicle_Prev_Appraisal.this);
            pDialog.setMessage("Loading Lists. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting All
         * */
        protected String doInBackground(String... args) {
            String appraiser="", appraised_value="";


            List<Motor_Vehicle_API_Prev_Appraisal> lirdps = db.getMV_Prev(record_id);
            if (!lirdps.isEmpty()) {
                for (Motor_Vehicle_API_Prev_Appraisal imrdps : lirdps) {
                    prev_id = String.valueOf(imrdps.getID());
                    record_id = imrdps.getrecord_id();
                    appraiser = imrdps.getreport_prev_appraiser();
                    appraised_value = gds.numberFormat(imrdps.getreport_prev_appraised_value());

                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(TAG_RECORD_ID, record_id);
                    map.put(tag_prev_id, prev_id);
                    map.put(tag_appraiser, appraiser);
                    map.put(tag_prev_appraised_value, appraised_value);

                    // adding HashList to ArrayList
                    itemList.add(map);
                }
            }
            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all
            pDialog.dismiss();
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    ListAdapter adapter = new SimpleAdapter(
                            Motor_Vehicle_Prev_Appraisal.this, itemList,
                            R.layout.reports_mv_prev_list, new String[] { TAG_RECORD_ID, tag_prev_id, tag_appraiser,
                            tag_prev_appraised_value},
                            new int[] { R.id.report_record_id, R.id.primary_key, R.id.item_prev_appraiser, R.id.item_prev_appraised_value});
                    // updating listview
                    setListAdapter(adapter);
                }
            });
            if(itemList.isEmpty()){
                add_new();
            }
        }
    }

    @Override
    public void onUserInteraction(){
        st.resetDisconnectTimer();
    }
    @Override
    public void onStop() {
        super.onStop();
        st.stopDisconnectTimer();
    }
    @Override
    public void onResume() {
        super.onResume();
        st.resetDisconnectTimer();
    }


}