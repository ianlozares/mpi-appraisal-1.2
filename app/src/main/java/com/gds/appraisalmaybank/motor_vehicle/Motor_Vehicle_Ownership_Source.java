package com.gds.appraisalmaybank.motor_vehicle;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.database2.Motor_Vehicle_API_Ownership_Source;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Motor_Vehicle_Ownership_Source  extends ListActivity implements OnClickListener {
	DatabaseHandler dbMain = new DatabaseHandler(this);
	DatabaseHandler2 db = new DatabaseHandler2(this);
	GDS_methods gds = new GDS_methods();
	Button btn_add_os, btn_save_os;
	Button btn_create, btn_cancel, btn_save;

	String record_id="";
	private static final String TAG_RECORD_ID = "record_id";

	Dialog myDialog;
	//create dialog
	EditText report_source_mileage, report_source_range_min, report_source_range_max, report_source_source, report_source_vehicle_type;

	Session_Timer st = new Session_Timer(this);
	//forListViewDeclarations
	private ProgressDialog pDialog;
	ArrayList<HashMap<String, String>> itemList;
	private static final String tag_mvos_id = "mvos_id";
	private static final String tag_source = "source";
	private static final String tag_vehicle_type = "vehicle_type";
	private static final String tag_min = "min";
	private static final String tag_max = "max";
	String mvos_id="";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reports_mv_ownership_source);

		st.resetDisconnectTimer();
		btn_add_os = (Button) findViewById(R.id.btn_add_os);
		btn_add_os.setOnClickListener(this);

		btn_save_os = (Button) findViewById(R.id.btn_save_os);
		btn_save_os.setOnClickListener(this);

		// getting record_id from intent / previous class
		Intent i = getIntent();
		record_id = i.getStringExtra(TAG_RECORD_ID);
		//list
		itemList = new ArrayList<HashMap<String, String>>();
		// Loading in Background Thread
		new LoadAll().execute();
		// Get listview
		ListView lv = getListView();

		// on seleting single item
		// launching Edit item Screen
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {
				// getting values from selected ListItem
				mvos_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
				view_details();
			}
		});
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

			public boolean onItemLongClick(AdapterView<?> parent, View view,
										   int position, long id) {
				// TODO Auto-generated method stub

				Log.v("long clicked","pos: " + position);
				mvos_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
				delete_item();
				return true;
			}
		});

	}
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
			case R.id.btn_add_os:
				add_new();
				break;
			case R.id.btn_save_os:
				List<Motor_Vehicle_API_Ownership_Source> lirdps = db.getMotor_Vehicle_Ownership_Source(record_id);
				if (!lirdps.isEmpty()) {
					//getMinMax();
				}
				finish();
				break;
			default:
				break;
		}
	}



	public void add_new(){
		Toast.makeText(getApplicationContext(), "Add New",
				Toast.LENGTH_SHORT).show();
		myDialog = new Dialog(Motor_Vehicle_Ownership_Source.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.reports_mv_ownership_source_form);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);

		btn_create = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_create.setText("Create");
		btn_cancel.setText("Cancel");
		report_source_mileage = (EditText) myDialog.findViewById(R.id.report_source_mileage);
		report_source_range_min = (EditText) myDialog.findViewById(R.id.report_source_range_min);
		report_source_range_max = (EditText) myDialog.findViewById(R.id.report_source_range_max);
		report_source_source = (EditText) myDialog.findViewById(R.id.report_source_source);
		report_source_vehicle_type = (EditText) myDialog.findViewById(R.id.report_source_vehicle_type);

		//check if there's already a record
		List<Motor_Vehicle_API_Ownership_Source> lirdps = db.getMotor_Vehicle_Ownership_Source(record_id);
		if (!lirdps.isEmpty()) {
			for (Motor_Vehicle_API_Ownership_Source imrdps : lirdps) {
				report_source_vehicle_type.setText(imrdps.getreport_source_vehicle_type());
			}
		}

		btn_create.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {


					//add data in SQLite
					db.addMotor_Vehicle_Ownership_Source(new Motor_Vehicle_API_Ownership_Source(
							record_id,
							report_source_mileage.getText().toString(),
							report_source_range_min.getText().toString(),
							report_source_range_max.getText().toString(),
							report_source_source.getText().toString(),
							report_source_vehicle_type.getText().toString()));

					Toast.makeText(getApplicationContext(),"data created", Toast.LENGTH_SHORT).show();
					myDialog.dismiss();
					reloadClass();


			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});

		gds.fill_in_error(new EditText[] {
				report_source_mileage, report_source_range_min,
				report_source_range_max, report_source_source,
				report_source_vehicle_type });
		myDialog.show();
	}

	public void view_details(){
		myDialog = new Dialog(Motor_Vehicle_Ownership_Source.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.reports_mv_ownership_source_form);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);

		btn_save = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_save.setText("Save");
		btn_cancel.setText("Cancel");

		report_source_mileage = (EditText) myDialog.findViewById(R.id.report_source_mileage);
		report_source_range_min = (EditText) myDialog.findViewById(R.id.report_source_range_min);
		report_source_range_max = (EditText) myDialog.findViewById(R.id.report_source_range_max);
		report_source_source = (EditText) myDialog.findViewById(R.id.report_source_source);
		report_source_vehicle_type = (EditText) myDialog.findViewById(R.id.report_source_vehicle_type);

		List<Motor_Vehicle_API_Ownership_Source> mvohList = db
				.getMotor_Vehicle_Ownership_Source(String.valueOf(record_id), String.valueOf(mvos_id));
		if (!mvohList.isEmpty()) {
			for (Motor_Vehicle_API_Ownership_Source im : mvohList) {
				//date of inspection
				report_source_mileage.setText(im.getreport_source_mileage());
				report_source_range_min.setText(im.getreport_source_range_min());
				report_source_range_max.setText(im.getreport_source_range_max());
				report_source_source.setText(im.getreport_source_source());
				report_source_vehicle_type.setText(im.getreport_source_vehicle_type());
			}
		}

		btn_save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

					//update data in SQLite
					Motor_Vehicle_API_Ownership_Source mv = new Motor_Vehicle_API_Ownership_Source();
					mv.setreport_source_mileage(report_source_mileage.getText().toString());
					mv.setreport_source_range_min(report_source_range_min.getText().toString());
					mv.setreport_source_range_max(report_source_range_max.getText().toString());
					mv.setreport_source_source(report_source_source.getText().toString());
					mv.setreport_source_vehicle_type(report_source_vehicle_type.getText().toString());

					db.updateMotor_Vehicle_Ownership_Source(mv, record_id, mvos_id);
					db.close();
					myDialog.dismiss();
					Toast.makeText(getApplicationContext(), "Saved",Toast.LENGTH_SHORT).show();
					reloadClass();

			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});

		gds.fill_in_error(new EditText[] {
				report_source_mileage, report_source_range_min,
				report_source_range_max, report_source_source,
				report_source_vehicle_type });
		myDialog.show();
	}


	public void reloadClass(){
		Intent intent = getIntent();
		finish();
		startActivity(intent);
	}
	public void delete_item(){
		myDialog = new Dialog(Motor_Vehicle_Ownership_Source.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.delete_dialog);
		myDialog.setCancelable(true);
		final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
		final Button btn_yes = (Button) myDialog.findViewById(R.id.btn_yes);
		final Button btn_no = (Button) myDialog.findViewById(R.id.btn_no);
		tv_question.setText("Are you sure you want to delete the selected item?");
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				db.deleteMotor_Vehicle_Ownership_Source(record_id, mvos_id);
				db.close();
				myDialog.dismiss();
				reloadClass();
				Toast.makeText(getApplicationContext(), "Data Deleted",Toast.LENGTH_SHORT).show();
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});

		myDialog.show();
	}


	/**
	 * Background Async Task to Load all
	 * */
	class LoadAll extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Motor_Vehicle_Ownership_Source.this);
			pDialog.setMessage("Loading Lists. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All
		 * */
		protected String doInBackground(String... args) {
			String source="", vehicle_type="", min="0.00", max="0.00";


			List<Motor_Vehicle_API_Ownership_Source> lirdps = db.getMotor_Vehicle_Ownership_Source(record_id);
			if (!lirdps.isEmpty()) {
				for (Motor_Vehicle_API_Ownership_Source imrdps : lirdps) {
					mvos_id = String.valueOf(imrdps.getID());
					record_id = imrdps.getrecord_id();
					source = imrdps.getreport_source_source();
					vehicle_type = imrdps.getreport_source_vehicle_type();
					min = imrdps.getreport_source_range_min();
					max = imrdps.getreport_source_range_max();

					HashMap<String, String> map = new HashMap<String, String>();
					map.put(TAG_RECORD_ID, record_id);
					map.put(tag_mvos_id, mvos_id);
					map.put(tag_source, source);
					map.put(tag_vehicle_type, vehicle_type);
					map.put(tag_min, min);
					map.put(tag_max, max);

					// adding HashList to ArrayList
					itemList.add(map);
				}
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					ListAdapter adapter = new SimpleAdapter(
							Motor_Vehicle_Ownership_Source.this, itemList,
							R.layout.reports_mv_ownership_source_list, new String[] { TAG_RECORD_ID, tag_mvos_id, tag_source,
							tag_vehicle_type, tag_min, tag_max},
							new int[] { R.id.report_record_id, R.id.primary_key, R.id.item_source, R.id.item_type_of_vehicle,
									R.id.item_min, R.id.item_max});
					// updating listview
					setListAdapter(adapter);
				}
			});

		}
	}
	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}


}