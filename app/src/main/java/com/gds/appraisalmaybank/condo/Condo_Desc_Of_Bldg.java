package com.gds.appraisalmaybank.condo;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database2.Condo_API_Unit_Details;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Condo_Desc_Of_Bldg extends ListActivity implements OnClickListener {
	DatabaseHandler2 db = new DatabaseHandler2(this);
	String tbl_name="condo_unit_details";
	Button btn_add_desc_of_bldg, btn_save_desc_of_bldg;
	Button btn_create, btn_cancel, btn_save, btn_delete;
	GDS_methods gds = new GDS_methods();
	String record_id="";
	private static final String TAG_RECORD_ID = "record_id";
	private static final String tag_unit_details_id = "bldg_details_id";
	Session_Timer st = new Session_Timer(this);
	Dialog myDialog;
	//create dialog
	AutoCompleteTextView report_unit_no;
	EditText report_desc_of_bldg, report_num_of_storeys, report_floor_location, report_floor_area, report_num_of_bedrooms,
	//Added By IAN
	report_bldg_age,report_developer_name;
	EditText report_unit_features, report_occupants,
		report_owned_or_leased;
	MultiAutoCompleteTextView report_flooring, report_partitions, report_doors, report_windows, report_ceiling;
	Spinner spinner_observed_condition, spinner_type_of_property, spinner_type_of_housing_unit;
	//forListViewDeclarations
	private ProgressDialog pDialog;
	ArrayList<HashMap<String, String>> itemList;
	private static final String tag_building_desc = "building_desc";
	private static final String tag_type_of_property = "type_of_property";
	String unit_details_id="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.report_condo_desc_of_bldg);
		st.resetDisconnectTimer();
		btn_add_desc_of_bldg = (Button) findViewById(R.id.btn_add_desc_of_bldg);
		btn_add_desc_of_bldg.setOnClickListener(this);
		
		btn_save_desc_of_bldg = (Button) findViewById(R.id.btn_save_desc_of_bldg);
		btn_save_desc_of_bldg.setOnClickListener(this);
		
		// getting record_id from intent / previous class
		Intent i = getIntent();
		record_id = i.getStringExtra(TAG_RECORD_ID);
		//list
		itemList = new ArrayList<HashMap<String, String>>();
		new LoadAll().execute();
		// Get listview
		ListView lv = getListView();

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// getting values from selected ListItem
				unit_details_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
				view_details();
			}
		});
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
                // TODO Auto-generated method stub

                Log.v("long clicked","pos: " + position);
                unit_details_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
                delete_item();
                return true;
            }
        }); 
		
	}
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.btn_add_desc_of_bldg:
			add_new();
			break;
		case R.id.btn_save_desc_of_bldg:
			finish();
			break;
		default:
			break;
		}
	}
	
	public void add_new(){
		Toast.makeText(getApplicationContext(), "Add New",Toast.LENGTH_SHORT).show();
		myDialog = new Dialog(Condo_Desc_Of_Bldg.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.report_condo_desc_of_bldg_form);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.FILL_PARENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		
		btn_create = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_create.setText("Create");
		btn_cancel.setText("Cancel");
		
		report_desc_of_bldg = (EditText) myDialog.findViewById(R.id.report_desc_of_bldg);
		report_num_of_storeys = (EditText) myDialog.findViewById(R.id.report_num_of_storeys);
		report_unit_no = (AutoCompleteTextView) myDialog.findViewById(R.id.report_unit_no);
		report_floor_location = (EditText) myDialog.findViewById(R.id.report_floor_location);
		report_floor_area = (EditText) myDialog.findViewById(R.id.report_floor_area);
		report_flooring = (MultiAutoCompleteTextView) myDialog.findViewById(R.id.report_flooring);
		report_partitions = (MultiAutoCompleteTextView) myDialog.findViewById(R.id.report_partitions);
		report_doors = (MultiAutoCompleteTextView) myDialog.findViewById(R.id.report_doors);
		report_windows = (MultiAutoCompleteTextView) myDialog.findViewById(R.id.report_windows);
		report_ceiling = (MultiAutoCompleteTextView) myDialog.findViewById(R.id.report_ceiling);
		report_unit_features = (EditText) myDialog.findViewById(R.id.report_unit_features);
		report_occupants = (EditText) myDialog.findViewById(R.id.report_occupants);
		report_owned_or_leased = (EditText) myDialog.findViewById(R.id.report_owned_or_leased);
		spinner_observed_condition = (Spinner) myDialog.findViewById(R.id.spinner_observed_condition);
		spinner_type_of_property = (Spinner) myDialog.findViewById(R.id.spinner_type_of_property);
		spinner_type_of_housing_unit = (Spinner) myDialog.findViewById(R.id.spinner_type_of_housing_unit);

		report_num_of_bedrooms = (EditText) myDialog.findViewById(R.id.report_num_of_bedrooms);
		//ADDED By Ian
		report_bldg_age = (EditText) myDialog.findViewById(R.id.report_bldg_age);
		report_developer_name = (EditText) myDialog.findViewById(R.id.report_developer_name);
		String unit_no[]=db.getCondo_Title_Details_Unit_no(record_id);
		gds.autocomplete(report_unit_no,unit_no,this);


		gds.multiautocomplete(report_flooring, getResources().getStringArray(R.array.report_flooring), this);
		gds.multiautocomplete(report_partitions, getResources().getStringArray(R.array.report_interior_walls), this);
		gds.multiautocomplete(report_doors, getResources().getStringArray(R.array.report_doors), this);
		gds.multiautocomplete(report_windows, getResources().getStringArray(R.array.report_windows), this);
		gds.multiautocomplete(report_ceiling, getResources().getStringArray(R.array.report_ceiling), this);

		report_floor_area.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_floor_area, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});


		btn_create.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {




					//add data in SQLite
					db.addCondo_Unit_Details(new Condo_API_Unit_Details(
							record_id,
							report_desc_of_bldg.getText().toString(),
							report_num_of_storeys.getText().toString(),
							report_unit_no.getText().toString(),
							report_floor_location.getText().toString(),
							gds.replaceFormat(report_floor_area.getText().toString()),
							report_flooring.getText().toString(),
							report_partitions.getText().toString(),
							report_doors.getText().toString(),
							report_windows.getText().toString(),
							report_ceiling.getText().toString(),
							report_unit_features.getText().toString(),
							report_occupants.getText().toString(),
							report_owned_or_leased.getText().toString(),
							spinner_observed_condition.getSelectedItem().toString(),
							spinner_type_of_property.getSelectedItem().toString(),
							null,
							spinner_type_of_housing_unit.getSelectedItem().toString(),
							report_num_of_bedrooms.getText().toString(),
							report_bldg_age.getText().toString(),
							report_developer_name.getText().toString()));
						Toast.makeText(getApplicationContext(),
								"data created", Toast.LENGTH_SHORT).show();
						myDialog.dismiss();
						reloadClass();

				
			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});

		gds.fill_in_error(new EditText[]{
				report_flooring,
				report_partitions,
				report_doors,
				report_windows,
				report_ceiling,
				report_unit_features,
				report_occupants,
				report_desc_of_bldg,
				report_num_of_storeys,
				report_num_of_bedrooms,
				report_unit_no,
				report_floor_location,
				report_floor_area,
				report_flooring,
				report_partitions,
				report_doors,
				report_windows,
				report_ceiling,
				report_unit_features,
				report_occupants});

		gds.fill_in_error_spinner(new Spinner[]{
				spinner_type_of_housing_unit
		});
		myDialog.show();
	}
	
	public void view_details(){
		myDialog = new Dialog(Condo_Desc_Of_Bldg.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.report_condo_desc_of_bldg_form);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.FILL_PARENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		
		btn_save = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_save.setText("Save");
		btn_cancel.setText("Cancel");
		
		report_desc_of_bldg = (EditText) myDialog.findViewById(R.id.report_desc_of_bldg);
		report_num_of_storeys = (EditText) myDialog.findViewById(R.id.report_num_of_storeys);
		report_unit_no = (AutoCompleteTextView) myDialog.findViewById(R.id.report_unit_no);
		report_floor_location = (EditText) myDialog.findViewById(R.id.report_floor_location);
		report_floor_area = (EditText) myDialog.findViewById(R.id.report_floor_area);
		report_flooring = (MultiAutoCompleteTextView) myDialog.findViewById(R.id.report_flooring);
		report_partitions = (MultiAutoCompleteTextView) myDialog.findViewById(R.id.report_partitions);
		report_doors = (MultiAutoCompleteTextView) myDialog.findViewById(R.id.report_doors);
		report_windows = (MultiAutoCompleteTextView) myDialog.findViewById(R.id.report_windows);
		report_ceiling = (MultiAutoCompleteTextView) myDialog.findViewById(R.id.report_ceiling);
		report_unit_features = (EditText) myDialog.findViewById(R.id.report_unit_features);
		report_occupants = (EditText) myDialog.findViewById(R.id.report_occupants);
		report_owned_or_leased = (EditText) myDialog.findViewById(R.id.report_owned_or_leased);
		spinner_observed_condition = (Spinner) myDialog.findViewById(R.id.spinner_observed_condition);
		spinner_type_of_property = (Spinner) myDialog.findViewById(R.id.spinner_type_of_property);
		spinner_type_of_housing_unit = (Spinner) myDialog.findViewById(R.id.spinner_type_of_housing_unit);
		
		report_num_of_bedrooms = (EditText) myDialog.findViewById(R.id.report_num_of_bedrooms);
		report_bldg_age = (EditText) myDialog.findViewById(R.id.report_bldg_age);
		report_developer_name = (EditText) myDialog.findViewById(R.id.report_developer_name);
		String unit_no[]=db.getCondo_Title_Details_Unit_no(record_id);
		gds.autocomplete(report_unit_no,unit_no,this);
		gds.multiautocomplete(report_flooring, getResources().getStringArray(R.array.report_flooring), this);
		gds.multiautocomplete(report_partitions, getResources().getStringArray(R.array.report_interior_walls), this);
		gds.multiautocomplete(report_doors, getResources().getStringArray(R.array.report_doors), this);
		gds.multiautocomplete(report_windows, getResources().getStringArray(R.array.report_windows), this);
		gds.multiautocomplete(report_ceiling, getResources().getStringArray(R.array.report_ceiling), this);

		report_floor_area.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_floor_area, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});

		List<Condo_API_Unit_Details> liid = db.getCondo_Unit_Details(record_id, unit_details_id, tbl_name);
		if (!liid.isEmpty()) {
			for (Condo_API_Unit_Details imid : liid) {
				unit_details_id = String.valueOf(imid.getID());
				report_desc_of_bldg.setText(imid.getreport_unit_description());
				report_num_of_storeys.setText(imid.getreport_unit_no_of_storeys());
				report_unit_no.setText(imid.getreport_unit_unit_no());
				report_floor_location.setText(imid.getreport_unit_floor_location());
				report_floor_area.setText(gds.numberFormat(imid.getreport_unit_floor_area()));
				report_flooring.setText(imid.getreport_unit_interior_flooring());
				report_partitions.setText(imid.getreport_unit_interior_partitions());
				report_doors.setText(imid.getreport_unit_interior_doors());
				report_windows.setText(imid.getreport_unit_interior_windows());
				report_ceiling.setText(imid.getreport_unit_interior_ceiling());
				report_unit_features.setText(imid.getreport_unit_features());
				report_occupants.setText(imid.getreport_unit_occupants());
				report_owned_or_leased.setText(imid.getreport_unit_owned_or_leased());
				spinner_observed_condition.setSelection(gds.spinnervalue(spinner_observed_condition,imid.getreport_unit_observed_condition()));
				spinner_type_of_property.setSelection(gds.spinnervalue(spinner_type_of_property,imid.getreport_unit_ownership_of_property()));
				spinner_type_of_housing_unit.setSelection(gds.spinnervalue(spinner_type_of_housing_unit,imid.getreport_unit_type_of_property()));

				
				report_num_of_bedrooms.setText(imid.getreport_no_of_bedrooms());
				//Added By Ian
				report_bldg_age.setText(imid.getreport_bldg_age());
				report_developer_name.setText(imid.getreport_developer_name());
			}
		}
		
		btn_save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

					Condo_API_Unit_Details c = new Condo_API_Unit_Details();
					c.setreport_unit_description(report_desc_of_bldg.getText().toString());
					c.setreport_unit_no_of_storeys(report_num_of_storeys.getText().toString());
					c.setreport_unit_unit_no(report_unit_no.getText().toString());
					c.setreport_unit_floor_location(report_floor_location.getText().toString());
					c.setreport_unit_floor_area(gds.replaceFormat(report_floor_area.getText().toString()));
					c.setreport_unit_interior_flooring(report_flooring.getText().toString());
					c.setreport_unit_interior_partitions(report_partitions.getText().toString());
					c.setreport_unit_interior_doors(report_doors.getText().toString());
					c.setreport_unit_interior_windows(report_windows.getText().toString());
					c.setreport_unit_interior_ceiling(report_ceiling.getText().toString());
					c.setreport_unit_features(report_unit_features.getText().toString());
					c.setreport_unit_occupants(report_occupants.getText().toString());
					c.setreport_unit_owned_or_leased(report_owned_or_leased.getText().toString());
					c.setreport_unit_observed_condition(spinner_observed_condition.getSelectedItem().toString());
					c.setreport_unit_ownership_of_property(spinner_type_of_property.getSelectedItem().toString());
					c.setreport_unit_socialized_housing(null);
					c.setreport_unit_type_of_property(spinner_type_of_housing_unit.getSelectedItem().toString());
					c.setreport_no_of_bedrooms(report_num_of_bedrooms.getText().toString());
					//ADDED By IAN
					c.setreport_bldg_age(report_bldg_age.getText().toString());
					c.setreport_developer_name(report_developer_name.getText().toString());


					db.updateCondo_Unit_Details(c, record_id, unit_details_id, tbl_name);
					db.close();
						Toast.makeText(getApplicationContext(),
								"data created", Toast.LENGTH_SHORT).show();
						myDialog.dismiss();
						reloadClass();

				
			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});


		//list of EditTexts put in array
		gds.fill_in_error(new EditText[]{
				report_flooring,
				report_partitions,
				report_doors,
				report_windows,
				report_ceiling,
				report_unit_features,
				report_occupants,
				report_desc_of_bldg,
				report_num_of_storeys,
				report_num_of_bedrooms,
				report_unit_no,
				report_floor_location,
				report_floor_area,
				report_flooring,
				report_partitions,
				report_doors,
				report_windows,
				report_ceiling,
				report_unit_features,
				report_occupants});

		gds.fill_in_error_spinner(new Spinner[]{
				spinner_type_of_housing_unit
		});
		myDialog.show();
	}
	
	//EditText checker
	private boolean validate(EditText[] fields){
        for(int i=0; i<fields.length; i++){
            EditText currentField=fields[i];
            if(currentField.getText().toString().length()<=0){
                return false;
            }
        }
        return true;
	}
	public void reloadClass(){
		Intent intent = getIntent();
		finish();
		startActivity(intent);
	}
	public void delete_item(){
		myDialog = new Dialog(Condo_Desc_Of_Bldg.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.delete_dialog);
		myDialog.setCancelable(true);
		final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
		final Button btn_yes = (Button) myDialog.findViewById(R.id.btn_yes);
		final Button btn_no = (Button) myDialog.findViewById(R.id.btn_no);
		tv_question.setText("Are you sure you want to delete the selected item?");
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//sub table details must be deleted first
				db.deleteCondo_Unit_Details(record_id, unit_details_id, tbl_name);//delete desc of bldg
				db.close();
				myDialog.dismiss();
				reloadClass();
				Toast.makeText(getApplicationContext(), "Data Deleted",
						Toast.LENGTH_SHORT).show();
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		
		myDialog.show();
	}
	// Response from LI_Desc_Of_Imp_View Activity
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// if result code 100
		if (resultCode == 100) {
			// if result code 100 is received 
			// means user edited/deleted product
			// reload this screen again
			Intent intent = getIntent();
			finish();
			startActivity(intent);
		}
	}
	
	/**
	 * Background Async Task to Load all 
	 * */
	class LoadAll extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Condo_Desc_Of_Bldg.this);
			pDialog.setMessage("Loading List. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All 
		 * */
		protected String doInBackground(String... args) {
			String building_desc="", type_of_property="";

			List<Condo_API_Unit_Details> liid = db.getCondo_Unit_Details(record_id, tbl_name);
			if (!liid.isEmpty()) {
				for (Condo_API_Unit_Details imid : liid) {
					unit_details_id = String.valueOf(imid.getID());
					building_desc = imid.getreport_unit_description();
					type_of_property = imid.getreport_unit_type_of_property();
					
					HashMap<String, String> map = new HashMap<String, String>();
					map.put(tag_unit_details_id, unit_details_id);
					map.put(tag_building_desc, building_desc);
					map.put(tag_type_of_property, type_of_property);
					//Toast.makeText(getApplicationContext(), ""+itemList,Toast.LENGTH_SHORT).show();

					// adding HashList to ArrayList
					itemList.add(map);
				}
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					ListAdapter adapter = new SimpleAdapter(
							Condo_Desc_Of_Bldg.this, itemList,
							R.layout.report_condo_desc_of_bldg_list, new String[] { TAG_RECORD_ID, tag_unit_details_id,
									tag_building_desc,
									tag_type_of_property},
									new int[] { R.id.report_record_id, R.id.primary_key, R.id.item_desc_of_bldg, R.id.item_type_of_property });
					// updating listview
					setListAdapter(adapter);
				}
			});
			if(itemList.isEmpty()){
				add_new();
			}
		}
	}
	private boolean validateSpinner(Spinner[] fields) {
		for (int i = 0; i < fields.length; i++) {
			Spinner currentField = fields[i];
			if (currentField.getSelectedItem().toString().length() <= 0) {
				return false;
			}
		}
		return true;
	}

	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}


}