package com.gds.appraisalmaybank.condo;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database2.Condo_API_RDPS;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Condo_RDPS extends ListActivity implements OnClickListener {
	DatabaseHandler2 db = new DatabaseHandler2(this);
	String tbl_name="condo_prev_appraisal";
	GDS_methods gds = new GDS_methods();
	Button btn_add_rdps, btn_save_rdps;
	Button btn_create, btn_cancel, btn_save;

	Session_Timer st = new Session_Timer(this);
	String record_id="";
	private static final String TAG_RECORD_ID = "record_id";
	
	Dialog myDialog;
	//create dialog
	EditText  report_account_name, report_location,	report_area, report_value, report_date_appraised;

	//forListViewDeclarations
	private ProgressDialog pDialog;
	ArrayList<HashMap<String, String>> itemList;
	private static final String tag_rdps_id = "rdps_id";
	private static final String tag_account_name = "account_name";
	private static final String tag_prev_app_date = "prev_app_date";
	String rdps_id="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reports_rdps);
		st.resetDisconnectTimer();
		btn_add_rdps = (Button) findViewById(R.id.btn_add_rdps);
		btn_add_rdps.setOnClickListener(this);
		
		btn_save_rdps = (Button) findViewById(R.id.btn_save_rdps);
		btn_save_rdps.setOnClickListener(this);
		
		// getting record_id from intent / previous class
		Intent i = getIntent();
		record_id = i.getStringExtra(TAG_RECORD_ID);
		//list
		itemList = new ArrayList<HashMap<String, String>>();
		// Loading in Background Thread
		new LoadAll().execute();
		// Get listview
		ListView lv = getListView();
		// on seleting single item
		// launching Edit item Screen
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// getting values from selected ListItem
				rdps_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
				view_details();
			}
		});
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
                // TODO Auto-generated method stub

                Log.v("long clicked","pos: " + position);
                rdps_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
                delete_item();
                return true;
            }
        }); 
		
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		this.finish();
		Intent intent = new Intent(Condo_RDPS.this, Condo.class);
		intent.putExtra("keyopen","1");
		startActivity(intent);
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.btn_add_rdps:
			add_new();
			break;
		case R.id.btn_save_rdps:
			this.finish();
			Intent intent = new Intent(Condo_RDPS.this, Condo.class);
			intent.putExtra("keyopen","1");
			startActivity(intent);
			break;
		default:
			break;
		}
	}
	
	public void add_new(){
		Toast.makeText(getApplicationContext(), "Add New",
				Toast.LENGTH_SHORT).show();
		myDialog = new Dialog(Condo_RDPS.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.reports_rdps_form);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		
		btn_create = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_create.setText("Create");
		btn_cancel.setText("Cancel");
		report_account_name = (EditText) myDialog.findViewById(R.id.report_account_name);
		report_location = (EditText) myDialog.findViewById(R.id.report_location);
		report_area = (EditText) myDialog.findViewById(R.id.report_area);
		report_value = (EditText) myDialog.findViewById(R.id.report_value);
		report_date_appraised = (EditText) myDialog.findViewById(R.id.report_date_appraised);

		report_area.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub

				current = gds.numberFormat(report_area, this, s, current);
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_value.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub

				current = gds.numberFormat(report_value, this, s, current);
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});

		btn_create.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

					//add data in SQLite
					db.addCondo_RDPS(new Condo_API_RDPS(
							record_id,
							report_account_name.getText().toString(),
							report_location.getText().toString(),
							gds.replaceFormat(report_area.getText().toString()),
							gds.replaceFormat(report_value.getText().toString()),
							report_date_appraised.getText().toString()));
					
						Toast.makeText(getApplicationContext(),"data created", Toast.LENGTH_SHORT).show();
						myDialog.dismiss();
						reloadClass();

				
			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		myDialog.show();
	}
	
	public void view_details(){
		myDialog = new Dialog(Condo_RDPS.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.reports_rdps_form);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		
		btn_save = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_save.setText("Save");
		btn_cancel.setText("Cancel");
		
		report_account_name = (EditText) myDialog.findViewById(R.id.report_account_name);
		report_location = (EditText) myDialog.findViewById(R.id.report_location);
		report_area = (EditText) myDialog.findViewById(R.id.report_area);
		report_value = (EditText) myDialog.findViewById(R.id.report_value);
		report_date_appraised = (EditText) myDialog.findViewById(R.id.report_date_appraised);

		report_area.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub

				current = gds.numberFormat(report_area, this, s, current);
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});
		report_value.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub

				current = gds.numberFormat(report_value, this, s, current);
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});

		List<Condo_API_RDPS> rdpsList = db
				.getCondo_RDPS_Single(String.valueOf(record_id), String.valueOf(rdps_id), tbl_name);
		if (!rdpsList.isEmpty()) {
			for (Condo_API_RDPS li : rdpsList) {
				report_account_name.setText(li.getreport_prev_app_name());
				report_location.setText(li.getreport_prev_app_location());
				report_area.setText(gds.numberFormat(li.getreport_prev_app_area()));
				report_value.setText(gds.numberFormat(li.getreport_prev_app_value()));
				report_date_appraised.setText(li.getreport_prev_app_date());
			}
		}
		
		btn_save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {


					Condo_API_RDPS li = new Condo_API_RDPS();
					li.setreport_prev_app_name(report_account_name.getText().toString());
					li.setreport_prev_app_location(report_location.getText().toString());
					li.setreport_prev_app_area(gds.replaceFormat(report_area.getText().toString()));
					li.setreport_prev_app_value(gds.replaceFormat(report_value.getText().toString()));
					li.setreport_prev_app_date(report_date_appraised.getText().toString());

					db.updateCondo_RDPS(li, record_id, String.valueOf(rdps_id));
					db.close();
					myDialog.dismiss();
					Toast.makeText(getApplicationContext(), "Saved",Toast.LENGTH_SHORT).show();
					reloadClass();

			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		myDialog.show();
	}
	
	//EditText checker
	private boolean validate(EditText[] fields){
        for(int i=0; i<fields.length; i++){
            EditText currentField=fields[i];
            if(currentField.getText().toString().length()<=0){
                return false;
            }
        }
        return true;
	}
	public void reloadClass(){
		Intent intent = getIntent();
		finish();
		startActivity(intent);
	}
	public void delete_item(){
		myDialog = new Dialog(Condo_RDPS.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.delete_dialog);
		myDialog.setCancelable(true);
		final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
		final Button btn_yes = (Button) myDialog.findViewById(R.id.btn_yes);
		final Button btn_no = (Button) myDialog.findViewById(R.id.btn_no);
		tv_question.setText("Are you sure you want to delete the selected item?");
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				db.deleteCondo_RDPS_Single(record_id, rdps_id, tbl_name);
				db.close();
				myDialog.dismiss();
				reloadClass();
				Toast.makeText(getApplicationContext(), "Data Deleted",Toast.LENGTH_SHORT).show();
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		
		myDialog.show();
	}
	
	
	/**
	 * Background Async Task to Load all
	 * */
	class LoadAll extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Condo_RDPS.this);
			pDialog.setMessage("Loading Lists. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All
		 * */
		protected String doInBackground(String... args) {
			String account_name="", prev_app_date="";
			

			List<Condo_API_RDPS> lirdps = db.getCondo_RDPS(record_id, tbl_name);
			if (!lirdps.isEmpty()) {
				for (Condo_API_RDPS imrdps : lirdps) {
					rdps_id = String.valueOf(imrdps.getID());
					record_id = imrdps.getrecord_id();					
					account_name = imrdps.getreport_prev_app_name();
					prev_app_date = imrdps.getreport_prev_app_date();
					
					HashMap<String, String> map = new HashMap<String, String>();
					map.put(TAG_RECORD_ID, record_id);
					map.put(tag_rdps_id, rdps_id);
					map.put(tag_account_name, account_name);
					map.put(tag_prev_app_date, prev_app_date);

					// adding HashList to ArrayList
					itemList.add(map);
				}
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					ListAdapter adapter = new SimpleAdapter(
							Condo_RDPS.this, itemList,
							R.layout.reports_rdps_list, new String[] { TAG_RECORD_ID, tag_rdps_id, tag_account_name,
									tag_prev_app_date},
									new int[] { R.id.report_record_id, R.id.primary_key, R.id.item_account_name, R.id.item_date_appraised});
					// updating listview
					setListAdapter(adapter);
				}
			});
			if(itemList.isEmpty()){
				add_new();
			}
		}
	}
	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}


}