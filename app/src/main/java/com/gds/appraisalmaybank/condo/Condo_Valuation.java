package com.gds.appraisalmaybank.condo;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database.Condo_API;
import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database2.Condo_API_Valuation;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Condo_Valuation extends ListActivity implements OnClickListener {
	DatabaseHandler2 db = new DatabaseHandler2(this);
	DatabaseHandler dbMain = new DatabaseHandler(this);
	GDS_methods gds = new GDS_methods();
	String tbl_name="condo_unit_valuation";
	Button btn_add_valuation, btn_view_total_valuation, btn_update_valuation;
	Session_Timer st = new Session_Timer(this);
	Button btn_create, btn_cancel, btn_save, btn_compute;
	
	String record_id="";
	
	private static final String TAG_RECORD_ID = "record_id";
	
	Dialog myDialog;
	
	EditText report_grand_total_area, report_grand_total_deduc, report_grand_total_net_area, report_grand_total_land_value;
	//create dialog
	Spinner spinner_property_type;
	EditText report_cct_no, report_unit_no, report_floor_area, report_deduc, report_net_area, report_unit_value, report_total_land_val;
	
	//forListViewDeclarations
	private ProgressDialog pDialog;
	ArrayList<HashMap<String, String>> itemList;
	private static final String tag_unit_valuation_id = "unit_valuation_id";
	private static final String tag_cct_no = "tct_no";
	private static final String tag_floor_area = "area";
	private static final String tag_deduc = "deduc";
	private static final String tag_net_area = "net_area";
	private static final String tag_land_value = "land_value";
	String unit_valuation_id="", cct_no="", floor_area="", deduc="", net_area="", land_value="";
	//total
	//int grand_total_area=0, grand_total_deduc=0, grand_total_net_area=0, grand_total_land_value=0;
	//computation variables


	
	String grand_total_land_val_d = "0";
	
	//net_area from comparative
	String fixed_unit_value="";
	int item_position=-1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.report_condo_valuation);
		st.resetDisconnectTimer();
		btn_add_valuation = (Button) findViewById(R.id.btn_add_valuation);
		btn_add_valuation.setOnClickListener(this);
		btn_view_total_valuation = (Button) findViewById(R.id.btn_view_total_valuation);
		btn_view_total_valuation.setOnClickListener(this);
		
		btn_update_valuation = (Button) findViewById(R.id.btn_update_valuation);
		btn_update_valuation.setOnClickListener(this);
		
		
		// getting record_id from intent / previous class
		Intent i = getIntent();
		record_id = i.getStringExtra(TAG_RECORD_ID);
		//list
		itemList = new ArrayList<HashMap<String, String>>();
		// Loading in Background Thread
		new LoadAll().execute();
		// Get listview
		ListView lv = getListView();
		// on seleting single item
		// launching Edit item Screen
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// getting values from selected ListItem
				unit_valuation_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
				view_details();

			}
		});
		/*lv.setOnItemLongClickListener(new OnItemLongClickListener() {

			public boolean onItemLongClick(AdapterView<?> parent, View view,
										   int position, long id) {
				// TODO Auto-generated method stub

				Log.v("long clicked", "pos: " + position);
				unit_valuation_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
				delete_item();
				return true;
			}
		}); */
	}
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.btn_add_valuation:
			add_new();
			break;
		case R.id.btn_update_valuation:
			save_summary_total();
			finish();
			break;
		default:
			break;
		}
	}
	
	
	public void add_new(){
		Toast.makeText(Condo_Valuation.this, "Add New",
				Toast.LENGTH_SHORT).show();
		myDialog = new Dialog(Condo_Valuation.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.report_condo_valuation_form);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.FILL_PARENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		
		btn_create = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_create.setText("Create");
		btn_cancel.setText("Cancel");
		
		spinner_property_type = (Spinner) myDialog.findViewById(R.id.spinner_property_type);
		report_cct_no = (EditText) myDialog.findViewById(R.id.report_cct_no);
		report_unit_no = (EditText) myDialog.findViewById(R.id.report_unit_no);
		report_floor_area = (EditText) myDialog.findViewById(R.id.report_floor_area);
		report_deduc = (EditText) myDialog.findViewById(R.id.report_deduc);
		report_net_area = (EditText) myDialog.findViewById(R.id.report_net_area);
		report_unit_value = (EditText) myDialog.findViewById(R.id.report_unit_value);
		report_total_land_val = (EditText) myDialog.findViewById(R.id.report_total_land_val);
		
		
		btn_create.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				 //check if all EditTexts in array are filled up
					//add data in SQLite
					db.addCondo_Valuation(new Condo_API_Valuation(
							record_id,
							spinner_property_type.getSelectedItem().toString(),
							report_cct_no.getText().toString(),
							report_unit_no.getText().toString(),
							report_floor_area.getText().toString(),
							report_deduc.getText().toString(),
							report_net_area.getText().toString(),
							report_unit_value.getText().toString(),
							report_total_land_val.getText().toString()));
						Toast.makeText(getApplicationContext(),"data created", Toast.LENGTH_SHORT).show();
						//auto-save of total
						myDialog.dismiss();
						reloadClass();

				
			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
				reloadClass();
			}
		});
		gds.fill_in_error(new EditText[]{report_deduc, report_unit_value,
				report_total_land_val});
		myDialog.show();
	}
	
	public void view_details(){
		myDialog = new Dialog(Condo_Valuation.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.report_condo_valuation_form);
		myDialog.setCancelable(false);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.FILL_PARENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		
		//btn_compute = (Button) myDialog.findViewById(R.id.btn_compute);
		btn_save = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_save.setText("Save");
		btn_cancel.setText("Cancel");
		spinner_property_type = (Spinner) myDialog.findViewById(R.id.spinner_property_type);
		report_cct_no = (EditText) myDialog.findViewById(R.id.report_cct_no);
		report_unit_no = (EditText) myDialog.findViewById(R.id.report_unit_no);
		report_floor_area = (EditText) myDialog.findViewById(R.id.report_floor_area);
		report_deduc = (EditText) myDialog.findViewById(R.id.report_deduc);
		report_net_area = (EditText) myDialog.findViewById(R.id.report_net_area);
		report_unit_value = (EditText) myDialog.findViewById(R.id.report_unit_value);
		report_total_land_val = (EditText) myDialog.findViewById(R.id.report_total_land_val);


		//Added By IAN

			List<Condo_API_Valuation> lotValuationDetailsList2 = db
					.getCondo_Valuation(String.valueOf(record_id), String.valueOf(unit_valuation_id), tbl_name);
			if (!lotValuationDetailsList2.isEmpty()) {
				for (Condo_API_Valuation vl : lotValuationDetailsList2) {
					report_unit_value.setText(gds.numberFormat(vl.getreport_value_unit_value()));
				}
			}



		List<Condo_API_Valuation> lotValuationDetailsList = db
				.getCondo_Valuation(String.valueOf(record_id), String.valueOf(unit_valuation_id), tbl_name);
		if (!lotValuationDetailsList.isEmpty()) {
			for (Condo_API_Valuation vl : lotValuationDetailsList) {
				
				if (vl.getreport_value_property_type().equals("Unit")){
					spinner_property_type.setSelection(0);
				} else if (vl.getreport_value_property_type().equals("Parking_Slot")){
					spinner_property_type.setSelection(1);
				} else if (vl.getreport_value_property_type().equals("Drying_Area")){
					spinner_property_type.setSelection(2);
				}
				
				report_cct_no.setText(vl.getreport_value_cct_no());
				report_unit_no.setText(vl.getreport_value_unit_no());
				report_floor_area.setText(gds.numberFormat(vl.getreport_value_floor_area()));
				report_deduc.setText(gds.numberFormat(vl.getreport_value_deduction()));
				report_net_area.setText(gds.numberFormat(vl.getreport_value_net_area()));
				//report_unit_value.setText(vl.getreport_value_unit_value());
				report_total_land_val.setText(gds.numberFormat(vl.getreport_value_appraised_value()));
			}
		}
		
		//spinner check if Unit / drying area / parking slot
		if (spinner_property_type.getSelectedItem().toString().equals("Unit")){
			List<Condo_API> vl = dbMain.getCondo(String.valueOf(record_id));
			if (!vl.isEmpty()) {
				for (Condo_API im : vl) {
					fixed_unit_value = im.getreport_comp_rounded_to();
				}
			}
			report_unit_value.setEnabled(false);
			report_unit_value.setText(gds.numberFormat(fixed_unit_value));
		}

		String total_area_val = gds.nullCheck2(report_floor_area.getText().toString());
		String deduc_val = gds.nullCheck2(report_deduc.getText().toString());
		String net_area_val = gds.netAreaVL(total_area_val,deduc_val);
		report_net_area.setText(gds.numberFormat(net_area_val));

		String unit_value_val = gds.nullCheck2(report_unit_value.getText().toString());
		String total_land_val_val = gds.totalLandVL(net_area_val,unit_value_val);
		report_total_land_val.setText(gds.numberFormat(total_land_val_val));

		report_floor_area.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_floor_area, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});

		report_net_area.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_net_area, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});

		report_total_land_val.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_total_land_val, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});

		report_deduc.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_deduc, this, s, current);

				String total_area_val = gds.nullCheck2(report_floor_area.getText().toString());
				String deduc_val = gds.nullCheck2(report_deduc.getText().toString());
				String net_area_val = gds.netAreaVL(total_area_val,deduc_val);
				report_net_area.setText(net_area_val);

				String unit_value_val = gds.nullCheck2(report_unit_value.getText().toString());
				String total_land_val_val = gds.totalLandVL(net_area_val,unit_value_val);
				report_total_land_val.setText(total_land_val_val);

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		report_unit_value.addTextChangedListener(new TextWatcher() {
			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				current = gds.numberFormat(report_unit_value, this, s, current);

				String total_area_val = gds.nullCheck2(report_floor_area.getText().toString());
				String deduc_val = gds.nullCheck2(report_deduc.getText().toString());
				String net_area_val = gds.netAreaVL(total_area_val,deduc_val);
				report_net_area.setText(net_area_val);

				String unit_value_val = gds.nullCheck2(report_unit_value.getText().toString());
				String total_land_val_val = gds.totalLandVL(net_area_val,unit_value_val);
				report_total_land_val.setText(total_land_val_val);

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		
		btn_save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

					//update data in SQLite
					Condo_API_Valuation vl = new Condo_API_Valuation();
					vl.setreport_value_property_type(spinner_property_type.getSelectedItem().toString());
					vl.setreport_value_cct_no(report_cct_no.getText().toString());
					vl.setreport_value_unit_no(report_unit_no.getText().toString());
					vl.setreport_value_floor_area(gds.replaceFormat(report_floor_area.getText().toString()));
					vl.setreport_value_deduction(gds.replaceFormat(report_deduc.getText().toString()));
					vl.setreport_value_net_area(gds.replaceFormat(report_net_area.getText().toString()));
					//vl.setreport_value_net_area(total_area_val);
					vl.setreport_value_unit_value(gds.replaceFormat(report_unit_value.getText().toString()));
					vl.setreport_value_appraised_value(gds.replaceFormat(report_total_land_val.getText().toString()));
					//vl.setreport_value_appraised_value(String.valueOf(df.format(total_land_val_val)));
					
					db.updateCondo_Valuation(vl, record_id, unit_valuation_id, tbl_name);
					db.close();
					myDialog.dismiss();
					Toast.makeText(getApplicationContext(), "Saved",Toast.LENGTH_SHORT).show();
					reloadClass();

			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});

		gds.fill_in_error(new EditText[]{report_deduc, report_unit_value,
				report_total_land_val});
		myDialog.show();
	}
	//EditText checker
	private boolean validate(EditText[] fields){
        for(int i=0; i<fields.length; i++){
            EditText currentField=fields[i];
            if(currentField.getText().toString().length()<=0){
                return false;
            }
        }
        return true;
	}
	
	public void reloadClass(){
		Intent intent = getIntent();
		finish();
		startActivity(intent);
	}
	
	public void delete_item(){
		myDialog = new Dialog(Condo_Valuation.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.delete_dialog);
		myDialog.setCancelable(true);
		final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
		final Button btn_yes = (Button) myDialog.findViewById(R.id.btn_yes);
		final Button btn_no = (Button) myDialog.findViewById(R.id.btn_no);
		tv_question.setText("Are you sure you want to delete the selected item?");
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				db.deleteCondo_Valuation(record_id, unit_valuation_id, tbl_name);
				db.close();
				myDialog.dismiss();
				reloadClass();
				Toast.makeText(getApplicationContext(), "Data Deleted",Toast.LENGTH_SHORT).show();
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		
		myDialog.show();
	}
	
	public void save_summary_total(){
		List<Condo_API_Valuation> vllvd = db.getCondo_Valuation(record_id, tbl_name);
		if (!vllvd.isEmpty()) {
			for (Condo_API_Valuation imlvd : vllvd) {
				land_value = imlvd.getreport_value_appraised_value();
				grand_total_land_val_d = gds.addition(land_value,grand_total_land_val_d);
			}
		}
		Condo_API vl = new Condo_API();
		vl.setreport_final_value_total_appraised_value(gds.replaceFormat(grand_total_land_val_d));
		dbMain.updateCondo_Summary_Total(vl, record_id);
		dbMain.close();
	}
	
	/**
	 * Background Async Task to Load all
	 * */
	class LoadAll extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Condo_Valuation.this);
			pDialog.setMessage("Loading List. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All
		 * */
		protected String doInBackground(String... args) {
			
			List<Condo_API_Valuation> vllvd = db.getCondo_Valuation(record_id, tbl_name);
			if (!vllvd.isEmpty()) {
				for (Condo_API_Valuation imlvd : vllvd) {
					unit_valuation_id = String.valueOf(imlvd.getID());
					cct_no = imlvd.getreport_value_cct_no();
					floor_area = gds.numberFormat(imlvd.getreport_value_floor_area());
					deduc = gds.numberFormat(imlvd.getreport_value_deduction());
					net_area = gds.numberFormat(imlvd.getreport_value_net_area());
					land_value = gds.numberFormat(imlvd.getreport_value_appraised_value());

					HashMap<String, String> map = new HashMap<String, String>();
					map.put(tag_unit_valuation_id, unit_valuation_id);
					map.put(tag_cct_no, cct_no);
					map.put(tag_floor_area, floor_area);
					map.put(tag_deduc, deduc);
					map.put(tag_net_area, net_area);
					map.put(tag_land_value, land_value);


					// adding HashList to ArrayList
					itemList.add(map);
				}
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					ListAdapter adapter = new SimpleAdapter(
							Condo_Valuation.this, itemList,
							R.layout.report_condo_valuation_list, new String[] {
									tag_unit_valuation_id, tag_cct_no,
									tag_floor_area, tag_deduc, tag_net_area,
									tag_land_value }, new int[] {
									R.id.primary_key, R.id.item_cct_no,
									R.id.item_total_area, R.id.item_deduc,
									R.id.item_net_area,
									R.id.item_total_land_value });
					// updating listview
					setListAdapter(adapter);
				}
			});

		}
	}

	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}
}