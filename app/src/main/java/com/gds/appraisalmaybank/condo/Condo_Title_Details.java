package com.gds.appraisalmaybank.condo;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gds.appraisalmaybank.database.Condo_API;
import com.gds.appraisalmaybank.database.DatabaseHandler;
import com.gds.appraisalmaybank.database2.Condo_API_Title_Details;
import com.gds.appraisalmaybank.database2.Condo_API_Valuation;
import com.gds.appraisalmaybank.database2.DatabaseHandler2;
import com.gds.appraisalmaybank.main.R;
import com.gds.appraisalmaybank.methods.GDS_methods;
import com.gds.appraisalmaybank.methods.Session_Timer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Condo_Title_Details extends ListActivity implements OnClickListener {
	DatabaseHandler2 db = new DatabaseHandler2(this);
	DatabaseHandler dbMain = new DatabaseHandler(this);
	GDS_methods gds = new GDS_methods();
	String tbl_name="condo_title_details";
	
	Button btn_add_prop_desc, btn_save_prop_desc;
	Button btn_create, btn_cancel, btn_save;
	Session_Timer st = new Session_Timer(this);
	String record_id="";
	private static final String TAG_RECORD_ID = "record_id";
	
	Dialog myDialog;
	//create dialog
	EditText valrep_condo_propdesc_registry_date;
	EditText report_title_no, report_unit_no, report_floor, report_building_name,
		report_fa, report_deeds, report_reg_owner;
	Spinner spinner_property_type;

	//forListViewDeclarations
	private ProgressDialog pDialog;
	ArrayList<HashMap<String, String>> itemList;
	private static final String tag_lot_details_id = "lot_details_id";
	private static final String tag_cct_no = "cct_no";
	private static final String tag_reg_owner = "reg_owner";
	private static final String tag_date_reg = "date_reg";
	String lot_details_id="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.report_condo_prop_desc);
		st.resetDisconnectTimer();
		btn_add_prop_desc = (Button) findViewById(R.id.btn_add_prop_desc);
		btn_add_prop_desc.setOnClickListener(this);
		
		btn_save_prop_desc = (Button) findViewById(R.id.btn_save_prop_desc);
		btn_save_prop_desc.setOnClickListener(this);
		
		// getting record_id from intent / previous class
		Intent i = getIntent();
		record_id = i.getStringExtra(TAG_RECORD_ID);
		//list
		itemList = new ArrayList<HashMap<String, String>>();
		// Loading in Background Thread
		new LoadAll().execute();
		// Get listview
		ListView lv = getListView();

		// on seleting single item
		// launching Edit item Screen
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// getting values from selected ListItem
				lot_details_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
				view_details();
			}
		});
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
                // TODO Auto-generated method stub

                Log.v("long clicked","pos: " + position);
                lot_details_id = ((TextView) view.findViewById(R.id.primary_key)).getText()
						.toString();
                delete_item();
                return true;
            }
        }); 
		
	}
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.btn_add_prop_desc:
			add_new();
			break;
		case R.id.btn_save_prop_desc:
			finish();
			break;
		default:
			break;
		}
	}
	
	public void add_new(){
		Toast.makeText(getApplicationContext(), "Add New",
				Toast.LENGTH_SHORT).show();
		myDialog = new Dialog(Condo_Title_Details.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.report_condo_prop_desc_form);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.FILL_PARENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		
		btn_create = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_create.setText("Create");
		btn_cancel.setText("Cancel");
		
		spinner_property_type = (Spinner) myDialog.findViewById(R.id.spinner_property_type);
		
		report_title_no = (EditText) myDialog.findViewById(R.id.report_title_no);
		report_unit_no = (EditText) myDialog.findViewById(R.id.report_unit_no);
		report_floor = (EditText) myDialog.findViewById(R.id.report_floor);
		report_building_name = (EditText) myDialog.findViewById(R.id.report_building_name);
		report_fa = (EditText) myDialog.findViewById(R.id.report_fa);
		report_deeds = (EditText) myDialog.findViewById(R.id.report_deeds);
		report_reg_owner = (EditText) myDialog.findViewById(R.id.report_reg_owner);

		valrep_condo_propdesc_registry_date = (EditText) myDialog.findViewById(R.id.valrep_condo_propdesc_registry_date);

		report_fa.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub

				current = gds.numberFormat(report_fa, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});

		btn_create.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				//list of EditTexts put in array

					//adjust for month

					//add data in SQLite
					db.addCondo_Title_Details(new Condo_API_Title_Details(
							record_id,
							spinner_property_type.getSelectedItem().toString(),
							report_title_no.getText().toString(),
							report_unit_no.getText().toString(),
							report_floor.getText().toString(),
							report_building_name.getText().toString(),
							gds.replaceFormat(report_fa.getText().toString()),
							valrep_condo_propdesc_registry_date.getText().toString(),
							report_deeds.getText().toString(),
							report_reg_owner.getText().toString()));


					//sync with valuation (if 3 records added, then 3 records should also be created to valuation
					db.addCondo_Valuation(new Condo_API_Valuation(
							record_id,
							spinner_property_type.getSelectedItem().toString(),
							report_title_no.getText().toString(),
							report_unit_no.getText().toString(),
							gds.replaceFormat(report_fa.getText().toString()),
							"0",//report_deduc.getText().toString(),
							"0",//report_net_area.getText().toString(),
							"0",//report_unit_value.getText().toString(),
							"0"));//report_total_land_val.getText().toString()));
						Toast.makeText(getApplicationContext(),
								"data created", Toast.LENGTH_SHORT).show();
						myDialog.dismiss();
						reloadClass();

				
			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		gds.fill_in_error(new EditText[]{report_title_no, report_unit_no, report_floor,
				report_building_name, report_fa,valrep_condo_propdesc_registry_date,report_deeds,report_reg_owner});
		myDialog.show();
	}
	
	public void view_details(){
		myDialog = new Dialog(Condo_Title_Details.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.report_condo_prop_desc_form);
		myDialog.setCancelable(true);

		android.view.WindowManager.LayoutParams params = myDialog.getWindow()
				.getAttributes();
		params.height = LayoutParams.FILL_PARENT;
		params.width = LayoutParams.FILL_PARENT;
		myDialog.getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		
		btn_save = (Button) myDialog.findViewById(R.id.btn_right);
		btn_cancel = (Button) myDialog.findViewById(R.id.btn_left);
		btn_save.setText("Save");
		btn_cancel.setText("Cancel");
		spinner_property_type = (Spinner) myDialog.findViewById(R.id.spinner_property_type);
		report_title_no = (EditText) myDialog.findViewById(R.id.report_title_no);
		report_unit_no = (EditText) myDialog.findViewById(R.id.report_unit_no);
		report_floor = (EditText) myDialog.findViewById(R.id.report_floor);
		report_building_name = (EditText) myDialog.findViewById(R.id.report_building_name);
		report_fa = (EditText) myDialog.findViewById(R.id.report_fa);
		report_deeds = (EditText) myDialog.findViewById(R.id.report_deeds);
		report_reg_owner = (EditText) myDialog.findViewById(R.id.report_reg_owner);

		valrep_condo_propdesc_registry_date = (EditText) myDialog.findViewById(R.id.valrep_condo_propdesc_registry_date);

		report_fa.addTextChangedListener(new TextWatcher() {

			String current="";
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
									  int after) {
				// TODO Auto-generated method stub

				current = gds.numberFormat(report_fa, this, s, current);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

		});

		List<Condo_API_Title_Details> propDescList = db
				.getCondo_Title_Details(String.valueOf(record_id), String.valueOf(lot_details_id), tbl_name);
		if (!propDescList.isEmpty()) {
			for (Condo_API_Title_Details li : propDescList) {
				if (li.getreport_propdesc_property_type().equals("Unit")){
					spinner_property_type.setSelection(0);
				} else if (li.getreport_propdesc_property_type().equals("Parking_Slot")){
					spinner_property_type.setSelection(1);
				} else if (li.getreport_propdesc_property_type().equals("Drying_Area")){
					spinner_property_type.setSelection(2);
				}
				report_title_no.setText(li.getreport_propdesc_cct_no());
				report_unit_no.setText(li.getreport_propdesc_unit_no());
				report_floor.setText(li.getreport_propdesc_floor());
				report_building_name.setText(li.getreport_propdesc_bldg_name());
				report_fa.setText(gds.numberFormat(li.getreport_propdesc_area()));
				report_deeds.setText(li.getreport_propdesc_reg_of_deeds());
				report_reg_owner.setText(li.getreport_propdesc_registered_owner());
				valrep_condo_propdesc_registry_date.setText(li.getvalrep_condo_propdesc_registry_date());

			}
		}
		
		btn_save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

					//update data in SQLite
					Condo_API_Title_Details li = new Condo_API_Title_Details();
					li.setreport_propdesc_property_type(spinner_property_type.getSelectedItem().toString());
					li.setreport_propdesc_cct_no(report_title_no.getText().toString());
					li.setreport_propdesc_unit_no(report_unit_no.getText().toString());
					li.setreport_propdesc_floor(report_floor.getText().toString());
					li.setreport_propdesc_bldg_name(report_building_name.getText().toString());
					li.setreport_propdesc_area(gds.replaceFormat(report_fa.getText().toString()));
					li.setvalrep_condo_propdesc_registry_date(valrep_condo_propdesc_registry_date.getText().toString());

					li.setreport_propdesc_reg_of_deeds(report_deeds.getText().toString());
					li.setreport_propdesc_registered_owner(report_reg_owner.getText().toString());
					db.updateCondo_Title_Details(li, record_id, lot_details_id, tbl_name);


					Condo_API_Valuation vl = new Condo_API_Valuation();
					vl.setreport_value_property_type(spinner_property_type.getSelectedItem().toString());
					vl.setreport_value_cct_no(report_title_no.getText().toString());
					vl.setreport_value_unit_no(report_unit_no.getText().toString());
					vl.setreport_value_floor_area(gds.replaceFormat(report_fa.getText().toString()));


					//ADDED BY IAN

					String floor_are_s=gds.nullCheck2(report_fa.getText().toString());
					String deduc_s="0";
					String unit_value_s="0";
					List<Condo_API_Valuation> lotValuationDetailsList = db
							.getCondo_Valuation(String.valueOf(record_id), lot_details_id, "condo_unit_valuation");
					if (!lotValuationDetailsList.isEmpty()) {
						for (Condo_API_Valuation cv : lotValuationDetailsList) {

							deduc_s=gds.nullCheck2(cv.getreport_value_deduction());
							unit_value_s=gds.nullCheck2(cv.getreport_value_unit_value());
						}
					}
					String net_area_val = gds.netAreaVL(floor_are_s,deduc_s);
					String total_land_val_val = gds.totalLandVL(floor_are_s,unit_value_s);


					vl.setreport_value_net_area(net_area_val);
					vl.setreport_value_appraised_value(total_land_val_val);

					db.updateCondo_Title_Details_Valuation(vl, record_id, lot_details_id);


					//total land value
					String grand_total_land_val =gds.nullCheck(db.getCondo_API_Total_Land_Value_Lot_Valuation_Details(record_id));
					String grand_total_land_val_s = gds.stringToDecimal(grand_total_land_val);

					Condo_API cv3 = new Condo_API();
					cv3.setreport_final_value_total_appraised_value(grand_total_land_val_s);
					dbMain.updateCondo_Summary_Total(cv3, record_id);
					dbMain.close();
					//END OF ADDED BY IAN


					db.close();
					myDialog.dismiss();
					Toast.makeText(getApplicationContext(), "Saved",Toast.LENGTH_SHORT).show();
					reloadClass();

			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});

		gds.fill_in_error(new EditText[]{report_title_no, report_unit_no, report_floor,
				report_building_name, report_fa,valrep_condo_propdesc_registry_date,report_deeds,report_reg_owner});
		myDialog.show();
	}
	
	//EditText checker
	private boolean validate(EditText[] fields){
        for(int i=0; i<fields.length; i++){
            EditText currentField=fields[i];
            if(currentField.getText().toString().length()<=0){
                return false;
            }
        }
        return true;
	}
	public void reloadClass(){
		Intent intent = getIntent();
		finish();
		startActivity(intent);
	}
	public void delete_item(){
		myDialog = new Dialog(Condo_Title_Details.this);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.delete_dialog);
		myDialog.setCancelable(true);
		final TextView tv_question = (TextView) myDialog.findViewById(R.id.tv_question);
		final Button btn_yes = (Button) myDialog.findViewById(R.id.btn_yes);
		final Button btn_no = (Button) myDialog.findViewById(R.id.btn_no);
		tv_question.setText("Are you sure you want to delete the selected item?");
		btn_yes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				db.deleteCondo_Title_Details(record_id, lot_details_id, tbl_name);
				db.deleteCondo_Valuation(record_id, lot_details_id, "condo_unit_valuation");
				db.close();

				//ADDED BY IAN
				//total land value
				String grand_total_land_val =gds.nullCheck(db.getCondo_API_Total_Land_Value_Lot_Valuation_Details(record_id));
				String grand_total_land_val_s = gds.stringToDecimal(grand_total_land_val);

				Condo_API cv3 = new Condo_API();
				cv3.setreport_final_value_total_appraised_value(grand_total_land_val_s);
				dbMain.updateCondo_Summary_Total(cv3, record_id);
				dbMain.close();
				//END ADDED BY IAN

				myDialog.dismiss();
				reloadClass();
				Toast.makeText(getApplicationContext(), "Data Deleted",Toast.LENGTH_SHORT).show();
			}
		});
		btn_no.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		
		myDialog.show();
	}
	
	
	/**
	 * Background Async Task to Load all
	 * */
	class LoadAll extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Condo_Title_Details.this);
			pDialog.setMessage("Loading Lists. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All
		 * */
		protected String doInBackground(String... args) {
			String cct_no="", reg_owner="", reg_date="";
			int reg_month_int;
			String reg_day="", reg_year="";
			

			List<Condo_API_Title_Details> lild = db.getCondo_Title_Details(record_id, tbl_name);
			if (!lild.isEmpty()) {
				for (Condo_API_Title_Details imld : lild) {
					lot_details_id = String.valueOf(imld.getID());
					cct_no = imld.getreport_propdesc_cct_no();
					reg_owner = imld.getreport_propdesc_registered_owner();

					reg_date = imld.getvalrep_condo_propdesc_registry_date();
					HashMap<String, String> map = new HashMap<String, String>();
					map.put(tag_lot_details_id, lot_details_id);
					map.put(tag_cct_no, cct_no);
					map.put(tag_date_reg, reg_date);
					map.put(tag_reg_owner, reg_owner);

					// adding HashList to ArrayList
					itemList.add(map);
				}
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					ListAdapter adapter = new SimpleAdapter(
							Condo_Title_Details.this, itemList,
							R.layout.report_condo_prop_desc_list, new String[] { tag_lot_details_id, tag_cct_no,
									tag_reg_owner, tag_date_reg},
									new int[] { R.id.primary_key, R.id.item_cct_no, R.id.item_reg_owner, R.id.item_date_reg });
					// updating listview
					setListAdapter(adapter);
				}
			});
			if(itemList.isEmpty()){
				add_new();
			}
		}
	}
	@Override
	public void onUserInteraction(){
		st.resetDisconnectTimer();
	}
	@Override
	public void onStop() {
		super.onStop();
		st.stopDisconnectTimer();
	}
	@Override
	public void onResume() {
		super.onResume();
		st.resetDisconnectTimer();
	}


}
